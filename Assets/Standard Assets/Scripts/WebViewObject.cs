using System;
using UnityEngine;

public class WebViewObject : MonoBehaviour
{
	private Action<string> onJS;

	private Action<string> onError;

	private bool visibility;

	private AndroidJavaObject webView;

	private bool mIsKeyboardVisible;

	public bool IsKeyboardVisible => mIsKeyboardVisible;

	public void SetKeyboardVisible(string pIsVisible)
	{
		mIsKeyboardVisible = (pIsVisible == "true");
	}

	public void Init(Action<string> cb = null, bool transparent = false, Action<string> err = null)
	{
		onJS = cb;
		onError = err;
		webView = new AndroidJavaObject("net.gree.unitywebview.CWebViewPlugin");
		webView.Call("Init", base.name, transparent);
	}

	protected virtual void OnDestroy()
	{
		if (webView != null)
		{
			webView.Call("Destroy");
			webView = null;
		}
	}

	public void SetCenterPositionWithScale(Vector2 center, Vector2 scale)
	{
	}

	public void SetMargins(int left, int top, int right, int bottom)
	{
		if (webView != null)
		{
			webView.Call("SetMargins", left, top, right, bottom);
		}
	}

	public void SetVisibility(bool v)
	{
		if (webView != null)
		{
			webView.Call("SetVisibility", v);
			visibility = v;
		}
	}

	public bool GetVisibility()
	{
		return visibility;
	}

	public void LoadURL(string url)
	{
		if (webView != null)
		{
			webView.Call("LoadURL", url);
		}
	}

	public void EvaluateJS(string js)
	{
		if (webView != null)
		{
			webView.Call("LoadURL", "javascript:" + js);
		}
	}

	public void CallOnError(string message)
	{
		if (onError != null)
		{
			onError(message);
		}
	}

	public void CallFromJS(string message)
	{
		if (onJS != null)
		{
			onJS(message);
		}
	}
}
