using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BuglyInit : MonoBehaviour
{
	private const string BuglyAppID = "YOUR APP ID GOES HERE";

	[CompilerGenerated]
	private static Func<Dictionary<string, string>> _003C_003Ef__mg_0024cache0;

	private void Awake()
	{
		BuglyAgent.ConfigDebugMode(enable: false);
		BuglyAgent.ConfigDefault(null, null, null, 0L);
		BuglyAgent.ConfigAutoReportLogLevel(LogSeverity.LogError);
		BuglyAgent.ConfigAutoQuitApplication(autoQuit: false);
		BuglyAgent.RegisterLogCallback(null);
		BuglyAgent.InitWithAppId("YOUR APP ID GOES HERE");
		BuglyAgent.EnableExceptionHandler();
		BuglyAgent.SetLogCallbackExtrasHandler(MyLogCallbackExtrasHandler);
		UnityEngine.Object.Destroy(this);
	}

	private static Dictionary<string, string> MyLogCallbackExtrasHandler()
	{
		BuglyAgent.PrintLog(LogSeverity.Log, "extra handler");
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("ScreenSolution", $"{Screen.width}x{Screen.height}");
		dictionary.Add("deviceModel", SystemInfo.deviceModel);
		dictionary.Add("deviceName", SystemInfo.deviceName);
		dictionary.Add("deviceType", SystemInfo.deviceType.ToString());
		dictionary.Add("deviceUId", SystemInfo.deviceUniqueIdentifier);
		dictionary.Add("gDId", $"{SystemInfo.graphicsDeviceID}");
		dictionary.Add("gDName", SystemInfo.graphicsDeviceName);
		dictionary.Add("gDVdr", SystemInfo.graphicsDeviceVendor);
		dictionary.Add("gDVer", SystemInfo.graphicsDeviceVersion);
		dictionary.Add("gDVdrID", $"{SystemInfo.graphicsDeviceVendorID}");
		dictionary.Add("graphicsMemorySize", $"{SystemInfo.graphicsMemorySize}");
		dictionary.Add("systemMemorySize", $"{SystemInfo.systemMemorySize}");
		dictionary.Add("UnityVersion", Application.unityVersion);
		BuglyAgent.PrintLog(LogSeverity.LogInfo, "Package extra data");
		return dictionary;
	}
}
