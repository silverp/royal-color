using System;
using UnityEngine;

namespace SVGImporter.Utils
{
	[RequireComponent(typeof(Camera))]
	public class SVGCamera : MonoBehaviour
	{
		public static Action<Camera> onPreRender;

		protected Camera _camera;

		public Camera Camera
		{
			get
			{
				if (_camera == null)
				{
					_camera = GetComponent<Camera>();
				}
				return _camera;
			}
		}

		private void OnPreRender()
		{
			if (onPreRender != null)
			{
				onPreRender(Camera);
			}
		}

		public static void UpdateCameras()
		{
			Camera[] allCameras = Camera.allCameras;
			for (int i = 0; i < allCameras.Length; i++)
			{
				if (!(allCameras[i] == null) && !(allCameras[i].GetComponent<SVGCamera>() != null))
				{
					allCameras[i].gameObject.AddComponent<SVGCamera>();
				}
			}
		}
	}
}
