Shader "UI/Default-White"
{
  Properties
  {
    [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
  }
  SubShader
  {
    Tags
    { 
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
      }
      Fog
      { 
        Mode  Off
      } 
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX 
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float4 xlv_COLOR0 : COLOR0;
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float4 xlv_COLOR0 : COLOR0;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          float4 tmpvar_1;
          
          float4 tmpvar_2;
          
          tmpvar_2 = clamp (float4(1.0, 1.0, 1.0, 1.0), 0.0, 1.0);
          
          tmpvar_1 = tmpvar_2;
          
          float4 tmpvar_3;
          
          tmpvar_3.w = 1.0;
          
          tmpvar_3.xyz = in_v.vertex.xyz;
          
          OUT_Data_Vert OUT;
          OUT.xlv_COLOR0 = tmpvar_1;
          
          OUT.vertex = mul(unity_MatrixVP , mul(unity_ObjectToWorld, tmpvar_3));
      
        return OUT;
      }
      
      
      
      #define CODE_BLOCK_FRAGMENT
      OUT_Data_Frag frag(v2f in_f)
      {
          
          OUT_Data_Frag out_f;
          out_f.color = in_f.xlv_COLOR0;
      
            return out_f;
      }
      
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
