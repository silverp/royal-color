Shader "UI/GradientShader"
{
    Properties
    {
        _MainTex ("MainTex Texture", 2D) = "white" { }
        _Palette ("Palette Texture", 2D) = "white" { }
        _ExtraInfo ("ExtraInfo Texture", 2D) = "white" { }
        _PrevPalette ("Previous Palette Texture", 2D) = "white" { }
        _PrevExtraInfo ("Previous ExtraInfo Texture", 2D) = "white" { }
        _Gradients ("Gradients Texture", 2D) = "white" { }
        _RadiusTime ("Radius Time", float) = 10
        _Mask ("Mask Texture", 2D) = "white" { }
        _HasMask ("HasMask", float) = 0
    }
    SubShader
    {
        Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend SrcAlpha OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            
            
            // uniform float4 unity_ObjectToWorld[4];
            
            // uniform float4 unity_MatrixVP[4];
            
            uniform float _RadiusTime;
            
            uniform int _HasMask;
            
            uniform sampler2D _MainTex;
            
            uniform sampler2D _ExtraInfo;
            
            uniform sampler2D _Palette;
            
            uniform sampler2D _Gradients;
            
            uniform sampler2D _PrevPalette;
            
            uniform sampler2D _PrevExtraInfo;
            
            uniform sampler2D _Mask; 
            float4 _MainTex_ST;
            
            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
            
            
            struct OUT_Data_Vert
            {
                float2 texcoord : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                float4 color : SV_Target0;
            };
            
            
            float4 u_xlat0;
            
            float4 u_xlat1;
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                // OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(v.vertex); 

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            #define CODE_BLOCK_FRAGMENT 
            
            float2 u_xlat0_d;
            
            float4 u_xlat10_0;
            
            float3 u_xlat1_d;
            
            float2 u_xlat16_1;
            
            float4 u_xlat10_1;
            
            int u_xlatb1;
            
            float2 u_xlat2;
            
            float u_xlat16_2;
            
            float4 u_xlat10_2;
            
            float2 u_xlat3;
            
            float2 u_xlat16_3;
            
            float3 u_xlat4;
            
            float3 u_xlat16_4;
            
            float3 u_xlat10_4;
            
            float3 u_xlat5;
            
            float3 u_xlat16_5;
            
            float3 u_xlat10_5;
            
            float3 u_xlat6;
            
            float3 u_xlat16_6;
            
            float2 u_xlat7;
            
            float2 u_xlat16_7;
            
            float3 u_xlat10_7;
            
            int u_xlatb7;
            
            float3 u_xlat8;
            
            float3 u_xlat16_8;
            
            float2 u_xlat16_9;
            
            float2 u_xlat11;
            
            float3 u_xlat16_11;
            
            float3 u_xlat10_11;
            
            int u_xlatb17;
            
            int u_xlatb18;
            
            int u_xlati20;
            
            float2 u_xlat22;
            
            float2 u_xlat23;
            
            float2 u_xlat24;
            
            float2 u_xlat25;
            
            float u_xlat27;
            
            int2 u_xlatb27;
            
            float u_xlat31;
            
            float u_xlat16_31;
            
            int u_xlati31;
            
            int u_xlatb31;
            
            float u_xlat16_36;
            
            float u_xlat37;
            
            int u_xlatb37;
            
            float roundEven(float x)
            {
                float y = floor(x + 0.5);
                return (y - x == 0.5) ? floor(0.5 * y) * 2.0 : y;
            }
            
            float2 roundEven(float2 a)
            {
                a.x = roundEven(a.x);
                a.y = roundEven(a.y);
                return a;
            }
            
            float3 roundEven(float3 a)
            {
                a.x = roundEven(a.x);
                a.y = roundEven(a.y);
                a.z = roundEven(a.z);
                return a;
            }
            
            float4 roundEven(float4 a)
            {
                a.x = roundEven(a.x);
                a.y = roundEven(a.y);
                a.z = roundEven(a.z);
                a.w = roundEven(a.w);
                return a;
            }

            int lessThan(float v, float v2) {
                return v < v2 ? 1 : 0;
            }
            int4 lessThan(float4 f, float value)
            { 
                return int4(lessThan(f[0],value), lessThan(f[1],value), lessThan(f[2],value), lessThan(f[3],value));
            }
            
            OUT_Data_Frag frag(v2f in_f)
            { 
                u_xlat10_0.xy = tex2D(_MainTex, in_f.texcoord.xy).xy;
                
                u_xlat0_d.xy = u_xlat10_0.xy * float2(255.0, 255.0);
                
                u_xlat0_d.xy = roundEven(u_xlat0_d.xy);
                
                u_xlat0_d.xy = u_xlat0_d.xy * float2(0.00392156886, 0.00392156886);
                
                u_xlat0_d.xy = max(u_xlat0_d.xy, float2(0.0, 0.0));
                
                u_xlat0_d.xy = min(u_xlat0_d.xy, float2(0.999998987, 0.999998987));
                
                u_xlat10_1 = tex2D(_ExtraInfo, u_xlat0_d.xy);
                
                //u_xlat16_2 = u_xlat10_1.w * 255.0;
                
                //u_xlat16_2 = roundEven(u_xlat16_2);
                
                //u_xlati20 = int(u_xlat16_2);
                
                //u_xlatb31 = u_xlati20 == 1;
                
                if (u_xlat10_1.w == 1)
                {
                    
                    u_xlat10_2 = tex2D(_Palette, u_xlat0_d.xy);
                    
                    u_xlat16_3.x = u_xlat10_2.w * 255.0;
                    
                    u_xlat16_3.x = roundEven(u_xlat16_3.x);
                    
                    u_xlati31 = int(u_xlat16_3.x);
                    
                    u_xlatb31 = u_xlati31 == 1;
                    
                    if (u_xlatb31)
                    {
                        
                        u_xlat16_4.xy = (-u_xlat10_1.xy) + in_f.texcoord.xy;
                        
                        u_xlat16_4.xy = u_xlat16_4.xy / u_xlat10_1.zz;
                        
                        u_xlat16_31 = dot(u_xlat16_4.xy, u_xlat16_4.xy);
                        
                        u_xlat16_31 = u_xlat16_31 * 0.5;
                        
                        u_xlat16_31 = sqrt(u_xlat16_31);
                        
                        u_xlat16_31 = min(u_xlat16_31, 1.0);
                        
                        u_xlat16_3.x = u_xlat10_2.x * 2.0 + 0.0029296875;
                        
                        u_xlat16_3.y = u_xlat10_2.y * 32.0;
                        
                        u_xlat16_4.xy = max(u_xlat16_3.xy, float2(0.0, 0.0));
                        
                        u_xlat16_4.xy = min(u_xlat16_4.xy, float2(0.999998987, 0.999998987));
                        
                        u_xlat31 = u_xlat16_31 * 64.0;
                        
                        u_xlat24.x = floor(u_xlat31);
                        
                        u_xlat24.x = u_xlat24.x + 0.5;
                        
                        u_xlat3.y = u_xlat24.x * 0.001953125;
                        
                        u_xlat3.x = float(0.0);
                        
                        u_xlat23.x = float(0.0);
                        
                        u_xlat24.xy = u_xlat3.xy + u_xlat16_4.xy;
                        
                        u_xlat10_5.xyz = tex2D(_Gradients, u_xlat24.xy).xyz;
                        
                        u_xlat24.x = ceil(u_xlat31);
                        
                        u_xlat24.x = u_xlat24.x + 0.5;
                        
                        u_xlat24.x = u_xlat24.x * 0.001953125;
                        
                        u_xlat23.y = min(u_xlat24.x, 0.123242185);
                        
                        u_xlat4.xy = u_xlat23.xy + u_xlat16_4.xy;
                        
                        u_xlat10_4.xyz = tex2D(_Gradients, u_xlat4.xy).xyz;
                        
                        u_xlat31 = frac(u_xlat31);
                        
                        u_xlat16_4.xyz = (-u_xlat10_5.xyz) + u_xlat10_4.xyz;
                        
                        u_xlat4.xyz = u_xlat31 * u_xlat16_4.xyz + u_xlat10_5.xyz;
                        
                        u_xlat16_4.xyz = u_xlat4.xyz;
                    }
                    else
                    {
                        
                        u_xlat16_4.xyz = u_xlat10_2.xyz;
                    }
                    
                    u_xlat10_2 = tex2D(_PrevPalette, u_xlat0_d.xy);
                    
                    u_xlat16_6.x = u_xlat10_2.w * 255.0;
                    
                    u_xlat16_6.x = roundEven(u_xlat16_6.x);
                    
                    u_xlati31 = int(u_xlat16_6.x);
                    
                    u_xlatb31 = u_xlati31 == 1;
                    
                    if (u_xlatb31)
                    {
                        
                        u_xlat10_5.xyz = tex2D(_PrevExtraInfo, u_xlat0_d.xy).xyz;
                        
                        u_xlat16_5.xy = (-u_xlat10_5.xy) + in_f.texcoord.xy;
                        
                        u_xlat16_5.xy = u_xlat16_5.xy / u_xlat10_5.zz;
                        
                        u_xlat16_31 = dot(u_xlat16_5.xy, u_xlat16_5.xy);
                        
                        u_xlat16_31 = u_xlat16_31 * 0.5;
                        
                        u_xlat16_31 = sqrt(u_xlat16_31);
                        
                        u_xlat16_31 = min(u_xlat16_31, 1.0);
                        
                        u_xlat16_6.x = u_xlat10_2.x * 2.0 + 0.0029296875;
                        
                        u_xlat16_6.y = u_xlat10_2.y * 32.0;
                        
                        u_xlat16_5.xy = max(u_xlat16_6.xy, float2(0.0, 0.0));
                        
                        u_xlat16_5.xy = min(u_xlat16_5.xy, float2(0.999998987, 0.999998987));
                        
                        u_xlat31 = u_xlat16_31 * 64.0;
                        
                        u_xlat25.x = floor(u_xlat31);
                        
                        u_xlat25.x = u_xlat25.x + 0.5;
                        
                        u_xlat3.y = u_xlat25.x * 0.001953125;
                        
                        u_xlat3.x = float(0.0);
                        
                        u_xlat23.x = float(0.0);
                        
                        u_xlat25.xy = u_xlat3.xy + u_xlat16_5.xy;
                        
                        u_xlat10_7.xyz = tex2D(_Gradients, u_xlat25.xy).xyz;
                        
                        u_xlat25.x = ceil(u_xlat31);
                        
                        u_xlat25.x = u_xlat25.x + 0.5;
                        
                        u_xlat25.x = u_xlat25.x * 0.001953125;
                        
                        u_xlat23.y = min(u_xlat25.x, 0.123242185);
                        
                        u_xlat5.xy = u_xlat23.xy + u_xlat16_5.xy;
                        
                        u_xlat10_5.xyz = tex2D(_Gradients, u_xlat5.xy).xyz;
                        
                        u_xlat31 = frac(u_xlat31);
                        
                        u_xlat16_5.xyz = (-u_xlat10_7.xyz) + u_xlat10_5.xyz;
                        
                        u_xlat5.xyz = u_xlat31 * u_xlat16_5.xyz + u_xlat10_7.xyz;
                        
                        u_xlat16_5.xyz = u_xlat5.xyz;
                    }
                    else
                    {
                        
                        u_xlat16_5.xyz = u_xlat10_2.xyz;
                    }
                    
                    u_xlat16_7.xy = u_xlat10_1.xy + (-in_f.texcoord.xy);
                    
                    u_xlat16_31 = dot(u_xlat16_7.xy, u_xlat16_7.xy);
                    
                    u_xlat31 = sqrt(u_xlat16_31);
                    
                    u_xlat7.xy = (-u_xlat10_1.yx) + in_f.texcoord.yx;
                    
                    u_xlat27 = min(abs(u_xlat7.y), abs(u_xlat7.x));
                    
                    u_xlat37 = max(abs(u_xlat7.y), abs(u_xlat7.x));
                    
                    u_xlat37 = float(1.0) / u_xlat37;
                    
                    u_xlat27 = u_xlat37 * u_xlat27;
                    
                    u_xlat37 = u_xlat27 * u_xlat27;
                    
                    u_xlat8.x = u_xlat37 * 0.0208350997 + - 0.0851330012;
                    
                    u_xlat8.x = u_xlat37 * u_xlat8.x + 0.180141002;
                    
                    u_xlat8.x = u_xlat37 * u_xlat8.x + - 0.330299497;
                    
                    u_xlat37 = u_xlat37 * u_xlat8.x + 0.999866009;
                    
                    u_xlat8.x = u_xlat37 * u_xlat27;
                    
                    u_xlatb18 = abs(u_xlat7.y) < abs(u_xlat7.x);
                    
                    u_xlat8.x = u_xlat8.x * - 2.0 + 1.57079637;
                    
                    u_xlat8.x = u_xlatb18 ? u_xlat8.x : float(0.0);
                    
                    u_xlat27 = u_xlat27 * u_xlat37 + u_xlat8.x;
                    
                    u_xlatb37 = u_xlat7.y < (-u_xlat7.y);
                    
                    u_xlat37 = u_xlatb37 ? - 3.14159274 : float(0.0);
                    
                    u_xlat27 = u_xlat37 + u_xlat27;
                    
                    u_xlat37 = min(u_xlat7.y, u_xlat7.x);
                    
                    u_xlat7.x = max(u_xlat7.y, u_xlat7.x);
                    
                    u_xlatb17 = u_xlat37 < (-u_xlat37);
                    
                    u_xlatb7 = u_xlat7.x >= (-u_xlat7.x);
                    
                    u_xlatb7 = u_xlatb7 && u_xlatb17;
                    
                    u_xlat7.x = (u_xlatb7) ? (-u_xlat27) : u_xlat27;
                    
                    u_xlat7.x = u_xlat7.x * 20.0;
                    
                    u_xlat7.x = sin(u_xlat7.x);
                    
                    u_xlat7.x = u_xlat7.x * _RadiusTime;
                    
                    u_xlat7.x = u_xlat7.x * 0.0199999996 + _RadiusTime;
                    
                    u_xlat7.y = u_xlat7.x + 0.0250000004;
                    
                    u_xlatb27.xy = lessThan(u_xlat7.yxyx, u_xlat31).xy;
                    
                    u_xlatb17 = u_xlat7.y >= u_xlat31;
                    
                    u_xlatb17 = u_xlatb27.y && u_xlatb17;
                    
                    u_xlat31 = u_xlat31 + (-u_xlat7.x);
                    
                    u_xlat31 = u_xlat31 * 40.0;
                    
                    u_xlat16_8.xyz = (-u_xlat16_4.xyz) + u_xlat16_5.xyz;
                    
                    u_xlat8.xyz = u_xlat31 * u_xlat16_8.xyz + u_xlat16_4.xyz;
                    
                    u_xlat16_6.xyz = (int(u_xlatb17)) ? u_xlat8.xyz : u_xlat16_4.xyz;
                    
                    u_xlat16_6.xyz = (u_xlatb27.x) ? u_xlat16_5.xyz : u_xlat16_6.xyz;
                }
                else
                {
                    
                    u_xlat10_0 = tex2D(_Palette, u_xlat0_d.xy);
                    
                    u_xlat16_36 = u_xlat10_0.w * 255.0;
                    
                    u_xlat16_36 = roundEven(u_xlat16_36);
                    
                    u_xlati31 = int(u_xlat16_36);
                    
                    u_xlatb31 = u_xlati31 == 1;
                    
                    if (u_xlatb31)
                    {
                        
                        u_xlat16_1.xy = (-u_xlat10_1.xy) + in_f.texcoord.xy;
                        
                        u_xlat16_1.xy = u_xlat16_1.xy / u_xlat10_1.zz;
                        
                        u_xlat16_1.x = dot(u_xlat16_1.xy, u_xlat16_1.xy);
                        
                        u_xlat16_1.x = u_xlat16_1.x * 0.5;
                        
                        u_xlat1_d.x = sqrt(u_xlat16_1.x);
                        
                        u_xlat16_9.x = u_xlat10_0.x * 2.0 + 0.0029296875;
                        
                        u_xlat16_9.y = u_xlat10_0.y * 32.0;
                        
                        u_xlat1_d.yz = max(u_xlat16_9.xy, float2(0.0, 0.0));
                        
                        u_xlat1_d.xyz = min(u_xlat1_d.xyz, float3(1.0, 0.999998987, 0.999998987));
                        
                        u_xlat1_d.x = u_xlat1_d.x * 64.0;
                        
                        u_xlat31 = floor(u_xlat1_d.x);
                        
                        u_xlat31 = u_xlat31 + 0.5;
                        
                        u_xlat2.y = u_xlat31 * 0.001953125;
                        
                        u_xlat2.x = float(0.0);
                        
                        u_xlat22.x = float(0.0);
                        
                        u_xlat7.xy = u_xlat1_d.yz + u_xlat2.xy;
                        
                        u_xlat10_7.xyz = tex2D(_Gradients, u_xlat7.xy).xyz;
                        
                        u_xlat31 = ceil(u_xlat1_d.x);
                        
                        u_xlat31 = u_xlat31 + 0.5;
                        
                        u_xlat31 = u_xlat31 * 0.001953125;
                        
                        u_xlat22.y = min(u_xlat31, 0.123242185);
                        
                        u_xlat11.xy = u_xlat1_d.yz + u_xlat22.xy;
                        
                        u_xlat10_11.xyz = tex2D(_Gradients, u_xlat11.xy).xyz;
                        
                        u_xlat1_d.x = frac(u_xlat1_d.x);
                        
                        u_xlat16_11.xyz = (-u_xlat10_7.xyz) + u_xlat10_11.xyz;
                        
                        u_xlat6.xyz = u_xlat1_d.xxx * u_xlat16_11.xyz + u_xlat10_7.xyz;
                        
                        u_xlat16_6.xyz = u_xlat6.xyz;
                    }
                    else
                    {
                        
                        u_xlat16_6.xyz = u_xlat10_0.xyz;
                    }
                }
                
                u_xlatb1 = _HasMask == 1;
                
                if (u_xlatb1)
                {
                    
                    u_xlat10_0 = tex2D(_Mask, in_f.texcoord.xy);
                    
                    u_xlat16_36 = u_xlat10_0.y + u_xlat10_0.x;
                    
                    u_xlat16_36 = u_xlat10_0.z + u_xlat16_36;
                    
                    u_xlat16_1.x = (-u_xlat16_36) * 0.333333343 + 1.0;
                    
                    u_xlat16_1.x = u_xlat10_0.w * u_xlat16_1.x;
                    
                    u_xlat16_6.xyz = u_xlat16_1.xxx * (-u_xlat16_6.xyz) + u_xlat16_6.xyz;
                }
                
                OUT_Data_Frag out_f;

                out_f.color.xyz = u_xlat16_6.xyz;
                
                out_f.color.w = 1.0;
                
                return out_f;
            }
            
            
            ENDCG
        }// end phase

    }
    FallBack Off
}
