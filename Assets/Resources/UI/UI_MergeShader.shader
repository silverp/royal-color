Shader "UI/MergeShader"
{
  Properties
  {
    _MainTex ("Sprite Texture", 2D) = "white" {}
    _Palette ("Palette Texture", 2D) = "white" {}
    _Mask ("Mask Texture", 2D) = "white" {}
    _Palette_Scale ("Palette_Scale Scale", float) = 1
  }
  SubShader
  {
    Tags
    { 
      "CanUseSpriteAtlas" = "true"
      "IGNOREPROJECTOR" = "true"
      "PreviewType" = "Plane"
      "QUEUE" = "Transparent"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "CanUseSpriteAtlas" = "true"
        "IGNOREPROJECTOR" = "true"
        "PreviewType" = "Plane"
        "QUEUE" = "Transparent"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Fog
      { 
        Mode  Off
      } 
      Blend SrcAlpha OneMinusSrcAlpha
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      
      //uniform float4x4 unity_ObjectToWorld;
      
      //uniform float4x4 unity_MatrixVP;
      
      uniform sampler2D _MainTex;
      
      uniform sampler2D _Palette;
      
      uniform float4 _Palette_ST;
      
      uniform int _Palette_Scale;
      
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION;
          
          float4 color : COLOR;
          
          float4 texcoord : TEXCOORD0;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float4 xlv_COLOR : COLOR;
          
          float2 xlv_TEXCOORD0 : TEXCOORD0;
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float2 xlv_TEXCOORD0 : TEXCOORD0;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          float4 tmpvar_1;
          
          tmpvar_1 = in_v.color;
          
          float2 tmpvar_2;
          
          tmpvar_2 = in_v.texcoord.xy;
          
          float4 tmpvar_3;
          
          float2 tmpvar_4;
          
          float4 tmpvar_5;
          
          tmpvar_5.w = 1.0;
          
          tmpvar_5.xyz = in_v.vertex.xyz;
          
          tmpvar_4 = tmpvar_2;
          
          tmpvar_3 = tmpvar_1;
          
    
          OUT_Data_Vert OUT;
          OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_5));
          
          OUT.xlv_COLOR = tmpvar_3;
          
          OUT.xlv_TEXCOORD0 = tmpvar_4;
    return OUT;

}
      
      
      
      #define CODE_BLOCK_FRAGMENT
      OUT_Data_Frag frag(v2f in_f)
      {
          
          float4 outColor_1;
          
          float y_2;
          
          float x_3; 
          
          float4 tmpvar_4 = tex2D (_MainTex, in_f.xlv_TEXCOORD0);
          
          float tmpvar_5;
          
          tmpvar_5 = tmpvar_4.x;
          
          x_3 = tmpvar_5;
          
          float tmpvar_6;
          
          tmpvar_6 = tmpvar_4.y;
          
          y_2 = tmpvar_6;
          
          float2 tmpvar_7;
          
          tmpvar_7.x = (floor(( (x_3 * 255.0) + 0.5)) / 255.0);
          
          tmpvar_7.y = (floor(( (y_2 * 255.0) + 0.5)) / 255.0);
          
          float2 P_8;
          
          P_8 = ((( clamp (tmpvar_7, float2(0.0, 0.0), float2(0.999999, 0.999999)) * _Palette_ST.xy) + _Palette_ST.zw) * float(_Palette_Scale));
          
          outColor_1.xyz = tex2D (_Palette, P_8).xyz;
          
          outColor_1.w = tmpvar_4.w;
          
          OUT_Data_Frag out_f;
          out_f.color = outColor_1;
          return out_f;
      }
      
      
      
      ENDCG
      
    } // end phase
    Pass // ind: 2, name: 
    {
      Tags
      { 
        "CanUseSpriteAtlas" = "true"
        "IGNOREPROJECTOR" = "true"
        "PreviewType" = "Plane"
        "QUEUE" = "Transparent"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Fog
      { 
        Mode  Off
      } 
      Blend SrcAlpha OneMinusSrcAlpha
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      
      //uniform float4x4 unity_ObjectToWorld;
      
      //uniform float4x4 unity_MatrixVP;
      
      uniform sampler2D _Mask;
      
      
      
      struct appdata_t
      {
          
          float4 vertex : POSITION;
          
          float4 texcoord : TEXCOORD0;
      
      };
      
      
      struct OUT_Data_Vert
      {
          
          float2 xlv_TEXCOORD0 : TEXCOORD0;
          
          float4 vertex : SV_POSITION;
      
      };
      
      
      struct v2f
      {
          
          float2 xlv_TEXCOORD0 : TEXCOORD0;
      
      };
      
      
      struct OUT_Data_Frag
      {
          
          float4 color : SV_Target0;
      
      };
      
      
      OUT_Data_Vert vert(appdata_t in_v)
      {
          
          float4 tmpvar_1;
          
          tmpvar_1.w = 1.0;
          
          tmpvar_1.xyz = in_v.vertex.xyz;
          
          OUT_Data_Vert OUT;
          OUT.xlv_TEXCOORD0 = in_v.texcoord.xy;
          
          OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
          return OUT;
      }
      
      
      
      #define CODE_BLOCK_FRAGMENT
      OUT_Data_Frag frag(v2f in_f)
      {
           
          
          float4 tmpvar_1 = tex2D (_Mask, in_f.xlv_TEXCOORD0);
          OUT_Data_Frag out_f;
          out_f.color = tmpvar_1;
        return out_f;
      }
      
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}
