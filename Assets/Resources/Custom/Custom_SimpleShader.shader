Shader "Custom/SimpleShader"
{
    Properties { }
    SubShader
    {
        Tags {  }
        Pass // ind: 1, name:

        {
            Tags { "LIGHTMODE" = "Vertex" }
            Fog
            {
                Mode  Off
            }
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX 
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float3 normal : NORMAL;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_COLOR0 : COLOR0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float4 xlv_COLOR0 : COLOR0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            float3 createVector3(float v)
            {
              float3 ret = float3(v, v, v);
              return ret;
            }
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4x4 m_1;
                
                m_1 = mul(unity_WorldToObject, unity_MatrixInvV);
                
                float4 tmpvar_2;
                
                float4 tmpvar_3;
                
                float4 tmpvar_4;
                
                tmpvar_2.x = m_1[0].x;
                
                tmpvar_2.y = m_1[1].x;
                
                tmpvar_2.z = m_1[2].x;
                
                tmpvar_2.w = m_1[3].x;
                
                tmpvar_3.x = m_1[0].y;
                
                tmpvar_3.y = m_1[1].y;
                
                tmpvar_3.z = m_1[2].y;
                
                tmpvar_3.w = m_1[3].y;
                
                tmpvar_4.x = m_1[0].z;
                
                tmpvar_4.y = m_1[1].z;
                
                tmpvar_4.z = m_1[2].z;
                
                tmpvar_4.w = m_1[3].z;
                
                float3 lcolor_5;
                
                float3 eyeNormal_6;
                
                float4 color_7;
                
                float3x3 tmpvar_8;
                
                tmpvar_8[0] = tmpvar_2.xyz;
                
                tmpvar_8[1] = tmpvar_3.xyz;
                
                tmpvar_8[2] = tmpvar_4.xyz;
                
                float3 tmpvar_9;
                
                tmpvar_9 = normalize(mul(tmpvar_8, in_v.normal));
                
                eyeNormal_6 = tmpvar_9;
                
                float3 tmpvar_10;
                
                tmpvar_10 = glstate_lightmodel_ambient.xyz;
                
                lcolor_5 = tmpvar_10;
                
                float3 tmpvar_11;
                
                tmpvar_11 = unity_LightPosition[0].xyz;
                
                float3 dirToLight_12;
                
                dirToLight_12 = tmpvar_11;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_12), 0.0)),unity_LightColor[0].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_13;
                
                tmpvar_13 = unity_LightPosition[1].xyz;
                
                float3 dirToLight_14;
                
                dirToLight_14 = tmpvar_13;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_14), 0.0)), unity_LightColor[1].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_15;
                
                tmpvar_15 = unity_LightPosition[2].xyz;
                
                float3 dirToLight_16;
                
                dirToLight_16 = tmpvar_15;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_16), 0.0)), unity_LightColor[2].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_17;
                
                tmpvar_17 = unity_LightPosition[3].xyz;
                
                float3 dirToLight_18;
                
                dirToLight_18 = tmpvar_17;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_18), 0.0)), unity_LightColor[3].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_19;
                
                tmpvar_19 = unity_LightPosition[4].xyz;
                
                float3 dirToLight_20;
                
                dirToLight_20 = tmpvar_19;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_20), 0.0)), unity_LightColor[4].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_21;
                
                tmpvar_21 = unity_LightPosition[5].xyz;
                
                float3 dirToLight_22;
                
                dirToLight_22 = tmpvar_21;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_22), 0.0)), unity_LightColor[5].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_23;
                
                tmpvar_23 = unity_LightPosition[6].xyz;
                
                float3 dirToLight_24;
                
                dirToLight_24 = tmpvar_23;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_24), 0.0)), unity_LightColor[6].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                float3 tmpvar_25;
                
                tmpvar_25 = unity_LightPosition[7].xyz;
                
                float3 dirToLight_26;
                
                dirToLight_26 = tmpvar_25;
                
                lcolor_5 = (lcolor_5 + min((mul(createVector3(max(dot(eyeNormal_6, dirToLight_26), 0.0)), unity_LightColor[7].xyz) * 0.5), float3(1.0, 1.0, 1.0)));
                
                color_7.xyz = lcolor_5;
                
                color_7.w = 1.0;
                
                float4 tmpvar_27;
                
                float4 tmpvar_28;
                
                tmpvar_28 = clamp(color_7, 0.0, 1.0);
                
                tmpvar_27 = tmpvar_28;
                
                float4 tmpvar_29;
                
                tmpvar_29.w = 1.0;
                
                tmpvar_29.xyz = in_v.vertex.xyz;
                
                OUT_Data_Vert OUT;
                OUT.xlv_COLOR0 = tmpvar_27;
                
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_29));
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                OUT_Data_Frag out_f;
                out_f.color = in_f.xlv_COLOR0;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack Off
}
