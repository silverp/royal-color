Shader "Custom/PixelPreview"
{
    Properties
    {
        _MainTex ("MainTex (RGB)", 2D) = "white" { }
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        Pass // ind: 1, name:

        {
            Tags { "RenderType" = "Opaque" }
            LOD 200
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            float4 _MainTex_ST;
            
            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_INPUT_INSTANCE_ID
            }; 

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                // OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(v.vertex); 

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 dstColor_1;
                
                dstColor_1.xyz = tex2D(_MainTex, in_f.texcoord).xyz;
                
                dstColor_1.w = 1.0;
                
                OUT_Data_Frag out_f;
                out_f.color = dstColor_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
