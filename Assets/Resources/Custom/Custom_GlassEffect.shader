Shader "Custom/GlassEffect"
{
    Properties
    {
        _blurSizeXY ("BlurSizeXY", Range(0, 10)) = 2
    }
    SubShader
    {
        Tags { "QUEUE" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags {  }
            ZClip Off
            ZWrite Off
            Cull Off
            Stencil
            {
                Ref 0
                ReadMask 0
                WriteMask 0
                Pass Keep
                Fail Keep
                ZFail Keep
                PassFront Keep
                FailFront Keep
                ZFailFront Keep
                PassBack Keep
                FailBack Keep
                ZFailBack Keep
            }
            // m_ProgramMask = 0
            // Program "vp" { }
            // Program "fp" { }
            // Program "gp" { }
            // Program "hp" { }
            // Program "dp" { }
            // Program "surface" { }
            // Program "rtp" { }
        }// end phase
        Pass // ind: 2, name:

        {
            Tags { "QUEUE" = "Transparent" }
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
             
            uniform sampler2D _GrabTexture;
            
            uniform float _blurSizeXY;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float4 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                float4 tmpvar_2;
                
                float4 tmpvar_3;
                
                tmpvar_3.w = 1.0;
                
                tmpvar_3.xyz = in_v.vertex.xyz;
                
                tmpvar_2 = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_3));
                
                tmpvar_1 = tmpvar_2;
                
                if ((_ProjectionParams.x < 0.0))
                {
                    
                    tmpvar_1.y = (1.0 - tmpvar_2.y);
                };
                
                OUT_Data_Vert OUT;
                OUT.vertex = tmpvar_2;
                
                OUT.xlv_TEXCOORD0 = tmpvar_1;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float2 screenPos_1;
                
                float tmpvar_2;
                
                tmpvar_2 = (_blurSizeXY * 0.0005);
                
                screenPos_1 = (((in_f.xlv_TEXCOORD0.xy / in_f.xlv_TEXCOORD0.w) + float2(1.0, 1.0)) * float2(0.5, 0.5));
                
                float4 sum_3;
                
                float2 tmpvar_4;
                
                tmpvar_4.x = (screenPos_1.x - (5.0 * tmpvar_2));
                
                tmpvar_4.y = (screenPos_1.y + (5.0 * tmpvar_2));
                
                float4 tmpvar_5;
                
                tmpvar_5 = tex2D(_GrabTexture, tmpvar_4);
                
                sum_3 = (tmpvar_5 * 0.025);
                
                float2 tmpvar_6;
                
                tmpvar_6.x = (screenPos_1.x + (5.0 * tmpvar_2));
                
                tmpvar_6.y = (screenPos_1.y - (5.0 * tmpvar_2));
                
                float4 tmpvar_7;
                
                tmpvar_7 = tex2D(_GrabTexture, tmpvar_6);
                
                sum_3 = (sum_3 + (tmpvar_7 * 0.025));
                
                float2 tmpvar_8;
                
                tmpvar_8.x = (screenPos_1.x - (4.0 * tmpvar_2));
                
                tmpvar_8.y = (screenPos_1.y + (4.0 * tmpvar_2));
                
                float4 tmpvar_9;
                
                tmpvar_9 = tex2D(_GrabTexture, tmpvar_8);
                
                sum_3 = (sum_3 + (tmpvar_9 * 0.05));
                
                float2 tmpvar_10;
                
                tmpvar_10.x = (screenPos_1.x + (4.0 * tmpvar_2));
                
                tmpvar_10.y = (screenPos_1.y - (4.0 * tmpvar_2));
                
                float4 tmpvar_11;
                
                tmpvar_11 = tex2D(_GrabTexture, tmpvar_10);
                
                sum_3 = (sum_3 + (tmpvar_11 * 0.05));
                
                float2 tmpvar_12;
                
                tmpvar_12.x = (screenPos_1.x - (3.0 * tmpvar_2));
                
                tmpvar_12.y = (screenPos_1.y + (3.0 * tmpvar_2));
                
                float4 tmpvar_13;
                
                tmpvar_13 = tex2D(_GrabTexture, tmpvar_12);
                
                sum_3 = (sum_3 + (tmpvar_13 * 0.09));
                
                float2 tmpvar_14;
                
                tmpvar_14.x = (screenPos_1.x + (3.0 * tmpvar_2));
                
                tmpvar_14.y = (screenPos_1.y - (3.0 * tmpvar_2));
                
                float4 tmpvar_15;
                
                tmpvar_15 = tex2D(_GrabTexture, tmpvar_14);
                
                sum_3 = (sum_3 + (tmpvar_15 * 0.09));
                
                float2 tmpvar_16;
                
                tmpvar_16.x = (screenPos_1.x - (2.0 * tmpvar_2));
                
                tmpvar_16.y = (screenPos_1.y + (2.0 * tmpvar_2));
                
                float4 tmpvar_17;
                
                tmpvar_17 = tex2D(_GrabTexture, tmpvar_16);
                
                sum_3 = (sum_3 + (tmpvar_17 * 0.12));
                
                float2 tmpvar_18;
                
                tmpvar_18.x = (screenPos_1.x + (2.0 * tmpvar_2));
                
                tmpvar_18.y = (screenPos_1.y - (2.0 * tmpvar_2));
                
                float4 tmpvar_19;
                
                tmpvar_19 = tex2D(_GrabTexture, tmpvar_18);
                
                sum_3 = (sum_3 + (tmpvar_19 * 0.12));
                
                float2 tmpvar_20;
                
                tmpvar_20.x = (screenPos_1.x - tmpvar_2);
                
                tmpvar_20.y = (screenPos_1.y + tmpvar_2);
                
                float4 tmpvar_21;
                
                tmpvar_21 = tex2D(_GrabTexture, tmpvar_20);
                
                sum_3 = (sum_3 + (tmpvar_21 * 0.15));
                
                float2 tmpvar_22;
                
                tmpvar_22.x = (screenPos_1.x + tmpvar_2);
                
                tmpvar_22.y = (screenPos_1.y - tmpvar_2);
                
                float4 tmpvar_23;
                
                tmpvar_23 = tex2D(_GrabTexture, tmpvar_22);
                
                sum_3 = (sum_3 + (tmpvar_23 * 0.15));
                
                float4 tmpvar_24;
                
                float2 P_25;
                
                P_25 = (screenPos_1 - (5.0 * tmpvar_2));
                
                tmpvar_24 = tex2D(_GrabTexture, P_25);
                
                sum_3 = (sum_3 + (tmpvar_24 * 0.025));
                
                float4 tmpvar_26;
                
                float2 P_27;
                
                P_27 = (screenPos_1 - (4.0 * tmpvar_2));
                
                tmpvar_26 = tex2D(_GrabTexture, P_27);
                
                sum_3 = (sum_3 + (tmpvar_26 * 0.05));
                
                float4 tmpvar_28;
                
                float2 P_29;
                
                P_29 = (screenPos_1 - (3.0 * tmpvar_2));
                
                tmpvar_28 = tex2D(_GrabTexture, P_29);
                
                sum_3 = (sum_3 + (tmpvar_28 * 0.09));
                
                float4 tmpvar_30;
                
                float2 P_31;
                
                P_31 = (screenPos_1 - (2.0 * tmpvar_2));
                
                tmpvar_30 = tex2D(_GrabTexture, P_31);
                
                sum_3 = (sum_3 + (tmpvar_30 * 0.12));
                
                float4 tmpvar_32;
                
                float2 P_33;
                
                P_33 = (screenPos_1 - tmpvar_2);
                
                tmpvar_32 = tex2D(_GrabTexture, P_33);
                
                sum_3 = (sum_3 + (tmpvar_32 * 0.15));
                
                float4 tmpvar_34;
                
                tmpvar_34 = tex2D(_GrabTexture, screenPos_1);
                
                sum_3 = (sum_3 + (tmpvar_34 * 0.16));
                
                float4 tmpvar_35;
                
                float2 P_36;
                
                P_36 = (screenPos_1 + (5.0 * tmpvar_2));
                
                tmpvar_35 = tex2D(_GrabTexture, P_36);
                
                sum_3 = (sum_3 + (tmpvar_35 * 0.15));
                
                float4 tmpvar_37;
                
                float2 P_38;
                
                P_38 = (screenPos_1 + (4.0 * tmpvar_2));
                
                tmpvar_37 = tex2D(_GrabTexture, P_38);
                
                sum_3 = (sum_3 + (tmpvar_37 * 0.12));
                
                float4 tmpvar_39;
                
                float2 P_40;
                
                P_40 = (screenPos_1 + (3.0 * tmpvar_2));
                
                tmpvar_39 = tex2D(_GrabTexture, P_40);
                
                sum_3 = (sum_3 + (tmpvar_39 * 0.09));
                
                float4 tmpvar_41;
                
                float2 P_42;
                
                P_42 = (screenPos_1 + (2.0 * tmpvar_2));
                
                tmpvar_41 = tex2D(_GrabTexture, P_42);
                
                sum_3 = (sum_3 + (tmpvar_41 * 0.05));
                
                float4 tmpvar_43;
                
                float2 P_44;
                
                P_44 = (screenPos_1 + tmpvar_2);
                
                tmpvar_43 = tex2D(_GrabTexture, P_44);
                
                sum_3 = (sum_3 + (tmpvar_43 * 0.025));
                
                OUT_Data_Frag out_f;
                out_f.color = (sum_3 / 2.0);
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack Off
}
