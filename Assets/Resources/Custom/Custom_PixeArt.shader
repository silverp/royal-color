Shader "Custom/PixeArt"
{
    Properties
    {
        _MainTex ("MainTex (RGB)", 2D) = "white" { }
        _ColorMap ("ColorMap (RGB)", 2D) = "white" { }
        _FontTex ("Font Texure", 2D) = "white" { }
        _FrameTex ("FrameTex", 2D) = "white" { }
        _ColorIndex ("ColorIndex", 2D) = "white" { }
        _TexScale ("MainTex Scale", float) = 1
        _ChooseColor ("Choose Color", float) = 0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200
        Pass // ind: 1, name:

        {
            Tags { "RenderType" = "Opaque" }
            LOD 200
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            uniform float4 _MainTex_TexelSize;
            
            uniform sampler2D _FontTex;
            
            uniform sampler2D _ColorIndex;
            
            uniform sampler2D _ColorMap;
            
            uniform sampler2D _FrameTex;
            
            uniform float _TexScale;
            
            uniform int _ChooseColor;
            float4 _MainTex_ST;
            
            struct appdata_t
            {
                float4 vertex : POSITION;
                float4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_INPUT_INSTANCE_ID
            }; 

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0; 
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                // OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(v.vertex); 

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 dstColor_1;
                
                float4 tmpvar_2;
                
                tmpvar_2 = tex2D(_MainTex, in_f.texcoord);
                
                dstColor_1 = tmpvar_2;
                
                float tmpvar_3;
                
                float tmpvar_4;
                
                tmpvar_4 = floor(((tmpvar_2.w * 255.0) + 0.5));
                
                tmpvar_3 = tmpvar_4;
                
                float tmpvar_5;
                
                float tmpvar_6;
                
                tmpvar_6 = floor(((tex2D(_ColorMap, in_f.texcoord).x * 255.0) + 0.5));
                
                tmpvar_5 = tmpvar_6;
                
                if ((_TexScale > 16.0))
                {
                    
                    if (((tmpvar_3 != 255.0) && (tmpvar_3 != 0.0)))
                    {
                        
                        float2 uv_7;
                        
                        float tmpvar_8;
                        
                        tmpvar_8 = clamp((tmpvar_3 - 1.0), 0.0, 63.0);
                        
                        float tmpvar_9;
                        
                        tmpvar_9 = (tmpvar_8 / 8.0);
                        
                        float tmpvar_10;
                        
                        tmpvar_10 = (frac(abs(tmpvar_9)) * 8.0);
                        
                        float tmpvar_11;
                        
                        if ((tmpvar_9 >= 0.0))
                        {
                            
                            tmpvar_11 = tmpvar_10;
                        }
                        else
                        {
                            
                            tmpvar_11 = - (tmpvar_10);
                        };
                        
                        uv_7.x = ((tmpvar_11 + frac((in_f.texcoord.x * _MainTex_TexelSize.z))) / 8.0);
                        
                        uv_7.y = ((floor((tmpvar_8 / 8.0)) + frac((in_f.texcoord.y * _MainTex_TexelSize.w))) / 8.0);
                        
                        dstColor_1 = tex2D(_FontTex, uv_7);
                    };
                    
                    if ((tmpvar_3 != 255.0))
                    {
                        
                        float2 tmpvar_12;
                        
                        tmpvar_12 = frac((in_f.texcoord * _MainTex_TexelSize.w));
                        
                        float4 tmpvar_13;
                        
                        tmpvar_13 = tex2D(_FrameTex, tmpvar_12);
                        
                        dstColor_1 = lerp(dstColor_1, tmpvar_13, tmpvar_13.wwww);
                        
                        if ((tmpvar_5 != 0.0))
                        {
                            
                            float2 uv_14;
                            
                            float tmpvar_15;
                            
                            tmpvar_15 = clamp((tmpvar_5 - 1.0), 0.0, 63.0);
                            
                            float tmpvar_16;
                            
                            tmpvar_16 = (tmpvar_15 / 8.0);
                            
                            float tmpvar_17;
                            
                            tmpvar_17 = (frac(abs(tmpvar_16)) * 8.0);
                            
                            float tmpvar_18;
                            
                            if ((tmpvar_16 >= 0.0))
                            {
                                
                                tmpvar_18 = tmpvar_17;
                            }
                            else
                            {
                                
                                tmpvar_18 = - (tmpvar_17);
                            };
                            
                            uv_14.x = (tmpvar_18 / 8.0);
                            
                            uv_14.y = clamp(((1.0 - (floor((tmpvar_15 / 8.0)) / 8.0)) - 0.001960784), 0.0, 1.0);
                            
                            float4 tmpvar_19;
                            
                            tmpvar_19 = tex2D(_ColorIndex, uv_14);
                            
                            if ((tmpvar_5 == tmpvar_3))
                            {
                                
                                dstColor_1 = tmpvar_19;
                            }
                            else
                            {
                                
                                dstColor_1 = mul(lerp(tmpvar_19, float4(1.0, 1.0, 1.0, 1.0), float4(0.5, 0.5, 0.5, 0.5)), dstColor_1);
                            };
                        }
                        else
                        {
                            
                            if ((tmpvar_3 == float(_ChooseColor)))
                            {
                                
                                dstColor_1 = mul(dstColor_1, float4(0.85, 0.85, 0.85, 1.0));
                            };
                        };
                    };
                }
                else
                {
                    
                    if ((tmpvar_5 != 0.0))
                    {
                        
                        float2 uv_20;
                        
                        float tmpvar_21;
                        
                        tmpvar_21 = clamp((tmpvar_5 - 1.0), 0.0, 63.0);
                        
                        float tmpvar_22;
                        
                        tmpvar_22 = (tmpvar_21 / 8.0);
                        
                        float tmpvar_23;
                        
                        tmpvar_23 = (frac(abs(tmpvar_22)) * 8.0);
                        
                        float tmpvar_24;
                        
                        if ((tmpvar_22 >= 0.0))
                        {
                            
                            tmpvar_24 = tmpvar_23;
                        }
                        else
                        {
                            
                            tmpvar_24 = - (tmpvar_23);
                        };
                        
                        uv_20.x = (tmpvar_24 / 8.0);
                        
                        uv_20.y = clamp(((1.0 - (floor((tmpvar_21 / 8.0)) / 8.0)) - 0.001960784), 0.0, 1.0);
                        
                        dstColor_1 = tex2D(_ColorIndex, uv_20);
                    };
                };
                
                dstColor_1.w = 1.0;
                
                OUT_Data_Frag out_f;
                out_f.color = dstColor_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
