Shader "custom/svg/GradiantShader"
{
    Properties
    {
        _GradientColor ("Gradient Color (RGBA)", 2D) = "white" { }
        _GradientShape ("Gradient Shape (RGBA)", 2D) = "white" { }
        _Params ("Params", Vector) = (1, 1, 1, 1)
        _Color ("Tint", Color) = (1, 1, 1, 1)
        _StencilComp ("Stencil Comparison", float) = 8
        _Stencil ("Stencil ID", float) = 0
        _StencilOp ("Stencil Operation", float) = 0
        _StencilWriteMask ("Stencil Write Mask", float) = 255
        _StencilReadMask ("Stencil Read Mask", float) = 255
        _ColorMask ("Color Mask", float) = 15
        _MainTex ("MainTex Texture", 2D) = "white" { }
        _Palette ("Palette Texture", 2D) = "white" { }
        _PositionIndex ("Palette Extract Texture", 2D) = "white" { }
        _Grandiants ("Grandiants Texture", 2D) = "white" { }
        _Mask ("Mask Texture", 2D) = "white" { }
        _Palette_Scale ("Palette_Scale Scale", float) = 1
    }
    SubShader
    {
        Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend SrcAlpha OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 color : COLOR;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_COLOR : COLOR;
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1 = in_v.color;
                
                float2 tmpvar_2;
                
                tmpvar_2 = in_v.texcoord.xy;
                
                float4 tmpvar_3;
                
                float2 tmpvar_4;
                
                float4 tmpvar_5;
                
                tmpvar_5.w = 1.0;
                
                tmpvar_5.xyz = in_v.vertex.xyz;
                
                tmpvar_4 = tmpvar_2;
                
                tmpvar_3 = tmpvar_1;
                
                OUT_Data_Vert OUT;
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_5));
                
                OUT.xlv_COLOR = tmpvar_3;
                
                OUT.xlv_TEXCOORD0 = tmpvar_4;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                
                OUT_Data_Frag out_f;
                out_f.color = tmpvar_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack Off
}
