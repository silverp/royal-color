Shader "Custom/sticker/wall-bricks"
{
    Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" { }
        _StickerTex ("Sticker Texture", 2D) = "white" { }
        _ColorDodge ("ColorDodge", Range(0, 1)) = 0.393
    }
    SubShader
    {
        Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend SrcAlpha OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
             
            uniform sampler2D _MainTex;
            
            uniform sampler2D _StickerTex;
            float4 _MainTex_ST; 
            struct appdata_t
            { 
                float4 vertex : POSITION;
                
                float4 color : COLOR;
                
                float4 texcoord : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            }; 
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_COLOR : COLOR;
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 tmpvar_1;
                
                float4 outPut_2;
                
                float3 mergeColor_3;
                
                float3 formatColor_4;
                
                float4 overlayColor_5;
                
                float gray_6;
                
                float4 texColor_7;
                
                float4 tmpvar_8;
                
                tmpvar_8 = tex2D(_MainTex, in_f.texcoord);
                
                float tmpvar_9;
                
                tmpvar_9 = ((0.5 - ((max(max(tmpvar_8.x, tmpvar_8.y), tmpvar_8.z) + min(min(tmpvar_8.x, tmpvar_8.y), tmpvar_8.z)) / 2.0)) * 2.0);
                
                gray_6 = tmpvar_9;
                
                float tmpvar_10;
                
                tmpvar_10 = clamp(gray_6, -0.5, 0.5);
                
                texColor_7 = (tmpvar_8 + ((gray_6 - tmpvar_10) * 0.125));
                
                float4 tmpvar_11;
                
                tmpvar_11 = tex2D(_StickerTex, in_f.texcoord);
                
                overlayColor_5 = tmpvar_11;
                
                float tmpvar_12;
                
                tmpvar_12 = ((max(max(overlayColor_5.x, overlayColor_5.y), overlayColor_5.z) + min(min(overlayColor_5.x, overlayColor_5.y), overlayColor_5.z)) / 2.0);
                
                formatColor_4 = overlayColor_5.xyz;
                
                if ((tmpvar_12 > 0.75))
                {
                    
                    formatColor_4 = (overlayColor_5.xyz - ((tmpvar_12 - 0.75) * 0.5));
                }
                else
                {
                    
                    if ((tmpvar_12 > 0.5))
                    {
                        
                        formatColor_4 = overlayColor_5.xyz;
                    }
                    else
                    {
                        
                        formatColor_4 = (overlayColor_5.xyz + ((0.5 - tmpvar_12) * 0.5));
                    };
                };
                
                if ((formatColor_4.x < 0.5))
                {
                    
                    mergeColor_3.x = max(((texColor_7.x + (2.0 * formatColor_4.x)) - 1.0), 0.0);
                }
                else
                {
                    
                    mergeColor_3.x = min((texColor_7.x + (2.0 * (formatColor_4.x - 0.5))), 1.0);
                };
                
                if ((formatColor_4.y < 0.5))
                {
                    
                    mergeColor_3.y = max(((texColor_7.y + (2.0 * formatColor_4.y)) - 1.0), 0.0);
                }
                else
                {
                    
                    mergeColor_3.y = min((texColor_7.y + (2.0 * (formatColor_4.y - 0.5))), 1.0);
                };
                
                if ((formatColor_4.z < 0.5))
                {
                    
                    mergeColor_3.z = max(((texColor_7.z + (2.0 * formatColor_4.z)) - 1.0), 0.0);
                }
                else
                {
                    
                    mergeColor_3.z = min((texColor_7.z + (2.0 * (formatColor_4.z - 0.5))), 1.0);
                };
                
                if ((overlayColor_5.x < 0.52))
                {
                    
                    outPut_2.x = ((1.923077 * overlayColor_5.x) * mergeColor_3.x);
                }
                else
                {
                    
                    outPut_2.x = (1.0 - ((1.923077 * (1.0 - overlayColor_5.x)) * (1.0 - mergeColor_3.x)));
                };
                
                if ((overlayColor_5.y < 0.52))
                {
                    
                    outPut_2.y = ((1.923077 * overlayColor_5.y) * mergeColor_3.y);
                }
                else
                {
                    
                    outPut_2.y = (1.0 - ((1.923077 * (1.0 - overlayColor_5.y)) * (1.0 - mergeColor_3.y)));
                };
                
                if ((overlayColor_5.z < 0.52))
                {
                    
                    outPut_2.z = ((1.923077 * overlayColor_5.z) * mergeColor_3.z);
                }
                else
                {
                    
                    outPut_2.z = (1.0 - ((1.923077 * (1.0 - overlayColor_5.z)) * (1.0 - mergeColor_3.z)));
                };
                
                outPut_2.w = 1.0;
                
                tmpvar_1 = outPut_2;
                
                OUT_Data_Frag out_f;
                out_f.color = tmpvar_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
