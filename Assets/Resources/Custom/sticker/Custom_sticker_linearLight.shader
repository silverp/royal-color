Shader "Custom/sticker/linearLight"
{
    Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" { }
        _StickerTex ("Sticker Texture", 2D) = "white" { }
        _ColorDodge ("ColorDodge", Range(0, 1)) = 0.393
    }
    SubShader
    {
        Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            
            uniform sampler2D _MainTex;
            
            uniform sampler2D _StickerTex;
            float4 _MainTex_ST; 
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 color : COLOR;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_COLOR : COLOR;
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 xlat_varoutput_1;
                
                float4 bgColor_2;
                
                float gray_3;
                
                float4 texColor_4;
                
                float4 tmpvar_5;
                
                tmpvar_5 = tex2D(_MainTex, in_f.texcoord);
                
                float tmpvar_6;
                
                tmpvar_6 = ((0.5 - ((max(max(tmpvar_5.x, tmpvar_5.y), tmpvar_5.z) + min(min(tmpvar_5.x, tmpvar_5.y), tmpvar_5.z)) / 2.0)) * 2.0);
                
                gray_3 = tmpvar_6;
                
                float tmpvar_7;
                
                tmpvar_7 = clamp(gray_3, -0.5, 0.5);
                
                texColor_4 = (tmpvar_5 + ((gray_3 - tmpvar_7) * 0.125));
                
                float4 tmpvar_8;
                
                tmpvar_8 = tex2D(_StickerTex, in_f.texcoord);
                
                bgColor_2 = tmpvar_8;
                
                if ((bgColor_2.x < 0.5))
                {
                    
                    float tmpvar_9;
                    
                    tmpvar_9 = max(((texColor_4.x + (2.0 * bgColor_2.x)) - 1.0), 0.0);
                    
                    xlat_varoutput_1.x = tmpvar_9;
                }
                else
                {
                    
                    float tmpvar_10;
                    
                    tmpvar_10 = min((texColor_4.x + (2.0 * (bgColor_2.x - 0.5))), 1.0);
                    
                    xlat_varoutput_1.x = tmpvar_10;
                };
                
                if ((bgColor_2.y < 0.5))
                {
                    
                    float tmpvar_11;
                    
                    tmpvar_11 = max(((texColor_4.y + (2.0 * bgColor_2.y)) - 1.0), 0.0);
                    
                    xlat_varoutput_1.y = tmpvar_11;
                }
                else
                {
                    
                    float tmpvar_12;
                    
                    tmpvar_12 = min((texColor_4.y + (2.0 * (bgColor_2.y - 0.5))), 1.0);
                    
                    xlat_varoutput_1.y = tmpvar_12;
                };
                
                if ((bgColor_2.z < 0.5))
                {
                    
                    float tmpvar_13;
                    
                    tmpvar_13 = max(((texColor_4.z + (2.0 * bgColor_2.z)) - 1.0), 0.0);
                    
                    xlat_varoutput_1.z = tmpvar_13;
                }
                else
                {
                    
                    float tmpvar_14;
                    
                    tmpvar_14 = min((texColor_4.z + (2.0 * (bgColor_2.z - 0.5))), 1.0);
                    
                    xlat_varoutput_1.z = tmpvar_14;
                };
                
                xlat_varoutput_1.w = 1.0;
                OUT_Data_Frag out_f;
                out_f.color = xlat_varoutput_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
