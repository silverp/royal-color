Shader "Custom/sticker/muladd"
{
    Properties
    {
        _MainTex ("Sprite Texture", 2D) = "white" { }
        _StickerTex ("Sticker Texture", 2D) = "white" { }
        _ColorDodge ("ColorDodge", Range(0, 1)) = 0.393
    }
    SubShader
    {
        Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
        Pass // ind: 1, name:

        {
            Tags { "CanUseSpriteAtlas" = "true" "IGNOREPROJECTOR" = "true" "PreviewType" = "Plane" "QUEUE" = "Transparent" "RenderType" = "Transparent" }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend SrcAlpha OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _StickerTex;
            
            uniform sampler2D _MainTex;
            float4 _MainTex_ST; 
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 color : COLOR;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float4 xlv_COLOR : COLOR;
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
                UNITY_VERTEX_OUTPUT_STEREO
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            v2f vert(appdata_t v)
            {
                v2f OUT;
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
                OUT.worldPosition = v.vertex;
                OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

                OUT.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

                OUT.color = v.color;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1 = tex2D(_MainTex, in_f.texcoord);
                
                float4 r_2;
                
                r_2.xyz = (((tex2D(_StickerTex, in_f.texcoord) + 0.08) + tmpvar_1) - 1.0).xyz;
                
                r_2.w = tmpvar_1.w;
                
                OUT_Data_Frag out_f;
                out_f.color = r_2;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
