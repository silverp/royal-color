// Upgrade NOTE: commented out 'float3 _WorldSpaceCameraPos', a built-in variable

Shader "Custom/TestShader"
{
    Properties
    {
        [Gamma] _Metallic ("Metallic", Range(0, 1)) = 0.1
        _Smoothness ("Smoothness", Range(0, 1)) = 0.1
        _Diffuse ("Diffuse", Color) = (1, 1, 1, 1)
        _MainTex ("RampTex (RGB)", 2D) = "white" { }
        _RampTex ("Ramp Texture", 2D) = "white" { }
        _RampTexCoordX ("Ramp x Coordinate", float) = 0
        _RampTexCoordY ("Ramp Y Coordinate", float) = 0
    }
    SubShader
    {
        Tags { "LIGHTMODE" = "FORWARDBASE" "RenderType" = "Opaque" }
        LOD 200
        Pass // ind: 1, name:

        {
            Tags { "LIGHTMODE" = "FORWARDBASE" "RenderType" = "Opaque" }
            LOD 200
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            
            // uniform float3 _WorldSpaceCameraPos;
            

            
          
            

            
            uniform float4 _MainTex_ST;
            
            uniform sampler2D _RampTex;
            
            uniform float4 _RampTex_ST;
            
            uniform int _RampTexCoordX;
            
            uniform int _RampTexCoordY;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float3 normal : NORMAL;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float3 xlv_TEXCOORD1 : TEXCOORD1;
                
                float3 xlv_TEXCOORD2 : TEXCOORD2;
                
                float3 xlv_TEXCOORD3 : TEXCOORD3;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1.w = 1.0;
                
                tmpvar_1.xyz = in_v.vertex.xyz;
                
                float3x3 tmpvar_2;
                
                tmpvar_2[0] = unity_WorldToObject[0].xyz;
                
                tmpvar_2[1] = unity_WorldToObject[1].xyz;
                
                tmpvar_2[2] = unity_WorldToObject[2].xyz;
                
                OUT_Data_Vert OUT;
                OUT.xlv_TEXCOORD0 = (mul(in_v.texcoord.xy, _MainTex_ST.xy) + _MainTex_ST.zw);
                
                OUT.xlv_TEXCOORD1 = normalize(mul(in_v.normal, tmpvar_2));
                
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
                
                float4 tmpvar_3;
                
                tmpvar_3 = mul(unity_ObjectToWorld, in_v.vertex);
                
                OUT.xlv_TEXCOORD2 = normalize((_WorldSpaceCameraPos - tmpvar_3.xyz));
                
                OUT.xlv_TEXCOORD3 = tmpvar_3.xyz;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 tmpvar_1;
                
                float2 rampUV_2;
                
                float tmpvar_3;
                
                tmpvar_3 = (float(((_RampTexCoordY * 64) + 1)) / 512.0);
                
                float2 tmpvar_4;
                
                tmpvar_4.x = (float(((_RampTexCoordX * 4) + 1)) / 512.0);
                
                tmpvar_4.y = tmpvar_3;
                
                rampUV_2 = tmpvar_4;
                
                float tmpvar_5;
                
                tmpvar_5 = frac((in_f.xlv_TEXCOORD0.y * 64.0));
                
                if (((tmpvar_5 * 64.0) <= 2.0))
                {
                    
                    tmpvar_1 = float4(0.0, 0.0, 0.0, 0.0);
                }
                else
                {
                    
                    rampUV_2.y = (tmpvar_3 + (((floor((in_f.xlv_TEXCOORD0.y * 64.0)) / 64.0) * 64.0) / 512.0));
                    
                    rampUV_2 = (mul(rampUV_2, _RampTex_ST.xy) + _RampTex_ST.zw);
                    
                    tmpvar_1 = tex2D(_RampTex, rampUV_2);
                };
                
                OUT_Data_Frag out_f;
                out_f.color = tmpvar_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack "Diffuse"
}
