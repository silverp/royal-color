{
  "Library": "Library",
  "My work_top": "我的作品",
  "My": "我的",
  "My work": "我的作品",
  "My likes": "我喜欢的",
  "Loading...": "加载中...",
  "PAUSED": "暂停",
  "Press back again to quit!": "再按一次返回键退出游戏！",
  "Share": "分享",
  "Finish": "完成",
  "Save to album": "保存到相册",
  "Color model": "颜色模式",
  "Solid Color": "纯色",
  "Coloring palettes": "主题色",
  "Loading material...": "正在加载素材...",
  "Load resources completed ^o^": "加载素材完成^o^",
  "Loading resources for the first time,please wait...^o^": "第一次加载资源，请耐心等待^o^",
  "Loading information...": "正在加载资源信息...",
  "DownLoading  resources...": "正在加载资源信息...",
  "Work has been preserved!": "作品已经保存",
  "New": "最新",
  "Popular": "热门",
  "Inspiration": "广场",
  "RateTitle": "评价Ecolor",
  "RateMessage": "给五星好评以获得更新",
  "RateRefuse": "拒绝",
  "RatePostpone": "稍后提示",
  "RateOK": "评价",
  "Gradient": "渐变色",
  "TelePhone:": "客服电话:",
  "About us": "关于我们",
  "Set up": "设置",
  "Positive comment": "给个好评",
  "Feedback": "意见反馈",
  "Log out": "退出登录",
  "version:": "版本号:",
  "Save QRcode": "保存二维码",
  "Copy number": "复制群号",
  "Perfact!": "完美!",
  "Share your good work with friendsO(∩_∩)O~": "与朋友们分享吧~~",
  "His or she work": "TA的作品",
  "She or he likes": "TA喜欢的",
  "days,": "天,",
  "cancel": "取消",
  "repaint": "重绘",
  "delete": "删除",
  "wechat": "微信",
  "Login": "登录",
  "Create": "创作",
  "Goto Login": "去登录",
  "Log in to see what you like~": "登录才能看到你喜欢的作品呦~",
  "No work yet. Come and write!": "还没有作品哦，快来创作吧~",
  "have no likes!": "没有喜欢的作品哦",
  "QR code has been preserved!": "二维码已经保存！",
  "Log in...": "登录中...",
  "Login successful,jumping..": "登录成功,正在跳转..",
  "Login in to save work": "登录以保存创作",
  "The QRCode has been saved!": "二维码已经保存",
  "continue": "继续",
  "QQ number:582336415": "QQ官方反馈群:582336415",
  "Logon failure": "登录失败",
  "copy successful": "复制成功",
  "request error!": "请求错误！",
  "Not login": "未登录",
  "just now": "刚刚",
  "Add Texture": "添加纹理",
  "Background Texture": "背景纹理",
  "My pic,any likes?": "我画的O(∩_∩)O，求点赞~",
  "Refresh...": "刷新中...",
  "Daily free": "今日推荐",
  "unlock painting": "解锁画稿",
  "has no enough color beans": "当前涂豆不足",
  "Publish": "发布",
  "You haven't logged in yet": "Hello,你还没有登录哦",
  "My follow": "我的关注",
  "My fans": "我的粉丝",
  "His/Her follow": "TA的关注",
  "His/Her fans": "TA的粉丝",
  "VIP purchase success": "VIP开通成功!",
  "Artist": "艺术家",
  "Purchase fail!": "购买失败!",
  "You haven't follow anybody~": "还没有关注任何人哦~",
  "You have no fans~": "没有人关注哦~",
  "Purchase cancle!": "购买取消!",
  "Releasing...": "发布中...",
  "Release to square": "发布到广场",
  "Artist Application": "申请艺术家",
  "+Follow": "+关注",
  "Following": "已关注",
  "create": "创作",
  "Sign": "签到",
  "Signed": "已签到",
  "Click to login": "点击登录",
  "Share your work": "分享您的作品",
  "Delete work": "删除作品",
  "Are you sure to Delete?": "确认删除此幅作品吗?",
  "Solid": "纯色",
  "Theme": "主题色",
  "History": "历史",
  "Double-finger drag to move the picture,open and close to zoom the picture.": "双指拖拽移动图片 双指开合缩放图片",
  "Click to coloring,Hold down and drag to continuous painting.": "点击填充颜色，按住拖拽，连续涂色.",
  "join us, in the Ecolor creation of the ocean, to show your talent!": "热爱插画的你，加入我们吧，在Ecolor的创作海洋，尽情施展你的才华！",
  "Basic information": "基本资料",
  "name": "姓名",
  "Phone/QQ/Email": "电话/QQ/邮箱",
  "Personal profile": "个人简介",
  "Let's introduce yourself to yourself.": "向我们介绍介绍一下你自己吧~",
  "Apply for admission": "提交申请",
  "Your information has been successfully submitted, we will get in touch with you as soon as possible, Ecolor is looking forward to your joining!": "您的信息已成功提交，我们会尽快跟您取得联系，Ecolor期待您的加入!",
  "OK": "好的",
  "Application failure": "申请失败",
  "Sorry, your application is not successfully submitted, system will automatically return to the previous level page, please repeat it.": "很抱歉，您的申请未能成功提交，系统i将自动返回上一级页面，请您重新提交。",
  "Your audit is under application. After the review is completed, our staff will get in touch with you as soon as possible. Please wait patiently.": "您的审核正在申请中，审核完成后，我们的工作人员会尽快与您取得联系，请耐心等待~",
  "Under review": "审核中",
  "Submitted successfully": "提交成功",
  "Subscribe details": "订阅详情",
  "Get more than 1000 drawing permissions, update each week, unlock the paid background texture!": "获取超过1000张画稿的绘制权限，每周获得更新，解锁付费背景纹理功能!",
  "Facebook login": "Facebook登录",
  "release successful, under review": "已发布，审核中...",
  "fans": "粉丝",
  "expire": "到期",
  "Not open vip": "未开通",
  "Subscribed": "已订阅",
  "Two-finger opening and closing, zoom drawing": "双指开合，缩放画稿",
  "Single finger drag, move the position of the draft": "单指拖动，移动画稿位置",
  "Single-click to fill.": "单指点击，进行填色",
  "Long press and drag to finish continuous coloring": "长按后拖拽，完成连续涂色",
  "beans": "涂豆",
  "beans:": "涂豆:",
  "open vip": "开通VIP",
  "Invite friends": "邀请好友",
  "confirm": "确定",
  "has released": "已发布",
  "There is no reference map.": "还没有参考图哦~",
  "Purchase VIP": "购买VIP",
  "Click to goto mywork page": "进入我的作品页面",
  "Share success": "分享成功",
  "Watch Video to unlock": "观看视频解锁",
  "The ad is not ready yet": "广告还没有准备好"
  //"": "星级好评",
  "Daily art on Royal Color": "Daily <b>Art</b> on <b>Royal Color</b>",
  "Color it now": "Color it now",
  "Find it now": "Find it now",
  "Remove all ADS in this game": "Remove all <b>ADS</b> in this game" 
}