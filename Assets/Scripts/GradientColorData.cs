using System.Collections.Generic;

public class GradientColorData
{
	public string gradientImageUrl;

	public int version;

	public List<GradientColor> gradientColors;

	public bool fetchedGradientColors = false;

	public bool isPrefab;

	public GradientColorData(string url, int version, List<GradientColor> gradientColors, bool isPrefab)
	{
		gradientImageUrl = url;
		this.version = version;
		this.gradientColors = gradientColors;
		this.isPrefab = isPrefab;
	}
}
