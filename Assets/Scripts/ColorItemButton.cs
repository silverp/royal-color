using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorItemButton : Button
{
    public int colorIndex;
    protected override void Awake()
    {
        base.Awake();
        this.transition = Transition.None;
    }
}
