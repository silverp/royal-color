using UnityEngine;

public class CateBtnController : MonoBehaviour
{
	public int cid;

	private void OnEnable()
	{
		IndexCardInitController.OnShowRedPointAction += showRedPoint;
	}

	private void OnDisable()
	{
		IndexCardInitController.OnShowRedPointAction -= showRedPoint;
	}

	private void showRedPoint(int cid)
	{
		UnityEngine.Debug.Log("CateBtnController show red point...");
		if (cid == this.cid)
		{
			UnityEngine.Debug.Log("show red point");
			base.transform.GetChild(1).GetChild(0).gameObject.SetActive(value: true);
		}
	}
}
