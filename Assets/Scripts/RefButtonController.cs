
using UnityEngine;

public class RefButtonController : MonoBehaviour
{
	public void ShowRef()
	{
		UnityEngine.Debug.Log("ShowRef....");
		base.transform.gameObject.SetActive(value: true);
	}

	public void HideRef()
	{
		base.transform.gameObject.SetActive(value: false);
		TotalGA.Event("workshop_page_event", "HideRef");
	}

	public void HideNonRef()
	{
		base.transform.parent.Find("NonRef").gameObject.SetActive(value: false);
	}
}
