using UI.ThreeDimensional;
using UnityEngine;
using UnityEngine.UI;

public class UITempleController : MonoBehaviour
{
	public UITemplate template;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void OnRenderCallback(Texture2D texture)
	{
		UnityEngine.Debug.Log("OnRenderCallback:");
		if (texture != null)
		{
			UnityEngine.Debug.Log("OnRenderCallback:" + texture.texelSize);
			GetComponent<RawImage>().texture = texture;
		}
	}

	public void render()
	{
		UnityEngine.Debug.Log("render!");
		UITemplate component = GetComponent<UITemplate>();
		component.OnRenderCallback += OnRenderCallback;
		component.renderPreferb();
	}
}
