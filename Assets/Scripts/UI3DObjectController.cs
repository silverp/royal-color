using UnityEngine;

public class UI3DObjectController : MonoBehaviour
{
	[Header("Target")]
	[SerializeField]
	private Transform _ObjectPrefab;

	private int count;

	private bool isShow;

	public Transform ObjectPrefab
	{
		get
		{
			return _ObjectPrefab;
		}
		set
		{
			_ObjectPrefab = value;
		}
	}

	public void showHide()
	{
		int childCount = base.transform.childCount;
		for (int i = 0; i < childCount; i++)
		{
			UnityEngine.Debug.Log("showHide:" + base.transform.GetChild(i).name.Contains("ColorGradientItem"));
			if (base.transform.GetChild(i).name.Contains("ColorGradientItem"))
			{
				base.transform.GetChild(i).gameObject.SetActive(isShow);
			}
		}
		isShow = !isShow;
	}

	public void addItem()
	{
		Transform transform = UnityEngine.Object.Instantiate(ObjectPrefab);
		if (transform != null)
		{
			transform.localPosition = new Vector3(count % 15 * 150 + 80, count / 15 * 150 + 80, 0f);
		}
		transform.SetParent(base.transform);
		transform = UnityEngine.Object.Instantiate(ObjectPrefab);
		if (transform != null)
		{
			transform.localPosition = new Vector3((count % 15 + 1) * 150 + 80, Mathf.Ceil(count / 15) * 150f + 80f, 0f);
		}
		transform.SetParent(base.transform);
		count += 2;
	}

	private void Start()
	{
	}

	private void Update()
	{
	}
}
