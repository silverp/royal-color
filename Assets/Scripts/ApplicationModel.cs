using Mgl;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ApplicationModel
{
	public static string defaultColor;

	private static readonly string COLOR_THEME;

	private static readonly string COLOR_THEME_PURE;

	private static readonly string COLOR_THEME_GRADIENT;

	public static readonly string LANGUAGE;

	private static readonly string VERSION;

	public static readonly string HOST;

	public static readonly string COLOR_LIST;

	public static readonly string COLOR_LIST_PURE;

	public static readonly string CLICK_IMAGE_EVENT;

	public static readonly string UPLOAD_IMAGE_EVENT;

	public static readonly string CATE_RESOURCE_LIST;

	public static List<CateResource> CateResourceList;

	public static readonly string EDITED_RESOURCEIMG_LIST;

	public static List<EditedImg> EditedResourceImgList;

	public static readonly string EDITED_PIXELIMG_LIST;

	public static List<PixleEditedImg> PixleEditedImgList;

	public static Texture2D GradiantImage;

	public static readonly string EDITED_COLORED_IMGLIST;

	public static List<ColoredImage> ColoredImgList;

	public static BaseEditedImg currentEditedImg;

	public static ColoredImage currentColoredImg;

	public static List<ColorTheme> ColorThemes;

	public static List<ColorTheme> ColorThemesPure;

	public static GradientColorData ColorThemesGradient;

	public static CateResource currentCategroy;

	public static ArtistAlbumModel currentArtistAlbumModel;

	public static readonly string ARTIST_ALBUM_MODEL_LIST;

	public static List<ArtistAlbumModel> artistAlbumModelList;

	//public static string workShopFrom;

	public static string ReturnPageName;
	public static string lastVisibleScreen;

	public static string ColorThemeName;

	public static bool IsGradient;

	public static List<ColorComplex> recentColorlist;

	public static ColorComplex currentColorComplex;

	public static int currentRecentColorNum;

	public static int version;

	private static readonly int old_version1;

	private static readonly int old_version2;

	private static readonly int old_version3;

	private static readonly int old_version4;

	public static string device_id;

	public static string device_id_name;

	public static string CurrentCanvasName;

	public static readonly string LANGUAGE_VERSION;

	public static int rateTimes;

	public static readonly string RATETIMES;

	public static int MaxRateTimes;

	public static Color CurrentCol;

	public static UserInfo userInfo;

	public static UserInfo otherUserInfo;

	public static readonly string sessionIdName;

	public static string sessionId;

	public static List<CustomProduct> productList;

	public static List<CateTimeStamp> CateTimeStampList;

	public static string CateTimeStampListName;

	public static bool IsPayChannel;

	public static List<ColoredImage> publishImglist;

	public static List<PixelArt> pixelArtList;

	public static string currentChannel;

	public static DeviceVipInfo deviceVipInfo;

	public static bool isAdChannel;

	public static List<string> unlockList;

	public bool pixelNote;

	private static bool _initializedColors = false;

	public static void Initialize()
	{
		if (!_initializedColors)
		{
			_initializedColors = true;
			foreach(var c in ColorThemes)
				c.Initialize();
			foreach(var c in ColorThemesPure)
				c.Initialize();
		}
	}

	static ApplicationModel()
	{
		COLOR_THEME = "colortheme";
		COLOR_THEME_PURE = "colortheme_pure";
		COLOR_THEME_GRADIENT = "colortheme_gradient";
		LANGUAGE = LanguageHelper.GetLocaleCodeSystemLanguage();
		VERSION = "version";
		HOST = APIFactory.HOST;
		COLOR_LIST = HOST + "/colorThemeList?category=themeColor&language=" + LANGUAGE;
		COLOR_LIST_PURE = HOST + "/colorThemeList?category=solidColor&language=" + LANGUAGE;
		CLICK_IMAGE_EVENT = HOST + "/saveColorClick";
		UPLOAD_IMAGE_EVENT = HOST + "/saveColorAfter";
		CATE_RESOURCE_LIST = "CateResourceList";
		CateResourceList = new List<CateResource>();
		EDITED_RESOURCEIMG_LIST = "EditedResourceImgList";
		EditedResourceImgList = new List<EditedImg>();
		EDITED_PIXELIMG_LIST = "PixleEditedImgList";
		PixleEditedImgList = new List<PixleEditedImg>();
		GradiantImage = Resources.Load<Texture2D>("Images/Image/gradiant");
		EDITED_COLORED_IMGLIST = "EditedColoredImglist";
		ARTIST_ALBUM_MODEL_LIST = "artistAlbumModelList";
		//workShopFrom = "ImglistCanvas";
		ReturnPageName = "ImglistCanvas";
		ColorThemeName = "gradient";
		IsGradient = false;
		version = 2001;
		old_version1 = 1090;
		old_version2 = 1960;
		old_version3 = 2001;
		old_version4 = 2201;
		device_id = string.Empty;
		device_id_name = "DEVICE_ID";
		CurrentCanvasName = "ImglistCanvas";
		LANGUAGE_VERSION = "language_version";
		rateTimes = 0;
		RATETIMES = "rateTimes";
		MaxRateTimes = 1;
		sessionIdName = "SESSIONID_NAME";
		sessionId = string.Empty;
		CateTimeStampListName = "CateTimeStampList";
		IsPayChannel = true;
		currentChannel = string.Empty;
		isAdChannel = true;
		InitData();
	}

	public static void SetColorThemesFirstTime()
    {
        TextAsset textAsset = Resources.Load<TextAsset>("json/themeColor_en");
		ColorThemes = JsonConvert.DeserializeObject<List<ColorTheme>>(textAsset.text);
	}

	public static void SetColorThemesPureFirstTime()
	{
		TextAsset textAsset = Resources.Load<TextAsset>("json/solidColor_en");
		ColorThemesPure = JsonConvert.DeserializeObject<List<ColorTheme>>(textAsset.text);
	}

	private static void SetColorGradientFirstTime()
    {
        TextAsset textAsset = Resources.Load<TextAsset>("json/gradientColor_en");
		ColorThemesGradient = JsonConvert.DeserializeObject<GradientColorData>(textAsset.text);
	}

	public static void InitData()
	{
		int num = 0;
		I18n instance = I18n.Instance;
		if (PlayerPrefs.HasKey(LANGUAGE_VERSION) && PlayerPrefs.GetString(LANGUAGE_VERSION) != LANGUAGE)
		{
			PlayerPrefs.DeleteAll();
		}
		if (PlayerPrefs.HasKey(sessionIdName))
		{
			sessionId = PlayerPrefs.GetString(sessionIdName);
		}
		else
		{
			sessionId = string.Empty;
		}
		if (PlayerPrefs.HasKey(ARTIST_ALBUM_MODEL_LIST))
		{
			artistAlbumModelList = JsonConvert.DeserializeObject<List<ArtistAlbumModel>>(PlayerPrefs.GetString(ARTIST_ALBUM_MODEL_LIST));
		}
		if (PlayerPrefs.HasKey(VERSION))
		{
			num = PlayerPrefs.GetInt(VERSION);
		}
		if (PlayerPrefs.HasKey(COLOR_THEME))
		{
			string @string = PlayerPrefs.GetString(COLOR_THEME);
			ColorThemes = JsonConvert.DeserializeObject<List<ColorTheme>>(@string);
		}
		else
		{
			SetColorThemesFirstTime();
		}
		if (PlayerPrefs.HasKey(COLOR_THEME_PURE))
		{
			string string2 = PlayerPrefs.GetString(COLOR_THEME_PURE);
			ColorThemesPure = JsonConvert.DeserializeObject<List<ColorTheme>>(string2);
		}
		else
		{
			SetColorThemesPureFirstTime();
		}
		SetColorGradientFirstTime();
		if (PlayerPrefs.HasKey(device_id_name))
		{
			device_id = PlayerPrefs.GetString(device_id_name);
		}
		else
		{
			device_id = GetDeviceId();
		}
		if (PlayerPrefs.HasKey(RATETIMES))
		{
			rateTimes = PlayerPrefs.GetInt(RATETIMES);
		}
		else
		{
			rateTimes = 0;
		}
		if (PlayerPrefs.HasKey(EDITED_RESOURCEIMG_LIST) && num >= old_version3)
		{
			EditedResourceImgList = JsonConvert.DeserializeObject<List<EditedImg>>(PlayerPrefs.GetString(EDITED_RESOURCEIMG_LIST));
		}
		else if (PlayerPrefs.HasKey(EDITED_RESOURCEIMG_LIST) && num == old_version2)
		{
			AssembleEditedImgListForVersion2(PlayerPrefs.GetString(EDITED_RESOURCEIMG_LIST));
		}
		if (PlayerPrefs.HasKey(EDITED_RESOURCEIMG_LIST) && num < old_version4 && EditedResourceImgList != null)
		{
			EmptyEditedImgListOriginDetail();
		}
		if (PlayerPrefs.HasKey(EDITED_PIXELIMG_LIST))
		{
			PixleEditedImgList = JsonConvert.DeserializeObject<List<PixleEditedImg>>(PlayerPrefs.GetString(EDITED_PIXELIMG_LIST));
		}
		defaultColor = ColorThemesPure[0].Colors[0];
		if (num < old_version2)
		{
			CreateEditedResourceImgList();
		}
		if (PlayerPrefs.HasKey(CateTimeStampListName))
		{
			CateTimeStampList = JsonConvert.DeserializeObject<List<CateTimeStamp>>(PlayerPrefs.GetString(CateTimeStampListName));
		}
		if (PlayerPrefs.HasKey(CATE_RESOURCE_LIST))
		{
			CateResourceList = JsonConvert.DeserializeObject<List<CateResource>>(PlayerPrefs.GetString(CATE_RESOURCE_LIST));
		}
	}

	private static void EmptyEditedImgListOriginDetail()
	{
		foreach (EditedImg editedResourceImg in EditedResourceImgList)
		{
			if (editedResourceImg != null)
			{
				editedResourceImg.ouline = null;
				editedResourceImg.index = null;
			}
		}
	}

	private static void AssembleEditedImgListForVersion2(string json)
	{
		List<ResourceImg> list = JsonConvert.DeserializeObject<List<ResourceImg>>(json);
		foreach (ResourceImg item in list)
		{
			if (item.hasEdited)
			{
				EditedResourceImgList.Add(new EditedImg(item.imageId, item.url, item.indexImageUrl, hasEdited: true, isRemote: false));
			}
		}
	}

	private static void CreateEditedResourceImgList()
	{
		EditedResourceImgList = new List<EditedImg>();
		foreach (CateResource cateResource in CateResourceList)
		{
			foreach (ResourceImg img in cateResource.imgList)
			{
				if (img.hasEdited)
				{
					EditedResourceImgList.Add(new EditedImg(img.imageId, img.url, img.indexImageUrl, hasEdited: true, isRemote: false));
				}
			}
		}
	}

	private static string GetDeviceId()
	{
		string deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
		if (deviceUniqueIdentifier != null && deviceUniqueIdentifier != string.Empty)
		{
			return deviceUniqueIdentifier;
		}
		return PublicToolController.RandomString(32);
	}

	public static string[] getCategoryNamesFromResources()
	{
		int num = 0;
		foreach (CateResource cateResource in CateResourceList)
		{
			num++;
		}
		string[] array = new string[num];
		for (int i = 0; i < num; i++)
		{
			array[i] = CateResourceList[i].name;
		}
		return array;
	}

	public static Texture2D GetToBeEditedImg(ResourceImg img)
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		string path = "Category/imageData/" + img.path;
		File.WriteAllBytes(Application.persistentDataPath + "/" + img.path + ".txt", Resources.Load<Texture2D>(path).EncodeToPNG());
		texture2D.LoadImage(PublicToolController.FileReaderBytes(img.path), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	public static Texture2D GetCurrentText2d()
	{
		return GetTex2dFromImg(currentEditedImg);
	}

	public static Texture2D GetCurrentStickerTexture2d()
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(currentEditedImg.imageId + "_ok"), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	public static EditedImg GetEditedImgByImageId(string imageId)
	{
		foreach (EditedImg editedResourceImg in EditedResourceImgList)
		{
			if (editedResourceImg.imageId == imageId)
			{
				return editedResourceImg;
			}
		}
		return null;
	}

	public static Texture2D GEtTex2dThumbnailFromImg(ResourceImg img)
	{
		if (img == null)
		{
			return null;
		}
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.RGBA32, mipChain: false);
		if (img.isPrefab)
		{
			string path = "Category/imageData/" + img.thumbnailPath;
			texture2D = Resources.Load<Texture2D>(path);
		}
		else
		{
			texture2D.LoadImage(PublicToolController.FileReaderBytes(img.thumbnailPath), markNonReadable: false);
			texture2D.Apply();
		}
		return texture2D;
	}

	public static Texture2D GetTex2dFromImg(BaseEditedImg img)
	{
		if (img == null)
		{
			return null;
		}
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(img.imageId + "_ok"), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	public static Texture2D GetTexture2dFromName(string name)
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(name), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	public static void RestoreAlbumList()
	{
		PlayerPrefs.SetString(ARTIST_ALBUM_MODEL_LIST, JsonConvert.SerializeObject(artistAlbumModelList));
		PlayerPrefs.Save();
	}

	public static void RestoreEditedImgList()
	{
		List<EditedImg> list = new List<EditedImg>();
		foreach (EditedImg editedResourceImg in EditedResourceImgList)
		{
			if (!editedResourceImg.isRemote && editedResourceImg.HasEdited())
			{
				list.Add(editedResourceImg);
			}
		}
		string value = JsonConvert.SerializeObject(list);
		PlayerPrefs.SetString(EDITED_RESOURCEIMG_LIST, value);
		PlayerPrefs.Save();
	}

	public static void RestoreEditedPixelImglist()
	{
		List<PixleEditedImg> list = new List<PixleEditedImg>();
		foreach (PixleEditedImg pixleEditedImg in PixleEditedImgList)
		{
			if (!pixleEditedImg.isRemote && pixleEditedImg.HasEdited())
			{
				list.Add(pixleEditedImg);
			}
		}
		string value = JsonConvert.SerializeObject(list);
		PlayerPrefs.SetString(EDITED_PIXELIMG_LIST, value);
		PlayerPrefs.Save();
	}

	public static void RestoreRateTimes()
	{
		PlayerPrefs.SetInt(RATETIMES, rateTimes);
		PlayerPrefs.Save();
	}

	public static void RestoreCateTimeStampList()
	{
		PlayerPrefs.SetString(CateTimeStampListName, JsonConvert.SerializeObject(CateTimeStampList));
		PlayerPrefs.Save();
	}

	public static void RestoreCateList()
	{
		PlayerPrefs.SetString(CATE_RESOURCE_LIST, JsonConvert.SerializeObject(CateResourceList));
		PlayerPrefs.Save();
	}

	public static void RestorePerfab()
	{
		string value = JsonConvert.SerializeObject(ColorThemes);
		PlayerPrefs.SetString(COLOR_THEME, value);
		PlayerPrefs.SetString(COLOR_THEME_PURE, JsonConvert.SerializeObject(ColorThemesPure));
		PlayerPrefs.SetString(COLOR_THEME_GRADIENT, JsonConvert.SerializeObject(ColorThemesGradient));
		PlayerPrefs.SetInt(VERSION, version);
		PlayerPrefs.SetInt(RATETIMES, rateTimes);
		PlayerPrefs.SetString(LANGUAGE_VERSION, LANGUAGE);
		PlayerPrefs.SetString(device_id_name, device_id);
		PlayerPrefs.SetString(sessionIdName, sessionId);
		PlayerPrefs.SetString(CATE_RESOURCE_LIST, JsonConvert.SerializeObject(CateResourceList));
		PlayerPrefs.Save();
	}

	public static void SaveSessionIdTolocal()
	{
		PlayerPrefs.SetString(sessionIdName, sessionId);
		PlayerPrefs.Save();
	}

	public static string GetCurrentCateName()
	{
		return currentCategroy.name;
	}

	public static int GetTotalDownloadImgCntOfCurrentCate()
	{
		int num = 0;
		foreach (ResourceImg img in currentCategroy.imgList)
		{
			if (img.hasDownloaded)
			{
				num++;
			}
		}
		return num;
	}

	public static List<ResourceImg> GetHasDownImgList(CateResource cate)
	{
		List<ResourceImg> list = new List<ResourceImg>();
		foreach (ResourceImg img in currentCategroy.imgList)
		{
			if (img.hasDownloaded)
			{
				list.Add(img);
			}
		}
		return list;
	}

	public static int GetDownloadCountInLoading()
	{
		int num = 0;
		foreach (CateResource cateResource in CateResourceList)
		{
			ResourceImg headImg = cateResource.headImg;
			if (headImg != null && !headImg.hasDownloaded)
			{
				num++;
			}
			if (cateResource.imgList != null)
			{
				foreach (ResourceImg img in cateResource.imgList)
				{
					if (!img.thumbnailHasDownload)
					{
						num++;
					}
				}
			}
		}
		return num;
	}
}
