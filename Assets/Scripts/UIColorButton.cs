using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class UIColorButton : MonoBehaviour
{
    private Animator anim;
    private Image outer;
    private Image circle;
    private Image color;
    private Image select;
    private RawImage gradient;
    
    [HideInInspector]
    public int pageIndex;
    [HideInInspector]
    public int index;
    [HideInInspector]
    public Vector2 coordColor;
    [HideInInspector]
    public Color attachedColor;
  
    public void Init(Texture2D gradientTexture)
    {
        anim = GetComponent<Animator>();
        outer = GetComponent<Image>();
        circle = transform.Find("circle_line").GetComponent<Image>();
        color = circle.transform.Find("color").GetComponent<Image>();
        select = circle.transform.Find("select").GetComponent<Image>();
        gradient = color.transform.Find("gradient").GetComponent<RawImage>();
        gradient.material = new Material(Shader.Find("Unlit/RadialGradient"));
        gradient.material.SetTexture("_RampTex", gradientTexture);
        var button = GetComponent<Button>();
        if (!button)
            transform.AddComponent<Button>();
    }

    public void RemoveSelection()
    {
        if (select.gameObject.activeSelf)
            select.gameObject.SetActive(false);
    }

    public void Select()
    {
        select.gameObject.SetActive(true);
        PlayAnim();
    }
    public void PlayAnim()
    { 
        anim?.Play("color-button", -1, 0f);
    }
    
    public void EnableGradientMode(bool enabled)
    {
        gradient.gameObject.SetActive(enabled);
        // color.gameObject.SetActive(!enabled);
    }
    public void SetColor(Color c)
    {
        this.color.color = c;
        SetCircleColor(c / 1.2f);
        SetSelectedColor(c / 2f);
    }
    public void SetSelectedColor(Color c)
    {
        c.a = 1;
        this.select.color = c;
    }
    public void SetCircleColor(Color c)
    {
        c.a = 1;
        this.circle.color = c;
    }

    public void SetGradientCoordColor(Vector2 point)
    {
        coordColor = point;
        gradient.material.SetInt("_RampTexCoordX", (int)Mathf.Ceil(point.x));
        gradient.material.SetInt("_RampTexCoordY", (int)Mathf.Ceil(point.y));
    }

    public void SetSelected(bool selected)
    {
        this.select.gameObject.SetActive(selected);
    }
}
