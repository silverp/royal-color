using System.Collections.Generic;

public class ColorThemeJson
{
	public string id;

	public string name;

	public List<string> colorsRaw;

	public List<string> colors;

	public long updatetime;

	public string bgcolor;
}
