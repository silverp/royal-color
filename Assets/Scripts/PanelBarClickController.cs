using System.Collections;
using UnityEngine;

public class PanelBarClickController : MonoBehaviour
{
	private float ButtonWidth = 400f;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void ClickBar(int num)
	{
		Vector2 anchoredPosition = base.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
		float x = anchoredPosition.x;
		Vector2 anchoredPosition2 = base.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
		float y = anchoredPosition2.y;
		if ((float)num * ButtonWidth + x > 1080f - ButtonWidth - 150f)
		{
			StartCoroutine(MoveToPosition(base.transform.gameObject.GetComponent<RectTransform>(), new Vector2(x - ButtonWidth, y), 0.3f));
		}
		else if ((float)num * ButtonWidth + x < 200f && x != 0f)
		{
			UnityEngine.Debug.Log("need to move toward right!");
		}
		else
		{
			UnityEngine.Debug.Log("change color normaly!");
		}
	}

	private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
	{
		Vector2 currentPos = rectTransform.anchoredPosition;
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / TimeToMove;
			rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
			yield return null;
		}
	}
}
