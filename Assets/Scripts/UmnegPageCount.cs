
using UnityEngine;

public class UmnegPageCount : MonoBehaviour
{
	private void OnEnable()
	{
		//Analytics.PageBegin(base.transform.name);
		TotalGA.Event("Enter_page_" + base.transform.name);
	}

	private void OnDisable()
	{
        TotalGA.Event("End_page_" + base.transform.name);
	}
}
