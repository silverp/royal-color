using UnityEngine;

public class AsyncImageDownload : MonoBehaviour
{
	public Texture placeholder;

	public static AsyncImageDownload Instance;

	public static AsyncImageDownload CreateSingleton()
	{
		GameObject gameObject = new GameObject();
		AsyncImageDownload asyncImageDownload = Instance = gameObject.GetComponent<AsyncImageDownload>();
		asyncImageDownload.placeholder = Resources.Load<Texture>("Category/imageData/jiaozhunwangge");
		return asyncImageDownload;
	}
}
