using UnityEngine;

public class RefreshDeviceInfoEvent : MonoBehaviour
{
	private void OnEnable()
	{
		RefreshDeviceInfo();
	}

	private void RefreshDeviceInfo()
	{
		UnityEngine.Debug.Log(string.Empty);
		if (ApplicationModel.device_id != null || ApplicationModel.device_id != string.Empty)
		{
			UnityEngine.Debug.Log("RefreshDeviceInfo device id:" + ApplicationModel.device_id);
			PublicControllerApi.getDeviceInfo(ApplicationModel.device_id, RequestDeviceInfoCallback);
		}
	}

	private void RequestDeviceInfoCallback(Errcode err, DeviceVipInfo deviceVipInfo)
	{
		if (err.errorCode == Errcode.OK)
		{
			if (ApplicationModel.deviceVipInfo == null)
			{
				ApplicationModel.deviceVipInfo = new DeviceVipInfo();
			}
			ApplicationModel.deviceVipInfo.vipExpired = deviceVipInfo.vipExpired;
			ApplicationModel.deviceVipInfo.vipLevel = deviceVipInfo.vipLevel;
			ApplicationModel.deviceVipInfo.deviceId = deviceVipInfo.deviceId;
		}
	}
}
