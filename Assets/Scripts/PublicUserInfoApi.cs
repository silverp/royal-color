using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class PublicUserInfoApi
{
	private Action callback;

	private static List<EditedImg> tempEditedImgList;

	private static string privateKey = "_wawaGame";

	public static bool IsHasLogin()
	{
		if (ApplicationModel.userInfo != null)
		{
			return true;
		}
		return false;
	}

	public static bool IsVip()
	{
		if (ApplicationModel.userInfo != null)
		{
			if (ApplicationModel.userInfo.VipLevel != 0)
			{
				return true;
			}
		}
		else if (ApplicationModel.deviceVipInfo != null && ApplicationModel.deviceVipInfo.vipLevel != 0)
		{
			return true;
		}
		return false;
	}

	public static void UploadImgIdToUnLockList(string imageId)
	{
		if (IsHasLogin())
		{
			if (ApplicationModel.userInfo.unlockList == null)
			{
				ApplicationModel.userInfo.unlockList = new List<string>();
			}
			ApplicationModel.userInfo.unlockList.Add(imageId);
			URLRequest uRLRequest = APIFactory.create(APIFactory.UPLOAD_UNLOCK_IMAGEID);
			uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
			uRLRequest.addParam("imageId", imageId);
			DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
			{
				if (detail.status != 0)
				{
				}
			}, delegate
			{
			});
		}
		else
		{
			if (ApplicationModel.unlockList == null)
			{
				ApplicationModel.unlockList = new List<string>();
			}
			ApplicationModel.unlockList.Add(imageId);
		}
	}

	public static bool IsExistInUnlocklist(string imageId)
	{
		if (IsHasLogin())
		{
			foreach (string unlock in ApplicationModel.userInfo.unlockList)
			{
				if (unlock == imageId)
				{
					return true;
				}
			}
		}
		else if (ApplicationModel.unlockList != null && ApplicationModel.unlockList.Count != 0)
		{
			foreach (string unlock2 in ApplicationModel.unlockList)
			{
				if (unlock2 == imageId)
				{
					return true;
				}
			}
		}
		return false;
	}

	public static bool DeviceIsHasVip()
	{
		if (ApplicationModel.deviceVipInfo != null && ApplicationModel.deviceVipInfo.vipLevel != 0)
		{
			return true;
		}
		return false;
	}

	public static string MD5Hash(string input)
	{
		StringBuilder stringBuilder = new StringBuilder();
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		byte[] array = mD5CryptoServiceProvider.ComputeHash(new UTF8Encoding().GetBytes(input));
		for (int i = 0; i < array.Length; i++)
		{
			stringBuilder.Append(array[i].ToString("x2"));
		}
		return stringBuilder.ToString();
	}

	public static string GetUSerMd5HashValue(string tamp, string id)
	{
		string input = privateKey + tamp + id;
		return MD5Hash(input);
	}

	public static void AssignOtherUserInfo(BriefUserInfo user)
	{
		UserInfo userInfo = ApplicationModel.otherUserInfo = new UserInfo(user.uid, user.opendId, user.nickname, user.sex, user.headimgUrl, user.create);
	}

	public static void AssignOtherUserInfo(FollowUserInfo user)
	{
		UserInfo userInfo = ApplicationModel.otherUserInfo = new UserInfo(user.uid, string.Empty, user.nickname, user.sex, user.headImgUrl, user.create);
	}

	public static string GetSessionId()
	{
		string result = string.Empty;
		if (ApplicationModel.userInfo != null)
		{
			result = ApplicationModel.userInfo.SessionId;
		}
		return result;
	}

	public static string GetUserUid()
	{
		string result = string.Empty;
		if (ApplicationModel.userInfo != null)
		{
			result = ApplicationModel.userInfo.Uid;
		}
		return result;
	}

	public static void StartMergeData(Action callback = null)
	{
		if (IsHasLogin())
		{
			UnityEngine.Debug.Log("merge data has login");
			URLRequest uRLRequest = APIFactory.create(APIFactory.GET_WORK_LIST);
			uRLRequest.addParam("uid", GetUserUid());
			uRLRequest.addParam("sessionId", GetSessionId());
			uRLRequest.addParam("isMine", true.ToString());
			DataBroker.getJson(uRLRequest, delegate(MySelfWorkInfoJson detail)
			{
				if (detail.status == 0)
				{
					MergeEditedImglist(detail.data);
				}
				if (callback != null)
				{
					callback();
				}
			}, delegate
			{
				if (callback != null)
				{
					callback();
				}
			});
		}
		else
		{
			UnityEngine.Debug.Log("merge data not has login");
		}
	}

	private static void MergeEditedImglist(List<MyselfWork> RemoteEditedImglist)
	{
		UnityEngine.Debug.Log("ApplicationModel.PixleEditedImgList is:" + JsonConvert.SerializeObject(ApplicationModel.PixleEditedImgList));
		UnityEngine.Debug.Log("RemoteEditedImglist is:" + JsonConvert.SerializeObject(RemoteEditedImglist));
		if (RemoteEditedImglist != null && RemoteEditedImglist.Count > 0)
		{
			foreach (MyselfWork item in RemoteEditedImglist)
			{
				if (item.img.type == "normal")
				{
					EditedImg editedImgByImageId = GetEditedImgByImageId(item.img.imageId);
					MergeEditedImg(editedImgByImageId, item);
				}
				else if (item.img.type == "pixel")
				{
					PixleEditedImg pixelEditedImgByImageId = GetPixelEditedImgByImageId(item.img.imageId);
					MergeEditedImg(pixelEditedImgByImageId, item);
				}
			}
		}
	}

	private static void MergeEditedImg(PixleEditedImg localImg, MyselfWork remoteImg)
	{
		UnityEngine.Debug.Log("MergeEditedImg pixel localImg is:" + JsonConvert.SerializeObject(localImg));
		UnityEngine.Debug.Log("MergeEditedImg pixel remoteImg is:" + JsonConvert.SerializeObject(remoteImg));
		if (localImg != null)
		{
			localImg.workId = remoteImg.img.workId;
			localImg.likeCnt = remoteImg.img.likeCnt;
			localImg.isLike = remoteImg.isLike;
			localImg.create = remoteImg.create;
		}
		else
		{
			PixleEditedImg pixleEditedImg = new PixleEditedImg(remoteImg.img.imageId, remoteImg.img.workId, remoteImg.img.finalImg, remoteImg.img.thumb, remoteImg.img.likeCnt, remoteImg.isLike, remoteImg.create, hasEdited: true);
			pixleEditedImg.isRemote = true;
			ApplicationModel.PixleEditedImgList.Add(pixleEditedImg);
			UnityEngine.Debug.Log("ApplicationModel.PixleEditedImgList is:" + JsonConvert.SerializeObject(ApplicationModel.PixleEditedImgList));
		}
	}

	private static void MergeEditedImg(EditedImg localImg, MyselfWork remoteImg)
	{
		if (localImg != null)
		{
			localImg.workId = remoteImg.img.workId;
			localImg.likeCnt = remoteImg.img.likeCnt;
			localImg.isLike = remoteImg.isLike;
			localImg.create = remoteImg.create;
		}
		else
		{
			EditedImg editedImg = new EditedImg(remoteImg.img.imageId, remoteImg.img.workId, remoteImg.img.finalImg, remoteImg.img.thumb, remoteImg.img.likeCnt, remoteImg.isLike, remoteImg.create, hasEdited: true);
			editedImg.isRemote = true;
			ApplicationModel.EditedResourceImgList.Add(editedImg);
		}
	}

	public static void UploadUserEditedImgList()
	{
		UnityEngine.Debug.Log("UploadUserEditedImgList...");
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.workId == null || editedResourceImg.workId == string.Empty)
			{
				FileUploadEventProxy.Instance.SendUploadUserEditedFileMsg(editedResourceImg);
				UnityEngine.Debug.Log("UploadUserEditedImgList...  send msg!!!");
			}
		}
	}

	private static PixleEditedImg GetPixelEditedImgByImageId(string imageId)
	{
		foreach (PixleEditedImg pixleEditedImg in ApplicationModel.PixleEditedImgList)
		{
			if (pixleEditedImg.imageId == imageId)
			{
				return pixleEditedImg;
			}
		}
		return null;
	}

	private static EditedImg GetEditedImgByImageId(string imageId)
	{
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.imageId == imageId)
			{
				return editedResourceImg;
			}
		}
		return null;
	}

	public static void GetOtherUserInfo(string uid, Action<Errcode, OtherUserInfo> callback)
	{
		Errcode err = new Errcode();
		OtherUserInfo otherUserInfo = null;
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_USER_INFO);
		uRLRequest.addParam("uid", uid);
		uRLRequest.addParam("sessionId", GetSessionId());
		DataBroker.getJson(uRLRequest, delegate(OtherUserInfoJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				otherUserInfo = detail.data;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err, otherUserInfo);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, otherUserInfo);
		});
	}

	public static void RequestUserInfo(string sessionId, Action<Errcode> callback)
	{
		UnityEngine.Debug.Log("Request user info .....");
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.USER_INFO);
		uRLRequest.addParam("sessionId", sessionId);
		DataBroker.getJson(uRLRequest, delegate(UserDataJson detail)
		{
			if (detail.status == 0)
			{
				UnityEngine.Debug.Log("request user info ok");
				err.errorCode = Errcode.OK;
				ApplicationModel.userInfo = detail.data;
			}
			else
			{
				UnityEngine.Debug.Log("request info error!!!");
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate
		{
			UnityEngine.Debug.Log("request info ex!!!");
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}

	public static void RefreshUserInfo()
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.USER_INFO);
		uRLRequest.addParam("sessionId", GetSessionId());
		DataBroker.getJson(uRLRequest, delegate(UserDataJson detail)
		{
			if (detail.status == 0)
			{
				ApplicationModel.userInfo = detail.data;
			}
		}, delegate
		{
		});
	}

	public static void productExpireEvent(long productId)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.PRODUCT_HAS_EXPIRE);
		uRLRequest.addParam("uid", GetUserUid());
		uRLRequest.addParam("sessionId", GetSessionId());
		uRLRequest.addParam("productId", productId);
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				RefreshUserInfo();
			}
		}, delegate
		{
		});
	}

	public static void JudgeProductIsExpire()
	{
		if (IsHasLogin())
		{
		}
	}

	private static CustomProduct getCustomProductById(long productId)
	{
		if (ApplicationModel.productList != null || ApplicationModel.productList.Count != 0)
		{
			foreach (CustomProduct product in ApplicationModel.productList)
			{
				if (productId == product.productId)
				{
					return product;
				}
			}
		}
		return null;
	}
}
