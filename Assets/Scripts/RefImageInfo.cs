using System.Collections.Generic;

public class RefImageInfo
{
	public string imageID;

	public string outLine;

	public List<ReferInfo> referList;

	public string thumbnaiUrl;

	public string index;

	public bool locked;
}
