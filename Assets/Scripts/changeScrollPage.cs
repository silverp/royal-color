using UnityEngine;
using UnityEngine.UI;

public class changeScrollPage : MonoBehaviour
{
	private ScrollRect scroll;

	private RectTransform scrollTransform;

	private void Start()
	{
		scroll = GetComponent<ScrollRect>();
		scrollTransform = GetComponent<RectTransform>();
	}

	private void Update()
	{
	}

	private void ClickToChange()
	{
	}

	public void CenterToItem(RectTransform obj)
	{
		Vector2 anchorMin = scrollTransform.anchorMin;
		float y = anchorMin.y;
		Vector2 anchoredPosition = obj.anchoredPosition;
		float num = y - anchoredPosition.y;
		num += (float)obj.transform.GetSiblingIndex() / (float)scroll.content.transform.childCount;
		num /= 1000f;
		num = Mathf.Clamp01(1f - num);
		scroll.verticalNormalizedPosition = num;
		UnityEngine.Debug.Log(num);
	}
}
