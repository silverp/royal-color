using UnityEngine;

public class AboutUsCanvasBtnController : MonoBehaviour
{
	[SerializeField]
	private GameObject AboutUsCanvas;

	[SerializeField]
	private GameObject SettingCanvas;

	private void OnEnable()
	{
		GameQuit.Back += BackTosettingCanvas;
	}

	private void OnDisable()
	{
		GameQuit.Back -= BackTosettingCanvas;
	}

	public void BackTosettingCanvas()
	{
		AboutUsCanvas.SetActive(value: false);
		SettingCanvas.SetActive(value: true);
	}
}
