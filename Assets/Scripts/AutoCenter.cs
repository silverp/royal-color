using UnityEngine;
using UnityEngine.UI;

public class AutoCenter : MonoBehaviour
{
	public Scrollbar m_Scrollbar;

	private float mTargetValue;

	private bool mNeedMove;

	private const float MOVE_SPEED = 1f;

	private const float SMOOTH_TIME = 0.2f;

	private float mMoveSpeed;

	private void Update()
	{
		if (mNeedMove)
		{
			if (Mathf.Abs(m_Scrollbar.value - mTargetValue) < 0.01f)
			{
				m_Scrollbar.value = mTargetValue;
				mNeedMove = false;
			}
			else
			{
				m_Scrollbar.value = Mathf.SmoothDamp(m_Scrollbar.value, mTargetValue, ref mMoveSpeed, 0.2f);
			}
		}
	}

	public void PosChange()
	{
		if (m_Scrollbar.value <= 0.125f)
		{
			mTargetValue = 0f;
			MonoBehaviour.print("0");
		}
		else if (m_Scrollbar.value <= 0.375f)
		{
			mTargetValue = 0.25f;
			MonoBehaviour.print("0.25");
		}
		else if (m_Scrollbar.value <= 0.625f)
		{
			mTargetValue = 0.5f;
			MonoBehaviour.print("0.5");
		}
		else if (m_Scrollbar.value <= 0.875f)
		{
			mTargetValue = 0.75f;
			MonoBehaviour.print("0.75");
		}
		else
		{
			mTargetValue = 1f;
			MonoBehaviour.print("1");
		}
		mNeedMove = true;
	}
}
