using UnityEngine;

public class pixelArtController : MonoBehaviour
{
	[SerializeField]
	private GameObject PixelArtDetailCanvas;

	[SerializeField]
	private GameObject ImglistCanvas;

	[SerializeField]
	private GameObject pixelArtWorkshop;

	private void OnEnable()
	{
		GameQuit.Back += Back;
		pixelArtWorkshop.SetActive(value: false);
	}

	private void OnDisable()
	{
		GameQuit.Back -= Back;
	}

	public void Back()
	{
		PixelArtDetailCanvas.SetActive(value: false);
		UIRoot.ShowUI(ImglistCanvas);;
	}
}
