using System.Collections;
using UnityEngine;

public class MagnifyingGlass : MonoBehaviour
{
	public Material m_Mat;

	public float m_InitialAmount = 0.1f;

	public float m_InitialRadiusX = 0.1f;

	public float m_InitialRadiusY = 0.1f;

	public float m_InitialComplicatedRadiusInner = 0.3f;

	public float m_InitialComplicatedRadiusOuter = 0.6f;

	private float[] m_Amount = new float[8];

	private float[] m_RadiusX = new float[8];

	private float[] m_RadiusY = new float[8];

	private float[] m_RadiusInner = new float[8];

	private float[] m_RadiusOuter = new float[8];

	private float m_MouseX;

	private float m_MouseY;

	private bool m_TraceMouse;

	private Rect[] m_GUIRects = new Rect[9];

	private bool m_UseComplicated;

	private bool m_UseMultiple;

	private bool m_InvertScale;

	private int m_GlassIndex;

	private void ResetData()
	{
		for (int i = 0; i < 8; i++)
		{
			m_Amount[i] = m_InitialAmount;
			m_RadiusX[i] = 0f;
			m_RadiusY[i] = 0f;
			m_RadiusInner[i] = m_InitialComplicatedRadiusInner;
			m_RadiusOuter[i] = m_InitialComplicatedRadiusOuter;
		}
		m_Mat.SetVector("_DisplayCenterOffSet", new Vector4(0f, 25f / 108f * (float)Screen.width / (float)Screen.height, 0f, 0f));
		m_Mat.SetVector("_View_Radial", new Vector4(5f / 216f, 5f / 216f * (float)Screen.width / (float)Screen.height, 5f / 216f, 5f / 216f * (float)Screen.width / (float)Screen.height));
		Material mat = m_Mat;
		Vector3 mousePosition = UnityEngine.Input.mousePosition;
		float x = mousePosition.x / (float)Screen.width;
		Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
		mat.SetVector("_SimpleCenterRadial1", new Vector4(x, mousePosition2.y / (float)Screen.height, m_RadiusX[0], m_RadiusY[0]));
		m_Mat.SetVector("_SimpleCenterRadial2", new Vector4(0.3f, 0.4f, m_RadiusX[1], m_RadiusY[1]));
		m_Mat.SetVector("_SimpleCenterRadial3", new Vector4(0.3f, 0.6f, m_RadiusX[2], m_RadiusY[2]));
		m_Mat.SetVector("_SimpleCenterRadial4", new Vector4(0.3f, 0.8f, m_RadiusX[3], m_RadiusY[3]));
		m_Mat.SetVector("_SimpleCenterRadial5", new Vector4(0.6f, 0.2f, m_RadiusX[4], m_RadiusY[4]));
		m_Mat.SetVector("_SimpleCenterRadial6", new Vector4(0.6f, 0.4f, m_RadiusX[5], m_RadiusY[5]));
		m_Mat.SetVector("_SimpleCenterRadial7", new Vector4(0.6f, 0.6f, m_RadiusX[6], m_RadiusY[6]));
		m_Mat.SetVector("_SimpleCenterRadial8", new Vector4(0.6f, 0.8f, m_RadiusX[7], m_RadiusY[7]));
		m_Mat.SetFloat("_SimpleAmount1", m_Amount[0]);
		m_Mat.SetFloat("_SimpleAmount2", m_Amount[1]);
		m_Mat.SetFloat("_SimpleAmount3", m_Amount[2]);
		m_Mat.SetFloat("_SimpleAmount4", m_Amount[3]);
		m_Mat.SetFloat("_SimpleAmount5", m_Amount[4]);
		m_Mat.SetFloat("_SimpleAmount6", m_Amount[5]);
		m_Mat.SetFloat("_SimpleAmount7", m_Amount[6]);
		m_Mat.SetFloat("_SimpleAmount8", m_Amount[7]);
		m_Mat.SetVector("_ComplicatedCenterRadial1", new Vector4(0.3f, 0.2f, m_RadiusX[0], m_RadiusY[0]));
		m_Mat.SetVector("_ComplicatedCenterRadial2", new Vector4(0.3f, 0.4f, m_RadiusX[1], m_RadiusY[1]));
		m_Mat.SetVector("_ComplicatedCenterRadial3", new Vector4(0.3f, 0.6f, m_RadiusX[2], m_RadiusY[2]));
		m_Mat.SetVector("_ComplicatedCenterRadial4", new Vector4(0.3f, 0.8f, m_RadiusX[3], m_RadiusY[3]));
		m_Mat.SetVector("_ComplicatedCenterRadial5", new Vector4(0.6f, 0.2f, m_RadiusX[4], m_RadiusY[4]));
		m_Mat.SetVector("_ComplicatedCenterRadial6", new Vector4(0.6f, 0.4f, m_RadiusX[5], m_RadiusY[5]));
		m_Mat.SetVector("_ComplicatedCenterRadial7", new Vector4(0.6f, 0.6f, m_RadiusX[6], m_RadiusY[6]));
		m_Mat.SetVector("_ComplicatedCenterRadial8", new Vector4(0.6f, 0.8f, m_RadiusX[7], m_RadiusY[7]));
		m_Mat.SetFloat("_ComplicatedAmount1", m_Amount[0]);
		m_Mat.SetFloat("_ComplicatedAmount2", m_Amount[1]);
		m_Mat.SetFloat("_ComplicatedAmount3", m_Amount[2]);
		m_Mat.SetFloat("_ComplicatedAmount4", m_Amount[3]);
		m_Mat.SetFloat("_ComplicatedAmount5", m_Amount[4]);
		m_Mat.SetFloat("_ComplicatedAmount6", m_Amount[5]);
		m_Mat.SetFloat("_ComplicatedAmount7", m_Amount[6]);
		m_Mat.SetFloat("_ComplicatedAmount8", m_Amount[7]);
		m_Mat.SetFloat("_ComplicatedRadiusInner1", m_RadiusInner[0]);
		m_Mat.SetFloat("_ComplicatedRadiusInner2", m_RadiusInner[1]);
		m_Mat.SetFloat("_ComplicatedRadiusInner3", m_RadiusInner[2]);
		m_Mat.SetFloat("_ComplicatedRadiusInner4", m_RadiusInner[3]);
		m_Mat.SetFloat("_ComplicatedRadiusInner5", m_RadiusInner[4]);
		m_Mat.SetFloat("_ComplicatedRadiusInner6", m_RadiusInner[5]);
		m_Mat.SetFloat("_ComplicatedRadiusInner7", m_RadiusInner[6]);
		m_Mat.SetFloat("_ComplicatedRadiusInner8", m_RadiusInner[7]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter1", m_RadiusOuter[0]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter2", m_RadiusOuter[1]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter3", m_RadiusOuter[2]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter4", m_RadiusOuter[3]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter5", m_RadiusOuter[4]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter6", m_RadiusOuter[5]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter7", m_RadiusOuter[6]);
		m_Mat.SetFloat("_ComplicatedRadiusOuter8", m_RadiusOuter[7]);
	}

	private void OnEnable()
	{
		m_TraceMouse = true;
		StartCoroutine(ResetRadius());
	}

	private IEnumerator ResetRadius()
	{
		yield return null;
		for (int i = 0; i < 8; i++)
		{
			m_RadiusX[i] = 5f / 24f;
			m_RadiusY[i] = 5f / 24f * (float)Screen.width / (float)Screen.height;
		}
	}

	private void Start()
	{
		if (!SystemInfo.supportsImageEffects)
		{
			base.enabled = false;
		}
		QualitySettings.antiAliasing = 8;
		ResetData();
		m_GUIRects[0] = new Rect(10f, 245f, 150f, 25f);
		m_GUIRects[1] = new Rect(10f, 270f, 150f, 25f);
		m_GUIRects[2] = new Rect(10f, 300f, 200f, 25f);
		m_GUIRects[3] = new Rect(95f, 345f, 115f, 25f);
		m_GUIRects[4] = new Rect(95f, 375f, 115f, 25f);
		m_GUIRects[5] = new Rect(95f, 405f, 115f, 25f);
		m_GUIRects[6] = new Rect(95f, 435f, 115f, 25f);
		m_GUIRects[7] = new Rect(95f, 465f, 115f, 25f);
		m_GUIRects[8] = new Rect(10f, 495f, 150f, 25f);
	}

	private void OnDisable()
	{
		for (int i = 0; i < 8; i++)
		{
			m_RadiusX[i] = 0f;
			m_RadiusY[i] = 0f;
		}
	}

	private void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
	{
		int num = 0;
		num = (m_UseComplicated ? ((!m_UseMultiple) ? 2 : 3) : (m_UseMultiple ? 1 : 0));
		int glassIndex = m_GlassIndex;
		string name = "_SimpleAmount" + (glassIndex + 1);
		m_Mat.SetFloat(name, m_Amount[glassIndex]);
		string name2 = "_SimpleCenterRadial" + (glassIndex + 1);
		m_Mat.SetVector(name2, new Vector4(m_MouseX, m_MouseY, m_RadiusX[glassIndex], m_RadiusY[glassIndex]));
		string name3 = "_ComplicatedAmount" + (glassIndex + 1);
		m_Mat.SetFloat(name3, m_Amount[glassIndex]);
		string name4 = "_ComplicatedCenterRadial" + (glassIndex + 1);
		m_Mat.SetVector(name4, new Vector4(m_MouseX, m_MouseY, m_RadiusX[glassIndex], m_RadiusY[glassIndex]));
		string name5 = "_ComplicatedRadiusInner" + (glassIndex + 1);
		m_Mat.SetFloat(name5, m_RadiusInner[glassIndex]);
		string name6 = "_ComplicatedRadiusOuter" + (glassIndex + 1);
		m_Mat.SetFloat(name6, m_RadiusOuter[glassIndex]);
		Graphics.Blit(sourceTexture, destTexture, m_Mat, num);
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			m_TraceMouse = true;
			int num = 0;
			while (true)
			{
				if (num < m_GUIRects.Length)
				{
					ref Rect reference = ref m_GUIRects[num];
					Vector3 mousePosition = UnityEngine.Input.mousePosition;
					float x = mousePosition.x;
					float num2 = Screen.height;
					Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
					if (reference.Contains(new Vector2(x, num2 - mousePosition2.y)))
					{
						break;
					}
					num++;
					continue;
				}
				return;
			}
			m_TraceMouse = false;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			m_TraceMouse = false;
		}
		else if (Input.GetMouseButton(0) && m_TraceMouse)
		{
			Vector3 mousePosition3 = UnityEngine.Input.mousePosition;
			m_MouseX = mousePosition3.x / (float)Screen.width;
			Vector3 mousePosition4 = UnityEngine.Input.mousePosition;
			m_MouseY = mousePosition4.y / (float)Screen.height;
		}
	}
}
