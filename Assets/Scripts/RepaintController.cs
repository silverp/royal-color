using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RepaintController : MonoBehaviour
{
	[SerializeField]
	private GameObject BeanBox;

	[SerializeField]
	private GameObject BeanNoteBox;

	[SerializeField]
	private GameObject workShopCanvas;

	[SerializeField]
	private GameObject pixelArtWorkshop;

	private void OnEnable()
	{
		DragEventProxy.OnRepaint += GotWorkShop;
		DragEventProxy.OnRepaintAfterPurchase += EnterWorkShopCanvasAfterPurchase;
		DragEventProxy.OnShareCallback += ShareCallback;
		DragEventProxy.OnGotoZoomImage += GotoZoomEvent;
	}

	private void OnDisable()
	{
		DragEventProxy.OnRepaint -= GotWorkShop;
		DragEventProxy.OnRepaintAfterPurchase -= EnterWorkShopCanvasAfterPurchase;
		DragEventProxy.OnShareCallback -= ShareCallback;
		DragEventProxy.OnGotoZoomImage -= GotoZoomEvent;
	}

	public void GotWorkShop(WorkInfo workInfo)
	{
		UnityEngine.Debug.Log("gotto work shop wrokinfo is:" + JsonConvert.SerializeObject(workInfo));
		if (!PublicUserInfoApi.IsHasLogin())
		{
			DragEventProxy.Instance.SendOffLineMsg(base.transform.parent.name);
		}
		else if (workInfo.type == "pixel")
		{
			PixelArt pixelArtByImageId = PublicImgaeApi.GetPixelArtByImageId(workInfo.imageId);
			if (pixelArtByImageId == null)
			{
				PublicImgaeApi.GetPixelOriginInfo(workInfo.imageId, GetPixelOriginInfoCallback);
			}
			else
			{
				AfterGetResourceImg(pixelArtByImageId);
			}
		}
		else
		{
			ResourceImg resourceImgFromColoredimg = PublicImgaeApi.GetResourceImgFromColoredimg(workInfo.imageId);
			if (resourceImgFromColoredimg == null)
			{
				PublicControllerApi.GetOriginImageDetailInfo(workInfo.imageId, delegate(OriginDetail x)
				{
					RequestResourceimgCallback(x);
				});
			}
			else
			{
				AfterGetResourceImg(resourceImgFromColoredimg);
			}
		}
	}

	private void GetPixelOriginInfoCallback(Errcode err, PixelArt pixelArt)
	{
		if (err.errorCode == Errcode.OK)
		{
			AfterGetResourceImg(pixelArt);
		}
		else
		{
			UnityEngine.Debug.Log("request url error!!!");
		}
	}

	private void RequestResourceimgCallback(OriginDetail imgInfo)
	{
		ResourceImg resourceImg = new ResourceImg(imgInfo.imageId, imgInfo.outline, imgInfo.index, imgInfo.thumbnailUrl, imgInfo.locked);
		AfterGetResourceImg(resourceImg);
	}

	private void AfterGetResourceImg(ResourceImg resourceImg)
	{
		bool flag = UnlockArtManager.Instance.IsShowLock(resourceImg);
		UnityEngine.Debug.Log("inspiration GotWorkShop is:" + flag);
		if (flag)
		{
			UnlockArtManager.Instance.UnlockArt(resourceImg, delegate(Errcode err, int amount)
			{
				unlockCallback(err, amount, resourceImg);
			});
		}
		else
		{
			EnterWorkShopCanvas(resourceImg);
		}
	}

	private void AfterGetResourceImg(PixelArt pixelArt)
	{
		bool flag = UnlockArtManager.Instance.IsShowLock(pixelArt);
		UnityEngine.Debug.Log("inspiration GotWorkShop is:" + flag);
		if (flag)
		{
			UnlockArtManager.Instance.UnlockArt(pixelArt, delegate(Errcode err, int amount)
			{
				unlockCallback(err, amount, pixelArt);
			});
		}
		else
		{
			EnterWorkShopCanvas(pixelArt);
		}
	}

	private void unlockCallback(Errcode err, int amount, ResourceImg resurceImg)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("inspiration unlockCallback ok");
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 0.6f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				ApplicationModel.userInfo.BeanCnt += amount;
				EnterWorkShopCanvas(resurceImg);
			});
		}
		else
		{
			DragEventProxy.Instance.SendUnclockOutlineMsg(resurceImg, null, ImageType.normal);
		}
	}

	private void unlockCallback(Errcode err, int amount, PixelArt pixelArt)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("inspiration unlockCallback ok");
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 0.6f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				ApplicationModel.userInfo.BeanCnt += amount;
				EnterWorkShopCanvas(pixelArt);
			});
		}
		else
		{
			DragEventProxy.Instance.SendUnclockOutlineMsg(null, pixelArt, ImageType.normal);
		}
	}

	public void EnterWorkShopCanvasAfterPurchase(ResourceImg resourceImg, PixelArt pixelArt, ImageType type)
	{
		switch (type)
		{
		case ImageType.pixel:
			EnterWorkShopCanvas(resourceImg);
			break;
		case ImageType.normal:
			EnterWorkShopCanvas(pixelArt);
			break;
		}
	}

	public void EnterWorkShopCanvas(ResourceImg image)
	{
		EditedImg editedImg = (EditedImg)(ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedImgFromEditedList(image.imageId, isCreate: true));
		FileUploadEventProxy.Instance.SendClickImageMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
		base.transform.parent.gameObject.SetActive(value: false);
		workShopCanvas.gameObject.SetActive(value: true);
	}

	public void EnterWorkShopCanvas(PixelArt pixelart)
	{
		ApplicationModel.lastVisibleScreen = base.transform.parent.name;
		PixleEditedImg img = (PixleEditedImg)(ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedPixelImgFromEditedList(pixelart, isCreate: true));
		FileUploadEventProxy.Instance.SendClickImageMsg(img, ApplicationModel.device_id);
		base.transform.parent.gameObject.SetActive(value: false);
		pixelArtWorkshop.gameObject.SetActive(value: true);
	}

	public void ShareCallback(Errcode err)
	{
		if (err.errorCode == Errcode.OK && PublicUserInfoApi.IsHasLogin())
		{
			BeanAction.request(BeanAction.SHARE, beanActionCallback);
		}
	}

	public void ShareSuccess()
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			UnityEngine.Debug.Log("ShareSuccess....");
			BeanAction.request(BeanAction.SHARE, beanActionCallback);
		}
	}

	private void beanActionCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("beanActionCallback.... ok");
			UnityEngine.Debug.Log("top panel name is:" + base.transform.parent.name);
			BeanBox.gameObject.SetActive(value: true);
			ApplicationModel.userInfo.BeanCnt += amount;
			BeanBox.transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = PublicToolController.GetTextByLanguage("Share success");
				BeanBox.transform.GetChild(1).GetChild(4).GetComponent<Text>()
					.text = "+" + amount;
					DragEventProxy.Instance.SendRefreshBeanCnt(amount);
				}
			}

			public void GotoZoomEvent(Texture2D tex)
			{
				base.transform.parent.Find("ZoomPanel").GetComponent<ZoomPanelBtnController>().curTex = tex;
				base.transform.parent.Find("ZoomPanel").gameObject.SetActive(value: true);
			}
		}
