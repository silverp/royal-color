using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPageProgressBarController : MonoBehaviour
{
	[SerializeField]
	private GameObject pen;

	public static LoadingPageProgressBarController instance;

	private float loadingPrecentage;

	public int totalCntToDownload;

	private int currentDownloadCnt;

	private int progressBarStartPosX = 10;

	private int progressBarPosXCurrent = 10;

	private int progressBarStopPosX = 480;

	private float timeEsplated;

	private float maxWaitTime = 2f;

	private Coroutine _coroutine;

	private int totaltotalCntToDownload;

	public bool isFinishLoadInfo;

	private RectTransform maskTransform;

	private RectTransform penTransform;

	private bool isStartCoroutine;

	private bool flag;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		maskTransform = base.transform.GetChild(0).GetChild(0).GetChild(0)
			.GetComponent<RectTransform>();
		penTransform = pen.GetComponent<RectTransform>();
		maxWaitTime = 5f;
		base.transform.GetChild(1).GetComponent<Text>().text = "DownLoading  resources...";
		DragEventProxy.Instance.SendChangeTextMsg();
		timeEsplated = 0f;
		_coroutine = StartCoroutine(NetWorkRequest.DownloadInfoAndImgOnLoading());
	}

	public void IncreseDownloadCnt()
	{
		currentDownloadCnt++;
	}

	private IEnumerator waitTimeAndLoadSence()
	{
		StopCoroutine(_coroutine);
		yield return new WaitForSeconds(0.2f);
		GameObject LoadingCanvas = PublicToolController.FindParentWithTag(base.gameObject, "LoadingCanvas");
		CheckEditedImgListIsHasFile();
		AssembleData();
		ApplicationModel.RestorePerfab();
		LoadingCanvas.transform.parent.Find("UIRoot").gameObject.SetActive(value: true);
		LoadingCanvas.SetActive(value: false);
		Resources.UnloadUnusedAssets();
	}

	private void AssembleData()
	{
		int num = ApplicationModel.CateResourceList.Count;
		for (int i = 0; i < num; i++)
		{
			if (ApplicationModel.CateResourceList[i] == null)
			{
				ApplicationModel.CateResourceList.RemoveAt(i);
				i--;
				num--;
			}
		}
	}

	private void CheckEditedImgListIsHasFile()
	{
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (!editedResourceImg.isRemote && editedResourceImg.HasEdited() && !PublicImgaeApi.IsHasLocalFinalImg(editedResourceImg))
			{
				editedResourceImg.hasEdited = false;
			}
		}
	}

	private int GetOldIndexByCid(int cid)
	{
		int num = 0;
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return num;
			}
			num++;
		}
		return 0;
	}

	private void Move<T>(List<T> list, int oldIndex, int newIndex)
	{
		if (list != null && list.Count != 0)
		{
			T item = list[oldIndex];
			list.RemoveAt(oldIndex);
			list.Insert(newIndex, item);
		}
	}

	private int getElementIndexByCid(int cid)
	{
		int count = ApplicationModel.CateResourceList.Count;
		for (int i = 0; i < count; i++)
		{
			if (ApplicationModel.CateResourceList[i].cid == cid)
			{
				return i;
			}
		}
		return 0;
	}

	private void ExcuteCoroutine()
	{
		if (!isStartCoroutine)
		{
			RectTransform rectTransform = maskTransform;
			float x = progressBarStopPosX;
			Vector2 anchoredPosition = maskTransform.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x, anchoredPosition.y);
			RectTransform rectTransform2 = penTransform;
			float x2 = progressBarStopPosX;
			Vector2 anchoredPosition2 = penTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition2.y);
			base.transform.GetChild(1).GetComponent<Text>().text = "Load resources completed ^o^";
			DragEventProxy.Instance.SendChangeTextMsg();
			StartCoroutine(waitTimeAndLoadSence());
			isStartCoroutine = true;
		}
	}

	private void Update()
	{
		if (isStartCoroutine)
		{
			return;
		}
		if (isFinishLoadInfo)
		{
			ExcuteCoroutine();
			return;
		}
		timeEsplated += Time.deltaTime;
		Vector2 anchoredPosition = maskTransform.anchoredPosition;
		if (anchoredPosition.x <= (float)progressBarPosXCurrent)
		{
			RectTransform rectTransform = maskTransform;
			float x = progressBarPosXCurrent;
			Vector2 anchoredPosition2 = maskTransform.anchoredPosition;
			rectTransform.anchoredPosition = new Vector2(x, anchoredPosition2.y);
			RectTransform rectTransform2 = penTransform;
			float x2 = progressBarPosXCurrent;
			Vector2 anchoredPosition3 = penTransform.anchoredPosition;
			rectTransform2.anchoredPosition = new Vector2(x2, anchoredPosition3.y);
		}
		float num = (float)((double)timeEsplated + 0.1) / (maxWaitTime + 0.0001f);
		if (num < 1f)
		{
			progressBarPosXCurrent = progressBarStartPosX + (int)(num * (float)(progressBarStopPosX - progressBarStartPosX));
		}
		else if (num >= 1f || timeEsplated > maxWaitTime)
		{
			UnityEngine.Debug.Log("loading time out！！！");
			progressBarPosXCurrent = progressBarStopPosX;
			ExcuteCoroutine();
		}
	}
}
