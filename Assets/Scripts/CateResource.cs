using System;
using System.Collections.Generic;

public class CateResource : IComparable
{
	private static readonly int ATLEAST_DOWNLOAD_TO_SHOW = 1;

	public int cid;

	public string name;

	public ResourceImg headImg;

	public List<ResourceImg> imgList;

	public string url;

	public string innerUrl;

	public bool locked;

	public bool isFade;

	public long updateTime;

	public CateResource(int cid, string name)
	{
		this.cid = cid;
		this.name = name;
	}

	public bool ShouldShow()
	{
		if (!headImg.hasDownloaded)
		{
			return false;
		}
		return true;
	}

	public int CompareTo(object obj)
	{
		try
		{
			CateResource cateResource = obj as CateResource;
			if (cid > cateResource.cid)
			{
				return 0;
			}
			return 1;
		}
		catch (Exception ex)
		{
			throw new Exception(ex.Message);
		}
	}
}
