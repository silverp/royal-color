using HedgehogTeam.EasyTouch;
using SVGImporter;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NewWorkerController : MonoBehaviour, IPointerClickHandler
{
	public struct Point
	{
		public short x;

		public short y;

		public Point(short aX, short aY)
		{
			x = aX;
			y = aY;
		}

		public Point(int aX, int aY)
		{
			this = new Point((short)aX, (short)aY);
		}
	}

	//[SerializeField]
	//private EasyTouch easyTouch;

    [SerializeField]
	private GameObject SpinPanel;

	[SerializeField]
	private GameObject WorkShopCanvas;

	[SerializeField]
	private GameObject NoteText;

	[SerializeField]
	private GameObject outLine;

	[SerializeField]
	private Image[] ignoreElements;

	[SerializeField]
	private float alphaHitTestMinThreshold;


    private RBPixelPalette pixelPalette;

	private float pdfScale = 1f;

	private Texture2D mainTex;

	private Rect imageRect;

	private static float RECT_DEPEND_W = 1080f;

	private static float RECT_DEPEND_H = 1080f;

	private Vector2 startPos;

	private float PRESS_MARGIN = 15f;

	private float scale_factor = 0.14f;

	private float MAXSCALE = 30f;

	private float MIN_SCALE = 0.5f;

	private bool colorHolds;

	private Vector2 prevDist = new Vector2(0f, 0f);

	private Vector2 curDist = new Vector2(0f, 0f);

	private Vector2 midPoint = new Vector2(0f, 0f);

	private Vector2 ScreenSize;

	private Vector3 originalPos;

	private GameObject parentObject;

	private RectTransform parentRectTransform;

	private RectTransform grandParentRectTransform;

	private RectTransform rectTransform;

	private bool isMousePressed;

	private Vector2 parentOriginPos;

	private Vector3 originPos;

	private float SCREEN_SCALE;

	private float START_X;

	private int rect_deviation = 113;

	private Vector2 _lastMousePos = Vector2.zero;

	private float MOVE_FACTOR;

	public static bool EventLock = false;

	private int clickColorBtnCount;

	private GameObject canvasList;

	private bool isInitFinish;

	private GUIStyle guiStyle = new GUIStyle();

	private float MAX_WAIT_TIME = 12f;

	private float time;

	private bool isHasNote;

	private byte[] bytes_temp;

	private EditedImg currentImg;

	public string returnPageName;

	private Coroutine _downloadCoroutine;

	private float startCountTime;

	private bool pencilClocked;

	private static Vector3 prevPos = Vector3.zero;
    private float radiusTime;

    private void Awake()
	{
		foreach(var i in ignoreElements)
		{
			i.alphaHitTestMinimumThreshold = alphaHitTestMinThreshold;
		} 
		Vector3 localPosition = base.transform.localPosition;
        base.transform.localPosition = new Vector2(localPosition.x, rect_deviation);
		canvasList = PublicToolController.FindParentWithTag(base.gameObject, "canvasList");
		MOVE_FACTOR = 2.3f;
	}

	private void OnEnable()
	{
		GameQuit.Back += GotoCateList;
		PublicCallFrameTool.Instance.CancelCollectGarbage();
		currentImg = (EditedImg)ApplicationModel.currentEditedImg;
		Init();
		startCountTime = Time.realtimeSinceStartup;
		WorkShopManagerController.instance.ChangePencilColorTo(ApplicationModel.CurrentCol);
		_downloadCoroutine = StartCoroutine(WorkshopLoader.Instance.StartDownloadEditedAssets(currentImg, CompleteDownloadResource));
		WorkShopManagerController.onPencilColorChanged += ClickColorBtn;
	}

	private void Init()
	{
		isInitFinish = false;
		isHasNote = false;
		time = MAX_WAIT_TIME;
		SpinPanel.SetActive(value: true);
		NoteText.SetActive(value: false);
		clickColorBtnCount = 0;
		colorHolds = false;
	}

	private void FinishLoadPdf()
	{
		isInitFinish = true;
		SpinPanel.SetActive(value: false);
		NoteText.SetActive(value: false);
		float num = Time.realtimeSinceStartup - startCountTime;
	}

	private void CompleteDownloadResource(Errcode errcode, Texture2D texture, SVGAsset svg)
	{
		if (errcode.errorCode == 0)
		{
			float realtimeSinceStartup = Time.realtimeSinceStartup;
			mainTex = texture;
			mainTex.filterMode = FilterMode.Point;
			mainTex.wrapMode = TextureWrapMode.Clamp;
			pixelPalette = new RBPixelPalette(GetComponent<RawImage>(), mainTex, currentImg);
			InitWorkShop();
			outLine.GetComponent<SVGImage>().vectorGraphics = svg;
			pixelPalette.setMainTex(mainTex);
			pixelPalette.SetSvgAsset(svg);
			pixelPalette.setHasMask(0);
			//float num = Time.realtimeSinceStartup - realtimeSinceStartup;
			FinishLoadPdf();
		}
		else if (errcode.errorCode == 1)
		{
			SpinPanel.SetActive(value: false);
			NoteText.SetActive(value: true);
		}
		else if (errcode.errorCode == 2)
		{
			SpinPanel.SetActive(value: false);
			NoteText.SetActive(value: true);
		}
	}

	private void Update()
    { 
        this.radiusTime += Time.deltaTime * 2;
        if (this.pixelPalette != null)
        {
            this.pixelPalette.setRadiusTime(radiusTime);
        }
        if (time > 0f)
		{
			time -= Time.deltaTime;
		}
		else if (!isHasNote && !isInitFinish)
		{
			isHasNote = true;
			SpinPanel.SetActive(value: false);
			NoteText.SetActive(value: true);
		}
		if (isInitFinish)
		{
			if (EventLock)
			{
				UnityEngine.Debug.Log("EventLock is........false");
				EventLock = false;
			}
			else
            {
                var touches = InputHelper.GetTouches();
				checkTouch(touches);
				checkForMultiTouch(touches);

#if UNITY_EDITOR
                checkInputPc();
#endif
            }
        }
	}

	private void InitWorkShop()
	{
		pencilClocked = false;
		_lastMousePos = Vector2.zero;
		Color32 color = Color.white;
		SCREEN_SCALE = CanvasScreenAutoFix.instance.scaleRatio;
		START_X = ((float)Screen.width - RECT_DEPEND_W * SCREEN_SCALE) / 2f;
		imageRect = new Rect(START_X, 230f * SCREEN_SCALE, RECT_DEPEND_W * SCREEN_SCALE, RECT_DEPEND_H * SCREEN_SCALE);
		parentObject = base.transform.parent.gameObject;
		parentRectTransform = parentObject.GetComponent<RectTransform>();
		grandParentRectTransform = parentRectTransform.parent.GetComponent<RectTransform>();
		rectTransform = GetComponent<RectTransform>();
		parentOriginPos = parentRectTransform.anchoredPosition;
		ScreenSize = new Vector2(Screen.width, Screen.height);
		originalPos = base.transform.position;
		isMousePressed = false;
		parentRectTransform.localScale = new Vector3(1f, 1f, 1f);
		parentRectTransform.anchoredPosition = new Vector2(0f, 0f);
	}

	private void myMouseDown(PointerEventData dat)
	{
		RectTransform component = GetComponent<RectTransform>();
		Vector2 position = dat.position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(component, position, null, out Vector2 localPoint))
		{
			int num = (int)localPoint.x;
			int num2 = (int)localPoint.y;
			if (num < 0)
			{
				num += (int)component.rect.width / 2;
			}
			else
			{
				num += (int)component.rect.width / 2;
			}
			if (num2 > 0)
			{
				num2 += (int)component.rect.height / 2;
			}
			else
			{
				num2 += (int)component.rect.height / 2;
			}
		}
	}

	public void GotoCateList()
	{
		storeImageBeforeGotoCateList();
        UIRoot.ShowBack(canvasList, ApplicationModel.lastVisibleScreen); 
		DragEventProxy.Instance.SendCloseWorkShopCanvasMsg();
		WorkShopCanvas.SetActive(value: false);
	}

	public void Gotoshare()
	{
		if (isInitFinish)
		{
			storeImageBeforeGotoSticker();
			canvasList.transform.Find("StickerCanvas").gameObject.SetActive(value: true);
			ApplicationModel.CurrentCanvasName = "MyworkCanvas";
			WorkShopCanvas.SetActive(value: false);
			TotalGA.Event("workshop_page_event", "完成");
		}
	}

	public void storeImageBeforeGotoSticker()
	{
		ClickEventManagement.isAcceptClickEvent = false;
		if (isInitFinish)
		{
			pixelPalette.setHasMask(1);
			pixelPalette.savePattle();
			pixelPalette.saveTexture();
			currentImg.isRemote = false;
			PublicToolController.FileCopier(currentImg.imageId + "_ok", currentImg.imageId + "_unsticker");
			if (colorHolds)
			{
				currentImg.SetEdited();
				currentImg.create = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds * 1000;
			}
		}
	}

	public void storeImageBeforeGotoCateList()
	{
		ClickEventManagement.isAcceptClickEvent = false;
		StoreImagechange();
		RestorePlayfebManagement.instance.StoreEditedList();
		TotalGA.Event("workshop_page_event", "返回");
	}

	public void StoreImagechange()
	{
		if (!isInitFinish)
		{
			return;
		}
		pixelPalette.setHasMask(1);
		pixelPalette.savePattle();
		pixelPalette.saveTexture();
		currentImg.isRemote = false;
		if (colorHolds)
		{
			if (ApplicationModel.userInfo != null)
			{
				FileUploadEventProxy.Instance.SendUploadUserEditedFileMsg(currentImg);
			}
			currentImg.SetEdited();
			currentImg.create = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds * 1000;
			DragEventProxy.Instance.SendChangeIndexImageMsg(currentImg.imageId);
		}
	}

	public void checkInputPc()
	{
		if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0f)
		{
			float axis = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
			Vector3 mousePosition = UnityEngine.Input.mousePosition;
			if (!RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, mousePosition, Camera.main))
			{
				return;
			}
			RectTransformUtility.ScreenPointToLocalPointInRectangle(grandParentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 _);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint2);
			Vector3 localScale = parentObject.transform.localScale;
			float x = localScale.x + scale_factor * axis / Mathf.Abs(axis);
			Vector3 localScale2 = parentObject.transform.localScale;
			Vector2 scaleTo = new Vector3(x, localScale2.y + scale_factor * axis / Mathf.Abs(axis));
			if ((double)scaleTo.x > 6.0 || (double)scaleTo.y > 6.0)
			{
				scaleTo = new Vector2(5f, 5f);
			}
			newScale(localPoint2, scaleTo);
		}
		//if (Input.GetMouseButtonDown(0))
		//{
		//	Vector2 screenPoint = UnityEngine.Input.mousePosition;
		//	if (!RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, screenPoint, Camera.main))
		//	{
		//		return;
		//	}
		//	RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, screenPoint, Camera.main, out Vector2 localPoint3);
		//	_lastMousePos = localPoint3;
		//}
		//if (!Input.GetMouseButtonUp(0) || !RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, UnityEngine.Input.mousePosition, Camera.main))
		//{
		//	return;
		//}
		//RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint4);
		//if (Vector2.Distance(_lastMousePos, localPoint4) > PRESS_MARGIN)
		//{
		//	parentRectTransform.anchoredPosition += localPoint4 - _lastMousePos;
		//}
		//float num = (float)mainTex.width / RECT_DEPEND_W;
		//float num2 = (float)mainTex.height / RECT_DEPEND_H;
		//int num3 = (int)(localPoint4.x * num);
		//int num4 = (int)((localPoint4.y - (float)rect_deviation) * num2);
		//if (mainTex != null && num3 + num4 * mainTex.width < mainTex.width * mainTex.height && num3 >= 0 && num4 >= 0)
		//{
		//	if (ApplicationModel.IsGradient)
		//	{
		//		pixelPalette.setColor(num3, num4, WorkShopManagerController.instance.GetPencilColor(), RBPixelPalette.ColoringTheme.Gradient);
		//	}
		//	else
		//	{
		//		pixelPalette.setColor(num3, num4, WorkShopManagerController.instance.GetPencilColor());
		//	}
		//	AddColorToRecentList();
		//	colorHolds = true;
		//}
	}

	private void AddColorToRecentList()
	{
		if (ApplicationModel.recentColorlist != null)
		{
			ColorComplex existItem = null;
			foreach (ColorComplex item in ApplicationModel.recentColorlist)
			{
				if (item.IsEqual(ApplicationModel.currentColorComplex))
				{
					existItem = item;
					break;
				}
			}

			if (existItem != null)
				ApplicationModel.recentColorlist.Remove(existItem);
		}
		else
		{
			ApplicationModel.recentColorlist = new List<ColorComplex>();
		}
		if (ApplicationModel.recentColorlist.Count >= 8)
		{
			ApplicationModel.recentColorlist.RemoveAt(0);
		}
		ApplicationModel.recentColorlist.Add(ApplicationModel.currentColorComplex);
		DragEventProxy.Instance.SendAddColorToRecentList();
	}
	private bool pointClickValid = false;
    public void OnPointerClick(PointerEventData eventData)
	{
        pointClickValid = true;
        //Debug.Log("OnPointerClick " + eventData);
    }

    private bool _isMoving = false;
	private bool _mouseDown = false;
    private void checkTouch(Touch[] touches)
	{
		if (touches.Length != 1 || !RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, touches[0].position, Camera.main))
        {
			pointClickValid = false;
            _mouseDown = false;
            _isMoving = false;
            return;
        }
        var touch = touches[0];
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, touch.position, Camera.main, out Vector2 localPoint);
		float widthRatio = (float)mainTex.width / RECT_DEPEND_W;
		float heightRatio = (float)mainTex.height / RECT_DEPEND_H;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(grandParentRectTransform, touch.position, Camera.main, out Vector2 touchPoint);
        //RectTransformUtility.ScreenPointToWorldPointInRectangle(parentRectTransform, touch.position, Camera.main, out Vector3 touchPoint);
        //Vector2 position = touch.position;
        switch (touch.phase)
		{
			case TouchPhase.Began:
				startPos = touchPoint;
                _mouseDown = true;
                pointClickValid = false;
                break;
			case TouchPhase.Moved:
                if (_mouseDown && Vector2.Distance(touchPoint, startPos) > 0)
                {
					_isMoving = true;
					var diff = touchPoint - startPos;
                    parentRectTransform.anchoredPosition = parentRectTransform.anchoredPosition + diff;
     //               Transform transform = parentObject.transform;
					//Vector3 localPosition = transform.localPosition;
					//Vector3 right = Vector3.right;
					//Vector2 deltaPosition = touch.deltaPosition;
					//Vector3 a = right * deltaPosition.x;// * MOVE_FACTOR;
					//Vector3 up = Vector3.up; 
					//transform.localPosition = localPosition + (a + up * deltaPosition.y);
					startPos = touchPoint;
				}
				break;
			case TouchPhase.Ended:
				{
					//UnityEngine.Debug.Log("TouchPhase.Ended.....");
					if (!_isMoving && Vector2.Distance(touchPoint, startPos) <= 0.0001f)
                    {
                        _mouseDown = false;
                        _isMoving = false;
                        //UnityEngine.Debug.Log("Vector2.Distance ");
						int screenX = (int)(localPoint.x * widthRatio);
						int screenY = (int)((localPoint.y - (float)rect_deviation) * heightRatio);
						if (screenX + screenY * mainTex.width < mainTex.width * mainTex.height && screenX >= 0 && screenY >= 0)
                        {
                            //UnityEngine.Debug.Log("Coloring");
							if (pointClickValid)
							{
								this.radiusTime = 0f;
								if (ApplicationModel.IsGradient)
								{
									pixelPalette.setColor(screenX, screenY, WorkShopManagerController.instance.GetPencilColor(), RBPixelPalette.ColoringTheme.Gradient);
								}
								else
								{
									pixelPalette.setColor(screenX, screenY, WorkShopManagerController.instance.GetPencilColor());
								}
								// AddColorToRecentList();
								colorHolds = true;
                                pointClickValid = false;
                            }
                        }
                        break;
                    }
					pointClickValid = false;
                    _mouseDown = false;
					_isMoving = false;
                    Vector2 apos = parentRectTransform.anchoredPosition;
                    float newX = apos.x;
                    float newY = apos.y;
                    if (apos.x > 0f)
                    {
                        newX = 0f;
                    }
                    if (apos.y > 0f)
                    {
                        newY = 0f;
                    }
                    float width = parentRectTransform.rect.width;
                    Vector3 localScale = parentRectTransform.localScale;
                    if (apos.x < 0f - width * localScale.x + parentRectTransform.rect.width)
                    {
                        newX = 0f - width * localScale.x + parentRectTransform.rect.width;
                    }
                    float height = parentRectTransform.rect.height;
                    if (apos.y < 0f - height * localScale.y + parentRectTransform.rect.height)
                    {
                        newY = 0f - height * localScale.y + parentRectTransform.rect.height;
                    }
                    if (newX == apos.x && newY == apos.y)
                    {
                        break;
                    }
                    StartCoroutine(MoveToPosition(parentRectTransform, new Vector2(newX, newY), 0.3f));
                    break;
				}
		}
	}

	private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
	{
		Vector2 currentPos = rectTransform.anchoredPosition;
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / TimeToMove;
			rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
			yield return null;
		}
	}

	private void checkForMultiTouch(Touch[] touches)
	{
		if (touches.Length != 2)
		{
			return;
		}
		if (touches[0].phase == TouchPhase.Moved && touches[1].phase == TouchPhase.Moved)
		{
			Vector2 pos1 = touches[0].position; 
			Vector2 pos2 = touches[1].position; 
			midPoint = new Vector2((pos1.x + pos2.x) / 2f, (pos1.y + pos2.y) / 2f);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(grandParentRectTransform, midPoint, Camera.main, out Vector2 _);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition,
				Camera.main, out Vector2 localPoint);
			curDist = pos1 - pos2;
			prevDist = pos1 - touches[0].deltaPosition - (pos2 - touches[1].deltaPosition);
			float diff = curDist.magnitude - prevDist.magnitude;
			Vector2 scaleTo;// = default(Vector2);
			if (diff > 0f)
			{
				Vector3 localScale = parentRectTransform.localScale; 
				scaleTo = new Vector2(localScale.x + scale_factor, localScale.y + scale_factor);
			}
			else
			{
				Vector3 localScale = parentRectTransform.localScale; 
				scaleTo = new Vector2(localScale.x - scale_factor * 1.8f, localScale.y - scale_factor * 1.8f);
			}
			if (scaleTo.x > MAXSCALE || scaleTo.y > MAXSCALE)
			{
				scaleTo = new Vector2(MAXSCALE, MAXSCALE);
			}
			newScale(localPoint, scaleTo);
		}
		if (touches[0].phase == TouchPhase.Ended || touches[1].phase == TouchPhase.Ended)
		{
			Vector3 scaleTo = parentRectTransform.localScale;  
			if (scaleTo.x >= MAXSCALE || scaleTo.y >= MAXSCALE)
			{
                scaleTo = new Vector2(MAXSCALE - 1f, MAXSCALE - 1f);
			}
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, 
				Camera.main, out Vector2 localPoint);
			newScale(localPoint, scaleTo);
		}
	}

	private void newScale(Vector2 fromParent, Vector2 scaleTo)
	{
		Vector2 anchoredPosition = parentRectTransform.anchoredPosition;
		Vector2 vector = parentRectTransform.localScale;
		Vector2 b = Vector2.Scale(fromParent, scaleTo - vector);
		parentRectTransform.anchoredPosition = anchoredPosition - b;
		parentRectTransform.localScale = Vector3.one * scaleTo.x;
		Vector3 localScale = parentRectTransform.localScale;
		if (localScale.x <= 1f)
		{
			parentRectTransform.localScale = new Vector3(1f, 1f, 1f);
			parentRectTransform.anchoredPosition = new Vector2(0f, 0f);
		} 
		if (localScale.x > MAXSCALE)
		{
			parentRectTransform.localScale = vector;
			parentRectTransform.anchoredPosition = anchoredPosition;
		}
	}

	public void Undo()
	{
		pixelPalette.Undo();
		TotalGA.Event("workshop_page_event", "undo");
	}

	public void Redo()
	{
		pixelPalette.Redo();
		TotalGA.Event("workshop_page_event", "redo");
	}

	private void OnDisable()
	{
		GameQuit.Back -= GotoCateList;
		if (isInitFinish)
		{
			pixelPalette.destoryTex();
		}
		pixelPalette = null;
		mainTex = null;
		if (mainTex != null)
		{
			UnityEngine.Object.DestroyImmediate(mainTex);
		}
		RawImage component = WorkShopCanvas.transform.Find("Mask").Find("Panel").Find("NewWorkPlace")
			.GetComponent<RawImage>();
		component.texture = null;
		component.material = null;
		StopCoroutine(_downloadCoroutine);
		PublicCallFrameTool.Instance.CollectGarbage(force: true);
		WorkShopManagerController.onPencilColorChanged -= ClickColorBtn;
		outLine.GetComponent<SVGImage>().vectorGraphics = null;
		TotalGA.Event("use_color_num", clickColorBtnCount + "次");
	}

	private void ClickColorBtn(Color from, Color to)
	{
		if (from != to)
		{
			clickColorBtnCount++;
		}
	}
}
