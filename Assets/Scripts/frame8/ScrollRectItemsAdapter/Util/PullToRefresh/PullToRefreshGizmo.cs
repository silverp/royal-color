using UnityEngine;

namespace frame8.ScrollRectItemsAdapter.Util.PullToRefresh
{
	public class PullToRefreshGizmo : MonoBehaviour
	{
		private bool _IsShown;

		public virtual bool IsShown
		{
			get
			{
				return _IsShown;
			}
			set
			{
				_IsShown = value;
				base.gameObject.SetActive(_IsShown);
			}
		}

		internal virtual void Awake()
		{
		}

		internal virtual void OnPull(float power)
		{
		}

		internal virtual void OnRefreshed(bool autoHide)
		{
			if (autoHide)
			{
				IsShown = false;
			}
		}

		internal virtual void OnRefreshCancelled()
		{
			IsShown = false;
		}
	}
}
