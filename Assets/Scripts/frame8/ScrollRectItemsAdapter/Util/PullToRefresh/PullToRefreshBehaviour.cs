using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util.PullToRefresh
{
	public class PullToRefreshBehaviour : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		[Serializable]
		public class UnityEventFloat : UnityEvent<float>
		{
		}

		[SerializeField]
		[Range(0.1f, 1f)]
		[Tooltip("The normalized distance relative to screen size. Always between 0 and 1")]
		private float _PullAmountNormalized = 0.25f;

		[SerializeField]
		[Tooltip("If null, will try to GetComponentInChildren()")]
		private PullToRefreshGizmo _RefreshGizmo;

		[SerializeField]
		[Tooltip("If false, you'll need to call HideGizmo() manually after pull. Subscribe to PullToRefreshBehaviour.OnRefresh event to know when a refresh event occurred")]
		private bool _AutoHideRefreshGizmo = true;

		[SerializeField]
		private AudioClip _SoundOnPreRefresh;

		[SerializeField]
		private AudioClip _SoundOnRefresh;

		public UnityEvent OnRefresh;

		public UnityEventFloat OnPullProgress;

		private ScrollRect _ScrollRect;

		private float _ResolvedAVGScreenSize;

		private bool _PlayedPreSoundForCurrentDrag;

		private void Awake()
		{
			_ResolvedAVGScreenSize = (float)(Screen.width + Screen.height) / 2f;
			_ScrollRect = GetComponent<ScrollRect>();
			_RefreshGizmo = GetComponentInChildren<PullToRefreshGizmo>();
		}

		private void Update()
		{
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			ShowGizmo();
			_PlayedPreSoundForCurrentDrag = false;
		}

		public void OnDrag(PointerEventData eventData)
		{
			float dragAmountNormalized = GetDragAmountNormalized(eventData);
			if ((bool)_RefreshGizmo)
			{
				_RefreshGizmo.OnPull(dragAmountNormalized);
			}
			if (OnPullProgress != null)
			{
				OnPullProgress.Invoke(dragAmountNormalized);
			}
			if (dragAmountNormalized >= 1f && !_PlayedPreSoundForCurrentDrag)
			{
				_PlayedPreSoundForCurrentDrag = true;
				if ((bool)_SoundOnPreRefresh)
				{
					AudioSource.PlayClipAtPoint(_SoundOnPreRefresh, Camera.main.transform.position);
				}
			}
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			float dragAmountNormalized = GetDragAmountNormalized(eventData);
			if (dragAmountNormalized >= 1f)
			{
				if (OnRefresh != null)
				{
					OnRefresh.Invoke();
				}
				if ((bool)_RefreshGizmo)
				{
					_RefreshGizmo.OnRefreshed(_AutoHideRefreshGizmo);
				}
				if ((bool)_SoundOnRefresh)
				{
					AudioSource.PlayClipAtPoint(_SoundOnRefresh, Camera.main.transform.position);
				}
			}
			else if ((bool)_RefreshGizmo)
			{
				_RefreshGizmo.OnRefreshCancelled();
			}
			if (_AutoHideRefreshGizmo)
			{
				HideGizmo();
			}
		}

		public void ShowGizmo()
		{
			if ((bool)_RefreshGizmo)
			{
				_RefreshGizmo.IsShown = true;
			}
		}

		public void HideGizmo()
		{
			if ((bool)_RefreshGizmo)
			{
				_RefreshGizmo.IsShown = false;
			}
		}

		private float GetDragAmountNormalized(PointerEventData eventData)
		{
			float f;
			if (_ScrollRect.vertical)
			{
				if (_ScrollRect.verticalNormalizedPosition < 1f)
				{
					return 0f;
				}
				Vector2 pressPosition = eventData.pressPosition;
				float y = pressPosition.y;
				Vector2 position = eventData.position;
				f = Mathf.Abs(y - position.y);
			}
			else
			{
				if (_ScrollRect.verticalNormalizedPosition > 0f)
				{
					return 0f;
				}
				Vector2 pressPosition2 = eventData.pressPosition;
				float x = pressPosition2.x;
				Vector2 position2 = eventData.position;
				f = Mathf.Abs(x - position2.x);
			}
			return Mathf.Abs(f) / (_PullAmountNormalized * _ResolvedAVGScreenSize);
		}
	}
}
