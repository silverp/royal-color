using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class SimpleImageDownloader : MonoBehaviour
	{
		public class Request
		{
			public string url;

			public Action<Result> onDone;

			public Action onError;
		}

		public class Result
		{
			private WWW _UsedWWW;

			internal Result(WWW www)
			{
				_UsedWWW = www;
			}

			public Texture2D CreateTextureFromReceivedData()
			{
				return _UsedWWW.texture;
			}

			public void LoadTextureInto(Texture2D existingTexture)
			{
				_UsedWWW.LoadImageIntoTexture(existingTexture);
			}
		}

		private static SimpleImageDownloader _Instance;

		private const int DEFAULT_MAX_CONCURRENT_REQUESTS = 20;

		private List<Request> _QueuedRequests = new List<Request>();

		private List<Request> _ExecutingRequests = new List<Request>();

		private WaitForSeconds _Wait1Sec = new WaitForSeconds(1f);

		public static SimpleImageDownloader Instance
		{
			get
			{
				if (_Instance == null)
				{
					_Instance = new GameObject(typeof(SimpleImageDownloader).Name).AddComponent<SimpleImageDownloader>();
				}
				return _Instance;
			}
		}

		public int MaxConcurrentRequests
		{
			get;
			set;
		}

		private IEnumerator Start()
		{
			if (MaxConcurrentRequests == 0)
			{
				MaxConcurrentRequests = 20;
			}
			while (true)
			{
				if (_ExecutingRequests.Count >= MaxConcurrentRequests)
				{
					yield return _Wait1Sec;
					continue;
				}
				int lastIndex = _QueuedRequests.Count - 1;
				if (lastIndex >= 0)
				{
					Request request = _QueuedRequests[lastIndex];
					_QueuedRequests.RemoveAt(lastIndex);
					StartCoroutine(DownloadCoroutine(request));
				}
				yield return null;
			}
		}

		public void Enqueue(Request request)
		{
			_QueuedRequests.Add(request);
		}

		private IEnumerator DownloadCoroutine(Request request)
		{
			_ExecutingRequests.Add(request);
			WWW www = new WWW(request.url);
			yield return www;
			if (string.IsNullOrEmpty(www.error))
			{
				if (request.onDone != null)
				{
					Result obj = new Result(www);
					request.onDone(obj);
				}
			}
			else if (request.onError != null)
			{
				request.onError();
			}
			www.Dispose();
			_ExecutingRequests.Remove(request);
		}
	}
}
