using UnityEngine;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class ExpandCollapseOnClick : MonoBehaviour
	{
		public interface ISizeChangesHandler
		{
			bool HandleSizeChangeRequest(RectTransform rt, float newSize);

			void OnExpandedStateChanged(RectTransform rt, bool expanded);
		}

		[Tooltip("will be taken from this object, if not specified")]
		public Button button;

		public float expandFactor = 2f;

		public float animDuration = 0.2f;

		[HideInInspector]
		public float nonExpandedSize = -1f;

		[HideInInspector]
		public bool expanded;

		private float startSize;

		private float endSize;

		private float animStart;

		private bool animating;

		private RectTransform rectTransform;

		public ISizeChangesHandler sizeChangesHandler;

		private void Awake()
		{
			rectTransform = (base.transform as RectTransform);
			if (button == null)
			{
				button = GetComponent<Button>();
			}
			button.onClick.AddListener(OnClicked);
		}

		public void OnClicked()
		{
			if (!animating && !(nonExpandedSize < 0f))
			{
				animating = true;
				animStart = Time.time;
				if (expanded)
				{
					startSize = nonExpandedSize * expandFactor;
					endSize = nonExpandedSize;
				}
				else
				{
					startSize = nonExpandedSize;
					endSize = nonExpandedSize * expandFactor;
				}
			}
		}

		private void Update()
		{
			if (!animating)
			{
				return;
			}
			float num = Time.time - animStart;
			float num2 = num / animDuration;
			if (num2 >= 1f)
			{
				num2 = 1f;
				animating = false;
			}
			float newSize = Mathf.Lerp(startSize, endSize, num2);
			if (sizeChangesHandler != null)
			{
				if (!sizeChangesHandler.HandleSizeChangeRequest(rectTransform, newSize))
				{
					animating = false;
				}
				if (!animating)
				{
					expanded = !expanded;
					sizeChangesHandler.OnExpandedStateChanged(rectTransform, expanded);
				}
			}
		}
	}
}
