using frame8.Logic.Misc.Other.Extensions;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class RectTransformEdgeDragger : MonoBehaviour, IDragHandler, IPointerDownHandler, IEventSystemHandler
	{
		public RectTransform draggedRectTransform;

		public RectTransform.Edge draggedEdge;

		public float maxDistance = 100f;

		private RectTransform rt;

		private Vector2 startDragPos;

		private Vector2 initialPos;

		private float startInset;

		private float startSize;

		public event Action TargetDragged;

		private void Awake()
		{
			rt = (base.transform as RectTransform);
		}

		private void Start()
		{
			initialPos = rt.position;
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			startDragPos = base.transform.position;
			startInset = draggedRectTransform.GetInsetFromParentEdge(draggedRectTransform.parent as RectTransform, draggedEdge);
			if (draggedEdge == RectTransform.Edge.Left || draggedEdge == RectTransform.Edge.Right)
			{
				startSize = draggedRectTransform.rect.width;
			}
			else
			{
				startSize = draggedRectTransform.rect.height;
			}
		}

		public void OnDrag(PointerEventData ped)
		{
			Vector2 vector = ped.position - ped.pressPosition;
			Vector2 v = startDragPos;
			float num;
			if (draggedEdge == RectTransform.Edge.Left || draggedEdge == RectTransform.Edge.Right)
			{
				num = vector.x;
				v.x += num;
				float f = v.x - initialPos.x;
				if (Mathf.Abs(f) > maxDistance)
				{
					return;
				}
			}
			else
			{
				num = vector.y;
				v.y += num;
				float f = v.y - initialPos.y;
				if (Mathf.Abs(f) > maxDistance)
				{
					return;
				}
			}
			rt.position = v;
			draggedRectTransform.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(draggedEdge, startInset + num, startSize - num);
			if (this.TargetDragged != null)
			{
				this.TargetDragged();
			}
		}
	}
}
