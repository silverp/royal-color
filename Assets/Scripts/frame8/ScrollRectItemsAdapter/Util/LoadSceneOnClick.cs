using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class LoadSceneOnClick : MonoBehaviour
	{
		public string sceneName;

		private void Start()
		{
			GetComponent<Button>().onClick.AddListener(LoadScene);
		}

		private void LoadScene()
		{
			SceneManager.LoadScene(sceneName);
		}
	}
}
