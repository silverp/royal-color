using UnityEngine;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class ShowOnlyOnPortraitOrientation : MonoBehaviour
	{
		public GameObject target;

		private void Update()
		{
			if (Screen.height > Screen.width)
			{
				if (!target.activeSelf)
				{
					target.SetActive(value: true);
				}
			}
			else if (target.activeSelf)
			{
				target.SetActive(value: false);
			}
		}
	}
}
