using UnityEngine;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util
{
	public class SimpleImageColorBouncer : MonoBehaviour
	{
		public Color a;

		public Color b;

		[Range(0.001f, 10f)]
		[SerializeField]
		private float _PeriodInSeconds = 0.3f;

		private Graphic _Graphic;

		private void Start()
		{
			_Graphic = GetComponent<Graphic>();
		}

		private void Update()
		{
			_Graphic.color = Color.Lerp(a, b, (Mathf.Sin(Time.time / _PeriodInSeconds) + 1f) / 2f);
		}
	}
}
