using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util
{
	[RequireComponent(typeof(Graphic))]
	public class LongClickableItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, ICancelHandler, IEventSystemHandler
	{
		public interface IItemLongClickListener
		{
			void OnItemLongClicked(LongClickableItem longClickedItem);
		}

		public float longClickTime = 0.7f;

		public IItemLongClickListener longClickListener;

		private float _PressedTime;

		private bool _Pressing;

		private void Update()
		{
			if (_Pressing && Time.time - _PressedTime >= longClickTime)
			{
				_Pressing = false;
				if (longClickListener != null)
				{
					longClickListener.OnItemLongClicked(this);
				}
			}
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			_Pressing = true;
			_PressedTime = Time.time;
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			_Pressing = false;
		}

		public void OnCancel(BaseEventData eventData)
		{
			_Pressing = false;
		}
	}
}
