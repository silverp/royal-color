using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util.GridView
{
	[Serializable]
	public class GridParams : BaseParams
	{
		[Header("Grid")]
		public RectTransform cellPrefab;

		[Tooltip("The max. number of cells in a row group (for vertical ScrollView) or column group (for horizontal ScrollView)")]
		public int numCellsPerGroup;

		public TextAnchor alignmentOfCellsInGroup;

		public RectOffset groupPadding;

		public bool cellWidthForceExpandInGroup;

		public bool cellHeightForceExpandInGroup;

		private HorizontalOrVerticalLayoutGroup _TheOnlyGroupPrefab;

		private int NumCellsPerGroupHorizontally => (!scrollRect.horizontal) ? 1 : numCellsPerGroup;

		private int NumCellsPerGroupVertically => (!scrollRect.horizontal) ? 1 : numCellsPerGroup;

		public virtual HorizontalOrVerticalLayoutGroup GetGroupPrefab(int forGroupAtThisIndex)
		{
			if (_TheOnlyGroupPrefab == null)
			{
				GameObject gameObject = new GameObject(scrollRect.name + "_GridAdapter_GroupPrefab");
				gameObject.SetActive(value: false);
				gameObject.transform.SetParent(scrollRect.transform, worldPositionStays: false);
				if (scrollRect.horizontal)
				{
					_TheOnlyGroupPrefab = gameObject.AddComponent<VerticalLayoutGroup>();
				}
				else
				{
					_TheOnlyGroupPrefab = gameObject.AddComponent<HorizontalLayoutGroup>();
				}
				_TheOnlyGroupPrefab.spacing = contentSpacing;
				_TheOnlyGroupPrefab.childForceExpandWidth = cellWidthForceExpandInGroup;
				_TheOnlyGroupPrefab.childForceExpandHeight = cellHeightForceExpandInGroup;
				_TheOnlyGroupPrefab.childAlignment = alignmentOfCellsInGroup;
				_TheOnlyGroupPrefab.padding = groupPadding;
			}
			return _TheOnlyGroupPrefab;
		}

		public virtual float GetGroupWidth()
		{
			return (float)groupPadding.left + (cellPrefab.rect.width + contentSpacing) * (float)NumCellsPerGroupHorizontally - contentSpacing + (float)groupPadding.right;
		}

		public virtual float GetGroupHeight()
		{
			return (float)groupPadding.top + (cellPrefab.rect.height + contentSpacing) * (float)NumCellsPerGroupVertically - contentSpacing + (float)groupPadding.bottom;
		}

		public virtual int GetGroupIndex(int cellIndex)
		{
			return cellIndex / numCellsPerGroup;
		}

		public virtual int GetNumberOfRequiredGroups(int numberOfCells)
		{
			return (numberOfCells != 0) ? (GetGroupIndex(numberOfCells - 1) + 1) : 0;
		}
	}
}
