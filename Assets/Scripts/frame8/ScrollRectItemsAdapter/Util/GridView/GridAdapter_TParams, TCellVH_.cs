using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;

namespace frame8.ScrollRectItemsAdapter.Util.GridView
{
	public abstract class GridAdapter<TParams, TCellVH> : ScrollRectItemsAdapter8<TParams, CellGroupViewsHolder<TCellVH>> where TParams : GridParams where TCellVH : CellViewsHolder, new()
	{
		protected int _CellsCount;

		public override void ChangeItemCountTo(int cellsCount)
		{
			_CellsCount = cellsCount;
			int numberOfRequiredGroups = _Params.GetNumberOfRequiredGroups(_CellsCount);
			base.ChangeItemCountTo(numberOfRequiredGroups);
		}

		public virtual int GetNumVisibleCells()
		{
			if (_VisibleItemsCount == 0)
			{
				return 0;
			}
			return (_VisibleItemsCount - 1) * _Params.numCellsPerGroup + _VisibleItems[_VisibleItemsCount - 1].NumActiveCells;
		}

		public virtual TCellVH GetCellViewsHolder(int cellViewHolderIndex)
		{
			if (_VisibleItemsCount == 0)
			{
				return (TCellVH)null;
			}
			if (cellViewHolderIndex > GetNumVisibleCells() - 1)
			{
				return (TCellVH)null;
			}
			return _VisibleItems[_Params.GetGroupIndex(cellViewHolderIndex)].ContainingCellViewHolders[cellViewHolderIndex % _Params.numCellsPerGroup];
		}

		public virtual TCellVH GetCellViewsHolderIfVisible(int withCellItemIndex)
		{
			CellGroupViewsHolder<TCellVH> itemViewsHolderIfVisible = GetItemViewsHolderIfVisible(_Params.GetGroupIndex(withCellItemIndex));
			if (itemViewsHolderIfVisible == null)
			{
				return (TCellVH)null;
			}
			int num = itemViewsHolderIfVisible.itemIndex * _Params.numCellsPerGroup;
			if (withCellItemIndex < num + itemViewsHolderIfVisible.NumActiveCells)
			{
				return itemViewsHolderIfVisible.ContainingCellViewHolders[withCellItemIndex - num];
			}
			return (TCellVH)null;
		}

		protected override float GetItemHeight(int groupIndex)
		{
			return _Params.GetGroupHeight();
		}

		protected override float GetItemWidth(int groupIndex)
		{
			return _Params.GetGroupWidth();
		}

		protected override CellGroupViewsHolder<TCellVH> CreateViewsHolder(int itemIndex)
		{
			CellGroupViewsHolder<TCellVH> cellGroupViewsHolder = new CellGroupViewsHolder<TCellVH>();
			cellGroupViewsHolder.Init(_Params.GetGroupPrefab(itemIndex).gameObject, itemIndex, _Params.cellPrefab, _Params.numCellsPerGroup);
			return cellGroupViewsHolder;
		}

		protected override void UpdateViewsHolder(CellGroupViewsHolder<TCellVH> newOrRecycled)
		{
			int num2;
			if (newOrRecycled.itemIndex + 1 == GetItemCount())
			{
				int num = 0;
				if (newOrRecycled.itemIndex > 0)
				{
					num = newOrRecycled.itemIndex * _Params.numCellsPerGroup;
				}
				num2 = _CellsCount - num;
			}
			else
			{
				num2 = _Params.numCellsPerGroup;
			}
			newOrRecycled.NumActiveCells = num2;
			for (int i = 0; i < num2; i++)
			{
				UpdateCellViewsHolder(newOrRecycled.ContainingCellViewHolders[i]);
			}
		}

		protected abstract void UpdateCellViewsHolder(TCellVH viewHolder);
	}
}
