using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace frame8.ScrollRectItemsAdapter.Util.GridView
{
	public abstract class CellViewsHolder : AbstractViewHolder
	{
		public RectTransform views;

		public override void Init(GameObject rootPrefabGO, int itemIndex, bool activateRootGameObject = true, bool callCollectViews = true)
		{
			throw new InvalidOperationException("A cell cannot be initialized this way. Use InitWithExistingRootPrefab(RectTransform) instead");
		}

		public virtual void InitWithExistingRootPrefab(RectTransform root)
		{
			base.root = root;
			itemIndex = -1;
			CollectViews();
		}

		public override void CollectViews()
		{
			base.CollectViews();
			views = GetViews();
			if (views == root)
			{
				throw new UnityException("CellViewsHolder: views == root not allowed: you should have a child of root that holds all the views, as the root should always be enabled for layouting purposes");
			}
		}

		public override void MarkForRebuild()
		{
			base.MarkForRebuild();
			if ((bool)views)
			{
				LayoutRebuilder.MarkLayoutForRebuild(views);
			}
		}

		protected abstract RectTransform GetViews();
	}
}
