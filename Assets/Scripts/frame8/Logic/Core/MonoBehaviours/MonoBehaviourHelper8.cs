using System;
using System.Collections;
using UnityEngine;

namespace frame8.Logic.Core.MonoBehaviours
{
	public class MonoBehaviourHelper8 : MonoBehaviour
	{
		private Action updateAction;

		public static MonoBehaviourHelper8 CreateInstance(Action updateAction, Transform parent = null, string name = "MonoBehaviourHelper8")
		{
			MonoBehaviourHelper8 monoBehaviourHelper = new GameObject(name).AddComponent<MonoBehaviourHelper8>();
			monoBehaviourHelper.updateAction = updateAction;
			if ((bool)parent)
			{
				monoBehaviourHelper.transform.parent = parent;
			}
			return monoBehaviourHelper;
		}

		public void CallDelayedByFrames(Action action, int afterFrames)
		{
			StartCoroutine(DelayedCallByFrames(action, afterFrames));
		}

		private IEnumerator DelayedCallByFrames(Action action, int afterFrames)
		{
			while (true)
			{
				int num;
				afterFrames = (num = afterFrames) - 1;
				if (num <= 0)
				{
					break;
				}
				yield return null;
			}
			action?.Invoke();
			yield return null;
		}

		private void Update()
		{
			if (updateAction != null)
			{
				updateAction();
			}
		}

		public void Dispose()
		{
			if ((bool)base.gameObject)
			{
				try
				{
					try
					{
						base.gameObject.SetActive(value: false);
						StopAllCoroutines();
					}
					catch
					{
					}
					UnityEngine.Object.Destroy(base.gameObject);
				}
				catch
				{
				}
			}
		}
	}
}
