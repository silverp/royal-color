using frame8.Logic.Core.MonoBehaviours;
using frame8.Logic.Misc.Other.Extensions;
using frame8.Logic.Misc.Visual.UI.MonoBehaviours;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter
{
	public abstract class ScrollRectItemsAdapter8<TParams, TItemViewsHolder> : OnScreenSizeChangedEventDispatcher.IOnScreenSizeChangedListener where TParams : BaseParams where TItemViewsHolder : BaseItemViewsHolder
	{
		private class InternalState
		{
			internal readonly Vector2 constantAnchorPosForAllItems = new Vector2(0f, 1f);

			internal float viewportSize;

			internal float paddingContentStart;

			internal float transversalPaddingContentStart;

			internal float paddingContentEnd;

			internal float paddingStartPlusEnd;

			internal float spacing;

			internal RectTransform.Edge startEdge;

			internal RectTransform.Edge endEdge;

			internal RectTransform.Edge transvStartEdge;

			internal float itemsConstantTransversalSize;

			internal int itemsCount;

			internal float lastProcessedAbstractNormalizedScrollPosition;

			internal int realIndexOfFirstItemInView;

			internal List<TItemViewsHolder> recyclableItems = new List<TItemViewsHolder>();

			internal int recyclableItemsCount;

			internal float[] itemsSizes;

			internal float[] itemsSizesCumulative;

			internal float cumulatedSizesOfAllItemsPlusSpacing;

			internal float contentPanelSize;

			internal bool onScrollPositionChangedFiredAndVisibilityComputedForCurrentItems;

			internal bool updateRequestPending;

			internal int maxVisibleItemsSeenSinceLastScrollViewSizeChange;

			internal bool layoutRebuildPendingDueToScreenSizeChangeEvent;

			internal bool computeVisibilityTwinPassScheduled;

			private TParams _SourceParams;

			private Func<int, float> _GetItemSizeFunc;

			private Func<int, float> getItemOffsetFunction;

			private Func<TItemViewsHolder, float> getItemSizeFunction;

			private InternalState(TParams sourceParams, ScrollRectItemsAdapter8<TParams, TItemViewsHolder> adapter)
			{
				_SourceParams = sourceParams;
				HorizontalOrVerticalLayoutGroup component = sourceParams.content.GetComponent<HorizontalOrVerticalLayoutGroup>();
				if ((bool)component && component.enabled)
				{
					component.enabled = false;
					UnityEngine.Debug.Log("LayoutGroup on GameObject " + component.name + " has beed disabled in order to use ScrollRectItemsAdapter8");
				}
				ContentSizeFitter component2 = sourceParams.content.GetComponent<ContentSizeFitter>();
				if ((bool)component2 && component2.enabled)
				{
					component2.enabled = false;
					UnityEngine.Debug.Log("ContentSizeFitter on GameObject " + component2.name + " has beed disabled in order to use ScrollRectItemsAdapter8");
				}
				LayoutElement component3 = sourceParams.content.GetComponent<LayoutElement>();
				if ((bool)component3)
				{
					UnityEngine.Object.Destroy(component3);
					UnityEngine.Debug.Log("LayoutElement on GameObject " + component2.name + " has beed DESTROYED in order to use ScrollRectItemsAdapter8");
				}
				if (sourceParams.scrollRect.horizontal)
				{
					startEdge = RectTransform.Edge.Left;
					endEdge = RectTransform.Edge.Right;
					transvStartEdge = RectTransform.Edge.Top;
					_GetItemSizeFunc = ((int i) => adapter.GetItemWidth(i));
				}
				else
				{
					startEdge = RectTransform.Edge.Top;
					endEdge = RectTransform.Edge.Bottom;
					transvStartEdge = RectTransform.Edge.Left;
					_GetItemSizeFunc = ((int i) => adapter.GetItemHeight(i));
				}
				CacheScrollViewInfo();
			}

			internal static InternalState CreateFromSourceParamsOrThrow(TParams sourceParams, ScrollRectItemsAdapter8<TParams, TItemViewsHolder> adapter)
			{
				if (sourceParams.scrollRect.horizontal && sourceParams.scrollRect.vertical)
				{
					throw new UnityException("Can't optimize a ScrollRect with both horizontal and vertical scrolling modes. Disable one of them");
				}
				return new InternalState(sourceParams, adapter);
			}

			internal void CacheScrollViewInfo()
			{
				RectTransform viewport = _SourceParams.viewport;
				Rect rect = viewport.rect;
				if (_SourceParams.scrollRect.horizontal)
				{
					viewportSize = rect.width;
					paddingContentStart = _SourceParams.contentPadding.left;
					paddingContentEnd = _SourceParams.contentPadding.right;
					transversalPaddingContentStart = _SourceParams.contentPadding.top;
					itemsConstantTransversalSize = _SourceParams.content.rect.height - (transversalPaddingContentStart + (float)_SourceParams.contentPadding.bottom);
				}
				else
				{
					viewportSize = rect.height;
					paddingContentStart = _SourceParams.contentPadding.top;
					paddingContentEnd = _SourceParams.contentPadding.bottom;
					transversalPaddingContentStart = _SourceParams.contentPadding.left;
					itemsConstantTransversalSize = _SourceParams.content.rect.width - (transversalPaddingContentStart + (float)_SourceParams.contentPadding.right);
				}
				paddingStartPlusEnd = paddingContentStart + paddingContentEnd;
				spacing = _SourceParams.contentSpacing;
			}

			private void AssureItemsSizesArrayCapacity()
			{
				if (itemsSizes == null || itemsSizes.Length != itemsCount)
				{
					itemsSizes = new float[itemsCount];
				}
				if (itemsSizesCumulative == null || itemsSizesCumulative.Length != itemsCount)
				{
					itemsSizesCumulative = new float[itemsCount];
				}
			}

			private float CollectSizesOfAllItems()
			{
				AssureItemsSizesArrayCapacity();
				float num = 0f;
				for (int i = 0; i < itemsCount; i++)
				{
					int itemRealIndexFromViewIndex = GetItemRealIndexFromViewIndex(i);
					float num2 = _GetItemSizeFunc(itemRealIndexFromViewIndex);
					itemsSizes[i] = num2;
					num += num2;
					itemsSizesCumulative[i] = num;
				}
				return num;
			}

			internal void OnItemsCountChanged(int itemsNewCount, bool contentPanelEndEdgeStationary)
			{
				realIndexOfFirstItemInView = ((itemsNewCount <= 0) ? (-1) : 0);
				itemsCount = itemsNewCount;
				OnTotalSizeOfAllItemsChanged(CollectSizesOfAllItems(), contentPanelEndEdgeStationary);
				onScrollPositionChangedFiredAndVisibilityComputedForCurrentItems = false;
				computeVisibilityTwinPassScheduled = false;
			}

			internal float ChangeItemSizeAndUpdateContentSizeAccordingly(TItemViewsHolder viewHolder, float requestedSize, bool itemEndEdgeStationary, bool rebuild = true)
			{
				if (itemsSizes == null)
				{
					throw new UnityException("Wait for initialization first");
				}
				if (viewHolder.root == null)
				{
					throw new UnityException("God bless: shouldn't happen if implemented according to documentation/examples");
				}
				RectTransform.Edge fixedEdge;
				float newInset;
				if (itemEndEdgeStationary)
				{
					fixedEdge = endEdge;
					newInset = GetItemOffsetFromParentEndUsingItemIndexInView(viewHolder.itemIndexInView);
				}
				else
				{
					fixedEdge = startEdge;
					newInset = GetItemOffsetFromParentStartUsingItemIndexInView(viewHolder.itemIndexInView);
				}
				viewHolder.root.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_SourceParams.content, fixedEdge, newInset, requestedSize);
				float num = viewHolder.cachedSize = ((!_SourceParams.scrollRect.horizontal) ? viewHolder.root.rect.height : viewHolder.root.rect.width);
				float num2 = 0f;
				for (int i = 0; i < itemsCount; i++)
				{
					int itemRealIndexFromViewIndex = GetItemRealIndexFromViewIndex(i);
					float num3 = (i != viewHolder.itemIndexInView) ? _GetItemSizeFunc(itemRealIndexFromViewIndex) : num;
					itemsSizes[i] = num3;
					num2 += num3;
					itemsSizesCumulative[i] = num2;
				}
				OnTotalSizeOfAllItemsChanged(num2, itemEndEdgeStationary, rebuild);
				return num;
			}

			internal void OnItemsSizesChangedExternally(List<TItemViewsHolder> viewHolders, bool itemEndEdgeStationary)
			{
				int count = viewHolders.Count;
				RectTransform.Edge fixedEdge = (!itemEndEdgeStationary) ? startEdge : endEdge;
				if (_SourceParams.scrollRect.horizontal)
				{
					getItemSizeFunction = ((TItemViewsHolder v) => v.root.rect.width);
				}
				else
				{
					getItemSizeFunction = ((TItemViewsHolder v) => v.root.rect.height);
				}
				float num = 0f;
				if (count > 0)
				{
					int itemIndexInView = viewHolders[0].itemIndexInView;
					if (itemIndexInView > 0)
					{
						num = itemsSizesCumulative[itemIndexInView - 1];
					}
					for (int i = 0; i < count; i++)
					{
						TItemViewsHolder val = viewHolders[i];
						int itemIndexInView2 = val.itemIndexInView;
						float num2 = val.cachedSize = getItemSizeFunction(val);
						itemsSizes[itemIndexInView2] = num2;
						num += num2;
						itemsSizesCumulative[itemIndexInView2] = num;
					}
					for (int j = count; j < itemsCount; j++)
					{
						itemsSizesCumulative[j] = itemsSizesCumulative[j - 1] + itemsSizes[j];
					}
					if (itemEndEdgeStationary)
					{
						getItemOffsetFunction = GetItemOffsetFromParentEndUsingItemIndexInView;
					}
					else
					{
						getItemOffsetFunction = GetItemOffsetFromParentStartUsingItemIndexInView;
					}
					if (itemIndexInView > 0)
					{
						num = itemsSizesCumulative[itemIndexInView - 1];
					}
					else
					{
						num = 0f;
					}
					for (int k = 0; k < count; k++)
					{
						TItemViewsHolder val = viewHolders[k];
						int itemIndexInView2 = val.itemIndexInView;
						float newInset = getItemOffsetFunction(itemIndexInView2);
						val.root.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_SourceParams.content, fixedEdge, newInset, itemsSizes[itemIndexInView2]);
					}
				}
				OnTotalSizeOfAllItemsChanged((itemsCount <= 0) ? 0f : itemsSizesCumulative[itemsCount - 1], itemEndEdgeStationary);
			}

			internal void UpdateLastProcessedNormalizedScrollPosition()
			{
				lastProcessedAbstractNormalizedScrollPosition = _SourceParams.GetAbstractNormalizedScrollPosition();
			}

			internal void ResetLastProcessedNormalizedScrollPositionToStart()
			{
				lastProcessedAbstractNormalizedScrollPosition = 1f;
			}

			internal void ResetLastProcessedNormalizedScrollPositionToEnd()
			{
				lastProcessedAbstractNormalizedScrollPosition = 0f;
			}

			internal float GetItemOffsetFromParentStartUsingItemIndexInView(int itemIndexInView)
			{
				float num = 0f;
				if (itemIndexInView > 0)
				{
					num = itemsSizesCumulative[itemIndexInView - 1] + (float)itemIndexInView * spacing;
				}
				return paddingContentStart + num;
			}

			internal float GetItemOffsetFromParentEndUsingItemIndexInView(int itemIndexInView)
			{
				return contentPanelSize - (GetItemOffsetFromParentStartUsingItemIndexInView(itemIndexInView) + itemsSizes[itemIndexInView]);
			}

			internal int GetItemRealIndexFromViewIndex(int indexInView)
			{
				return (realIndexOfFirstItemInView + indexInView) % itemsCount;
			}

			internal int GetItemViewIndexFromRealIndex(int realIndex)
			{
				return (realIndex - realIndexOfFirstItemInView + itemsCount) % itemsCount;
			}

			internal void OnScrollViewLooped(int newValueOf_RealIndexOfFirstItemInView)
			{
				int num = realIndexOfFirstItemInView;
				realIndexOfFirstItemInView = newValueOf_RealIndexOfFirstItemInView;
				int num2 = num - realIndexOfFirstItemInView;
				if (num2 != 0)
				{
					itemsSizes = itemsSizes.GetRotatedArray(num2);
					float num3 = 0f;
					for (int i = 0; i < itemsCount; i++)
					{
						num3 += itemsSizes[i];
						itemsSizesCumulative[i] = num3;
					}
				}
			}

			private void OnTotalSizeOfAllItemsChanged(float cumulatedSizeOfAllItems, bool contentPanelEndEdgeStationary, bool rebuild = true)
			{
				cumulatedSizesOfAllItemsPlusSpacing = cumulatedSizeOfAllItems + (float)Mathf.Max(0, itemsCount - 1) * spacing;
				OnCumulatedSizesOfAllItemsPlusSpacingChanged(cumulatedSizesOfAllItemsPlusSpacing, contentPanelEndEdgeStationary, rebuild);
			}

			private void OnCumulatedSizesOfAllItemsPlusSpacingChanged(float newValue, bool contentPanelEndEdgeStationary, bool rebuild = true)
			{
				contentPanelSize = cumulatedSizesOfAllItemsPlusSpacing + paddingStartPlusEnd;
				RectTransform.Edge edge = (!contentPanelEndEdgeStationary) ? startEdge : endEdge;
				float insetFromParentEdge = _SourceParams.content.GetInsetFromParentEdge(_SourceParams.viewport, edge);
				_SourceParams.content.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_SourceParams.viewport, edge, insetFromParentEdge, contentPanelSize);
				if (rebuild)
				{
					_SourceParams.scrollRect.Rebuild(CanvasUpdate.PostLayout);
					Canvas.ForceUpdateCanvases();
				}
			}
		}

		protected TParams _Params;

		protected List<TItemViewsHolder> _VisibleItems;

		protected int _VisibleItemsCount;

		private InternalState _InternalState;

		private MonoBehaviourHelper8 _MonoBehaviourHelper;

		private Coroutine _SmoothScrollCoroutine;

		public TParams Parameters => _Params;

		public int VisibleItemsCount => _VisibleItemsCount;

		public event Action<int, int> ItemsRefreshed;

		public void Init(TParams parms)
		{
			if (_Params != null)
			{
				Dispose();
			}
			_Params = parms;
			_Params.InitIfNeeded();
			_InternalState = InternalState.CreateFromSourceParamsOrThrow(_Params, this);
			_VisibleItems = new List<TItemViewsHolder>();
			_MonoBehaviourHelper = MonoBehaviourHelper8.CreateInstance(MyUpdate, _Params.scrollRect.transform, "SRIA-Helper");
			_MonoBehaviourHelper.gameObject.AddComponent<OnScreenSizeChangedEventDispatcher>().RegisterListenerManually(this);
			ChangeItemCountInternal(0, contentPanelEndEdgeStationary: false);
			_Params.ScrollToStart();
			_InternalState.ResetLastProcessedNormalizedScrollPositionToStart();
			_Params.scrollRect.onValueChanged.AddListener(OnScrollPositionChanged);
		}

		public void ScrollToStart()
		{
			_Params.ScrollToStart();
		}

		public virtual void Refresh()
		{
			ChangeItemCountTo(_InternalState.itemsCount);
		}

		public virtual void ChangeItemCountTo(int itemsCount)
		{
			ChangeItemCountTo(itemsCount, contentPanelEndEdgeStationary: false);
		}

		public void ChangeItemCountTo(int itemsCount, bool contentPanelEndEdgeStationary)
		{
			ChangeItemCountInternal(itemsCount, contentPanelEndEdgeStationary);
		}

		public int GetItemCount()
		{
			return _InternalState.itemsCount;
		}

		public TItemViewsHolder GetItemViewsHolder(int viewHolderIndex)
		{
			if (viewHolderIndex >= _VisibleItemsCount)
			{
				return (TItemViewsHolder)null;
			}
			return _VisibleItems[viewHolderIndex];
		}

		public TItemViewsHolder GetItemViewsHolderIfVisible(int withItemIndex)
		{
			int num = 0;
			for (num = 0; num < _VisibleItemsCount; num++)
			{
				TItemViewsHolder result = _VisibleItems[num];
				int itemIndex = result.itemIndex;
				if (itemIndex == withItemIndex)
				{
					return result;
				}
			}
			return (TItemViewsHolder)null;
		}

		public TItemViewsHolder GetItemViewsHolderIfVisible(RectTransform withRoot)
		{
			for (int i = 0; i < _VisibleItemsCount; i++)
			{
				TItemViewsHolder val = _VisibleItems[i];
				if (val.root == withRoot)
				{
					return val;
				}
			}
			return (TItemViewsHolder)null;
		}

		public void ScrollTo(int itemIndex, float normalizedOffsetFromViewportStart = 0f, float normalizedPositionOfItemPivotToUse = 0f)
		{
			float num = _InternalState.viewportSize - _InternalState.contentPanelSize;
			if (!(num >= 0f))
			{
				SetContentStartOffsetFromViewportStart(ClampContentStartOffsetFromViewportStart(num, itemIndex, normalizedOffsetFromViewportStart, normalizedPositionOfItemPivotToUse));
			}
		}

		public bool SmoothScrollTo(int itemIndex, float duration, float normalizedOffsetFromViewportStart = 0f, float normalizedPositionOfItemPivotToUse = 0f, Func<float, bool> onProgress = null)
		{
			if (_SmoothScrollCoroutine != null)
			{
				return false;
			}
			_SmoothScrollCoroutine = _MonoBehaviourHelper.StartCoroutine(SmoothScrollProgressCoroutine(itemIndex, duration, normalizedOffsetFromViewportStart, normalizedPositionOfItemPivotToUse, onProgress));
			return true;
		}

		private IEnumerator SmoothScrollProgressCoroutine(int itemIndex, float duration, float normalizedOffsetFromViewportStart = 0f, float normalizedPositionOfItemPivotToUse = 0f, Func<float, bool> onProgress = null)
		{
			float minContentOffsetFromVPAllowed = _InternalState.viewportSize - _InternalState.contentPanelSize;
			if (minContentOffsetFromVPAllowed >= 0f)
			{
				_SmoothScrollCoroutine = null;
				onProgress?.Invoke(1f);
				yield break;
			}
			Canvas.ForceUpdateCanvases();
			_Params.scrollRect.StopMovement();
			float initialInsetFromParent = _Params.content.GetInsetFromParentEdge(_Params.viewport, _InternalState.startEdge);
			float targetInsetFromParent = ClampContentStartOffsetFromViewportStart(minContentOffsetFromVPAllowed, itemIndex, normalizedOffsetFromViewportStart, normalizedPositionOfItemPivotToUse);
			float startTime = Time.time;
			WaitForEndOfFrame endOfFrame = new WaitForEndOfFrame();
			bool notCanceled = true;
			float progress;
			do
			{
				yield return null;
				yield return endOfFrame;
				float elapsedTime = Time.time - startTime;
				progress = ((!(elapsedTime >= duration)) ? Mathf.Sin(elapsedTime / duration * (float)Math.PI / 2f) : 1f);
				float value = Mathf.Lerp(initialInsetFromParent, targetInsetFromParent, progress);
				SetContentStartOffsetFromViewportStart(value);
				if (onProgress != null)
				{
					bool flag;
					notCanceled = (flag = onProgress(progress));
					if (!flag)
					{
						break;
					}
				}
			}
			while (progress < 1f);
			if (notCanceled)
			{
				ScrollTo(itemIndex, normalizedOffsetFromViewportStart, normalizedPositionOfItemPivotToUse);
			}
			_SmoothScrollCoroutine = null;
		}

		public void SetContentStartOffsetFromViewportStart(float offset)
		{
			SetContentEdgeOffsetFromViewportEdge(_InternalState.startEdge, offset);
		}

		public void SetContentEndOffsetFromViewportEnd(float offset)
		{
			SetContentEdgeOffsetFromViewportEdge(_InternalState.endEdge, offset);
		}

		public float RequestChangeItemSizeAndUpdateLayout(TItemViewsHolder withViewHolder, float requestedSize, bool itemEndEdgeStationary = false)
		{
			_Params.scrollRect.StopMovement();
			int num = _Params.scrollRect.vertical ? 1 : 0;
			int num2 = -num * 2 + 1;
			float num3 = _InternalState.itemsSizes[withViewHolder.itemIndexInView];
			float num4 = _InternalState.ChangeItemSizeAndUpdateContentSizeAccordingly(withViewHolder, requestedSize, itemEndEdgeStationary);
			float num5 = num4 - num3;
			int num6 = 0;
			int num7 = _VisibleItems.IndexOf(withViewHolder);
			for (num6 = num7 + 1; num6 < _VisibleItemsCount; num6++)
			{
				TItemViewsHolder val = _VisibleItems[num6];
				Vector3 localPosition = val.root.localPosition;
				int index;
				localPosition[index = num] = localPosition[index] + (float)num2 * num5;
				val.root.localPosition = localPosition;
			}
			return num4;
		}

		public float GetItemOffsetFromParentStart(int itemIndex)
		{
			return _InternalState.GetItemOffsetFromParentStartUsingItemIndexInView(_InternalState.GetItemViewIndexFromRealIndex(itemIndex));
		}

		public virtual void NotifyScrollViewSizeChanged()
		{
			_InternalState.layoutRebuildPendingDueToScreenSizeChangeEvent = true;
			ChangeItemCountInternal(_InternalState.itemsCount, contentPanelEndEdgeStationary: false);
		}

		public virtual void Dispose()
		{
			if (_Params != null && (bool)_Params.scrollRect)
			{
				_Params.scrollRect.onValueChanged.RemoveListener(OnScrollPositionChanged);
			}
			if (_SmoothScrollCoroutine != null)
			{
				_SmoothScrollCoroutine = null;
			}
			if ((bool)_MonoBehaviourHelper)
			{
				_MonoBehaviourHelper.Dispose();
				_MonoBehaviourHelper = null;
			}
			ClearCachedRecyclableItems();
			ClearVisibleItems();
			_VisibleItems = null;
			_Params = (TParams)null;
			_InternalState = null;
			if (this.ItemsRefreshed != null)
			{
				this.ItemsRefreshed = null;
			}
		}

		protected abstract float GetItemHeight(int index);

		protected abstract float GetItemWidth(int index);

		protected abstract TItemViewsHolder CreateViewsHolder(int itemIndex);

		protected abstract void UpdateViewsHolder(TItemViewsHolder newOrRecycled);

		protected virtual bool IsRecyclable(TItemViewsHolder potentiallyRecyclable, int indexOfItemThatWillBecomeVisible, float heightOfItemThatWillBecomeVisible)
		{
			return true;
		}

		protected virtual void ClearCachedRecyclableItems()
		{
			if (_InternalState != null && _InternalState.recyclableItems != null)
			{
				foreach (TItemViewsHolder recyclableItem in _InternalState.recyclableItems)
				{
					if (recyclableItem != null && recyclableItem.root != null)
					{
						try
						{
							UnityEngine.Object.Destroy(recyclableItem.root.gameObject);
						}
						catch (Exception exception)
						{
							UnityEngine.Debug.LogException(exception);
						}
					}
				}
				_InternalState.recyclableItems.Clear();
				_InternalState.recyclableItemsCount = 0;
			}
		}

		protected virtual void ClearVisibleItems()
		{
			if (_VisibleItems != null)
			{
				foreach (TItemViewsHolder visibleItem in _VisibleItems)
				{
					if (visibleItem != null && visibleItem.root != null)
					{
						try
						{
							UnityEngine.Object.Destroy(visibleItem.root.gameObject);
						}
						catch (Exception exception)
						{
							UnityEngine.Debug.LogException(exception);
						}
					}
				}
				_VisibleItems.Clear();
				_VisibleItemsCount = 0;
			}
		}

		protected virtual void RebuildLayoutDueToScrollViewSizeChange()
		{
			MarkViewHoldersForRebuild(_VisibleItems);
			MarkViewHoldersForRebuild(_InternalState.recyclableItems);
			Canvas.ForceUpdateCanvases();
			_InternalState.CacheScrollViewInfo();
			_InternalState.maxVisibleItemsSeenSinceLastScrollViewSizeChange = 0;
		}

		protected virtual void OnItemHeightChangedPreTwinPass(TItemViewsHolder viewHolder)
		{
		}

		protected virtual void OnItemWidthChangedPreTwinPass(TItemViewsHolder viewHolder)
		{
		}

		protected void ScheduleComputeVisibilityTwinPass()
		{
			if (_Params.updateMode != BaseParams.UpdateMode.MONOBEHAVIOUR_UPDATE)
			{
				throw new UnityException("Twin pass is only possible if updateMode is " + BaseParams.UpdateMode.MONOBEHAVIOUR_UPDATE);
			}
			_InternalState.computeVisibilityTwinPassScheduled = true;
		}

		private void MarkViewHoldersForRebuild(List<TItemViewsHolder> vhs)
		{
			if (vhs != null)
			{
				foreach (TItemViewsHolder vh in vhs)
				{
					TItemViewsHolder current = vh;
					if (current != null && current.root != null)
					{
						current.MarkForRebuild();
					}
				}
			}
		}

		private float ClampContentStartOffsetFromViewportStart(float minContentOffsetFromVPAllowed, int itemIndex, float normalizedOffsetFromStart, float normalizedPositionOfItemPivotToUse)
		{
			float val = 0f;
			int itemViewIndexFromRealIndex = _InternalState.GetItemViewIndexFromRealIndex(itemIndex);
			float num = _InternalState.itemsSizes[itemViewIndexFromRealIndex];
			float num2 = _InternalState.viewportSize * normalizedOffsetFromStart - num * normalizedPositionOfItemPivotToUse;
			return Mathf.Max(minContentOffsetFromVPAllowed, Math.Min(val, 0f - GetItemOffsetFromParentStart(itemIndex) + num2));
		}

		private void ChangeItemCountInternal(int itemsCount, bool contentPanelEndEdgeStationary)
		{
			int itemsCount2 = _InternalState.itemsCount;
			_Params.scrollRect.StopMovement();
			_InternalState.OnItemsCountChanged(itemsCount, contentPanelEndEdgeStationary);
			if (GetNumExcessObjects() > 0)
			{
				throw new UnityException("ChangeItemCountInternal: GetNumExcessObjects() > 0 when calling ChangeItemCountInternal(); this may be due ComputeVisibility not being finished executing yet");
			}
			_InternalState.recyclableItems.AddRange(_VisibleItems);
			_InternalState.recyclableItemsCount += _VisibleItemsCount;
			if (itemsCount == 0)
			{
				ClearCachedRecyclableItems();
			}
			_VisibleItems.Clear();
			_VisibleItemsCount = 0;
			_InternalState.updateRequestPending = true;
			if (this.ItemsRefreshed != null)
			{
				this.ItemsRefreshed(itemsCount2, itemsCount);
			}
		}

		private void MyUpdate()
		{
			if (_InternalState.updateRequestPending)
			{
				_InternalState.updateRequestPending = (_Params.updateMode != BaseParams.UpdateMode.ON_SCROLL);
				ComputeVisibilityForCurrentPosition();
			}
			else if (_InternalState.computeVisibilityTwinPassScheduled)
			{
				ComputeVisibilityForCurrentPosition();
			}
		}

		private void OnScrollPositionChanged(Vector2 pos)
		{
			if (_Params.updateMode != BaseParams.UpdateMode.MONOBEHAVIOUR_UPDATE)
			{
				ComputeVisibilityForCurrentPosition();
			}
			if (_Params.updateMode != BaseParams.UpdateMode.ON_SCROLL)
			{
				_InternalState.updateRequestPending = true;
			}
			_InternalState.onScrollPositionChangedFiredAndVisibilityComputedForCurrentItems = true;
		}

		private void ComputeVisibilityForCurrentPosition()
		{
			float abstractNormalizedScrollPosition = _Params.GetAbstractNormalizedScrollPosition();
			float num = abstractNormalizedScrollPosition - _InternalState.lastProcessedAbstractNormalizedScrollPosition;
			bool flag = false;
			if (_InternalState.layoutRebuildPendingDueToScreenSizeChangeEvent)
			{
				RebuildLayoutDueToScrollViewSizeChange();
				_InternalState.layoutRebuildPendingDueToScreenSizeChangeEvent = false;
			}
			else if (_InternalState.computeVisibilityTwinPassScheduled)
			{
				ForceSetPointerEventDistanceToZero(throwExceptionIfImpossible: false);
				if (_Params.scrollRect.horizontal)
				{
					for (int i = 0; i < _VisibleItemsCount; i++)
					{
						OnItemWidthChangedPreTwinPass(_VisibleItems[i]);
					}
				}
				else
				{
					for (int j = 0; j < _VisibleItemsCount; j++)
					{
						OnItemHeightChangedPreTwinPass(_VisibleItems[j]);
					}
				}
				_InternalState.OnItemsSizesChangedExternally(_VisibleItems, num > 0f);
				flag = true;
			}
			_InternalState.computeVisibilityTwinPassScheduled = false;
			if (_Params.loopItems)
			{
				LoopIfNeeded(num, abstractNormalizedScrollPosition);
			}
			ComputeVisibility(num);
			if (!flag)
			{
				_InternalState.lastProcessedAbstractNormalizedScrollPosition = ((!_Params.loopItems) ? abstractNormalizedScrollPosition : _Params.GetAbstractNormalizedScrollPosition());
			}
		}

		private void ForceSetPointerEventDistanceToZero(bool throwExceptionIfImpossible)
		{
			if (throwExceptionIfImpossible && !(EventSystem.current.currentInputModule is PointerInputModule))
			{
				throw new InvalidOperationException("Cannot use looping with if the current input module does not inherit from PointerInputModule");
			}
			PointerInputModule pointerInputModule = EventSystem.current.currentInputModule as PointerInputModule;
			Dictionary<int, PointerEventData> dictionary = pointerInputModule.GetType().GetField("m_PointerData", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(pointerInputModule) as Dictionary<int, PointerEventData>;
			foreach (PointerEventData value in dictionary.Values)
			{
				if (value.pointerDrag == _Params.scrollRect.gameObject)
				{
					value.pointerPressRaycast = value.pointerCurrentRaycast;
					value.pressPosition = value.position;
					value.delta = Vector2.zero;
					value.dragging = false;
					value.scrollDelta = Vector2.zero;
					break;
				}
			}
		}

		private void LoopIfNeeded(float delta, float curPos)
		{
			if (_VisibleItems.Count <= 0)
			{
				return;
			}
			int num = 0;
			if (delta > 0f && curPos > 0.95f)
			{
				num = 2;
			}
			else
			{
				if (!(delta < 0f) || !(curPos < 0.05f))
				{
					return;
				}
				num = 1;
			}
			Vector2 velocity = _Params.scrollRect.velocity;
			int itemIndexInView = _VisibleItems[0].itemIndexInView;
			int num2 = itemIndexInView + _VisibleItemsCount - 1;
			ForceSetPointerEventDistanceToZero(throwExceptionIfImpossible: true);
			float newInset;
			RectTransform.Edge fixedEdge;
			if (num == 1)
			{
				if (itemIndexInView == 0)
				{
					return;
				}
				float itemOffsetFromParentStartUsingItemIndexInView = _InternalState.GetItemOffsetFromParentStartUsingItemIndexInView(itemIndexInView);
				float insetFromParentEdge = _Params.content.GetInsetFromParentEdge(_Params.viewport, _InternalState.startEdge);
				float num3 = 0f - insetFromParentEdge - itemOffsetFromParentStartUsingItemIndexInView;
				float num4 = 0f - (num3 + _InternalState.paddingContentStart);
				newInset = num4;
				fixedEdge = _InternalState.startEdge;
			}
			else
			{
				if (num2 + 1 >= _InternalState.itemsCount)
				{
					return;
				}
				float itemOffsetFromParentStartUsingItemIndexInView2 = _InternalState.GetItemOffsetFromParentStartUsingItemIndexInView(num2);
				float num5 = _InternalState.itemsSizes[num2];
				float num6 = _InternalState.contentPanelSize - (itemOffsetFromParentStartUsingItemIndexInView2 + num5);
				float insetFromParentEdge2 = _Params.content.GetInsetFromParentEdge(_Params.viewport, _InternalState.endEdge);
				float num7 = 0f - insetFromParentEdge2 - num6;
				float num8 = 0f - (num7 + _InternalState.paddingContentEnd);
				newInset = num8;
				fixedEdge = _InternalState.endEdge;
			}
			_Params.content.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_Params.viewport, fixedEdge, newInset, _InternalState.contentPanelSize);
			_Params.scrollRect.Rebuild(CanvasUpdate.PostLayout);
			_Params.scrollRect.velocity = velocity;
			int newValueOf_RealIndexOfFirstItemInView;
			if (num == 1)
			{
				TItemViewsHolder val = _VisibleItems[0];
				newValueOf_RealIndexOfFirstItemInView = val.itemIndex;
				for (int i = 0; i < _VisibleItemsCount; i++)
				{
					_VisibleItems[i].itemIndexInView = i;
				}
			}
			else
			{
				newValueOf_RealIndexOfFirstItemInView = _InternalState.GetItemRealIndexFromViewIndex(num2 + 1);
				for (int j = 0; j < _VisibleItemsCount; j++)
				{
					_VisibleItems[j].itemIndexInView = _InternalState.itemsCount - _VisibleItemsCount + j;
				}
			}
			_InternalState.OnScrollViewLooped(newValueOf_RealIndexOfFirstItemInView);
			for (int k = 0; k < _VisibleItemsCount; k++)
			{
				TItemViewsHolder val2 = _VisibleItems[k];
				float num9 = _InternalState.paddingContentStart + (float)val2.itemIndexInView * _InternalState.spacing;
				if (val2.itemIndexInView > 0)
				{
					num9 += _InternalState.itemsSizesCumulative[val2.itemIndexInView - 1];
				}
				val2.root.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_Params.content, _InternalState.startEdge, num9, _InternalState.itemsSizes[val2.itemIndexInView]);
			}
			_InternalState.UpdateLastProcessedNormalizedScrollPosition();
		}

		private void ComputeVisibility(float abstractDelta)
		{
			bool flag = abstractDelta <= 0f;
			float viewportSize = _InternalState.viewportSize;
			float spacing = _InternalState.spacing;
			float transversalPaddingContentStart = _InternalState.transversalPaddingContentStart;
			float itemsConstantTransversalSize = _InternalState.itemsConstantTransversalSize;
			TItemViewsHolder val = (TItemViewsHolder)null;
			RectTransform.Edge transvStartEdge = _InternalState.transvStartEdge;
			int num;
			RectTransform.Edge edge;
			if (flag)
			{
				num = 1;
				edge = _InternalState.startEdge;
			}
			else
			{
				num = -1;
				edge = _InternalState.endEdge;
			}
			int num2 = (num + 1) / 2;
			int num3 = 1 - num2;
			int num4 = num3 * (_InternalState.itemsCount - 1) - num;
			int num5 = num2 * (_InternalState.itemsCount - 1);
			float insetFromParentEdge = _Params.content.GetInsetFromParentEdge(_Params.viewport, edge);
			if (_VisibleItemsCount > 0)
			{
				int index = num2 * (_VisibleItemsCount - 1);
				TItemViewsHolder val2 = _VisibleItems[index];
				num4 = val2.itemIndexInView;
				do
				{
					int index2 = num3 * (_VisibleItemsCount - 1);
					TItemViewsHolder val3 = _VisibleItems[index2];
					float num6 = _InternalState.itemsSizes[val3.itemIndexInView] + spacing;
					float num7 = (!flag) ? _InternalState.GetItemOffsetFromParentEndUsingItemIndexInView(val3.itemIndexInView) : _InternalState.GetItemOffsetFromParentStartUsingItemIndexInView(val3.itemIndexInView);
					if (insetFromParentEdge + (num7 + num6) <= 0f)
					{
						_InternalState.recyclableItems.Add(val3);
						_VisibleItems.RemoveAt(index2);
						_VisibleItemsCount--;
						_InternalState.recyclableItemsCount++;
						continue;
					}
					break;
				}
				while (_VisibleItemsCount != 0);
			}
			while (num4 != num5)
			{
				int num8 = num4;
				bool flag2 = false;
				float num9;
				float num10;
				while (true)
				{
					num8 += num;
					num9 = ((!flag) ? _InternalState.GetItemOffsetFromParentEndUsingItemIndexInView(num8) : _InternalState.GetItemOffsetFromParentStartUsingItemIndexInView(num8));
					num10 = _InternalState.itemsSizes[num8];
					if (insetFromParentEdge + (num9 + num10) <= 0f)
					{
						if (num8 == num5)
						{
							flag2 = true;
							break;
						}
						continue;
					}
					if (insetFromParentEdge + num9 > viewportSize)
					{
						flag2 = true;
					}
					break;
				}
				if (flag2)
				{
					break;
				}
				int itemRealIndexFromViewIndex = _InternalState.GetItemRealIndexFromViewIndex(num8);
				int num11 = 0;
				while (true)
				{
					if (num11 < _InternalState.recyclableItemsCount)
					{
						TItemViewsHolder val4 = _InternalState.recyclableItems[num11];
						if (IsRecyclable(val4, itemRealIndexFromViewIndex, num10))
						{
							_InternalState.recyclableItems.RemoveAt(num11);
							_InternalState.recyclableItemsCount--;
							val = val4;
							break;
						}
						num11++;
						continue;
					}
					val = CreateViewsHolder(itemRealIndexFromViewIndex);
					break;
				}
				_VisibleItems.Insert(num2 * _VisibleItemsCount, val);
				_VisibleItemsCount++;
				val.itemIndex = itemRealIndexFromViewIndex;
				val.itemIndexInView = num8;
				val.cachedSize = _InternalState.itemsSizes[num8];
				RectTransform root = val.root;
				root.SetParent(_Params.content, worldPositionStays: false);
				UpdateViewsHolder(val);
				val.root.gameObject.SetActive(value: true);
				Vector2 vector2 = root.anchorMin = (root.anchorMax = _InternalState.constantAnchorPosForAllItems);
				root.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_Params.content, edge, num9, num10);
				root.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_Params.content, transvStartEdge, transversalPaddingContentStart, itemsConstantTransversalSize);
				num4 = num8;
			}
			if (_VisibleItemsCount > _InternalState.maxVisibleItemsSeenSinceLastScrollViewSizeChange)
			{
				_InternalState.maxVisibleItemsSeenSinceLastScrollViewSizeChange = _VisibleItemsCount;
			}
			int num12 = 0;
			while (num12 < _InternalState.recyclableItemsCount)
			{
				GameObject gameObject = _InternalState.recyclableItems[num12].root.gameObject;
				gameObject.SetActive(value: false);
				if (GetNumExcessObjects() > 0)
				{
					UnityEngine.Object.Destroy(gameObject);
					_InternalState.recyclableItems.RemoveAt(num12);
					_InternalState.recyclableItemsCount--;
				}
				else
				{
					num12++;
				}
			}
		}

		private int GetNumExcessObjects()
		{
			if (_InternalState.recyclableItemsCount > 1)
			{
				int num = _InternalState.recyclableItemsCount + _VisibleItemsCount - GetMinNumObjectsToKeepInMemory();
				if (num > 0)
				{
					return num;
				}
			}
			return 0;
		}

		private int GetMinNumObjectsToKeepInMemory()
		{
			return (_Params.minNumberOfObjectsToKeepInMemory <= 0) ? (_InternalState.maxVisibleItemsSeenSinceLastScrollViewSizeChange + 1) : _Params.minNumberOfObjectsToKeepInMemory;
		}

		private void SetContentEdgeOffsetFromViewportEdge(RectTransform.Edge contentAndViewportEdge, float offset)
		{
			Canvas.ForceUpdateCanvases();
			_Params.scrollRect.StopMovement();
			_Params.content.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(_Params.viewport, contentAndViewportEdge, offset, _InternalState.contentPanelSize);
			_InternalState.CacheScrollViewInfo();
		}

		void OnScreenSizeChangedEventDispatcher.IOnScreenSizeChangedListener.OnScreenSizeChanged(float lastWidth, float lastHeight, float width, float height)
		{
			NotifyScrollViewSizeChanged();
		}
	}
}
