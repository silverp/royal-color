using UnityEngine;
using UnityEngine.UI;

namespace frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter
{
	public abstract class AbstractViewHolder
	{
		public RectTransform root;

		public virtual int itemIndex
		{
			get;
			set;
		}

		public virtual void Init(RectTransform rootPrefab, int itemIndex, bool activateRootGameObject = true, bool callCollectViews = true)
		{
			Init(rootPrefab.gameObject, itemIndex, activateRootGameObject, callCollectViews);
		}

		public virtual void Init(GameObject rootPrefabGO, int itemIndex, bool activateRootGameObject = true, bool callCollectViews = true)
		{
			root = (Object.Instantiate(rootPrefabGO).transform as RectTransform);
			if (activateRootGameObject)
			{
				root.gameObject.SetActive(value: true);
			}
			this.itemIndex = itemIndex;
			if (callCollectViews)
			{
				CollectViews();
			}
		}

		public virtual void CollectViews()
		{
		}

		public virtual void MarkForRebuild()
		{
			if ((bool)root)
			{
				LayoutRebuilder.MarkLayoutForRebuild(root);
			}
		}
	}
}
