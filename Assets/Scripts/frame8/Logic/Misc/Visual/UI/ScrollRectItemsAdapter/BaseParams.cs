using frame8.Logic.Misc.Other.Extensions;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter
{
	[Serializable]
	public class BaseParams
	{
		public enum UpdateMode
		{
			ON_SCROLL_THEN_MONOBEHAVIOUR_UPDATE,
			ON_SCROLL,
			MONOBEHAVIOUR_UPDATE
		}

		[Header("Optimizing Process")]
		[Tooltip("How much objects to always keep in memory, no matter what. This includes visible items + items in the recycle bin.  The recycle bin will always have at least one item in it, regardless of this setting. Set to -1 or 0 to Detect automatically (Recommended!). Change it only if you know what you're doing (usually, it's the estimated number of visible items + 1). Last note: this field will only be considered after the number <visible+in the bin> grows past it")]
		public int minNumberOfObjectsToKeepInMemory = -1;

		[Tooltip("See BaseParams.UpdateMode enum for full description. The default is ON_SCROLL_THEN_MONOBEHAVIOUR_UPDATE and if the framerate is acceptable, it should be leaved this way")]
		public UpdateMode updateMode;

		[Tooltip("If true: When the last item is reached, the first one appears after it, basically allowing you to scroll infinitely.\n Initially intended for things like spinners, but it can be used for anything alike.\n It may interfere with other functionalities in some very obscure/complex contexts/setups, so be sure to test the hell out of it.\n Also please note that sometimes during dragging the content, the actual looping changes the Unity's internal PointerEventData for the current click/touch pointer id, so if you're also externally tracking the current click/touch, in this case only 'PointerEventData.pointerCurrentRaycast' and 'PointerEventData.position'(current position) are preserved, the other ones are reset to defaults to assure a smooth loop transition. Sorry for the long decription. Here's an ASCII potato: (@)")]
		public bool loopItems;

		[Space(10f, order = 0)]
		public ScrollRect scrollRect;

		[Tooltip("If null, the scrollRect is considered to be the viewport")]
		public RectTransform viewport;

		[Tooltip("If null, will be the same as scrollRect.content")]
		public RectTransform content;

		[Tooltip("This is used instead of the old way of putting a disabled LayoutGroup component on the content")]
		public RectOffset contentPadding = new RectOffset();

		[Tooltip("This is used instead of the old way of putting a disabled LayoutGroup component on the content")]
		public float contentSpacing;

		public BaseParams()
		{
		}

		public BaseParams(BaseParams other)
		{
			minNumberOfObjectsToKeepInMemory = other.minNumberOfObjectsToKeepInMemory;
			updateMode = other.updateMode;
			scrollRect = other.scrollRect;
			viewport = other.viewport;
			content = other.content;
			contentPadding = ((other.contentPadding != null) ? new RectOffset(contentPadding.left, contentPadding.right, contentPadding.top, contentPadding.bottom) : new RectOffset());
			contentSpacing = other.contentSpacing;
		}

		public BaseParams(ScrollRect scrollRect)
			: this(scrollRect, scrollRect.transform as RectTransform, scrollRect.content)
		{
		}

		public BaseParams(ScrollRect scrollRect, RectTransform viewport, RectTransform content)
		{
			this.scrollRect = scrollRect;
			this.viewport = (viewport ?? (scrollRect.transform as RectTransform));
			this.content = (content ?? scrollRect.content);
		}

		internal void InitIfNeeded()
		{
			if (viewport == null)
			{
				viewport = (scrollRect.transform as RectTransform);
			}
			if (content == null)
			{
				content = scrollRect.content;
			}
		}

		internal float GetAbstractNormalizedScrollPosition()
		{
			if (scrollRect.horizontal)
			{
				float width = viewport.rect.width;
				float width2 = content.rect.width;
				if (width > width2)
				{
					return content.GetInsetFromParentLeftEdge(viewport) / width;
				}
				return 1f - scrollRect.horizontalNormalizedPosition;
			}
			float height = viewport.rect.height;
			float height2 = content.rect.height;
			if (height > height2)
			{
				return content.GetInsetFromParentTopEdge(viewport) / height;
			}
			return scrollRect.verticalNormalizedPosition;
		}

		internal float TransformVelocityToAbstract(Vector2 rawVelocity)
		{
			if (scrollRect.horizontal)
			{
				rawVelocity.x = 0f - rawVelocity.x;
				return rawVelocity.x;
			}
			return rawVelocity.y;
		}

		internal void ScrollToStart()
		{
			if (scrollRect.horizontal)
			{
				scrollRect.horizontalNormalizedPosition = 0f;
			}
			else
			{
				scrollRect.verticalNormalizedPosition = 1f;
			}
		}

		internal void ScrollToEnd()
		{
			if (scrollRect.horizontal)
			{
				scrollRect.horizontalNormalizedPosition = 1f;
			}
			else
			{
				scrollRect.verticalNormalizedPosition = 0f;
			}
		}

		internal void ClampScroll01()
		{
			if (scrollRect.horizontal)
			{
				scrollRect.horizontalNormalizedPosition = Mathf.Clamp01(scrollRect.horizontalNormalizedPosition);
			}
			else
			{
				scrollRect.verticalNormalizedPosition = Mathf.Clamp01(scrollRect.verticalNormalizedPosition);
			}
		}
	}
}
