using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace frame8.Logic.Misc.Visual.UI.MonoBehaviours
{
	[RequireComponent(typeof(Scrollbar))]
	public class ScrollbarFixer8 : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
	{
		public bool hideWhenNotNeeded = true;

		public bool autoHide = true;

		[Tooltip("Used if autoHide is on. Duration in seconds")]
		public float autoHideTime = 1f;

		[Range(0.01f, 1f)]
		public float minSize = 0.1f;

		[Range(0.1f, 2f)]
		public float sizeUpdateInterval = 0.2f;

		[Tooltip("If not assigned, will try yo find one in the parent")]
		public ScrollRect scrollRect;

		[Tooltip("If not assigned, will use the resolved scrollRect")]
		public RectTransform viewport;

		private RectTransform _ScrollRectRT;

		private RectTransform _ViewPortRT;

		private Scrollbar _Scrollbar;

		private bool _HorizontalScrollBar;

		private Vector3 _ScaleBeforeHide = Vector3.one;

		private bool _Hidden;

		private bool _AutoHidden;

		private bool _HiddenNotNeeded;

		private float _LastValue;

		private float _TimeOnLastValueChange;

		private bool _Dragging;

		private Coroutine _SlowUpdateCoroutine;

		private void Awake()
		{
			_Scrollbar = GetComponent<Scrollbar>();
			_LastValue = _Scrollbar.value;
			_TimeOnLastValueChange = Time.time;
			_HorizontalScrollBar = (_Scrollbar.direction == Scrollbar.Direction.LeftToRight || _Scrollbar.direction == Scrollbar.Direction.RightToLeft);
			if (!scrollRect)
			{
				scrollRect = GetComponentInParent<ScrollRect>();
			}
			if ((bool)scrollRect)
			{
				_ScrollRectRT = (scrollRect.transform as RectTransform);
				if (!viewport)
				{
					viewport = _ScrollRectRT;
				}
				if (_HorizontalScrollBar)
				{
					if (!scrollRect.horizontal)
					{
						throw new UnityException("Can't use horizontal scrollbar with non-horizontal scrollRect");
					}
					if ((bool)scrollRect.horizontalScrollbar)
					{
						UnityEngine.Debug.Log("ScrollbarFixer8: setting scrollRect.horizontalScrollbar to null (the whole point of using ScrollbarFixer8 is to NOT have any scrollbars assigned)");
						scrollRect.horizontalScrollbar = null;
					}
					if (scrollRect.verticalScrollbar == _Scrollbar)
					{
						UnityEngine.Debug.Log("ScrollbarFixer8: Can't use the same scrollbar for both vert and hor");
						scrollRect.verticalScrollbar = null;
					}
				}
				else
				{
					if (!scrollRect.vertical)
					{
						throw new UnityException("Can't use vertical scrollbar with non-vertical scrollRect");
					}
					if ((bool)scrollRect.verticalScrollbar)
					{
						UnityEngine.Debug.Log("ScrollbarFixer8: setting scrollRect.verticalScrollbar to null (the whole point of using ScrollbarFixer8 is to NOT have any scrollbars assigned)");
						scrollRect.verticalScrollbar = null;
					}
					if (scrollRect.horizontalScrollbar == _Scrollbar)
					{
						UnityEngine.Debug.Log("ScrollbarFixer8: Can't use the same scrollbar for both vert and hor");
						scrollRect.horizontalScrollbar = null;
					}
				}
			}
			else
			{
				UnityEngine.Debug.LogError("No ScrollRect assigned!");
			}
		}

		private void OnEnable()
		{
			_Dragging = false;
			_SlowUpdateCoroutine = StartCoroutine(SlowUpdate());
		}

		private void OnDisable()
		{
			StopCoroutine(_SlowUpdateCoroutine);
		}

		private IEnumerator SlowUpdate()
		{
			WaitForSeconds wait1Sec = new WaitForSeconds(sizeUpdateInterval);
			while (true)
			{
				yield return wait1Sec;
				if (!base.enabled)
				{
					break;
				}
				if (!_ScrollRectRT || !scrollRect.content)
				{
					continue;
				}
				float num;
				float num2;
				if (_HorizontalScrollBar)
				{
					num = scrollRect.content.rect.width;
					num2 = viewport.rect.width;
				}
				else
				{
					num = scrollRect.content.rect.height;
					num2 = viewport.rect.height;
				}
				float num3 = (!(num <= 0f) && num != float.NaN && num != float.Epsilon && num != float.NegativeInfinity && num != float.PositiveInfinity) ? Mathf.Clamp(num2 / num, minSize, 1f) : 1f;
				_Scrollbar.size = num3;
				if (hideWhenNotNeeded)
				{
					if (num3 == 1f)
					{
						if (!_Hidden)
						{
							_HiddenNotNeeded = true;
							Hide();
						}
					}
					else if (_Hidden && !_AutoHidden)
					{
						Show();
					}
				}
				else if (!autoHide && _Hidden)
				{
					Show();
				}
			}
		}

		private void Hide()
		{
			_Hidden = true;
			_ScaleBeforeHide = base.gameObject.transform.localScale;
			base.gameObject.transform.localScale = Vector3.zero;
		}

		private void Show()
		{
			base.gameObject.transform.localScale = _ScaleBeforeHide;
			_HiddenNotNeeded = (_AutoHidden = (_Hidden = false));
		}

		private void Update()
		{
			if (!scrollRect)
			{
				return;
			}
			if (_Dragging)
			{
				_TimeOnLastValueChange = Time.time;
				return;
			}
			float num;
			if (scrollRect.vertical)
			{
				Vector2 normalizedPosition = scrollRect.normalizedPosition;
				num = normalizedPosition.y;
			}
			else
			{
				Vector2 normalizedPosition2 = scrollRect.normalizedPosition;
				num = normalizedPosition2.x;
			}
			_Scrollbar.value = num;
			if (autoHide)
			{
				if (num == _LastValue)
				{
					if (!_Hidden && Time.time - _TimeOnLastValueChange >= autoHideTime)
					{
						_AutoHidden = true;
						Hide();
					}
					return;
				}
				_TimeOnLastValueChange = Time.time;
				_LastValue = num;
				if (_Hidden && !_HiddenNotNeeded)
				{
					Show();
				}
			}
			else if (!hideWhenNotNeeded && _Hidden)
			{
				Show();
			}
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			_Dragging = true;
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			_Dragging = false;
		}

		public void OnDrag(PointerEventData eventData)
		{
			scrollRect.StopMovement();
			Vector2 normalizedPosition = scrollRect.normalizedPosition;
			if (_HorizontalScrollBar)
			{
				normalizedPosition.x = _Scrollbar.value;
			}
			else
			{
				normalizedPosition.y = _Scrollbar.value;
			}
			scrollRect.normalizedPosition = normalizedPosition;
		}
	}
}
