using System;
using System.Collections.Generic;
using UnityEngine;

namespace frame8.Logic.Misc.Visual.UI.MonoBehaviours
{
	public class OnScreenSizeChangedEventDispatcher : MonoBehaviour
	{
		public interface IOnScreenSizeChangedListener
		{
			void OnScreenSizeChanged(float lastWidth, float lastHeight, float width, float height);
		}

		private float _LastScreenW;

		private float _LastScreenH;

		private IOnScreenSizeChangedListener[] _Listeners = new IOnScreenSizeChangedListener[0];

		private List<IOnScreenSizeChangedListener> _ManuallyRegisteredListeners = new List<IOnScreenSizeChangedListener>();

		private void Start()
		{
			_LastScreenW = Screen.width;
			_LastScreenH = Screen.height;
			_Listeners = Array.ConvertAll(GetComponents(typeof(IOnScreenSizeChangedListener)), (Component c) => (IOnScreenSizeChangedListener)c);
		}

		private void Update()
		{
			float num = Screen.width;
			float num2 = Screen.height;
			if (num != _LastScreenW || num2 != _LastScreenH)
			{
				IOnScreenSizeChangedListener[] listeners = _Listeners;
				for (int i = 0; i < listeners.Length; i++)
				{
					listeners[i]?.OnScreenSizeChanged(_LastScreenW, _LastScreenH, num, num2);
				}
				_LastScreenW = num;
				_LastScreenH = num2;
			}
			if (_ManuallyRegisteredListeners.Count > 0)
			{
				_ManuallyRegisteredListeners.AddRange(_Listeners);
				_Listeners = _ManuallyRegisteredListeners.ToArray();
				_ManuallyRegisteredListeners.Clear();
			}
		}

		public void RegisterListenerManually(IOnScreenSizeChangedListener listener)
		{
			_ManuallyRegisteredListeners.Add(listener);
		}
	}
}
