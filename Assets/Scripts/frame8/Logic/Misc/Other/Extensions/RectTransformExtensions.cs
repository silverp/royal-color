using UnityEngine;

namespace frame8.Logic.Misc.Other.Extensions
{
	public static class RectTransformExtensions
	{
		public static float GetWorldTop(this RectTransform rt)
		{
			Vector3 position = rt.position;
			float y = position.y;
			Vector2 pivot = rt.pivot;
			return y + (1f - pivot.y) * rt.rect.height;
		}

		public static float GetWorldBottom(this RectTransform rt)
		{
			Vector3 position = rt.position;
			float y = position.y;
			Vector2 pivot = rt.pivot;
			return y - pivot.y * rt.rect.height;
		}

		public static float GetWorldLeft(this RectTransform rt)
		{
			Vector3 position = rt.position;
			float x = position.x;
			Vector2 pivot = rt.pivot;
			return x - pivot.x * rt.rect.width;
		}

		public static float GetWorldRight(this RectTransform rt)
		{
			Vector3 position = rt.position;
			float x = position.x;
			Vector2 pivot = rt.pivot;
			return x + (1f - pivot.x) * rt.rect.width;
		}

		public static float GetInsetFromParentTopEdge(this RectTransform child, RectTransform parentHint)
		{
			Vector2 pivot = parentHint.pivot;
			float num = (1f - pivot.y) * parentHint.rect.height;
			Vector3 localPosition = child.localPosition;
			float y = localPosition.y;
			return num - child.rect.yMax - y;
		}

		public static float GetInsetFromParentBottomEdge(this RectTransform child, RectTransform parentHint)
		{
			Vector2 pivot = parentHint.pivot;
			float num = pivot.y * parentHint.rect.height;
			Vector3 localPosition = child.localPosition;
			float y = localPosition.y;
			return num + child.rect.yMin + y;
		}

		public static float GetInsetFromParentLeftEdge(this RectTransform child, RectTransform parentHint)
		{
			Vector2 pivot = parentHint.pivot;
			float num = pivot.x * parentHint.rect.width;
			Vector3 localPosition = child.localPosition;
			float x = localPosition.x;
			return num + child.rect.xMin + x;
		}

		public static float GetInsetFromParentRightEdge(this RectTransform child, RectTransform parentHint)
		{
			Vector2 pivot = parentHint.pivot;
			float num = (1f - pivot.x) * parentHint.rect.width;
			Vector3 localPosition = child.localPosition;
			float x = localPosition.x;
			return num - child.rect.xMax - x;
		}

		public static float GetInsetFromParentEdge(this RectTransform child, RectTransform parentHint, RectTransform.Edge parentEdge)
		{
			switch (parentEdge)
			{
			case RectTransform.Edge.Top:
				return child.GetInsetFromParentTopEdge(parentHint);
			case RectTransform.Edge.Bottom:
				return child.GetInsetFromParentBottomEdge(parentHint);
			case RectTransform.Edge.Left:
				return child.GetInsetFromParentLeftEdge(parentHint);
			case RectTransform.Edge.Right:
				return child.GetInsetFromParentRightEdge(parentHint);
			default:
				return 0f;
			}
		}

		public static void SetSizeFromParentEdgeWithCurrentAnchors(this RectTransform child, RectTransform.Edge fixedEdge, float newSize)
		{
			RectTransform parentHint = child.parent as RectTransform;
			child.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(parentHint, fixedEdge, child.GetInsetFromParentEdge(parentHint, fixedEdge), newSize);
		}

		public static void SetSizeFromParentEdgeWithCurrentAnchors(this RectTransform child, RectTransform parentHint, RectTransform.Edge fixedEdge, float newSize)
		{
			child.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(parentHint, fixedEdge, child.GetInsetFromParentEdge(parentHint, fixedEdge), newSize);
		}

		public static void SetInsetAndSizeFromParentEdgeWithCurrentAnchors(this RectTransform child, RectTransform.Edge fixedEdge, float newInset, float newSize)
		{
			child.SetInsetAndSizeFromParentEdgeWithCurrentAnchors(child.parent as RectTransform, fixedEdge, newInset, newSize);
		}

		public static void SetInsetAndSizeFromParentEdgeWithCurrentAnchors(this RectTransform child, RectTransform parentHint, RectTransform.Edge fixedEdge, float newInset, float newSize)
		{
			Vector2 offsetMin = child.offsetMin;
			Vector2 offsetMax = child.offsetMax;
			switch (fixedEdge)
			{
			case RectTransform.Edge.Bottom:
			{
				float insetFromParentRightEdge = child.GetInsetFromParentBottomEdge(parentHint);
				float num = newInset - insetFromParentRightEdge;
				offsetMax.y += newSize - child.rect.height + num;
				offsetMin.y += num;
				break;
			}
			case RectTransform.Edge.Top:
			{
				float insetFromParentRightEdge = child.GetInsetFromParentTopEdge(parentHint);
				float num = newInset - insetFromParentRightEdge;
				offsetMin.y -= newSize - child.rect.height + num;
				offsetMax.y -= num;
				break;
			}
			case RectTransform.Edge.Left:
			{
				float insetFromParentRightEdge = child.GetInsetFromParentLeftEdge(parentHint);
				float num = newInset - insetFromParentRightEdge;
				offsetMax.x += newSize - child.rect.width + num;
				offsetMin.x += num;
				break;
			}
			case RectTransform.Edge.Right:
			{
				float insetFromParentRightEdge = child.GetInsetFromParentRightEdge(parentHint);
				float num = newInset - insetFromParentRightEdge;
				offsetMin.x -= newSize - child.rect.width + num;
				offsetMax.x -= num;
				break;
			}
			}
			child.offsetMin = offsetMin;
			child.offsetMax = offsetMax;
		}

		public static void MatchParentSize(this RectTransform rt)
		{
			rt.anchorMin = Vector2.zero;
			rt.anchorMax = Vector2.one;
			rt.sizeDelta = Vector3.zero;
			rt.pivot = Vector2.one * 0.5f;
			rt.anchoredPosition = Vector3.zero;
		}
	}
}
