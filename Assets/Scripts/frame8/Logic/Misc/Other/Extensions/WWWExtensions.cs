using UnityEngine;

namespace frame8.Logic.Misc.Other.Extensions
{
	public static class WWWExtensions
	{
		public static long GetContentLengthFromHeader(this WWW www)
		{
			if (www.responseHeaders.TryGetValue("Content-Length", out string value) && long.TryParse(value, out long result))
			{
				return result;
			}
			return -1L;
		}
	}
}
