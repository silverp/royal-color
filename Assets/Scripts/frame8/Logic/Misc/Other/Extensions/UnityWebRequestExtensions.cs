using UnityEngine.Networking;

namespace frame8.Logic.Misc.Other.Extensions
{
	public static class UnityWebRequestExtensions
	{
		public static long GetContentLengthFromHeader(this UnityWebRequest www)
		{
			string responseHeader;
			if ((responseHeader = www.GetResponseHeader("Content-Length")) != null && long.TryParse(responseHeader, out long result))
			{
				return result;
			}
			return -1L;
		}
	}
}
