using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragDirectionDetermine : MonoBehaviour
{
	public enum Direction
	{
		None,
		X,
		Y
	}

	private bool isStart;

	private Direction nowDirection;

	private List<ScrollRect> verticalScrollRect;

	public ScrollRect horizontalScrollRect;

	public RectTransform pageGropTransform;

	private bool changeFlag = true;

	private Vector2 TouchBeginPos;

	private float TOUCH_MAGIN = 10f;

	private bool DirectionDectDetectLock;

	private void Awake()
	{
		AssginEvent();
	}

	private void Start()
	{
	}

	private void Update()
	{
		if (isStart)
		{
			Determine();
			DirectionSwitch();
		}
	}

	private void Determine()
    {
        var touches = InputHelper.GetTouches();
        if (touches.Length <= 0)
		{
			return;
		}
		switch (touches[0].phase)
		{
		case TouchPhase.Stationary:
			break;
		case TouchPhase.Ended:
			break;
		case TouchPhase.Began:
			DirectionDectDetectLock = false;
			TouchBeginPos = touches[0].position;
			break;
		case TouchPhase.Moved:
		{
			float num = Vector2.Distance(TouchBeginPos, touches[0].position);
			Vector2 vector = touches[0].position - TouchBeginPos;
			if (num > TOUCH_MAGIN && !DirectionDectDetectLock)
			{
				if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
				{
					nowDirection = Direction.X;
				}
				else
				{
					nowDirection = Direction.Y;
				}
				DirectionDectDetectLock = true;
			}
			break;
		}
		}
	}

	private void CompletePageInit()
	{
		verticalScrollRect = new List<ScrollRect>();
		IEnumerator enumerator = pageGropTransform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform = (Transform)enumerator.Current;
				verticalScrollRect.Add(transform.GetComponent<ScrollRect>());
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		isStart = true;
	}

	private void DirectionSwitch()
	{
		switch (nowDirection)
		{
		case Direction.None:
			SetSwitch(x: true, y: true);
			break;
		case Direction.X:
			SetSwitch(x: true, y: false);
			break;
		case Direction.Y:
			SetSwitch(x: false, y: true);
			break;
		}
	}

	private void SetSwitch(bool x, bool y)
	{
		for (int i = 0; i < verticalScrollRect.Count; i++)
		{
			verticalScrollRect[i].enabled = y;
		}
		horizontalScrollRect.enabled = x;
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvent()
	{
		DragEventProxy.OnCompletePageInit += CompletePageInit;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnCompletePageInit -= CompletePageInit;
	}
}
