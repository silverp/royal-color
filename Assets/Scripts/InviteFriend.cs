using cn.sharesdk.unity3d;
using System;
using System.Collections;
using UI.ThreeDimensional;
using UnityEngine;

public class InviteFriend : MonoBehaviour
{
	[SerializeField]
	private Transform shareObjectprefab;

	[SerializeField]
	private Transform inviteObjectprefab;

	public UITemplate template;

	public ShareSDK ssdk;

	private bool IsShowMsg;

	private GUIStyle guiStyle = new GUIStyle();

	private string ShareMsg;

	private float WaitTime = 3f;

	private bool shareClock;

	private Action<Errcode> shareCallback;

	private void Start()
	{
		ssdk.shareHandler = OnShareResultHandler;
	}

	private void Update()
	{
	}

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
	}

	private void DiscardEvents()
	{
	}

	private void OnShareResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		Errcode errcode = new Errcode();
		switch (state)
		{
		case ResponseState.Success:
			errcode.errorCode = Errcode.OK;
			ShareMsg = "share successfully!";
			UnityEngine.Debug.Log("ResponseState.Successs !!!");
			break;
		case ResponseState.Fail:
			errcode.errorCode = Errcode.FAIL;
			TotalGA.Event("share_fail");
			ShareMsg = "fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"];
			UnityEngine.Debug.Log("share fail!!!");
			break;
		case ResponseState.Cancel:
			errcode.errorCode = Errcode.FAIL;
			ShareMsg = "cancel !";
			TotalGA.Event("share_cancel");
			UnityEngine.Debug.Log("share cancel!!!");
			break;
		}
		shareClock = false;
		UnityEngine.Debug.Log("invite share end!!!");
		shareCallback(errcode);
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(WaitTime);
		IsShowMsg = false;
	}

	private void OnDisable()
	{
		IsShowMsg = false;
	}
}
