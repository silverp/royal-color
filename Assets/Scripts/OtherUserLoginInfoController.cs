using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class OtherUserLoginInfoController : MonoBehaviour
{
	private string workCountUrl = "/user/getWorkListCount";

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		string str = string.Empty;
		if (ApplicationModel.otherUserInfo != null)
		{
			int num = PublicToolController.CountDiffDaysByTimeStamp(ApplicationModel.otherUserInfo.Create.ToString());
			if (ApplicationModel.LANGUAGE == "zh-cn")
			{
				str = num + "天";
			}
			else
			{
				str = num + " days";
			}
		}
		NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + workCountUrl + "?uid=" + ApplicationModel.otherUserInfo.Uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false, delegate(string json, bool isSuccess)
		{
			RequestUrlCallBack(json, isSuccess, str);
		});
	}

	private void RequestUrlCallBack(string json, bool isSuccess, string text)
	{
		if (isSuccess)
		{
			WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
			if (workCountJson.status == 0)
			{
				if (ApplicationModel.LANGUAGE == "zh-cn")
				{
					string text2 = text;
					text = text2 + "," + workCountJson.data.count + "次创作";
				}
				else
				{
					string text2 = text;
					text = text2 + ",created " + workCountJson.data.count + " times";
				}
			}
		}
		GetComponent<Text>().text = text;
	}
}
