using Facebook.Unity;
using System.Collections.Generic;

using UnityEngine;

public class UmengShareController : MonoBehaviour
{
	public static bool hasInit;

	//private AndroidJavaClass mAndroidJavaClass;

	//private AndroidJavaObject mAndroidJavaObject;

	private readonly string payChannel1 = "5a4b0242f43e481457000086";

	private readonly string payChannel2 = "5a5ddf4d8f4a9d6c230001af";

	private List<string> payChannelList1;

	private List<string> payChannelList2;

	private List<string> adChannelList;

	private void Awake()
	{
		PublicControllerApi.GetPayChannelList(RequestPayChannel1Callback);
		PublicControllerApi.GetAdChannleList(RequestAdChannelBack);
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
			FB.Init(delegate
			{
				FB.ActivateApp();
			});
		}
	}

	private void RequestAdChannelBack(Errcode err, List<string> list)
	{
		if (err.errorCode == Errcode.OK)
		{
			adChannelList = list;
		}
	}

	private void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
			else
			{
				FB.Init(delegate
				{
					FB.ActivateApp();
				});
			}
		}
	}

	private void RequestPayChannel1Callback(Errcode err, List<string> list)
	{
		UnityEngine.Debug.Log("umeng err is:" + err.errorCode.ToString());
		if (err.errorCode == Errcode.OK)
		{
			payChannelList1 = list;
		}
		PublicControllerApi.GetPayChannelList(RequestPayChannel2Callback, 2);
	}

	private void RequestPayChannel2Callback(Errcode err, List<string> list)
	{
		UnityEngine.Debug.Log("RequestPayChannel2Callback ...");
		if (err.errorCode == Errcode.OK)
		{
			payChannelList2 = list;
		}
		startInit();
	}

	private void startInit()
	{
		//UnityEngine.Debug.Log("umeng start init!!!");
		if (!hasInit)
		{
			hasInit = true;
			//UnityEngine.Debug.Log("UNITY_ANDROID!");
			//mAndroidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			//mAndroidJavaObject = mAndroidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			//mAndroidJavaObject.Call<int>("readMeta", new object[1]
			//{
			//	"UMENG_CHANNEL"
			//});
		}
	}

	public void recieveMessage(string value)
	{
		ApplicationModel.currentChannel = value;
		UnityEngine.Debug.Log("recieveMessage:" + value);
		//TalkingDataGA.OnStart("AC2E5604A04747CFAABE47713B41D051", value);
		//TDGAAccount tDGAAccount = TDGAAccount.SetAccount(SystemInfo.deviceUniqueIdentifier);
		//tDGAAccount.SetAccountType(AccountType.ANONYMOUS);
		if (IsPayChannel1(value))
		{
			UnityEngine.Debug.Log("IsPayChannel");
			ApplicationModel.IsPayChannel = true;
			UnityEngine.Debug.Log("ApplicationModel.IsPayChannel is:" + ApplicationModel.IsPayChannel);
			//Analytics.StartWithAppKeyAndChannelId(payChannel1, value);
		}
		else if (IsPayChannel2(value))
		{
			ApplicationModel.IsPayChannel = true;
			UnityEngine.Debug.Log("ApplicationModel.IsPayChannel is:" + ApplicationModel.IsPayChannel);
			//Analytics.StartWithAppKeyAndChannelId(payChannel2, value);
		}
		else
		{
			UnityEngine.Debug.Log("not IsPayChannel");
			ApplicationModel.IsPayChannel = false;
			//Analytics.StartWithAppKeyAndChannelId("59279932bbea8358c6000506", value);
		}
		ApplicationModel.IsPayChannel = true;
		TotalGA.Event("start");
		if (IsAdChannel(value))
		{
			ApplicationModel.isAdChannel = true;
		}
		else
		{
			ApplicationModel.isAdChannel = false;
		}
	}

	private bool IsPayChannel1(string value)
	{
		foreach (string item in payChannelList1)
		{
			if (value == item)
			{
				return true;
			}
		}
		return false;
	}

	private bool IsPayChannel2(string value)
	{
		foreach (string item in payChannelList2)
		{
			if (value == item)
			{
				return true;
			}
		}
		return false;
	}

	private bool IsAdChannel(string value)
	{
		foreach (string adChannel in adChannelList)
		{
			if (value == adChannel)
			{
				return true;
			}
		}
		return false;
	}
}
