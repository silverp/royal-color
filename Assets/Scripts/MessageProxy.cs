using UnityEngine;

public class MessageProxy
{
	public delegate void ChangeImage(string imageId);

	public delegate void ShareWork(string imageId);

	private static MessageProxy _instance;

	public static event ChangeImage OnChangeImage;

	public static event ShareWork OnShareWork;

	private MessageProxy()
	{
	}

	public static MessageProxy GetInstance()
	{
		if (_instance == null)
		{
			_instance = new MessageProxy();
		}
		return _instance;
	}

	public void SendOnShareWork(string imageId)
	{
		UnityEngine.Debug.Log("SendOnShareWork.OnShareWork = " + MessageProxy.OnShareWork);
		if (MessageProxy.OnShareWork != null)
		{
			MessageProxy.OnShareWork(imageId);
		}
	}

	public void SendOnChangeImageMsg(string imageId)
	{
		if (MessageProxy.OnChangeImage != null)
		{
			MessageProxy.OnChangeImage(imageId);
		}
	}
}
