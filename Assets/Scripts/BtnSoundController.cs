using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BtnSoundController : MonoBehaviour
{
	private Vector3 originScale;

	private float scaleTime = 0.2f;

	private float maxScaleFactor = 0.2f;

	private void Start()
	{
		originScale = base.transform.localScale;
	}

	private IEnumerator scale()
	{
		float eslapedTime = 0f;
		while (eslapedTime < scaleTime)
		{
			eslapedTime += Time.deltaTime;
			base.transform.localScale = originScale * (1f + maxScaleFactor * Mathf.Sin((float)Math.PI * eslapedTime / scaleTime));
			yield return null;
		}
		base.transform.localScale = originScale;
	}

	private void Awake()
	{
		GetComponent<Button>().onClick.AddListener(delegate
		{
			//StartCoroutine(scale());
			NewWorkerController.EventLock = true;
			//UnityEngine.Debug.Log("btn sound  触发了eventlock!");
			//UnityEngine.Debug.Log("name is:" + base.transform.name);
		});
	}

	public void OnPressAnmiationStop()
	{
		UnityEngine.Debug.Log("Anmiation has stoped");
	}
}
