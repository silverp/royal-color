using UnityEngine;

public class MagnifyingGlassVolumetricSphere : MonoBehaviour
{
	public Material m_Mat;

	[Range(0.1f, 2f)]
	public float m_Radius = 1f;

	[Range(1f, 3f)]
	public float m_Distortion = 2f;

	[Range(0.05f, 0.3f)]
	public float m_Form = 0.2f;

	private float m_MouseX;

	private float m_MouseY;

	private bool m_TraceMouse;

	private void Start()
	{
		if (!SystemInfo.supportsImageEffects)
		{
			base.enabled = false;
		}
		m_MouseX = (m_MouseY = 0.5f);
	}

	private void OnRenderImage(RenderTexture sourceTexture, RenderTexture destTexture)
	{
		m_Mat.SetVector("_Center", new Vector4(m_MouseX, m_MouseY, 0f, 0f));
		m_Mat.SetFloat("_Radius", m_Radius);
		m_Mat.SetFloat("_Distortion", m_Distortion);
		m_Mat.SetFloat("_Form", m_Form);
		Graphics.Blit(sourceTexture, destTexture, m_Mat);
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			m_TraceMouse = true;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			m_TraceMouse = false;
		}
		else if (Input.GetMouseButton(0) && m_TraceMouse)
		{
			Vector3 mousePosition = UnityEngine.Input.mousePosition;
			m_MouseX = mousePosition.x / (float)Screen.width;
			Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
			m_MouseY = mousePosition2.y / (float)Screen.height;
		}
	}

	private void OnGUI()
	{
		GUI.Box(new Rect(10f, 10f, 300f, 25f), "Magnifying Glass Demo --- Volumetric Sphere");
	}
}
