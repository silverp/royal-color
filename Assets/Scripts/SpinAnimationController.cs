using UnityEngine;

public class SpinAnimationController : MonoBehaviour
{
	[SerializeField]
	private float speed = 1000f;

	[SerializeField]
	private int direction = -1;

	private void FixedUpdate()
	{
		base.transform.Rotate(Vector3.forward, (float)direction * speed * Time.deltaTime);
	}
}
