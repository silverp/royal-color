using UnityEngine;

public class ImageLoadingEffect : MonoBehaviour
{
	[SerializeField]
	private float speed = 1f;

	[SerializeField]
	private float height = 162f;

	private float width;

	private float s;

	private void Awake()
	{
		Vector2 sizeDelta = base.transform.GetComponent<RectTransform>().sizeDelta;
		width = sizeDelta.x;
	}

	private void FixedUpdate()
	{
		s += speed * Time.deltaTime;
		UnityEngine.Debug.Log("speed * Time.deltaTime is:" + speed * Time.deltaTime);
		base.transform.GetComponent<RectTransform>().sizeDelta = new Vector2(width, height - s % height);
	}
}
