using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OtherWorkScrollRectAdpter : MonoBehaviour
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class DoubleImageModel
	{
		public MyselfWork img;

		public int index;

		public DoubleImageModel(MyselfWork img, int index)
		{
			this.img = img;
			this.index = index;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<DoubleImageModel> _Data;

		public RectTransform root;

		private MyWorkItemsViewHolder myWorkItemsViewHolder;

		private Texture2D loadingTex = Resources.Load<Texture2D>("Images/inspiration/prefabImg");

		public MyWorkScrollRectItemsAdapter(List<DoubleImageModel> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		public void DestoryAssets(string name)
		{
			if (root.GetComponent<RawImageDataController>().downloadStatus == 1)
			{
				UnityEngine.Object.DestroyImmediate(root.Find("Image").GetComponent<RawImage>().texture);
			}
		}

		private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageId)
		{
			return _Data.Count > itemIndex && itemIdexAtRequest == itemIndex && imageId == _Data[itemIndex].img.img.imageId;
		}

		public void SetImage(MyselfWork work, int index)
		{
			if (work != null)
			{
				root.Find("Image").GetComponent<RawImage>().texture = loadingTex;
				root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
				GameObject continueBtn = root.Find("BottomPanel").Find("continue").gameObject;
				string _imageId = work.img.imageId;
				if (work.img.thumb != string.Empty)
				{
					NetLoadcontroller.Instance.RequestImg(work.img.thumb, delegate (Texture2D tex)
					{
						callback(tex, work, continueBtn, _imageId);
					});
				}
				root.Find("BottomPanel").Find("share").GetComponent<Button>()
					.onClick.RemoveAllListeners();
				root.Find("BottomPanel").Find("share").GetComponent<Button>()
					.onClick.AddListener(delegate
					{
						clickShareBtn(work);
					});
				LikeBtnControl(work);
				if (index % 8 == 0)
				{
					PublicCallFrameTool.Instance.CollectGarbage();
				}
			}
		}

		private void clickShareBtn(MyselfWork work)
		{
			string url = ApplicationModel.HOST + "/user/getWorkListCount?uid=" + ApplicationModel.otherUserInfo.Uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false;
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(url, delegate (string json, bool isSuccess)
			{
				RequestWorkCountCallback(json, isSuccess, work);
			});
		}

		private void RequestWorkCountCallback(string json, bool isSuccess, MyselfWork img)
		{
			int count = 0;
			if (isSuccess)
			{
				WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
				if (workCountJson.status == 0)
				{
					UnityEngine.Debug.Log("work count is：" + workCountJson.data.count);
					count = workCountJson.data.count;
				}
			}
			NetLoadcontroller.Instance.RequestImg(img.img.thumb, delegate (Texture2D tex)
			{
				RequestFinalOnshare(tex, ApplicationModel.otherUserInfo.Nickname, count);
			});
		}

		private void RequestFinalOnshare(Texture2D tex, string name, int count)
		{
			UnityEngine.Debug.Log("go to share !!!");
			FileUploadEventProxy.Instance.SendShareWorkEventMsg(tex, name, count, isHasinfo: true, shareCallback);
		}

		private void shareCallback(Errcode err)
		{
			DragEventProxy.Instance.SendShareCallbackMsg(err);
		}

		private void LikeBtnControl(MyselfWork work)
		{
			GameObject likeBtn = root.Find("BottomPanel").Find("like").gameObject;
			GameObject likeCntBtn = root.Find("BottomPanel").Find("like").Find("likeCnt")
				.gameObject;
			ChangeBtnStyle(work.isLike, likeBtn);
			likeCntBtn.GetComponent<Text>().text = work.img.likeCnt.ToString();
			likeBtn.GetComponent<Button>().onClick.RemoveAllListeners();
			likeBtn.GetComponent<Button>().onClick.AddListener(delegate
			{
				OnHitLikeBtn(work, likeBtn, likeCntBtn);
			});
		}

		private void OnHitLikeBtn(MyselfWork work, GameObject btn, GameObject likeCntBtn)
		{
			if (ApplicationModel.userInfo == null)
			{
				DragEventProxy.Instance.SendOffLineMsg(ApplicationModel.lastVisibleScreen);
				return;
			}
			work.isLike = !work.isLike;
			if (work.isLike)
			{
				work.img.likeCnt++;
			}
			else
			{
				work.img.likeCnt--;
			}
			likeCntBtn.GetComponent<Text>().text = work.img.likeCnt.ToString();
			ChangeBtnStyle(work.isLike, btn);
			FileUploadEventProxy.Instance.SendLikeEventMsg(ApplicationModel.otherUserInfo.Uid, work.img.workId, work.isLike);
		}

		private void ChangeBtnStyle(bool isLike, GameObject likeBtn)
		{
			if (isLike)
			{
				likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_en");
			}
			else
			{
				likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_un");
			}
		}

		private void callback(Texture2D tex, MyselfWork work, GameObject continueBtn, string imageId)
		{
			int itemIndex = myWorkItemsViewHolder.itemIndex;
			if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, imageId))
			{
				UnityEngine.Debug.Log("likes work is:" + JsonConvert.SerializeObject(work));
				RawImage component = root.Find("Image").GetComponent<RawImage>();
				if (component != null)
				{
					component.texture = tex;
				}
				continueBtn.GetComponent<Button>().onClick.RemoveAllListeners();
				continueBtn.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnContinueBtnHit(work);
				});
				root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
				root.Find("Image").GetComponent<Button>().onClick.AddListener(delegate
				{
					OnImageHit(tex);
				});
			}
		}

		private void SetFrommImgData()
		{
			root.GetComponent<RawImageDataController>().downloadStatus = 1;
		}

		public void callback(Texture2D tex, string name)
		{
			root.Find("Image").GetComponent<RawImage>().texture = tex;
		}

		private Texture2D GetTex2dFromImg(EditedImg img)
		{
			Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
			texture2D.LoadImage(PublicToolController.FileReaderBytes(img.imageId + "_ok"), markNonReadable: false);
			texture2D.Apply();
			return texture2D;
		}

		protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
			myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return myWorkItemsViewHolder;
		}

		protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			myWorkItemsViewHolder = newOrRecycled;
			DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
			SetImage(doubleImageModel.img, doubleImageModel.index);
		}

		private void OnImageHit(Texture2D tex)
		{
			DragEventProxy.Instance.SendGotoZoomImageMsg(tex);
		}

		private void OnContinueBtnHit(MyselfWork work)
		{
			ApplicationModel.lastVisibleScreen = "OtherWorkCanvas";
			DragEventProxy.Instance.SendRepaintMsg(work.img);
		}
	}

	[SerializeField]
	private MyWorkParams _ScrollRectAdapterParams;

	private List<DoubleImageModel> _Data = new List<DoubleImageModel>();

	private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

	private List<MyselfWork> workList;

	public BaseParams Params => _ScrollRectAdapterParams;

	public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<DoubleImageModel> Data => _Data;

	private void Awake()
	{
		_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("other work enable!!");
		HideChild(base.transform.GetChild(0).GetChild(0));
		DownloadWorkListInfo();
	}

	private void DownloadWorkListInfo()
	{
		NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/user/getWorkList?uid=" + ApplicationModel.otherUserInfo.Uid + "&page=1&step=300&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false, delegate (string json, bool isSuccess)
		{
			RequesetInfoCallback(json, isSuccess);
		});
	}

	private void RequesetInfoCallback(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			UnityEngine.Debug.Log("other work json is:" + json);
			MyselfWorkJson myselfWorkJson = JsonConvert.DeserializeObject<MyselfWorkJson>(json);
			if (myselfWorkJson.status == 0)
			{
				workList = myselfWorkJson.data;
				StartCoroutine(initContent());
			}
		}
	}

	private IEnumerator initContent()
	{
		yield return null;
		_Data.Clear();
		List<MyselfWork> imglist = workList;
		int ImgTotal = imglist.Count;
		for (int i = 0; i < ImgTotal; i++)
		{
			_Data.Add(new DoubleImageModel(imglist[i], i));
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		_ScrollRectItemsAdapter.ScrollToStart();
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void OnDestroy()
	{
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private List<EditedImg> GetHasEditedImgList()
	{
		List<EditedImg> list = new List<EditedImg>();
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg != null && editedResourceImg.HasEdited())
			{
				list.Add(editedResourceImg);
			}
		}
		return list;
	}

	private void HideChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				transform2.gameObject.SetActive(value: false);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
