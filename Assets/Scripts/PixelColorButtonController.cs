using System;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PixelColorButtonController : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public class ColorIndex
	{
		public int id
		{
			get;
			set;
		}

		public int action
		{
			get;
			set;
		}
	}

	private int _index;

	private bool selected;

	private bool disable;

	protected IDisposable subscribe;

	public Color color
	{
		set
		{
			base.transform.Find("innerBg").GetComponent<Image>().color = value;
			base.transform.Find("outerBg").GetComponent<Image>().color = value;
		}
	}

	public int index
	{
		get
		{
			return _index;
		}
		set
		{
			_index = value;
			base.transform.Find("innerBg/Text").GetComponent<Text>().text = _index.ToString();
		}
	}

	private void Awake()
	{
		subscribe = MessageBroker.Default.Receive<ColorIndex>().Subscribe(onColorChange);
		UnityEngine.Debug.Log("init :PixelColorButton!");
	}

	public void OnDisable()
	{
		if (subscribe != null)
		{
			subscribe.Dispose();
		}
	}

	public void onColorChange(ColorIndex idx)
	{
		if (idx.id == index)
		{
			UnityEngine.Debug.Log("onColorChange: id:" + idx.id + " " + index + " " + idx.action);
			switch (idx.action)
			{
			case 1:
				base.transform.Find("outerBg").gameObject.SetActive(true);
				selected = true;
				break;
			case 2:
				base.transform.Find("disable").gameObject.SetActive(true);
				disable = true;
				break;
			case 3:
				base.transform.Find("disable").gameObject.SetActive(false);
				disable = false;
				break;
			}
		}
		else if (selected && idx.action == 1)
		{
			base.transform.Find("outerBg").gameObject.SetActive(false);
			selected = false;
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!disable)
		{
			MessageBroker.Default.Publish(new ColorIndex
			{
				id = _index,
				action = 1
			});
		}
	}
}
