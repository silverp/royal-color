using System.Collections.Generic;

public class CateListJson
{
	public List<CategoryDetail> data;

	public int status
	{
		get;
		set;
	}

	public string msg
	{
		get;
		set;
	}
}
