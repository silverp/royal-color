using UnityEngine;

public class ChangeWidthByLanuage : MonoBehaviour
{
	[SerializeField]
	private int enWidth;

	[SerializeField]
	private int zhcnWidth;

	private void Start()
	{
		if (ApplicationModel.LANGUAGE == "en")
		{
			RectTransform component = GetComponent<RectTransform>();
			float x = enWidth;
			Vector2 sizeDelta = GetComponent<RectTransform>().sizeDelta;
			component.sizeDelta = new Vector2(x, sizeDelta.y);
		}
		else if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			RectTransform component2 = GetComponent<RectTransform>();
			float x2 = zhcnWidth;
			Vector2 sizeDelta2 = GetComponent<RectTransform>().sizeDelta;
			component2.sizeDelta = new Vector2(x2, sizeDelta2.y);
		}
	}

	private void Update()
	{
	}
}
