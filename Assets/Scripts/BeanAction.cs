using System;
using UnityEngine;

public class BeanAction
{
	public static readonly string FIRST_LOGIN = "1000";

	public static readonly string INVITE = "1001";

	public static readonly string COMMENT = "1002";

	public static readonly string SHARE = "1003";

	public static readonly string PUBLISH = "1004";

	public static readonly string UNCLOCK_OUTLINE = "1005";

	public static readonly string UNCLOCK_TEXTURE = "1006";

	public static readonly string SIGN = "1008";

	public static readonly string WATCH_VIDEO = "1007";

	public static void request(string actionId, Action<Errcode, int> callback)
	{
		int amount = 0;
		Errcode errcode = new Errcode();
		if (ApplicationModel.userInfo != null)
		{
			URLRequest uRLRequest = APIFactory.create(APIFactory.BEAN_ACTION);
			uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
			uRLRequest.addParam("actionId", actionId);
			UnityEngine.Debug.Log(" bean url request is:" + uRLRequest.getUrl());
			DataBroker.getJson(uRLRequest, delegate(BeanActionReturnJson detail)
			{
				if (detail.status == 0)
				{
					errcode.errorCode = Errcode.OK;
					amount = detail.data.amount;
				}
				else if (detail.status == 10405)
				{
					errcode.errorCode = Errcode.BEAN_NOT_ENOUGH;
				}
				else
				{
					errcode.errorCode = Errcode.FAIL;
				}
				callback(errcode, amount);
			}, delegate
			{
				errcode.errorCode = Errcode.FAIL;
				callback(errcode, amount);
			});
		}
		else
		{
			errcode.errorCode = Errcode.NOT_LOGIN;
			callback(errcode, amount);
		}
	}
}
