using System.Collections.Generic;

public class ReferenceColorData
{
	public string imageId;

	public string outline;

	public List<ReferenceItem> referList;

	public string thumbnailUrl;

	public string index;

	public bool locked;
}
