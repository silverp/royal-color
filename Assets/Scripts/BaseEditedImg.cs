using System;

public class BaseEditedImg : IComparable<BaseEditedImg>
{
	public string imageId;

	public int likeCnt;

	public bool isRemote;

	public bool isLike;

	public long create;

	public bool hasEdited;

	public string workId;

	public string finalImg;

	public string thumb;

	public void SetEdited()
	{
		hasEdited = true;
	}

	public bool HasEdited()
	{
		return hasEdited;
	}

	public void SaveToLocal()
	{
		isRemote = true;
	}

	public int CompareTo(BaseEditedImg baseEditedImg)
	{
		if (baseEditedImg == null)
		{
			return -1;
		}
		return -create.CompareTo(baseEditedImg.create);
	}
}
