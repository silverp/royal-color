using UnityEngine;
using UnityEngine.UI;

public class OtherUserHeadImageController : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("change head img");
		if (ApplicationModel.otherUserInfo != null)
		{
			NetLoadcontroller.Instance.RequestImg(ApplicationModel.otherUserInfo.HeadimgUrl, delegate(Texture2D tex)
			{
				changeImage(tex);
			});
			return;
		}
		Texture2D tex2 = Resources.Load<Texture2D>("Images/Mywork/default_headimg");
		changeImage(tex2);
	}

	private void changeImage(Texture2D tex)
	{
		GetComponent<RawImage>().texture = tex;
	}
}
