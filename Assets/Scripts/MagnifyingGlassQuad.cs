using UnityEngine;

public class MagnifyingGlassQuad : MonoBehaviour
{
	private Camera m_MagnifyCamera;

	private Rect[] m_GUIRects = new Rect[3];

	private float m_QuadMagAmount = 1f;

	private float m_QuadMagWidth = (float)Screen.width / 5f;

	private float m_QuadMagHeight = (float)Screen.width / 5f;

	private float m_MouseX;

	private float m_MouseY;

	private bool m_TraceMouse;

	private void Start()
	{
		m_GUIRects[0] = new Rect(95f, 65f, 115f, 25f);
		m_GUIRects[1] = new Rect(95f, 95f, 115f, 25f);
		m_GUIRects[2] = new Rect(95f, 125f, 115f, 25f);
		m_MouseX = (float)Screen.width / 2f;
		m_MouseY = (float)Screen.height / 2f;
		CreateMagnifyGlass();
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			m_TraceMouse = true;
			for (int i = 0; i < m_GUIRects.Length; i++)
			{
				ref Rect reference = ref m_GUIRects[i];
				Vector3 mousePosition = UnityEngine.Input.mousePosition;
				float x = mousePosition.x;
				float num = Screen.height;
				Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
				if (reference.Contains(new Vector2(x, num - mousePosition2.y)))
				{
					m_TraceMouse = false;
					break;
				}
			}
		}
		else if (Input.GetMouseButtonUp(0))
		{
			m_TraceMouse = false;
		}
		else if (Input.GetMouseButton(0) && m_TraceMouse)
		{
			if (CanMoveHorizontal())
			{
				Vector3 mousePosition3 = UnityEngine.Input.mousePosition;
				m_MouseX = mousePosition3.x;
			}
			if (CanMoveVertical())
			{
				Vector3 mousePosition4 = UnityEngine.Input.mousePosition;
				m_MouseY = mousePosition4.y;
			}
		}
		m_MagnifyCamera.orthographicSize = m_QuadMagAmount;
		m_MagnifyCamera.pixelRect = new Rect(m_MouseX - m_QuadMagWidth / 2f, m_MouseY - m_QuadMagHeight / 2f, m_QuadMagWidth, m_QuadMagHeight);
		Transform transform = m_MagnifyCamera.transform;
		float mouseX = m_MouseX;
		float mouseY = m_MouseY;
		Vector3 mousePosition5 = UnityEngine.Input.mousePosition;
		transform.position = GetWorldPosition(new Vector3(mouseX, mouseY, mousePosition5.z));
	}

	private bool CanMoveHorizontal()
	{
		if (m_MouseX < m_QuadMagWidth / 2f)
		{
			Vector3 mousePosition = UnityEngine.Input.mousePosition;
			if (mousePosition.x < m_MouseX)
			{
				return false;
			}
		}
		if (m_MouseX > (float)Screen.width - m_QuadMagWidth / 2f)
		{
			Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
			if (mousePosition2.x > m_MouseX)
			{
				return false;
			}
		}
		return true;
	}

	private bool CanMoveVertical()
	{
		if (m_MouseY < m_QuadMagHeight / 2f)
		{
			Vector3 mousePosition = UnityEngine.Input.mousePosition;
			if (mousePosition.y < m_MouseY)
			{
				return false;
			}
		}
		if (m_MouseY > (float)Screen.height - m_QuadMagHeight / 2f)
		{
			Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
			if (mousePosition2.y > m_MouseY)
			{
				return false;
			}
		}
		return true;
	}

	private void CreateMagnifyGlass()
	{
		GameObject gameObject = new GameObject("Magnify Camera");
		float x = (float)Screen.width / 2f - m_QuadMagWidth / 2f;
		float y = (float)Screen.height / 2f - m_QuadMagHeight / 2f;
		m_MagnifyCamera = gameObject.AddComponent<Camera>();
		m_MagnifyCamera.pixelRect = new Rect(x, y, m_QuadMagWidth, m_QuadMagHeight);
		m_MagnifyCamera.transform.position = new Vector3(0f, 0f, 0f);
		m_MagnifyCamera.clearFlags = Camera.main.clearFlags;
		m_MagnifyCamera.backgroundColor = Camera.main.backgroundColor;
		if (Camera.main.orthographic)
		{
			m_MagnifyCamera.orthographic = true;
			m_MagnifyCamera.orthographicSize = Camera.main.orthographicSize / 5f;
			m_QuadMagAmount = m_MagnifyCamera.orthographicSize;
		}
		else
		{
			m_MagnifyCamera.orthographic = false;
			m_MagnifyCamera.fieldOfView = Camera.main.fieldOfView / 10f;
		}
	}

	public Vector3 GetWorldPosition(Vector3 screenPos)
	{
		Vector3 result = Vector3.zero;
		if (Camera.main.orthographic)
		{
			result = Camera.main.ScreenToWorldPoint(screenPos);
			Vector3 position = Camera.main.transform.position;
			result.z = position.z;
		}
		else
		{
			Camera main = Camera.main;
			float x = screenPos.x;
			float y = screenPos.y;
			Vector3 position2 = Camera.main.transform.position;
			result = main.ScreenToWorldPoint(new Vector3(x, y, position2.z));
			result.x *= -1f;
			result.y *= -1f;
		}
		return result;
	}

	private void OnGUI()
	{
		GUI.Box(new Rect(10f, 10f, 280f, 25f), "Magnifying Glass Demo --- Quad Glass");
		GUI.Box(new Rect(10f, 50f, 80f, 25f), "Amount");
		m_QuadMagAmount = GUI.HorizontalSlider(m_GUIRects[0], m_QuadMagAmount, 0.1f, 2f);
		GUI.Box(new Rect(10f, 80f, 80f, 25f), "Width");
		m_QuadMagWidth = GUI.HorizontalSlider(m_GUIRects[1], m_QuadMagWidth, 0f, Screen.width);
		GUI.Box(new Rect(10f, 110f, 80f, 25f), "Height");
		m_QuadMagHeight = GUI.HorizontalSlider(m_GUIRects[2], m_QuadMagHeight, 0f, Screen.height);
	}
}
