using UnityEngine;

public class ShareButtonController : MonoBehaviour
{
	// [SerializeField]
	// private GameObject PublishPanel;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject ShareCanvas;

	[SerializeField] private GameObject pixelArtWorkshop;
	[SerializeField] private GameObject StickerCanvas;

	private void Start()
	{
	//	MyworkCanvas = base.transform.parent.parent.Find("MyworkCanvas").gameObject;
	//	ShareCanvas = base.transform.parent.gameObject;
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("On enable!!!!");
		GameQuit.Back += Back;
	}

	private void OnDisable()
	{
		GameQuit.Back -= Back;
	}

	public void Back()
	{
		UnityEngine.Debug.Log("back...");
		if (ClickEventManagement.isAcceptClickEvent)
		{
			base.transform.parent.gameObject.SetActive(value: false);
			if (ApplicationModel.currentEditedImg is PixleEditedImg)
			{
				pixelArtWorkshop.SetActive(value: true);
			}
			else
			{
				StickerCanvas.SetActive(value: true);
			}
		}
	}

	public void GotoMyWork()
	{
		if (ClickEventManagement.isAcceptClickEvent)
		{
			UIRoot.ShowUI(MyworkCanvas);
			ShareCanvas.SetActive(value: false);
			DragEventProxy.Instance.SendEnterMyworkCanvas();
			ApplicationModel.CurrentCanvasName = "MyworkCanvas";
			TotalGA.Event("share_page_event", "finish");
		}
	}
}
