using UnityEngine;
using UnityEngine.UI;

public class ApplyArtistController : MonoBehaviour
{
	[SerializeField]
	public GameObject nameField;

	[SerializeField]
	public GameObject contactField;

	[SerializeField]
	public GameObject introField;

	[SerializeField]
	public GameObject resultPanel;

	[SerializeField]
	private GameObject Setting;

	[SerializeField]
	private GameObject MyworkCanvas;

	private Color redCol;

	private Color normalCol;

	private string redColStr = "#f11f23ff";

	private string normalColStr = "#999999ff";

	public void Back()
	{
		base.transform.parent.gameObject.SetActive(value: false);
		Setting.SetActive(value: true);
	}

	public void GotoUserInfoPage()
	{
		base.transform.parent.gameObject.SetActive(value: false);
		UIRoot.ShowUI(MyworkCanvas);
	}

	private void Awake()
	{
		ColorUtility.TryParseHtmlString(redColStr, out redCol);
		ColorUtility.TryParseHtmlString(normalColStr, out normalCol);
	}

	private void OnEnable()
	{
		GameQuit.Back += Back;
		init();
	}

	private void OnDisable()
	{
		GameQuit.Back -= Back;
	}

	private void init()
	{
		resultPanel.SetActive(value: false);
		nameField.transform.Find("Placeholder").GetComponent<Text>().text = "姓名";
		nameField.transform.Find("Placeholder").GetComponent<Text>().color = normalCol;
		contactField.transform.Find("Placeholder").GetComponent<Text>().text = "电话/QQ/邮箱";
		contactField.transform.Find("Placeholder").GetComponent<Text>().color = normalCol;
	}

	public void Onsubmmit()
	{
		if (!PublicUserInfoApi.IsHasLogin())
		{
			DragEventProxy.Instance.SendOffLineMsg("ApplicationArtistCanvas");
			return;
		}
		string text = nameField.transform.Find("Text").GetComponent<Text>().text;
		string text2 = contactField.transform.Find("Text").GetComponent<Text>().text;
		string text3 = introField.transform.Find("Text").GetComponent<Text>().text;
		if (verify(text, text2, text3))
		{
			PublicControllerApi.ApplyAritst(text, text2, text3, SubmitCallback);
		}
	}

	private bool verify(string name, string contact, string introduction)
	{
		bool result = true;
		if (name == string.Empty)
		{
			nameField.transform.Find("Placeholder").GetComponent<Text>().text = "*请填写您的姓名";
			nameField.transform.Find("Placeholder").GetComponent<Text>().color = redCol;
			result = false;
		}
		if (contact == string.Empty)
		{
			contactField.transform.Find("Placeholder").GetComponent<Text>().text = "*请填写您的联系方式";
			contactField.transform.Find("Placeholder").GetComponent<Text>().color = redCol;
			result = false;
		}
		if (introduction == string.Empty)
		{
			result = false;
		}
		return result;
	}

	public void SubmitCallback(Errcode err)
	{
		resultPanel.SetActive(value: true);
		setresultPanelChildFalse();
		if (err.errorCode == Errcode.OK)
		{
			resultPanel.transform.Find("SuccessPanel").gameObject.SetActive(value: true);
		}
		else if (err.errorCode == Errcode.HAS_APPLY)
		{
			resultPanel.transform.Find("hasApplyPanel").gameObject.SetActive(value: true);
		}
		else
		{
			resultPanel.transform.Find("FAilPanel").gameObject.SetActive(value: true);
		}
	}

	private void setresultPanelChildFalse()
	{
		resultPanel.transform.GetChild(0).gameObject.SetActive(value: false);
		resultPanel.transform.GetChild(1).gameObject.SetActive(value: false);
		resultPanel.transform.GetChild(2).gameObject.SetActive(value: false);
	}
}
