using System;
using UnityEngine;

public class JumpToLoadingCanvas : MonoBehaviour
{
	private void Start()
	{
		LeanTween.alpha(base.gameObject.GetComponent<RectTransform>(), 0f, 0.5f).setOnComplete((Action)delegate
		{
			base.transform.parent.parent.parent.Find("LoadingCanvas").gameObject.SetActive(value: true);
		});
	}

	private void Update()
	{
	}
}
