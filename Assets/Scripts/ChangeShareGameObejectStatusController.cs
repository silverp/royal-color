using UnityEngine;

public class ChangeShareGameObejectStatusController : MonoBehaviour
{
	[SerializeField]
	private GameObject UITemplateGameObject;

	private void OnEnable()
	{
		UITemplateGameObject.SetActive(value: false);
	}

	private void OnDisable()
	{
		UITemplateGameObject.SetActive(value: true);
	}
}
