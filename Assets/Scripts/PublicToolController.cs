using Mgl;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public class PublicToolController : MonoBehaviour
{
	private static I18n i18n = I18n.Instance;

	private static readonly int memorySize = SystemInfo.systemMemorySize;

	public static string GetTextByLanguage(string text)
	{
		return i18n.__(text);
	}

	public static void FileCreatorBytes(byte[] fileData, string fileName, bool isStoreChange)
	{
		string text = Application.persistentDataPath + "/" + fileName + ".txt";
		UnityEngine.Debug.Log("will save to disk " + text);
		if (File.Exists(text))
		{
			if (!isStoreChange)
			{
				return;
			}
			File.Delete(text);
		}
		FileStream fileStream = File.Create(text);
		fileStream.Write(fileData, 0, fileData.Length);
		fileStream.Close();
	}

	public static string RandomString(int Size)
	{
		System.Random random = new System.Random();
		string text = "abcdefghijklmnopqrstuvwxyz0123456789";
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < Size; i++)
		{
			char value = text[random.Next(0, text.Length)];
			stringBuilder.Append(value);
		}
		return stringBuilder.ToString();
	}

	public static void FileCreatorBytes(byte[] fileData, string fileName)
	{
		FileCreatorBytes(fileData, fileName, isStoreChange: false);
	}

	public static void FileCopier(string fileName, string copiedFileName)
	{
		string sourceFileName = Application.persistentDataPath + "/" + fileName + ".txt";
		string destFileName = Application.persistentDataPath + "/" + copiedFileName + ".txt";
		File.Copy(sourceFileName, destFileName, overwrite: true);
	}

	public static byte[] FileReaderBytes(string filePath)
	{
		byte[] result = null;
		string path = Application.persistentDataPath + "/" + filePath + ".txt";
		if (File.Exists(path))
		{
			result = File.ReadAllBytes(path);
		}
		return result;
	}

	public static bool IsExistFile(string filePath)
	{
		string path = Application.persistentDataPath + "/" + filePath + ".txt";
		if (File.Exists(path))
		{
			return true;
		}
		return false;
	}

	public static void FileCreatorBytesForJpg(byte[] fileData, string fileName)
	{
		string path = Application.persistentDataPath + "/" + fileName + ".jpg";
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		FileStream fileStream = File.Create(path);
		fileStream.Write(fileData, 0, fileData.Length);
		fileStream.Close();
	}

	public static void FileCreatorBytesForPng(byte[] fileData, string fileName)
	{
		string path = Application.persistentDataPath + "/" + fileName + ".png";
		if (File.Exists(path))
		{
			File.Delete(path);
		}
		FileStream fileStream = File.Create(path);
		fileStream.Write(fileData, 0, fileData.Length);
		fileStream.Close();
	}

	public static GameObject FindParentWithTag(GameObject childObject, string tag)
	{
		Transform transform = childObject.transform;
		while (transform.parent != null)
		{
			if (transform.parent.tag == tag)
			{
				return transform.parent.gameObject;
			}
			transform = transform.parent.transform;
		}
		return null;
	}

	public static void SetImageEditedAsImageIdSame(string imageId)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			foreach (ResourceImg img in cateResource.imgList)
			{
				if (img.imageId == imageId)
				{
					img.SetEdited();
				}
			}
		}
	}

	public static int compareTime(string timeStamp1, string timeStamp2)
	{
		DateTime t = ConvertStringToDateTime(timeStamp1);
		DateTime t2 = ConvertStringToDateTime(timeStamp2);
		return DateTime.Compare(t, t2);
	}

	public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
	{
		return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimeStamp).ToLocalTime();
	}

	public static DateTime ConvertStringToDateTime(string timeStamp)
	{
		DateTime dateTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
		long ticks = long.Parse(timeStamp + "0000");
		TimeSpan value = new TimeSpan(ticks);
		return dateTime.Add(value);
	}

	public static string CountDiffTimeByTimeStamp(string timeStamp)
	{
		TimeSpan timeSpan = new TimeSpan(DateTime.Now.Ticks);
		TimeSpan ts = new TimeSpan(ConvertStringToDateTime(timeStamp).Ticks);
		TimeSpan ts2 = timeSpan.Subtract(ts).Duration();
		//if (ApplicationModel.LANGUAGE == "zh-cn")
		//{
		//	string text = ts2.Hours.ToString() + "h前";
		//}
		//else
		//{
		//	string text = ts2.Hours.ToString() + "hago";
		//}
		return ConvertDiffTimeStr(ts2);
	}

	public static int CountDiffDaysByTimeStamp(string timeStamp)
	{
		TimeSpan timeSpan = new TimeSpan(DateTime.Now.Ticks);
		TimeSpan ts = new TimeSpan(ConvertStringToDateTime(timeStamp).Ticks);
		TimeSpan timeSpan2 = timeSpan.Subtract(ts).Duration();
		//string text = timeSpan2.Hours.ToString();
		return timeSpan2.Days;
	}

	public static string ConvertDiffTimeStr(TimeSpan ts)
	{
		string time = string.Empty;
		if (ts.Days >= 1)
		{
			time = ((!(ApplicationModel.LANGUAGE == "zh-cn")) ? (ts.Days + "days") : (ts.Days + "天"));
		}
		else if (ts.Hours < 1)
		{
			if (ts.Minutes < 1)
			{
				return GetTextByLanguage("just now");
			}
			time = ts.Minutes.ToString() + " min";
		}
		else
		{
			time = ts.Hours.ToString() + "h";
		}
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			return time + "前";
		}
		return time + " ago";
	}

	public static string GetShareStrByCount(int count)
	{
		string empty = string.Empty;
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			return "已经创作" + count.ToString() + "幅作品";
		}
		return count.ToString() + " masterpieces";
	}

	public static int GetMaxRenderPdfWith()
	{
		int deviceMemoryByGbytes = GetDeviceMemoryByGbytes(memorySize);
		if (deviceMemoryByGbytes == 1)
		{
			return 1024;
		}
		return 2048;
	}

	private static int GetDeviceMemoryByGbytes(int realSize)
	{
		if (realSize < 1024)
		{
			return 1;
		}
		if (realSize >= 1024 && realSize < 2024)
		{
			return 2;
		}
		if (realSize >= 2024 && realSize < 3072)
		{
			return 3;
		}
		return 4;
	}

	public static string GetPlatformName()
	{
		return Application.platform == RuntimePlatform.Android ? "android" : "iOS";
	}

	public static long UnixTimestampFromDateTime(DateTime date)
	{
		long num = date.Ticks - new DateTime(1970, 1, 1).Ticks;
		return num / 10000000;
	}

	public static void SortBaseEditedImgByTime(List<BaseEditedImg> list)
	{
		UnityEngine.Debug.Log("before SortBaseEditedImgByTime list is:" + JsonConvert.SerializeObject(list));
		list.Sort((BaseEditedImg img1, BaseEditedImg img2) => compareTime(img1.create.ToString(), img2.create.ToString()));
		UnityEngine.Debug.Log("after SortBaseEditedImgByTime list is:" + JsonConvert.SerializeObject(list));
	}

	public static Texture2D GetTexture2dFromLocalByPath(string path)
	{
		Texture2D texture2D = new Texture2D(8, 8, TextureFormat.ARGB32, mipChain: false);
		UnityEngine.Debug.Log(path + "length is:" + FileReaderBytes(path).Length);
		texture2D.LoadImage(FileReaderBytes(path), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}
}
