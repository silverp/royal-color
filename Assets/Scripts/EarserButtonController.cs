using System;
using UniRx;
using UnityEngine;

public class EarserButtonController : MonoBehaviour
{
	protected IDisposable subscribe;

	private int index;

	private void Start()
	{
		subscribe = MessageBroker.Default.Receive<PixelColorButtonController.ColorIndex>().Subscribe(onColorChange);
		UnityEngine.Debug.Log("init :PixelColorButton!");
	}

	public void onColorChange(PixelColorButtonController.ColorIndex idx)
	{
		if (idx.id == index)
		{
			UnityEngine.Debug.Log("onColorChange: id:" + idx.id + " " + index + " " + idx.action);
			base.transform.GetChild(0).gameObject.SetActive(value: true);
			base.transform.GetChild(1).gameObject.SetActive(value: false);
		}
		else
		{
			base.transform.GetChild(0).gameObject.SetActive(value: false);
			base.transform.GetChild(1).gameObject.SetActive(value: true);
		}
	}

	private void Update()
	{
	}
}
