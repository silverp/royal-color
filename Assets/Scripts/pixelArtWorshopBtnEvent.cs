using UnityEngine;

public class pixelArtWorshopBtnEvent : MonoBehaviour
{
	[SerializeField]
	private GameObject ShareCanvas;

	[SerializeField]
	private GameObject PixelArtDetailCanvas;

	[SerializeField]
	private GameObject helpBox;

	[SerializeField]
	private GameObject canvasList;

	public string PixelNoteStr = "PIXEL_NOTE";

	public void Back()
	{
		base.transform.parent.gameObject.SetActive(value: false);
		UIRoot.ShowBack(canvasList, ApplicationModel.lastVisibleScreen); 
	}

	private void Start()
	{
		if (PlayerPrefs.GetString(PixelNoteStr) != "ok")
		{
			UnityEngine.Debug.Log("helpBox.SetActive.....");
			helpBox.SetActive(value: true);
		}
	}

	public void ShowHelpBox()
	{
		helpBox.SetActive(value: true);
	}

	public void Finish()
	{
		base.transform.parent.gameObject.SetActive(value: false);
		ShareCanvas.SetActive(value: true);
	}

	private void OnDisable()
	{
		helpBox.SetActive(value: false);
		saveNoteToPlayerfab();
	}

	private void saveNoteToPlayerfab()
	{
		PlayerPrefs.SetString(PixelNoteStr, "ok");
		PlayerPrefs.Save();
	}

	private void OnEnable()
	{
		PixelArtDetailCanvas.SetActive(value: false);
	}
}
