public class Errcode
{
	public int errorCode;

	public static int OK = 10000;

	public static int FAIL = 10001;

	public static int NOT_LOGIN = 10002;

	public static int BEAN_NOT_ENOUGH = 10003;

	public static int HAS_PUBLISHED = 10004;

	public static int SHARE_CANCEL = 10005;

	public static int LOGIN_CANCEL = 10006;

	public static int HAS_APPLY = 10007;

	public static int OFF_LINE = 10008;
}
