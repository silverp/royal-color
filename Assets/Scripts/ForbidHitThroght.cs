using System;
using System.Collections;
using UniRx;
using UnityEngine;

public class ForbidHitThroght : MonoBehaviour
{
	public class ForbidHit
	{
	}

	private Vector3 originScale;

	private float scaleTime = 0.2f;

	private float maxScaleFactor = 0.2f;

	[SerializeField]
	private GameObject rawImage;

	private PixelDrawController pixelDrawController;

	private void Awake()
	{
	}

	private void Start()
	{
		if (rawImage != null)
		{
			pixelDrawController = rawImage.transform.GetComponent<PixelDrawController>();
		}
		MessageBroker.Default.Receive<ForbidHit>().Subscribe(delegate
		{
			ForbidThoughtWithoutScaleChange();
		});
	}

	private IEnumerator scale()
	{
		float eslapedTime = 0f;
		while (eslapedTime < scaleTime)
		{
			eslapedTime += Time.deltaTime;
			base.transform.localScale = originScale * (1f + maxScaleFactor * Mathf.Sin((float)Math.PI * eslapedTime / scaleTime));
			yield return null;
		}
		base.transform.localScale = originScale;
	}

	public void ForbidThoughtWithoutScaleChange()
	{
		pixelDrawController.EventLock = true;
	}

	public void ForbidThoughtWithScaleChange()
	{
		originScale = base.transform.localScale;
		StartCoroutine(scale());
		pixelDrawController.EventLock = true;
	}
}
