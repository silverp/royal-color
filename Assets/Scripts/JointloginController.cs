using cn.sharesdk.unity3d;
using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class JointloginController : MonoBehaviour
{
	[SerializeField]
	private GameObject Login;

	[SerializeField]
	private GameObject canvasList;

	[SerializeField]
	private GameObject noteText;

	[SerializeField]
	private GameObject loginBtnPanel;

	[SerializeField]
	private GameObject LoginStatus;

	private float startTime;

	private bool IsShowMsg;

	private GUIStyle guiStyle = new GUIStyle();

	private string ShareMsg;

	private float WaitTime = 3f;

	private bool shareClock;

	private Action<Errcode> shareCallback;

	private float jumpStart;

	private void OnEnable()
	{
		ShareSdkManager.LoginErrorCallback += LoginFail;
		ShareSdkManager.LoginSuccessCallback += LoginSuccessCallback;
	}

	private void OnDisable()
	{
		ShareSdkManager.LoginErrorCallback -= LoginFail;
		ShareSdkManager.LoginSuccessCallback -= LoginSuccessCallback;
		IsShowMsg = false;
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(WaitTime);
		IsShowMsg = false;
	}

	protected void LoginSuccessCallback(Hashtable result, Hashtable userinfo, PlatformType type)
	{
		UnityEngine.Debug.Log("result is:" + (result == null));
		UnityEngine.Debug.Log("result is:" + MiniJSON.jsonEncode(result));
		UnityEngine.Debug.Log("userinfo is:" + (result == null));
		UnityEngine.Debug.Log("userinfo is:" + MiniJSON.jsonEncode(userinfo));
		noteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("Login successful,jumping..");
		UnityEngine.Debug.Log("loginCallback time is:" + (Time.realtimeSinceStartup - jumpStart));
		string url = ApplicationModel.HOST + "/user/login";
		NetLoadcontroller.Instance.RequestUrlWidthMethodPost(url, GetPostFormByEditedImg(result, userinfo, type), delegate(string json, bool isSuccess)
		{
			RequestLoginCallback(json, isSuccess);
		});
	}

	private void RequestLoginCallback(string json, bool isSuccess)
	{
		UnityEngine.Debug.Log("RequestLoginCallback info time is:" + (Time.realtimeSinceStartup - jumpStart));
		if (isSuccess)
		{
			LoginReturnjson loginReturnjson = JsonConvert.DeserializeObject<LoginReturnjson>(json);
			if (loginReturnjson.status == 0)
			{
				ApplicationModel.sessionId = loginReturnjson.data.sessionId;
				UnityEngine.Debug.Log("login sessionId is:" + ApplicationModel.sessionId);
				ApplicationModel.SaveSessionIdTolocal();
				if (PublicUserInfoApi.DeviceIsHasVip())
				{
					UnityEngine.Debug.Log("device has vip info ,need to merge!!!");
					PublicControllerApi.mergeDeviceInfo(ApplicationModel.device_id, ApplicationModel.sessionId, delegate(Errcode x)
					{
						MergeVipInfoCallback(loginReturnjson.data.sessionId, x);
					});
				}
				else
				{
					PublicUserInfoApi.RequestUserInfo(loginReturnjson.data.sessionId, RequestUserInfoCallback);
				}
			}
			else
			{
				LoginFail();
			}
		}
		else
		{
			LoginFail();
		}
	}

	private void MergeVipInfoCallback(string sessionId, Errcode err)
	{
		UnityEngine.Debug.Log("MergeVipInfoCallback err code is:" + err.errorCode);
		PublicUserInfoApi.RequestUserInfo(sessionId, RequestUserInfoCallback);
	}

	private void RequestUserInfoCallback(Errcode err)
	{
		UnityEngine.Debug.Log("RequestUserInfoCallback info time is:" + (Time.realtimeSinceStartup - jumpStart));
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("ApplicationModel.userInfo is:" + JsonConvert.SerializeObject(ApplicationModel.userInfo));
			PublicUserInfoApi.JudgeProductIsExpire();
			PublicUserInfoApi.StartMergeData(MergeDataCallBack);
		}
		else
		{
			UnityEngine.Debug.Log("request error!!!");
			LoginFail();
		}
	}

	private void FirstLoginJudge(bool isFirstLogin)
	{
		if (isFirstLogin)
		{
			BeanAction.request(BeanAction.FIRST_LOGIN, BeanActionCallback);
		}
	}

	private void BeanActionCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			ApplicationModel.userInfo.BeanCnt += amount;
		}
	}

	public void login()
	{
		startTime = Time.realtimeSinceStartup;
		if (loginBtnPanel != null)
		{
			loginBtnPanel.SetActive(value: false);
		}
		if (LoginStatus != null)
		{
			LoginStatus.SetActive(value: true);
		}
		noteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("Log in...");
		UnityEngine.Debug.Log(" Clicked login ");
	}

	private void MergeDataCallBack()
	{
		UnityEngine.Debug.Log("merge data ok after login");
		UnityEngine.Debug.Log("MergeDataCallBack info time is:" + (Time.realtimeSinceStartup - jumpStart));
		Login.SetActive(value: false);
        UIRoot.ShowBack(canvasList, ApplicationModel.lastVisibleScreen);
        DragEventProxy.Instance.SendRefreshAfterLoginMsg();
	}

	private void LoginFail(Errcode err = null, PlatformType type = PlatformType.WeChat)
	{
		if (err != null && err.errorCode == Errcode.LOGIN_CANCEL)
		{
			noteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("Logon failure");
		}
		else
		{
			noteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("Logon failure");
		}
		Login.SetActive(value: false);
        UIRoot.ShowBack(canvasList, ApplicationModel.lastVisibleScreen);
	}

	private WWWForm GetPostFormByEditedImg(Hashtable result, Hashtable userinfo, PlatformType type)
	{
		switch (type)
		{
		case PlatformType.WeChat:
			return GetPostFormByEditedImgWithWechat(result);
		case PlatformType.SinaWeibo:
			return GetPostFormByEditedImgWidthWeibo(result);
		case PlatformType.QQ:
			return GetPostFormByEditedImgWidthQQ(result, userinfo);
		case PlatformType.Facebook:
			return GetPostFormByEditedImgWidthFb(result, userinfo);
		default:
			return GetPostFormByEditedImgWithWechat(result);
		}
	}

	private WWWForm GetPostFormByEditedImgWithWechat(Hashtable result)
	{
		UnityEngine.Debug.Log("GetPostFormByEditedImg...");
		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("openid", result["openid"].ToString());
		wWWForm.AddField("nickname", result["nickname"].ToString());
		wWWForm.AddField("sex", result["sex"].ToString());
		wWWForm.AddField("province", result["province"].ToString());
		wWWForm.AddField("city", result["city"].ToString());
		wWWForm.AddField("country", result["country"].ToString());
		wWWForm.AddField("headimgurl", result["headimgurl"].ToString());
		wWWForm.AddField("privilege", result["privilege"].ToString());
		wWWForm.AddField("unionid", result["unionid"].ToString());
		wWWForm.AddField("device", SystemInfo.deviceModel);
		wWWForm.AddField("platform", "wechat");
		return wWWForm;
	}

	private WWWForm GetPostFormByEditedImgWidthWeibo(Hashtable result)
	{
		UnityEngine.Debug.Log("result is:" + MiniJSON.jsonEncode(result));
		UnityEngine.Debug.Log("GetPostFormByEditedImg...");
		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("openid", result["id"].ToString());
		wWWForm.AddField("nickname", result["name"].ToString());
		wWWForm.AddField("sex", ConvertGender(result["gender"].ToString()));
		wWWForm.AddField("headimgurl", result["avatar_large"].ToString());
		wWWForm.AddField("device", SystemInfo.deviceModel);
		wWWForm.AddField("platform", "sinaWeibo");
		return wWWForm;
	}

	private WWWForm GetPostFormByEditedImgWidthFb(Hashtable result, Hashtable userinfo)
	{
		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("openid", userinfo["userID"].ToString());
		wWWForm.AddField("nickname", userinfo["userName"].ToString());
		wWWForm.AddField("sex", ConvertGender(userinfo["userGender"].ToString()));
		wWWForm.AddField("headimgurl", userinfo["userIcon"].ToString());
		wWWForm.AddField("device", SystemInfo.deviceModel);
		wWWForm.AddField("platform", "facebook");
		return wWWForm;
	}

	private WWWForm GetPostFormByEditedImgWidthQQ(Hashtable result, Hashtable userinfo)
	{
		UnityEngine.Debug.Log("result is:" + MiniJSON.jsonEncode(result));
		UnityEngine.Debug.Log("GetPostFormByEditedImg...");
		WWWForm wWWForm = new WWWForm();
		wWWForm.AddField("openid", userinfo["userID"].ToString());
		wWWForm.AddField("nickname", userinfo["userName"].ToString());
		wWWForm.AddField("sex", ConvertGender(userinfo["userGender"].ToString()));
		wWWForm.AddField("headimgurl", userinfo["userIcon"].ToString());
		wWWForm.AddField("device", SystemInfo.deviceModel);
		wWWForm.AddField("platform", "QQ");
		return wWWForm;
	}

	private string ConvertGender(string gender)
	{
		if (gender == "m")
		{
			return "1";
		}
		return "2";
	}
}
