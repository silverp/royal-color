using UnityEngine;
using UnityEngine.UI;

public class ChangeTexByLanguage : MonoBehaviour
{
	public int days;

	public int times;

	private void Start()
	{
		if (ApplicationModel.LANGUAGE == "en")
		{
			GetComponent<Text>().text = days + "days," + times + "creations";
		}
		else if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			GetComponent<Text>().text = days + "天," + times + "次创作";
		}
	}

	private void Update()
	{
	}
}
