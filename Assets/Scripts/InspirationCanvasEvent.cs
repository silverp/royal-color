using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InspirationCanvasEvent : MonoBehaviour
{
	[SerializeField]
	private GameObject OtherWorkCanvas;

	[SerializeField]
	private GameObject BeanBox;

	[SerializeField]
	private GameObject BeanNoteBox;

	[SerializeField]
	private GameObject top;

	[SerializeField]
	private RectTransform content;

	[SerializeField]
	private GameObject MyworkCanvas;

	private bool isNetError;

	private GameObject CurrentSpin;

	[SerializeField]
	private GameObject InspirationCanvas;

	[SerializeField]
	private GameObject workShopCanvas;

	public static InspirationCanvasEvent instance;

	private GUIStyle guiStyle = new GUIStyle();

	private float MAX_WAIT_TIME = 5f;

	private float time;

	private bool isHasNoted;

	private bool isHasCompletedEvent;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		// 	InspirationCanvas = PublicToolController.FindParentWithTag(base.gameObject, "InspirationCanvas");
		// 	workShopCanvas = InspirationCanvas.transform.parent.Find("WorkShopCanvas").gameObject;
	}

	private void OnEnable()
	{
		OtherWorkCanvas.SetActive(value: false);
		// UIRoot.HideUI();
	}

	private void Update()
	{
		if (time > 0f)
		{
			time -= Time.deltaTime;
		}
		if (time < 0f && !isHasCompletedEvent && CurrentSpin.activeSelf)
		{
			CurrentSpin.SetActive(value: false);
			isNetError = true;
		}
		if (base.gameObject.activeSelf)
		{
			Vector2 anchoredPosition = content.anchoredPosition;
			if (anchoredPosition.y > 2000f && !top.activeSelf)
			{
				top.SetActive(value: true);
			}
		}
		if (base.gameObject.activeSelf)
		{
			Vector2 anchoredPosition2 = content.anchoredPosition;
			if (anchoredPosition2.y < 2000f && top.activeSelf)
			{
				top.SetActive(value: false);
			}
		}
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(2f);
		isNetError = false;
	}

	private void OnGUI()
	{
		if (isNetError)
		{
			guiStyle.fontSize = 34;
			guiStyle.normal.textColor = Color.black;
			guiStyle.alignment = TextAnchor.MiddleCenter;
			GUI.color = Color.black;
			GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 300, 100f, 80f), "网络异常！", guiStyle);
			StartCoroutine(showSavedInfo());
		}
	}

	public void GotoZoomEvent(Texture2D tex)
	{
		base.transform.parent.Find("ZoomPanel").GetComponent<ZoomPanelBtnController>().curTex = tex;
		base.transform.parent.Find("ZoomPanel").gameObject.SetActive(value: true);
	}

	public void GotoOtherCanvas(ColoredImage img)
	{
		UnityEngine.Debug.Log("other img is:" + JsonConvert.SerializeObject(img));
		PublicUserInfoApi.AssignOtherUserInfo(img.user);
		UIRoot.HideUI();
		OtherWorkCanvas.SetActive(value: true);
		ApplicationModel.lastVisibleScreen = "InspirationCanvas";
	}

	public void GotWorkShop(ColoredImage img)
	{
		if (ApplicationModel.userInfo == null)
		{
			DragEventProxy.Instance.SendOffLineMsg("InspirationCanvas");
			return;
		}
		ResourceImg resourceImgFromColoredimg = PublicImgaeApi.GetResourceImgFromColoredimg(img.img.imageId);
		UnityEngine.Debug.Log("inspiration resourceimg is:" + JsonConvert.SerializeObject(resourceImgFromColoredimg));
		if (resourceImgFromColoredimg == null)
		{
			PublicControllerApi.GetOriginImageDetailInfo(img.img.imageId, delegate(OriginDetail x)
			{
				RequestResourceimgCallback(x);
			});
		}
		else
		{
			AfterGetResourceImg(resourceImgFromColoredimg);
		}
	}

	private void AfterGetResourceImg(ResourceImg resourceImg)
	{
		bool flag = UnlockArtManager.Instance.IsShowLock(resourceImg);
		UnityEngine.Debug.Log("inspiration GotWorkShop is:" + flag);
		if (flag)
		{
			UnlockArtManager.Instance.UnlockArt(resourceImg, delegate(Errcode err, int amount)
			{
				unlockCallback(err, amount, resourceImg);
			});
		}
		else
		{
			EnterWorkShopCanvas(resourceImg.imageId);
		}
	}

	private void RequestResourceimgCallback(OriginDetail imgInfo)
	{
		ResourceImg resourceImg = new ResourceImg(imgInfo.imageId, imgInfo.outline, imgInfo.index, imgInfo.thumbnailUrl, imgInfo.locked);
		AfterGetResourceImg(resourceImg);
	}

	private void unlockCallback(Errcode err, int amount, ResourceImg resurceImg)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("inspiration unlockCallback ok");
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 0.6f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				ApplicationModel.userInfo.BeanCnt += amount;
				EnterWorkShopCanvas(resurceImg.imageId);
			});
		}
		else
		{
			DragEventProxy.Instance.SendUnclockOutlineMsg(resurceImg, null, ImageType.normal);
		}
	}

	public void EnterWorkShopCanvas(string imageId)
	{
		EditedImg img = (EditedImg)(ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedImgFromEditedList(imageId, isCreate: true));
		FileUploadEventProxy.Instance.SendClickImageMsg(img, ApplicationModel.device_id);
		UIRoot.HideUI();
		workShopCanvas.gameObject.SetActive(value: true);
	}

	public void ShareCallback(Errcode err)
	{
		if (err.errorCode == Errcode.OK && PublicUserInfoApi.IsHasLogin())
		{
			BeanAction.request(BeanAction.SHARE, beanActionCallback);
		}
	}

	private void beanActionCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			BeanBox.SetActive(value: true);
			ApplicationModel.userInfo.BeanCnt += amount;
			BeanBox.transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = PublicToolController.GetTextByLanguage("Share success");
				BeanBox.transform.GetChild(1).GetChild(4).GetComponent<Text>()
					.text = "+" + amount;
				}
			}
		}
