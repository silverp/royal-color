
using UnityEngine;

public class UmengManager : MonoBehaviour
{
	private void Awake()
	{
		Object.DontDestroyOnLoad(base.transform.gameObject);
	}

	private void OnApplicationPause(bool isPause)
	{
		if (isPause)
		{
			TotalGA.Event("onPause");
		}
		else
		{
            TotalGA.Event("onResume"); 
		}
	}

	private void OnApplicationQuit()
    {
        TotalGA.Event("onKillProcess");
        //Analytics.onKillProcess();
    }
}
