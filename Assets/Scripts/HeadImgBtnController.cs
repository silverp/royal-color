using UnityEngine;
using UnityEngine.UI;

public class HeadImgBtnController : MonoBehaviour
{
	private Button button;

	private void Start()
	{
		button = GetComponent<Button>();
		button.onClick.AddListener(ClickHeadImgBtn);
	}

	private void ClickHeadImgBtn()
	{
		if (ApplicationModel.userInfo == null)
		{
			DragEventProxy.Instance.SendOffLineMsg("MyworkCanvas");
		}
	}

	private void Update()
	{
	}
}
