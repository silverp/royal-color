using cn.sharesdk.unity3d;
using UnityEngine;

public class LoginPanelController : MonoBehaviour
{
	[SerializeField]
	private GameObject LoginPanel;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject wechatBtn;

	[SerializeField]
	private GameObject QQBtn;

	[SerializeField]
	private GameObject shareSdk;

	private Transform canvasList;

	private void Start()
	{
		canvasList = base.transform.parent;
		if (!shareSdk.GetComponent<ShareSdkManager>().IsShowIcon(PlatformType.WeChat))
		{
			wechatBtn.SetActive(value: false);
		}
		if (!shareSdk.GetComponent<ShareSdkManager>().IsShowIcon(PlatformType.QQ))
		{
			QQBtn.SetActive(value: false);
		}
	}

	private void Update()
	{
	}

	public void CloseLoginPanel()
	{
		LoginPanel.gameObject.SetActive(value: false);

		UIRoot.ShowBack(canvasList.gameObject, ApplicationModel.ReturnPageName);
	}
}
