using UnityEngine;
using UnityEngine.UI;

public class HeadImageController : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("change head img");
		if (ApplicationModel.userInfo != null && ApplicationModel.userInfo.HeadimgUrl != null)
		{
			NetLoadcontroller.Instance.RequestImg(ApplicationModel.userInfo.HeadimgUrl, delegate(Texture2D tex)
			{
				changeImage(tex);
			});
			return;
		}
		Texture2D tex2 = Resources.Load<Texture2D>("Images/Mywork/default_headimg");
		changeImage(tex2);
	}

	private void changeImage(Texture2D tex)
	{
		base.transform.GetChild(1).GetChild(0).GetComponent<RawImage>()
			.texture = tex;
		}
	}
