using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InspirationScrollRectAdapter : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class DoubleImageModel
	{
		public ColoredImage img;

		public int index;

		public DoubleImageModel(ColoredImage img, int index)
		{
			this.img = img;
			this.index = index;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public RawImage rawImage;

		public string Uid;

		public int downloadStatus;

		public Button continueBtn;

		public GameObject headImg;

		public Text timeText;

		public Transform nickName;

		public Button imageButton;

		public Button shareButton;

		public Button headButton;

		public Button likeButton;

		public GameObject likeBtnGameObject;

		public GameObject likeCntGameObject;

		public GameObject followBtn;

		public override void CollectViews()
		{
			Root = root;
			rawImage = root.GetChild(2).GetComponent<RawImage>();
			Uid = root.GetComponent<CardItemUserEvent>().curUid;
			downloadStatus = root.GetComponent<RawImageDataController>().downloadStatus;
			headImg = root.GetChild(1).Find("headimg").gameObject;
			timeText = root.GetChild(1).GetChild(2).GetComponent<Text>();
			nickName = root.GetChild(1).GetChild(1);
			continueBtn = root.Find("BottomPanel").Find("continue").GetComponent<Button>();
			imageButton = root.Find("Image").GetComponent<Button>();
			shareButton = root.GetChild(3).Find("share").GetComponent<Button>();
			headButton = root.GetChild(1).Find("headimg").GetComponent<Button>();
			likeButton = root.Find("BottomPanel").Find("like").GetComponent<Button>();
			likeBtnGameObject = root.Find("BottomPanel").Find("like").gameObject;
			likeCntGameObject = root.Find("BottomPanel").Find("like").Find("likeCnt")
				.gameObject;
				followBtn = root.Find("TopPanel").Find("follow").gameObject;
				base.CollectViews();
			}
		}

		public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
		{
			private float _PrefabSize;

			private float[] _ItemsSizessToUse;

			private List<DoubleImageModel> _Data;

			public RectTransform root;

			public MyWorkItemsViewHolder myWorkItemsViewHolder;

			private static Texture2D loadingTex = Resources.Load<Texture2D>("Images/inspiration/prefabImg");

			private static Texture2D defaultHeadTex = Resources.Load<Texture2D>("Images/share/touxiang_moren");

			public MyWorkScrollRectItemsAdapter(List<DoubleImageModel> data, MyWorkParams parms)
			{
				_Data = data;
				if (parms.scrollRect.horizontal)
				{
					_PrefabSize = parms.itemPrefab.rect.width;
				}
				else
				{
					_PrefabSize = parms.itemPrefab.rect.height;
				}
				InitSizes();
				Init(parms);
			}

			private void InitSizes()
			{
				int count = _Data.Count;
				if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
				{
					_ItemsSizessToUse = new float[count];
				}
				for (int i = 0; i < count; i++)
				{
					_ItemsSizessToUse[i] = _PrefabSize;
				}
			}

			public override void ChangeItemCountTo(int itemsCount)
			{
				InitSizes();
				base.ChangeItemCountTo(itemsCount);
			}

			protected override float GetItemHeight(int index)
			{
				if (index >= _ItemsSizessToUse.Length)
				{
					return 0f;
				}
				return _ItemsSizessToUse[index];
			}

			protected override float GetItemWidth(int index)
			{
				return _ItemsSizessToUse[index];
			}

			public void setImage(ColoredImage img, int index)
			{
				if (img != null)
				{
					myWorkItemsViewHolder.Uid = img.user.uid;
					myWorkItemsViewHolder.downloadStatus = 0;
					string _thumbUrl = img.img.thumb;
					if (_thumbUrl != string.Empty)
					{
						NetLoadcontroller.Instance.RequestImg(_thumbUrl, delegate(Texture2D tex)
						{
							callback(tex, img, myWorkItemsViewHolder.continueBtn, _thumbUrl);
						});
					}
					TextEmojiConvert.Instance.ConvertEmoji(myWorkItemsViewHolder.nickName, img.user.nickname, isBig: false);
					myWorkItemsViewHolder.timeText.text = PublicToolController.CountDiffTimeByTimeStamp(img.create.ToString());
					myWorkItemsViewHolder.headImg.transform.Find("Image").GetComponent<RawImage>().texture = defaultHeadTex;
					if (img.user.headimgUrl != string.Empty)
					{
						string headimgUrl = img.user.headimgUrl;
						NetLoadcontroller.Instance.RequestImg(headimgUrl, delegate(Texture2D tex)
						{
							RequestHeadImgCallback(tex, img, myWorkItemsViewHolder.headImg, _thumbUrl);
						});
					}
					LikeBtnControl(img);
					FollowBtnController(img);
					myWorkItemsViewHolder.shareButton.onClick.RemoveAllListeners();
					myWorkItemsViewHolder.shareButton.onClick.AddListener(delegate
					{
						clickShareBtn(img);
					});
					myWorkItemsViewHolder.headButton.onClick.RemoveAllListeners();
					myWorkItemsViewHolder.headButton.onClick.AddListener(delegate
					{
						OnHeadImgHit(img);
					});
				}
				else
				{
					root.gameObject.SetActive(value: false);
				}
				if (index % 8 == 0)
				{
					PublicCallFrameTool.Instance.CollectGarbage();
				}
				if (index == ApplicationModel.ColoredImgList.Count - 8 || index == ApplicationModel.ColoredImgList.Count)
				{
					DragEventProxy.Instance.SendRequestNextPageMsg();
				}
			}

			private void clickShareBtn(ColoredImage img)
			{
				TotalGA.Event("inspiration_page_event", "share");
				string url = ApplicationModel.HOST + "/user/getWorkListCount?uid=" + img.user.uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false;
				NetLoadcontroller.Instance.RequestUrlWidthMethodGet(url, delegate(string json, bool isSuccess)
				{
					RequestWorkCountCallback(json, isSuccess, img);
				});
			}

			private void RequestWorkCountCallback(string json, bool isSuccess, ColoredImage img)
			{
				int count = 0;
				if (isSuccess)
				{
					WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
					if (workCountJson.status == 0)
					{
						count = workCountJson.data.count;
					}
				}
				NetLoadcontroller.Instance.RequestImg(img.img.thumb, delegate(Texture2D tex)
				{
					RequestFinalOnshare(tex, img.user.nickname, count);
				});
			}

			private void RequestFinalOnshare(Texture2D tex, string name, int count)
			{
				FileUploadEventProxy.Instance.SendShareWorkEventMsg(tex, name, count, isHasinfo: true, ShareCallback);
			}

			private void ShareCallback(Errcode err)
			{
				InspirationCanvasEvent.instance.ShareCallback(err);
			}

			private void LikeBtnControl(ColoredImage img)
			{
				Button likeButton = myWorkItemsViewHolder.likeButton;
				GameObject likeBtnGameObject = myWorkItemsViewHolder.likeBtnGameObject;
				GameObject likeCntGameObject = myWorkItemsViewHolder.likeCntGameObject;
				ChangeBtnStyle(img.isLike, likeBtnGameObject);
				likeCntGameObject.GetComponent<Text>().text = img.img.likeCnt.ToString();
				likeButton.onClick.RemoveAllListeners();
				likeButton.onClick.AddListener(delegate
				{
					OnHitLikeBtn(img, likeBtnGameObject, likeCntGameObject);
				});
			}

			private void FollowBtnController(ColoredImage img)
			{
				GameObject button = myWorkItemsViewHolder.followBtn;
				button.GetComponent<Button>().onClick.RemoveAllListeners();
				string _uid = img.user.uid;
				button.GetComponent<Button>().onClick.AddListener(delegate
				{
					clickFollowBtn(_uid, button);
				});
				FollowBtnControl(img.isFollow, button);
			}

			private bool IsFollowByBtn(GameObject followBtn)
			{
				if (followBtn.transform.GetChild(0).gameObject.activeSelf)
				{
					return false;
				}
				return true;
			}

			private void clickFollowBtn(string uid, GameObject followBtn)
			{
				bool flag = IsFollowByBtn(followBtn);
				if (ApplicationModel.userInfo == null)
				{
					DragEventProxy.Instance.SendOffLineMsg("InspirationCanvas");
					return;
				}
				flag = !flag;
				if (flag)
				{
					PublicControllerApi.AddFollow(uid, AddFollowCallback);
				}
				else
				{
					PublicControllerApi.RemoveFollow(uid, AddFollowCallback);
				}
				FollowBtnControl(flag, followBtn);
				SendClickFollowMsg(uid, flag);
			}

			private void AddFollowCallback(Errcode err)
			{
			}

			private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageURLAtRequest)
			{
				return _Data.Count > itemIndex && itemIdexAtRequest == itemIndex && imageURLAtRequest == _Data[itemIndex].img.img.thumb;
			}

			private void SendClickFollowMsg(string uid, bool isFollow)
			{
				if (isFollow)
				{
					DragEventProxy.Instance.SendAddFollowMsg(uid);
				}
				else
				{
					DragEventProxy.Instance.SendRemoveFollowMsg(uid);
				}
			}

			private void FollowBtnControl(bool isFollow, GameObject followBtn)
			{
				if (isFollow)
				{
					followBtn.transform.GetChild(1).gameObject.SetActive(value: true);
					followBtn.transform.GetChild(0).gameObject.SetActive(value: false);
				}
				else
				{
					followBtn.transform.GetChild(1).gameObject.SetActive(value: false);
					followBtn.transform.GetChild(0).gameObject.SetActive(value: true);
				}
			}

			private void OnHitLikeBtn(ColoredImage img, GameObject btn, GameObject likeCntBtn)
			{
				TotalGA.Event("inspiration_page_event", "like");
				if (ApplicationModel.userInfo == null)
				{
					DragEventProxy.Instance.SendOffLineMsg("InspirationCanvas");
					return;
				}
				img.isLike = !img.isLike;
				if (img.isLike)
				{
					img.img.likeCnt++;
				}
				else
				{
					img.img.likeCnt--;
				}
				likeCntBtn.GetComponent<Text>().text = img.img.likeCnt.ToString();
				ChangeBtnStyle(img.isLike, btn);
				FileUploadEventProxy.Instance.SendLikeEventMsg(img.user.uid, img.img.workId, img.isLike);
			}

			private void ChangeBtnStyle(bool isLike, GameObject likeBtn)
			{
				if (isLike)
				{
					likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_en");
				}
				else
				{
					likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_un");
				}
			}

			private void RequestHeadImgCallback(Texture2D tex, ColoredImage img, GameObject headImg, string requestUrl)
			{
				int itemIndex = myWorkItemsViewHolder.itemIndex;
				if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, requestUrl))
				{
					headImg.transform.Find("Image").GetComponent<RawImage>().texture = tex;
					headImg.GetComponent<Button>().onClick.AddListener(delegate
					{
						OnHeadImgHit(img);
					});
				}
			}

			public void DestoryAssets()
			{
				if (myWorkItemsViewHolder.downloadStatus == 1)
				{
					UnityEngine.Object.DestroyImmediate(myWorkItemsViewHolder.rawImage.texture);
				}
			}

			public void callback(Texture2D tex, ColoredImage img, Button continueBtn, string requestUrl)
			{
				int itemIndex = myWorkItemsViewHolder.itemIndex;
				if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, requestUrl))
				{
					myWorkItemsViewHolder.rawImage.texture = tex;
					ColoredImage coloredImage = img;
					myWorkItemsViewHolder.imageButton.onClick.RemoveAllListeners();
					myWorkItemsViewHolder.imageButton.onClick.AddListener(delegate
					{
						OnImageHit(tex);
					});
					myWorkItemsViewHolder.downloadStatus = 1;
					continueBtn.onClick.RemoveAllListeners();
					continueBtn.onClick.AddListener(delegate
					{
						OnContinueBtnHit(img);
					});
				}
			}

			private void OnContinueBtnHit(ColoredImage img)
			{
				TotalGA.Event("inspiration_page_event", "重画");
				DragEventProxy.Instance.SendRepaintMsg(img.img);
			}

			private void OnImageHit(Texture2D tex)
			{
				InspirationCanvasEvent.instance.GotoZoomEvent(tex);
			}

			private void OnHeadImgHit(ColoredImage img)
			{
				TotalGA.Event("inspiration_page_event", "点击头像");
				InspirationCanvasEvent.instance.GotoOtherCanvas(img);
			}

			protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
			{
				MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
				myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
				return myWorkItemsViewHolder;
			}

			protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
			{
				root = newOrRecycled.Root;
				myWorkItemsViewHolder = newOrRecycled;
				DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
				newOrRecycled.rawImage.texture = loadingTex;
				setImage(doubleImageModel.img, doubleImageModel.index);
			}
		}

		[SerializeField]
		private GameObject netErrorPanel;

		[SerializeField]
		private GameObject content;

		[SerializeField]
		private GameObject LoadingPanel;

		[SerializeField]
		private GameObject DownPanel;

		[SerializeField]
		private GameObject NetErrorPanel;

		[SerializeField]
		private MyWorkParams _ScrollRectAdapterParams;

		private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

		private List<DoubleImageModel> _Data = new List<DoubleImageModel>();

		private bool RefreshEventClock;

		private int step = 20;

		private bool isRefresh;

		public BaseParams Params => _ScrollRectAdapterParams;

		public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

		public List<DoubleImageModel> Data => _Data;

		private void AssginEvents()
		{
			DragEventProxy.OnRequestNextPage += RequestNextPage;
			DragEventProxy.OnAddFollow += addFollow;
			DragEventProxy.OnRemoveFollow += addFollow;
		}

		public void GotoScrollToStart()
		{
			_ScrollRectItemsAdapter.ScrollToStart();
		}

		private void DiscardEvents()
		{
			DragEventProxy.OnRequestNextPage -= RequestNextPage;
			DragEventProxy.OnAddFollow -= addFollow;
			DragEventProxy.OnRemoveFollow -= addFollow;
		}

		private void addFollow(string uid)
		{
			foreach (ColoredImage coloredImg in ApplicationModel.ColoredImgList)
			{
				if (uid == coloredImg.user.uid)
				{
					coloredImg.isFollow = true;
				}
			}
			foreach (DoubleImageModel datum in _Data)
			{
				if (datum.img.user.uid == uid)
				{
					datum.img.isFollow = true;
				}
			}
		}

		private void RefreshInspirationDataWhenEnter()
		{
			UnityEngine.Debug.Log("RefreshInspirationDataWhenEnter.....  ");
			GotoScrollToStart();
			isRefresh = true;
		}

		public void removeFollow(string uid)
		{
			foreach (ColoredImage coloredImg in ApplicationModel.ColoredImgList)
			{
				if (uid == coloredImg.user.uid)
				{
					coloredImg.isFollow = false;
				}
			}
			foreach (DoubleImageModel datum in _Data)
			{
				if (datum.img.user.uid == uid)
				{
					datum.img.isFollow = false;
				}
			}
		}

		private void Awake()
		{
			DragEventProxy.OnRefreshAfterLogin += RefreshInspirationDataWhenEnter;
			_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
		}

		private void OnEnable()
		{
			AssginEvents();
			ApplicationModel.lastVisibleScreen = "InspirationCanvas";
			if (ApplicationModel.ColoredImgList == null || ApplicationModel.ColoredImgList.Count == 0 || isRefresh)
			{
				ApplicationModel.ColoredImgList = null;
				HideChild(base.transform.GetChild(0).GetChild(0));
				PublicImgaeApi.GetInspirationImglist(InitInspirationCanvas, 1, step);
				isRefresh = false;
			}
			else
			{
				StartCoroutine(InitContent());
			}
		}

		private void InitInspirationCanvas(List<ColoredImage> coloredImglist)
		{
			ApplicationModel.ColoredImgList = coloredImglist;
			StartCoroutine(InitContent());
		}

		private void RefreshInsImgList()
		{
			PublicImgaeApi.GetInspirationImglist(InitInspirationCanvas, 1, step);
		}

		private void RequestNextPage()
		{
			int prePage = (int)Mathf.Ceil(ApplicationModel.ColoredImgList.Count / step);
			PublicImgaeApi.GetInspirationImglist(delegate(List<ColoredImage> x)
			{
				AddItem(x, prePage + 1);
			}, prePage + 1, step);
		}

		private void AddItem(List<ColoredImage> imglist, int page)
		{
			if (imglist != null && imglist.Count != 0)
			{
				int count = ApplicationModel.ColoredImgList.Count;
				ApplicationModel.ColoredImgList.AddRange(imglist);
				int count2 = imglist.Count;
				for (int i = 0; i < count2; i++)
				{
					_Data.Add(new DoubleImageModel(imglist[i], i + count));
				}
				_Data.Capacity = _Data.Count;
				_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
			}
		}

		private IEnumerator InitContent()
		{
			yield return null;
			List<ColoredImage> imglist = ApplicationModel.ColoredImgList;
			if (imglist != null && imglist.Count > 0)
			{
				NetErrorPanel.SetActive(value: false);
				int count = imglist.Count;
				_Data.Clear();
				for (int i = 0; i < count; i++)
				{
					_Data.Add(new DoubleImageModel(imglist[i], i));
				}
				_Data.Capacity = _Data.Count;
				_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
				StartCoroutine(unloadAssets());
			}
			else
			{
				NetErrorPanel.SetActive(value: true);
			}
		}

		private IEnumerator unloadAssets()
		{
			yield return null;
			yield return new WaitForSeconds(2f);
			Resources.UnloadUnusedAssets();
		}

		private void OnDestroy()
		{
			DragEventProxy.OnRefreshAfterLogin -= RefreshInspirationDataWhenEnter;
			if (_ScrollRectItemsAdapter != null)
			{
				_ScrollRectItemsAdapter.Dispose();
			}
		}

		private List<ColoredImage> GetHasEditedImgList()
		{
			return ApplicationModel.ColoredImgList;
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (!RefreshEventClock)
			{
				Vector2 anchoredPosition = content.GetComponent<RectTransform>().anchoredPosition;
				if (anchoredPosition.y < -200f)
				{
					RefreshEventClock = true;
					DownPanel.gameObject.SetActive(value: false);
					LoadingPanel.gameObject.SetActive(value: true);
				}
			}
			Vector2 anchoredPosition2 = content.GetComponent<RectTransform>().anchoredPosition;
			if (anchoredPosition2.y < -125f)
			{
				DownPanel.SetActive(value: true);
			}
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			if (RefreshEventClock)
			{
				RefreshInsImgList();
				RefreshEventClock = false;
				LoadingPanel.gameObject.SetActive(value: false);
				DownPanel.SetActive(value: false);
			}
			if (DownPanel.activeSelf)
			{
				DownPanel.SetActive(value: false);
			}
		}

		private void HideChild(Transform transform)
		{
			IEnumerator enumerator = transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					Transform transform2 = (Transform)enumerator.Current;
					transform2.gameObject.SetActive(value: false);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		private void OnDisable()
		{
			DiscardEvents();
		}
	}
