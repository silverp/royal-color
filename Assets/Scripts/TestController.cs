using HedgehogTeam.EasyTouch;
using SVGImporter;
using UnityEngine;

public class TestController : MonoBehaviour
{
	private string onClick = "false";

	private void Start()
	{
		UnityEngine.Debug.Log("Start!");
		EasyTouch.On_PinchEnd += OnPinchEnd;
		EasyTouch.On_Pinch += OnPinch;
	}

	private void Update()
	{
	}

	private void OnGUI()
	{
		GUI.color = Color.red;
		GUI.Label(new Rect(100f, 100f, 200f, 100f), onClick);
	}

	public void OnTouchStart(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnTouchStart:" + gesture.deltaPinch);
	}

	public void OnTapStart(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnTapStart:" + gesture.deltaPinch);
	}

	public void OnSwipe(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnSwipe:" + gesture.swipe + " " + gesture.swipeVector);
	}

	public void OnDrag(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnDrag:" + gesture.deltaPosition + " " + gesture.position);
	}

	public void OnPinchEnd(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnPinchEnd:" + gesture.deltaPosition + " " + gesture.position);
	}

	public void OnPinch(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnPinch:" + gesture.pickedObject + " " + gesture.pickedCamera);
		RectTransform component = base.transform.GetComponent<RectTransform>();
		base.transform.localScale = base.transform.localScale + Vector3.one * (gesture.deltaPinch / component.rect.width);
		base.transform.Translate(gesture.position * (gesture.deltaPinch / component.rect.width));
	}

	public void OnTwist(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnTwist:" + gesture.twistAngle + gesture.position);
	}

	public void onPay()
	{
		onClick = "true";
		string req = "{\"sign\":\"1B00BAAE8EE1DDAAA85CBAF8CABD56CF\",\"timestamp\":\"1512553785\",\"partnerid\":\"1493744042\",\"noncestr\":\"FFC11289CDBC9E943080332F14C6BC28\",\"prepayid\":\"wx2017120617494509f0244d1f0523512773\",\"package\":\"Sign=WXPay\",\"appid\":\"wxf054505a8e565f5f\",\"out_trade_no\":\"152e6248_20171206174944997\"}";
		//AndroidBrige.instance.payReq(AndroidBrige.Plaform.WECHAT, req);
		UnityEngine.Debug.Log("onPay!");
	}

	public void saveTexture()
	{
		int num = 2048;
		SVGImage component = GetComponent<SVGImage>();
		RenderTexture renderTexture = SVGRenderTexture.RenderSVG(textureSize: new Rect(0f, 0f, num, num), svgAsset: component.vectorGraphics);
		RenderTexture active = RenderTexture.active;
		Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
		RenderTexture.active = renderTexture;
		texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture2D.Apply();
		RenderTexture.active = active;
		PublicToolController.FileCreatorBytesForPng(texture2D.EncodeToPNG(), "outline_" + num);
	}
}
