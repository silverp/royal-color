using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LikesWorkScrollRectAdapter : MonoBehaviour
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class DoubleImageModel
	{
		public LikesWork img;

		public int index;

		public DoubleImageModel(LikesWork img, int index)
		{
			this.img = img;
			this.index = index;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<DoubleImageModel> _Data;

		public MyWorkItemsViewHolder myWorkItemsViewHolder;

		private RectTransform root;

		private Texture2D loadingTex = Resources.Load<Texture2D>("Images/inspiration/prefabImg");

		private static Texture2D defaultHeadTex = Resources.Load<Texture2D>("Images/share/touxiang_moren");

		public MyWorkScrollRectItemsAdapter(List<DoubleImageModel> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		public void DestoryAssets(string name)
		{
			if (root.GetComponent<RawImageDataController>().downloadStatus == 1)
			{
				UnityEngine.Object.DestroyImmediate(root.Find("Image").GetComponent<RawImage>().texture);
			}
		}

		public void SetImage(LikesWork work, int index)
		{
			root.Find("Image").GetComponent<RawImage>().texture = loadingTex;
			root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
			GameObject continueBtn = root.Find("BottomPanel").Find("continue").gameObject;
			string _thumbUrl = work.img.thumb;
			if (_thumbUrl != string.Empty)
			{
				NetLoadcontroller.Instance.RequestImg(_thumbUrl, delegate (Texture2D tex)
				{
					callback(tex, work, continueBtn, _thumbUrl);
				});
			}
			Transform transform = root.Find("TopPanel");
			transform.Find("name").GetComponent<Text>().text = work.user.nickname;
			transform.Find("info").GetComponent<Text>().text = PublicToolController.CountDiffTimeByTimeStamp(work.create.ToString());
			transform.Find("headimg").GetComponent<Button>().onClick.RemoveAllListeners();
			TextEmojiConvert.Instance.ConvertEmoji(transform.Find("name"), work.user.nickname, isBig: false);
			string headimgUrl = work.user.headimgUrl;
			root.Find("TopPanel").Find("headimg").Find("Image")
				.GetComponent<RawImage>()
				.texture = defaultHeadTex;
			if (headimgUrl != string.Empty)
			{
				NetLoadcontroller.Instance.RequestImg(headimgUrl, delegate (Texture2D tex)
				{
					RequestHeadImgCallback(tex, work, _thumbUrl);
				});
			}
			root.Find("BottomPanel").Find("share").GetComponent<Button>()
				.onClick.RemoveAllListeners();
			root.Find("BottomPanel").Find("share").GetComponent<Button>()
				.onClick.AddListener(delegate
				{
					clickShareBtn(work);
				});
			LikeBtnControl(work);
			FollowBtnController(work);
			root.Find("TopPanel").Find("name").GetComponent<Button>()
				.onClick.RemoveAllListeners();
			root.Find("TopPanel").Find("name").GetComponent<Button>()
				.onClick.AddListener(delegate
				{
					OnHeadImgHit(work);
				});
			if (index % 8 == 0)
			{
				PublicCallFrameTool.Instance.CollectGarbage();
			}
			if (index == likesWorkList.Count - 4)
			{
				DragEventProxy.Instance.SendRequestNextPageMsg();
			}
		}

		private void FollowBtnController(LikesWork work)
		{
			GameObject followBtn = root.Find("TopPanel").Find("follow").gameObject;
			FollowBtnControl(work.isFollow, followBtn);
			followBtn.GetComponent<Button>().onClick.RemoveAllListeners();
			followBtn.GetComponent<Button>().onClick.AddListener(delegate
			{
				clickFollowBtn(work, followBtn);
			});
		}

		private void clickFollowBtn(LikesWork work, GameObject followBtn)
		{
			UnityEngine.Debug.Log("clickFollowBtn....");
			if (ApplicationModel.userInfo == null)
			{
				DragEventProxy.Instance.SendOffLineMsg("InspirationCanvas");
				return;
			}
			work.isFollow = !work.isFollow;
			if (work.isFollow)
			{
				PublicControllerApi.AddFollow(work.user.uid, delegate (Errcode err)
				{
					requestFollowCallback(err, isFollow: true);
				});
			}
			else
			{
				PublicControllerApi.RemoveFollow(work.user.uid, delegate (Errcode err)
				{
					requestFollowCallback(err, isFollow: false);
				});
			}
			FollowBtnControl(work.isFollow, followBtn);
		}

		private void requestFollowCallback(Errcode err, bool isFollow)
		{
			if (err.errorCode == Errcode.OK)
			{
				int x = isFollow ? 1 : (-1);
				DragEventProxy.Instance.SendAddFollowCntMsg(x);
			}
		}

		private void FollowBtnControl(bool isFollow, GameObject followBtn)
		{
			if (isFollow)
			{
				followBtn.transform.GetChild(1).gameObject.SetActive(value: true);
				followBtn.transform.GetChild(0).gameObject.SetActive(value: false);
			}
			else
			{
				followBtn.transform.GetChild(1).gameObject.SetActive(value: false);
				followBtn.transform.GetChild(0).gameObject.SetActive(value: true);
			}
		}

		private void clickShareBtn(LikesWork work)
		{
			string url = ApplicationModel.HOST + "/user/getWorkListCount?uid=" + work.user.uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false;
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(url, delegate (string json, bool isSuccess)
			{
				RequestWorkCountCallback(json, isSuccess, work);
			});
		}

		private void RequestWorkCountCallback(string json, bool isSuccess, LikesWork work)
		{
			int count = 0;
			if (isSuccess)
			{
				WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
				if (workCountJson.status == 0)
				{
					count = workCountJson.data.count;
				}
			}
			NetLoadcontroller.Instance.RequestImg(work.img.thumb, delegate (Texture2D tex)
			{
				RequestFinalOnshare(tex, work.user.nickname, count);
			});
		}

		private void RequestFinalOnshare(Texture2D tex, string name, int count)
		{
			FileUploadEventProxy.Instance.SendShareWorkEventMsg(tex, name, count, isHasinfo: true, shareCallbck);
		}

		private void shareCallbck(Errcode err)
		{
			DragEventProxy.Instance.SendShareCallbackMsg(err);
		}

		private void LikeBtnControl(LikesWork img)
		{
			GameObject likeBtn = root.Find("BottomPanel").Find("like").gameObject;
			GameObject likeCntBtn = root.Find("BottomPanel").Find("like").Find("likeCnt")
				.gameObject;
			ChangeBtnStyle(img.isLike, likeBtn);
			likeCntBtn.GetComponent<Text>().text = img.img.likeCnt.ToString();
			likeBtn.GetComponent<Button>().onClick.RemoveAllListeners();
			likeBtn.GetComponent<Button>().onClick.AddListener(delegate
			{
				OnHitLikeBtn(img, likeBtn, likeCntBtn);
			});
		}

		private void OnHitLikeBtn(LikesWork work, GameObject btn, GameObject likeCntBtn)
		{
			if (ApplicationModel.userInfo == null)
			{
				DragEventProxy.Instance.SendOffLineMsg(ApplicationModel.lastVisibleScreen);
				return;
			}
			work.isLike = !work.isLike;
			if (work.isLike)
			{
				work.img.likeCnt++;
			}
			else
			{
				work.img.likeCnt--;
			}
			likeCntBtn.GetComponent<Text>().text = work.img.likeCnt.ToString();
			ChangeBtnStyle(work.isLike, btn);
			FileUploadEventProxy.Instance.SendLikeEventMsg(work.user.uid, work.img.workId, work.isLike);
		}

		private void ChangeBtnStyle(bool isLike, GameObject likeBtn)
		{
			if (isLike)
			{
				likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_en");
			}
			else
			{
				likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_un");
			}
		}

		private void RequestHeadImgCallback(Texture2D tex, LikesWork work, string requestUrl)
		{
			int itemIndex = myWorkItemsViewHolder.itemIndex;
			if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, requestUrl))
			{
				root.Find("TopPanel").Find("headimg").Find("Image")
					.GetComponent<RawImage>()
					.texture = tex;
				root.Find("TopPanel").Find("headimg").GetComponent<Button>()
					.onClick.AddListener(delegate
					{
						OnHeadImgHit(work);
					});
			}
		}

		private void callback(Texture2D tex, LikesWork work, GameObject continueBtn, string requestUrl)
		{
			int itemIndex = myWorkItemsViewHolder.itemIndex;
			if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, requestUrl))
			{
				root.Find("Image").GetComponent<RawImage>().texture = tex;
				LikesWork likesWork = work;
				continueBtn.GetComponent<Button>().onClick.RemoveAllListeners();
				continueBtn.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnContinueBtnHit(work);
				});
				root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
				root.Find("Image").GetComponent<Button>().onClick.AddListener(delegate
				{
					OnImageHit(tex);
				});
			}
		}

		private void OnContinueBtnHit(LikesWork work)
		{
			DragEventProxy.Instance.SendRepaintMsg(work.img);
		}

		private void SetFrommImgData()
		{
			root.GetComponent<RawImageDataController>().downloadStatus = 1;
		}

		public void callback(Texture2D tex, string name)
		{
			root.Find("Image").GetComponent<RawImage>().texture = tex;
		}

		private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageURLAtRequest)
		{
			return _Data.Count > itemIndex && itemIdexAtRequest == itemIndex && imageURLAtRequest == _Data[itemIndex].img.img.thumb;
		}

		private void OnImageHit(Texture2D tex)
		{
			DragEventProxy.Instance.SendGotoZoomImageMsg(tex);
		}

		private void OnHeadImgHit(LikesWork work)
		{
			MyworkCanvasEvent.instance.GotoOtherCanvas(work);
		}

		protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
			myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return myWorkItemsViewHolder;
		}

		protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			myWorkItemsViewHolder = newOrRecycled;
			DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
			SetImage(doubleImageModel.img, doubleImageModel.index);
		}
	}

	[SerializeField]
	private GameObject topCanvas;

	[SerializeField]
	private MyWorkParams _ScrollRectAdapterParams;

	private string CanvasName;

	private List<DoubleImageModel> _Data = new List<DoubleImageModel>();

	private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

	private int step = 20;

	private bool isMine;

	public static List<LikesWork> likesWorkList;

	public BaseParams Params => _ScrollRectAdapterParams;

	public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<DoubleImageModel> Data => _Data;

	private void AssginEvents()
	{
		DragEventProxy.OnRequestNextPage += RequestNextPage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnRequestNextPage -= RequestNextPage;
	}

	private void Awake()
	{
		CanvasName = topCanvas.name;
		ApplicationModel.lastVisibleScreen = CanvasName;
		_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("likes work enable!!!");
		AssginEvents();
		if (CanvasName == "MyworkCanvas")
		{
			isMine = true;
		}
		else
		{
			isMine = false;
		}
		PublicImgaeApi.GetLikesData(RequestCallback, 1, step, isMine);
	}

	private void RequestCallback(List<LikesWork> likesList, Errcode err)
	{
		if (err.errorCode == 0)
		{
			UnityEngine.Debug.Log("RequestCallback errcode o");
			likesWorkList = likesList;
			InitContent();
		}
		if (err.errorCode == 2)
		{
			DragEventProxy.Instance.SendOffLineMsg(CanvasName, isOffline: true);
		}
	}

	private void InitContent()
	{
		_Data.Clear();
		int count = likesWorkList.Count;
		int i = 0;
		UnityEngine.Debug.Log("InitContent ImgTotal is:" + count);
		for (; i < count; i++)
		{
			_Data.Add(new DoubleImageModel(likesWorkList[i], i));
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		if (CanvasName != "MyworkCanvas")
		{
			_ScrollRectItemsAdapter.ScrollToStart();
		}
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void OnDestroy()
	{
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void RequestNextPage()
	{
		int prePage = (int)Mathf.Ceil(likesWorkList.Count / step);
		PublicImgaeApi.GetLikesData(delegate (List<LikesWork> x, Errcode err)
		{
			AddItem(x, err, prePage + 1);
		}, prePage + 1, step, isMine);
	}

	private void AddItem(List<LikesWork> imglist, Errcode err, int page)
	{
		if (err.errorCode == 0 && imglist != null && imglist.Count != 0 && likesWorkList.Count == (page - 1) * step)
		{
			int count = likesWorkList.Count;
			likesWorkList.AddRange(imglist);
			int count2 = imglist.Count;
			for (int i = 0; i < count2; i++)
			{
				_Data.Add(new DoubleImageModel(imglist[i], i + count));
			}
			_Data.Capacity = _Data.Count;
			_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		}
		if (err.errorCode == 2)
		{
			DragEventProxy.Instance.SendOffLineMsg(CanvasName, isOffline: true);
		}
	}

	private bool isExistsImageid(string imageId, List<string> list)
	{
		foreach (string item in list)
		{
			if (item == imageId)
			{
				return true;
			}
		}
		return false;
	}

	private void Update()
	{
	}
}
