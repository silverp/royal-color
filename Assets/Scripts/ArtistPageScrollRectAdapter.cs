using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ArtistPageScrollRectAdapter : MonoBehaviour
{
	[Serializable]
	public class CatePageParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class AlbumModel
	{
		public string albumName;

		public string author;

		public string headImgUrl;

		public string coverUrl;

		public string albumId;

		public AlbumModel(string albumName, string author, string headImgUrl, string coverUrl, string albumId)
		{
			this.albumName = albumName;
			this.author = author;
			this.headImgUrl = headImgUrl;
			this.coverUrl = coverUrl;
			this.albumId = albumId;
		}
	}

	public class CatePageItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class CatePageScrollRectItemsAdapter : ScrollRectItemsAdapter8<CatePageParams, CatePageItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<AlbumModel> _Data;

		private readonly string index;

		private CatePageItemsViewHolder catePageItemsViewHolder;

		public CatePageScrollRectItemsAdapter(List<AlbumModel> data, CatePageParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

        public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

        protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageURLAtRequest)
		{
			return _Data.Count > itemIndex && itemIdexAtRequest == itemIndex && imageURLAtRequest == _Data[itemIndex].coverUrl;
		}

        protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

        protected override CatePageItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			CatePageItemsViewHolder catePageItemsViewHolder = new CatePageItemsViewHolder();
			catePageItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return catePageItemsViewHolder;
		}

        protected override void UpdateViewsHolder(CatePageItemsViewHolder newOrRecycled)
		{
			RectTransform root = newOrRecycled.Root;
			AlbumModel albumModel = _Data[newOrRecycled.itemIndex];
			catePageItemsViewHolder = newOrRecycled;
			root.transform.Find("albumName").GetChild(0).GetComponent<Text>()
				.text = albumModel.albumName;
				root.transform.Find("name").GetComponent<Text>().text = albumModel.author;
				GameObject coverImage = root.transform.Find("cover").Find("Image").gameObject;
				string _coverUrl = albumModel.coverUrl;
				NetLoadcontroller.Instance.RequestImg(_coverUrl, delegate(Texture2D tex)
				{
					RequestCovercallback(tex, coverImage, _coverUrl);
				});
				GameObject headImage = root.transform.Find("headImg").gameObject;
				NetLoadcontroller.Instance.RequestImg(albumModel.headImgUrl, delegate(Texture2D tex)
				{
					RequestHeadImgcallback(tex, headImage, _coverUrl);
				});
				root.GetComponent<Button>().onClick.RemoveAllListeners();
				root.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnAlbumHit(albumModel.albumId);
				});
			}

			private void OnAlbumHit(string albumId)
			{
				UnityEngine.Debug.Log("OnAlbumHit id is:" + albumId);
				AritistCanvasEvent.Instance.EnterCateDetailPage(albumId);
			}

			private void RequestHeadImgcallback(Texture2D tex, GameObject headImage, string requestUrl)
			{
				int itemIndex = catePageItemsViewHolder.itemIndex;
				if (IsModelStillValid(catePageItemsViewHolder.itemIndex, itemIndex, requestUrl))
				{
					headImage.GetComponent<RawImage>().texture = tex;
				}
			}

			private void RequestCovercallback(Texture2D tex, GameObject coverImage, string requestUrl)
			{
				int itemIndex = catePageItemsViewHolder.itemIndex;
				if (IsModelStillValid(catePageItemsViewHolder.itemIndex, itemIndex, requestUrl))
				{
					coverImage.GetComponent<RawImage>().texture = tex;
				}
			}

			private void countDropdownEvent(int num)
			{
				if (num % 6 == 0)
				{
					TotalGA.Event("cate_drop", "分类:" + ApplicationModel.GetCurrentCateName() + "下拉" + (num / 6).ToString() + "页");
				}
			}

			public void judgeIsLoadNext(int num)
			{
				if (num % 20 == 8)
				{
					DragEventProxy.Instance.SendRequestNextPageMsg();
				}
			}
		}

		[SerializeField]
		private CatePageParams _ScrollRectAdapterParams;

		[SerializeField]
		private GameObject CateDetailCanvas;

		[SerializeField]
		private GameObject netErrorpanel;

		private List<AlbumModel> _Data;

		private CatePageScrollRectItemsAdapter _ScrollRectItemsAdapter;

		private int step = 20;

		private List<ArtistAlbumModel> artistAlbumModelList;

		public BaseParams Params => _ScrollRectAdapterParams;

		public CatePageScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

		public List<AlbumModel> Data => _Data;

		private void Awake()
		{
			_Data = new List<AlbumModel>();
			_ScrollRectItemsAdapter = new CatePageScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
		}

		private void AssginEvents()
		{
			DragEventProxy.OnRequestNextPage += RequestNextPage;
			DragEventProxy.OnRefreshAlbumList += RequestAlbumInfo;
		}

		private void DiscardEvents()
		{
			DragEventProxy.OnRequestNextPage -= RequestNextPage;
			DragEventProxy.OnRefreshAlbumList -= RequestAlbumInfo;
		}

		private void OnEnable()
		{
			AssginEvents();
			netErrorpanel.SetActive(value: false);
			if (artistAlbumModelList == null || artistAlbumModelList.Count == 0 || base.transform.GetChild(0).GetChild(0).childCount == 0)
			{
				RequestAlbumInfo();
			}
		}

		private void OnDisable()
		{
			DiscardEvents();
		}

		private void RequestAlbumInfo()
		{
			UnityEngine.Debug.Log("RequestAlbumInfo。。。。");
			PublicControllerApi.GetAlbumList(1, step, RequestAlbumInfoCallback);
		}

		private void RequestAlbumInfoCallback(Errcode err, List<ArtistAlbumModel> list)
		{
			if (err.errorCode == Errcode.OK)
			{
				if (list.Count == 0)
				{
					netErrorpanel.SetActive(value: true);
					return;
				}
				netErrorpanel.SetActive(value: false);
				ApplicationModel.artistAlbumModelList = list;
				artistAlbumModelList = list;
				UnityEngine.Debug.Log("list count is:" + list.Count);
				StartCoroutine(initContent(list));
				ApplicationModel.RestoreAlbumList();
			}
			else
			{
				netErrorpanel.SetActive(value: true);
			}
		}

		private void RequestNextPage()
		{
		}

		private void AddItem(Errcode err, List<ArtistAlbumModel> imglist, int page)
		{
			if (imglist != null && imglist.Count != 0 && artistAlbumModelList.Count == (page - 1) * step)
			{
				int count = artistAlbumModelList.Count;
				artistAlbumModelList.AddRange(imglist);
				int count2 = imglist.Count;
				for (int i = 0; i < count2; i++)
				{
					_Data.Add(new AlbumModel(imglist[i].name, imglist[i].artistName, imglist[i].headImgUrl, imglist[i].url, imglist[i].albumId));
				}
				_Data.Capacity = _Data.Count;
				_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
			}
		}

		private IEnumerator initContent(List<ArtistAlbumModel> list)
		{
			yield return null;
			yield return null;
			yield return null;
			_Data.Clear();
			UnityEngine.Debug.Log("initContent....");
			int ImgTotal = (list != null) ? list.Count : 0;
			if (ImgTotal == 0)
			{
				UnityEngine.Debug.Log("initContent imgtotal is:" + ImgTotal);
				netErrorpanel.SetActive(value: true);
				yield break;
			}
			netErrorpanel.SetActive(value: false);
			for (int i = 0; i < ImgTotal; i++)
			{
				_Data.Add(new AlbumModel(list[i].name, list[i].artistName, list[i].headImgUrl, list[i].url, list[i].albumId));
			}
			_Data.Capacity = _Data.Count;
			_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		}

		private IEnumerator unloadAssets()
		{
			yield return null;
			yield return null;
			Resources.UnloadUnusedAssets();
		}

		private void OnDestroy()
		{
			if (_ScrollRectItemsAdapter != null)
			{
				_ScrollRectItemsAdapter.Dispose();
			}
		}

		private void DestoryChild(Transform transform)
		{
			IEnumerator enumerator = transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					Transform transform2 = (Transform)enumerator.Current;
					UnityEngine.Object.Destroy(transform2.gameObject);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}
	}
