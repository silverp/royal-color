using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ZoomPanelWorkController : MonoBehaviour
{
	public struct Point
	{
		public short x;

		public short y;

		public Point(short aX, short aY)
		{
			x = aX;
			y = aY;
		}

		public Point(int aX, int aY)
		{
			this = new Point((short)aX, (short)aY);
		}
	}

	public static NewWorkerController Instance;

	private Texture2D image;

	private Rect imageRect;

	private static float RECT_DEPEND_W = 1080f;

	private static float RECT_DEPEND_H = 1170f;

	private Vector2 startPos;

	private float PRESS_MARGIN = 4f;

	private float scale_factor = 0.14f;

	private float MAXSCALE = 30f;

	private float MIN_SCALE = 0.5f;

	private bool colorHolds;

	private Vector2 prevDist = new Vector2(0f, 0f);

	private Vector2 curDist = new Vector2(0f, 0f);

	private Vector2 midPoint = new Vector2(0f, 0f);

	private Vector2 ScreenSize;

	private Vector3 originalPos;

	private GameObject parentObject;

	private RectTransform parentRectTransform;

	private RectTransform grandParentRectTransform;

	private RectTransform rectTransform;

	private bool isMousePressed;

	private RawImage rawImg;

	private Vector2 parentOriginPos;

	private Vector3 originPos;

	private float SCREEN_SCALE;

	private float START_X;

	private int imageW;

	private int imageH;

	private int rect_deviation = 82;

	private Vector2 _lastMousePos = Vector2.zero;

	private float MOVE_FACTOR;

	public Color tarGetCol;

	public bool EventLock;

	private bool pencilClocked;

	private static Vector3 prevPos = Vector3.zero;

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssignEvents()
	{
		DragEventProxy.OnEnterZoomPanel += EnterZoomPanel;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnEnterZoomPanel -= EnterZoomPanel;
	}

	private void Start()
	{
		MOVE_FACTOR = 1.3f;
	}

	private void Awake()
	{
		AssignEvents();
		Transform transform = base.transform;
		Vector3 localPosition = base.transform.localPosition;
		transform.localPosition = new Vector2(localPosition.x, rect_deviation);
	}

	private void Update()
    {
        var touches = InputHelper.GetTouches();
        checkTouch(touches);
		checkForMultiTouch(touches);

#if UNITY_EDITOR
        checkInputPc();
#endif

    }

    private void EnterZoomPanel()
	{
		NetLoadcontroller.Instance.RequestImg(ApplicationModel.currentColoredImg.img.finalImg, delegate(Texture2D tex)
		{
			InitWorkShop(tex);
		});
	}

	private void InitWorkShop(Texture2D tex)
	{
		_lastMousePos = Vector2.zero;
		image = tex;
		rawImg = GetComponent<RawImage>();
		rawImg.texture = image;
		SCREEN_SCALE = CanvasScreenAutoFix.instance.scaleRatio;
		START_X = ((float)Screen.width - RECT_DEPEND_W * SCREEN_SCALE) / 2f;
		imageRect = new Rect(START_X, 230f * SCREEN_SCALE, RECT_DEPEND_W * SCREEN_SCALE, RECT_DEPEND_H * SCREEN_SCALE);
		parentObject = base.transform.parent.gameObject;
		parentRectTransform = parentObject.GetComponent<RectTransform>();
		grandParentRectTransform = parentRectTransform.parent.GetComponent<RectTransform>();
		rectTransform = GetComponent<RectTransform>();
		parentOriginPos = parentRectTransform.anchoredPosition;
		ScreenSize = new Vector2(Screen.width, Screen.height);
		originalPos = base.transform.position;
		isMousePressed = false;
		imageW = image.width;
		imageH = image.height;
		parentRectTransform.localScale = new Vector3(1f, 1f, 1f);
		parentRectTransform.anchoredPosition = new Vector2(0f, 0f);
	}

	private void OnDisable()
	{
		if (image != null)
		{
			UnityEngine.Object.DestroyImmediate(image);
		}
		if (rawImg != null)
		{
			UnityEngine.Object.DestroyImmediate(rawImg);
		}
	}

	private void OnGUI()
	{
	}

	private void myMouseDown(PointerEventData dat)
	{
		RectTransform component = GetComponent<RectTransform>();
		Vector2 position = dat.position;
		if (RectTransformUtility.ScreenPointToLocalPointInRectangle(component, position, null, out Vector2 localPoint))
		{
			int num = (int)localPoint.x;
			int num2 = (int)localPoint.y;
			if (num < 0)
			{
				num += (int)component.rect.width / 2;
			}
			else
			{
				num += (int)component.rect.width / 2;
			}
			if (num2 > 0)
			{
				num2 += (int)component.rect.height / 2;
			}
			else
			{
				num2 += (int)component.rect.height / 2;
			}
		}
	}

	public void checkInputPc()
	{
		if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0f)
		{
			float axis = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
			Vector3 mousePosition = UnityEngine.Input.mousePosition;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(grandParentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 _);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint2);
			_lastMousePos = localPoint2;
			Vector3 localScale = parentObject.transform.localScale;
			float x = localScale.x + scale_factor * axis / Mathf.Abs(axis);
			Vector3 localScale2 = parentObject.transform.localScale;
			Vector2 scaleTo = new Vector3(x, localScale2.y + scale_factor * axis / Mathf.Abs(axis));
			if ((double)scaleTo.x > 6.0 || (double)scaleTo.y > 6.0)
			{
				scaleTo = new Vector2(5f, 5f);
			}
			newScale(localPoint2, scaleTo);
		}
		//if (Input.GetMouseButtonDown(0))
		//{
		//	Vector2 screenPoint = UnityEngine.Input.mousePosition;
		//	if (!RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, screenPoint, Camera.main))
		//	{
		//		return;
		//	}
		//	RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, screenPoint, Camera.main, out Vector2 localPoint3);
		//	_lastMousePos = localPoint3;
		//}
		//if (Input.GetMouseButtonUp(0) && RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, UnityEngine.Input.mousePosition, Camera.main))
		//{
		//	RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint4);
		//	if (Vector2.Distance(_lastMousePos, localPoint4) > PRESS_MARGIN)
		//	{
		//		parentRectTransform.anchoredPosition += localPoint4 - _lastMousePos;
		//	}
		//	float num = (float)image.width / RECT_DEPEND_W;
		//	float num2 = (float)image.height / RECT_DEPEND_H;
		//}
	}
	private bool _isMoving = false;
	private void checkTouch(Touch[] touches)
	{
		if (touches.Length != 1 || !RectTransformUtility.RectangleContainsScreenPoint(grandParentRectTransform, touches[0].position, Camera.main))
		{
            _isMoving = false;
            return;
		}
		RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, touches[0].position, Camera.main, out Vector2 _);
		//float num = (float)image.width / RECT_DEPEND_W;
		//float num2 = (float)image.height / RECT_DEPEND_H;
		Touch touch = touches[0];
		Vector2 position = touch.position;
		switch (touch.phase)
		{
			case TouchPhase.Began:
				startPos = position;
                _isMoving = true;
                break;
			case TouchPhase.Moved:
				if (_isMoving && Vector2.Distance(touch.position, startPos) > 0)
				{
					Transform transform = parentObject.transform;
					Vector3 localPosition = transform.localPosition;
					Vector3 right = Vector3.right;
					Vector2 deltaPosition = touch.deltaPosition;
					Vector3 a = right * deltaPosition.x;
					Vector3 up = Vector3.up;
					Vector2 deltaPosition2 = touch.deltaPosition;
					transform.localPosition = localPosition + (a + up * deltaPosition2.y);
					startPos = position;
				}
				break;
			case TouchPhase.Ended:
				{
					if (Vector2.Distance(touch.position, startPos) == 0 || !_isMoving)
					{
						break;
					}
					Vector2 apos = parentRectTransform.anchoredPosition;
					float newX = apos.x; 
					float newY = apos.y; 
					if (apos.x > 0f)
					{
						newX = 0f;
					} 
					if (apos.y > 0f)
					{
						newY = 0f;
					}  
					float width = parentRectTransform.rect.width;
					Vector3 localScale = parentRectTransform.localScale;
					if (apos.x < 0f - width * localScale.x + parentRectTransform.rect.width)
					{ 
						newX = 0f - width * localScale.x + parentRectTransform.rect.width;
					}  
					float height = parentRectTransform.rect.height; 
					if (apos.y < 0f - height * localScale.y + parentRectTransform.rect.height)
					{ 
						newY = 0f - height * localScale.y + parentRectTransform.rect.height;
					} 
					if (newX == apos.x)
					{ 
						if (newY == apos.y)
						{
							break;
						}
					}
					StartCoroutine(MoveToPosition(parentRectTransform, new Vector2(newX, newY), 0.3f));
					break;
				}
		}
	}

	private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
	{
		Vector2 currentPos = rectTransform.anchoredPosition;
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / TimeToMove;
			rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
			yield return null;
		}
	}

	private void checkForMultiTouch(Touch[] touches)
	{
		if (touches.Length != 2)
		{
			return;
		}
		if (touches[0].phase == TouchPhase.Moved && touches[1].phase == TouchPhase.Moved)
		{
			Vector2 position = touches[0].position;
			float x = position.x;
			Vector2 position2 = touches[1].position;
			float x2 = (x + position2.x) / 2f;
			Vector2 position3 = touches[0].position;
			float y = position3.y;
			Vector2 position4 = touches[1].position;
			midPoint = new Vector2(x2, (y + position4.y) / 2f);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(grandParentRectTransform, midPoint, Camera.main, out Vector2 _);
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint2);
			curDist = touches[0].position - touches[1].position;
			prevDist = touches[0].position - touches[0].deltaPosition - (touches[1].position - touches[1].deltaPosition);
			float num = curDist.magnitude - prevDist.magnitude;
			Vector2 scaleTo = default(Vector2);
			if (num > 0f)
			{
				Vector3 localScale = parentRectTransform.localScale;
				float x3 = localScale.x + scale_factor;
				Vector3 localScale2 = parentRectTransform.localScale;
				scaleTo = new Vector2(x3, localScale2.y + scale_factor);
			}
			else
			{
				Vector3 localScale3 = parentRectTransform.localScale;
				float x4 = localScale3.x - scale_factor;
				Vector3 localScale4 = parentRectTransform.localScale;
				scaleTo = new Vector2(x4, localScale4.y - scale_factor);
			}
			if (scaleTo.x > MAXSCALE || scaleTo.y > MAXSCALE)
			{
				scaleTo = new Vector2(MAXSCALE, MAXSCALE);
			}
			newScale(localPoint2, scaleTo);
		}
		if (touches[0].phase == TouchPhase.Ended || touches[1].phase == TouchPhase.Ended)
		{
			Vector3 localScale5 = parentRectTransform.localScale;
			float x5 = localScale5.x;
			Vector3 localScale6 = parentRectTransform.localScale;
			Vector2 scaleTo2 = new Vector2(x5, localScale6.y);
			if (scaleTo2.x >= MAXSCALE || scaleTo2.y >= MAXSCALE)
			{
				scaleTo2 = new Vector2(MAXSCALE - 1f, MAXSCALE - 1f);
			}
			RectTransformUtility.ScreenPointToLocalPointInRectangle(parentRectTransform, UnityEngine.Input.mousePosition, Camera.main, out Vector2 localPoint3);
			newScale(localPoint3, scaleTo2);
		}
	}

	private void newScale(Vector2 fromParent, Vector2 scaleTo)
	{
		Vector2 anchoredPosition = parentRectTransform.anchoredPosition;
		Vector2 vector = parentRectTransform.localScale;
		Vector2 b = Vector2.Scale(fromParent, scaleTo - vector);
		parentRectTransform.anchoredPosition = anchoredPosition - b;
		parentRectTransform.localScale = Vector3.one * scaleTo.x;
		Vector3 localScale = parentRectTransform.localScale;
		if (localScale.x <= 1f)
		{
			parentRectTransform.localScale = new Vector3(1f, 1f, 1f);
			parentRectTransform.anchoredPosition = new Vector2(0f, 0f);
		}
		Vector3 localScale2 = parentRectTransform.localScale;
		if (localScale2.x > MAXSCALE)
		{
			parentRectTransform.localScale = vector;
			parentRectTransform.anchoredPosition = anchoredPosition;
		}
	}
}
