using System.Collections;
using UnityEngine;

public class CaptureTexArea : MonoBehaviour
{
	private string textData = "lallal";

	private Rect TextAreaRect = new Rect(0f, 0f, 400f, 400f);

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void CaptureText()
	{
		StartCoroutine(CaptureTextArea());
	}

	private IEnumerator CaptureTextArea()
	{
		yield return new WaitForEndOfFrame();
		int width = Mathf.FloorToInt(TextAreaRect.width);
		int height = Mathf.FloorToInt(TextAreaRect.height);
		Texture2D text_texture = new Texture2D(width, height, TextureFormat.RGB24, mipChain: false);
		Rect tempRect = TextAreaRect;
		tempRect.y = (float)Screen.height - TextAreaRect.y - TextAreaRect.height;
		text_texture.ReadPixels(tempRect, 0, 0);
		text_texture.Apply();
		PublicToolController.FileCreatorBytes(text_texture.EncodeToPNG(), "textImg", isStoreChange: true);
	}
}
