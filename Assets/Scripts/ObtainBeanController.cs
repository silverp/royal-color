using System;
using UnityEngine;
using UnityEngine.UI;

public class ObtainBeanController : MonoBehaviour
{
	[SerializeField]
	private GameObject Purchase;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject BeanBox;

	[SerializeField]
	private GameObject OpenVipPanel;

	private bool publishFuncPanelIsExpand;

	private bool shareFuncPanelIsExpand;

	private bool signFuncPanelIsExpand;

	private Texture2D downArrow;

	private Texture2D upArrow;

	private void Start()
	{
		downArrow = Resources.Load<Texture2D>("Images/obtainBean/jiantou_down");
		upArrow = Resources.Load<Texture2D>("Images/obtainBean/jiantou_up");
	}

	public void back()
	{
		base.transform.parent.gameObject.SetActive(value: false);
		UIRoot.ShowUI(MyworkCanvas);
	}

	private void OnDisable()
	{
		GameQuit.Back -= back;
	}

	private void OnEnable()
	{
		GameQuit.Back += back;
		showExpireTime();
	}

	private void showExpireTime()
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			if (ApplicationModel.userInfo.VipLevel != 0)
			{
				PublicControllerApi.GetExpiredTime(RequestVipExpiredCallback);
				addClickEvent();
			}
			else
			{
				OpenVipPanel.transform.Find("expire").GetComponent<Text>().text = "未开通";
				addClickEvent();
			}
		}
	}

	private void addClickEvent()
	{
		OpenVipPanel.GetComponent<Button>().onClick.RemoveAllListeners();
		OpenVipPanel.GetComponent<Button>().onClick.AddListener(delegate
		{
			ShowPurchasePanel();
		});
	}

	private void RequestVipExpiredCallback(VipExpired vipExpired)
	{
		if (vipExpired != null)
		{
			OpenVipPanel.transform.Find("expire").GetComponent<Text>().text = PublicToolController.ConvertStringToDateTime(vipExpired.vipExpired.ToString()).ToString("yyyy-MM-dd") + "到期";
		}
	}

	public void ShowPurchasePanel()
	{
		Purchase.SetActive(value: true);
		RectTransform component = Purchase.transform.GetComponent<RectTransform>();
		Vector2 anchoredPosition = Purchase.transform.GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(anchoredPosition.x, -1920f);
		LeanTween.moveY(Purchase.GetComponent<RectTransform>(), 0f, 0.4f).setEase(LeanTweenType.easeOutQuad).setOnComplete((Action)delegate
		{
			Purchase.transform.Find("GameObject").GetComponent<OpenVipController>().returnPage = "ObtainBeanCanvas";
		});
	}

	public void InviteFriend()
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			URLRequest uRLRequest = new URLRequest(ApplicationModel.userInfo.HeadimgUrl);
			uRLRequest.cacheType = HttpRequest.CacheType.FILE;
			DataBroker.getTexture2D(uRLRequest, delegate(Texture2D tex)
			{
				FileUploadEventProxy.Instance.SendInviteFriendMsg(tex, ApplicationModel.userInfo.Nickname, ShareCallback);
			}, delegate
			{
			});
		}
	}

	public void Recommend()
	{
	}

	public void ShareCallback(Errcode err)
	{
		if (err.errorCode == Errcode.OK)
		{
			BeanAction.request(BeanAction.SHARE, beanActionCallback);
		}
	}

	public void ShareSuccess()
	{
		BeanAction.request(BeanAction.SHARE, beanActionCallback);
	}

	private void beanActionCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			BeanBox.SetActive(value: true);
			ApplicationModel.userInfo.BeanCnt += amount;
			BeanBox.transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = "邀请分享成功";
				BeanBox.transform.GetChild(1).GetChild(4).GetComponent<Text>()
					.text = "+" + amount;
				}
			}
		}
