using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JumpToNextScene : MonoBehaviour
{
	private void Start()
	{
		StartCoroutine(waitToLoadNextScene());
	}

	private IEnumerator waitToLoadNextScene()
	{
		yield return new WaitForSeconds(0.05f);
		SceneManager.LoadScene("GamePlay");
	}
}
