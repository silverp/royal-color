using UnityEngine;

public class WorkPanelController : MonoBehaviour
{
	[SerializeField]
	private GameObject topCanvas;

	[SerializeField]
	private GameObject WorkScrollView;

	[SerializeField]
	private GameObject EmptyPanel;

	private string CanvaName;

	private void Awake()
	{
		CanvaName = topCanvas.name;
	}

	private void OnEnable()
	{
		EmptyPanel.SetActive(value: false);
		WorkScrollView.SetActive(value: true);
	}
}
