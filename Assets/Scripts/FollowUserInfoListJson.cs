using System.Collections.Generic;

public class FollowUserInfoListJson
{
	public int status;

	public List<FollowUserInfo> data;

	public string msg;
}
