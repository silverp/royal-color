using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PanelColorChooseController : MonoBehaviour
{
	// [SerializeField] private string GradientColorName = "GradientColor";
	//
	// [SerializeField] private string SolidColorName = "SolidColor";
	//
	// [SerializeField] private string ThemeColorName = "ThemeColor";

	// [SerializeField] private GameObject ColorPanel;

	[SerializeField] private GameObject SelectButton;

	[SerializeField] private GameObject ColorModelPanelBar;

	[SerializeField] private ColorRecentPageInitController RecentColor;

	[SerializeField] private CreateColorBarController _colorBarController;

	[SerializeField] private Animator modelButtonAnim;
	[SerializeField] private Animator historyButtonAnim;

	// [SerializeField] private GameObject otherAreaButton;

	private Vector3 LocalPosPurePanel;

	private Vector3 localPosSelectPanel;

	// private string SelectedColorThemeName = "pure";

	private Texture2D tex_active;

	// private string newColorThemeName = string.Empty;

	[SerializeField] private GameObject WorkShopCanvas;

	// private GameObject mask;

	private Texture2D solid_icon;

	private Texture2D gradient_icon;

	private Texture2D theme_icon;

	private Texture2D selectd_btn;

	private float MaskHeightForcolorPanel = 1440f;

	private float MaskHeightForRecentPanel = 1440f;

	private void Start()
	{
		solid_icon = Resources.Load<Texture2D>("Images/work/solid");
		gradient_icon = Resources.Load<Texture2D>("Images/work/gradient");
		theme_icon = Resources.Load<Texture2D>("Images/work/theme");
		// WorkShopCanvas = PublicToolController.FindParentWithTag(base.gameObject, "WorkShopCanvas");
		// mask = WorkShopCanvas.transform.Find("Mask").gameObject;
	}

	public void GotoRecentPanel()
	{
		historyButtonAnim?.Play("Pressed", -1, 0f);
		_GotoRecentPanel();
		DragEventProxy.Instance.SendAddColorToRecentList();
	}
	public void _GotoRecentPanel()
	{  
		if (!RecentColor.gameObject.activeSelf)
		{ 
			if (ColorModelPanelBar.activeSelf)
				_GoToChoose();
			UnityEngine.Debug.Log("GotoRecentPanel");
			var rect = RecentColor.GetComponent<RectTransform>();
			// SelectButton.SetActive(value: false);
			var posD = rect.anchoredPosition;
			rect.anchoredPosition = new Vector2(posD.x, 452);
			RecentColor.ScaleDown(0);
			RecentColor.gameObject.SetActive(value: true);
			var spin = RecentColor.transform.Find("spin").GetComponent<Image>();
			LeanTween.value(spin.gameObject, v =>
			{
				var pos = rect.anchoredPosition;
				rect.anchoredPosition = new Vector2(pos.x, v);
			}, 452, 601, 0.5f);
			LeanTween.value(spin.gameObject, v =>
			{
				var c = Color.white;
				c.a = v;
				spin.color = c;
			}, 0f, 0.95f, 0.5f);
			
			RecentColor.InitContent();
			LeanTween.delayedCall(this.gameObject, 0.3f, () =>
			{
				RecentColor.ScaleUp(0.1f);
			});
		}
		else
		{
			TotalGA.Event("ExitRecentPanel");
			RecentColor.ScaleDown(0.15f);
			LeanTween.delayedCall(this.gameObject, 0.1f, () =>
			{
				var spin = RecentColor.transform.Find("spin").GetComponent<Image>();
				var rect = RecentColor.GetComponent<RectTransform>();
				LeanTween.value(spin.gameObject, v =>
				{
					var pos = rect.anchoredPosition;
					rect.anchoredPosition = new Vector2(pos.x, v);
				}, 601, 452, 0.5f);
				LeanTween.value(spin.gameObject, v =>
				{
					var c = Color.white;
					c.a = v;
					spin.color = c;
				}, 0.95f, 0f, 0.5f).setOnComplete(() => { RecentColor.gameObject.SetActive(value: false); });
			});
			
		} 
	} 

	public void GotoSolidColor()
	{
		TotalGA.Event("click_colorModel_name", "Solid");
		_colorBarController.InitColorButtonBar(CreateColorBarController.ColorTheme.Solid);
		// RecentColor.SetActive(value: false);
		SelectButton.GetComponent<RawImage>().texture = solid_icon;
		SetBtnSelected("SolidButton");
		SetBtnNotSelected("GradientButton");
		SetBtnNotSelected("ThemeButton");
		// // newColorThemeName = "solid";
		// GotoColorPanel();
	}

	public void GotoGradientColor()
	{
		TotalGA.Event("click_colorModel_name", "Gradient");
		_colorBarController.InitColorButtonBar(CreateColorBarController.ColorTheme.Gradient);
		// RecentColor.SetActive(value: false);
		SelectButton.GetComponent<RawImage>().texture = gradient_icon;
		SetBtnSelected("GradientButton");
		SetBtnNotSelected("SolidButton");
		SetBtnNotSelected("ThemeButton");
		// // newColorThemeName = "gradient";
		// GotoColorPanel();
	}

	public void GotoThemeColor()
	{
		TotalGA.Event("click_colorModel_name", "Thematic");
		_colorBarController.InitColorButtonBar(CreateColorBarController.ColorTheme.Thematic);
		// RecentColor.SetActive(value: false);
		SelectButton.GetComponent<RawImage>().texture = theme_icon;
		SetBtnSelected("ThemeButton");
		SetBtnNotSelected("SolidButton");
		SetBtnNotSelected("GradientButton");
		// newColorThemeName = "theme";
		// GotoColorPanel();
	}

	public void GotoColorPanel()
	{
		TotalGA.Event("click_colorModel_btn");
		var spin = ColorModelPanelBar.transform.Find("spin").GetComponent<Image>();
		var rect = ColorModelPanelBar.GetComponent<RectTransform>();
		LeanTween.value(spin.gameObject, v =>
		{
			var pos = rect.anchoredPosition;
			rect.anchoredPosition = new Vector2(pos.x, v);
		}, 601, 452, 0.5f);
		LeanTween.value(spin.gameObject, v =>
		{
			var c = Color.white;
			c.a = v;
			spin.color = c;
		}, 0.9f, 0f, 0.5f).setOnComplete(() => { ColorModelPanelBar.SetActive(value: false); });
 
	}

	public void GoToChoose()
	{
		modelButtonAnim?.Play("Pressed", -1, 0f);
		_GoToChoose();
	}
	public void _GoToChoose()
	{
		if (!ColorModelPanelBar.activeSelf)
		{
			if (RecentColor.gameObject.activeSelf)
				_GotoRecentPanel();
			UnityEngine.Debug.Log("GoToChoose");
			var rect = ColorModelPanelBar.GetComponent<RectTransform>();
			// SelectButton.SetActive(value: false);
			var posD = rect.anchoredPosition;
			rect.anchoredPosition = new Vector2(posD.x, 452);
			ColorModelPanelBar.SetActive(value: true);
			var spin = ColorModelPanelBar.transform.Find("spin").GetComponent<Image>();
			LeanTween.value(spin.gameObject, v =>
			{
				var pos = rect.anchoredPosition;
				rect.anchoredPosition = new Vector2(pos.x, v);
			}, 452, 601, 0.5f);
			LeanTween.value(spin.gameObject, v =>
			{
				var c = Color.white;
				c.a = v;
				spin.color = c;
			}, 0f, 0.9f, 0.5f);
		}
		else
		{
			GotoColorPanel();
		} 
	}

	public void BackDefault()
	{
		// newColorThemeName = ApplicationModel.ColorThemeName;
		GotoColorPanel();
	}

	private void SetBtnSelected(string btnName)
	{
		ColorModelPanelBar.transform.Find("spin/Image").Find(btnName).GetComponent<Image>().enabled = true;	
	}

	private void SetBtnNotSelected(string btnName)
	{
        ColorModelPanelBar.transform.Find("spin/Image").Find(btnName).GetComponent<Image>().enabled = false;
    }

	// private string getPanelNameByColorName(string colorName)
	// {
	// 	if (colorName == "solid")
	// 	{
	// 		return SolidColorName;
	// 	}
	//
	// 	if (colorName == "theme")
	// 	{
	// 		return ThemeColorName;
	// 	}
	//
	// 	if (colorName == "gradient")
	// 	{
	// 		return GradientColorName;
	// 	}
	//
	// 	return "solid";
	// }

	// private GameObject GetCurColorBarGameObjectByName(string colorName)
	// {
	// 	Transform transform = base.transform.parent.Find("ColorPanel");
	// 	if (colorName == "solid")
	// 	{
	// 		return transform.Find("SolidColor").Find("PanelColorPureBar").gameObject;
	// 	}
	//
	// 	if (colorName == "theme")
	// 	{
	// 		return transform.Find("ThemeColor").Find("PanelColorThemeBar").gameObject;
	// 	}
	//
	// 	if (colorName == "gradient")
	// 	{
	// 		return transform.Find("GradientColor").Find("PanelColorGradientBar").gameObject;
	// 	}
	//
	// 	return transform.Find("SolidColor").Find("PanelColorPureBar").gameObject;
	// }

	private GameObject GetCurColorPanelGameObjectByName(string colorName)
	{
		Transform transform = base.transform.parent.Find("ColorPanel");
		if (colorName == "solid")
		{
			return transform.Find("SolidColor").gameObject;
		}

		if (colorName == "theme")
		{
			return transform.Find("ThemeColor").gameObject;
		}

		if (colorName == "gradient")
		{
			return transform.Find("GradientColor").gameObject;
		}

		return transform.Find("SolidColor").gameObject;
	}

	private void OnEnable()
	{
		StartCoroutine(setColorPanelClockTrue());
	}

	private IEnumerator setColorPanelClockTrue()
	{
		yield return new WaitForSeconds(0.5f);
		ClickEventManagement.ClickEventForColorPanel = true;
	}
}
