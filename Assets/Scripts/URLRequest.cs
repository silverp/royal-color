using System.Collections.Generic;
using UnityEngine;

public class URLRequest
{
	public enum REQ_METHOD
	{
		GET,
		POST
	}

	private string path;

	private Dictionary<string, string> Params;

	private WWWForm _body;

	private REQ_METHOD _method;

	private HttpRequest.CacheType _cacheType = HttpRequest.CacheType.MEMORY;

	public REQ_METHOD Method => _method;

	public HttpRequest.CacheType cacheType
	{
		get
		{
			return _cacheType;
		}
		set
		{
			_cacheType = value;
		}
	}

	public WWWForm body
	{
		get
		{
			return _body;
		}
		set
		{
			_body = value;
		}
	}

	public URLRequest(string path)
	{
		this.path = path;
		Params = new Dictionary<string, string>();
		Params.Add("language", LanguageHelper.GetLocaleCodeSystemLanguage());
		Params.Add("device", "android");
	}

	public URLRequest(string path, REQ_METHOD method)
		: this(path)
	{
		this.path = path;
		_method = method;
		_body = new WWWForm();
	}

	public string getUrl()
	{
		string text = path;
		string text2 = null;
		foreach (KeyValuePair<string, string> param in Params)
		{
			if (text2 != null)
			{
				string text3 = text2;
				text2 = text3 + "&" + param.Key + "=" + param.Value;
			}
			else
			{
				text2 = param.Key + "=" + param.Value;
			}
		}
		if (text2 != null)
		{
			text = text + "?" + text2;
		}
		return text;
	}

	public void setParams(Dictionary<string, string> param)
	{
		Params = param;
	}

	public void AddPageAndStepParams(int page, int step)
	{
		if (Params.ContainsKey("page"))
		{
			Params.Remove("page");
		}
		if (Params.ContainsKey("step"))
		{
			Params.Remove("step");
		}
		Params.Add("page", page.ToString());
		Params.Add("step", step.ToString());
	}

	public void appendParams(Dictionary<string, string> param)
	{
		if (param != null)
		{
			Params = param;
			foreach (KeyValuePair<string, string> item in param)
			{
				if (Params.ContainsKey(item.Key))
				{
					Params.Remove(item.Key);
				}
				Params.Add(item.Key, item.Value);
			}
		}
	}

	public void addParam(string key, string value)
	{
		if (Params.ContainsKey(key))
		{
			Params.Remove(key);
		}
		Params.Add(key, value);
	}

	public void addParam(string key, int value)
	{
		addParam(key, value.ToString());
	}

	public void addParam(string key, double value)
	{
		addParam(key, value.ToString());
	}

	public void addbody(string key, string value)
	{
		_body.AddField(key, value);
	}

	public void addbody(string key, int value)
	{
		addbody(key, value.ToString());
	}

	public void addbody(string key, double value)
	{
		addbody(key, value.ToString());
	}

	public void addbody(string key, byte[] contents, string fileName)
	{
		_body.AddBinaryData(key, contents, fileName);
	}
}
