using UnityEngine;

public class MyworkIsvisibleController : MonoBehaviour
{
	[SerializeField]
	private GameObject workContent;

	[SerializeField]
	private GameObject likeContent;

	[SerializeField]
	private GameObject userPanel;

	[SerializeField]
	private GameObject unloginUserPanel;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("mywork canvas enable!!!!");
		workContent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
		likeContent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, 0f);
		ApplicationModel.lastVisibleScreen = "MyworkCanvas";
		if (ApplicationModel.userInfo != null)
		{
			UnityEngine.Debug.Log("mywork canvas has userinfo");
			userPanel.SetActive(value: true);
			unloginUserPanel.SetActive(value: false);
		}
		else
		{
			UnityEngine.Debug.Log("mywork canvas has no userinfo");
			userPanel.SetActive(value: false);
			unloginUserPanel.SetActive(value: true);
		}
	}
}
