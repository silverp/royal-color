using UnityEngine;
using UnityEngine.UI;

public class CanvasscreenAutoFixForLoading : MonoBehaviour
{
	public static CanvasscreenAutoFixForLoading instance;

	private static float DEPEND_W = 1080f;

	private static float DEPEND_H = 1920f;

	public float scaleRatio = 1f;

	private void ResizeCanvas()
	{
		int width = Screen.width;
		int height = Screen.height;
		if (DEPEND_W == (float)width && DEPEND_H == (float)height)
		{
			scaleRatio = 1f;
			return;
		}
		float num = (float)width / DEPEND_W;
		float num2 = (float)height / DEPEND_H;
		float scaleFactor = (!(num < num2)) ? num2 : num;
		GetComponent<CanvasScaler>().scaleFactor = scaleFactor;
		scaleRatio = scaleFactor;
	}

	private void Awake()
	{
		ResizeCanvas();
		if (instance == null)
		{
			instance = this;
		}
	}
}
