using SVGImporter;
using System;
using System.Collections;
using UnityEngine;

public class PublicCallFrameTool : MonoBehaviour
{
	public static PublicCallFrameTool Instance;

	private float MAX_WAIT_TIME = 5f;

	private float time;

	private Coroutine _coroutine;
	private bool _disposed = false;

	private PublicCallFrameTool()
	{
	}

    private void OnEnable()
    {
		_disposed = false;
    }

    private void OnDisable()
    {
		_disposed = true;
    }

    private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void Update()
	{
		if (!(time < 0f))
		{
			time -= Time.deltaTime;
		}
	}

	public void CancelCollectGarbage()
	{
		if (_coroutine != null)
		{
			StopCoroutine(_coroutine);
		}
	}

	public void DelayCallFrames(Action callback, int afterFrames)
	{
		StartCoroutine(CallDelayedByFrames(callback, afterFrames));
	}

	private IEnumerator CallDelayedByFrames(Action callback, int afterFrames)
	{
		while (true)
		{
			int num;
			afterFrames = (num = afterFrames) - 1;
			if (num <= 0)
			{
				break;
			}
			yield return null;
		}
		callback();
		yield return null;
	}

	public void CollectGarbage()
	{
		CollectGarbage(force: false);
	}

	public void CollectGarbage(bool force)
	{
		if (!(time > 0f) || force)
		{
			time = MAX_WAIT_TIME;
			UnityEngine.Debug.Log("CollectGarbage!!!");
			if (!_disposed)
				_coroutine = StartCoroutine(startCollectGarbage());
		}
	}

	private IEnumerator startCollectGarbage()
	{
		yield return null;
		yield return null;
		Resources.UnloadUnusedAssets();
	}

	public void LoadSvgAsset(EditedImg img, Action<SVGAsset, Errcode> callback)
	{
		StartCoroutine(StartDownloadOutline(img, callback));
	}

	private IEnumerator StartDownloadOutline(EditedImg img, Action<SVGAsset, Errcode> callback)
	{
		UnityEngine.Debug.Log("StartDownloadOutline...");
		Errcode errcode = new Errcode();
		SVGAsset svgAsset2 = null;
		if (JudgeIsLoadCorrectAssetBundle(img.ouline))
		{
			using (var asset = WWW.LoadFromCacheOrDownload(img.ouline, 1))
			{
				try
				{
					yield return asset;
					AssetBundle bundle = asset.assetBundle;
					string bundleName = img.imageId;
					svgAsset2 = (bundle.LoadAsset(bundleName, typeof(SVGAsset)) as SVGAsset);
					bundle.Unload(unloadAllLoadedObjects: false);
					errcode.errorCode = 0;
					callback(svgAsset2, errcode);
					yield return null;
				}
				finally
				{
				}
			}
		}
		else
		{
			errcode.errorCode = 1;
			callback(svgAsset2, errcode);
		}
		yield return null;
	}

	private bool JudgeIsLoadCorrectAssetBundle(string url)
	{
		bool flag = false;
		return url.EndsWith("_android.outline");
	}
}
