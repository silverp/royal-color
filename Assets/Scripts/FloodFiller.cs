using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FloodFiller
{
	public struct Point
	{
		public short x;

		public short y;

		public Point(short aX, short aY)
		{
			x = aX;
			y = aY;
		}

		public Point(int aX, int aY)
		{
			this = new Point((short)aX, (short)aY);
		}
	}

	public static Color tarGetCol;

	public static IEnumerator RevisedQueueFloodFillCoroutine(Texture2D targetTex, int hitX, int hitY, Color replaceColor, bool dontPush)
	{
		Color32[] pixels = targetTex.GetPixels32();
		int w = targetTex.width;
		int h = targetTex.height;
		int batch = 2;
		UnityEngine.Debug.Log("hit X is " + hitX + "hit Y is :" + hitY + " image w:" + w + ", image h:" + h);
		Color targetColor = tarGetCol = pixels[hitX + hitY * w];
		if (targetColor == replaceColor)
		{
			yield return null;
		}
		Queue<Point> q = new Queue<Point>();
		q.Enqueue(new Point(hitX, hitY));
		int count = 0;
		while (q.Count > 0)
		{
			Point i = q.Dequeue();
			if (pixels[i.x + i.y * w] == targetColor && pixels[i.x + i.y * w] != Color.black)
			{
				Point t = i;
				while (t.x > 0 && pixels[t.x + t.y * w] == targetColor && pixels[t.x + t.y * w] != Color.black)
				{
					pixels[t.x + t.y * w] = replaceColor;
					t.x--;
				}
				int num = t.x + 1;
				t = i;
				t.x++;
				while (t.x < w - 1 && pixels[t.x + t.y * w] == targetColor && pixels[t.x + t.y * w] != Color.black)
				{
					pixels[t.x + t.y * w] = replaceColor;
					t.x++;
				}
				int num2 = t.x - 1;
				t = i;
				t.y++;
				Point u = i;
				u.y--;
				for (int j = num; j <= num2; j++)
				{
					t.x = (short)j;
					u.x = (short)j;
					if (t.y < h - 1 && pixels[t.x + t.y * w] == targetColor)
					{
						q.Enqueue(t);
					}
					if (u.y >= 0 && pixels[u.x + u.y * w] == targetColor)
					{
						q.Enqueue(u);
					}
				}
			}
			count++;
			if (count % batch == 0)
			{
				batch *= 2;
				targetTex.Apply();
				yield return count;
			}
		}
		targetTex.SetPixels32(pixels);
		yield return null;
	}

	public static void RevisedQueueFloodFill(Texture2D targetTex, int hitX, int hitY, Color replaceColor, bool dontPush)
	{
		UnityEngine.Debug.Log("we are in queueFloodFill");
		Color32[] pixels = targetTex.GetPixels32();
		int width = targetTex.width;
		int height = targetTex.height;
		UnityEngine.Debug.Log("hit X is " + hitX + "hit Y is :" + hitY + " image w:" + width + ", image h:" + height);
		Color color = tarGetCol = pixels[hitX + hitY * width];
		if (color == replaceColor)
		{
			return;
		}
		Queue<Point> queue = new Queue<Point>();
		queue.Enqueue(new Point(hitX, hitY));
		while (queue.Count > 0)
		{
			UnityEngine.Debug.Log("q.Count is :" + queue.Count);
			Point point = queue.Dequeue();
			if (!(pixels[point.x + point.y * width] == color) || !(pixels[point.x + point.y * width] != Color.black))
			{
				continue;
			}
			Point item = point;
			while (item.x > 0 && pixels[item.x + item.y * width] == color && pixels[item.x + item.y * width] != Color.black)
			{
				pixels[item.x + item.y * width] = replaceColor;
				item.x--;
			}
			int num = item.x + 1;
			item = point;
			item.x++;
			while (item.x < width - 1 && pixels[item.x + item.y * width] == color && pixels[item.x + item.y * width] != Color.black)
			{
				pixels[item.x + item.y * width] = replaceColor;
				item.x++;
			}
			int num2 = item.x - 1;
			item = point;
			item.y++;
			Point item2 = point;
			item2.y--;
			for (int i = num; i <= num2; i++)
			{
				item.x = (short)i;
				item2.x = (short)i;
				if (item.y < height - 1 && pixels[item.x + item.y * width] == color)
				{
					queue.Enqueue(item);
				}
				if (item2.y >= 0 && pixels[item2.x + item2.y * width] == color)
				{
					queue.Enqueue(item2);
				}
			}
		}
		targetTex.SetPixels32(pixels);
	}
}
