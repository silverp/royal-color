using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class CreateColorBarController : MonoBehaviour//, LoopScrollPrefabSource, LoopScrollDataSource
{
	public enum ColorTheme
	{
		None,
		Gradient,
		Solid,
		Thematic 
	}
	[SerializeField] 
	private UIColorButton[] _colorButtons;

	[SerializeField] private Transform objectPools;
	[SerializeField] private Text _name;
	[SerializeField] private GameObject colorItem;
	
	[SerializeField] private RectTransform rootContainer;
	// [SerializeField] private ScrollSnapBigCenter _scrollSnap;
	
	[SerializeField]
	private HorizontalScrollSnap horizontalScrollSnap;

	private ScrollRect _scrollRect;
	private UI_InfiniteScroll _infiniteScroll;

	[SerializeField]
	private Image HistoryBtnImage;

	private RawImage gradientHistory; 
	private ColorTheme colorTheme = ColorTheme.None;
 
	public Texture2D gradientTexture; 

    private float leftDeviation;

	private List<ColorTheme> currentTheme; 

	private int oldIndex; 
	private ColorTheme lastSnapTheme = ColorTheme.None;
	private int lastSnapPage = -1;
	private float realItemWidth = 0;

	private float GetItemWidth()
	{
		if (realItemWidth == 0)
			realItemWidth = rootContainer.sizeDelta.x / 5f;
		return realItemWidth;
	}
	void onPageSnapChange(int page)
	{
		if (page == lastSnapPage && this.colorTheme == lastSnapTheme)
			return;
		Debug.Log($"onPageSnapChange {page}");
		this.lastSnapPage = page;
		this.lastSnapTheme = this.colorTheme;
		InitColorButtonBar(this.colorTheme, page);
	}
	// Implement your own Cache Pool here. The following is just for example.
	Stack<Transform> pool = new Stack<Transform>();
	public GameObject GetObject()
	{
		if (pool.Count == 0)
		{
			var item = Instantiate(colorItem);
			// item.name = $"Cell {index}";
			var t = item.GetComponent<RectTransform>();
			var delta = t.sizeDelta;
			delta.x = GetItemWidth();
			t.sizeDelta = delta;
			var b = t.GetComponent<ColorItemButton>();
			if (!b)
			{
				b = t.AddComponent<ColorItemButton>();
				b.colorIndex = 0;
				b.onClick.AddListener(() =>
				{
					horizontalScrollSnap.GoToScreen(b.colorIndex, true);
                    //SetCurrentPage(b.colorIndex, true);
				});
			}
			return item;
		}
		Transform candidate = pool.Pop();
		candidate.gameObject.SetActive(true);
		return candidate.gameObject;
	}
	//
	public void ReturnObject(Transform trans)
	{
		// Use `DestroyImmediate` here if you don't need Pool
		trans.SendMessage("ScrollCellReturn", SendMessageOptions.DontRequireReceiver);
		trans.gameObject.SetActive(false);
		trans.SetParent(objectPools, false);
		pool.Push(trans);
	}

	private int GetScrolledPage(int page)
	{
        if (_scrollRect.content.childCount == 0)
        {
            return 0;
        }
		return page;// (page + 3) % _scrollRect.content.childCount;
    }
	private bool lockedPageChange = false;
	private void SetCurrentPage(int page, bool forceEvents = false)
	{
        if (_scrollRect.content.childCount > 0)
        {
			if (!forceEvents)
				lockedPageChange = true;
   //         var realPage = page - 3;
			//while (realPage < 0)
			//	realPage += _scrollRect.content.childCount;

            horizontalScrollSnap.ChangePage(page);
            if (!forceEvents)
                lockedPageChange = false;
        }
    }

	private ColorTheme lastScrolledTheme = ColorTheme.None;
	private int lastScrolledPage = -1;
	private Coroutine pageChangeCo;
	public void OnPageChange(int page)
	{
		if (lockedPageChange) return;
		if (isFirstTime || _scrollRect.content.childCount == 0) return;
		if (pageChangeCo != null)
			StopCoroutine(pageChangeCo);
		pageChangeCo = StartCoroutine(LazyPageChange(page)); 
    }

	private IEnumerator LazyPageChange(int page)
	{
		yield return new WaitForSeconds(0.1f);
        if (lastScrolledTheme == this.colorTheme && lastScrolledPage == page) yield break;
        lastScrolledPage = page;
        lastScrolledTheme = this.colorTheme;
		var realPage = GetScrolledPage(page);
        Debug.LogWarning($"Page {realPage}");
        onPageSnapChange(realPage);
    }

	private void Awake()
	{
		_scrollRect = horizontalScrollSnap.GetComponent<ScrollRect>();
		_infiniteScroll = horizontalScrollSnap.GetComponent<UI_InfiniteScroll>();
		horizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(OnPageChange);
		gradientHistory = HistoryBtnImage.transform.Find("gradient").GetComponent<RawImage>();
		gradientHistory.material = new Material(Shader.Find("Unlit/RadialGradient"));
		gradientHistory.material.SetTexture("_RampTex", gradientTexture);

		if (!ApplicationModel.ColorThemesGradient.fetchedGradientColors)
		{
			ApplicationModel.ColorThemesGradient.fetchedGradientColors = true;
			foreach (var g in ApplicationModel.ColorThemesGradient.gradientColors)
			{
				g.bgColor = gradientTexture.GetPixel(g.colorCoordinate.x * 4, g.colorCoordinate.y * 64 + 63); 
			}
		}
		foreach(var b in _colorButtons)
			b.Init(gradientTexture); 
	}
	private static readonly Color32 DEFAULT_HISTORY_BACK = new Color32(172, 172, 172, 255);
	public void SetHistoryGradientCoordColor(Vector2 point)
	{
		HistoryBtnImage.color = DEFAULT_HISTORY_BACK;
		gradientHistory.gameObject.SetActive(true);
		gradientHistory.material.SetInt("_RampTexCoordX", (int)Mathf.Ceil(point.x));
		gradientHistory.material.SetInt("_RampTexCoordY", (int)Mathf.Ceil(point.y));
	}

	public void SetHistoryColor(Color color)
	{
		gradientHistory.gameObject.SetActive(false);
		HistoryBtnImage.color = color;
	}

	private void OnDisable()
    { 
        DiscardEvents(); 
    }

	private void OnEnable()
    { 
        AssginEvents();
    }

	private void AssginEvents()
	{
		// _scrollSnap.onPageChange += onPageSnapChange;
		DragEventProxy.OnChangeColorlistPage += ChangeColorlistPage;
	}

	private void DiscardEvents()
	{
		// _scrollSnap.onPageChange -= onPageSnapChange;
		DragEventProxy.OnChangeColorlistPage -= ChangeColorlistPage;
	}

	private void ChangeColorlistPage(UnityEngine.Object sender, int index)
	{
		UnityEngine.Debug.Log("ChangeColorlistPage.......");
		//changePage(index);
	}

	private IEnumerator Start()
	{
		yield return new WaitForSeconds(0.1f);
        InitColorButtonBar(CreateColorBarController.ColorTheme.Gradient);
        //InitColorButtonBar(ColorTheme.Gradient, -999);
        //SetHistoryBtnFalse();
        //changePage(0);
    }
 
	private Color32 getColorByColorIndex(int x, int y, int index)
	{
		return new Color32((byte)(x * 9 + index), (byte)y, 0, 0);
	} 
	private float getIndexByGradientColor(int x, int index)
	{
		return x * 9 + index;
	}

	public static readonly Color32 DEFAULT_COLOR = new Color32(111, 181, 255, 255);
	private bool isFirstTime = true; 

	public void RemoveAllSelection()
	{
		foreach (var c in _colorButtons)
		{
			c.RemoveSelection();
		}
	}
	private void Select(int page, UIColorButton b)
	{
		RemoveAllSelection();
		if (b)
        {
			_selectedmap[colorTheme] = new Tuple<int, int>(page, b.index);
			b.Select();

		}
	} 
	
	private void AddColorToRecentList()
	{ 
		if (ApplicationModel.recentColorlist != null)
		{
			ColorComplex existItem = null;
			foreach (ColorComplex item in ApplicationModel.recentColorlist)
			{
				if (item.IsEqual(ApplicationModel.currentColorComplex))
				{
					existItem = item;
					break;
				}
			}

			if (existItem != null)
				ApplicationModel.recentColorlist.Remove(existItem);
		}
		else
		{
			ApplicationModel.recentColorlist = new List<ColorComplex>();
		}
		if (ApplicationModel.recentColorlist.Count >= 8)
		{
			ApplicationModel.recentColorlist.RemoveAt(0);
		}
		ApplicationModel.recentColorlist.Add(ApplicationModel.currentColorComplex);
		DragEventProxy.Instance.SendAddColorToRecentList();
	}
	private class ColorItem
	{
		public string Name { get; set; }
		public Color Color { get; set; }
	}
	private IEnumerator FillBottomColors(List<ColorItem> colors, Action<int> callback, int index)
	{
		if (!_scrollRect) yield break;
		// currentColorData = colors.ToList();
		// reversedCurrentColorData = colors.ToList();
		// reversedCurrentColorData.Reverse();
		lockedPageChange = true;
		horizontalScrollSnap.RemoveAllChildren(out GameObject[] children);
		foreach(var c in children)
			ReturnObject(c.transform);

		List<GameObject> GOs = new List<GameObject>();
		for(int i = 0; i < colors.Count; i++)
		{
			var c = colors[i];
			var item = GetObject();
			item.name = c.Name;// "Color " + (i+1);
			item.GetComponent<Image>().color = c.Color;
			item.GetComponent<ColorItemButton>().colorIndex = i;
            GOs.Add(item);
		}
		horizontalScrollSnap.AddRange(GOs);
		
		_infiniteScroll?.ResetItems();
		_infiniteScroll?.Init();
		horizontalScrollSnap.UpdateLayout();
		lockedPageChange = false;

        callback?.Invoke(index);
		_infiniteScroll.OnScroll(Vector2.zero);
        // _scrollRect.RefillCells(); 
    }

	private Dictionary<ColorTheme, Tuple<int,int>> _selectedmap = new();
    //private Dictionary<ColorTheme, int> _selectedmap = new();
    public void InitColorButtonBar(ColorTheme colorTheme, int pageIndex = -1)
	{ 
		List<ColorItem> lazyColors = null;

        if (pageIndex < 0)
		{
			if (pageIndex != -999)
			{
				if (colorTheme == this.colorTheme)
				{
					return;
				}
			}

			if (_selectedmap.ContainsKey(colorTheme))
			{
				pageIndex = _selectedmap[colorTheme].Item1;
			}
			else
			{
				pageIndex = 0;
				_selectedmap[colorTheme] = new Tuple<int, int>(pageIndex, -1);
			}

			ApplicationModel.Initialize();
			List<ColorItem> colors = new List<ColorItem>();
			if (colorTheme == ColorTheme.Gradient)
			{
				colors.AddRange(ApplicationModel.ColorThemesGradient.gradientColors.Select(t => new ColorItem() { Color = t.bgColor, Name = t.name }));
			}
			else if (colorTheme == ColorTheme.Solid)
			{
				colors.AddRange(ApplicationModel.ColorThemesPure.Select(t => new ColorItem() { Color = t.bgColorUnity, Name = t.Name }));
			}
			else if (colorTheme == ColorTheme.Thematic)
			{
				colors.AddRange(ApplicationModel.ColorThemes.Select(t => new ColorItem() { Color = t.bgColorUnity, Name = t.Name }));
			}

			lazyColors = colors; 
        }

		this.colorTheme = colorTheme;
		if (colorTheme == ColorTheme.Gradient)
		{
			GradientColor gradientColor = ApplicationModel.ColorThemesGradient.gradientColors[pageIndex];
			_name.text = gradientColor.name;
			Point colorCoordinate = ApplicationModel.ColorThemesGradient.gradientColors[pageIndex].colorCoordinate; 
			int count = ApplicationModel.ColorThemesGradient.gradientColors[pageIndex].colorIndex.Count; 
			for (int i = 0; i < count + 1; i++)
			{
				var b = _colorButtons[i];
				b.EnableGradientMode(true);
				b.SetColor(DEFAULT_COLOR);
				b.pageIndex = pageIndex;
				b.index = i;
				Color32 color;
				Vector2 colorCoord;
				if (i == count)
				{
					color = getColorByColorIndex(colorCoordinate.x, 0, gradientColor.colorIndex[0]);
					colorCoord =
						new Vector2(getIndexByGradientColor(colorCoordinate.x, gradientColor.colorIndex[0]), 0);
				}
				else
				{
					color = getColorByColorIndex(colorCoordinate.x, colorCoordinate.y,
						gradientColor.colorIndex[i]);
					colorCoord =
						new Vector2(getIndexByGradientColor(colorCoordinate.x, gradientColor.colorIndex[i]),
							colorCoordinate.y);
				}

				b.attachedColor = color;
				b.SetGradientCoordColor(colorCoord);
				b.PlayAnim();
				var button = b.GetComponent<Button>();
				button.onClick.RemoveAllListeners();
				button.onClick.AddListener(() =>
				{
					Select(pageIndex, b);
					onClickGradient(b.attachedColor, b.coordColor);
					AddColorToRecentList();
				});
			}
		} 
		else if (colorTheme == ColorTheme.Solid)
		{ 
			var colors = ApplicationModel.ColorThemesPure[pageIndex].unityColors;
			_name.text = ApplicationModel.ColorThemesPure[pageIndex].Name;
			int count = colors.Count; 
			for (int i = 0; i < count; i++)
			{ 
				Color32 color = colors[i];
				var b = _colorButtons[i]; 
				b.EnableGradientMode(false); 
				b.SetColor(color); 
				b.PlayAnim();
				b.pageIndex = pageIndex;
				b.index = i;
				b.attachedColor = color;
				var button = _colorButtons[i].GetComponent<Button>();
				button.onClick.RemoveAllListeners();
				button.onClick.AddListener(() =>
				{
					Select(pageIndex, b);
					onClick(b.attachedColor);
					AddColorToRecentList();
				});
			}
		}
		else if (colorTheme == ColorTheme.Thematic)
		{
			var colors = ApplicationModel.ColorThemes[pageIndex].unityColors;
			_name.text = ApplicationModel.ColorThemes[pageIndex].Name;
			int count = colors.Count; 
			for (int i = 0; i < count; i++)
			{
				var b = _colorButtons[i];
				Color32 color = colors[i];
				b.EnableGradientMode(false); 
				b.SetColor(color); 
				b.PlayAnim();
				b.pageIndex = pageIndex;
				b.index = i;
				b.attachedColor = color;
				var button = _colorButtons[i].GetComponent<Button>();
				button.onClick.RemoveAllListeners();
				button.onClick.AddListener(() =>
				{
					Select(pageIndex, b);
					onClick(b.attachedColor);
					AddColorToRecentList();
				});
			}
		}

		var action = new Action<int>(idx =>
		{
            if (isFirstTime)
            {
				SetCurrentPage(0);
				isFirstTime = false;

                var b = _colorButtons.Last();
                Select(idx, b);

                onClickGradient(b.attachedColor, b.coordColor);
            }
            else if(_selectedmap.ContainsKey(colorTheme))//if (this.colorTheme == selectedTheme && this.selectedPage == idx)
            {
				var s = _selectedmap[colorTheme];
				if (s.Item1 == idx)
				{
                    SetCurrentPage(idx);

                    Select(idx, _colorButtons.FirstOrDefault(t => t.index == s.Item2));
				}
				else
					Select(-1, null);
            }
			else
			{
				Select(-1, null);
			}
		});
		if (lazyColors != null)
		{
            StartCoroutine(FillBottomColors(lazyColors, action, pageIndex));
        } 
		else
		{
			action(pageIndex);
        } 
	}

	private void onClick(Color col)
	{
		ApplicationModel.IsGradient = false;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(false, col, new Vector2(0f, 0f));
		SetHistoryColor(col);
	}	

	private void onClickGradient(Color col, Vector2 colorcoord)
	{
		ApplicationModel.IsGradient = true;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(true, col, colorcoord);
		SetHistoryGradientCoordColor(colorcoord);
	} 

	private void changePage(int index)
	{ 
	}  
	private void OnDestroy()
	{
		DiscardEvents();
	} 
	private void SetHistoryBtnFalse()
	{ 
    } 
}
