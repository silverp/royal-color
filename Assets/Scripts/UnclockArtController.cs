using System;
using UnityEngine;
using UnityEngine.UI;

public class UnclockArtController : MonoBehaviour
{
	[SerializeField]
	private GameObject popPanel;

	[SerializeField]
	private GameObject PurchasePanel;

	private void AssginEvents()
	{
		DragEventProxy.OnUnclockOutline += unclockPagePOP;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnUnclockOutline -= unclockPagePOP;
	}

	private void OnEnable()
	{
		AssginEvents();
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void unclockPagePOP(ResourceImg image, PixelArt pixelArtImage, ImageType type)
	{
		UnityEngine.Debug.Log("unclockPagePOP...");
		popPanel.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
		popPanel.SetActive(value: true);
		if (type == ImageType.normal)
		{
			PurchasePanel.transform.Find("GameObject").GetComponent<OpenVipController>().curImg = image;
			PurchasePanel.transform.Find("GameObject").GetComponent<OpenVipController>().imageType = ImageType.normal;
			NetLoadcontroller.Instance.RequestImg(image.thumbnailUrl, delegate(Texture2D tex)
			{
				popPanel.transform.Find("mask").Find("Image").GetComponent<RawImage>()
					.texture = tex;
				});
				popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().currentImgId = image.imageId;
				popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().type = "normal";
				popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().resourceImg = image;
			}
			else
			{
				PurchasePanel.transform.Find("GameObject").GetComponent<OpenVipController>().curPixelArtImg = pixelArtImage;
				PurchasePanel.transform.Find("GameObject").GetComponent<OpenVipController>().imageType = ImageType.pixel;
				NetLoadcontroller.Instance.RequestImg(pixelArtImage.baseUrl, delegate(Texture2D tex)
				{
					popPanel.transform.Find("mask").Find("Image").GetComponent<RawImage>()
						.texture = tex;
					});
					popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().type = "pixel";
					popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().currentImgId = pixelArtImage.imageId;
					popPanel.transform.Find("GameObject").GetComponent<AdButtonController>().pixelArtImage = pixelArtImage;
				}
				LeanTween.scale(popPanel.GetComponent<RectTransform>(), new Vector3(1f, 1f, 1f), 0.25f).setOnComplete((Action)delegate
				{
					UnityEngine.Debug.Log("complete scale!!!");
				});
			}
		}
