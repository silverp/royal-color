using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TextEmojiConvert : MonoBehaviour
{
	private struct PosStringTuple
	{
		public int pos;

		public string emoji;

		public PosStringTuple(int p, string s)
		{
			pos = p;
			emoji = s;
		}
	}

	[SerializeField]
	public TextAsset textAsset;

	[SerializeField]
	public GameObject rawImageToClone_big;

	[SerializeField]
	public GameObject rawImageToClone_small;

	private Dictionary<string, Rect> emojiRects = new Dictionary<string, Rect>();

	private static char emSpace = '\u2001';

	public static TextEmojiConvert Instance;

	private readonly float big_offset = -17f;

	private readonly float small_offset = -20f;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void Start()
	{
		ParseEmojiInfo(textAsset.text);
	}

	private void DestoryChild(Transform tranform)
	{
		IEnumerator enumerator = tranform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	public void ConvertEmoji(Transform transform, string content, bool isBig)
	{
		DestoryChild(transform);
		StartCoroutine(SetUITextThatHasEmoji(transform.GetComponent<Text>(), content, isBig));
	}

	private static string GetConvertedString(string inputString)
	{
		string[] array = inputString.Split('-');
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = char.ConvertFromUtf32(Convert.ToInt32(array[i], 16));
		}
		return string.Join(string.Empty, array);
	}

	private void ParseEmojiInfo(string inputString)
	{
		using (StringReader stringReader = new StringReader(inputString))
		{
			string text = stringReader.ReadLine();
			while (text != null && text.Length > 1)
			{
				string[] array = text.Split(' ');
				float x = float.Parse(array[1], CultureInfo.InvariantCulture);
				float y = float.Parse(array[2], CultureInfo.InvariantCulture);
				float width = float.Parse(array[3], CultureInfo.InvariantCulture);
				float height = float.Parse(array[4], CultureInfo.InvariantCulture);
				emojiRects[GetConvertedString(array[0])] = new Rect(x, y, width, height);
				text = stringReader.ReadLine();
			}
		}
	}

	public IEnumerator SetUITextThatHasEmoji(Text textToEdit, string inputString, bool isBig)
	{
		List<PosStringTuple> emojiReplacements = new List<PosStringTuple>();
		StringBuilder sb = new StringBuilder();
		int i = 0;
		while (i < inputString.Length)
		{
			string text = inputString.Substring(i, 1);
			string text2 = string.Empty;
			string text3 = string.Empty;
			if (i < inputString.Length - 1)
			{
				text2 = inputString.Substring(i, 2);
			}
			if (i < inputString.Length - 3)
			{
				text3 = inputString.Substring(i, 4);
			}
			if (emojiRects.ContainsKey(text3))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text3));
				i += 4;
			}
			else if (emojiRects.ContainsKey(text2))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text2));
				i += 2;
			}
			else if (emojiRects.ContainsKey(text))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text));
				i++;
			}
			else
			{
				sb.Append(inputString[i]);
				i++;
			}
		}
		textToEdit.text = sb.ToString();
		yield return null;
		TextGenerator textGen = textToEdit.cachedTextGenerator;
		Vector3 localPosition = default(Vector3);
		for (int j = 0; j < emojiReplacements.Count; j++)
		{
			PosStringTuple posStringTuple = emojiReplacements[j];
			int pos = posStringTuple.pos;
			GameObject gameObject;
			float offset_Y;
			if (isBig)
			{
				gameObject = UnityEngine.Object.Instantiate(rawImageToClone_big);
				offset_Y = big_offset;
			}
			else
			{
				gameObject = UnityEngine.Object.Instantiate(rawImageToClone_small);
				offset_Y = small_offset;
			}
			gameObject.transform.SetParent(textToEdit.transform);
			if (pos * 4 <= textGen.verts.Count - 1)
			{
				UIVertex uIVertex = textGen.verts[pos * 4];
				float x = uIVertex.position.x / CanvasScreenAutoFix.instance.scaleRatio;
				UIVertex uIVertex2 = textGen.verts[pos * 4];
				localPosition = new Vector3(x, uIVertex2.position.y, 0f);
				gameObject.transform.localPosition = localPosition;
				RectTransform component = gameObject.transform.GetComponent<RectTransform>();
				Vector2 anchoredPosition = gameObject.transform.GetComponent<RectTransform>().anchoredPosition;
				component.anchoredPosition = new Vector3(anchoredPosition.x, offset_Y, 0f);
				gameObject.transform.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
				RawImage component2 = gameObject.GetComponent<RawImage>();
				RawImage rawImage = component2;
				Dictionary<string, Rect> dictionary = emojiRects;
				PosStringTuple posStringTuple2 = emojiReplacements[j];
				rawImage.uvRect = dictionary[posStringTuple2.emoji];
			}
		}
	}
}
