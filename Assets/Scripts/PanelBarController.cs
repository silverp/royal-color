using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PanelBarController : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler, IEventSystemHandler
{
	public ScrollRect XScrollRect;

	public void OnEndDrag(PointerEventData eventData)
	{
		Vector3 localPosition = base.transform.Find("content").localPosition;
		if (localPosition.x > 0f)
		{
			RectTransform component = base.transform.Find("content").gameObject.GetComponent<RectTransform>();
			Vector3 localPosition2 = base.transform.Find("content").localPosition;
			StartCoroutine(MoveToPosition(component, new Vector2(0f, localPosition2.y), 0.4f));
		}
		Transform transform = base.transform.Find("content");
		Vector3 localPosition3 = base.transform.Find("content").localPosition;
		transform.localPosition = new Vector2(0f, localPosition3.y);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		UnityEngine.Debug.Log("on beginDrag");
	}

	public void OnDrag(PointerEventData eventData)
	{
		UnityEngine.Debug.Log("on drag");
	}

	private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
	{
		UnityEngine.Debug.Log("moving!");
		Vector2 currentPos = rectTransform.anchoredPosition;
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / TimeToMove;
			rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
			yield return null;
		}
	}
}
