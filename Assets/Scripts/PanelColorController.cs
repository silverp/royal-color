using UnityEngine;

public class PanelColorController : MonoBehaviour
{
	private void Start()
	{
		AssginEvents();
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("OnEnable  XXXXXXXXXXXXXXXXXX");
		if (base.transform.gameObject.GetComponent<ScrollSnapRect>() == null)
		{
			base.transform.gameObject.AddComponent<ScrollSnapRect>();
		}
		ClickEventManagement.ColorThemeName = base.name;
	}

	private void OnDisable()
	{
	}

	private void AssginEvents()
	{
		DragEventProxy.OnCloseWorkShopCanvas += CloseWorkShopCanvas;
		DragEventProxy.OnOpenWorkShopCanvas += OpenWorkShopCanvas;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnCloseWorkShopCanvas -= CloseWorkShopCanvas;
		DragEventProxy.OnOpenWorkShopCanvas -= OpenWorkShopCanvas;
	}

	private void OpenWorkShopCanvas()
	{
		if (base.transform.gameObject.GetComponent<ScrollSnapRect>() == null)
		{
			base.transform.gameObject.AddComponent<ScrollSnapRect>();
		}
	}

	private void CloseWorkShopCanvas()
	{
		UnityEngine.Object.Destroy(base.transform.gameObject.GetComponent<ScrollSnapRect>());
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}
}
