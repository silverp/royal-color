using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DragEventSyn : MonoBehaviour, IEndDragHandler, IBeginDragHandler, IDragHandler, IEventSystemHandler
{
	public ScrollRect XScrollRect;

	public void OnEndDrag(PointerEventData eventData)
	{
		if ((bool)XScrollRect)
		{
			DragEventProxy.Instance.SendDragEnd(eventData);
			XScrollRect.OnEndDrag(eventData);
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if ((bool)XScrollRect)
		{
			DragEventProxy.Instance.SendDragBegin(eventData);
			XScrollRect.OnBeginDrag(eventData);
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if ((bool)XScrollRect)
		{
			DragEventProxy.Instance.SendDragXMsg(eventData);
			XScrollRect.OnDrag(eventData);
		}
	}
}
