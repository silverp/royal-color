using UnityEngine;
using UnityEngine.UI;

public class OtherSexController : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		Texture2D texture2D = null;
		if (ApplicationModel.otherUserInfo != null && ApplicationModel.otherUserInfo.Sex != null)
		{
			if (ApplicationModel.otherUserInfo.Sex == "m")
			{
				texture2D = Resources.Load<Texture2D>("Images/Mywork/male");
			}
			else if (ApplicationModel.otherUserInfo.Sex == "f")
			{
				texture2D = Resources.Load<Texture2D>("Images/Mywork/female");
			}
		}
		if (texture2D != null)
		{
			GetComponent<RawImage>().enabled = true;
			GetComponent<RawImage>().texture = texture2D;
		}
		else
		{
			GetComponent<RawImage>().enabled = false;
		}
	}
}
