using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BottomBannerController : MonoBehaviour
{
	// private GameObject MyworkCanvas;
	//
	// private GameObject InspirationCanvas;
	//
	// private GameObject ImglistCanvas;
	//
	// private GameObject AritistCanvas;
	//
	public Transform MyworkButton;
	
	public Transform InspirationButton;
	
	public Transform ImglistButton;
	
	public Transform AritistButton;

	private Color activeCol;

	private Color disableCol;

	private string activeColStr = "#4d4d4dff";

	private string disableColStr = "#999999ff";

	[SerializeField] private ScrollSnapRect _snapRect;

	private void Start()
	{
		// MyworkCanvas = base.transform.parent.parent.Find("MyworkCanvas").gameObject;
		// InspirationCanvas = base.transform.parent.parent.Find("InspirationCanvas").gameObject;
		// ImglistCanvas = base.transform.parent.parent.Find("ImglistCanvas").gameObject;
		// AritistCanvas = base.transform.parent.parent.Find("AritistCanvas").gameObject;
		ColorUtility.TryParseHtmlString(activeColStr, out activeCol);
		ColorUtility.TryParseHtmlString(disableColStr, out disableCol);
		init();
	}

	private void init()
	{
		// SetAllBtnGray();
		// string name = base.transform.parent.name;
		// if (name == MyworkCanvas.name)
		// {
		// 	SetBtnActive(base.transform.GetChild(3));
		// }
		// else if (name == InspirationCanvas.name)
		// {
		// 	SetBtnActive(base.transform.GetChild(2));
		// }
		// else if (name == ImglistCanvas.name)
		// {
		// 	SetBtnActive(base.transform.GetChild(0));
		// }
		// else if (name == AritistCanvas.name)
		// {
		// 	SetBtnActive(base.transform.GetChild(1));
		// }
	}

	public void GotoMyWork()
	{
		_snapRect.ScrollTo(MyworkButton);
		// SetAllCanvasDisable();
		// UIRoot.ShowUI(MyworkCanvas);
	}

	public void GotoArtistCanvas()
	{
		_snapRect.ScrollTo(AritistButton);
		// SetAllCanvasDisable();
		// AritistCanvas.SetActive(value: true);
	}

	public void GotoInspiration()
	{
		_snapRect.ScrollTo(InspirationButton);
		// SetAllCanvasDisable();
		// UIRoot.ShowUI(InspirationCanvas);
	}

	public void GotoImageList()
	{
		_snapRect.ScrollTo(ImglistButton);
		// SetAllCanvasDisable();
		// UIRoot.ShowUI(ImglistCanvas);;
	}

	private void SetAllCanvasDisable()
	{
		// UIRoot.HideUI();
		// UIRoot.HideUI();
		// UIRoot.HideUI();
		// UIRoot.HideUI();;
	}

	private void SetAllBtnGray()
	{
		// SetBtnGray(base.transform.GetChild(0));
		// SetBtnGray(base.transform.GetChild(1));
		// SetBtnGray(base.transform.GetChild(2));
		// SetBtnGray(base.transform.GetChild(3));
	}

	private void SetBtnGray(Transform transform)
	{
		transform.Find("Text").GetComponent<Text>().color = disableCol;
		transform.GetChild(0).gameObject.SetActive(value: false);
		transform.GetChild(1).gameObject.SetActive(value: true);
	}

	private void SetBtnActive(Transform transform)
	{
		transform.Find("Text").GetComponent<Text>().color = activeCol;
		transform.GetChild(0).gameObject.SetActive(value: true);
		transform.GetChild(1).gameObject.SetActive(value: false);
	}

	private void DestoryChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform2.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
