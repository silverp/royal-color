using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ShowOffEmoji : MonoBehaviour
{
	private struct PosStringTuple
	{
		public int pos;

		public string emoji;

		public PosStringTuple(int p, string s)
		{
			pos = p;
			emoji = s;
		}
	}

	public TextAsset textAsset;

	public Text bicycleAndUSFlagText;

	public Text footballText;

	public Text receipeText;

	public RawImage rawImageToClone;

	private Dictionary<string, Rect> emojiRects = new Dictionary<string, Rect>();

	private static char emSpace = '\u2001';

	private void Awake()
	{
	}

	private void Start()
	{
		ParseEmojiInfo(textAsset.text);
		StartCoroutine(SetUITextThatHasEmoji(bicycleAndUSFlagText, "bicyclist: \ud83d\udeb4, and US flag: \ud83c\uddfa\ud83c\uddf8"));
		StartCoroutine(SetUITextThatHasEmoji(footballText, "⚽ ➕ ❤ = I love football"));
		StartCoroutine(SetUITextThatHasEmoji(receipeText, "\ud83c\udfb5 \ud83c\udfb6 \ud83c\udfb7 \ud83c\udfb8 \ud83c\udfb9 \ud83c\udfba"));
	}

	private static string GetConvertedString(string inputString)
	{
		string[] array = inputString.Split('-');
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = char.ConvertFromUtf32(Convert.ToInt32(array[i], 16));
		}
		return string.Join(string.Empty, array);
	}

	private void ParseEmojiInfo(string inputString)
	{
		using (StringReader stringReader = new StringReader(inputString))
		{
			string text = stringReader.ReadLine();
			while (text != null && text.Length > 1)
			{
				string[] array = text.Split(' ');
				float x = float.Parse(array[1], CultureInfo.InvariantCulture);
				float y = float.Parse(array[2], CultureInfo.InvariantCulture);
				float width = float.Parse(array[3], CultureInfo.InvariantCulture);
				float height = float.Parse(array[4], CultureInfo.InvariantCulture);
				emojiRects[GetConvertedString(array[0])] = new Rect(x, y, width, height);
				text = stringReader.ReadLine();
			}
		}
	}

	public void SetReceipeTextFromJavascript(string input)
	{
		IEnumerator enumerator = receipeText.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		StartCoroutine(SetUITextThatHasEmoji(receipeText, input));
	}

	public IEnumerator SetUITextThatHasEmoji(Text textToEdit, string inputString)
	{
		List<PosStringTuple> emojiReplacements = new List<PosStringTuple>();
		StringBuilder sb = new StringBuilder();
		int i = 0;
		while (i < inputString.Length)
		{
			string text = inputString.Substring(i, 1);
			string text2 = string.Empty;
			string text3 = string.Empty;
			if (i < inputString.Length - 1)
			{
				text2 = inputString.Substring(i, 2);
			}
			if (i < inputString.Length - 3)
			{
				text3 = inputString.Substring(i, 4);
			}
			if (emojiRects.ContainsKey(text3))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text3));
				i += 4;
			}
			else if (emojiRects.ContainsKey(text2))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text2));
				i += 2;
			}
			else if (emojiRects.ContainsKey(text))
			{
				sb.Append(emSpace);
				emojiReplacements.Add(new PosStringTuple(sb.Length - 1, text));
				i++;
			}
			else
			{
				sb.Append(inputString[i]);
				i++;
			}
		}
		textToEdit.text = sb.ToString();
		yield return null;
		TextGenerator textGen = textToEdit.cachedTextGenerator;
		Vector3 localPosition = default(Vector3);
		for (int j = 0; j < emojiReplacements.Count; j++)
		{
			PosStringTuple posStringTuple = emojiReplacements[j];
			int pos = posStringTuple.pos;
			GameObject gameObject = UnityEngine.Object.Instantiate(rawImageToClone.gameObject);
			gameObject.transform.SetParent(textToEdit.transform);
			UIVertex uIVertex = textGen.verts[pos * 4];
			float x = uIVertex.position.x;
			UIVertex uIVertex2 = textGen.verts[pos * 4];
			localPosition = new Vector3(x, uIVertex2.position.y, 0f);
			gameObject.transform.localPosition = localPosition;
			RawImage component = gameObject.GetComponent<RawImage>();
			RawImage rawImage = component;
			Dictionary<string, Rect> dictionary = emojiRects;
			PosStringTuple posStringTuple2 = emojiReplacements[j];
			rawImage.uvRect = dictionary[posStringTuple2.emoji];
		}
	}
}
