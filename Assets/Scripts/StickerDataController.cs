using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class StickerDataController : MonoBehaviour
{
	[SerializeField]
	private GameObject DiscPanel;

	[SerializeField]
	private GameObject workPlace;

	[SerializeField]
	private GameObject StickerCanvas;

	[SerializeField]
	private GameObject ShareCanvas;

	[SerializeField]
	private GameObject Stickeritem;

	[SerializeField]
	private GameObject WorkShopCanvas;

	private readonly string ICON_PATH = "Images/stickerIcon/";

	private readonly string IMAGE_PATH = "Images/stickerImage/";

	private List<Sticker> stickerList;

	private Texture2D mainTexture;

	private string CurrentImageId;

	private int diffRotation = 20;

	private Texture2D paletteLine;

	private int oldIndex;

	private EditedImg currentImg;

	private void Awake()
	{
		LoadStickerSource();
	}

	private void OnEnable()
	{
		GameQuit.Back += BackWorkshop;
		Init();
		InitRawImage();
		InitDiscPanel();
	}

	private void OnDisable()
	{
		GameQuit.Back -= BackWorkshop;
	}

	private Texture2D GetUnstickerText2d(string imageId)
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(imageId + "_unsticker"), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	private void Init()
	{
		oldIndex = 0;
		currentImg = (EditedImg)ApplicationModel.currentEditedImg;
		CurrentImageId = currentImg.imageId;
		mainTexture = GetUnstickerText2d(CurrentImageId);
	}

	private void InitRawImage()
	{
		workPlace.GetComponent<RawImage>().texture = mainTexture;
	}

	private void InitDiscPanel()
	{
		DestoryChild(DiscPanel.transform);
		int count = stickerList.Count;
		for (int i = 0; i < count; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(Stickeritem, DiscPanel.transform, worldPositionStays: false);
			Transform transform = gameObject.transform;
			gameObject.GetComponent<RectTransform>().Rotate(Vector3.forward, -i * diffRotation);
			Transform child = transform.GetChild(0).GetChild(0);
			child.GetComponent<RawImage>().texture = Resources.Load<Texture2D>(IMAGE_PATH + stickerList[i].icon);
			UnityEngine.Debug.Log("path is:" + IMAGE_PATH + stickerList[i].shaderModel);
			transform.GetChild(1).GetComponent<Text>().text = stickerList[i].title;
			transform.GetComponent<Button>().onClick.RemoveAllListeners();
			int index = i;
			transform.GetComponent<Button>().onClick.AddListener(delegate
			{
				ClickStickerButton(index);
			});
			if (i == 0)
			{
				gameObject.transform.GetChild(3).gameObject.SetActive(value: true);
			}
		}
		ClickStickerButton(0);
	}

	private void ClickStickerButton(int index)
	{
		UnityEngine.Debug.Log("ClickStickerButton..." + index);
		DiscPanel.transform.GetChild(oldIndex).GetChild(3).gameObject.SetActive(value: false);
		DiscPanel.transform.GetChild(index).GetChild(3).gameObject.SetActive(value: true);
		UnityEngine.Debug.Log("o index si show:" + DiscPanel.transform.GetChild(index).GetChild(3).gameObject.activeSelf);
		oldIndex = index;
		if (stickerList[index].shaderModel != "none")
		{
			workPlace.GetComponent<RawImage>().material = GetMaterial(stickerList[index]);
			workPlace.GetComponent<RawImage>().texture = null;
		}
		else
		{
			workPlace.GetComponent<RawImage>().texture = mainTexture;
			workPlace.GetComponent<RawImage>().material = null;
		}
		Rotate(-index * diffRotation);
	}

	private Material GetMaterial(Sticker sticker)
	{
		string text = "Custom/sticker/" + sticker.shaderModel;
		UnityEngine.Debug.Log("shader:" + text);
		Material material = new Material(Shader.Find(text));
		material.SetTexture("_MainTex", mainTexture);
		material.SetTexture("_StickerTex", Resources.Load<Texture2D>(ICON_PATH + sticker.icon));
		return material;
	}

	private void LoadStickerSource()
	{
		TextAsset textAsset = (!(ApplicationModel.LANGUAGE == "zh-cn")) ? ((TextAsset)Resources.Load("json/sticker_en")) : ((TextAsset)Resources.Load("json/sticker_zh-cn"));
		stickerList = JsonConvert.DeserializeObject<List<Sticker>>(textAsset.text);
	}

	public void BackWorkshop()
	{
		StickerCanvas.SetActive(value: false);
		WorkShopCanvas.SetActive(value: true);
		DragEventProxy.Instance.SendEnterWorkShopCanvasMsg();
		StoreEditedImgInfo();
	}

	public void GotoShare()
	{
		TotalGA.Event("texture_model_name", stickerList[oldIndex].title);
		if (stickerList[oldIndex].shaderModel != "none")
		{
			currentImg.SetEdited();
		}
		SaveTexture();
		StickerCanvas.SetActive(value: false);
		ShareCanvas.SetActive(value: true);
		UploadFile();
		StoreEditedImgInfo();
		RestorePlayfebManagement.instance.StoreEditedList();
	}

	private void StoreEditedImgInfo()
	{
		if (currentImg.hasEdited)
		{
			if (ApplicationModel.userInfo != null)
			{
				FileUploadEventProxy.Instance.SendUploadUserEditedFileMsg(currentImg);
			}
			PublicImgaeApi.SetLatesEditedImgFirst(currentImg);
			DragEventProxy.Instance.SendChangeIndexImageMsg(currentImg.imageId);
		}
	}

	private void UploadFile()
	{
		if (ApplicationModel.userInfo == null && currentImg.hasEdited)
		{
			FileUploadEventProxy.Instance.SendUploadFileMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
		}
	}

	private Texture2D GetTex2dByPath(string path)
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(path), markNonReadable: false);
		texture2D.Apply();
		texture2D.filterMode = FilterMode.Point;
		texture2D.wrapMode = TextureWrapMode.Clamp;
		return texture2D;
	}

	private void SaveTexture()
	{
		Texture2D tex;
		if (stickerList[oldIndex].shaderModel != "none")
		{
			RenderTexture renderTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
			Graphics.Blit(mainTexture, renderTexture, GetMaterial(stickerList[oldIndex]));
			RenderTexture active = RenderTexture.active;
			Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
			RenderTexture.active = renderTexture;
			texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
			texture2D.Apply();
			tex = texture2D;
			RenderTexture.active = active;
			renderTexture.Release();
		}
		else
		{
			tex = mainTexture;
		}
		try
		{
			PublicToolController.FileCreatorBytes(tex.EncodeToPNG(), CurrentImageId + "_ok", isStoreChange: true);
		}
		catch (IOException message)
		{
			MonoBehaviour.print(message);
		}
	}

	private void Rotate(float posZ)
	{
		LeanTween.rotate(DiscPanel, new Vector3(0f, 0f, 0f - posZ), 0.25f).setEase(LeanTweenType.easeInQuad).setOnComplete((Action)delegate
		{
			UnityEngine.Debug.Log("lala ");
		});
	}

	private void DestoryChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform2.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
