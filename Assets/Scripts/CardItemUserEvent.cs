using UnityEngine;

public class CardItemUserEvent : MonoBehaviour
{
	public string curUid;

	private void OnEnable()
	{
		DragEventProxy.OnAddFollow += addFollow;
		DragEventProxy.OnRemoveFollow += removeFollow;
	}

	private void OnDisable()
	{
		DragEventProxy.OnAddFollow -= addFollow;
		DragEventProxy.OnRemoveFollow -= removeFollow;
	}

	public void addFollow(string uid)
	{
		UnityEngine.Debug.Log("addFollow");
		if (uid == curUid)
		{
			base.transform.Find("TopPanel").Find("follow").GetChild(0)
				.gameObject.SetActive(value: false);
				base.transform.Find("TopPanel").Find("follow").GetChild(1)
					.gameObject.SetActive(value: true);
				}
			}

			public void removeFollow(string uid)
			{
				UnityEngine.Debug.Log("removeFollow");
				if (uid == curUid)
				{
					base.transform.Find("TopPanel").Find("follow").GetChild(1)
						.gameObject.SetActive(value: false);
						base.transform.Find("TopPanel").Find("follow").GetChild(0)
							.gameObject.SetActive(value: true);
						}
					}
				}
