using UnityEngine;
using UnityEngine.UI;

public class NickNameController : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		string text;
		if (ApplicationModel.userInfo == null || ApplicationModel.userInfo.Nickname == null)
		{
			text = ((!(ApplicationModel.LANGUAGE == "zh-cn")) ? "Click to login!" : "点击登录");
		}
		else
		{
			text = ApplicationModel.userInfo.Nickname;
			TextEmojiConvert.Instance.ConvertEmoji(base.transform, text, isBig: true);
		}
		GetComponent<Text>().text = text;
	}
}
