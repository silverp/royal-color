using Newtonsoft.Json;

public class EditedImg : BaseEditedImg
{
	public string ouline;

	public string index;

	public EditedImg(string imageId, string outline, string index, bool hasEdited, bool isRemote)
	{
		base.imageId = imageId;
		ouline = outline;
		this.index = index;
		base.hasEdited = hasEdited;
		base.isRemote = isRemote;
	}

	public EditedImg(string imageId, bool isRemote)
	{
		base.imageId = imageId;
		base.isRemote = isRemote;
	}

	[JsonConstructor]
	public EditedImg(string imageId, string workId, string finalImg, string thumb, int likeCnt, bool isLike, long create, bool hasEdited)
	{
		base.imageId = imageId;
		base.workId = workId;
		base.finalImg = finalImg;
		base.thumb = thumb;
		base.likeCnt = likeCnt;
		base.isLike = isLike;
		base.create = create;
		base.hasEdited = hasEdited;
	}
}
