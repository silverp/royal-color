using UnityEngine;

public class PurchaseDEtailEvent : MonoBehaviour
{
	private void OnEnable()
	{
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			base.transform.Find("Panel_zh-cn").gameObject.SetActive(value: true);
			base.transform.Find("Panel_en").gameObject.SetActive(value: false);
		}
		else
		{
			base.transform.Find("Panel_zh-cn").gameObject.SetActive(value: false);
			base.transform.Find("Panel_en").gameObject.SetActive(value: true);
		}
	}

	public void Close()
	{
		base.gameObject.SetActive(value: false);
	}

	public void GotoServiceItem()
	{
		Application.OpenURL("https://gameapi.orange-social.com/games/ecolor/legal/tos");
	}

	public void GotoPrivacyPoliy()
	{
		Application.OpenURL("https://gameapi.orange-social.com/games/ecolor/legal/privacy");
	}
}
