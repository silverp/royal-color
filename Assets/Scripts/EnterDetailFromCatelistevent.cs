using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class EnterDetailFromCatelistevent : MonoBehaviour
{
	private bool isNetError;

	private GameObject CurrentSpin;

	[SerializeField]
	private GameObject CateDetailCanvas;

	[SerializeField]
	private GameObject WorkShopCanvas;

	[SerializeField]
	private GameObject BeanNoteBox;

	public static EnterDetailFromCatelistevent instance;

	private GUIStyle guiStyle = new GUIStyle();

	private int _index;

	private void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(3f);
		isNetError = false;
	}

	private void OnGUI()
	{
		if (isNetError)
		{
			guiStyle.fontSize = 34;
			guiStyle.normal.textColor = Color.black;
			guiStyle.alignment = TextAnchor.MiddleCenter;
			GUI.color = Color.black;
			GUI.Label(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 300, 100f, 80f), "网络异常！", guiStyle);
			StartCoroutine(showSavedInfo());
		}
	}

	private void UnlockOutlineCallback(ResourceImg img, Errcode err, int amount, GameObject lockBtn)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("unlock Success!!!");
			lockBtn.SetActive(value: false);
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 0.6f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				GoToWorkShop(img);
			});
		}
		else
		{
			DragEventProxy.Instance.SendUnclockOutlineMsg(img, null, ImageType.normal);
		}
	}

	public void GotoWorkShopEvent(ResourceImg img, GameObject lockBtn)
	{
		if (lockBtn.activeSelf)
		{
			if (ApplicationModel.userInfo != null)
			{
				UnityEngine.Debug.Log("need to unlock!!!!");
				UnlockArtManager.Instance.UnlockArt(img, delegate(Errcode err, int amount)
				{
					UnlockOutlineCallback(img, err, amount, lockBtn);
				});
			}
			else
			{
				DragEventProxy.Instance.SendUnclockOutlineMsg(img, null, ImageType.normal);
			}
		}
		else
		{
			GoToWorkShop(img);
		}
	}

	public void GoToWorkShop(ResourceImg img)
	{
		ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedImgFromEditedList(img.imageId, isCreate: true);
		ApplicationModel.lastVisibleScreen = "CateDetailCanvas";
		CateDetailCanvas.SetActive(value: false);
		WorkShopCanvas.gameObject.SetActive(value: true);
		FileUploadEventProxy.Instance.SendClickImageMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
	}

	private CateResource GetCateResourceById(int cid)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return cateResource;
			}
		}
		return null;
	}

	private CateResource GetCateResourceByIndex(int num)
	{
		int num2 = 0;
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (num2 == num)
			{
				return cateResource;
			}
			num2++;
		}
		return null;
	}
}
