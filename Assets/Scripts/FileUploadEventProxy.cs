using System;
using UnityEngine;

public class FileUploadEventProxy
{
	public delegate void ClickImage(BaseEditedImg img, string uuid);

	public delegate void UploadFile(BaseEditedImg img, string uuid);

	public delegate void UploadUserEditedFile(BaseEditedImg img);

	public delegate void UploadLocalUserFile();

	public delegate void LikeEvent(string uid, string workId, bool islike);

	public delegate void LikeMyworkEvent(BaseEditedImg img, bool islike);

	public delegate void PublishMyworkEvent(EditedImg img, Action<Errcode> callback);

	public delegate void InviteFriend(Texture2D headImg, string nickname, Action<Errcode> callback);

	public delegate void ShareWorkEvent(Texture2D texture, string name, int count, bool isHasinfo, Action<Errcode> callback);

	public delegate void ShareMyworkEvent(ColoredImage img);

	protected static FileUploadEventProxy _instance;

	public static FileUploadEventProxy Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new FileUploadEventProxy();
			}
			return _instance;
		}
	}

	public static event ClickImage OnClickImage;

	public static event UploadFile OnUploadFile;

	public static event UploadUserEditedFile OnUploadUserEditedFile;

	public static event UploadLocalUserFile OnUploadLocalUserFile;

	public static event LikeEvent OnLikeEvent;

	public static event LikeMyworkEvent OnLikeMyworkEvent;

	public static event PublishMyworkEvent OnPublishMyworkEvent;

	public static event InviteFriend OnInviteFriend;

	public static event ShareWorkEvent OnShareWorkEvent;

	public static event ShareMyworkEvent OnShareMyworkEvent;

	public void SendInviteFriendMsg(Texture2D headImg, string nickname, Action<Errcode> callback)
	{
		if (FileUploadEventProxy.OnInviteFriend != null)
		{
			FileUploadEventProxy.OnInviteFriend(headImg, nickname, callback);
		}
	}

	public void SendPublishMyworkEventMsg(EditedImg img, Action<Errcode> callback)
	{
		if (FileUploadEventProxy.OnPublishMyworkEvent != null)
		{
			FileUploadEventProxy.OnPublishMyworkEvent(img, callback);
		}
	}

	public void SendShareMyworkEventMsg(ColoredImage img)
	{
		if (FileUploadEventProxy.OnShareMyworkEvent != null)
		{
			FileUploadEventProxy.OnShareMyworkEvent(img);
		}
	}

	public void SendShareWorkEventMsg(Texture2D texture, string name, int count, bool isHasinfo, Action<Errcode> callback)
	{
		if (FileUploadEventProxy.OnShareWorkEvent != null)
		{
			FileUploadEventProxy.OnShareWorkEvent(texture, name, count, isHasinfo, callback);
		}
	}

	public void SendLikeEventMsg(string uid, string workId, bool islike)
	{
		if (FileUploadEventProxy.OnLikeEvent != null)
		{
			FileUploadEventProxy.OnLikeEvent(uid, workId, islike);
		}
	}

	public void SendLikeMyworkEventMsg(BaseEditedImg img, bool islike)
	{
		if (FileUploadEventProxy.OnLikeMyworkEvent != null)
		{
			FileUploadEventProxy.OnLikeMyworkEvent(img, islike);
		}
	}

	public void SendClickImageMsg(BaseEditedImg img, string uuid)
	{
		if (FileUploadEventProxy.OnClickImage != null)
		{
			FileUploadEventProxy.OnClickImage(img, uuid);
		}
	}

	public void SendUploadFileMsg(BaseEditedImg img, string uuid)
	{
		if (FileUploadEventProxy.OnUploadFile != null)
		{
			FileUploadEventProxy.OnUploadFile(img, uuid);
		}
	}

	public void SendUploadUserEditedFileMsg(BaseEditedImg img)
	{
		if (FileUploadEventProxy.OnUploadUserEditedFile != null)
		{
			FileUploadEventProxy.OnUploadUserEditedFile(img);
		}
	}
}
