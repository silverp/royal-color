using HedgehogTeam.EasyTouch;
using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

public class GestureTrigger : ObservableTriggerBase
{
	public enum ActionType
	{
		NONE,
		DRAW,
		DRAW_END,
		MOVE,
		MOVE_END,
		SCALE,
		SCALE_END
	}

	public class GestureResponse
	{
		public ActionType action;

		public float scale;

		public Vector2 postion;

		public Vector2 delta;

		public bool singleOrNot;
	}

	private float longTapThread = 0.2f;

	private float startTouchTime;

	private bool tapMove;

	private GestureResponse currentGesture;

	private Touch[] touches;

	private Subject<GestureResponse> onGesture;

	private float? raiseTime;

	private ActionType lastAction;

	private void Start()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("OnEnable!");
		EasyTouch.SetEnableTwist(enable: false);
		EasyTouch.SetEnablePinch(enable: true);
		EasyTouch.On_Pinch += OnPinch;
		EasyTouch.On_PinchEnd += OnPinchEnd;
		EasyTouch.On_SimpleTap += OnTap;
		EasyTouch.On_Swipe += OnSwipe;
		EasyTouch.On_SwipeEnd += OnSwipeEnd;
		EasyTouch.On_TouchStart += OnTouchStart;
		EasyTouch.On_TouchUp += OnTouchUp;
		EasyTouch.On_SwipeStart += OnSwipeStart;
		EasyTouch.On_Swipe2Fingers += OnSwipeStart2Fingers;
		EasyTouch.On_LongTapStart += OnLongTapStart;
	}

	private void Update()
	{
	}

	private void OnDisable()
	{
		UnsubscribeEvent();
	}

	private void UnsubscribeEvent()
	{
		UnityEngine.Debug.Log("UnsubscribeEvent!");
		EasyTouch.SetEnableTwist(enable: true);
		EasyTouch.On_Pinch -= OnPinch;
		EasyTouch.On_PinchEnd -= OnPinchEnd;
		EasyTouch.On_SimpleTap -= OnTap;
		EasyTouch.On_Swipe -= OnSwipe;
		EasyTouch.On_SwipeEnd -= OnSwipeEnd;
		EasyTouch.On_TouchStart -= OnTouchStart;
		EasyTouch.On_TouchUp -= OnTouchUp;
		EasyTouch.On_SwipeStart -= OnSwipeStart;
		EasyTouch.On_Swipe2Fingers -= OnSwipeStart2Fingers;
		EasyTouch.On_LongTapStart -= OnLongTapStart;
	}

	public void OnTouchStart(Gesture gesture)
	{
		UnityEngine.Debug.Log("OnTouchStart...");
		UnityEngine.Debug.Log("Gesture1Trigger.OnTouchStart.time = " + Time.realtimeSinceStartup);
		if (gesture.fingerIndex == 0)
		{
			startTouchTime = Time.realtimeSinceStartup;
		}
	}

	public void OnTouchUp(Gesture gesture)
	{
		if (hitme(gesture))
		{
			startTouchTime = 0f;
			tapMove = false;
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.DRAW_END;
			gestureResponse.postion = gesture.position;
			gestureResponse.singleOrNot = false;
			syncAction(gestureResponse);
		}
	}

	public void OnSwipeStart(Gesture gesture)
	{
		UnityEngine.Debug.Log("Gesture1Trigger.OnSwipeStart.time = " + Time.realtimeSinceStartup);
		if (!hitme(gesture))
		{
			return;
		}
		tapMove = ((double)(Time.realtimeSinceStartup - startTouchTime) < 0.2);
		GestureResponse gestureResponse = new GestureResponse();
		if (gesture.touchCount == 1)
		{
			gestureResponse.action = ((!tapMove) ? ActionType.DRAW : ActionType.MOVE);
		}
		else
		{
			if (gesture.fingerIndex != 0)
			{
				return;
			}
			gestureResponse.action = ActionType.MOVE;
		}
		gestureResponse.delta = gesture.swipeVector;
		gestureResponse.postion = gesture.position;
		syncAction(gestureResponse);
	}

	public void OnPinchEnd(Gesture gesture)
	{
		if (hitme(gesture))
		{
			UnityEngine.Debug.Log("OnPinchEnd!");
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.SCALE_END;
			gestureResponse.postion = gesture.deltaPosition;
			gestureResponse.scale = gesture.deltaPinch / (gesture.twoFingerDistance + gesture.deltaPinch);
			syncAction(gestureResponse);
		}
	}

	protected bool hitme(Gesture gesture)
	{
		if (gesture.pickedUIElement == null)
		{
			return false;
		}
		UnityEngine.Debug.Log("hitme.transform.gameObject = " + base.transform.gameObject);
		if (gesture.pickedUIElement == base.transform.gameObject && LegalMousePosition())
		{
			return true;
		}
		if (gesture.pickedUIElement.transform.IsChildOf(base.transform) && LegalMousePosition())
		{
			return true;
		}
		return false;
	}

	protected bool LegalMousePosition()
	{
		float scaleRatio = base.transform.GetComponentInParent<CanvasScreenAutoFix>().scaleRatio;
		object[] obj = new object[4]
		{
			"GestureTrigger.LegalMousePosition.mousePosition + Screen.height = ",
			null,
			null,
			null
		};
		Vector3 mousePosition = UnityEngine.Input.mousePosition;
		obj[1] = mousePosition.y;
		obj[2] = "  ";
		obj[3] = Screen.height;
		UnityEngine.Debug.Log(string.Concat(obj));
		Vector3 mousePosition2 = UnityEngine.Input.mousePosition;
		if (mousePosition2.y > 520f * scaleRatio)
		{
			Vector3 mousePosition3 = UnityEngine.Input.mousePosition;
			if (mousePosition3.y < (float)Screen.height - 194f * scaleRatio)
			{
				return true;
			}
		}
		return false;
	}

	public void OnPinch(Gesture gesture)
	{
		if (hitme(gesture))
		{
			UnityEngine.Debug.Log("OnPinch!");
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.SCALE;
			gestureResponse.postion = gesture.position;
			gestureResponse.scale = gesture.deltaPinch / (gesture.twoFingerDistance + gesture.deltaPinch);
			gestureResponse.delta = gesture.position * gestureResponse.scale;
			UnityEngine.Debug.Log("deltaPinch:" + gesture.deltaPinch + " twoFingerDistance:" + gesture.twoFingerDistance + " position:" + gesture.position + " startPosition:" + gesture.startPosition + " deltaPosition:" + gesture.deltaPosition);
			syncAction(gestureResponse);
		}
	}

	private void OnTap(Gesture gesture)
	{
		if (hitme(gesture) && gesture.touchCount == 1)
		{
			UnityEngine.Debug.Log("OnTap!");
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.DRAW;
			gestureResponse.postion = gesture.position;
			gestureResponse.delta = gesture.deltaPosition * gesture.deltaTime;
			syncAction(gestureResponse);
		}
	}

	private void OnLongTapStart(Gesture gesture)
	{
		if (hitme(gesture) && gesture.touchCount == 1)
		{
			UnityEngine.Debug.Log("OnLongTapStart!");
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.DRAW;
			gestureResponse.postion = gesture.position;
			gestureResponse.singleOrNot = true;
			syncAction(gestureResponse);
		}
	}

	private void OnSwipe(Gesture gesture)
	{
		UnityEngine.Debug.Log("GestureTrigger.OnSwipe...");
		if (hitme(gesture))
		{
			GestureResponse gestureResponse = new GestureResponse();
			if (gesture.touchCount == 1)
			{
				gestureResponse.action = ((!tapMove) ? ActionType.DRAW : ActionType.MOVE);
			}
			gestureResponse.delta = gesture.deltaPosition;
			gestureResponse.postion = gesture.position;
			syncAction(gestureResponse);
		}
		else
		{
			GestureResponse gestureResponse2 = new GestureResponse();
			gestureResponse2.action = ActionType.DRAW_END;
			gestureResponse2.postion = gesture.position;
			gestureResponse2.singleOrNot = false;
			syncAction(gestureResponse2);
		}
	}

	private void OnSwipeStart2Fingers(Gesture gesture)
	{
		if (hitme(gesture))
		{
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.MOVE;
			gestureResponse.delta = gesture.deltaPosition;
			gestureResponse.postion = gesture.position;
			syncAction(gestureResponse);
		}
	}

	private void OnSwipeEnd(Gesture gesture)
	{
		if (hitme(gesture))
		{
			UnityEngine.Debug.Log("OnSwipeEnd:touchCount:" + gesture.touchCount);
			GestureResponse gestureResponse = new GestureResponse();
			gestureResponse.action = ActionType.MOVE_END;
			gestureResponse.delta = gesture.deltaPosition;
			syncAction(gestureResponse);
		}
	}

	protected void syncAction(GestureResponse action)
	{
		if (onGesture == null || action == null)
		{
			return;
		}
		UnityEngine.Debug.Log("syncAction:" + action.action);
		ActionType action2 = action.action;
		if (action2 != ActionType.MOVE_END && action2 == ActionType.DRAW && (lastAction == ActionType.MOVE_END || lastAction == ActionType.MOVE))
		{
			float? num = raiseTime;
			float? num2 = (!num.HasValue) ? null : new float?(Time.realtimeSinceStartup - num.GetValueOrDefault());
			if (num2.HasValue && num2.GetValueOrDefault() < 0.2f)
			{
				UnityEngine.Debug.Log("skip draw!");
				return;
			}
		}
		raiseTime = Time.realtimeSinceStartup;
		lastAction = action.action;
		onGesture.OnNext(action);
	}

	public IObservable<GestureResponse> OnGestureAsObservable()
	{
		return onGesture ?? (onGesture = new Subject<GestureResponse>());
	}

	protected override void RaiseOnCompletedOnDestroy()
	{
		if (onGesture != null)
		{
			onGesture.OnCompleted();
		}
	}
}
