using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class PixelCatepageScrollRectAdapter : MonoBehaviour
{
	[Serializable]
	public class CatePageParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class DoubleImageModel
	{
		public PixelArt leftImg;

		public PixelArt rightImg;

		public int cidNum;

		public DoubleImageModel(PixelArt leftImg, PixelArt rightImg, int index)
		{
			if (leftImg != null)
			{
				this.leftImg = leftImg;
			}
			else
			{
				this.leftImg = null;
			}
			if (rightImg != null)
			{
				this.rightImg = rightImg;
			}
			else
			{
				this.rightImg = null;
			}
			cidNum = index;
		}
	}

	public class CatePageItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class CatePageScrollRectItemsAdapter : ScrollRectItemsAdapter8<CatePageParams, CatePageItemsViewHolder>
	{
		private Texture2D loadingTex = Resources.Load<Texture2D>("Images/Image/small_prefab");

		private RectTransform root;

		private CatePageItemsViewHolder catePageItemsViewHolder;

		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<DoubleImageModel> _Data;

		private readonly string index;

		public CatePageScrollRectItemsAdapter(List<DoubleImageModel> data, CatePageParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		public void setLeftImage(PixelArt img, int index)
		{
			setImageByBtnName("BtnLeft", img, index);
			judgeIsLoadNext(index);
			countDropdownEvent(index);
		}

		public void setRightImage(PixelArt img, int index)
		{
			setImageByBtnName("BtnRight", img, index);
		}

		private void countDropdownEvent(int num)
		{
			if (num % 6 == 0)
			{
				TotalGA.Event("cate_drop", "分类:像素画下拉" + (num / 6).ToString() + "页");
			}
		}

		public void judgeIsLoadNext(int num)
		{
			List<PixelArt> pixelArtList = ApplicationModel.pixelArtList;
			if (pixelArtList != null && num == pixelArtList.Count - 8)
			{
				DragEventProxy.Instance.SendRequestNextPageMsg();
			}
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageId, int i)
		{
			//string b = (i != 0) ? _Data[itemIndex].rightImg.imageId : _Data[itemIndex].leftImg.imageId;
			return _Data.Count > itemIndex;// && itemIdexAtRequest == itemIndex && imageId == b;
		}

		private void setImageByBtnName(string name, PixelArt img, int num)
		{
			if (img != null)
			{
				PixleEditedImg editedPixelImgFromEditedList = PublicImgaeApi.GetEditedPixelImgFromEditedList(img.imageId);
				bool locked = UnlockArtManager.Instance.IsShowLock(img);
				UnityEngine.Debug.Log("is show lock is:" + locked);
				if (!root.Find(name).gameObject.activeSelf)
				{
					root.Find(name).gameObject.SetActive(value: true);
				}
				if (editedPixelImgFromEditedList != null && !editedPixelImgFromEditedList.isRemote && editedPixelImgFromEditedList.HasEdited())
				{
					Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
					texture2D.LoadImage(PublicToolController.FileReaderBytes(editedPixelImgFromEditedList.imageId + "_ok"), markNonReadable: false);
					texture2D.Apply();
					Transform imgTransform = root.Find(name);
					string imageId = img.imageId;
					callback(texture2D, name, img, imgTransform, locked, imageId);
				}
				else
				{
					root.Find(name).GetChild(1).GetChild(0)
						.GetComponent<RawImage>()
						.texture = loadingTex;
					root.Find(name).GetComponent<RawImageDataController>().downloadStatus = 0;
					root.Find(name).GetComponent<Button>().onClick.RemoveAllListeners();
					string empty = string.Empty;
					empty = ((editedPixelImgFromEditedList == null || !editedPixelImgFromEditedList.isRemote) ? img.baseUrl : editedPixelImgFromEditedList.thumb);
					Transform imgTRansform = root.Find(name);
					string _imageId = img.imageId;
					NetLoadcontroller.Instance.RequestImg(empty, delegate (Texture2D tex)
					{
						callback(tex, name, img, imgTRansform, locked, _imageId);
					});
				}
				LockControl(locked, root.Find(name).GetChild(2).gameObject);
			}
			else
			{
				root.Find(name).gameObject.SetActive(value: false);
			}
		}

		public void callback(Texture2D tex, string btnName, PixelArt img, Transform imgTransform, bool locked, string imageId)
		{
			int itemIndex = catePageItemsViewHolder.itemIndex;
			int i = (!(btnName == "BtnLeft")) ? 1 : 0;
			if (IsModelStillValid(catePageItemsViewHolder.itemIndex, itemIndex, imageId, i))
			{
				RawImage component = imgTransform.GetChild(1).GetChild(0).GetComponent<RawImage>();
				if (component != null)
				{
					component.texture = tex;
				}
				imgTransform.GetChild(1).GetChild(0).GetComponent<ImageChangeController>()
					.imageId = img.imageId;
				imgTransform.GetComponent<Button>().onClick.RemoveAllListeners();
				bool flag = locked;
				imgTransform.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnImageHit(img, imgTransform.Find("lock").gameObject);
				});
			}
		}

		private void LockControl(bool locked, GameObject lockBtn)
		{
			if (locked)
			{
				lockBtn.SetActive(value: true);
			}
			else
			{
				lockBtn.SetActive(value: false);
			}
		}

		private void OnImageHit(PixelArt img, GameObject lockbtn)
		{
			EnterWorkshopFromPixelListPage.instance.GotoWorkShopEvent(img, lockbtn);
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		protected override CatePageItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			CatePageItemsViewHolder catePageItemsViewHolder = new CatePageItemsViewHolder();
			catePageItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return catePageItemsViewHolder;
		}

		protected override void UpdateViewsHolder(CatePageItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
			catePageItemsViewHolder = newOrRecycled;
			setLeftImage(doubleImageModel.leftImg, doubleImageModel.cidNum);
			setRightImage(doubleImageModel.rightImg, doubleImageModel.cidNum);
		}
	}

	[SerializeField]
	private CatePageParams _ScrollRectAdapterParams;

	[SerializeField]
	private GameObject PixelArtDetailCanvas;

	private List<DoubleImageModel> _Data;

	private CatePageScrollRectItemsAdapter _ScrollRectItemsAdapter;

	private int step = 20;

	public bool isRefresh;

	public CateResource currentCategory;

	public List<PixelArt> currentPixelArtList;

	public BaseParams Params => _ScrollRectAdapterParams;

	public CatePageScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<DoubleImageModel> Data => _Data;

	private void Awake()
	{
		_Data = new List<DoubleImageModel>();
		DragEventProxy.OnRefreshAfterPurchase += RefreshData;
		DragEventProxy.OnRefreshAfterLogin += RefreshData;
		_ScrollRectItemsAdapter = new CatePageScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void AssginEvents()
	{
		DragEventProxy.OnRequestNextPage += RequestNextPage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnRequestNextPage -= RequestNextPage;
	}

	private void OnEnable()
	{
		AssginEvents();
		RefreshImgList();
	}

	private void RefreshImgList()
	{
		if (isRefresh)
		{
			HideChild(base.transform.GetChild(0).GetChild(0));
			init();
			LoadCateData();
			isRefresh = false;
		}
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void init()
	{
		Transform transform = PixelArtDetailCanvas.transform.Find("wave");
		transform.GetChild(0).gameObject.SetActive(value: true);
		transform.GetChild(1).gameObject.SetActive(value: false);
		transform.GetChild(0).GetChild(0).GetComponent<Text>()
			.text = currentCategory.name;
		LoadTopCover(currentCategory.innerUrl);
	}

	private void LoadTopCover(string url)
	{
		URLRequest request = new URLRequest(url);
		DataBroker.getTexture2D(request, delegate (Texture2D tex)
		{
			base.transform.parent.Find("TopPanel").Find("Image").GetComponent<RawImage>()
				.texture = tex;
		}, delegate (Exception ex)
					{
				UnityEngine.Debug.Log(ex);
			});
	}

	private void LoadCateData()
	{
		if (currentPixelArtList == null || currentPixelArtList.Count == 0)
		{
			PublicControllerApi.GetPixelArtList(1, step, delegate (Errcode err, List<PixelArt> x)
			{
				RequestPixelArtlistCallback(err, x);
			});
		}
		else
		{
			StartCoroutine(initContent(currentPixelArtList));
		}
	}

	private void RequestNextPage()
	{
		int prePage = (int)Mathf.Ceil(currentPixelArtList.Count / step);
		PublicControllerApi.GetPixelArtList(prePage + 1, step, delegate (Errcode err, List<PixelArt> x)
		{
			AddItem(err, x, prePage + 1);
		});
	}

	private void AddItem(Errcode err, List<PixelArt> imglist, int page)
	{
		if (imglist == null || imglist.Count == 0 || currentPixelArtList.Count != (page - 1) * step)
		{
			return;
		}
		int count = currentPixelArtList.Count;
		currentPixelArtList.AddRange(imglist);
		int count2 = imglist.Count;
		for (int i = 0; i < count2; i += 2)
		{
			if (i + 1 < count2)
			{
				_Data.Add(new DoubleImageModel(imglist[i], imglist[i + 1], i + count));
			}
			else
			{
				_Data.Add(new DoubleImageModel(imglist[i], null, i + count));
			}
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
	}

	private void RefreshData()
	{
		isRefresh = true;
	}

	private void RequestPixelArtlistCallback(Errcode err, List<PixelArt> imglist)
	{
		if (err.errorCode == Errcode.OK)
		{
			currentPixelArtList = imglist;
			StartCoroutine(initContent(imglist));
		}
	}

	private IEnumerator initContent(List<PixelArt> list)
	{
		yield return null;
		yield return null;
		int ImgTotal = (list != null) ? list.Count : 0;
		UnityEngine.Debug.Log("ImgTotal is:" + ImgTotal);
		if (ImgTotal <= 0)
		{
			yield break;
		}
		currentPixelArtList = list;
		ApplicationModel.pixelArtList = currentPixelArtList;
		_Data.Clear();
		for (int i = 0; i < ImgTotal; i += 2)
		{
			if (i + 1 < ImgTotal)
			{
				_Data.Add(new DoubleImageModel(list[i], list[i + 1], i));
			}
			else
			{
				_Data.Add(new DoubleImageModel(list[i], null, i));
			}
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		StartCoroutine(unloadAssets());
	}

	private IEnumerator unloadAssets()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		Resources.UnloadUnusedAssets();
	}

	private void OnDestroy()
	{
		DragEventProxy.OnRefreshAfterPurchase -= RefreshData;
		DragEventProxy.OnRefreshAfterLogin -= RefreshData;
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private void HideChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				transform2.gameObject.SetActive(value: false);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
