using Newtonsoft.Json;
using SVGImporter;
using System;
using System.Collections;
using UnityEngine;

public class WorkshopLoader : MonoBehaviour
{
	public class EditedImgInfo
	{
		public string imageId;

		public string palette;

		public string clickpos;
	}

	public class EditedImgInfoJson
	{
		public int status;

		public EditedImgInfo data;

		public string msg;
	}

	private Errcode errcode;

	private bool indexImageIsDownload;

	private bool pdfImageIsdownload;

	private bool PaletteIsDownload;

	private bool ClickposIsdownload;

	private Texture2D texture;

	private byte[] bytes;

	private AssetBundle assetBundle;

	private SVGAsset svgAsset;

	private Action<Errcode, Texture2D, SVGAsset> callback;

	private EditedImg editedImg;

	public static WorkshopLoader Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public IEnumerator StartDownloadEditedAssets(EditedImg editedImg, Action<Errcode, Texture2D, SVGAsset> callback)
	{
		Init();
		errcode = new Errcode();
		this.callback = callback;
		this.editedImg = editedImg;
		RequestEditedInfo();
		RequestImgOriginDetailInfo();
		yield return null;
	}

	private void Init()
	{
		indexImageIsDownload = false;
		pdfImageIsdownload = false;
		PaletteIsDownload = false;
		ClickposIsdownload = false;
	}

	private void RequestEditedInfo()
	{
		if (editedImg.isRemote)
		{
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/image/getEditedInfo?uid=" + ApplicationModel.userInfo.Uid + "&workId=" + editedImg.workId + "&sessionId=" + ApplicationModel.userInfo.SessionId, RequestEditedInfoCallback);
			return;
		}
		PaletteIsDownload = true;
		ClickposIsdownload = true;
	}

	private void RequestEditedInfoCallback(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			EditedImgInfoJson editedImgInfoJson = JsonConvert.DeserializeObject<EditedImgInfoJson>(json);
			if (editedImgInfoJson.msg == "ok")
			{
				errcode.errorCode = 0;
				EditedImgInfo data = editedImgInfoJson.data;
				DownLoadEditedImg(data);
			}
			else if (editedImgInfoJson.status == 10403)
			{
				errcode.errorCode = 2;
				callback(errcode, texture, svgAsset);
			}
			else
			{
				errcode.errorCode = 1;
				callback(errcode, texture, svgAsset);
			}
		}
		else
		{
			errcode.errorCode = 1;
			callback(errcode, texture, svgAsset);
		}
	}

	private void DownLoadEditedImg(EditedImgInfo editedImgInfo)
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		NetLoadcontroller.Instance.RequestBytes(editedImgInfo.palette, delegate(byte[] bytes)
		{
			CopyPaletteTolocal(bytes, editedImgInfo.imageId);
			PaletteIsDownload = true;
			TestIsDownAll();
		});
		NetLoadcontroller.Instance.RequestBytes(editedImgInfo.clickpos, delegate(byte[] bytes)
		{
			CopyClickposTolocal(bytes, editedImgInfo.imageId);
			ClickposIsdownload = true;
			TestIsDownAll();
		});
	}

	private void RequestImgOriginDetailInfo()
	{
		if (editedImg.ouline == null || editedImg.ouline == string.Empty || editedImg.index == null || editedImg.index == string.Empty)
		{
			URLRequest uRLRequest = APIFactory.create(APIFactory.ORIGIN_DETAIL);
			uRLRequest.addParam("imageId", editedImg.imageId);
			DataBroker.getJson(uRLRequest, delegate(OriginDetailJson detail)
			{
				if (detail.msg == "ok")
				{
					editedImg.ouline = detail.data.outline;
					editedImg.index = detail.data.index;
					DownloadOutLineAndIndex(editedImg);
				}
				else
				{
					errcode.errorCode = 1;
					callback(errcode, texture, svgAsset);
				}
			}, delegate
			{
				errcode.errorCode = 1;
				callback(errcode, texture, svgAsset);
			});
		}
		else
		{
			DownloadOutLineAndIndex(editedImg);
		}
	}

	private void DownloadOutLineAndIndex(EditedImg currentImg)
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		NetLoadcontroller.Instance.RequestImg(currentImg.index, delegate(Texture2D texture)
		{
			this.texture = texture;
			indexImageIsDownload = true;
			TestIsDownAll();
		});
		StartCoroutine(StartDownloadOutline(currentImg));
	}

	private IEnumerator StartDownloadOutline(EditedImg currentImg)
	{
		float outline_startTime = Time.realtimeSinceStartup;
		if (JudgeIsLoadCorrectAssetBundle(currentImg.ouline))
		{
			using (var asset = WWW.LoadFromCacheOrDownload(currentImg.ouline, 1))
			{
				try
				{
					yield return asset;
					float num = Time.realtimeSinceStartup - outline_startTime;
					float start_time = Time.realtimeSinceStartup;
					AssetBundle bundle = asset.assetBundle;
					string bundleName = currentImg.imageId;
					SVGAsset svg = svgAsset = (bundle.LoadAsset(bundleName, typeof(SVGAsset)) as SVGAsset);
					bundle.Unload(unloadAllLoadedObjects: false);
					float num2 = Time.realtimeSinceStartup - start_time;
					pdfImageIsdownload = true;
					TestIsDownAll();
					yield return null;
				}
				finally
				{
				}
			}
		}
		else
		{
			errcode.errorCode = 1;
			callback(errcode, texture, svgAsset);
			TotalGA.Event("load_assetbundle_error", PublicToolController.GetPlatformName());
		}
		yield return null;
	}

	private bool JudgeIsLoadCorrectAssetBundle(string url)
	{
		bool flag = false;
		return url.EndsWith("_android.outline");
	}

	private void TestIsDownAll()
	{
		if (indexImageIsDownload && pdfImageIsdownload && PaletteIsDownload && ClickposIsdownload)
		{
			callback(errcode, texture, svgAsset);
		}
	}

	private void CopyPaletteTolocal(byte[] bytes, string imageId)
	{
		PublicToolController.FileCreatorBytes(bytes, imageId + "_ed", isStoreChange: true);
	}

	private void CopyClickposTolocal(byte[] bytes, string imageId)
	{
		PublicToolController.FileCreatorBytes(bytes, imageId + "_pos", isStoreChange: true);
	}
}
