using UnityEngine;
using UnityEngine.UI;

public class SlideController : MonoBehaviour
{
	public Scrollbar m_Scrollbar;

	public ScrollRect m_ScrollRect;

	private float mTargetValue;

	private bool mNeedMove;

	private const float MOVE_SPEED = 1f;

	private const float SMOOTH_TIME = 0.2f;

	private float mMoveSpeed;

	public void OnPointerDown()
	{
		mNeedMove = false;
	}

	public void OnPointerUp()
	{
		if (m_Scrollbar.value <= 0.125f)
		{
			mTargetValue = 0f;
		}
		else if (m_Scrollbar.value <= 0.375f)
		{
			mTargetValue = 0.25f;
		}
		else if (m_Scrollbar.value <= 0.625f)
		{
			mTargetValue = 0.5f;
		}
		else if (m_Scrollbar.value <= 0.875f)
		{
			mTargetValue = 0.75f;
		}
		else
		{
			mTargetValue = 1f;
		}
		mNeedMove = true;
		mMoveSpeed = 0f;
	}

	public void OnButtonClick(int value)
	{
		switch (value)
		{
		case 1:
			mTargetValue = 0f;
			break;
		case 2:
			mTargetValue = 0.25f;
			break;
		case 3:
			mTargetValue = 0.5f;
			break;
		case 4:
			mTargetValue = 0.75f;
			break;
		case 5:
			mTargetValue = 1f;
			break;
		default:
			UnityEngine.Debug.LogError("!!!!!");
			break;
		}
		mNeedMove = true;
	}

	private void Update()
	{
		if (mNeedMove)
		{
			if (Mathf.Abs(m_Scrollbar.value - mTargetValue) < 0.01f)
			{
				m_Scrollbar.value = mTargetValue;
				mNeedMove = false;
			}
			else
			{
				m_Scrollbar.value = Mathf.SmoothDamp(m_Scrollbar.value, mTargetValue, ref mMoveSpeed, 0.2f);
			}
		}
	}
}
