using System;

public class ColoredImage : IComparable<ColoredImage>
{
	public WorkInfo img;

	public BriefUserInfo user;

	public bool isLike;

	public bool isFollow;

	public long create;

	public int CompareTo(ColoredImage coloredImage)
	{
		if (coloredImage == null)
		{
			return -1;
		}
		return -create.CompareTo(coloredImage.create);
	}
}
