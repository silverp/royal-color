using System;
using System.Collections;
using System.Collections.Generic;
using UI.ThreeDimensional;
using UnityEngine;
using UnityEngine.UI;

public class ColorReferencePanelController : MonoBehaviour
{
	private EditedImg currentImg;

	private string lastImageId;

	[SerializeField]
	private GameObject colorComplexItem;

	public List<ColorComplex> targetData;

	private int ColorCLCount = 5;

	public ColorComplex[] models;

	public static List<ColorComplex> modelsList;

	private List<ReferenceColor> referenceItemList;

	public int pageIndex;

	public List<ReferenceColor> ReferenceItemList
	{
		get
		{
			return referenceItemList;
		}
		set
		{
			referenceItemList = value;
			ChangeReferenceItemToColorComplex();
		}
	}

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void InitContent()
	{
		modelsList = new List<ColorComplex>();
		modelsList.AddRange(models);
		targetData = modelsList;
		Transform transform = base.transform.Find("ColorComplex").transform;
		int num = 0;
		if (targetData != null)
		{
			num = targetData.Count;
		}
		for (int i = 0; i < 9; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(colorComplexItem, transform, worldPositionStays: false);
			int num2 = (i >= 4) ? ((i + 1) % 5) : i;
			int num3 = (i >= 4) ? 1 : 0;
			gameObject.transform.localPosition += Vector3.right * 111f * num2;
			gameObject.transform.localPosition += Vector3.down * 121f * num3;
			if (num3 == 0)
			{
				gameObject.transform.localPosition += Vector3.right * 59f;
			}
			if (targetData != null && num > i)
			{
				ColorComplex colorComplex = targetData[i];
				if (colorComplex.isGradient)
				{
					gameObject.transform.Find("colorGradientBtn").gameObject.SetActive(value: true);
					gameObject.transform.Find("ColorSolidBtn").gameObject.SetActive(value: false);
					gameObject.transform.Find("colData").GetComponent<Image>().color = colorComplex.color;
					gameObject.transform.Find("colorGradientBtn").GetComponent<UIObject3D>().ColorCoord = colorComplex.gradientColorCoordinate;
					gameObject.transform.Find("colorGradientBtn").GetComponent<Button>().onClick.AddListener(delegate
					{
						onClickGradient(colorComplex.color, isGradient: true, colorComplex.gradientColorCoordinate);
					});
					UnityEngine.Debug.Log("InitContent.colorComplex.if.color... = " + colorComplex.color);
				}
				else
				{
					gameObject.transform.Find("colorGradientBtn").gameObject.SetActive(value: false);
					gameObject.transform.Find("ColorSolidBtn").gameObject.SetActive(value: true);
					gameObject.transform.Find("ColorSolidBtn").GetComponent<Image>().color = colorComplex.color;
					gameObject.transform.Find("colData").GetComponent<Image>().color = colorComplex.color;
					gameObject.transform.Find("ColorSolidBtn").GetComponent<Button>().onClick.AddListener(delegate
					{
						onClick(colorComplex.color, isGradient: false);
					});
					UnityEngine.Debug.Log("InitContent.else.colorComplex.color =  " + colorComplex.color);
					UnityEngine.Debug.Log("InitContent.else.colorItemCurrent.transform.Find(ColorSolidBtn).GetComponent<Image>().color =  " + gameObject.transform.Find("ColorSolidBtn").GetComponent<Image>().color);
				}
			}
		}
	}

	private void ChangeReferenceItemToColorComplex()
	{
		UnityEngine.Debug.Log("ColorReferencePanelController.ChangeReferenceItemToColorComplex.pageIndex = " + pageIndex);
		UnityEngine.Debug.Log("ColorReferencePanelController.ChangeReferenceItemToColorComplex.referenceItemList = " + referenceItemList.Count);
		models = new ColorComplex[referenceItemList.Count];
		for (int i = 0; i < 9; i++)
		{
			ColorComplex colorComplex = new ColorComplex(isGradient: false, new Color(0f, 0f, 0f), new Vector2(0f, 0f));
			colorComplex.isGradient = ((referenceItemList[i].a == 1) ? true : false);
			colorComplex.color = new Color((float)referenceItemList[i].r / 255f, (float)referenceItemList[i].g / 255f, (float)referenceItemList[i].b / 255f);
			colorComplex.gradientColorCoordinate = new Vector2(referenceItemList[i].r, referenceItemList[i].g);
			models[i] = colorComplex;
		}
		InitContent();
	}

	private void onClick(Color col, bool isGradient)
	{
		UnityEngine.Debug.Log("onclick color");
		ApplicationModel.IsGradient = isGradient;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(isGradient, col, new Vector2(0f, 0f));
	}

	private void onClickGradient(Color col, bool isGradient, Vector2 colorcoord)
	{
		ApplicationModel.IsGradient = isGradient;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(isGradient, col, colorcoord);
	}

	private IEnumerator DelayInit()
	{
		while (referenceItemList == null)
		{
			yield return null;
		}
		UnityEngine.Debug.Log("referenceItemList.count = " + referenceItemList.Count);
		ChangeReferenceItemToColorComplex();
		InitContent();
	}

	private void AssginEvents()
	{
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnAddColorToRecentList -= RefreshPanel;
	}

	private void RefreshPanel()
	{
	}

	private void DestoryChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform2.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
