using Newtonsoft.Json;
using UnityEngine;

public class ClearDataAfterLogout : MonoBehaviour
{
	protected static ClearDataAfterLogout _instance;

	public static ClearDataAfterLogout Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new ClearDataAfterLogout();
			}
			return _instance;
		}
	}

	public void ClearUserData()
	{
		ApplicationModel.userInfo = null;
		ClearUserDataFromEditedImgList();
		ConvertInspirationInfoToNotLogin();
	}

	private void ConvertInspirationInfoToNotLogin()
	{
		NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/image/getChoiceList", delegate(string json, bool isSuccess)
		{
			MergeColoredImageResource(json, isSuccess);
		});
	}

	private static void MergeColoredImageResource(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			ColoredImageJson coloredImageJson = JsonConvert.DeserializeObject<ColoredImageJson>(json);
			if (coloredImageJson.status == 0)
			{
				ApplicationModel.ColoredImgList = coloredImageJson.data;
			}
		}
	}

	private void ClearUserDataFromEditedImgList()
	{
		for (int num = ApplicationModel.EditedResourceImgList.Count - 1; num >= 0; num--)
		{
			if (ApplicationModel.EditedResourceImgList[num].isRemote)
			{
				ApplicationModel.EditedResourceImgList.RemoveAt(num);
			}
			else
			{
				ApplicationModel.EditedResourceImgList[num].likeCnt = 0;
				ApplicationModel.EditedResourceImgList[num].isLike = false;
				ApplicationModel.EditedResourceImgList[num].workId = string.Empty;
			}
		}
		for (int num2 = ApplicationModel.PixleEditedImgList.Count - 1; num2 >= 0; num2--)
		{
			if (ApplicationModel.PixleEditedImgList[num2].isRemote)
			{
				ApplicationModel.PixleEditedImgList.RemoveAt(num2);
			}
			else
			{
				ApplicationModel.PixleEditedImgList[num2].likeCnt = 0;
				ApplicationModel.PixleEditedImgList[num2].isLike = false;
				ApplicationModel.PixleEditedImgList[num2].workId = string.Empty;
			}
		}
	}
}
