using UnityEngine;

public class ResizeSprite : MonoBehaviour
{
	private void Start()
	{
		SpriteRenderer component = GetComponent<SpriteRenderer>();
		base.transform.localScale = new Vector3(1f, 1f, 1f);
		component.sprite.texture.filterMode = FilterMode.Bilinear;
	}

	private void Update()
	{
	}
}
