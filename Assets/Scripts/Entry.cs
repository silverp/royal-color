using UnityEngine;

public class Entry : MonoBehaviour
{
	private void OnGUI()
	{
		if (GUI.Button(new Rect(150f, 100f, 500f, 100f), "AnalyticsEntry"))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("AnalyticsEntry");
		}
		if (GUI.Button(new Rect(150f, 300f, 500f, 100f), "Push"))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("PushDemo");
		}
		if (GUI.Button(new Rect(150f, 500f, 500f, 100f), "Social"))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("SocialDemo");
		}
	}
}
