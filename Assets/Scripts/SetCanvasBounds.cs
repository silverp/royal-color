using System;
using UnityEngine;

public class SetCanvasBounds : MonoBehaviour
{
	public enum ChangeModel
	{
		NOMARL,
		CUSTOM
	}

	private enum RectTransformModel
	{
		STRETCH,
		UN_STRETCH
	}

	[Serializable]
	public class RectangleRect
	{
		[Header("set RectangleRect")]
		public float Left;

		public float Top;

		public float Right;

		public float Bottom;
	}

	[Serializable]
	public class WidthAndHeight
	{
		public float Width;

		public float Height;
	}

	[Serializable]
	public class PostionPos
	{
		public float X;

		public float Y;

		public float Z;
	}

	[SerializeField]
	public ChangeModel changemodel;

	public RectangleRect SetRectangleRect;

	public WidthAndHeight SetWidthAndHeight;

	public PostionPos SetPostionPos;

	private void Start()
	{
		ApplySafeArea(Screen.safeArea);
	}

	private void Update()
	{
	}

	private void ApplySafeArea(Rect area)
	{
	}

	private void CustomMaker()
	{
		UnityEngine.Debug.Log("CustomMaker....");
		RectTransform rt = base.transform.GetComponent<RectTransform>(); 
		Vector2 anchoredPosition = rt.anchoredPosition; 
		Vector2 sizeDelta = rt.sizeDelta; 
		Vector2 offsetMin = rt.offsetMin; 
		Vector2 offsetMax = rt.offsetMax; 
		rt.anchoredPosition = new Vector2(anchoredPosition.x + SetPostionPos.X, anchoredPosition.y + SetPostionPos.Y); 
		rt.sizeDelta = new Vector2(sizeDelta.x + SetWidthAndHeight.Width, sizeDelta.y + SetWidthAndHeight.Height); 
		rt.offsetMin = new Vector2(offsetMin.x + SetRectangleRect.Left, offsetMin.y + SetRectangleRect.Bottom); 
		rt.offsetMax = new Vector2(offsetMax.x + SetRectangleRect.Right, offsetMax.y + SetRectangleRect.Top);
	}

	private RectTransformModel judgemolde(RectTransform rect)
	{ 
		if (rect.anchorMax.x == rect.anchorMin.x && rect.anchorMax.y == rect.anchorMin.y)
		{  
			return RectTransformModel.UN_STRETCH; 
		}
		return RectTransformModel.STRETCH;
	}
}
