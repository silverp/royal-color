using UnityEngine;

public class ClickEventManagement : MonoBehaviour
{
	public static bool isAcceptClickEvent;

	public static bool IsClickImageListScene;

	public static bool IsClickCateListScene;

	public static bool IsClickMyWorkScene;

	public static bool IsClickWorkShopScene;

	public static bool IsClickShareScene;

	public static string CanvasNameInCateScene = "cateList";

	public static string ColorThemeName = string.Empty;

	public static int ColorThemeIndex;

	public static bool ClickEventForColorPanel;

	public static int barCount;

	public static int pureIndex;

	public static int themeIndex;
}
