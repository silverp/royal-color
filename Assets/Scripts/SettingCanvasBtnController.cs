using Newtonsoft.Json;
using UnityEngine;

public class SettingCanvasBtnController : MonoBehaviour
{
	[SerializeField]
	private GameObject LogoutPanel;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject SettingCanvas;

	[SerializeField]
	private GameObject FeedbackCanvas;

	[SerializeField]
	private GameObject AboutUsCanvas;

	[SerializeField]
	private GameObject ApplicationArtistCanvas;

	[SerializeField]
	private GameObject applyArtist;

	private void Start()
	{
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			applyArtist.SetActive(value: true);
		}
		else
		{
			applyArtist.SetActive(value: false);
		}
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		JudgeIfLogin();
	}

	public void GotoApplicationArtistCanvas()
	{
		SettingCanvas.SetActive(value: false);
		ApplicationArtistCanvas.SetActive(value: true);
	}

	public void GotoFeedBackPanel()
	{
		SettingCanvas.SetActive(value: false);
		FeedbackCanvas.SetActive(value: true);
	}

	public void GotoAboutUsPanel()
	{
		SettingCanvas.SetActive(value: false);
		AboutUsCanvas.SetActive(value: true);
	}

	public void BackToMyworkCanvas()
	{
		SettingCanvas.SetActive(value: false);
		UIRoot.ShowUI(MyworkCanvas);
	}

	private void JudgeIfLogin()
	{
		if (ApplicationModel.userInfo != null)
		{
			LogoutPanel.SetActive(value: true);
		}
		else
		{
			LogoutPanel.SetActive(value: false);
		}
	}

	public void Logout()
	{
		NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/user/logout?sessionId=" + ApplicationModel.userInfo.SessionId, RequestCallback);
	}

	private void RequestCallback(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			StandardReturnCode standardReturnCode = JsonConvert.DeserializeObject<StandardReturnCode>(json);
			if (standardReturnCode.msg == "ok")
			{
				ClearDataAfterLogout.Instance.ClearUserData();
				SettingCanvas.SetActive(value: false);
				UIRoot.ShowUI(MyworkCanvas);
			}
			else
			{
				LogOutError();
			}
		}
		else
		{
			LogOutError();
		}
	}

	private void LogOutError()
	{
	}
}
