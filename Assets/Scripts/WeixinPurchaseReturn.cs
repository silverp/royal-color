public class WeixinPurchaseReturn
{
	public string sign;

	public string timestamp;

	public string partnerid;

	public string noncestr;

	public string prepayid;

	public string package;

	public string appid;

	public string serialNumber;
}
