using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IndexCardInitController : MonoBehaviour
{
	public delegate void ShowRedPointAction(int cid);

	[SerializeField]
	private GameObject ActivityBanner;

	[SerializeField]
	private GameObject cateScrollPanel;

	[SerializeField]
	private GameObject ImglistCanvas;

	[SerializeField]
	private GameObject CateDetailCanvas;

	[SerializeField]
	private GameObject DailyfreeCanvas;

	[SerializeField]
	private GameObject PixelArtDetailCanvas;

	[SerializeField]
	private GameObject netErrorpanel;

	private float itemHeight = 516f;

	private float startPosY = -24f;

	public static event ShowRedPointAction OnShowRedPointAction;

	private void Start()
	{
		init();
	}

	private void OnEnable()
	{
		RequestCateInfo();
		DragEventProxy.OnRefreshCatelist += RefreshCateList;
	}

	private void OnDisable()
	{
		DragEventProxy.OnRefreshCatelist -= RefreshCateList;
	}

	private void RefreshCateList()
	{
		UnityEngine.Debug.Log("RefreshCateList");
		NetWorkRequest.DownloadCateListInfo(RefreshCateListFinish);
	}

	private void RefreshCateListFinish()
	{
		init();
	}

	public void RequestCateInfo()
	{
		URLRequest request = APIFactory.create(APIFactory.ALL_CATEGORY);
		DataBroker.getJson(request, delegate(AllCateResourceJson detail)
		{
			if (detail.status == 0)
			{
				ReuqestCateInfoCallback(detail.data);
			}
		}, delegate
		{
		});
	}

	private void ReuqestCateInfoCallback(List<CateResource> list)
	{
		foreach (CateResource item in list)
		{
			CateResource cateByCid = getCateByCid(item.cid);
			int indexByCid = GetIndexByCid(item.cid);
			if (indexByCid != -1 && PublicImgaeApi.IsShowRedPoint(ApplicationModel.CateResourceList[indexByCid], item))
			{
				UnityEngine.Debug.Log("IsShowRedPoint...");
				IndexCardInitController.OnShowRedPointAction(item.cid);
				UnityEngine.Debug.Log("old cate time is:" + cateByCid.updateTime);
				ApplicationModel.CateResourceList[indexByCid] = item;
				UnityEngine.Debug.Log("new cate time is:" + cateByCid.updateTime);
				UnityEngine.Debug.Log("new cate is:" + getCateByCid(cateByCid.cid).updateTime);
			}
		}
	}

	private int GetIndexByCid(int cid)
	{
		int num = 0;
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return num;
			}
			num++;
		}
		return -1;
	}

	private CateResource getCateByCid(int cid)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return cateResource;
			}
		}
		return null;
	}

	private void init()
	{
		List<CateResource> cateResourceList = ApplicationModel.CateResourceList;
		UnityEngine.Debug.Log("cateList is:" + JsonConvert.SerializeObject(cateResourceList));
		if (cateResourceList == null || cateResourceList.Count == 0)
		{
			netErrorpanel.SetActive(value: true);
		}
		else
		{
			netErrorpanel.SetActive(value: false);
			int count = cateResourceList.Count;
			UnityEngine.Debug.Log("cnt is:" + count);
			UnityEngine.Debug.Log("Mathf.Ceil((cnt-1) is:" + Math.Ceiling((double)(count - 1) / 2.0));
			float yOffset = startPosY;
			RectTransform component = GetComponent<RectTransform>();
			Vector2 sizeDelta = GetComponent<RectTransform>().sizeDelta;
			component.sizeDelta = new Vector2(sizeDelta.x, (float)(Math.Ceiling((double)count / 2.0 + 1.0) * itemHeight + 100.0));
			//GameObject gameObject = UnityEngine.Object.Instantiate(ActivityBanner, base.transform, worldPositionStays: false);
			//gameObject.GetComponent<Button>().onClick.AddListener(delegate
			//{
			//	GoToDealiyPage();
			//});
			//RectTransform component2 = gameObject.GetComponent<RectTransform>();
			//Vector2 anchoredPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
			//component2.anchoredPosition = new Vector3(anchoredPosition.x, num, 0f);
			//num -= itemHeight + 14;
			Vector2 anchoredPosition2 = cateScrollPanel.GetComponent<RectTransform>().anchoredPosition;
			float x = anchoredPosition2.x;
			for (int i = 0; i < count; i += 2)
			{
				GameObject gameObject2 = UnityEngine.Object.Instantiate(cateScrollPanel, base.transform, worldPositionStays: false);
				Transform leftTransform = gameObject2.transform.Find("BtnLeft");
				var leftParent = leftTransform.GetChild(0);
                leftParent.GetChild(1).GetComponent<Text>().text = cateResourceList[i].name;
				int leftCid = cateResourceList[i].cid;
				leftTransform.GetComponent<CateBtnController>().cid = leftCid;
				leftTransform.GetComponent<Button>().onClick.AddListener(delegate
				{
					GoToCateDetailPageByCid(leftCid, leftParent);
				});
				RawImage component3 = leftParent.GetChild(0).GetChild(0).GetComponent<RawImage>();
				LoadCoverImage(component3, cateResourceList[i].url);
				JudgeIsShowRedPoint(cateResourceList[i], leftParent.GetChild(1).GetChild(0));
				if (i + 1 < count)
				{
					Transform rightTransform = gameObject2.transform.Find("BtnRight");
                    var rightParent = rightTransform.GetChild(0);
                    rightParent.GetChild(1).GetComponent<Text>().text = cateResourceList[i + 1].name;
					int rightCid = cateResourceList[i + 1].cid;
					rightTransform.GetComponent<Button>().onClick.AddListener(delegate
					{
						GoToCateDetailPageByCid(rightCid, rightParent);
					});
					RawImage component4 = rightParent.GetChild(0).GetChild(0).GetComponent<RawImage>();
					LoadCoverImage(component4, cateResourceList[i + 1].url);
					rightTransform.GetComponent<CateBtnController>().cid = rightCid;
					JudgeIsShowRedPoint(cateResourceList[i + 1], rightParent.GetChild(1).GetChild(0));
				}
				else
				{
					UnityEngine.Object.Destroy(gameObject2.transform.Find("BtnRight").gameObject);
				}
				gameObject2.GetComponent<RectTransform>().anchoredPosition = new Vector3(x, yOffset, 0f);
				yOffset -= itemHeight;
			}
		}
		SaveToPlayerfab();
	}

	private void JudgeIsShowRedPoint(CateResource cate, Transform transform)
	{
		if (ApplicationModel.CateTimeStampList != null && ApplicationModel.CateTimeStampList.Count != 0)
		{
			foreach (CateTimeStamp cateTimeStamp in ApplicationModel.CateTimeStampList)
			{
				if (cateTimeStamp.cid == cate.cid && PublicImgaeApi.IsShowRedPoint(cateTimeStamp, cate))
				{
					transform.gameObject.SetActive(value: true);
				}
			}
		}
	}

	private void SaveToPlayerfab()
	{
		if (ApplicationModel.CateTimeStampList == null)
		{
			ApplicationModel.CateTimeStampList = new List<CateTimeStamp>();
		}
		ApplicationModel.CateTimeStampList.Clear();
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			ApplicationModel.CateTimeStampList.Add(new CateTimeStamp(cateResource.cid, cateResource.updateTime));
		}
		ApplicationModel.RestoreCateTimeStampList();
	}

	private void LoadCoverImage(RawImage rawImage, string url)
	{
		URLRequest request = new URLRequest(url);
		DataBroker.getTexture2D(request, delegate(Texture2D tex)
		{
			rawImage.texture = tex;
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.Log(ex);
		});
	}

	private void GoToCateDetailPageByCid(int cid, Transform transform)
	{
		UnityEngine.Debug.Log("GoToCateDetailPageByCid:" + cid);
		if (cid == NetWorkRequest.CATE_XIANGSU_ART)
		{
			transform.GetChild(1).GetChild(0).gameObject.SetActive(value: false);
			PixelArtDetailCanvas.transform.Find("CatePage").GetComponent<PixelCatepageScrollRectAdapter>().currentCategory = PublicImgaeApi.GetCateByCid(cid);
			UIRoot.HideUI();;
			PixelArtDetailCanvas.transform.Find("CatePage").GetComponent<PixelCatepageScrollRectAdapter>().isRefresh = true;
			PixelArtDetailCanvas.SetActive(value: true);
		}
		else
		{
			transform.GetChild(1).GetChild(0).gameObject.SetActive(value: false);
			CateDetailCanvas.transform.Find("CatePage").GetComponent<CatePageScrollRectAdapter>().currentCategory = PublicImgaeApi.GetCateByCid(cid);
			UIRoot.HideUI();;
			CateDetailCanvas.transform.Find("CatePage").GetComponent<CatePageScrollRectAdapter>().isRefresh = true;
			CatePageScrollRectAdapter.from = CatePageScrollRectAdapter.PageFrom.index;
			CateDetailCanvas.SetActive(value: true);
		}
	}

	public void GoToDealiyPage()
	{
		URLRequest request = APIFactory.create(APIFactory.DAILY_FREE);
		DataBroker.getJson(request, delegate(SingleImageIdJson detail)
		{
			if (detail.status == 0)
			{
				requestDailyFreeCallback(detail.data.imageId);
			}
			else
			{
				UnityEngine.Debug.Log("request error!!!");
			}
		}, delegate
		{
			UnityEngine.Debug.Log("request error!!!");
		});
	}

	private void requestDailyFreeCallback(string imageId)
	{
		DailyfreeCanvas.transform.Find("event").GetComponent<DailfreePagecontroller>().freeImageId = imageId;
		UIRoot.HideUI();;
		DailyfreeCanvas.SetActive(value: true);
	}
}
