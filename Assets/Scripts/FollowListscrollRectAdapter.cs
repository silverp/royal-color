using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowListscrollRectAdapter : MonoBehaviour
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class Model
	{
		public FollowUserInfo user;

		public int index;

		public Model(FollowUserInfo user, int index)
		{
			this.user = user;
			this.index = index;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<Model> _Data;

		public RectTransform root;

		private static Texture2D defaultHeadTex = Resources.Load<Texture2D>("Images/share/touxiang_moren");

		public MyWorkScrollRectItemsAdapter(List<Model> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		public void SetImage(FollowUserInfo user, int index)
		{
			URLRequest request = new URLRequest(user.headImgUrl);
			root.GetChild(0).GetChild(0).GetComponent<RawImage>()
				.texture = defaultHeadTex;
			DataBroker.getTexture2D(request, delegate (Texture2D tex)
			{
				root.GetChild(0).GetChild(0).GetComponent<RawImage>()
					.texture = tex;
			}, delegate (Exception ex)
				{
					UnityEngine.Debug.Log(ex);
				});
			root.GetChild(0).GetChild(2).GetComponent<Text>()
				.text = user.nickname;
			root.GetChild(1).GetChild(0).GetComponent<Text>()
				.text = user.likeCnt.ToString();
			Transform child = root.GetChild(2);
			FollowBtnControl(user, child);
			UserClickController(user);
		}

		private void FollowBtnControl(bool isFollow, GameObject followBtn)
		{
			if (isFollow)
			{
				followBtn.transform.GetChild(0).gameObject.SetActive(value: true);
				followBtn.transform.GetChild(1).gameObject.SetActive(value: false);
			}
			else
			{
				followBtn.transform.GetChild(0).gameObject.SetActive(value: false);
				followBtn.transform.GetChild(1).gameObject.SetActive(value: true);
			}
		}

		private void UserClickController(FollowUserInfo user)
		{
			root.GetChild(0).GetComponent<Button>().onClick.RemoveAllListeners();
			root.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate
			{
				onClickHeadImg(user);
			});
		}

		private void onClickHeadImg(FollowUserInfo FollowUserInfo)
		{
			FollowStatusCanvasEvent.Instance.GotoOtherCanvas(FollowUserInfo);
		}

		private void FollowBtnControl(FollowUserInfo user, Transform followBtn)
		{
			followBtn.GetComponent<Button>().onClick.RemoveAllListeners();
			if (user.isFollow)
			{
				followBtn.GetChild(0).gameObject.SetActive(value: false);
				followBtn.GetChild(1).gameObject.SetActive(value: true);
				FollowUserInfo followUserInfo = user;
				followBtn.GetComponent<Button>().onClick.AddListener(delegate
				{
					clickFollowBtn(user, followBtn);
				});
			}
			else
			{
				followBtn.GetChild(0).gameObject.SetActive(value: true);
				followBtn.GetChild(1).gameObject.SetActive(value: false);
				FollowUserInfo followUserInfo2 = user;
				followBtn.GetComponent<Button>().onClick.AddListener(delegate
				{
					clickFollowBtn(user, followBtn);
				});
			}
		}

		private void clickFollowBtn(FollowUserInfo user, Transform followBtn)
		{
			URLRequest uRLRequest = (!followBtn.GetChild(0).gameObject.activeSelf) ? APIFactory.create(APIFactory.REMOVE_FOLLOW) : APIFactory.create(APIFactory.ADD_FOLLOW);
			uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			uRLRequest.addParam("followUid", user.uid);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
			DataBroker.getJson(uRLRequest, delegate (StandardReturnCode detail)
			{
				if (detail.status == 0)
				{
					user.isFollow = !user.isFollow;
					FollowBtnControl(user, followBtn);
				}
			}, delegate (Exception ex)
			{
				UnityEngine.Debug.Log(ex);
			});
		}

		protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
			myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return myWorkItemsViewHolder;
		}

		protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			Model model = _Data[newOrRecycled.itemIndex];
			SetImage(model.user, model.index);
		}
	}

	[SerializeField]
	private MyWorkParams _ScrollRectAdapterParams;

	[SerializeField]
	private GameObject TextList;

	[SerializeField]
	private GameObject canvasList;

	[SerializeField]
	private GameObject FollowStatusCanvas;

	[SerializeField]
	private GameObject note;

	private List<Model> _Data = new List<Model>();

	private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

	public List<FollowUserInfo> followUserList;

	public int option;

	public string returnPage;

	public string uid;

	public BaseParams Params => _ScrollRectAdapterParams;

	public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<Model> Data => _Data;

	public void back()
	{
		FollowStatusCanvas.SetActive(value: false);
		canvasList.transform.Find(returnPage).gameObject.SetActive(value: true);
	}

	private void Awake()
	{
		_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void OnEnable()
	{
		GameQuit.Back += back;
		UnityEngine.Debug.Log("option is:" + option);
		note.SetActive(value: false);
		followUserList = null;
		HideChild(base.transform.GetChild(0).GetChild(0));
		SetTextListChildUnable();
		TextList.transform.GetChild(option).gameObject.SetActive(value: true);
		RequestInfo(option);
	}

	private void OnDisable()
	{
		GameQuit.Back -= back;
	}

	private void RequestInfo(int option)
	{
		string empty = string.Empty;
		bool flag;
		switch (option)
		{
			case 0:
				flag = true;
				empty = APIFactory.GET_ALLFOLLOW;
				break;
			case 1:
				flag = true;
				empty = APIFactory.GET_ALLFANS;
				break;
			case 2:
				flag = false;
				empty = APIFactory.GET_ALLFOLLOW;
				break;
			case 3:
				flag = false;
				empty = APIFactory.GET_ALLFANS;
				break;
			default:
				flag = true;
				empty = APIFactory.GET_ALLFOLLOW;
				break;
		}
		URLRequest uRLRequest = APIFactory.create(empty);
		uRLRequest.addParam("uid", uid);
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addParam("isMine", flag.ToString());
		DataBroker.getJson(uRLRequest, delegate (FollowUserInfoListJson detail)
		{
			if (detail.status == 0)
			{
				followUserList = detail.data;
				StartCoroutine(initContent());
			}
			else
			{
				UnityEngine.Debug.Log(string.Empty);
				StartCoroutine(initContent());
			}
		}, delegate (Exception ex)
		{
			UnityEngine.Debug.Log(ex);
			StartCoroutine(initContent());
		});
	}

	private void SetTextListChildUnable()
	{
		for (int i = 0; i < 4; i++)
		{
			TextList.transform.GetChild(i).gameObject.SetActive(value: false);
		}
	}

	private IEnumerator initContent()
	{
		if (followUserList == null || followUserList.Count == 0)
		{
			note.SetActive(value: true);
			if (option == 0 || option == 2)
			{
				note.transform.GetChild(0).gameObject.SetActive(value: true);
				note.transform.GetChild(1).gameObject.SetActive(value: false);
			}
			else
			{
				note.transform.GetChild(0).gameObject.SetActive(value: false);
				note.transform.GetChild(1).gameObject.SetActive(value: true);
			}
			yield break;
		}
		yield return null;
		_Data.Clear();
		int ImgTotal = followUserList.Count;
		for (int i = 0; i < ImgTotal; i++)
		{
			_Data.Add(new Model(followUserList[i], i));
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void OnDestroy()
	{
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private void HideChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				transform2.gameObject.SetActive(value: false);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
