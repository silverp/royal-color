using UnityEngine;
using UnityEngine.UI;

public class ColorComplexBtnController : MonoBehaviour
{
	private Color selfColor;

	private void Start()
	{
		selfColor = base.transform.Find("colData").transform.GetComponent<Image>().color;
	}

	private void OnEnable()
	{
		WorkShopManagerController.onPencilColorChanged += judgeIfSelected;
	}

	private void OnDisable()
	{
		WorkShopManagerController.onPencilColorChanged -= judgeIfSelected;
	}

	private void judgeIfSelected(Color from, Color to)
	{
		if (selfColor == to)
		{
			base.transform.GetChild(2).gameObject.SetActive(value: true);
		}
		else
		{
			base.transform.GetChild(2).gameObject.SetActive(value: false);
		}
	}
}
