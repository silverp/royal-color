using System.Collections.Generic;

public class CateListManager
{
	protected static CateListManager _instance;

	public int currentListBar;

	public List<int> cidList;

	public int MaxListBarNum;

	public static CateListManager instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new CateListManager();
			}
			return _instance;
		}
	}

	public int getCurrentCateCid()
	{
		return cidList[currentListBar];
	}
}
