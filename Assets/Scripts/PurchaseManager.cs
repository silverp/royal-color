using EasyMobile;
using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.Purchasing.Security;

public class PurchaseManager : MonoBehaviour
{
	public static PurchaseManager Instance;

	private string weixinOrderId = string.Empty;

	public static event Action<string, string> PurchaseCompleted;

	public static event Action PurchaseFailed;

	public static event Action PurchaseCancle;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void OnEnable()
	{
		InAppPurchasing.PurchaseCompleted += PurchaseCompletedHandler;
		InAppPurchasing.PurchaseFailed += PurchaseFailedHandler;
		//AndroidBrige.PurchaseCallback += WeixinPurchaseHandler;
	}

	private void OnDisable()
	{
		InAppPurchasing.PurchaseCompleted -= PurchaseCompletedHandler;
		InAppPurchasing.PurchaseFailed -= PurchaseFailedHandler;
		//AndroidBrige.PurchaseCallback -= WeixinPurchaseHandler;
	}

	public void Purchase(CustomProduct product)
	{
		UnityEngine.Debug.Log("Purchase name is:" + product.name);
		UnityEngine.Debug.Log("product is:" + JsonConvert.SerializeObject(product));
		string platform = product.platform;
		if (platform == null)
		{
			return;
		}
		if (!(platform == "ios"))
		{
			if (!(platform == "googleplay"))
			{
				if (platform == "weixin")
				{
					PurchaseWithWeixin(product);
				}
			}
			else
			{
				PurchaseWidthGoogle(product);
			}
		}
		else
		{
			PurchaseWidthIOS(product);
		}
	}

	public bool CheckIfOwned(string productName)
	{
		return InAppPurchasing.IsProductOwned(productName);
	}

	public bool IsExpire(CustomProduct product)
	{
		return true;
	}

	private void PurchaseWidthGoogle(CustomProduct product)
	{
		UnityEngine.Debug.Log("PurchaseWidthGoogles.." + product.name);
		InAppPurchasing.Purchase(product.name);
	}

	private void PurchaseWidthIOS(CustomProduct product)
	{
		bool flag = CheckIfOwned(product.name);
		UnityEngine.Debug.Log("purchase width ios is:" + product.name);
		InAppPurchasing.Purchase(product.name);
	}

	private void PurchaseWithWeixin(CustomProduct product)
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			PurchaseWithWeixinWithLogin(product);
		}
		else
		{
			PurchaseWithWeixinWithDeviceid(product);
		}
	}

	private void PurchaseWithWeixinWithDeviceid(CustomProduct product)
	{
		UnityEngine.Debug.Log("PurchaseWithWeixin ...");
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.PURCHASE_WITH_WEIXIN_DEVICEID);
		uRLRequest.addParam("deviceId", ApplicationModel.device_id);
		uRLRequest.addParam("productId", product.productId);
		string text = PublicToolController.RandomString(32);
		uRLRequest.addParam("check", PublicUserInfoApi.GetUSerMd5HashValue(text, ApplicationModel.device_id));
		uRLRequest.addParam("stamp", text);
		uRLRequest.addParam("timestamp", PublicToolController.UnixTimestampFromDateTime(DateTime.Now));
		DataBroker.getJson(uRLRequest, delegate(WeixinPurchaseReturnJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				weixinOrderId = detail.data.serialNumber;
				requestWeixinOrderCallback(err, JsonConvert.SerializeObject(detail.data), product);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				requestWeixinOrderCallback(err, string.Empty, product);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			requestWeixinOrderCallback(err, string.Empty, product);
		});
	}

	private void GetImageId()
	{
	}

	private void PurchaseWithWeixinWithLogin(CustomProduct product)
	{
		UnityEngine.Debug.Log("PurchaseWithWeixin ...");
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.PURCHASE_WITH_WEIXIN);
		uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
		uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		uRLRequest.addParam("productId", product.productId);
		string text = PublicToolController.RandomString(32);
		uRLRequest.addParam("check", PublicUserInfoApi.GetUSerMd5HashValue(text, ApplicationModel.userInfo.Uid));
		uRLRequest.addParam("stamp", text);
		uRLRequest.addParam("timestamp", PublicToolController.UnixTimestampFromDateTime(DateTime.Now));
		DataBroker.getJson(uRLRequest, delegate(WeixinPurchaseReturnJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				weixinOrderId = detail.data.serialNumber;
				requestWeixinOrderCallback(err, JsonConvert.SerializeObject(detail.data), product);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				requestWeixinOrderCallback(err, string.Empty, product);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			requestWeixinOrderCallback(err, string.Empty, product);
		});
	}

	private void requestWeixinOrderCallback(Errcode err, string req, CustomProduct product)
	{
		UnityEngine.Debug.Log("requestWeixinOrderCallback errcode is:" + err.errorCode);
		UnityEngine.Debug.Log("requestWeixinOrderCallback req is:" + req);
		//if (err.errorCode == Errcode.OK)
		//{
		//	//AndroidBrige.instance.payReq(AndroidBrige.Plaform.WECHAT, req);
		//}
		//else
		{
			PurchaseManager.PurchaseFailed();
		}
	}

	private void PurchaseCompletedHandler(string productName)
	{
		//GooglePlayReceipt googlePlayReceipt = InAppPurchasing.GetGooglePlayReceipt(productName);
		//if (googlePlayReceipt != null)
		//{
		//	PurchaseManager.PurchaseCompleted(string.Empty, googlePlayReceipt.transactionID);
		//}
		//else
		//{
		//	PurchaseManager.PurchaseCompleted(string.Empty, string.Empty);
		//}
	}

	private void PurchaseCompletedHandler(IAPProduct product)
	{
		PurchaseCompletedHandler(product.Name);
	}

	private void PurchaseFailedHandler(IAPProduct product, string error)
	{
		PurchaseManager.PurchaseFailed();
	}

	private void WeixinPurchaseHandler(string req)
	{
		UnityEngine.Debug.Log("purchase wexin ...complete");
		WexinErrcodeJson wexinErrcodeJson = JsonConvert.DeserializeObject<WexinErrcodeJson>(req);
		if (wexinErrcodeJson.errorCode == 0)
		{
			PurchaseManager.PurchaseCompleted(weixinOrderId, string.Empty);
			UnityEngine.Debug.Log("purchase wexin ...success");
		}
		else if (wexinErrcodeJson.errorCode == -1)
		{
			PurchaseManager.PurchaseFailed();
			UnityEngine.Debug.Log("purchase wexin ...fail");
		}
		else
		{
			PurchaseManager.PurchaseCancle();
			UnityEngine.Debug.Log("purchase wexin ...cancel");
		}
	}
}
