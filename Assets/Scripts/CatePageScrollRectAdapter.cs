using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class CatePageScrollRectAdapter : MonoBehaviour
{
	[Serializable]
	public class CatePageParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public enum PageFrom
	{
		index,
		artist
	}

	public class DoubleImageModel
	{
		public ResourceImg leftImg;

		public ResourceImg rightImg;

		public int cidNum;

		public DoubleImageModel(ResourceImg leftImg, ResourceImg rightImg, int index)
		{
			if (leftImg != null)
			{
				this.leftImg = leftImg;
			}
			else
			{
				this.leftImg = null;
			}
			if (rightImg != null)
			{
				this.rightImg = rightImg;
			}
			else
			{
				this.rightImg = null;
			}
			cidNum = index;
		}
	}

	public class CatePageItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class CatePageScrollRectItemsAdapter : ScrollRectItemsAdapter8<CatePageParams, CatePageItemsViewHolder>
	{
		private static Texture2D loadingTex = Resources.Load<Texture2D>("Images/Image/small_prefab");

		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<DoubleImageModel> _Data;

		private readonly string index;

		public RectTransform root;

		public CatePageItemsViewHolder catePageItemsViewHolder;

		public CatePageScrollRectItemsAdapter(List<DoubleImageModel> data, CatePageParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageId, int i)
		{
			//string b = string.Empty;
			//if (i == 0)
			//{
			//	b = _Data[itemIndex].leftImg.imageId;
			//}
			//else if (_Data[itemIndex].rightImg != null)
			//{
			//	b = _Data[itemIndex].rightImg.imageId;
			//}
			return _Data.Count > itemIndex;// && itemIdexAtRequest == itemIndex && imageId == b;
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		protected override CatePageItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			CatePageItemsViewHolder catePageItemsViewHolder = new CatePageItemsViewHolder();
			catePageItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return catePageItemsViewHolder;
		}

		protected override void UpdateViewsHolder(CatePageItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			catePageItemsViewHolder = newOrRecycled;
			DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
			setLeftImage(doubleImageModel.leftImg, doubleImageModel.cidNum);
			setRightImage(doubleImageModel.rightImg, doubleImageModel.cidNum);
		}

		public void setLeftImage(ResourceImg img, int index)
		{
			setImageByBtnName("BtnLeft", img, index);
			judgeIsLoadNext(index);
			countDropdownEvent(index);
		}

		public void setRightImage(ResourceImg img, int index)
		{
			setImageByBtnName("BtnRight", img, index);
		}

		private void countDropdownEvent(int num)
		{
			if (num % 6 == 0)
			{
				if (from == PageFrom.index)
				{
					TotalGA.Event("cate_drop", "分类:" + ApplicationModel.currentCategroy.name + "下拉" + (num / 6).ToString() + "页");
				}
				else
				{
					TotalGA.Event("cate_drop", "分类:" + ApplicationModel.currentArtistAlbumModel.artistName + "下拉" + (num / 6).ToString() + "页");
				}
			}
		}

		private void OnImageHit(ResourceImg img, GameObject lockbtn)
		{
			EnterDetailFromCatelistevent.instance.GotoWorkShopEvent(img, lockbtn);
		}

		public void judgeIsLoadNext(int num)
		{
			List<ResourceImg> list = (from != 0) ? ApplicationModel.currentArtistAlbumModel.imgList : ApplicationModel.currentCategroy.imgList;
			if (list != null && num == list.Count - 8)
			{
				DragEventProxy.Instance.SendRequestNextPageMsg();
			}
		}

		private void setImageByBtnName(string name, ResourceImg img, int num)
		{
			if (img != null)
			{
				EditedImg editedImgFromEditedList = PublicImgaeApi.GetEditedImgFromEditedList(img.imageId);
				bool locked = UnlockArtManager.Instance.IsShowLock(img);
				if (!root.Find(name).gameObject.activeSelf)
				{
					root.Find(name).gameObject.SetActive(value: true);
				}
				if (editedImgFromEditedList != null && !editedImgFromEditedList.isRemote && editedImgFromEditedList.HasEdited())
				{
					Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
					texture2D.LoadImage(PublicToolController.FileReaderBytes(editedImgFromEditedList.imageId + "_ok"), markNonReadable: false);
					texture2D.Apply();
					Transform imgTransform = root.Find(name);
					string imageId = img.imageId;
					callback(texture2D, name, img, imgTransform, locked, imageId);
				}
				else
				{
					root.Find(name).GetChild(1).GetChild(0)
						.GetComponent<RawImage>()
						.texture = loadingTex;
					root.Find(name).GetComponent<RawImageDataController>().downloadStatus = 0;
					root.Find(name).GetComponent<Button>().onClick.RemoveAllListeners();
					string empty = string.Empty;
					empty = ((editedImgFromEditedList == null || !editedImgFromEditedList.isRemote) ? img.thumbnailUrl : editedImgFromEditedList.thumb);
					Transform imgTRansform = root.Find(name);
					string _imageId = img.imageId;
					NetLoadcontroller.Instance.RequestImg(empty, delegate (Texture2D tex)
					{
						callback(tex, name, img, imgTRansform, locked, _imageId);
					});
				}
				LockControl(locked, root.Find(name).GetChild(2).gameObject);
			}
			else
			{
				root.Find(name).gameObject.SetActive(value: false);
			}
		}

		public void callback(Texture2D tex, string btnName, ResourceImg img, Transform imgTransform, bool locked, string imageId)
		{
			int itemIndex = catePageItemsViewHolder.itemIndex;
			int i = (!(btnName == "BtnLeft")) ? 1 : 0;
			if (IsModelStillValid(catePageItemsViewHolder.itemIndex, itemIndex, imageId, i))
			{
				RawImage component = imgTransform.GetChild(1).GetChild(0).GetComponent<RawImage>();
				if (component != null)
				{
					component.texture = tex;
				}
				ResourceImg resourceImg = img;
				imgTransform.GetChild(1).GetChild(0).GetComponent<ImageChangeController>()
					.imageId = img.imageId;
				imgTransform.GetComponent<Button>().onClick.RemoveAllListeners();
				bool flag = locked;
				imgTransform.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnImageHit(img, imgTransform.Find("lock").gameObject);
				});
			}
		}

		private void LockControl(bool locked, GameObject lockBtn)
		{
			if (locked)
			{
				lockBtn.SetActive(value: true);
			}
			else
			{
				lockBtn.SetActive(value: false);
			}
		}
	}

	[SerializeField]
	private CatePageParams _ScrollRectAdapterParams;

	[SerializeField]
	private GameObject CateDetailCanvas;

	[SerializeField]
	private GameObject NetErrorNotePanel;

	private List<DoubleImageModel> _Data;

	private CatePageScrollRectItemsAdapter _ScrollRectItemsAdapter;

	private int step = 20;

	public bool isRefresh;

	public CateResource currentCategory;

	public ArtistAlbumModel currentArtistAlbumModel;

	public List<PixelArt> currentPixelArtList;

	public List<ResourceImg> imgList;

	public static PageFrom from;

	public BaseParams Params => _ScrollRectAdapterParams;

	public CatePageScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<DoubleImageModel> Data => _Data;

	private void Awake()
	{
		DragEventProxy.OnRefreshAfterPurchase += RefreshData;
		DragEventProxy.OnRefreshAfterLogin += RefreshData;
		_Data = new List<DoubleImageModel>();
		_ScrollRectItemsAdapter = new CatePageScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void AssginEvents()
	{
		DragEventProxy.OnRequestNextPage += RequestNextPage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnRequestNextPage -= RequestNextPage;
	}

	private void RefreshData()
	{
		isRefresh = true;
	}

	private void OnEnable()
	{
		SetNetErrorPanelStatus(isShow: false);
		AssginEvents();
		RefreshImgList();
	}

	private void RefreshImgList()
	{
		if (isRefresh)
		{
			HideChild(base.transform.GetChild(0).GetChild(0));
			init();
			LoadCateData();
			isRefresh = false;
		}
		else if (base.transform.GetChild(0).GetChild(0).childCount == 0)
		{
			LoadCateData();
		}
	}

	public void ClickToRefresh()
	{
		LoadCateData();
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void init()
	{
		Transform transform = CateDetailCanvas.transform.Find("wave");
		if (from == PageFrom.index)
		{
			ApplicationModel.currentCategroy = currentCategory;
			transform.GetChild(0).gameObject.SetActive(value: true);
			transform.GetChild(1).gameObject.SetActive(value: false);
			transform.GetChild(0).GetChild(0).GetComponent<Text>()
				.text = currentCategory.name;
			LoadTopCover(currentCategory.innerUrl);
		}
		else if (from == PageFrom.artist)
		{
			transform.GetChild(0).gameObject.SetActive(value: false);
			transform.GetChild(1).gameObject.SetActive(value: true);
			transform.GetChild(1).GetChild(0).GetComponent<Text>()
				.text = currentArtistAlbumModel.name;
			transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = currentArtistAlbumModel.artistName + "  " + PublicToolController.GetTextByLanguage("create");
			ApplicationModel.currentArtistAlbumModel = currentArtistAlbumModel;
			LoadTopCover(currentArtistAlbumModel.innerUrl);
		}
	}

	private void LoadTopCover(string url)
	{
		URLRequest request = new URLRequest(url);
		DataBroker.getTexture2D(request, delegate (Texture2D tex)
		{
			base.transform.parent.Find("TopPanel").Find("Image").GetComponent<RawImage>()
				.texture = tex;
		}, delegate (Exception ex)
							{
				UnityEngine.Debug.Log(ex);
			});
	}

	private void LoadCateData()
	{
		if (from == PageFrom.index)
		{
			if (currentCategory.imgList == null || currentCategory.imgList.Count == 0)
			{
				PublicImgaeApi.GetCateImglist(delegate (Errcode err, List<ResourceImg> x)
				{
					RequestImglistJsonCallback(err, x);
				}, currentCategory.cid, 1, step);
			}
			else
			{
				StartCoroutine(initContent(currentCategory.imgList));
			}
		}
		else if (from == PageFrom.artist)
		{
			UnityEngine.Debug.Log("LoadCateData.......");
			if (currentArtistAlbumModel.imgList == null || currentArtistAlbumModel.imgList.Count == 0)
			{
				UnityEngine.Debug.Log("LoadCateData ....");
				PublicControllerApi.GetAlbumImageList(1, step, currentArtistAlbumModel.albumId, delegate (Errcode err, List<ResourceImg> x)
				{
					RequestAlbumImglistCallback(err, x);
				});
			}
			else
			{
				StartCoroutine(initContent(currentArtistAlbumModel.imgList));
			}
		}
	}

	private void RequestNextPage()
	{
		int prePage = (int)Mathf.Ceil(imgList.Count / step);
		if (from == PageFrom.index)
		{
			PublicImgaeApi.GetCateImglist(delegate (Errcode err, List<ResourceImg> x)
			{
				AddItem(err, x, prePage + 1);
			}, currentCategory.cid, prePage + 1, step);
		}
		else
		{
			PublicControllerApi.GetAlbumImageList(prePage + 1, step, currentArtistAlbumModel.albumId, delegate (Errcode err, List<ResourceImg> x)
			{
				AddItem(err, x, prePage + 1);
			});
		}
	}

	private void AddItem(Errcode err, List<ResourceImg> imglist, int page)
	{
		if (imglist == null || imglist.Count == 0 || imgList.Count != (page - 1) * step)
		{
			return;
		}
		int count = imgList.Count;
		imgList.AddRange(imglist);
		int count2 = imglist.Count;
		for (int i = 0; i < count2; i += 2)
		{
			if (i + 1 < count2)
			{
				_Data.Add(new DoubleImageModel(imglist[i], imglist[i + 1], i + count));
			}
			else
			{
				_Data.Add(new DoubleImageModel(imglist[i], null, i + count));
			}
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
	}

	private void RequestImglistJsonCallback(Errcode err, List<ResourceImg> imgList)
	{
		if (err.errorCode == Errcode.OK)
		{
			currentCategory.imgList = imgList;
			StartCoroutine(initContent(currentCategory.imgList));
			ApplicationModel.RestoreCateList();
		}
		else
		{
			SetNetErrorPanelStatus(isShow: true);
		}
	}

	private void RequestAlbumImglistCallback(Errcode err, List<ResourceImg> imgList)
	{
		if (err.errorCode == Errcode.OK)
		{
			currentArtistAlbumModel.imgList = imgList;
			StartCoroutine(initContent(currentArtistAlbumModel.imgList));
		}
		else
		{
			SetNetErrorPanelStatus(isShow: true);
		}
	}

	private void RequestPixelArtlistCallback(Errcode err, List<PixelArt> imgList)
	{
		if (err.errorCode == Errcode.OK)
		{
			currentPixelArtList = imgList;
			StartCoroutine(initContent(currentArtistAlbumModel.imgList));
		}
	}

	private void SetNetErrorPanelStatus(bool isShow)
	{
		NetErrorNotePanel.SetActive(isShow);
	}

	private IEnumerator initContent(List<ResourceImg> list)
	{
		yield return null;
		yield return null;
		int ImgTotal = (list != null) ? list.Count : 0;
		UnityEngine.Debug.Log("ImgTotal is:" + ImgTotal);
		if (ImgTotal > 0)
		{
			SetNetErrorPanelStatus(isShow: false);
			imgList = list;
			_Data.Clear();
			for (int i = 0; i < ImgTotal; i += 2)
			{
				if (i + 1 < ImgTotal)
				{
					_Data.Add(new DoubleImageModel(list[i], list[i + 1], i));
				}
				else
				{
					_Data.Add(new DoubleImageModel(list[i], null, i));
				}
			}
			_Data.Capacity = _Data.Count;
			_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
			_ScrollRectItemsAdapter.ScrollToStart();
			StartCoroutine(unloadAssets());
		}
		else
		{
			SetNetErrorPanelStatus(isShow: true);
		}
	}

	private IEnumerator unloadAssets()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		Resources.UnloadUnusedAssets();
	}

	private void OnDestroy()
	{
		DragEventProxy.OnRefreshAfterPurchase -= RefreshData;
		DragEventProxy.OnRefreshAfterLogin -= RefreshData;
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private bool isExistsImageid(string imageId, List<string> list)
	{
		foreach (string item in list)
		{
			if (item == imageId)
			{
				return true;
			}
		}
		return false;
	}

	private void HideChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				transform2.gameObject.SetActive(value: false);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}
}
