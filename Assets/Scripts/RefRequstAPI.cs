using System;
using UnityEngine;

public class RefRequstAPI
{
	public static void GetRefImage(Action<Errcode, RefImageInfo> callback, string imageId)
	{
		UnityEngine.Debug.Log("GetRefImage...");
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create("/image/getOriginDetail");
		uRLRequest.addParam("imageId", imageId);
		DataBroker.getJson(uRLRequest, delegate(RefImageInfoJsonList detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}
}
