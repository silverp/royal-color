using UnityEngine;
using UnityEngine.UI;

public class ImageReplaceControllerForBig : MonoBehaviour
{
	[SerializeField]
	private string imageName;

	private string imagePath;

	private void Start()
	{
		if (ApplicationModel.LANGUAGE != "en")
		{
			if (Screen.width > 1080)
			{
				imagePath = "Images/Icon_language/" + imageName + "_" + ApplicationModel.LANGUAGE + "_big";
			}
			else
			{
				imagePath = "Images/Icon_language/" + imageName + "_" + ApplicationModel.LANGUAGE;
			}
		}
		else if (Screen.width > 1080)
		{
			imagePath = "Images/Icon_language/" + imageName + "_big";
		}
		else
		{
			imagePath = "Images/Icon_language/" + imageName;
		}
		base.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePath);
	}

	private void Update()
	{
	}
}
