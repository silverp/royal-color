using UnityEngine;

public class MyworkCanvasBtnController : MonoBehaviour
{
	[SerializeField]
	private GameObject SettingPanel;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject ObtainBeanCanvas;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void GotoSettingPanel()
	{
		TotalGA.Event("enter_setting_page");
		UIRoot.HideUI();
		SettingPanel.SetActive(value: true);
	}

	public void GotoObtainBeansCanvas()
	{
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			UIRoot.HideUI();
			ObtainBeanCanvas.SetActive(value: true);
		}
	}
}
