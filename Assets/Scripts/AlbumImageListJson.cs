using System.Collections.Generic;

public class AlbumImageListJson
{
	public int status;

	public string msg;

	public List<ResourceImg> data;
}
