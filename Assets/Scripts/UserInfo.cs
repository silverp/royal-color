using System.Collections.Generic;

public class UserInfo
{
	private string uid;

	private string nickname;

	private string sessionId;

	private string sex;

	private string headimgUrl;

	private string platform;

	private string openid;

	private string unionId;

	private string country;

	private string province;

	private string city;

	private long create;

	private long lastLogin;

	private string device;

	private long update;

	private int followCnt;

	private int fansCnt;

	private int beanCnt;

	private int vipLevel;

	private long vipExpired;

	private bool isFirstLogin;

	private long ownProductId;

	public List<string> unlockList;

	public long OwnProductId
	{
		get
		{
			return ownProductId;
		}
		set
		{
			ownProductId = value;
		}
	}

	public int FollowCnt
	{
		get
		{
			return followCnt;
		}
		set
		{
			followCnt = value;
		}
	}

	public int FansCnt
	{
		get
		{
			return fansCnt;
		}
		set
		{
			fansCnt = value;
		}
	}

	public int BeanCnt
	{
		get
		{
			return beanCnt;
		}
		set
		{
			beanCnt = value;
		}
	}

	public int VipLevel
	{
		get
		{
			return vipLevel;
		}
		set
		{
			vipLevel = value;
		}
	}

	public long VipExpired
	{
		get
		{
			return vipExpired;
		}
		set
		{
			vipExpired = value;
		}
	}

	public string Uid
	{
		get
		{
			return uid;
		}
		set
		{
			uid = value;
		}
	}

	public string Nickname
	{
		get
		{
			return nickname;
		}
		set
		{
			nickname = value;
		}
	}

	public string SessionId
	{
		get
		{
			return sessionId;
		}
		set
		{
			sessionId = value;
		}
	}

	public string Sex
	{
		get
		{
			return sex;
		}
		set
		{
			sex = value;
		}
	}

	public string HeadimgUrl
	{
		get
		{
			return headimgUrl;
		}
		set
		{
			headimgUrl = value;
		}
	}

	public string Platform
	{
		get
		{
			return platform;
		}
		set
		{
			platform = value;
		}
	}

	public string Openid
	{
		get
		{
			return openid;
		}
		set
		{
			openid = value;
		}
	}

	public string UnionId
	{
		get
		{
			return unionId;
		}
		set
		{
			unionId = value;
		}
	}

	public string Country
	{
		get
		{
			return country;
		}
		set
		{
			country = value;
		}
	}

	public string City
	{
		get
		{
			return city;
		}
		set
		{
			city = value;
		}
	}

	public long Create
	{
		get
		{
			return create;
		}
		set
		{
			create = value;
		}
	}

	public long LastLogin
	{
		get
		{
			return lastLogin;
		}
		set
		{
			lastLogin = value;
		}
	}

	public string Device
	{
		get
		{
			return device;
		}
		set
		{
			device = value;
		}
	}

	public long Update
	{
		get
		{
			return update;
		}
		set
		{
			update = value;
		}
	}

	public bool IsFirstLogin
	{
		get
		{
			return isFirstLogin;
		}
		set
		{
			isFirstLogin = value;
		}
	}

	public UserInfo(string uid, string openid, string nickname, string sex, string headimgUrl, long create)
	{
		this.uid = uid;
		this.openid = openid;
		this.nickname = nickname;
		this.sex = sex;
		this.headimgUrl = headimgUrl;
		this.create = create;
	}
}
