using Mgl;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FeedbackCanvasBtncontroller : MonoBehaviour
{
	[SerializeField]
	private GameObject Feedbackcanvas;

	[SerializeField]
	private GameObject SettingCanvas;

	[SerializeField]
	private GameObject NoteText;

	[SerializeField]
	private GameObject normal;

	[SerializeField]
	private GameObject facebookUrl;

	private GUIStyle guiStyle = new GUIStyle();

	private I18n i18n = I18n.Instance;

	private bool saved;

	private float infoBlurTime = 2f;

	private Texture2D ErQcode;

	public void BackToSettingCanvas()
	{
		UnityEngine.Debug.Log("BackToSettingCanvas.....");
		SettingCanvas.SetActive(value: true);
		Feedbackcanvas.SetActive(value: false);
	}

	private void Start()
	{
		ErQcode = Resources.Load<Texture2D>("Images/feedback/erqcode");
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			normal.SetActive(value: true);
			facebookUrl.SetActive(value: false);
		}
		else
		{
			normal.SetActive(value: false);
			facebookUrl.SetActive(value: true);
		}
	}

	private void OnEnable()
	{
		NoteText.SetActive(value: false);
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(infoBlurTime);
		NoteText.SetActive(value: false);
	}

	private void Update()
	{
	}

	public void SaveErCode()
	{
		UnityEngine.Debug.Log("SaveErCode");
		StartCoroutine(WriteImageEvent());
	}

	private IEnumerator WriteImageEvent()
	{
		yield return startWriteImage();
		yield return showSavedInfo();
	}

	private IEnumerator startWriteImage()
	{
		string dateTime = DateTime.Now.ToString("dd-MM-yy_hhmmss");
		ScreenshotManager.SaveImage(fileName: "small_paint_" + dateTime, texture: ErQcode, albumName: "小涂鸦");
		NoteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("The QRCode has been saved!");
		NoteText.SetActive(value: true);
		yield return null;
	}

	public void GotoFacebook()
	{
		TotalGA.CountEventNumber_FB("feedback", 1);
		Application.OpenURL("https://business.facebook.com/Ecolor-Community-1155629844574429/");
	}

	public void CopyNumber()
	{
		string text = "582336415";
		UniClipboard.SetText(text);
		NoteText.GetComponent<Text>().text = PublicToolController.GetTextByLanguage("copy successful");
		NoteText.SetActive(value: true);
		StartCoroutine(showSavedInfo());
	}
}
