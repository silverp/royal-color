using System.Collections.Generic;

public class ArtistAlbumModel
{
	public string uid;

	public string albumId;

	public string innerUrl;

	public string name;

	public string artistName;

	public string headImgUrl;

	public string url;

	public List<ResourceImg> imgList;

	public long updateTime;
}
