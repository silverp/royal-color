using UnityEngine;

public class CateDetailController : MonoBehaviour
{
	[SerializeField]
	private GameObject CateDetailCanvas;

	[SerializeField]
	private GameObject ImglistCanvas;

	[SerializeField]
	private GameObject AritistCanvas;

	private void OnEnable()
	{
		GameQuit.Back += Back;
	}

	private void OnDisable()
	{
		GameQuit.Back -= Back;
	}

	public void Back()
	{
		if (CatePageScrollRectAdapter.from == CatePageScrollRectAdapter.PageFrom.index)
		{
			CateDetailCanvas.gameObject.SetActive(value: false);
			UIRoot.ShowUI(ImglistCanvas);
		}
		else
		{
			CateDetailCanvas.gameObject.SetActive(value: false);
			UIRoot.ShowUI(AritistCanvas);
		}
	}
}
