using System;
using System.Collections;
using UnityEngine;

namespace UI.ThreeDimensional
{
	[RequireComponent(typeof(RectTransform))]
	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	[AddComponentMenu("UI/UIObject3D/UITemplate")]
	public class UITemplate : MonoBehaviour
	{
		public delegate void takePictureHandler(Texture2D texture);

		[Header("Target")]
		[SerializeField]
		private Transform _ObjectPrefab;

		[SerializeField]
		private Vector2 _ColorCoord = Vector2.zero;

		[SerializeField]
		private Vector3 _TargetRotation = Vector3.zero;

		[SerializeField]
		[Range(-1f, 1f)]
		private float _TargetOffsetX;

		[SerializeField]
		[Range(-1f, 1f)]
		private float _TargetOffsetY;

		[Header("Camera Settings")]
		[SerializeField]
		[Range(20f, 100f)]
		private float _CameraFOV = 35f;

		[SerializeField]
		[Range(-10f, -1f)]
		private float _CameraDistance = -3.5f;

		public Vector2 TextureSize;

		[SerializeField]
		private Color _BackgroundColor = Color.clear;

		[Header("Performance")]
		[SerializeField]
		[Tooltip("Should this UIObject3D limit itself to a particular framerate?")]
		public bool LimitFrameRate;

		[SerializeField]
		[Tooltip("Maximum number of frames to render per second.")]
		public float FrameRateLimit = 30f;

		[Tooltip("If this is enabled, then this UIObject3D will render every frame (optionally limited by FrameRateLimit) even if none of the UIObject3D properties change. This should only be enabled if your target has animations of its own which are not controlled by UIObject3D.")]
		public bool RenderConstantly;

		private float timeSinceLastRender;

		[Header("Lighting")]
		[SerializeField]
		private bool _EnableCameraLight;

		[SerializeField]
		private Color _LightColor = Color.white;

		[SerializeField]
		[Range(0f, 8f)]
		private float _LightIntensity = 1f;

		[SerializeField]
		private Light _Light;

		private bool started;

		private bool hardUpdateQueued;

		private bool renderQueued;

		private bool _enabled;

		private RectTransform _rectTransform;

		private Texture2D _texture2D;

		private RenderTexture _renderTexture;

		private static Transform _parentContainer;

		private Transform _container;

		private Transform _targetContainer;

		private Transform _target;

		private bool initial;

		private Camera _targetCamera;

		private Light _cameraLight;

		private int _objectLayer = -1;

		public Transform ObjectPrefab
		{
			get
			{
				return _ObjectPrefab;
			}
			set
			{
				_ObjectPrefab = value;
				Cleanup();
				renderPreferb();
			}
		}

		public Vector2 ColorCoord
		{
			get
			{
				return _ColorCoord;
			}
			set
			{
				_ColorCoord = value;
				UpdateDisplay();
			}
		}

		public Vector3 TargetRotation
		{
			get
			{
				return _TargetRotation;
			}
			set
			{
				_TargetRotation = UIObject3DUtilities.NormalizeRotation(value);
				UpdateDisplay();
			}
		}

		[SerializeField]
		public Vector2 TargetOffset
		{
			get
			{
				return new Vector2(_TargetOffsetX, _TargetOffsetY);
			}
			set
			{
				_TargetOffsetX = value.x;
				_TargetOffsetY = value.y;
				UpdateDisplay();
			}
		}

		public float CameraFOV
		{
			get
			{
				return _CameraFOV;
			}
			set
			{
				_CameraFOV = value;
				UpdateDisplay();
			}
		}

		public float CameraDistance
		{
			get
			{
				return _CameraDistance;
			}
			set
			{
				_CameraDistance = value;
				UpdateDisplay();
			}
		}

		public Color BackgroundColor
		{
			get
			{
				return _BackgroundColor;
			}
			set
			{
				_BackgroundColor = value;
				UpdateDisplay();
			}
		}

		private float timeBetweenFrames => 1f / FrameRateLimit;

		public bool EnableCameraLight
		{
			get
			{
				return _EnableCameraLight;
			}
			set
			{
				_EnableCameraLight = value;
				UpdateDisplay();
			}
		}

		public Color LightColor
		{
			get
			{
				return _LightColor;
			}
			set
			{
				_LightColor = value;
				UpdateDisplay();
			}
		}

		public float LightIntensity
		{
			get
			{
				return _LightIntensity;
			}
			set
			{
				_LightIntensity = value;
				UpdateDisplay();
			}
		}

		protected RectTransform rectTransform
		{
			get
			{
				if (_rectTransform == null)
				{
					_rectTransform = GetComponent<RectTransform>();
				}
				return _rectTransform;
			}
		}

		protected Texture2D texture2D
		{
			get
			{
				if (_texture2D == null)
				{
					_texture2D = new Texture2D((int)TextureSize.x, (int)TextureSize.y, TextureFormat.ARGB32, mipChain: false, linear: false);
				}
				return _texture2D;
			}
		}

		protected RenderTexture renderTexture
		{
			get
			{
				if (_renderTexture == null)
				{
					_renderTexture = new RenderTexture((int)TextureSize.x, (int)TextureSize.y, 16, RenderTextureFormat.ARGB32);
					if (QualitySettings.antiAliasing > 0)
					{
						_renderTexture.antiAliasing = QualitySettings.antiAliasing;
					}
				}
				return _renderTexture;
			}
		}

		private static Transform parentContainer
		{
			get
			{
				if (_parentContainer == null)
				{
					GameObject gameObject = GameObject.Find("UIObject3D Scenes");
					if (gameObject != null)
					{
						_parentContainer = gameObject.transform;
					}
					else
					{
						_parentContainer = new GameObject().transform;
						_parentContainer.name = "UIObject3D Scenes";
					}
				}
				return _parentContainer;
			}
		}

		protected Transform container
		{
			get
			{
				if (_container == null)
				{
					if (ObjectPrefab == null)
					{
						return null;
					}
					_container = new GameObject().transform;
					_container.SetParent(parentContainer);
					_container.position = Vector3.zero;
					_container.localScale = Vector3.one;
					_container.localRotation = Quaternion.identity;
					_container.gameObject.layer = objectLayer;
					_container.name = "__UIObject3D_" + ObjectPrefab.name;
				}
				return _container;
			}
		}

		protected Transform targetContainer
		{
			get
			{
				if (_targetContainer == null)
				{
					if (container == null)
					{
						return null;
					}
					_targetContainer = new GameObject().transform;
					_targetContainer.SetParent(container);
					_targetContainer.localPosition = Vector3.zero;
					_targetContainer.localScale = Vector3.one;
					_targetContainer.localRotation = Quaternion.identity;
					_targetContainer.name = "Target Container";
					_targetContainer.gameObject.layer = objectLayer;
				}
				return _targetContainer;
			}
		}

		public Transform target
		{
			get
			{
				if (!initial)
				{
					SetupTarget();
				}
				return _target;
			}
		}

		protected Camera targetCamera
		{
			get
			{
				if (_targetCamera == null)
				{
					SetupTargetCamera();
				}
				return _targetCamera;
			}
		}

		protected Light cameraLight
		{
			get
			{
				if (_cameraLight == null)
				{
					SetupCameraLight();
				}
				return _cameraLight;
			}
		}

		protected int objectLayer
		{
			get
			{
				if (_objectLayer == -1)
				{
					_objectLayer = LayerMask.NameToLayer("UIObject3D");
				}
				return _objectLayer;
			}
		}

		public event takePictureHandler OnRenderCallback;

		public void renderPreferb()
		{
			UnityEngine.Debug.Log("renderPreferb");
			if (!initial)
			{
				if (_targetCamera != null)
				{
					_targetCamera.targetTexture = null;
				}
				if (_target != null)
				{
					UnityEngine.Object.DestroyImmediate(_target);
					_target = null;
				}
				Cleanup();
				SetupTarget();
			}
			PublicCallFrameTool.Instance.DelayCallFrames(WaitCallback, 1);
		}

		private void WaitCallback()
		{
			UpdateDisplay();
			Render(instant: true);
		}

		private void Start()
		{
			UIObject3DTimer.AtEndOfFrame(delegate
			{
				started = true;
				OnEnable();
			}, this);
		}

		public void UpdateDisplay(bool instantRender = false)
		{
			if (!started)
			{
				UnityEngine.Debug.Log("has not yet started!");
				return;
			}
			Prepare();
			UpdateTargetPositioningAndScale();
			UpdateTargetCameraPositioningEtc();
			if (_target != null)
			{
				MeshRenderer component = _target.GetComponent<MeshRenderer>();
				if (component != null)
				{
					component.material.SetInt("_RampTexCoordX", (int)Mathf.Ceil(_ColorCoord.x));
					component.material.SetInt("_RampTexCoordY", (int)Mathf.Ceil(_ColorCoord.y));
				}
			}
			Render(instantRender);
		}

		private void OnEnable()
		{
			if (started)
			{
				_enabled = true;
				if (objectLayer != -1)
				{
					ClearObjectLayerFromCameras();
					ClearObjectLayerFromLights();
				}
			}
		}

		private void ClearObjectLayerFromCameras()
		{
			Camera[] array = UnityEngine.Object.FindObjectsOfType<Camera>();
			Camera[] array2 = array;
			foreach (Camera camera in array2)
			{
				if (!(camera.GetComponent<UIObject3DCamera>() != null))
				{
					camera.cullingMask &= ~(1 << objectLayer);
				}
			}
		}

		private void ClearObjectLayerFromLights()
		{
			Light[] array = UnityEngine.Object.FindObjectsOfType<Light>();
			Light[] array2 = array;
			foreach (Light light in array2)
			{
				if (light.type != LightType.Directional && !(light.GetComponent<UIObject3DCamera>() != null))
				{
					light.cullingMask &= ~(1 << objectLayer);
				}
			}
		}

		private void OnDisable()
		{
			_enabled = false;
			Cleanup();
		}

		private void OnDestroy()
		{
			Cleanup();
		}

		private void Prepare()
		{
			SetupTargetCamera();
		}

		public void Cleanup()
		{
			_texture2D = null;
			_renderTexture = null;
			initial = false;
			if (_container != null)
			{
				UnityEngine.Object.DestroyImmediate(container.gameObject);
				_container = null;
			}
		}

		public Transform GetTargetInstance()
		{
			return target;
		}

		private void Render(bool instant = false)
		{
			if (Application.isPlaying && !instant)
			{
				renderQueued = true;
			}
			else if (!(targetCamera == null))
			{
				Rect rect = target.GetComponent<RectTransform>().rect;
				Rect source = new Rect((rect.width - TextureSize.x) / 2f, (rect.height - TextureSize.y) / 2f, (int)TextureSize.x, (int)TextureSize.y);
				RenderTexture.active = renderTexture;
				GL.Clear(clearDepth: false, clearColor: true, BackgroundColor);
				targetCamera.Render();
				texture2D.ReadPixels(source, 0, 0);
				texture2D.Apply();
				RenderTexture.active = null;
				renderQueued = false;
				if (this.OnRenderCallback != null)
				{
					this.OnRenderCallback(texture2D);
				}
			}
		}

		private void OnRectTransformDimensionsChange()
		{
			if (!Application.isPlaying)
			{
				UIObject3DTimer.AtEndOfFrame(delegate
				{
					if (_enabled)
					{
						renderPreferb();
					}
				}, this);
			}
		}

		private void Update()
		{
		}

		private void SetupTarget()
		{
			if (!initial)
			{
				if (ObjectPrefab == null)
				{
					if (Application.isPlaying)
					{
						UnityEngine.Debug.LogWarning("[UIObject3D] No prefab set.");
					}
					return;
				}
				_target = UnityEngine.Object.Instantiate(ObjectPrefab);
				_target.gameObject.layer = objectLayer;
			}
			UpdateTargetPositioningAndScale();
		}

		private void UpdateTargetPositioningAndScale()
		{
			if (_target == null)
			{
				return;
			}
			_target.name = "Target";
			if (!initial)
			{
				bool flag = false;
				Transform targetContainer = this.targetContainer;
				_target.transform.SetParent(targetContainer);
				Canvas component = _target.GetComponent<Canvas>();
				component.renderMode = RenderMode.ScreenSpaceCamera;
				component.worldCamera = targetCamera;
				if (flag)
				{
					_target.transform.localPosition = Vector3.zero;
					_target.transform.localScale = Vector3.one;
					_target.localRotation = Quaternion.identity;
				}
				else
				{
					_target.transform.localPosition = ObjectPrefab.localPosition;
					_target.transform.localScale = ObjectPrefab.localScale;
					_target.transform.localRotation = ObjectPrefab.localRotation;
				}
				SetLayerRecursively(_target.transform, objectLayer);
			}
			if (initial)
			{
				Vector3 localPosition = _target.transform.localPosition;
				_target.transform.position = Vector3.zero;
				_target.transform.localPosition = localPosition;
			}
			double num = 4.0 * Math.Tan((double)targetCamera.fieldOfView * 0.5 * 0.01745329238474369);
			double num2 = num * (double)targetCamera.aspect;
			this.targetContainer.transform.localScale = Vector3.one;
			initial = true;
			Transform transform = this.targetContainer.transform;
			Vector2 targetOffset = TargetOffset;
			float x = targetOffset.x;
			Vector2 targetOffset2 = TargetOffset;
			transform.localPosition = new Vector3(x, targetOffset2.y, 0f);
			this.targetContainer.transform.localEulerAngles = TargetRotation;
		}

		private void SetLayerRecursively(Transform transform, int layer)
		{
			transform.gameObject.layer = layer;
			IEnumerator enumerator = transform.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					Transform transform2 = (Transform)enumerator.Current;
					SetLayerRecursively(transform2, layer);
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
		}

		private void SetupTargetCamera()
		{
			if (_targetCamera == null)
			{
				if (ObjectPrefab == null)
				{
					return;
				}
				GameObject gameObject = new GameObject();
				gameObject.transform.SetParent(container);
				_targetCamera = gameObject.AddComponent<Camera>();
				_targetCamera.enabled = false;
				gameObject.AddComponent<UIObject3DCamera>();
			}
			UpdateTargetCameraPositioningEtc();
		}

		private void SetupCameraLight()
		{
			if (!(targetCamera == null))
			{
				if (_cameraLight == null)
				{
					_cameraLight = targetCamera.gameObject.AddComponent<Light>();
				}
				_cameraLight.enabled = EnableCameraLight;
				if (EnableCameraLight)
				{
					_cameraLight.gameObject.layer = objectLayer;
					_cameraLight.cullingMask = LayerMask.GetMask(LayerMask.LayerToName(objectLayer));
					_cameraLight.type = LightType.Point;
					_cameraLight.intensity = LightIntensity;
					_cameraLight.range = 200f;
					_cameraLight.color = LightColor;
				}
			}
		}

		private void UpdateTargetCameraPositioningEtc()
		{
			if (!(_targetCamera == null))
			{
				_targetCamera.transform.localPosition = Vector3.zero + new Vector3(0f, 0f, CameraDistance);
				_targetCamera.name = "Camera";
				_targetCamera.targetTexture = renderTexture;
				_targetCamera.clearFlags = CameraClearFlags.Color;
				_targetCamera.backgroundColor = Color.clear;
				_targetCamera.nearClipPlane = 0.1f;
				_targetCamera.farClipPlane = 1000f;
				_targetCamera.fieldOfView = CameraFOV;
				_targetCamera.gameObject.layer = objectLayer;
				_targetCamera.cullingMask = LayerMask.GetMask(LayerMask.LayerToName(objectLayer));
				_targetCamera.backgroundColor = BackgroundColor;
				SetupCameraLight();
			}
		}
	}
}
