using System;
using System.Collections.Generic;
using UnityEngine;

public class PublicControllerApi
{
	private static readonly int HAS_SIGN = 10402;

	private static readonly int HAS_PUBLISH = 10406;

	private static readonly int NOT_LOGIN = 10403;

	private static readonly int OK;

	private static readonly int HAS_APPLY = 10410;

	public static void AddFollow(string followUid, Action<Errcode> callback)
	{
		requestFollow(APIFactory.ADD_FOLLOW, followUid, callback);
		TotalGA.CountEventNumber_FB("follow_user", 1);
	}

	public static void RemoveFollow(string followUid, Action<Errcode> callback)
	{
		requestFollow(APIFactory.REMOVE_FOLLOW, followUid, callback);
	}

	public static void IsUploadInspiration(string workId, Action<bool> callback)
	{
		bool isRelease = false;
		URLRequest uRLRequest = APIFactory.create(APIFactory.IS_UPLOAD_INSPIRATION);
		uRLRequest.addParam("workId", workId);
		DataBroker.getJson(uRLRequest, delegate(WorkIsPublishJson detail)
		{
			if (detail.status == OK)
			{
				isRelease = detail.data.isRelease;
			}
			else
			{
				isRelease = false;
			}
			callback(isRelease);
		}, delegate
		{
			isRelease = false;
			callback(isRelease);
		});
	}

	private static void requestFollow(string url, string followUid, Action<Errcode> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(url);
		uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
		uRLRequest.addParam("followUid", followUid);
		uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else if (err.errorCode == NOT_LOGIN)
			{
				err.errorCode = Errcode.NOT_LOGIN;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}

	public static void RequestProductList(Action<ProductListJson> callback)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_PRODUCT_LIST);
		if (ApplicationModel.currentChannel != null || ApplicationModel.currentChannel != string.Empty)
		{
			uRLRequest.addParam("channelName", ApplicationModel.currentChannel);
		}
		uRLRequest.addParam("channelName", ApplicationModel.currentChannel);
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			uRLRequest.addParam("currency", "RMB");
		}
		if (ApplicationModel.currentChannel == "ios")
		{
			uRLRequest.addParam("platform", "ios");
		}
		else if (ApplicationModel.currentChannel == "googleplay")
		{
			uRLRequest.addParam("platform", "googleplay");
		}
		else
		{
			uRLRequest.addParam("platform", "weixin");
		}
		UnityEngine.Debug.Log("requestProductList is:" + uRLRequest.getUrl());
		DataBroker.getJson(uRLRequest, delegate(ProductListJson detail)
		{
			callback(detail);
		}, delegate
		{
			callback(null);
		});
	}

	public static void Sign(Action<bool> callback)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.SIGN);
		uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
		uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		bool isSiginSuccess;
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				isSiginSuccess = true;
			}
			else
			{
				isSiginSuccess = false;
			}
			callback(isSiginSuccess);
		}, delegate
		{
			isSiginSuccess = false;
			callback(isSiginSuccess);
		});
	}

	public static void GetExpiredTime(Action<VipExpired> callback)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.VIP_EXPIRED);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		DataBroker.getJson(uRLRequest, delegate(VipExpiredJson detail)
		{
			if (detail.status == 0)
			{
				callback(detail.data);
			}
			else
			{
				callback(null);
			}
		}, delegate
		{
			callback(null);
		});
	}

	public static void GetOriginImageDetailInfo(string imageId, Action<OriginDetail> callback)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.ORIGIN_DETAIL);
		uRLRequest.addParam("imageId", imageId);
		DataBroker.getJson(uRLRequest, delegate(OriginDetailJson detail)
		{
			if (detail.status == OK)
			{
				callback(detail.data);
			}
			else
			{
				callback(null);
			}
		}, delegate
		{
			callback(null);
		});
	}

	public static void PurchaseSuccess(CustomProduct product, string serialNumber, string platformSerialNumber, bool isComeFromImage)
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			PurchaseSuccessForLogin(product, serialNumber, platformSerialNumber, isComeFromImage);
		}
		else
		{
			PurchaseSuccessForDeviceId(product, serialNumber, platformSerialNumber, isComeFromImage);
		}
	}

	private static void PurchaseSuccessForLogin(CustomProduct product, string serialNumber, string platformSerialNumber, bool isComeFromImage)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.PURCHASE_SUCCESS);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addParam("productId", product.productId);
		uRLRequest.addParam("platform", product.platform);
		uRLRequest.addParam("serialNumber", serialNumber);
		uRLRequest.addParam("platformSerialNumber", platformSerialNumber);
		string text = PublicToolController.RandomString(32);
		uRLRequest.addParam("check", PublicUserInfoApi.GetUSerMd5HashValue(text, PublicUserInfoApi.GetUserUid()));
		uRLRequest.addParam("stamp", text);
		uRLRequest.addParam("timestamp", PublicToolController.UnixTimestampFromDateTime(DateTime.Now));
		uRLRequest.addParam("channel", ApplicationModel.currentChannel);
		if (isComeFromImage && ApplicationModel.currentEditedImg != null && ApplicationModel.currentEditedImg.imageId != null)
		{
			uRLRequest.addParam("imageId", ApplicationModel.currentEditedImg.imageId);
		}
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				PublicUserInfoApi.RefreshUserInfo();
			}
		}, delegate
		{
		});
	}

	private static void PurchaseSuccessForDeviceId(CustomProduct product, string serialNumber, string platformSerialNumber, bool isComeFromImage)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.PURCHASE_SUCCESS_DEVICEID);
		uRLRequest.addParam("deviceId", ApplicationModel.device_id);
		uRLRequest.addParam("productId", product.productId);
		uRLRequest.addParam("platform", product.platform);
		uRLRequest.addParam("serialNumber", serialNumber);
		uRLRequest.addParam("platformSerialNumber", platformSerialNumber);
		string text = PublicToolController.RandomString(32);
		uRLRequest.addParam("check", PublicUserInfoApi.GetUSerMd5HashValue(text, ApplicationModel.device_id));
		uRLRequest.addParam("stamp", text);
		uRLRequest.addParam("timestamp", PublicToolController.UnixTimestampFromDateTime(DateTime.Now));
		uRLRequest.addParam("channel", ApplicationModel.currentChannel);
		if (isComeFromImage && ApplicationModel.currentEditedImg != null && ApplicationModel.currentEditedImg.imageId != null)
		{
			uRLRequest.addParam("imageId", ApplicationModel.currentEditedImg.imageId);
		}
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				PublicUserInfoApi.RefreshUserInfo();
			}
		}, delegate
		{
		});
	}

	public static void ApplyAritst(string name, string contact, string intro, Action<Errcode> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.APPLY_ARTIST);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addParam("name", name);
		uRLRequest.addParam("contact", contact);
		uRLRequest.addParam("introduction", intro);
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else if (detail.status == HAS_APPLY)
			{
				err.errorCode = Errcode.HAS_APPLY;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}

	public static void GetPayChannelList(Action<Errcode, List<string>> callback, int version = 1)
	{
		UnityEngine.Debug.Log("GetPayChannelList....");
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_PAY_CHANNEL_LIST);
		uRLRequest.addParam("version", version);
		DataBroker.getJson(uRLRequest, delegate(PayChannelList detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void GetAdChannleList(Action<Errcode, List<string>> callback)
	{
		Errcode err = new Errcode();
		URLRequest request = APIFactory.create(APIFactory.GET_AD_CHANNEL);
		DataBroker.getJson(request, delegate(PayChannelList detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void ApplyArtist(string name, string contact, string introduction, Action<Errcode> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.APPLY_ARTIST);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addParam("name", name);
		uRLRequest.addParam("contact", contact);
		uRLRequest.addParam("introduction", introduction);
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detai)
		{
			if (detai.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}

	public static void GetGradientColorData(Action<string, Errcode> callback)
	{
		var detail = Resources.Load<TextAsset>("json/gradientColor_en").text;
		
		Errcode err = new Errcode();
		err.errorCode = Errcode.OK;
		callback(detail, err);
		// URLRequest uRLRequest = APIFactory.create(APIFactory.GET_CORLOR_DATA);
		// uRLRequest.addParam("category", "gradientColor");
		// DataBroker.getString(uRLRequest, delegate(string detail)
		// {
		// 	err.errorCode = Errcode.OK;
		// 	callback(detail, err);
		// }, delegate
		// {
		// 	err.errorCode = Errcode.OK;
		// 	callback(string.Empty, err);
		// });
	}

	public static void GetAlbumList(int page, int step, Action<Errcode, List<ArtistAlbumModel>> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_ALBUM_LIST);
		uRLRequest.addParam("page", page);
		uRLRequest.addParam("step", step);
		DataBroker.getJson(uRLRequest, delegate(ArtistAlbumModelListJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void GetAlbumImageList(int page, int step, string albumId, Action<Errcode, List<ResourceImg>> callbck)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_ALBUM_IMAGE_LIST);
		uRLRequest.addParam("page", page);
		uRLRequest.addParam("step", step);
		uRLRequest.addParam("albumId", albumId);
		DataBroker.getJson(uRLRequest, delegate(AlbumImageListJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callbck(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callbck(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callbck(err, null);
		});
	}

	public static void GetPixelArtList(int page, int step, Action<Errcode, List<PixelArt>> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_PIXEL_ART_LIST);
		uRLRequest.addParam("page", page);
		uRLRequest.addParam("step", step);
		DataBroker.getJson(uRLRequest, delegate(PixelArtListJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void DeleteWork(string workId)
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			Errcode err = new Errcode();
			URLRequest uRLRequest = APIFactory.create(APIFactory.DELETE_WORK);
			uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
			uRLRequest.addParam("workId", workId);
			uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
			DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
			{
				if (detail.status == 0)
				{
					err.errorCode = Errcode.OK;
				}
				else
				{
					err.errorCode = Errcode.FAIL;
				}
			}, delegate
			{
				err.errorCode = Errcode.FAIL;
			});
		}
	}

	public static void getDeviceInfo(string deviceId, Action<Errcode, DeviceVipInfo> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_DEVICE_INFO);
		uRLRequest.addParam("deviceId", deviceId);
		DataBroker.getJson(uRLRequest, delegate(DeviceVipInfoJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err, detail.data);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void mergeDeviceInfo(string deviceId, string sessionId, Action<Errcode> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.MERGE_VIP_INFO);
		uRLRequest.addParam("deviceId", deviceId);
		uRLRequest.addParam("sessionId", sessionId);
		string text = PublicToolController.RandomString(32);
		uRLRequest.addParam("check", PublicUserInfoApi.GetUSerMd5HashValue(text, deviceId));
		uRLRequest.addParam("stamp", text);
		uRLRequest.addParam("timestamp", PublicToolController.UnixTimestampFromDateTime(DateTime.Now));
		DataBroker.getJson(uRLRequest, delegate(StandardReturnCode detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}
}
