using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPnalePureController : MonoBehaviour
{
	private List<ColorTheme> colorThemes;

	private GameObject Desp;

	private GameObject colorBox;

	private int W_wrap = 1242;

	[SerializeField]
	private GameObject colorThemeWrap;

	[SerializeField]
	private GameObject colorItem;

	[SerializeField]
	private int ColorCLCount = 5;

	private void Awake()
	{
		UpdateColorPanel(ApplicationModel.ColorThemesPure);
	}

	private void UpdateColorPanel(List<ColorTheme> colorThemes)
	{
		int num = 0;
		foreach (ColorTheme colorTheme in colorThemes)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(colorThemeWrap, base.transform, worldPositionStays: false);
			gameObject.transform.localPosition += Vector3.right * 835f * num;
			Transform transform = gameObject.transform.Find("ColorBox").transform;
			for (int i = 0; i < colorTheme.Colors.Count; i++)
			{
				if (i > 8)
				{
					break;
				}
				GameObject gameObject2 = UnityEngine.Object.Instantiate(colorItem, transform, worldPositionStays: false);
				gameObject2.transform.localPosition += Vector3.right * 150f * (i % ColorCLCount);
				gameObject2.transform.localPosition += Vector3.down * 135f * (i / ColorCLCount);
				if (i / ColorCLCount % 2 == 1)
				{
					gameObject2.transform.localPosition += Vector3.right * 75f;
				}
				Color col = default(Color);
				ColorUtility.TryParseHtmlString(colorTheme.Colors[i], out col);
				gameObject2.transform.GetChild(0).GetComponent<Image>().color = col;
				gameObject2.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate
				{
					onClick(col);
				});
				if (i == 0 && num == 0)
				{
					gameObject2.transform.Find("Selected").gameObject.SetActive(value: true);
				}
			}
			num++;
		}
	}

	private void onClick(Color col)
	{
		WorkShopManagerController.instance.ChangePencilColorTo(col);
	}

	private void Update()
	{
	}
}
