using System.Collections;
using System.IO;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class PixelDraw : MonoBehaviour
{
	private RectTransform workPanel;

	private float max_scale = 195f;

	private float max_keep_scale = 110f;

	private float current_scale = 1f;

	private float animate_duration = 0.5f;

	private RectTransform image_rect;

	private RawImage imagePanel;

	private Material material;

	private Texture2D colorMap;

	private Texture2D cntMap;

	private Texture2D mainTex;

	private Transform container;

	private PixleEditedImg pixleEditedImg;

	private Vector2 oriAnchor;

	private bool hasInitial;

	private float base_scale => Mathf.Min(imagePanel.rectTransform.rect.width, imagePanel.rectTransform.rect.height) * 1f / (float)mainTex.width;

	public PixelDraw(Transform container, PixleEditedImg pixleEditedImg)
	{
		this.pixleEditedImg = pixleEditedImg;
		this.container = container;
		workPanel = container.gameObject.GetComponent<RectTransform>();
		InitWorkPanel(workPanel);
		imagePanel = container.GetComponent<RawImage>();
		image_rect = imagePanel.rectTransform;
		InitMaterial();
		LoadTexture(pixleEditedImg);
		hasInitial = true;
	}

	private void InitWorkPanel(RectTransform workPanel)
	{
		workPanel.localScale = new Vector3(1f, 1f, 1f);
		workPanel.anchoredPosition = new Vector3(0f, 0f, 0f);
	}

	private void InitMaterial()
	{
		material = new Material(Shader.Find("Custom/PixeArt"));
		imagePanel.material = material;
	}

	public Material createMat()
	{
		material = new Material(Shader.Find("Custom/PixeArt"));
		material.CopyPropertiesFromMaterial(imagePanel.material);
		return material;
	}

	public void LoadTexture(PixleEditedImg pixleEditedImg)
	{
		if (pixleEditedImg != null)
		{
			material.SetTexture("_FontTex", Resources.Load<Texture2D>("Images/pixelArt/fontTexture"));
			material.SetTexture("_FrameTex", Resources.Load<Texture2D>("Images/pixelArt/outline"));
			URLRequest request = new URLRequest(pixleEditedImg.colorUrl);
			DataBroker.getTexture2D(request, delegate(Texture2D tex)
			{
				material = createMat();
				material.SetTexture("_ColorIndex", tex);
				imagePanel.material = material;
				UnityEngine.Debug.Log("colorUrl:" + pixleEditedImg.colorUrl);
			}, delegate
			{
				UnityEngine.Debug.Log("load colorUrl fail!!!");
			});
			if (pixleEditedImg.HasEdited())
			{
				UnityEngine.Debug.Log("load cntmap from local!");
				cntMap = PublicToolController.GetTexture2dFromLocalByPath(pixleEditedImg.imageId + "_cntMap");
				MainThreadDispatcher.StartCoroutine(checkEmptyColor());
			}
			else
			{
				UnityEngine.Debug.Log("countUrl:" + pixleEditedImg.countUrl);
				request = new URLRequest(pixleEditedImg.countUrl);
				DataBroker.getTexture2D(request, delegate(Texture2D texture)
				{
					UnityEngine.Debug.Log("countUrl  has load!");
					cntMap = texture;
					MainThreadDispatcher.StartCoroutine(checkEmptyColor());
				}, delegate
				{
					UnityEngine.Debug.Log("load countUrl fail!");
				});
			}
			URLRequest request2 = new URLRequest(pixleEditedImg.baseUrl);
			DataBroker.getTexture2D(request2, delegate(Texture2D tex)
			{
				mainTex = tex;
				UnityEngine.Debug.Log("baseUrl:" + pixleEditedImg.baseUrl);
				UnityEngine.Debug.Log("imagePanel:" + imagePanel.rectTransform.rect + " " + mainTex.width);
				material = createMat();
				material.SetTexture("_MainTex", mainTex);
				if (pixleEditedImg.HasEdited())
				{
					colorMap = PublicToolController.GetTexture2dFromLocalByPath(pixleEditedImg.imageId + "_colorMap");
				}
				else
				{
					colorMap = new Texture2D(mainTex.width, mainTex.height, TextureFormat.RGBA32, mipChain: false);
					for (int i = 0; i < mainTex.width; i++)
					{
						for (int j = 0; j < mainTex.height; j++)
						{
							colorMap.SetPixel(i, j, Color.clear);
						}
					}
					colorMap.Apply();
				}
				colorMap.filterMode = FilterMode.Point;
				colorMap.wrapMode = TextureWrapMode.Clamp;
				material.SetTexture("_ColorMap", colorMap);
				imagePanel.material = material;
			}, delegate
			{
				UnityEngine.Debug.Log("load color url fail!!!");
			});
		}
	}

	public IEnumerator checkEmptyColor()
	{
		yield return new WaitForSeconds(0.5f);
		if (!(cntMap != null))
		{
			yield break;
		}
		int count = getCount(cntMap, 0, 0);
		int num = 0;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				if (num <= count && num > 0)
				{
					int count2 = getCount(cntMap, j, i);
					UnityEngine.Debug.Log("index:" + num + " xIndex:" + j + " yIndex:" + i + " cnt:" + count2);
					if (count2 == 0)
					{
						UnityEngine.Debug.Log("disable");
						MessageBroker.Default.Publish(new PixelColorButtonController.ColorIndex
						{
							id = num,
							action = 2
						});
					}
				}
				num++;
			}
		}
	}

	protected int getCount(Texture2D cnt, int x, int y)
	{
		Color32 color = cnt.GetPixel(x, cnt.height - y - 1);
		return color.r + 255 * color.g;
	}

	public void setColor(Vector2 pos, int colorIndex)
	{
		if (current_scale <= base_scale || current_scale < 16f)
		{
			UnityEngine.Debug.Log("rect:" + workPanel.rect);
			UnityEngine.Debug.Log("current_scale1:" + current_scale);
			scalePanel(pos, 15f / base_scale, animation: true);
			UnityEngine.Debug.Log("current_scale2:" + current_scale);
			return;
		}
		RectTransformUtility.ScreenPointToLocalPointInRectangle(imagePanel.rectTransform, pos, Camera.main, out Vector2 localPoint);
		localPoint -= new Vector2(imagePanel.rectTransform.rect.x, imagePanel.rectTransform.rect.y);
		localPoint /= base_scale;
		Color32 color = mainTex.GetPixel(Mathf.FloorToInt(localPoint.x), Mathf.FloorToInt(localPoint.y));
		UnityEngine.Debug.Log("localPos:" + localPoint + " colorIndex:" + colorIndex + color + " base_scale:" + base_scale);
		if (color.a == byte.MaxValue)
		{
			UnityEngine.Debug.Log("mainColor:" + color.a);
			return;
		}
		Color32 color2 = colorMap.GetPixel(Mathf.FloorToInt(localPoint.x), Mathf.FloorToInt(localPoint.y));
		Color32 color3 = default(Color32);
		color3.r = (byte)colorIndex;
		color3.g = (byte)((color.a == colorIndex) ? 1 : 0);
		color3.a = byte.MaxValue;
		if (!color2.Equals(color3))
		{
			if (color.a == colorIndex || color2.g > 0)
			{
				updateColorCount(Mathf.FloorToInt(localPoint.x), Mathf.FloorToInt(localPoint.y), colorIndex);
			}
			UnityEngine.Debug.Log("set color:" + Mathf.FloorToInt(localPoint.x) + "," + Mathf.FloorToInt(localPoint.y) + " color:" + color3);
			colorMap.SetPixel(Mathf.FloorToInt(localPoint.x), Mathf.FloorToInt(localPoint.y), color3);
			colorMap.Apply();
			material.SetTexture("_ColorMap", colorMap);
		}
	}

	private void updateColorCount(int x, int y, int colorIndex)
	{
		Color32 color = mainTex.GetPixel(x, y);
		if (color.a == 0 || color.a == byte.MaxValue)
		{
			return;
		}
		int a = color.a;
		int num = a % 8;
		int num2 = 7 - a / 8;
		Color32 color2 = cntMap.GetPixel(num, num2);
		UnityEngine.Debug.Log("cnt:" + color2);
		int num3 = color2.r + 255 * color2.g;
		UnityEngine.Debug.Log(num + "," + num2 + color + " colorIndex:" + colorIndex + " total:" + num3);
		if (color.a == colorIndex)
		{
			num3--;
			if (num3 == 0)
			{
				MessageBroker.Default.Publish(new PixelColorButtonController.ColorIndex
				{
					id = a,
					action = 2
				});
			}
		}
		else
		{
			if (num3 == 0)
			{
				MessageBroker.Default.Publish(new PixelColorButtonController.ColorIndex
				{
					id = a,
					action = 3
				});
			}
			num3++;
		}
		num3 = Mathf.Clamp(num3, 0, 65025);
		color2.r = (byte)(num3 % 255);
		color2.g = (byte)(num3 / 255);
		UnityEngine.Debug.Log(num + "," + num2 + " save total:" + num3 + " cnt:" + color2);
		cntMap.SetPixel(num, num2, color2);
		cntMap.Apply();
	}

	public void scrollScalePanel(Vector2 pos, float scale)
	{
		UnityEngine.Debug.Log("scrollScalePanel.imagePanelScale = " + imagePanel.rectTransform.sizeDelta);
		if (RectTransformUtility.RectangleContainsScreenPoint(imagePanel.rectTransform, UnityEngine.Input.mousePosition, Camera.main))
		{
			Vector3 localScale = workPanel.localScale;
			float a = (localScale.x + scale) * base_scale;
			a = Mathf.Min(a, 64f);
			a = Mathf.Max(a, 0.8f);
			if (a > 16f)
			{
				a = Mathf.Round(a);
			}
			a /= base_scale;
			Vector2 a2 = new Vector2(workPanel.rect.width, workPanel.rect.height);
			Vector3 localScale2 = workPanel.localScale;
			a2 = (localScale2.x - a) * (pos - a2 * 0.5f);
			float x = a;
			float y = a;
			Vector3 localScale3 = workPanel.localScale;
			Vector3 localScale4 = new Vector3(x, y, localScale3.z);
			workPanel.localScale = localScale4;
			current_scale = a * base_scale;
			UnityEngine.Debug.Log("scrollScalePanel.current_scale = " + current_scale);
			material.SetFloat("_TexScale", a * base_scale);
		}
	}

	public void setChooseColor(int color)
	{
		if (material != null)
		{
			material.SetInt("_ChooseColor", color);
		}
	}

	public void onScaleEnd()
	{
		if (current_scale > max_keep_scale)
		{
			LeanTween.scale(workPanel, Vector3.one * (max_keep_scale / base_scale), animate_duration);
			LeanTween.moveLocal(workPanel.gameObject, workPanel.anchoredPosition * (max_keep_scale / current_scale), animate_duration);
			material.SetFloat("_TexScale", max_keep_scale);
		}
		else if (current_scale < base_scale)
		{
			UnityEngine.Debug.Log("scroll to oringal:" + current_scale);
			LeanTween.scale(workPanel, Vector3.one, animate_duration);
			LeanTween.moveLocal(workPanel.gameObject, current_scale * oriAnchor, animate_duration);
			material.SetFloat("_TexScale", 8f);
		}
	}

	public void scalePanel(Vector2 center, float deltascale, bool animation = false)
	{
		float num = 0f;
		if (deltascale == 0f)
		{
			return;
		}
		UnityEngine.Debug.Log("scalePanel.localScale + base_scale + deltascale = " + workPanel.localScale + "  " + base_scale + "  " + deltascale);
		Vector3 localScale = workPanel.localScale;
		num = (localScale.x + deltascale) * base_scale;
		num = Mathf.Min(num, max_scale);
		num /= base_scale;
		num = Mathf.Max(num, 0.1f);
		float x = num;
		float y = num;
		Vector3 localScale2 = workPanel.localScale;
		Vector3 vector = new Vector3(x, y, localScale2.z);
		if ((double)vector.x >= 0.2)
		{
			RectTransformUtility.ScreenPointToLocalPointInRectangle(imagePanel.rectTransform, center, Camera.main, out Vector2 localPoint);
			if (animation)
			{
				LeanTween.moveLocal(workPanel.gameObject, workPanel.anchoredPosition + localPoint * (0f - deltascale), animate_duration);
			}
			else
			{
				workPanel.anchoredPosition += localPoint * (0f - deltascale);
			}
		}
		if ((double)vector.x >= 0.1)
		{
			current_scale = base_scale * vector.x;
			UnityEngine.Debug.Log("scalePanel.localScale.current_scale = " + current_scale);
			if ((double)vector.x > 0.9)
			{
				material.SetFloat("_TexScale", current_scale);
			}
			else
			{
				material.SetFloat("_TexScale", 8f);
			}
			if (animation)
			{
				LeanTween.scale(workPanel, vector, animate_duration);
			}
			else
			{
				workPanel.localScale = vector;
			}
			UnityEngine.Debug.Log("center:" + center + " current_scale:" + current_scale + " factor:" + num + " localScale:" + vector);
		}
	}

	public void movePanel(Vector2 deltaPosition)
	{
		UnityEngine.Debug.Log("movePanel:" + deltaPosition);
		workPanel.anchoredPosition += deltaPosition / workPanel.GetComponentInParent<CanvasScreenAutoFix>().scaleRatio;
	}

	public void onMoveEnd(Vector2 deltaPosition)
	{
		Vector3 localScale = workPanel.localScale;
		if (localScale.x > 1f)
		{
			RectTransform component = container.GetComponent<RectTransform>();
			UnityEngine.Debug.Log("image_rect:" + image_rect.rect + " " + component.rect + " " + oriAnchor);
			Vector3 localScale2 = workPanel.localScale;
			float x = localScale2.x * image_rect.rect.width - component.rect.width;
			Vector3 localScale3 = workPanel.localScale;
			Vector2 vector = new Vector2(x, localScale3.y * image_rect.rect.height - component.rect.height) * 0.5f;
			Vector2 anchoredPosition = workPanel.anchoredPosition;
			Vector2 vector2 = default(Vector2);
			vector2.x = Mathf.Clamp(anchoredPosition.x, 0f - vector.x, vector.x);
			Vector2 anchoredPosition2 = workPanel.anchoredPosition;
			vector2.y = Mathf.Clamp(anchoredPosition2.y, 0f - vector.y, vector.y);
			UnityEngine.Debug.Log("rect:" + vector + "anchoredPosition:" + workPanel.anchoredPosition + " newLocalPos:" + vector2);
			LeanTween.moveLocal(workPanel.gameObject, vector2, animate_duration);
		}
		else
		{
			LeanTween.scale(workPanel, Vector3.one, animate_duration);
			GameObject gameObject = workPanel.gameObject;
			Vector3 localScale4 = workPanel.localScale;
			LeanTween.moveLocal(gameObject, localScale4.x * oriAnchor, animate_duration);
		}
	}

	public void SaveTexture()
	{
		if (colorMap != null && cntMap != null && pixleEditedImg != null)
		{
			PublicToolController.FileCreatorBytes(colorMap.EncodeToPNG(), pixleEditedImg.imageId + "_colorMap", isStoreChange: true);
			PublicToolController.FileCreatorBytes(cntMap.EncodeToPNG(), pixleEditedImg.imageId + "_cntMap", isStoreChange: true);
			SaveFinalImg();
		}
		destoryTex();
	}

	private void SaveFinalImg()
	{
		RenderTexture renderTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
		material.SetFloat("_TexScale", 1f);
		Graphics.Blit(mainTex, renderTexture, material);
		RenderTexture active = RenderTexture.active;
		Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
		texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture2D.Apply();
		RenderTexture.active = renderTexture;
		try
		{
			PublicToolController.FileCreatorBytes(texture2D.EncodeToPNG(), pixleEditedImg.imageId + "_ok", isStoreChange: true);
		}
		catch (IOException message)
		{
			UnityEngine.Debug.Log(message);
		}
	}

	public void destoryTex()
	{
		workPanel.anchoredPosition = oriAnchor;
		material.SetTexture("_ColorIndex", null);
		material.SetTexture("_MainTex", null);
		material.SetTexture("_ColorMap", null);
		UnityEngine.Object.DestroyImmediate(colorMap);
		UnityEngine.Object.DestroyImmediate(cntMap);
	}
}
