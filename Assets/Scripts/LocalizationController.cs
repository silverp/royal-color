using Mgl;
using UnityEngine;
using UnityEngine.UI;

public class LocalizationController : MonoBehaviour
{
	private I18n i18n = I18n.Instance;

	private void Awake()
	{
		AssigiEvents();
	}

	private void Start()
	{
		ChangeText();
	}

	private void AssigiEvents()
	{
		DragEventProxy.OnChangeText += ChangeText;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnChangeText -= ChangeText;
	}

	private void ChangeText()
	{
		Text component = GetComponent<Text>();
		string text = component.text;
		string text2 = i18n.__(text);
		if (text2 != null && component != null)
		{
			component.text = text2;
		}
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}
}
