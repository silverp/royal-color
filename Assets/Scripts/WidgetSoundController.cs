using UnityEngine;

public class WidgetSoundController : MonoBehaviour
{
	public static WidgetSoundController instance;

	private AudioSource audioSource;

	[SerializeField]
	private AudioClip btnClick;

	[SerializeField]
	private AudioClip btnForbid;

	private void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
		audioSource = GetComponent<AudioSource>();
	}

	public void playBtnClick()
	{
		audioSource.PlayOneShot(btnClick, 1f);
	}

	public void playBtnForbid()
	{
		audioSource.PlayOneShot(btnForbid, 1f);
	}
}
