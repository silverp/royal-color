using Newtonsoft.Json;
using UnityEngine;

public class LikesPanelController : MonoBehaviour
{
	[SerializeField]
	private GameObject LikeScrollView;

	[SerializeField]
	private GameObject EmptyPanel;

	[SerializeField] private GameObject MyworkCanvas;

	private GameObject topCanvas;

	private string canvasName;

	private void Awake()
	{
		topCanvas = MyworkCanvas;//PublicToolController.FindParentWithTag(base.gameObject, "MyworkCanvas");
		canvasName = topCanvas.name;
	}

	private void RequestCallBack(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
			if (workCountJson.status == 0)
			{
				int count = workCountJson.data.count;
				if (count > 0)
				{
					LikeScrollView.SetActive(value: true);
					EmptyPanel.SetActive(value: false);
				}
				else
				{
					LikeScrollView.SetActive(value: false);
					EmptyPanel.SetActive(value: true);
				}
			}
			else if (workCountJson.status == 10403)
			{
				DragEventProxy.Instance.SendOffLineMsg(canvasName, isOffline: true);
			}
			else
			{
				LikeScrollView.SetActive(value: false);
				EmptyPanel.SetActive(value: true);
			}
		}
		else
		{
			LikeScrollView.SetActive(value: false);
			EmptyPanel.SetActive(value: true);
		}
	}

	private void OnEnable()
	{
		if (canvasName == "MyworkCanvas" && ApplicationModel.userInfo != null)
		{
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/user/getLikeListCount?uid=" + ApplicationModel.userInfo.Uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + true, delegate(string json, bool isSuccess)
			{
				RequestCallBack(json, isSuccess);
			});
		}
		else if (canvasName == "OtherWorkCanvas")
		{
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/user/getLikeListCount?uid=" + ApplicationModel.otherUserInfo.Uid + "&sessionId=" + PublicUserInfoApi.GetSessionId() + "&isMine=" + false, delegate(string json, bool isSuccess)
			{
				RequestCallBack(json, isSuccess);
			});
			EmptyPanel.SetActive(value: false);
		}
		if (canvasName == "MyworkCanvas" && ApplicationModel.userInfo == null)
		{
			EmptyPanel.SetActive(value: true);
			LikeScrollView.SetActive(value: false);
		}
	}
}
