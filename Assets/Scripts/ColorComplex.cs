using Newtonsoft.Json;
using UnityEngine;

public class ColorComplex
{
	public bool isGradient;

	public Color color;

	public Vector2 gradientColorCoordinate;

	[JsonConstructor]
	public ColorComplex(bool isGradient, Color color, Vector2 gradientColorCoordinate)
	{
		this.isGradient = isGradient;
		this.color = color;
		this.gradientColorCoordinate = gradientColorCoordinate;
	}

	public bool IsEqual(ColorComplex colorComplex)
	{
		if (isGradient == colorComplex.isGradient && color == colorComplex.color && gradientColorCoordinate.Equals(colorComplex.gradientColorCoordinate))
		{
			return true;
		}
		return false;
	}
}
