using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PublicImgaeApi
{
	public static void GetInspirationImglist(Action<List<ColoredImage>> callback, int page, int step)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.INSPIRATION_IMGLIST);
		uRLRequest.AddPageAndStepParams(page, step);
		if (ApplicationModel.userInfo != null)
		{
			uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		}
		DataBroker.getJson(uRLRequest, delegate(ColoredImageJson detail)
		{
			if (detail.status == 0)
			{
				callback(detail.data);
			}
			else
			{
				callback(null);
			}
		}, delegate
		{
			callback(null);
		});
	}

	public static void GetSingleInspirationByWorkId(string workId, Action<Errcode, ColoredImage> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_SINGLE_INSPIRATION);
		uRLRequest.addParam("workId", workId);
		DataBroker.getJson(uRLRequest, delegate(SingleColoredImageJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void GetCateImglist(Action<Errcode, List<ResourceImg>> callback, int cid, int page, int step)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = GeturlRequestByCid(cid);
		uRLRequest.AddPageAndStepParams(page, step);
		UnityEngine.Debug.Log("GetCateImglist url is:" + uRLRequest.getUrl());
		DataBroker.getJson(uRLRequest, delegate(CateDetailJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data.imgList);
				UnityEngine.Debug.Log("GetCateImglist is ok :" + JsonConvert.SerializeObject(detail.data));
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
				UnityEngine.Debug.Log("GetCateImglist is err :");
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
			UnityEngine.Debug.Log("GetCateImglist is err");
		});
	}

	public static void GetLikesData(Action<List<LikesWork>, Errcode> callback, int page, int step, bool isMine)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.LIKES_IMGLIST);
		uRLRequest.AddPageAndStepParams(page, step);
		uRLRequest.addParam("isMine", isMine.ToString());
		if (isMine)
		{
			if (ApplicationModel.userInfo != null)
			{
				uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			}
		}
		else
		{
			uRLRequest.addParam("uid", ApplicationModel.otherUserInfo.Uid);
			UnityEngine.Debug.Log("uid is :" + ApplicationModel.otherUserInfo.Uid);
		}
		if (ApplicationModel.userInfo != null)
		{
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		}
		Errcode errcode = new Errcode();
		DataBroker.getJson(uRLRequest, delegate(LikeWorkListJson detail)
		{
			if (detail.status == 0)
			{
				errcode.errorCode = 0;
				callback(detail.data, errcode);
			}
			else if (detail.status == 10403)
			{
				errcode.errorCode = 2;
				callback(detail.data, errcode);
			}
			else
			{
				errcode.errorCode = 1;
				callback(detail.data, errcode);
			}
		}, delegate
		{
			errcode.errorCode = 1;
			callback(null, errcode);
		});
	}

	private static URLRequest GeturlRequestByCid(int cid)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.SINGLE_CATE_INFO);
		uRLRequest.addParam("cid", cid);
		return uRLRequest;
	}

	private static void MergeColoredImageResource(string json, bool isSuccess, Action callback)
	{
		if (isSuccess)
		{
			ColoredImageJson coloredImageJson = JsonConvert.DeserializeObject<ColoredImageJson>(json);
			if (coloredImageJson.status == 0)
			{
				ApplicationModel.ColoredImgList = coloredImageJson.data;
			}
		}
		callback();
	}

	public static EditedImg GetEditedImgByLikesWork(LikesWork work)
	{
		if (ApplicationModel.EditedResourceImgList == null)
		{
			ApplicationModel.EditedResourceImgList = new List<EditedImg>();
		}
		if (ApplicationModel.EditedResourceImgList.Count == 0)
		{
			ApplicationModel.EditedResourceImgList.Add(new EditedImg(work.img.imageId, isRemote: false));
			return ApplicationModel.EditedResourceImgList[0];
		}
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.imageId == work.img.imageId)
			{
				return editedResourceImg;
			}
		}
		ApplicationModel.EditedResourceImgList.Add(new EditedImg(work.img.imageId, isRemote: false));
		int count = ApplicationModel.EditedResourceImgList.Count;
		return ApplicationModel.EditedResourceImgList[count - 1];
	}

	public static void SetLatesEditedImgFirst(EditedImg imgItem)
	{
		int count = ApplicationModel.EditedResourceImgList.Count;
		int index = 0;
		for (int i = 0; i < count; i++)
		{
			if (ApplicationModel.EditedResourceImgList[i] == imgItem)
			{
				index = i;
				break;
			}
		}
		ApplicationModel.EditedResourceImgList.RemoveAt(index);
		ApplicationModel.EditedResourceImgList.Insert(0, imgItem);
	}

	public static EditedImg GetImgFromEditedList(MyselfWork work)
	{
		if (ApplicationModel.EditedResourceImgList == null)
		{
			ApplicationModel.EditedResourceImgList = new List<EditedImg>();
		}
		if (ApplicationModel.EditedResourceImgList.Count == 0)
		{
			ApplicationModel.EditedResourceImgList.Add(new EditedImg(work.img.imageId, isRemote: false));
			return ApplicationModel.EditedResourceImgList[0];
		}
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.imageId == work.img.imageId)
			{
				return editedResourceImg;
			}
		}
		ApplicationModel.EditedResourceImgList.Add(new EditedImg(work.img.imageId, isRemote: false));
		int count = ApplicationModel.EditedResourceImgList.Count;
		return ApplicationModel.EditedResourceImgList[count - 1];
	}

	public static bool IsShowRedPoint(CateResource oldCate, CateResource newCate)
	{
		if (oldCate.updateTime != 0)
		{
			int num = DateTime.Compare(UnixTimeStampToDateTime(oldCate.updateTime), UnixTimeStampToDateTime(newCate.updateTime));
			if (num < 0)
			{
				return true;
			}
		}
		return false;
	}

	public static bool IsShowRedPoint(CateTimeStamp cateTimeStamp, CateResource newCate)
	{
		if (cateTimeStamp.stamp != 0)
		{
			int num = DateTime.Compare(UnixTimeStampToDateTime(cateTimeStamp.stamp), UnixTimeStampToDateTime(newCate.updateTime));
			if (num < 0)
			{
				return true;
			}
		}
		return false;
	}

	public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
	{
		return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(unixTimeStamp / 1000.0).ToLocalTime();
	}

	public static bool IsHasLocalFinalImg(EditedImg img)
	{
		string path = Application.persistentDataPath + "/" + img.imageId + "_ok.txt";
		if (File.Exists(path))
		{
			return true;
		}
		return false;
	}

	public static ResourceImg GetResourceImgFromColoredimg(string imageId)
	{
		UnityEngine.Debug.Log("coloredImg imageId is:" + imageId);
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.imgList != null && cateResource.imgList.Count != 0)
			{
				foreach (ResourceImg img in cateResource.imgList)
				{
					if (img.imageId == imageId)
					{
						return img;
					}
				}
			}
		}
		return null;
	}

	public static PixelArt GetPixelArtByImageId(string imageId)
	{
		if (ApplicationModel.pixelArtList != null && ApplicationModel.pixelArtList.Count != 0)
		{
			foreach (PixelArt pixelArt in ApplicationModel.pixelArtList)
			{
				if (pixelArt.imageId == imageId)
				{
					return pixelArt;
				}
			}
		}
		return null;
	}

	public static PixleEditedImg GetEditedPixelImgFromEditedList(string imageId)
	{
		List<PixleEditedImg> pixleEditedImgList = ApplicationModel.PixleEditedImgList;
		if (pixleEditedImgList != null)
		{
			foreach (PixleEditedImg item in pixleEditedImgList)
			{
				if (item.imageId == imageId)
				{
					return item;
				}
			}
		}
		else
		{
			ApplicationModel.PixleEditedImgList = new List<PixleEditedImg>();
		}
		return null;
	}

	public static PixleEditedImg GetEditedPixelImgFromEditedList(PixelArt pixelArt, bool isCreate = false)
	{
		List<PixleEditedImg> pixleEditedImgList = ApplicationModel.PixleEditedImgList;
		if (pixleEditedImgList != null)
		{
			foreach (PixleEditedImg item in pixleEditedImgList)
			{
				if (item.imageId == pixelArt.imageId)
				{
					return item;
				}
			}
		}
		else
		{
			ApplicationModel.PixleEditedImgList = new List<PixleEditedImg>();
		}
		if (isCreate)
		{
			PixleEditedImg pixleEditedImg = new PixleEditedImg(pixelArt.imageId, pixelArt.baseUrl, pixelArt.colorUrl, pixelArt.countUrl, isRemote: false);
			ApplicationModel.PixleEditedImgList.Add(pixleEditedImg);
			return pixleEditedImg;
		}
		return null;
	}

	public static EditedImg GetEditedImgFromEditedList(string imgId, bool isCreate = false)
	{
		List<EditedImg> editedResourceImgList = ApplicationModel.EditedResourceImgList;
		if (editedResourceImgList != null && editedResourceImgList.Count != 0)
		{
			foreach (EditedImg item in editedResourceImgList)
			{
				if (item.imageId == imgId)
				{
					return item;
				}
			}
		}
		else
		{
			ApplicationModel.EditedResourceImgList = new List<EditedImg>();
		}
		if (isCreate)
		{
			EditedImg editedImg = new EditedImg(imgId, isRemote: false);
			ApplicationModel.EditedResourceImgList.Add(editedImg);
			return editedImg;
		}
		return null;
	}

	public static CateResource GetCateByCid(int cid)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return cateResource;
			}
		}
		return null;
	}

	public static EditedImg GetEditedImgByImageId(string imageId)
	{
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.imageId == imageId)
			{
				return editedResourceImg;
			}
		}
		return null;
	}

	public static bool IsExistEditedImgByImageId(string imageId)
	{
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			if (editedResourceImg.imageId == imageId)
			{
				return true;
			}
		}
		return false;
	}

	public static Texture2D GetVipIcon(int level)
	{
		string str = "Images/userinfo/";
		switch (level)
		{
		case 0:
			str += "vip_lighten_gray";
			break;
		case 1:
			str += "vip_lighten_blue";
			break;
		case 2:
			str += "vip_lighten_yellow";
			break;
		case 3:
			str += "vip_lighten_red";
			break;
		default:
			str += "vip_lighten_gray";
			break;
		}
		return Resources.Load<Texture2D>(str);
	}

	public static void InsertListByTime(List<ColoredImage> list, List<ColoredImage> originList)
	{
		foreach (ColoredImage item in list)
		{
		}
	}

	public static void GetPixelOriginInfo(string imageId, Action<Errcode, PixelArt> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_PIXEL_ORIGIN_DETAIL);
		uRLRequest.addParam("imageId", imageId);
		DataBroker.getJson(uRLRequest, delegate(PixelArtJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}

	public static void GetPixelEditedInfo(string workId, Action<Errcode, PixelEditedInfo> callbck)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_PIXEL_EDITED_INFO);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("workId", workId);
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		DataBroker.getJson(uRLRequest, delegate(PixelEditedInfoJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callbck(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callbck(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callbck(err, null);
		});
	}

	public static List<BaseEditedImg> GetMyEditedNormalAndPixelList()
	{
		List<BaseEditedImg> list = new List<BaseEditedImg>();
		foreach (EditedImg editedResourceImg in ApplicationModel.EditedResourceImgList)
		{
			list.Add(editedResourceImg);
		}
		foreach (PixleEditedImg pixleEditedImg in ApplicationModel.PixleEditedImgList)
		{
			list.Add(pixleEditedImg);
		}
		UnityEngine.Debug.Log("before list is：" + JsonConvert.SerializeObject(list));
		UnityEngine.Debug.Log("GetMyEditedNormalAndPixelList PixleEditedImgList count is:" + ApplicationModel.PixleEditedImgList.Count);
		list.Sort();
		UnityEngine.Debug.Log("after list is：" + JsonConvert.SerializeObject(list));
		return list;
	}
}
