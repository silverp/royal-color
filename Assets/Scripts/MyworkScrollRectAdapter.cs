using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyworkScrollRectAdapter : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class DoubleImageModel
	{
		public BaseEditedImg img;

		public int index;

		public DoubleImageModel(BaseEditedImg img, int index)
		{
			this.img = img;
			this.index = index;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<DoubleImageModel> _Data;

		private MyWorkItemsViewHolder myWorkItemsViewHolder;

		public RectTransform root;

		private Texture2D loadingTex = Resources.Load<Texture2D>("Images/inspiration/prefabImg");

		public MyWorkScrollRectItemsAdapter(List<DoubleImageModel> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		public void Remove(int index)
		{
			UnityEngine.Debug.Log("remove item....");
			_Data.RemoveAt(index);
			ChangeItemCountTo(_Data.Count);
		}

		public void DestoryAssets(string name)
		{
			if (root.GetComponent<RawImageDataController>().downloadStatus == 1)
			{
				UnityEngine.Object.DestroyImmediate(root.Find("Image").GetComponent<RawImage>().texture);
			}
		}

		public void SetImage(BaseEditedImg img, int index)
		{
			if (img != null)
			{
				if (!img.isRemote && img.HasEdited())
				{
					Texture2D tex2dFromImg = GetTex2dFromImg(img);
					SetFrommImgData();
					root.Find("Image").GetComponent<RawImage>().texture = tex2dFromImg;
					BaseEditedImg baseEditedImg = img;
					string thumb = baseEditedImg.thumb;
					callback(tex2dFromImg, baseEditedImg, thumb);
				}
				else
				{
					root.Find("Image").GetComponent<RawImage>().texture = loadingTex;
					root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
					string requestUrl = img.thumb;
					NetLoadcontroller.Instance.RequestImg(img.thumb, delegate(Texture2D tex)
					{
						callback(tex, img, requestUrl);
					});
				}
				LikeBtnControl(img);
				Transform transform = root.Find("BottomPanel").Find("continue");
				transform.GetComponent<Button>().onClick.RemoveAllListeners();
				transform.GetComponent<Button>().onClick.AddListener(delegate
				{
					OnContinueBtnImageHit(img);
				});
				root.Find("BottomPanel").Find("continue").Find("continueText")
					.GetComponent<Text>()
					.text = PublicToolController.GetTextByLanguage("continue");
					Transform transform2 = root.Find("BottomPanel").Find("share");
					transform2.GetComponent<Button>().onClick.RemoveAllListeners();
					transform2.GetComponent<Button>().onClick.AddListener(delegate
					{
						clickShareBtn(img);
					});
					root.Find("more").GetComponent<Button>().onClick.RemoveAllListeners();
					root.Find("more").GetComponent<Button>().onClick.AddListener(delegate
					{
						clickMoreBtn(img.imageId);
					});
					if (index % 8 == 0)
					{
						PublicCallFrameTool.Instance.CollectGarbage();
					}
				}
			}

			private void clickMoreBtn(string imageId)
			{
				MyworkCanvasEvent.instance.ShowDeleteWork(imageId);
				UnityEngine.Debug.Log("clickMoreBtn..");
			}

			private void clickShareBtn(BaseEditedImg img)
			{
				if (!img.isRemote && img.HasEdited())
				{
					RequestFinalOnshare(GetTex2dFromImg(img));
				}
				else if (img.isRemote)
				{
					NetLoadcontroller.Instance.RequestImg(img.thumb, delegate(Texture2D tex)
					{
						RequestFinalOnshare(tex);
					});
				}
			}

			private void RequestFinalOnshare(Texture2D tex)
			{
				UnityEngine.Debug.Log("go to share !!!");
				if (ApplicationModel.userInfo != null)
				{
					FileUploadEventProxy.Instance.SendShareWorkEventMsg(tex, ApplicationModel.userInfo.Nickname, ApplicationModel.EditedResourceImgList.Count, isHasinfo: true, shareCallbck);
				}
				else
				{
					FileUploadEventProxy.Instance.SendShareWorkEventMsg(tex, string.Empty, 0, isHasinfo: false, shareCallbck);
				}
			}

			private void shareCallbck(Errcode err)
			{
				DragEventProxy.Instance.SendShareCallbackMsg(err);
			}

			private void LikeBtnControl(BaseEditedImg img)
			{
				GameObject likeBtn = root.Find("BottomPanel").Find("like").gameObject;
				GameObject likeCntBtn = root.Find("BottomPanel").Find("like").Find("likeCnt")
					.gameObject;
					ChangeBtnStyle(img.isLike, likeBtn);
					likeCntBtn.GetComponent<Text>().text = img.likeCnt.ToString();
					likeBtn.GetComponent<Button>().onClick.RemoveAllListeners();
					likeBtn.GetComponent<Button>().onClick.AddListener(delegate
					{
						OnHitLikeBtn(img, likeBtn, likeCntBtn);
					});
				}

				private void OnHitLikeBtn(BaseEditedImg img, GameObject btn, GameObject likeCntBtn)
				{
					if (ApplicationModel.userInfo == null)
					{
						DragEventProxy.Instance.SendOffLineMsg("MyworkCanvas");
						return;
					}
					img.isLike = !img.isLike;
					if (img.isLike)
					{
						img.likeCnt++;
					}
					else
					{
						img.likeCnt--;
					}
					likeCntBtn.GetComponent<Text>().text = img.likeCnt.ToString();
					ChangeBtnStyle(img.isLike, btn);
					FileUploadEventProxy.Instance.SendLikeMyworkEventMsg(img, img.isLike);
				}

				private void ChangeBtnStyle(bool isLike, GameObject likeBtn)
				{
					if (isLike)
					{
						likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_en");
					}
					else
					{
						likeBtn.GetComponent<RawImage>().texture = Resources.Load<Texture2D>("Images/Image/likeBtn_un");
					}
				}

				private void callback(Texture2D tex, BaseEditedImg img, string requestUrl)
				{
					int itemIndex = myWorkItemsViewHolder.itemIndex;
					if (IsModelStillValid(myWorkItemsViewHolder.itemIndex, itemIndex, requestUrl))
					{
						RawImage component = root.Find("Image").GetComponent<RawImage>();
						if (component != null)
						{
							component.texture = tex;
						}
						root.Find("Image").GetComponent<Button>().onClick.RemoveAllListeners();
						root.Find("Image").GetComponent<Button>().onClick.AddListener(delegate
						{
							OnImageHit(tex);
						});
					}
				}

				private void SetFrommImgData()
				{
					root.GetComponent<RawImageDataController>().downloadStatus = 1;
				}

				private Texture2D GetTex2dFromImg(BaseEditedImg img)
				{
					Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
					texture2D.LoadImage(PublicToolController.FileReaderBytes(img.imageId + "_ok"), markNonReadable: false);
					texture2D.Apply();
					return texture2D;
				}

				private void OnImageHit(Texture2D tex)
				{
					DragEventProxy.Instance.SendGotoZoomImageMsg(tex);
				}

				private void OnContinueBtnImageHit(BaseEditedImg img)
				{
					MyworkCanvasEvent.instance.GotoWorkShopEvent(img);
				}

				protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
				{
					MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
					myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
					return myWorkItemsViewHolder;
				}

				private bool IsModelStillValid(int itemIndex, int itemIdexAtRequest, string imageURLAtRequest)
				{
					return _Data.Count > itemIndex && itemIdexAtRequest == itemIndex && imageURLAtRequest == _Data[itemIndex].img.thumb;
				}

				protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
				{
					root = newOrRecycled.Root;
					DoubleImageModel doubleImageModel = _Data[newOrRecycled.itemIndex];
					myWorkItemsViewHolder = newOrRecycled;
					SetImage(doubleImageModel.img, doubleImageModel.index);
				}
			}

			[SerializeField]
			private GameObject EmptyPanel;

			[SerializeField]
			private MyWorkParams _ScrollRectAdapterParams;

			[SerializeField]
			private int ItemHeight;

			private List<DoubleImageModel> _Data = new List<DoubleImageModel>();

			private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

			public BaseParams Params => _ScrollRectAdapterParams;

			public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

			public List<DoubleImageModel> Data => _Data;

			private void Awake()
			{
				_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
			}

			private void OnEnable()
			{
				UnityEngine.Debug.Log("my work enable!!!");
				ApplicationModel.lastVisibleScreen = "MyworkCanvas";
				StartCoroutine(initContent());
				int childCount = base.transform.GetChild(0).GetChild(0).childCount;
				SetEmptyPanelStatus(childCount);
			}

			private void SetContentPositon()
			{
				base.transform.GetChild(0).GetChild(0).GetComponent<RectTransform>()
					.anchoredPosition = new Vector2(0f, 0f);
				}

				private void startInitContent()
				{
					StartCoroutine(initContent());
				}

				private void SetEmptyPanelStatus(int count)
				{
					UnityEngine.Debug.Log("my work count is:" + count);
					if (count > 0)
					{
						EmptyPanel.SetActive(value: false);
					}
					else
					{
						EmptyPanel.SetActive(value: true);
					}
				}

				private List<BaseEditedImg> GeTHasEditedImgList()
				{
					List<BaseEditedImg> myEditedNormalAndPixelList = PublicImgaeApi.GetMyEditedNormalAndPixelList();
					UnityEngine.Debug.Log("MixEditedImgList count is:" + myEditedNormalAndPixelList.Count);
					UnityEngine.Debug.Log("MixEditedImgList is:" + JsonConvert.SerializeObject(myEditedNormalAndPixelList));
					List<BaseEditedImg> list = new List<BaseEditedImg>();
					foreach (BaseEditedImg item in myEditedNormalAndPixelList)
					{
						if (!item.isRemote && item.HasEdited())
						{
							list.Add(item);
						}
						else if (item.isRemote)
						{
							list.Add(item);
						}
					}
					return list;
				}

				private IEnumerator initContent()
				{
					yield return null;
					_Data.Clear();
					List<BaseEditedImg> imglist = GeTHasEditedImgList();
					int ImgTotal = imglist.Count;
					UnityEngine.Debug.Log("initContent img list count is:" + ImgTotal);
					SetEmptyPanelStatus(ImgTotal);
					for (int i = 0; i < ImgTotal; i++)
					{
						_Data.Add(new DoubleImageModel(imglist[i], i));
					}
					_Data.Capacity = _Data.Count;
					_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
					PublicCallFrameTool.Instance.CollectGarbage();
				}

				private void DeleteWorkItemByWorkId(string imageId)
				{
					int num = -1;
					int count = ApplicationModel.EditedResourceImgList.Count;
					for (int i = 0; i < count; i++)
					{
						if (ApplicationModel.EditedResourceImgList[i].imageId == imageId)
						{
							num = i;
						}
					}
					if (num > -1)
					{
						ApplicationModel.EditedResourceImgList.RemoveAt(num);
						ApplicationModel.RestoreEditedImgList();
					}
					num = -1;
					int count2 = ApplicationModel.PixleEditedImgList.Count;
					for (int j = 0; j < count2; j++)
					{
						if (ApplicationModel.PixleEditedImgList[j].imageId == imageId)
						{
							num = j;
						}
					}
					if (num > -1)
					{
						ApplicationModel.PixleEditedImgList.RemoveAt(num);
						ApplicationModel.RestoreEditedPixelImglist();
					}
				}

				private int GetIndexByImageId(string imageId)
				{
					int count = _Data.Count;
					for (int i = 0; i < count; i++)
					{
						if (imageId == _Data[i].img.imageId)
						{
							return i;
						}
					}
					return -1;
				}

				public void RemoveItem(string imageId)
				{
					int indexByImageId = GetIndexByImageId(imageId);
					if (indexByImageId > -1)
					{
						DeleteWorkItemByWorkId(imageId);
						string workId = _Data[indexByImageId].img.workId;
						if (workId != null && workId != string.Empty)
						{
							PublicControllerApi.DeleteWork(workId);
						}
						_ScrollRectItemsAdapter.Remove(indexByImageId);
					}
					else
					{
						UnityEngine.Debug.Log("删除出现问题");
					}
				}

				private void ChangeContentHeight(int count)
				{
					Transform child = base.transform.GetChild(0).GetChild(0);
					RectTransform component = child.GetComponent<RectTransform>();
					Vector2 sizeDelta = child.GetComponent<RectTransform>().sizeDelta;
					component.sizeDelta = new Vector2(sizeDelta.x, count * ItemHeight + 20);
				}

				private void OnDestroy()
				{
					if (_ScrollRectItemsAdapter != null)
					{
						_ScrollRectItemsAdapter.Dispose();
					}
				}

				public void OnBeginDrag(PointerEventData eventData)
				{
				}

				public void OnDrag(PointerEventData eventData)
				{
				}

				public void OnEndDrag(PointerEventData eventData)
				{
				}
			}
