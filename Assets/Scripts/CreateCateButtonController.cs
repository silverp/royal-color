using Newtonsoft.Json;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CreateCateButtonController : MonoBehaviour
{
	[SerializeField]
	private GameObject CateButton;

	private int Screenwidth = 1080;

	private float leftDeviation = 23f;

	private string btnColorYellowCode = "#FFB63EFF";

	private string btnColorGrayCode = "#898989FF";

	private Color btnColorYellow;

	private Color btnColorGray;

	private Color btnActiveColor;

	private RectTransform contentRectTransform;

	private void Awake()
	{
		ColorUtility.TryParseHtmlString(btnColorYellowCode, out btnColorYellow);
		ColorUtility.TryParseHtmlString(btnColorGrayCode, out btnColorGray);
	}

	private void Start()
	{
		contentRectTransform = base.transform.GetChild(0).GetComponent<RectTransform>();
		InitCateButtonList();
		SetActive(0);
	}

	private void OnEnable()
	{
		AssginEvents();
	}

	private void AssginEvents()
	{
		DragEventProxy.OnClickCateListBar += ClickCateListBar;
		DragEventProxy.OnRefreshCatelistBar += RefreshCateListBar;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnClickCateListBar -= ClickCateListBar;
		DragEventProxy.OnRefreshCatelistBar -= RefreshCateListBar;
	}

	private void RefreshCateListBar()
	{
		InitCateButtonList();
		SetActive(0);
	}

	private void ClickCateListBar(int index)
	{
		ChangePage(index);
	}

	private void InitCateListBarLength(float length)
	{
		RectTransform component = base.transform.GetChild(0).gameObject.GetComponent<RectTransform>();
		Vector2 sizeDelta = base.transform.gameObject.GetComponent<RectTransform>().sizeDelta;
		component.sizeDelta = new Vector2(length, sizeDelta.y);
	}

	private void SetActive(int index)
	{
		if (index <= ApplicationModel.CateResourceList.Count - 1)
		{
			base.transform.Find("content").GetChild(index).Find("Image")
				.gameObject.SetActive(value: true);
				Color color = default(Color);
				ColorUtility.TryParseHtmlString("#FFB63EFF", out color);
				base.transform.Find("content").GetChild(index).Find("Text")
					.GetComponent<Text>()
					.color = color;
				}
			}

			private void SetFalse(int index)
			{
				base.transform.GetChild(0).GetChild(index).GetChild(0)
					.gameObject.SetActive(value: false);
					base.transform.GetChild(0).GetChild(index).GetChild(1)
						.GetComponent<Text>()
						.color = btnColorGray;
					}

					private void InitCateButtonList()
					{
						int num = 0;
						float num2 = leftDeviation;
						float y = 0f;
						float num3 = 0f;
						float y2 = 0f;
						UnityEngine.Debug.Log("cate list count is:" + ApplicationModel.CateResourceList.Count);
						UnityEngine.Debug.Log("cate list json is:" + JsonConvert.SerializeObject(ApplicationModel.CateResourceList));
						foreach (CateResource cateResource in ApplicationModel.CateResourceList)
						{
							if (cateResource != null)
							{
							}
							GameObject gameObject = UnityEngine.Object.Instantiate(CateButton, base.transform.Find("content"), worldPositionStays: false);
							gameObject.transform.Find("Text").GetComponent<Text>().text = cateResource.name;
							if (num == 0)
							{
								Vector2 anchoredPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
								y2 = anchoredPosition.y;
								Vector2 sizeDelta = gameObject.transform.Find("Text").GetComponent<RectTransform>().sizeDelta;
								num3 = sizeDelta.y;
								Vector2 sizeDelta2 = gameObject.GetComponent<RectTransform>().sizeDelta;
								y = sizeDelta2.y;
							}
							gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(num2, y2);
							int length = cateResource.name.Length;
							num2 += (float)getButtonWidth(length);
							gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(getButtonWidth(length), y);
							RectTransform component = gameObject.transform.Find("Text").GetComponent<RectTransform>();
							float x = getTextWidth(length);
							Vector2 sizeDelta3 = gameObject.transform.Find("Text").GetComponent<RectTransform>().sizeDelta;
							component.sizeDelta = new Vector2(x, sizeDelta3.y);
							RectTransform component2 = gameObject.transform.Find("Image").GetComponent<RectTransform>();
							float x2 = getImageWidth(length);
							Vector2 sizeDelta4 = gameObject.transform.Find("Image").GetComponent<RectTransform>().sizeDelta;
							component2.sizeDelta = new Vector2(x2, sizeDelta4.y);
							int _index = num;
							gameObject.GetComponent<Button>().onClick.AddListener(delegate
							{
								BtnChangePage(_index);
							});
							num++;
						}
						CateListManager.instance.MaxListBarNum = num;
						InitCateListBarLength(num2);
						InitCateListBarContentX();
					}

					private void InitCateListBarContentX()
					{
						RectTransform component = base.transform.Find("content").gameObject.GetComponent<RectTransform>();
						Vector2 anchoredPosition = base.transform.Find("content").gameObject.GetComponent<RectTransform>().anchoredPosition;
						component.anchoredPosition = new Vector2(0f, anchoredPosition.y);
					}

					private void BtnChangePage(int index)
					{
						DragEventProxy.Instance.SendClickPageMsg(index);
						ChangePage(index);
					}

					private void ChangePage(int index)
					{
						GameObject gameObject = base.transform.GetChild(0).GetChild(index).gameObject;
						RectTransform component = gameObject.GetComponent<RectTransform>();
						if (index >= 0 && index <= CateListManager.instance.MaxListBarNum - 1 && CateListManager.instance.currentListBar != index)
						{
							SetFalse(CateListManager.instance.currentListBar);
							Vector2 anchoredPosition = component.anchoredPosition;
							float x = anchoredPosition.x;
							Vector2 anchoredPosition2 = component.anchoredPosition;
							float y = anchoredPosition2.y;
							Vector2 sizeDelta = component.sizeDelta;
							float x2 = sizeDelta.x;
							Vector2 anchoredPosition3 = contentRectTransform.anchoredPosition;
							float x3 = anchoredPosition3.x;
							Vector2 anchoredPosition4 = contentRectTransform.anchoredPosition;
							float y2 = anchoredPosition4.y;
							Vector2 sizeDelta2 = contentRectTransform.sizeDelta;
							float x4 = sizeDelta2.x;
							if (x + x2 + x3 > (float)(Screenwidth - 200) && x3 + x4 >= (float)Screenwidth)
							{
								float num = x + x2 + x3 - (float)(Screenwidth - 200);
								StartCoroutine(MoveToPosition(contentRectTransform, new Vector2(x3 - num, y2), 0.3f));
							}
							else if (x + x3 < 200f && x3 < 0f)
							{
								float num2 = 200f - (x + x3);
								StartCoroutine(MoveToPosition(contentRectTransform, new Vector2(x3 + num2, y2), 0.3f));
							}
							SetActive(index);
							CateListManager.instance.currentListBar = index;
							TotalGA.Event("click_cate_name", gameObject.transform.GetChild(1).GetComponent<Text>().text);
							ApplicationModel.currentCategroy = ApplicationModel.CateResourceList[index];
						}
					}

					private int getButtonWidth(int num)
					{
						if (ApplicationModel.LANGUAGE == "zh-cn")
						{
							return num * 42 + 86;
						}
						return num * 23 + 76;
					}

					private int getImageWidth(int num)
					{
						if (ApplicationModel.LANGUAGE == "zh-cn")
						{
							return num * 42 - 4;
						}
						return num * 23 - 4;
					}

					private int getTextWidth(int num)
					{
						if (ApplicationModel.LANGUAGE == "zh-cn")
						{
							return num * 42 + 40;
						}
						return num * 23 + 40;
					}

					private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
					{
						Vector2 currentPos = rectTransform.anchoredPosition;
						float t = 0f;
						while (t < 1f)
						{
							t += Time.deltaTime / TimeToMove;
							rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
							yield return null;
						}
					}

					private void OnDisable()
					{
						DiscardEvents();
					}
				}
