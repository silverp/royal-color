using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class EnterWorkshopFromPixelListPage : MonoBehaviour
{
	private bool isNetError;

	[SerializeField]
	private GameObject pixelArtWorkshop;

	[SerializeField]
	private GameObject BeanNoteBox;

	public static EnterWorkshopFromPixelListPage instance;

	private GUIStyle guiStyle = new GUIStyle();

	private int _index;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void UnlockOutlineCallback(PixelArt img, Errcode err, int amount, GameObject lockBtn)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("unlock Success!!!");
			lockBtn.SetActive(value: false);
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 0.6f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				GoToWorkShop(img);
			});
		}
		else
		{
			DragEventProxy.Instance.SendUnclockOutlineMsg(null, img, ImageType.pixel);
		}
	}

	public void GotoWorkShopEvent(PixelArt img, GameObject lockBtn)
	{
		if (lockBtn.activeSelf)
		{
			if (ApplicationModel.userInfo != null)
			{
				UnityEngine.Debug.Log("need to unlock!!!!");
				UnlockArtManager.Instance.UnlockArt(img, delegate(Errcode err, int amount)
				{
					UnlockOutlineCallback(img, err, amount, lockBtn);
				});
			}
			else
			{
				DragEventProxy.Instance.SendUnclockOutlineMsg(null, img, ImageType.pixel);
			}
		}
		else
		{
			GoToWorkShop(img);
		}
	}

	public void GoToWorkShop(PixelArt img)
	{
		UnityEngine.Debug.Log("GoToWorkShop ....." + img.imageId);
		ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedPixelImgFromEditedList(img, isCreate: true);
		ApplicationModel.lastVisibleScreen = "PixelArtDetailCanvas";
		base.transform.parent.gameObject.SetActive(value: false);
		pixelArtWorkshop.SetActive(value: true);
		FileUploadEventProxy.Instance.SendClickImageMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
	}

	private IEnumerator publishMessge(PixelArt img)
	{
		yield return new WaitForSeconds(0.3f);
		MessageBroker.Default.Publish(new PixelColorPanel.CntAndColorMap
		{
			cntUrl = img.countUrl,
			colorUrl = img.colorUrl
		});
	}
}
