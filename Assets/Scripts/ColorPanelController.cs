using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPanelController : MonoBehaviour
{
	private List<ColorTheme> colorThemes;

	private GameObject Desp;

	private GameObject colorBox;

	private int W_wrap = 1242;

	[SerializeField]
	private GameObject colorThemeWrap;

	[SerializeField]
	private GameObject colorItem;

	private int ColorCLCount = 5;

	private void Awake()
	{
		UpdateColorPanel(ApplicationModel.ColorThemes);
	}

	private void UpdateColorPanel(List<ColorTheme> colorThemes)
	{
		int count = colorThemes.Count;
		int num = 0;
		foreach (ColorTheme colorTheme in colorThemes)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(colorThemeWrap, base.transform, worldPositionStays: false);
			gameObject.transform.localPosition += Vector3.right * 835f * num;
			gameObject.transform.Find("Desp").transform.Find("Theme").GetComponent<Text>().text = colorTheme.Name;
			Transform transform = gameObject.transform.Find("ColorBox").transform;
			for (int i = 0; i < colorTheme.Colors.Count; i++)
			{
				if (i > 8)
				{
					break;
				}
				GameObject gameObject2 = UnityEngine.Object.Instantiate(colorItem, transform, worldPositionStays: false);
				gameObject2.transform.localPosition += Vector3.right * 150f * (i % ColorCLCount);
				gameObject2.transform.localPosition += Vector3.down * 135f * (i / ColorCLCount);
				if (i / ColorCLCount % 2 == 1)
				{
					gameObject2.transform.localPosition += Vector3.right * 75f;
				}
				Color col = default(Color);
				ColorUtility.TryParseHtmlString(colorTheme.Colors[i], out col);
				gameObject2.transform.GetChild(0).GetComponent<Image>().color = col;
				gameObject2.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(delegate
				{
					onClick(col);
				});
				if (i == 0 && num == 0 && base.transform.name == "ColorPanelPure")
				{
					gameObject2.transform.Find("Selected").gameObject.SetActive(value: true);
				}
			}
			num++;
		}
	}

	private void onClick(Color col)
	{
		WorkShopManagerController.instance.ChangePencilColorTo(col);
	}

	private void Update()
	{
	}
}
