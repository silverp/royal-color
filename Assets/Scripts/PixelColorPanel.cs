using System;
using System.Collections;
using UniRx;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PixelColorPanel : MonoBehaviour, IEndDragHandler, IEventSystemHandler
{
	public class CntAndColorMap
	{
		public string cntUrl
		{
			get;
			set;
		}

		public string colorUrl
		{
			get;
			set;
		}
	}

	[SerializeField]
	private GameObject colorItem;

	[SerializeField]
	private new GameObject gameObject;

	private ScrollRect scrollRect;

	public PixelDrawController pixelDraw;

	protected const float item_width = 114f;

	protected const float item_gap = 16f;

	protected Texture2D cnt;

	protected Texture2D colorIndex;

	protected PixleEditedImg currentpixelArt;

	private void Start()
	{
	}

	private void OnEnable()
	{
		pixelDraw.loadEditInfo(OnLoadEditedInfo);
		UnityEngine.Debug.Log("PixelColorPanel.OnEnable.time = " + Time.realtimeSinceStartup);
	}

	private void OnDisable()
	{
		DestoryChild(base.transform.GetChild(0));
		ReSetContentPosition();
		UnityEngine.Debug.Log("OnDisable.InitPanel.content.count = " + base.transform.GetChild(0).childCount);
	}

	private void ReSetContentPosition()
	{
		Vector3 localPosition = base.transform.Find("Content").localPosition;
		localPosition = new Vector3(-540f, localPosition.y, localPosition.z);
		base.transform.Find("Content").localPosition = localPosition;
	}

	private void OnLoadEditedInfo(PixleEditedImg editor)
	{
		UnityEngine.Debug.Log("PixelColorPanel.cntUrl = " + editor.countUrl);
		currentpixelArt = editor;
		LoadTexture(currentpixelArt.countUrl, currentpixelArt.colorUrl);
	}

	protected void LoadTexture(string cntUrl, string colorUrl)
	{
		IObservable<byte[][]> source = Observable.WhenAll<byte[]>(ObservableWWW.GetAndGetBytes(cntUrl), ObservableWWW.GetAndGetBytes(colorUrl));
		source.Subscribe(delegate(byte[][] xs)
		{
			if (currentpixelArt.HasEdited())
			{
				UnityEngine.Debug.Log("load cntmap from local!");
				cnt = PublicToolController.GetTexture2dFromLocalByPath(currentpixelArt.imageId + "_cntMap");
			}
			else
			{
				cnt = new Texture2D(8, 8, TextureFormat.ARGB32, mipChain: false);
				cnt.LoadImage(xs[0]);
				cnt.Apply();
			}
			UnityEngine.Debug.Log("cnt:" + cnt.width + "," + cnt.height);
			colorIndex = new Texture2D(8, 8, TextureFormat.ARGB32, mipChain: false);
			colorIndex.LoadImage(xs[1]);
			colorIndex.Apply();
			InitPanel();
		});
	}

	protected void InitPanel()
	{
		Transform transform = base.transform.Find("Content");
		IEnumerator enumerator = transform.transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform2.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
		scrollRect = base.transform.GetComponent<ScrollRect>();
		int count = getCount(cnt, 0, 0);
		if (count < 0 || count > 64)
		{
			return;
		}
		Vector2 lines = arrange(count);
		Vector2 vector = arrangePos(lines);
		int num = 0;
		for (int i = 0; i <= count / 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				num++;
				if (num > count)
				{
					break;
				}
				Color pixel = colorIndex.GetPixel(j, colorIndex.height - i - 1);
				GameObject gameObject = UnityEngine.Object.Instantiate(colorItem, transform, worldPositionStays: false);
				PixelColorButtonController component = gameObject.transform.GetComponent<PixelColorButtonController>();
				component.color = pixel;
				component.index = num;
				if ((float)num <= lines.x)
				{
					gameObject.transform.localPosition += new Vector3(vector.x + (float)(num - 1) * 130f, -57f, 0f);
				}
				else
				{
					gameObject.transform.localPosition += new Vector3(vector.y + ((float)num - lines.x - 1f) * 130f, -187f, 0f);
				}
			}
		}
	}

	protected Vector2 arrange(int total)
	{
		Vector2 zero = Vector2.zero;
		if (total < 7)
		{
			zero.x = total;
		}
		else
		{
			zero.x = total / 2;
			zero.y = (float)total - zero.x;
		}
		return zero;
	}

	protected Vector2 arrangePos(Vector2 lines)
	{
		Vector2 zero = Vector2.zero;
		float width = base.transform.GetComponent<RectTransform>().rect.width;
		float num = 0f;
		if (lines.x <= lines.y)
		{
			zero.y = (0f - (lines.y * 114f + (lines.y - 1f) * 16f)) / 2f + 57f;
			zero.x = zero.y + 65f;
			num = lines.y * 114f + (lines.y - 1f) * 16f + 64f;
			if (lines.x == lines.y)
			{
				num += 65f;
			}
		}
		else
		{
			zero.x = (0f - (lines.x * 114f + (lines.x - 1f) * 16f)) / 2f + 57f;
			num = lines.x * 114f + (lines.x - 1f) * 16f + 64f;
		}
		RectTransform component = scrollRect.content.GetComponent<RectTransform>();
		num = Mathf.Max(num, base.transform.GetComponent<RectTransform>().rect.width);
		RectTransform rectTransform = component;
		float x = num;
		Vector2 sizeDelta = component.sizeDelta;
		rectTransform.sizeDelta = new Vector2(x, sizeDelta.y);
		return zero;
	}

	protected int getCount(Texture2D cnt, int x, int y)
	{
		Color32 color = cnt.GetPixel(x, cnt.height - y - 1);
		return color.r + 255 * color.g;
	}

	private void Update()
	{
	}

	private void DestoryChild(Transform transform)
	{
		IEnumerator enumerator = transform.GetEnumerator();
		try
		{
			while (enumerator.MoveNext())
			{
				Transform transform2 = (Transform)enumerator.Current;
				UnityEngine.Object.Destroy(transform2.gameObject);
			}
		}
		finally
		{
			IDisposable disposable;
			if ((disposable = (enumerator as IDisposable)) != null)
			{
				disposable.Dispose();
			}
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		UnityEngine.Debug.Log("OnEndDrag...");
		ForbidHitThroght component = gameObject.transform.GetComponent<ForbidHitThroght>();
		component.ForbidThoughtWithoutScaleChange();
	}
}
