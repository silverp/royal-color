using UnityEngine;
using UnityEngine.UI;

public class MyworkTabChangeController : MonoBehaviour
{
	[SerializeField]
	private GameObject leftButton;

	[SerializeField]
	private GameObject rightButton;

	[SerializeField]
	private GameObject WorkPanel;

	[SerializeField]
	private GameObject LikesPanel;

	private void Start()
	{
	}

	private void OnEnable()
	{
		ClickLeft();
	}

	private void Update()
	{
	}

	private void SetButtonActive(GameObject button)
	{
		button.transform.Find("Image").gameObject.SetActive(value: true);
		Color color = default(Color);
		ColorUtility.TryParseHtmlString("#333333FF", out color);
		button.transform.Find("Text").GetComponent<Text>().color = color;
	}

	private void SetButtonUnActive(GameObject button)
	{
		button.transform.Find("Image").gameObject.SetActive(value: false);
		Color color = default(Color);
		ColorUtility.TryParseHtmlString("#898989FF", out color);
		button.transform.Find("Text").GetComponent<Text>().color = color;
	}

	public void ClickLeft()
	{
		if (base.transform.parent.name == "MyworkCanvas")
		{
			TotalGA.Event("mywork_page_event", "进入作品页面");
		}
		else
		{
			TotalGA.Event("user_page_event", "进入作品页面");
		}
		SetButtonActive(leftButton);
		SetButtonUnActive(rightButton);
		WorkPanel.SetActive(value: true);
		LikesPanel.SetActive(value: false);
	}

	public void ClickRight()
	{
		if (base.transform.parent.name == "MyworkCanvas")
		{
			TotalGA.Event("mywork_page_event", "进入点赞页面");
		}
		else
		{
			TotalGA.Event("user_page_event", "进入点赞页面");
		}
		SetButtonActive(rightButton);
		SetButtonUnActive(leftButton);
		LikesPanel.SetActive(value: true);
		WorkPanel.SetActive(value: false);
	}
}
