using EasyMobile;
using Newtonsoft.Json;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class AdButtonController : MonoBehaviour
{
	[SerializeField]
	private GameObject adButton;

	[SerializeField]
	private GameObject AdNote;

	[SerializeField]
	private GameObject WorkShopCanvas;

	[SerializeField]
	private GameObject pixelArtWorkshop;

	public string currentImgId;

	public PixelArt pixelArtImage;

	public ResourceImg resourceImg;

	public string type;

	private void Start()
	{
		if (!ApplicationModel.isAdChannel)
		{
			adButton.SetActive(value: false);
			return;
		}
		adButton.SetActive(value: true);
		adButton.GetComponent<Button>().onClick.RemoveAllListeners();
		if (ApplicationModel.currentChannel == "googleplay")
		{
			adButton.GetComponent<Button>().onClick.AddListener(delegate
			{
				ShowRewardAd();
			});
		}
		else
		{
			NewAdController.Instance.requestAdGDTInter();
			adButton.GetComponent<Button>().onClick.AddListener(delegate
			{
				showAdGTDInter();
			});
		}
		AdNote.SetActive(value: false);
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			AdNote.GetComponent<Text>().text = "广告还没有加载好,请稍后再点击.";
		}
		else
		{
			AdNote.GetComponent<Text>().text = "The ad is not ready yet,click later";
		}
	}

	private void Update()
	{
	}

	private bool IOSIsRewardAdReady()
	{
		if (Advertising.IsRewardedAdReady())
		{
			return true;
		}
		return false;
	}

	private bool AndroidIsInterAdReady()
	{
		if (NewAdController.Instance.isInterAdGDTReady())
		{
			return true;
		}
		return false;
	}

	public void showAdGTDInter()
	{
		TotalGA.Event("click_ad_button");
		if (NewAdController.Instance.isInterAdGDTReady())
		{
			UnityEngine.Debug.Log(" inter ad gdt is ready, will show it");
			NewAdController.Instance.showAdGTDInter();
			PublicUserInfoApi.UploadImgIdToUnLockList(currentImgId);
			UnityEngine.Debug.Log("unlock list is:" + JsonConvert.SerializeObject(ApplicationModel.unlockList));
			DragEventProxy.Instance.SendRefreshAfterLoginMsg();
			StartCoroutine(StartEnterWorkshopCorutine());
		}
		else
		{
			UnityEngine.Debug.Log(" seems ad gdt is not ready, will request again");
			AdNote.SetActive(value: true);
			StartCoroutine(HideAdnote());
			NewAdController.Instance.requestAdGDTInter();
		}
	}

	private void RewardAdComplete(RewardedAdNetwork network, AdLocation location)
	{
		TotalGA.Event("reward_video_finished");
		PublicUserInfoApi.UploadImgIdToUnLockList(currentImgId);
		UnityEngine.Debug.Log("unlock list is:" + JsonConvert.SerializeObject(ApplicationModel.unlockList));
		DragEventProxy.Instance.SendRefreshAfterLoginMsg();
		afterUnlockAd();
		Advertising.LoadRewardedAd();
	}

	public void ShowRewardAd()
	{
		TotalGA.Event("click_ad_button");
		if (Advertising.IsRewardedAdReady())
		{
			AdNote.SetActive(value: false);
			Advertising.ShowRewardedAd();
			return;
		}
		Advertising.LoadRewardedAd();
		UnityEngine.Debug.Log(" seems ad gdt is not ready, will request again");
		AdNote.SetActive(value: true);
		StartCoroutine(HideAdnote());
	}

	private void afterUnlockAd()
	{
		if (type == "normal")
		{
			EnterWorkShopFornormal();
		}
		else
		{
			EnterWorkShopForPixel();
		}
		base.transform.parent.gameObject.SetActive(value: false);
	}

	private IEnumerator StartEnterWorkshopCorutine()
	{
		yield return new WaitForSeconds(1f);
		afterUnlockAd();
	}

	private IEnumerator HideAdnote()
	{
		yield return new WaitForSeconds(2f);
		AdNote.SetActive(value: false);
	}

	private void OnEnable()
	{
		AdNote.SetActive(value: false);
		Advertising.RewardedAdCompleted += RewardAdComplete;
	}

	private void OnDisable()
	{
		Advertising.RewardedAdCompleted -= RewardAdComplete;
	}

	private void EnterWorkShopFornormal()
	{
		GameObject gameObject = base.transform.parent.parent.gameObject;
		ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedImgFromEditedList(currentImgId, isCreate: true);
		ApplicationModel.lastVisibleScreen = gameObject.name;
		gameObject.SetActive(value: false);
		WorkShopCanvas.gameObject.SetActive(value: true);
		FileUploadEventProxy.Instance.SendClickImageMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
	}

	public void EnterWorkShopForPixel()
	{
		GameObject gameObject = base.transform.parent.parent.gameObject;
		ApplicationModel.currentEditedImg = PublicImgaeApi.GetEditedPixelImgFromEditedList(pixelArtImage, isCreate: true);
		ApplicationModel.lastVisibleScreen = gameObject.name;
		gameObject.SetActive(value: false);
		pixelArtWorkshop.gameObject.SetActive(value: true);
		FileUploadEventProxy.Instance.SendClickImageMsg(ApplicationModel.currentEditedImg, ApplicationModel.device_id);
	}
}
