using UnityEngine;

public class WorkShopManagerController : MonoBehaviour
{
	public delegate void PencilColorChangeAction(Color from, Color to);

	public static WorkShopManagerController instance;

	private Color CurrentPencilColor;

	public static event PencilColorChangeAction onPencilColorChanged;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		ClickFirstBtn();
	}

	public void ClickFirstBtn()
	{
		GradientColor gradientColor = ApplicationModel.ColorThemesGradient.gradientColors[0];
		Point colorCoordinate = gradientColor.colorCoordinate;
		Color32 colorByColorIndex = getColorByColorIndex(colorCoordinate.x, colorCoordinate.y, gradientColor.colorIndex[0]);
		Vector2 gradientColorCoordinate = new Vector2(getIndexByGradientColor(colorCoordinate.x, gradientColor.colorIndex[0]), colorCoordinate.y);
		ApplicationModel.IsGradient = true;
		ChangePencilColorTo(colorByColorIndex);
		ApplicationModel.currentColorComplex = new ColorComplex(isGradient: true, colorByColorIndex, gradientColorCoordinate);
	}

	public void DoShare()
	{
		UnityEngine.Debug.Log("share btn was clicked");
	}

	public void ChangePencilColorTo(Color newColor)
	{
		UnityEngine.Debug.Log("ChangePencilColorTo .." + newColor.ToString());
		if (WorkShopManagerController.onPencilColorChanged != null)
		{
			WorkShopManagerController.onPencilColorChanged(CurrentPencilColor, newColor);
		}
		CurrentPencilColor = newColor;
		ApplicationModel.CurrentCol = CurrentPencilColor;
	}

	public Color GetPencilColor()
	{
		return CurrentPencilColor;
	}

	private float getIndexByGradientColor(int x, int index)
	{
		return x * 9 + index;
	}

	private Color32 getColorByString(string colorStr)
	{
		Color color = default(Color);
		ColorUtility.TryParseHtmlString(colorStr, out color);
		return color;
	}

	private Color32 getColorByColorIndex(int x, int y, int index)
	{
		return new Color32((byte)(x * 9 + index), (byte)y, 0, 0);
	}
}
