using UnityEngine;

public class SharePageLoginBtnManager : MonoBehaviour
{
	[SerializeField]
	private GameObject loginBtnPanel;

	[SerializeField]
	private GameObject LoginStatus;

	private void OnEnable()
	{
		loginBtnPanel.SetActive(value: true);
		LoginStatus.SetActive(value: false);
	}

	private void OnDisable()
	{
		loginBtnPanel.SetActive(value: false);
		LoginStatus.SetActive(value: true);
	}
}
