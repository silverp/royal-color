using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;

public class UserLoginInfoController : MonoBehaviour
{
	private string workCountUrl = "/user/getWorkListCount";

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		string str = string.Empty;
		if (ApplicationModel.userInfo != null)
		{
			int num = PublicToolController.CountDiffDaysByTimeStamp(ApplicationModel.userInfo.Create.ToString());
			num = ((num == 0) ? 1 : num);
			if (ApplicationModel.LANGUAGE == "zh-cn")
			{
				str = "已登录" + num + "天";
			}
			else
			{
				str = string.Empty + num + " days";
			}
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + workCountUrl + "?uid=" + ApplicationModel.userInfo.Uid + "&sessionId=" + ApplicationModel.userInfo.SessionId + "&isMine=" + true, delegate(string json, bool isSuccess)
			{
				RequestUrlCallBack(json, isSuccess, str);
			});
		}
		else
		{
			if (ApplicationModel.LANGUAGE == "zh-cn")
			{
				str = "登录，保留你的精彩瞬间";
			}
			else
			{
				str = "Login,save your work!";
			}
			GetComponent<Text>().text = str;
		}
	}

	private void RequestUrlCallBack(string json, bool isSuccess, string text)
	{
		if (isSuccess)
		{
			WorkCountJson workCountJson = JsonConvert.DeserializeObject<WorkCountJson>(json);
			if (workCountJson.status == 0)
			{
				if (ApplicationModel.LANGUAGE == "zh-cn")
				{
					string text2 = text;
					text = text2 + "," + workCountJson.data.count + "次创作";
				}
				else
				{
					string text2 = text;
					text = text2 + ", " + workCountJson.data.count + " works";
				}
			}
		}
		GetComponent<Text>().text = text;
	}
}
