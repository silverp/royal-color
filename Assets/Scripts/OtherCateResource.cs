using System.Collections.Generic;

public class OtherCateResource
{
	private static readonly int ATLEAST_DOWNLOAD_TO_SHOW = 1;

	public ResourceImg headImg;

	public List<ResourceImg> imgList;

	public string url;

	public bool locked;

	public bool isFade;

	public OtherCateResource(ResourceImg headImg, List<ResourceImg> imgList)
	{
		this.headImg = headImg;
		this.imgList = imgList;
	}

	public bool ShouldShow()
	{
		if (!headImg.hasDownloaded)
		{
			return false;
		}
		return true;
	}
}
