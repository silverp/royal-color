using UnityEngine;
using UnityEngine.UI;

public class MyworkCanvasEvent : MonoBehaviour
{
	[SerializeField]
	private GameObject TopCanvas;

	[SerializeField]
	private GameObject OtherWorkCanvas;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject FollowStatusCanvas;

	[SerializeField]
	private GameObject LoginPanel;

	[SerializeField]
	private GameObject BeanBox;

	public static MyworkCanvasEvent instance;

	[SerializeField]
	private GameObject FansCnt;

	[SerializeField]
	private GameObject DeleteWork;

	[SerializeField] private GameObject WorkShopCanvas;
	[SerializeField] private GameObject pixelArtWorkshop;

	public DeleteWorkEvent deleteWorkEvent;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	public void GotoWorkShopEvent(BaseEditedImg img)
	{
		UIRoot.HideUI();
		OtherWorkCanvas.SetActive(value: false);
		ApplicationModel.lastVisibleScreen = "MyworkCanvas";
		ApplicationModel.currentEditedImg = img;
		if (img is EditedImg)
		{
			WorkShopCanvas.SetActive(value: true);
		}
		else if (img is PixleEditedImg)
		{
			pixelArtWorkshop.SetActive(value: true);
		}
	}

	public void GotoLogin()
	{
		DragEventProxy.Instance.SendOffLineMsg("MyworkCanvas");
	}

	public void GotoOtherCanvas(LikesWork work)
	{
		UnityEngine.Debug.Log("GotoOtherCanvas....");
		TopCanvas.SetActive(value: false);
		UIRoot.HideUI();
		OtherWorkCanvas.SetActive(value: false);
		PublicUserInfoApi.AssignOtherUserInfo(work.user);
		OtherWorkCanvas.SetActive(value: true);
		ApplicationModel.lastVisibleScreen = "MyworkCanvas";
	}

	public void GotoMyfollowPage()
	{
		UnityEngine.Debug.Log("GotoMyfollowPage...");
		Transform transform = FollowStatusCanvas.transform.Find("FollowListPanel");
		transform.GetComponent<FollowListscrollRectAdapter>().returnPage = TopCanvas.name;
		if (TopCanvas.name == "MyworkCanvas")
		{
			transform.GetComponent<FollowListscrollRectAdapter>().option = 0;
			transform.GetComponent<FollowListscrollRectAdapter>().uid = ApplicationModel.userInfo.Uid;
		}
		else
		{
			transform.GetComponent<FollowListscrollRectAdapter>().option = 2;
			transform.GetComponent<FollowListscrollRectAdapter>().uid = ApplicationModel.otherUserInfo.Uid;
		}
		TopCanvas.SetActive(value: false);
		FollowStatusCanvas.SetActive(value: true);
	}

	public void GotoMyfansPage()
	{
		UnityEngine.Debug.Log("GotoMyfansPage...");
		Transform transform = FollowStatusCanvas.transform.Find("FollowListPanel");
		transform.GetComponent<FollowListscrollRectAdapter>().returnPage = TopCanvas.name;
		if (TopCanvas.name == "MyworkCanvas")
		{
			transform.GetComponent<FollowListscrollRectAdapter>().option = 1;
			transform.GetComponent<FollowListscrollRectAdapter>().uid = ApplicationModel.userInfo.Uid;
		}
		else
		{
			transform.GetComponent<FollowListscrollRectAdapter>().option = 3;
			transform.GetComponent<FollowListscrollRectAdapter>().uid = ApplicationModel.otherUserInfo.Uid;
		}
		TopCanvas.SetActive(value: false);
		FollowStatusCanvas.SetActive(value: true);
	}

	public void changeFollowCnt(int x)
	{
		if (TopCanvas.name == "MyworkCanvas")
		{
			ApplicationModel.userInfo.FollowCnt += x;
			FansCnt.GetComponent<Text>().text = ApplicationModel.userInfo.FansCnt.ToString();
		}
	}

	public void ShowDeleteWork(string imageid)
	{
		DeleteWork.SetActive(value: true);
		deleteWorkEvent.imageId = imageid;
	}
}
