using UnityEngine;
using UnityEngine.EventSystems;

public class DiscPanelRotationController : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	[SerializeField]
	public RectTransform[] items;

	private bool drag;

	private int r = 280;

	private int count = 18;

	private int index;

	private float PieAngle;

	private float constant;

	private void Start()
	{
		PieAngle = 360f / (float)count;
		constant = (float)(180.0 / (3.14 * (double)r));
		UnityEngine.Debug.LogWarning("Pie Angle is :" + PieAngle);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		drag = true;
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (drag)
		{
			Transform transform = base.transform;
			Vector2 delta = eventData.delta;
			transform.Rotate(0f, 0f, 0f - GetRotateZ(delta.x));
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		drag = false;
	}

	public float GetRotateZ(float distance)
	{
		return distance * constant;
	}
}
