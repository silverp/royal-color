using UnityEngine;

public class BottomIsvisible : MonoBehaviour
{
	[SerializeField]
	private GameObject BottomCanvas;

	private void OnEnable()
	{
		BottomCanvas.SetActive(value: true);
	}

	private void OnDisable()
	{
		BottomCanvas.SetActive(value: false);
	}
}
