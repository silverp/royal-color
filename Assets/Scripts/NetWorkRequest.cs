using Newtonsoft.Json;
using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine;

public class NetWorkRequest : MonoBehaviour
{
	private static int TotalCntDownloadUrl = 6;

	private static int CurDownloadUrl;

	public static readonly int CATE_NEW_CID = 100;

	public static readonly int CATE_POPULAR_CID = 101;

	public static readonly int CATE_FREE_CID = 102;

	public static readonly int CATE_XIANGSU_ART = 103; 

	private void Update()
	{
		checkIfDownloadOk();
	}

	private void checkIfDownloadOk()
	{
		if (CurDownloadUrl >= TotalCntDownloadUrl && !LoadingPageProgressBarController.instance.isFinishLoadInfo)
		{
			LoadingPageProgressBarController.instance.isFinishLoadInfo = true;
			CurDownloadUrl = 0;
			PublicUserInfoApi.JudgeProductIsExpire();
		}
	}

	private static bool IsExistsCid(int cid)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return true;
			}
		}
		return false;
	}

	private static void loadColorsGradient()
	{
		PublicControllerApi.GetGradientColorData(loadColorsGradientDataFromJson);
	}

	private static void loadColorsGradientDataFromJson(string json, Errcode err)
	{
		if (err.errorCode == Errcode.OK)
		{
			// GradientColorData data = gradientColorDataJson.data;
			if (ApplicationModel.ColorThemesGradient == null)
			{
				GradientColorData data = JsonConvert.DeserializeObject<GradientColorData>(json);

				LoadGradiantImg(data.gradientImageUrl);
				data.isPrefab = false;
				ApplicationModel.ColorThemesGradient = data;
			}
		}
	}

	private static void LoadGradiantImg(string url)
	{
		// NetLoadcontroller.Instance.RequestImg(url, delegate
		// {
		// 	UnityEngine.Debug.Log("update gradient image..... " + url);
		// });
	}

	public static IEnumerator DownloadInfoAndImgOnLoading()
	{
		LoadDeviceInfo();
		LoadUserInfo();
		LoadAllCateResourceInfo();
		// loadColorsTheme();
		// loadColorsPure();
		// loadColorsGradient();
		yield return null;
	}

	public static void loadProductList()
	{
		PublicControllerApi.RequestProductList(loadProductListCallbck);
	}

	private static void loadProductListCallbck(ProductListJson json)
	{
		if (json.status == 0)
		{
			ApplicationModel.productList = json.data;
		}
		CurDownloadUrl++;
	}

	public static void DownloadCateListInfo(Action callback)
	{
		URLRequest request = APIFactory.create(APIFactory.ALL_CATEGORY);
		DataBroker.getJson(request, delegate(AllCateResourceJson detail)
		{
			if (detail.status == 0)
			{
				ApplicationModel.CateResourceList = detail.data;
			}
			callback();
		}, delegate
		{
			callback();
		});
	}

	private static void LoadUserInfo()
	{
		if (ApplicationModel.sessionId != null)
		{
			PublicUserInfoApi.RequestUserInfo(ApplicationModel.sessionId, RequestUserInfoCallback);
		}
		else
		{
			CurDownloadUrl++;
		}
	}

	private static void LoadDeviceInfo()
	{
		if (ApplicationModel.device_id != null || ApplicationModel.device_id != string.Empty)
		{
			PublicControllerApi.getDeviceInfo(ApplicationModel.device_id, RequestDeviceInfoCallback);
		}
		else
		{
			CurDownloadUrl++;
		}
	}

	private static void RequestDeviceInfoCallback(Errcode err, DeviceVipInfo deviceVipInfo)
	{
		if (err.errorCode == Errcode.OK)
		{
			if (ApplicationModel.deviceVipInfo == null)
			{
				ApplicationModel.deviceVipInfo = new DeviceVipInfo();
			}
			ApplicationModel.deviceVipInfo.vipExpired = deviceVipInfo.vipExpired;
			ApplicationModel.deviceVipInfo.vipLevel = deviceVipInfo.vipLevel;
			ApplicationModel.deviceVipInfo.deviceId = deviceVipInfo.deviceId;
		}
		else
		{
			CurDownloadUrl++;
		}
	}

	private static void RequestUserInfoCallback(Errcode err)
	{
		UnityEngine.Debug.Log("network RequestUserInfoCallback....");
		if (err.errorCode == Errcode.OK)
		{
			PublicUserInfoApi.StartMergeData();
			PublicUserInfoApi.JudgeProductIsExpire();
			PublicUserInfoApi.UploadUserEditedImgList();
		}
		else
		{
			UnityEngine.Debug.Log("request error!!!");
		}
		CurDownloadUrl++;
	}

	private static void LoadAllCateResourceInfo()
	{
		URLRequest request = APIFactory.create(APIFactory.ALL_CATEGORY);
		DataBroker.getJson(request, delegate(AllCateResourceJson detail)
		{
			if (detail.status == 0)
			{
				ApplicationModel.CateResourceList = detail.data;
			}
			CurDownloadUrl++;
		}, delegate
		{
			CurDownloadUrl++;
		});
	}

	// public static void loadColorsTheme()
	// {
	// 	NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.COLOR_LIST, delegate(string json, bool isSuccess)
	// 	{
	// 		loadColorsDataFromJson(json, isSuccess, isSolidColor: false);
	// 	});
	// }
	//
	// public static void loadColorsPure()
	// {
	// 	NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.COLOR_LIST_PURE, delegate(string json, bool isSuccess)
	// 	{
	// 		loadColorsDataFromJson(json, isSuccess, isSolidColor: true);
	// 	});
	// }

	// public static void loadColorsDataFromJson(string json, bool isSuccess, bool isSolidColor)
	// {
	// 	if (isSuccess)
	// 	{
	// 		ColorJson colorJson = JsonConvert.DeserializeObject<ColorJson>(json);
	// 		if (isSolidColor)
	// 		{
	// 			ApplicationModel.ColorThemesPure.Clear();
	// 			foreach (ColorThemeJson datum in colorJson.data)
	// 			{
	// 				ColorTheme item = new ColorTheme(datum.id, datum.name, datum.colors, datum.bgcolor);
	// 				ApplicationModel.ColorThemesPure.Add(item);
	// 			}
	// 		}
	// 		else
	// 		{
	// 			ApplicationModel.ColorThemes.Clear();
	// 			foreach (ColorThemeJson datum2 in colorJson.data)
	// 			{
	// 				ColorTheme item2 = new ColorTheme(datum2.id, datum2.name, datum2.colors, datum2.bgcolor);
	// 				ApplicationModel.ColorThemes.Add(item2);
	// 			}
	// 		}
	// 	}
	// 	CurDownloadUrl++;
	// }
}
