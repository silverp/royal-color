using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRoot : MonoBehaviour
{
    private static UIRoot instance;

    [SerializeField] private ScrollSnapRect _scrollSnapRect;
    [SerializeField] private GameObject[] innerScreens;

    private void Awake()
    {
        instance = this;
    }

    public static void HideUI()
    {
        instance?.gameObject?.SetActive(false);
    }

    public static void ShowUI(GameObject ui)
    {
        instance?.gameObject.SetActive(true);
        instance?._scrollSnapRect?.ScrollTo(ui.transform);
    }

    public static void Back()
    {
        instance?.gameObject.SetActive(true);
    }

    public static void ShowBack(GameObject canvasList, string name)
    {
        if (!instance) return;
        foreach(var i in instance.innerScreens)
        {
            if (i.name == name)
            {
                ShowUI(i);
                return;
            }
        }
        canvasList.transform.Find(name).gameObject.SetActive(value: true);
    }
}
