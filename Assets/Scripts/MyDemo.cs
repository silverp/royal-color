using cn.sharesdk.unity3d;
using System;
using System.Collections;
using UnityEngine;

public class MyDemo : MonoBehaviour
{
	public GUISkin demoSkin;

	public ShareSDK ssdk;

	private void Start()
	{
		ssdk = base.gameObject.GetComponent<ShareSDK>();
		ssdk.authHandler = OnAuthResultHandler;
		ssdk.shareHandler = OnShareResultHandler;
		ssdk.showUserHandler = OnGetUserInfoResultHandler;
		ssdk.getFriendsHandler = OnGetFriendsResultHandler;
		ssdk.followFriendHandler = OnFollowFriendResultHandler;
	}

	private void OnGUI()
	{
		GUI.skin = demoSkin;
		float num = 1f;
		if (Application.platform == RuntimePlatform.IPhonePlayer)
		{
			num = Screen.width / 320;
		}
		float num2 = 165f * num;
		float num3 = 30f * num;
		float num4 = 20f * num;
		float num5 = 20f * num;
		GUI.skin.button.fontSize = Convert.ToInt32(14f * num);
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f - num2, num4, num2, num3), "Authorize"))
		{
			MonoBehaviour.print(ssdk == null);
			ssdk.Authorize(PlatformType.Facebook);
		}
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f + num5, num4, num2, num3), "Get User Info"))
		{
			ssdk.GetUserInfo(PlatformType.Facebook);
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f - num2, num4, num2, num3), "Show Share Menu"))
		{
			ShareContent shareContent = new ShareContent();
			shareContent.SetText("this is a test string.");
			shareContent.SetImageUrl("http://ww3.sinaimg.cn/mw690/be159dedgw1evgxdt9h3fj218g0xctod.jpg");
			shareContent.SetTitle("test title");
			shareContent.SetTitleUrl("http://www.mob.com");
			shareContent.SetSite("Mob-ShareSDK");
			shareContent.SetSiteUrl("http://www.mob.com");
			shareContent.SetUrl("http://www.mob.com");
			shareContent.SetComment("test description");
			shareContent.SetMusicUrl("http://mp3.mwap8.com/destdir/Music/2009/20090601/ZuiXuanMinZuFeng20090601119.mp3");
			shareContent.SetShareType(2);
			ShareContent shareContent2 = new ShareContent();
			shareContent2.SetText("Sina share content");
			shareContent2.SetImageUrl("http://git.oschina.net/alexyu.yxj/MyTmpFiles/raw/master/kmk_pic_fld/small/107.JPG");
			shareContent2.SetShareType(1);
			shareContent2.SetObjectID("SinaID");
			shareContent.SetShareContentCustomize(PlatformType.SinaWeibo, shareContent2);
			ssdk.ShowPlatformList(null, shareContent, 100, 100);
		}
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f + num5, num4, num2, num3), "Show Share View"))
		{
			ShareContent shareContent3 = new ShareContent();
			shareContent3.SetText("this is a test string.");
			shareContent3.SetImageUrl("http://ww3.sinaimg.cn/mw690/be159dedgw1evgxdt9h3fj218g0xctod.jpg");
			shareContent3.SetTitle("test title");
			shareContent3.SetTitleUrl("http://www.mob.com");
			shareContent3.SetSite("Mob-ShareSDK");
			shareContent3.SetSiteUrl("http://www.mob.com");
			shareContent3.SetUrl("http://www.mob.com");
			shareContent3.SetComment("test description");
			shareContent3.SetMusicUrl("http://mp3.mwap8.com/destdir/Music/2009/20090601/ZuiXuanMinZuFeng20090601119.mp3");
			shareContent3.SetShareType(2);
			ssdk.ShowShareContentEditor(PlatformType.SinaWeibo, shareContent3);
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f - num2, num4, num2, num3), "Share Content"))
		{
			ShareContent shareContent4 = new ShareContent();
			shareContent4.SetText("this is a test string.");
			shareContent4.SetImageUrl("http://ww3.sinaimg.cn/mw690/be159dedgw1evgxdt9h3fj218g0xctod.jpg");
			shareContent4.SetTitle("test title");
			shareContent4.SetUrl("http://qjsj.youzu.com/jycs/");
			shareContent4.SetShareType(4);
			ssdk.ShareContent(PlatformType.WeChat, shareContent4);
		}
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f + num5, num4, num2, num3), "Get Friends SinaWeibo "))
		{
			MonoBehaviour.print("Click Btn Of Get Friends SinaWeibo");
			ssdk.GetFriendList(PlatformType.SinaWeibo, 15, 0);
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f - num2, num4, num2, num3), "Get Token SinaWeibo "))
		{
			Hashtable authInfo = ssdk.GetAuthInfo(PlatformType.SinaWeibo);
			MonoBehaviour.print("share result :");
			MonoBehaviour.print(MiniJSON.jsonEncode(authInfo));
		}
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f + num5, num4, num2, num3), "Close SSO Auth"))
		{
			ssdk.DisableSSO(open: true);
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f - num2, num4, num2, num3), "Remove Authorize "))
		{
			ssdk.CancelAuthorize(PlatformType.Facebook);
		}
		if (GUI.Button(new Rect(((float)Screen.width - num5) / 2f + num5, num4, num2, num3), "Add Friend "))
		{
			ssdk.AddFriend(PlatformType.SinaWeibo, "3189087725");
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num2) / 2f, num4, num2, num3), "ShareWithContentName"))
		{
			Hashtable hashtable = new Hashtable();
			hashtable["imgUrl"] = "http://ww1.sinaimg.cn/mw690/006dJESWgw1f6iyb8bzraj31kw0v67a2.jpg";
			ssdk.ShareWithContentName(PlatformType.SinaWeibo, "ShareSDK", hashtable);
		}
		num2 += 80f * num;
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num2) / 2f, num4, num2, num3), "ShowShareMenuWithContentName"))
		{
			Hashtable hashtable2 = new Hashtable();
			hashtable2["imgUrl"] = "http://ww1.sinaimg.cn/mw690/006dJESWgw1f6iyb8bzraj31kw0v67a2.jpg";
			ssdk.ShowPlatformListWithContentName("ShareSDK", hashtable2, null, 100, 100);
		}
		num4 += num3 + 20f * num;
		if (GUI.Button(new Rect(((float)Screen.width - num2) / 2f, num4, num2, num3), "ShowShareViewWithContentName"))
		{
			Hashtable hashtable3 = new Hashtable();
			hashtable3["imgUrl"] = "http://ww1.sinaimg.cn/mw690/006dJESWgw1f6iyb8bzraj31kw0v67a2.jpg";
			ssdk.ShowShareContentEditorWithContentName(PlatformType.SinaWeibo, "ShareSDK", hashtable3);
		}
	}

	private void OnAuthResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("authorize success !Platform :" + type);
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			break;
		}
	}

	private void OnGetUserInfoResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("get user info result :");
			MonoBehaviour.print(MiniJSON.jsonEncode(result));
			MonoBehaviour.print("AuthInfo:" + MiniJSON.jsonEncode(ssdk.GetAuthInfo(PlatformType.Facebook)));
			MonoBehaviour.print("Get userInfo success !Platform :" + type);
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			break;
		}
	}

	private void OnShareResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("share successfully - share result :");
			MonoBehaviour.print(MiniJSON.jsonEncode(result));
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			break;
		}
	}

	private void OnGetFriendsResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("get friend list result :");
			MonoBehaviour.print(MiniJSON.jsonEncode(result));
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			break;
		}
	}

	private void OnFollowFriendResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("Follow friend successfully !");
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			break;
		}
	}
}
