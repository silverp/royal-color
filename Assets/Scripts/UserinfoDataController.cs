using Newtonsoft.Json;
using System;
using UnityEngine;
using UnityEngine.UI;

public class UserinfoDataController : MonoBehaviour
{
	[SerializeField]
	private GameObject beanCnt;

	[SerializeField]
	private GameObject followCnt;

	[SerializeField]
	private GameObject fansCnt;

	[SerializeField]
	private GameObject vipIcon;

	[SerializeField]
	private GameObject Purchase;

	[SerializeField]
	private GameObject beans;

	[SerializeField]
	private GameObject sign;

	private void Awake()
	{
	}

	private void OnEnable()
	{
		RefreshUserInfo();
		AssginEvents();
		showExpireTime();
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		DragEventProxy.OnAddFollowCnt += changeFollowCnt;
		DragEventProxy.OnRefreshBeanCnt += RefreshBeanCnt;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnAddFollowCnt -= changeFollowCnt;
		DragEventProxy.OnRefreshBeanCnt -= RefreshBeanCnt;
	}

	private void RefreshBeanCnt(int amount)
	{
		beanCnt.GetComponent<Text>().text = ApplicationModel.userInfo.BeanCnt.ToString();
	}

	private void showExpireTime()
	{
		if (PublicUserInfoApi.IsHasLogin())
		{
			if (ApplicationModel.userInfo.VipLevel != 0)
			{
				PublicControllerApi.GetExpiredTime(RequestVipExpiredCallback);
				return;
			}
			vipIcon.transform.GetChild(0).GetComponent<Text>().text = PublicToolController.GetTextByLanguage("Not open vip") + ">>";
			addClickEvent();
		}
	}

	private void addClickEvent()
	{
		vipIcon.GetComponent<Button>().onClick.RemoveAllListeners();
		vipIcon.GetComponent<Button>().onClick.AddListener(delegate
		{
			ShowPurchasePanel();
		});
	}

	private void ShowPurchasePanel()
	{
		Purchase.SetActive(value: true);
		RectTransform component = Purchase.transform.GetComponent<RectTransform>();
		Vector2 anchoredPosition = Purchase.transform.GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(anchoredPosition.x, -1920f);
		LeanTween.moveY(Purchase.GetComponent<RectTransform>(), 0f, 0.4f).setEase(LeanTweenType.easeOutQuad).setOnComplete((Action)delegate
		{
			Purchase.transform.Find("GameObject").GetComponent<OpenVipController>().returnPage = "ObtainBeanCanvas";
		});
	}

	private void RequestVipExpiredCallback(VipExpired vipExpired)
	{
		if (vipExpired != null)
		{
			vipIcon.transform.GetChild(0).GetComponent<Text>().text = PublicToolController.ConvertStringToDateTime(vipExpired.vipExpired.ToString()).ToString("yyyy-MM-dd") + PublicToolController.GetTextByLanguage("expire");
		}
	}

	private void RefreshUserInfo()
	{
		if (ApplicationModel.userInfo != null)
		{
			URLRequest uRLRequest = APIFactory.create(APIFactory.USER_INFO);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
			DataBroker.getJson(uRLRequest, delegate(UserDataJson detail)
			{
				if (detail.status == 0)
				{
					RefreshUserInfoCallback(detail.data);
					UnityEngine.Debug.Log("user info is:" + detail.data);
				}
			}, delegate(Exception ex)
			{
				UnityEngine.Debug.Log(ex);
			});
		}
	}

	private void RefreshUserInfoCallback(UserInfo userInfo)
	{
		UnityEngine.Debug.Log("userInfo is:" + JsonConvert.SerializeObject(userInfo));
		ApplicationModel.userInfo = userInfo;
		beanCnt.GetComponent<Text>().text = userInfo.BeanCnt.ToString();
		followCnt.GetComponent<Text>().text = userInfo.FollowCnt.ToString();
		fansCnt.GetComponent<Text>().text = userInfo.FansCnt.ToString();
		vipIcon.GetComponent<RawImage>().texture = PublicImgaeApi.GetVipIcon(userInfo.VipLevel);
	}

	private void changeFollowCnt(int x)
	{
		UnityEngine.Debug.Log("changeFollowCnt...." + x);
		ApplicationModel.userInfo.FollowCnt += x;
		followCnt.GetComponent<Text>().text = ApplicationModel.userInfo.FollowCnt.ToString();
	}
}
