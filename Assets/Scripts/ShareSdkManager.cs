using cn.sharesdk.unity3d;
using System;
using System.Collections;
using UI.ThreeDimensional;
using UnityEngine;
using UnityEngine.UI;

public class ShareSdkManager : MonoBehaviour
{
	public GUISkin demoSkin;

	public ShareSDK ssdk;

	[SerializeField]
	private Transform shareObjectprefab;

	[SerializeField]
	private Transform inviteObjectprefab;

	public UITemplate template;

	private Action<Errcode> shareCallback;

	private bool shareLock;

	private PlatformType[] plats;

	public static event Action<Errcode, PlatformType> LoginErrorCallback;

	public static event Action<Hashtable, Hashtable, PlatformType> LoginSuccessCallback;

	private void Start()
	{
		ssdk = base.gameObject.GetComponent<ShareSDK>();
		ssdk.authHandler = OnAuthResultHandler;
		ssdk.shareHandler = OnShareResultHandler;
		ssdk.showUserHandler = OnGetUserInfoResultHandler;
		ssdk.getFriendsHandler = OnGetFriendsResultHandler;
		ssdk.followFriendHandler = OnFollowFriendResultHandler;
		plats = new PlatformType[1];
		//plats[0] = PlatformType.SinaWeibo;
		//plats[1] = PlatformType.WeChat;
		//plats[2] = PlatformType.QQ;
		plats[0] = PlatformType.Facebook;
	}

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		FileUploadEventProxy.OnShareWorkEvent += OnShareClick;
		FileUploadEventProxy.OnInviteFriend += OnInviteClick;
	}

	public bool IsShowIcon(PlatformType type)
	{
		bool flag = false;
		return true;
	}

	private void DiscardEvents()
	{
		FileUploadEventProxy.OnShareWorkEvent -= OnShareClick;
		FileUploadEventProxy.OnInviteFriend -= OnInviteClick;
	}

	private void Login(PlatformType type)
	{
		MonoBehaviour.print(ssdk == null);
		switch (type)
		{
		case PlatformType.WeChat:
			ssdk.GetUserInfo(PlatformType.WeChat);
			break;
		case PlatformType.SinaWeibo:
			ssdk.GetUserInfo(PlatformType.SinaWeibo);
			break;
		case PlatformType.QQ:
			ssdk.Authorize(PlatformType.QQ);
			break;
		case PlatformType.Facebook:
			ssdk.GetUserInfo(PlatformType.Facebook);
			break;
		default:
			ssdk.GetUserInfo(PlatformType.WeChat);
			break;
		}
	}

	public void LogOut()
	{
		string platform = ApplicationModel.userInfo.Platform;
		if (platform == null)
		{
			return;
		}
		if (!(platform == "QQ"))
		{
			if (!(platform == "wechat"))
			{
				if (!(platform == "sinaWeibo"))
				{
					if (platform == "facebook")
					{
						ssdk.CancelAuthorize(PlatformType.Facebook);
					}
				}
				else
				{
					ssdk.CancelAuthorize(PlatformType.SinaWeibo);
				}
			}
			else
			{
				ssdk.CancelAuthorize(PlatformType.WeChat);
			}
		}
		else
		{
			ssdk.CancelAuthorize(PlatformType.QQ);
		}
	}

	public void LoginWechat()
	{
		UnityEngine.Debug.Log("login wechat");
		Login(PlatformType.WeChat);
	}

	public void LoginQQ()
	{
		UnityEngine.Debug.Log("login qq");
		Login(PlatformType.QQ);
	}

	public void LoginSinaWeibo()
	{
		UnityEngine.Debug.Log("login weibo");
		Login(PlatformType.SinaWeibo);
	}

	public void LoginFacebook()
	{
		UnityEngine.Debug.Log("login facebook");
		Login(PlatformType.Facebook);
	}

	private void OnAuthResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		UnityEngine.Debug.Log("OnAuthResultHandler...");
		UnityEngine.Debug.Log("type is:" + type.ToString());
		Errcode errcode = new Errcode();
		switch (state)
		{
		case ResponseState.Success:
			MonoBehaviour.print("authorize success !Platform :" + type);
			ShareSdkManager.LoginSuccessCallback(result, ssdk.GetAuthInfo(type), type);
			break;
		case ResponseState.Fail:
			MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
			errcode.errorCode = Errcode.FAIL;
			ShareSdkManager.LoginErrorCallback(errcode, type);
			break;
		case ResponseState.Cancel:
			MonoBehaviour.print("cancel !");
			errcode.errorCode = Errcode.LOGIN_CANCEL;
			ShareSdkManager.LoginErrorCallback(errcode, type);
			break;
		}
	}

	private void OnShareRenderCallback(Texture2D texture)
	{
		UnityEngine.Debug.Log("OnRenderCallback:");
		if (texture != null)
		{
			string text = "share_test" + DateTime.Now.ToString("dd-MM-yy_hhmmss");
			string text2 = Application.persistentDataPath + "/" + text + ".jpg";
			PublicToolController.FileCreatorBytesForJpg(texture.EncodeToJPG(100), text);
			ShareContent shareContent = new ShareContent();
			UnityEngine.Debug.Log("share imagepath is:" + text2);
			shareContent.SetImagePath(text2);
			shareContent.SetShareType(2);
			ssdk.ShowPlatformList(plats, shareContent, 100, 100);
		}
		template.OnRenderCallback -= OnShareRenderCallback;
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void ShareRender(Texture2D texImage, string name, string info, bool isHasInfo)
	{
		UnityEngine.Debug.Log("Render");
		template = GetComponent<UITemplate>();
		template.OnRenderCallback += OnShareRenderCallback;
		Transform child = template.target.GetChild(0);
		if (isHasInfo)
		{
			TextEmojiConvert.Instance.ConvertEmoji(child.GetChild(0), name, isBig: false);
			UnityEngine.Debug.Log("change topPanelTransform name is:" + child.GetChild(0).GetComponent<Text>().text);
			child.GetChild(1).gameObject.SetActive(value: false);
			child.GetChild(2).GetComponent<Text>().text = info;
		}
		else
		{
			UnityEngine.Debug.Log("else isHasInfo");
			child.GetChild(0).GetComponent<Text>().text = string.Empty;
			child.GetChild(1).gameObject.SetActive(value: true);
			child.GetChild(2).GetComponent<Text>().text = string.Empty;
		}
		template.target.GetChild(1).GetComponent<RawImage>().texture = texImage;
		UnityEngine.Debug.Log("FINAL topPanelTransform name is:" + child.GetChild(0).GetComponent<Text>().text);
		template.renderPreferb();
	}

	private void OnShareClick(Texture2D texture, string name, int count, bool isHasInfo, Action<Errcode> callback)
	{
		if (!shareLock)
		{
			UnityEngine.Debug.Log("OnShareClick....");
			GetComponent<UITemplate>().ObjectPrefab = shareObjectprefab;
			TotalGA.Event("share");
			ShareRender(texture, "@" + name, PublicToolController.GetShareStrByCount(count), isHasInfo);
			shareCallback = callback;
			shareLock = true;
		}
		StartCoroutine(RecoverClick());
	}

	private IEnumerator RecoverClick()
	{
		yield return new WaitForSeconds(2f);
		shareLock = false;
	}

	private void OnGetUserInfoResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
	{
		Errcode errcode = new Errcode();
		if (state == ResponseState.Success)
		{
			MonoBehaviour.print("get user info result :");
			MonoBehaviour.print(MiniJSON.jsonEncode(result));
			MonoBehaviour.print("auth info is:" + MiniJSON.jsonEncode(ssdk.GetAuthInfo(type)));
			ShareSdkManager.LoginSuccessCallback(result, ssdk.GetAuthInfo(type), type);
		}
		else
		{
			MonoBehaviour.print("OnGetUserInfoResultHandler cancel !");
			errcode.errorCode = Errcode.LOGIN_CANCEL;
			ShareSdkManager.LoginErrorCallback(errcode, type);
		}
	}

	private void OnInviteRenderCallback(Texture2D texture)
	{
		if (texture != null)
		{
			string text = "share_test" + DateTime.Now.ToString("dd-MM-yy_hhmmss");
			string text2 = Application.persistentDataPath + "/" + text + ".jpg";
			PublicToolController.FileCreatorBytesForJpg(texture.EncodeToJPG(100), text);
			ShareContent shareContent = new ShareContent();
			UnityEngine.Debug.Log("share imagepath is:" + text2);
			shareContent.SetImagePath(text2);
			shareContent.SetShareType(2);
			ssdk.ShowPlatformList(plats, shareContent, 100, 100);
		}
		template.OnRenderCallback -= OnInviteRenderCallback;
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void InviteRender(Texture2D headImg, string nickname)
	{
		template = GetComponent<UITemplate>();
		template.OnRenderCallback += OnInviteRenderCallback;
		template.target.transform.Find("headimg").Find("mask").Find("Image")
			.GetComponent<RawImage>()
			.texture = headImg;
			template.target.transform.Find("name").GetComponent<Text>().text = nickname;
			template.renderPreferb();
		}

		private void OnInviteClick(Texture2D texture, string name, Action<Errcode> callback)
		{
			if (!shareLock)
			{
				GetComponent<UITemplate>().ObjectPrefab = inviteObjectprefab;
				InviteRender(texture, name);
				shareCallback = callback;
				shareLock = true;
			}
			StartCoroutine(RecoverClick());
		}

		private void OnShareResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
		{
			Errcode errcode = new Errcode();
			switch (state)
			{
			case ResponseState.Success:
				MonoBehaviour.print("share successfully - share result :");
				MonoBehaviour.print(MiniJSON.jsonEncode(result));
				errcode.errorCode = Errcode.OK;
				TotalGA.Event("share_success");
				break;
			case ResponseState.Fail:
				MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
				errcode.errorCode = Errcode.FAIL;
				TotalGA.Event("share_fail");
				break;
			case ResponseState.Cancel:
				MonoBehaviour.print("cancel !");
				errcode.errorCode = Errcode.SHARE_CANCEL;
				TotalGA.Event("share_cancel");
				break;
			}
			shareLock = false;
			shareCallback(errcode);
		}

		private void OnGetFriendsResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
		{
			switch (state)
			{
			case ResponseState.Success:
				MonoBehaviour.print("get friend list result :");
				MonoBehaviour.print(MiniJSON.jsonEncode(result));
				break;
			case ResponseState.Fail:
				MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
				break;
			case ResponseState.Cancel:
				MonoBehaviour.print("cancel !");
				break;
			}
		}

		private void OnFollowFriendResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
		{
			switch (state)
			{
			case ResponseState.Success:
				MonoBehaviour.print("Follow friend successfully !");
				break;
			case ResponseState.Fail:
				MonoBehaviour.print("fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"]);
				break;
			case ResponseState.Cancel:
				MonoBehaviour.print("cancel !");
				break;
			}
		}
	}
