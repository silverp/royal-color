using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ImageIsvisibleController : MonoBehaviour
{
	public bool isDownLoad;

	public bool isSatrtDownload;

	public string canvasName = string.Empty;

	private bool isInMycanvas;

	private void Start()
	{
		if (PublicToolController.FindParentWithTag(base.gameObject, "MyworkCanvas") != null)
		{
			isInMycanvas = true;
		}
	}

	private void Update()
	{
	}

	private void OnWillRenderObject()
	{
		isDownLoad = base.transform.GetComponent<ImageDataController>().isDownLoad;
		if (!isDownLoad && !isSatrtDownload && !isInMycanvas)
		{
			isSatrtDownload = true;
			StartCoroutine(downloadImageAndSet(getResourceImg(base.transform.GetComponent<ImageDataController>().cid, base.transform.GetComponent<ImageDataController>().imageId)));
		}
	}

	private IEnumerator downloadImageAndSet(ResourceImg img)
	{
		WWW www = new WWW(img.thumbnailUrl);
		yield return www;
		if (!string.IsNullOrEmpty(www.error))
		{
			yield return null;
			yield break;
		}
		Texture2D texture = www.texture;
		string thumbnailPath = img.thumbnailPath;
		byte[] fileData = texture.EncodeToPNG();
		try
		{
			PublicToolController.FileCreatorBytes(fileData, thumbnailPath);
			img.SetThumbnailDownLoad(isDownload: true);
		}
		catch (IOException message)
		{
			MonoBehaviour.print(message);
		}
		base.transform.Find("Image").GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f, 0u, SpriteMeshType.FullRect);
		isDownLoad = true;
		base.transform.GetComponent<ImageDataController>().isDownLoad = true;
	}

	private CateResource getCateResourceViaCid(int cid)
	{
		foreach (CateResource cateResource in ApplicationModel.CateResourceList)
		{
			if (cateResource.cid == cid)
			{
				return cateResource;
			}
		}
		return null;
	}

	private ResourceImg getResourceImg(int cid, string imageId)
	{
		foreach (ResourceImg img in getCateResourceViaCid(cid).imgList)
		{
			if (img.imageId == imageId)
			{
				return img;
			}
		}
		return null;
	}
}
