using System;
using System.Text;
using Unicache;
using Unicache.Plugin;
using UniRx;
using UnityEngine;

public class HttpRequest
{
	public enum CacheType
	{
		None,
		FILE,
		MEMORY
	}

	private IUnicache fileCache;

	private IUnicache memoryCache;

	protected static HttpRequest _instance;

	public static HttpRequest Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new HttpRequest();
			}
			return _instance;
		}
	}

	public HttpRequest()
	{
		fileCache = new FileCache(UrlCache.Instance.gameObject);
		fileCache.Handler = new SimpleDownloadHandler();
		fileCache.UrlLocator = new SimpleUrlLocator();
		fileCache.CacheLocator = new SimpleCacheLocator();
		memoryCache = new MemoryCache();
		memoryCache.Handler = new SimpleDownloadHandler();
		memoryCache.UrlLocator = new SimpleUrlLocator();
		memoryCache.CacheLocator = new SimpleCacheLocator();
	}

	public void RequestTexture(string url, CacheType cacheType, Action<Texture2D> callback, Action<Exception> onError)
	{
		RequestBytes(url, cacheType, delegate(byte[] res)
		{
			Texture2D texture2D = new Texture2D(0, 0, TextureFormat.RGBA32, mipChain: false)
			{
				filterMode = FilterMode.Point,
				wrapMode = TextureWrapMode.Clamp
			};
			texture2D.LoadImage(res);
			callback(texture2D);
		}, onError);
	}

	public void RequestBytes(string url, CacheType cacheType, Action<byte[]> callback, Action<Exception> onError)
	{
		switch (cacheType)
		{
		case CacheType.FILE:
			fileCache.Fetch(url).Subscribe(delegate(byte[] bytes)
			{
				callback(bytes);
			}, delegate(Exception error)
			{
				onError(error);
			});
			break;
		case CacheType.MEMORY:
			memoryCache.Fetch(url).Subscribe(delegate(byte[] bytes)
			{
				callback(bytes);
			}, delegate(Exception error)
			{
				onError(error);
			});
			break;
		default:
			ObservableWWW.GetAndGetBytes(url).Subscribe(delegate(byte[] x)
			{
				callback(x);
			}, delegate(Exception ex)
			{
				onError(ex);
			});
			break;
		}
	}

	public void RequestString(string url, CacheType cacheType, Action<string> callback, Action<Exception> onError)
	{
		RequestBytes(url, cacheType, delegate(byte[] res)
		{
			callback(Encoding.UTF8.GetString(res));
		}, onError);
	}

	public void WWWGetRequest(string url, Action<string> callback, Action<Exception> onError)
	{
		ObservableWWW.Get(url).Subscribe(delegate(string x)
		{
			callback(x);
		}, delegate(Exception ex)
		{
			onError(ex);
		});
	}

	public void WWWPostRequest(string url, WWWForm body, Action<string> callback, Action<Exception> onError)
	{
		ObservableWWW.Post(url, body).Subscribe(delegate(string x)
		{
			callback(x.ToString());
		}, delegate(Exception ex)
		{
			onError(ex);
		});
	}
}
