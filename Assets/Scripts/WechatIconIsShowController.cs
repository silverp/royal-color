using cn.sharesdk.unity3d;
using UnityEngine;

public class WechatIconIsShowController : MonoBehaviour
{
	[SerializeField]
	private GameObject shareSdk;

	private void Start()
	{
		if (!shareSdk.GetComponent<ShareSdkManager>().IsShowIcon(PlatformType.WeChat))
		{
			base.gameObject.SetActive(value: false);
		}
	}

	private void Update()
	{
	}
}
