using Newtonsoft.Json;

public class PixleEditedImg : BaseEditedImg
{
	public string baseUrl;

	public string colorUrl;

	public string countUrl;

	public string colorMapUrl;

	public PixleEditedImg(string imageId, string baseUrl, string colorUrl, string countUrl, bool isRemote)
	{
		base.imageId = imageId;
		base.isRemote = isRemote;
		this.baseUrl = baseUrl;
		this.colorUrl = colorUrl;
		this.countUrl = countUrl;
	}

	[JsonConstructor]
	public PixleEditedImg(string imageId, string workId, string finalImg, string thumb, int likeCnt, bool isLike, long create, bool hasEdited)
	{
		base.imageId = imageId;
		base.workId = workId;
		base.finalImg = finalImg;
		base.thumb = thumb;
		base.likeCnt = likeCnt;
		base.isLike = isLike;
		base.create = create;
		base.hasEdited = hasEdited;
	}
}
