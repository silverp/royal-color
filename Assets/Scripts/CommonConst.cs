public class CommonConst
{
	public const string ADMOB_ANDROID = "ca-app-pub-2726291437530559/4974510420";

	public const string VIDEO_ADMOB_ANDROID = "ca-app-pub-2726291437530559/6789967620";

	public const string ADMOB_IOS = "ca-app-pub-2726291437530559/4974510420";

	public const string VIDEO_ADMOB_IOS = "ca-app-pub-2726291437530559/6789967620";

	public const string ADMOB_BANNER = "ca-app-pub-2726291437530559/8954592424";

	public const string APP_NAME = "xty";

	public const string IOS_APP_ID = "";

	public const string MAC_APP_ID = "";

	public const string BB_APP_ID = "";

	public const string ANDROID_LINK = "https://play.google.com/store/apps/details?id=com.superpow.six";

	public const string GP_PAGE_LINK = "https://plus.google.com/106625749530285902481/posts";

	public const string GOOGLE_STORE = "https://play.google.com/store/apps/developer?id=Superpow";

	public const string umeng_appkey = "59279932bbea8358c6000506";

	public const bool ENCRYPTION_PREFS = true;

	public const int MIN_LEVEL_TO_RATE = 3;

	public const int ADS_PERIOD = 30;

	public const int NOTIFICATION_DAILY_GIFT = 0;

	public static readonly string[] LEADERBOARD = new string[1]
	{
		"CgkIgvm3v4UYEAIQAA"
	};

	public const int MAX_AUTO_SIGNIN = 2;
}
