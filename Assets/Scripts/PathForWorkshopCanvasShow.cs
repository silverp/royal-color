using UnityEngine;

public class PathForWorkshopCanvasShow : MonoBehaviour
{
	[SerializeField]
	private GameObject WorkShopCanvas;
	[SerializeField]
	private GameObject pixelArtWorkshop;
	private void OnEnable()
	{
		WorkShopCanvas?.SetActive(value: false);
		pixelArtWorkshop?.SetActive(value: false);
	}
}
