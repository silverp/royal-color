using Newtonsoft.Json;

public class ResourceImg
{
	public string path;

	public string type;

	public string imageId;

	public string name;

	public string url;

	public bool hasDownloaded;

	public bool hasEdited;

	public bool isPrefab;

	public string thumbnailUrl;

	public string thumbnailPath;

	public bool thumbnailHasDownload;

	public bool locked;

	public bool isFade;

	public int cid;

	public string albumId;

	public string indexImageId;

	public string indexImageUrl;

	public ResourceImg(string path, string imageId, string name, string url, bool hasDownloaded, bool isPrefab)
		: this(path, imageId, name, url, hasDownloaded, isPrefab, url, path, thumbnailHasDownload: true)
	{
	}

	public ResourceImg(string path, string imageId, string url, string indexImageId, string indexImageUrl, int cid)
	{
		this.path = path;
		this.imageId = imageId;
		this.url = url;
		this.indexImageId = indexImageId;
		this.indexImageUrl = indexImageUrl;
		this.cid = cid;
	}

	public ResourceImg(string imageId, string url, string indexImageUrl, string thumbnailUrl, bool locked)
	{
		this.imageId = imageId;
		this.url = url;
		this.indexImageUrl = indexImageUrl;
		this.locked = locked;
		this.thumbnailUrl = thumbnailUrl;
	}

	[JsonConstructor]
	public ResourceImg(string path, string imageId, string name, string url, bool hasDownloaded, bool isPrefab, string thumbnailUrl, string thumbnailPath, bool thumbnailHasDownload)
	{
		this.thumbnailUrl = thumbnailUrl;
		this.path = path;
		this.imageId = imageId;
		this.name = name;
		this.url = url;
		this.hasDownloaded = hasDownloaded;
		this.isPrefab = isPrefab;
		this.thumbnailPath = thumbnailPath;
		this.thumbnailHasDownload = thumbnailHasDownload;
	}

	public bool HasEdited()
	{
		return hasEdited;
	}

	public bool SetEdited()
	{
		hasEdited = true;
		return true;
	}

	public void SetImageEdit(bool isEdit)
	{
		hasEdited = isEdit;
	}

	public bool HasDownload()
	{
		return hasDownloaded;
	}

	public bool SetDownload()
	{
		return SetDownload(isThumbnail: false);
	}

	public void SetImageDownload(bool isDownload)
	{
		hasDownloaded = isDownload;
	}

	public void SetThumbnailDownLoad(bool isDownload)
	{
		thumbnailHasDownload = isDownload;
	}

	public bool SetDownload(bool isThumbnail)
	{
		if (isThumbnail)
		{
			thumbnailHasDownload = true;
		}
		else
		{
			hasDownloaded = true;
		}
		return true;
	}
}
