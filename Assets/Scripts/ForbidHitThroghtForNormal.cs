using System;
using System.Collections;
using UnityEngine;

public class ForbidHitThroghtForNormal : MonoBehaviour
{
	private Vector3 originScale;

	private float scaleTime = 0.2f;

	private float maxScaleFactor = 0.2f;

	private void Awake()
	{
	}

	private void Start()
	{
	}

	private IEnumerator scale()
	{
		float eslapedTime = 0f;
		while (eslapedTime < scaleTime)
		{
			eslapedTime += Time.deltaTime;
			base.transform.localScale = originScale * (1f + maxScaleFactor * Mathf.Sin((float)Math.PI * eslapedTime / scaleTime));
			yield return null;
		}
		base.transform.localScale = originScale;
	}

	//private IEnumerator scaleNoChange()
	//{
	//	float eslapedTime = 0f;
	//	while (eslapedTime < scaleTime)
	//	{
	//		eslapedTime += Time.deltaTime;
	//		yield return null;
	//	}
	//}

	public void ForbidThoughtWithoutScaleChange()
	{
		//StartCoroutine(scaleNoChange());
		NewWorkerController.EventLock = true;
	}

	public void ForbidThoughtWithScaleChange()
	{
		originScale = base.transform.localScale;
		StartCoroutine(scale());
		NewWorkerController.EventLock = true;
	}
}
