using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class GradientColor
{
	// public struct GradientPart
	// {
	// 	public Color colorFrom;
	// 	public Color colorTo;
	// }
	public string name;

	public List<int> colorIndex;
	// public List<GradientPart> colorParts;
	[JsonIgnore]
	public Color bgColor { get; set; }

	public Point colorCoordinate;
}
