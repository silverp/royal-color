using System;
using Unicache;
using Unicache.Plugin;
using UniRx;
using UnityEngine;

public class NetLoadcontroller
{
	private IUnicache cache;

	protected static NetLoadcontroller _instance;

	public static NetLoadcontroller Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new NetLoadcontroller();
			}
			return _instance;
		}
	}

	public NetLoadcontroller()
	{
		cache = new FileCache(UrlCache.Instance.gameObject);
		cache.Handler = new SimpleDownloadHandler();
		cache.UrlLocator = new SimpleUrlLocator();
		cache.CacheLocator = new SimpleCacheLocator();
	}

	public void RequestImg(string url, Action<Texture2D> callback)
	{
		cache.Fetch(url).ByteToTexture2D(string.Empty).Subscribe(delegate(Texture2D texture)
		{
			callback(texture);
		});
	}

	public void RequestBytes(string url, Action<byte[]> callback)
	{
		cache.Fetch(url).Subscribe(delegate(byte[] bytes)
		{
			callback(bytes);
		});
	}

	public void RequestUrlWidthMethodGet(string url, Action<string, bool> callback)
	{
		ObservableWWW.Get(url).Subscribe(delegate(string x)
		{
			callback(x.ToString(), arg2: true);
		}, delegate(Exception ex)
		{
			callback(ex.ToString(), arg2: false);
		});
	}

	public void RequestUrlWidthMethodPost(string url, WWWForm content, Action<string, bool> callback)
	{
		ObservableWWW.Post(url, content).Subscribe(delegate(string x)
		{
			callback(x.ToString(), arg2: true);
		}, delegate(Exception ex)
		{
			callback(ex.ToString(), arg2: false);
		});
	}
}
