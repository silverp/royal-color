using UnityEngine;
using UnityEngine.UI;

public class ZoomPanelBtnController : MonoBehaviour
{
	public Texture2D curTex;

	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		base.transform.Find("MainPanel").Find("ImagePanel").Find("Image")
			.GetComponent<RawImage>()
			.texture = curTex;
		}

		private void OnDisable()
		{
		}

		public void BackToInspirationCanvas()
		{
			base.gameObject.SetActive(value: false);
		}
	}
