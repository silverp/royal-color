public class APIFactory
{
	public static readonly string HOST = "https://gameapi.orange-social.com/coloring/v8";

	public static readonly string ORIGIN_DETAIL = "/image/getOriginDetail";

	public static readonly string ALL_CATE_RESOURCE = "/allCateResource";

	public static readonly string ALL_CATEGORY = "/getAllCategory";

	public static readonly string SINGLE_CATE_INFO = "/getCategoryImageList";

	public static readonly string INSPIRATION_IMGLIST = "/image/getChoiceList";

	public static readonly string LIKES_IMGLIST = "/user/getLikeList";

	public static readonly string DAILY_FREE = "/image/getTodayFree";

	public static readonly string ADD_FOLLOW = "/user/addFollow";

	public static readonly string REMOVE_FOLLOW = "/user/removeFollow";

	public static readonly string PUBLISH_INSPIRATION = "/image/uploadInspiration";

	public static readonly string USER_INFO = "/user/loginUserInfo";

	public static readonly string QUERY_SIGN_STATUS = "/user/querySignStatus";

	public static readonly string SIGN = "/user/sign";

	public static readonly string GET_USER_INFO = "/user/getUserInfo";

	public static readonly string BEAN_ACTION = "/user/changeColorBeans";

	public static readonly string GET_ALLFOLLOW = "/user/getAllFollow";

	public static readonly string GET_ALLFANS = "/user/getAllFancy";

	public static readonly string IS_UPLOAD_INSPIRATION = "/image/checkUploadInspiration";

	public static readonly string GET_PRODUCT_LIST = "/user/getProductVipList";

	public static readonly string UPLOAD_UNLOCK_IMAGEID = "/user/uploadUnlockImage";

	public static readonly string PURCHASE_SUCCESS = "/user/successPurchaseVip";

	public static readonly string PURCHASE_SUCCESS_DEVICEID = "/device/successPurchaseVip";

	public static readonly string PURCHASE_WITH_WEIXIN = "/user/weixinPurchaseVip";

	public static readonly string PURCHASE_WITH_WEIXIN_DEVICEID = "/device/weixinPurchaseVip";

	public static readonly string GET_DEVICE_INFO = "/device/getDevice";

	public static readonly string MERGE_VIP_INFO = "/device/mergeVip";

	public static readonly string PRODUCT_HAS_EXPIRE = "/user/productHasExpire";

	public static readonly string GET_WORK_LIST = "/user/getWorkList";

	public static readonly string SAVE_WORK = "/image/saveWork";

	public static readonly string VIP_EXPIRED = "/user/vipExpired";

	public static readonly string APPLY_ARTIST = "/user/applyArtist";

	public static readonly string GET_PAY_CHANNEL_LIST = "/getPayChannelList";

	public static readonly string GET_CORLOR_DATA = "/colorThemeList";

	public static readonly string GET_ALBUM_LIST = "/getAlbumList";

	public static readonly string GET_ALBUM_IMAGE_LIST = "/getAlbumImageList";

	public static readonly string GET_SINGLE_INSPIRATION = "/image/getInspiration";

	public static readonly string GET_PIXEL_ART_LIST = "/getPixelImageList";

	public static readonly string GET_PIXEL_ORIGIN_DETAIL = "/image/getPixelOriginDetail";

	public static readonly string GET_PIXEL_EDITED_INFO = "/image/getPixelEditedInfo";

	public static readonly string SAVE_PIXEL_WORK = "/image/savePixelWork";

	public static readonly string DELETE_WORK = "/image/deleteWork";

	public static readonly string GET_AD_CHANNEL = "/getVideoAdsChannelList";

	public static URLRequest create(string path, HttpRequest.CacheType cacheType = HttpRequest.CacheType.None, URLRequest.REQ_METHOD method = URLRequest.REQ_METHOD.GET)
	{
		string text = HOST + path;
		URLRequest uRLRequest = new URLRequest(HOST + path, method);
		uRLRequest.cacheType = cacheType;
		return uRLRequest;
	}
}
