using Newtonsoft.Json;
using System;
using System.Collections;
using UniRx;
using UnityEngine;

public class PixelDrawController : MonoBehaviour
{
	public class EditedImageMsg
	{
		public string imageId;
	}

	[SerializeField]
	private GameObject SpinPanel;

	[SerializeField]
	private GameObject NoteText;

	[SerializeField]
	private GameObject magnigyGlass;

	[SerializeField]
	private GameObject pixelArtWorkshop;

	[SerializeField]
	private Camera secondCamera;

	private Vector2 startMovePos;

	private PixelDraw drawHandle;

	private int color;

	private float MAX_WAIT_TIME = 12f;

	private float time;

	private bool isHasNote;

	private bool isInitFinish;

	private PixleEditedImg curPixelArt;

	private bool colorHolds;

	public static bool isLoadInfoFinish;

	public bool EventLock;

	public static event Action<PixleEditedImg> OnCompleteLoadEditedInfoSuccess;

	private void OnEnable()
	{
		init();
		new PixelWorkShopLoader().CheckEditedInfo(curPixelArt, OnCompleteLoadInfo);
	}

	private void init()
	{
		UnityEngine.Debug.Log("init.....");
		isLoadInfoFinish = false;
		colorHolds = false;
		isHasNote = false;
		isInitFinish = false;
		time = MAX_WAIT_TIME;
		curPixelArt = (PixleEditedImg)ApplicationModel.currentEditedImg;
		SpinPanel.SetActive(value: true);
		NoteText.SetActive(value: false);
	}

	public void loadEditInfo(Action<PixleEditedImg> callback)
	{
		UnityEngine.Debug.Log("loadEditInfo....");
		if (isLoadInfoFinish)
		{
			UnityEngine.Debug.Log("isLoadInfoFinish");
			callback(curPixelArt);
		}
		else
		{
			UnityEngine.Debug.Log("loadEditInfo OnCompleteLoadEditedInfoSuccess....");
			OnCompleteLoadEditedInfoSuccess += callback;
		}
	}

	private void OnCompleteLoadInfo(Errcode err)
	{
		UnityEngine.Debug.Log("curPixelArt is:" + JsonConvert.SerializeObject(curPixelArt));
		if (err.errorCode == Errcode.OK)
		{
			curPixelArt.isRemote = false;
			drawHandle = new PixelDraw(base.transform, curPixelArt);
			StartCoroutine(Worker());
			UnityEngine.Debug.Log("OnCompleteLoadInfo.....");
			isLoadInfoFinish = true;
			SpinPanel.SetActive(value: false);
			isInitFinish = true;
			if (PixelDrawController.OnCompleteLoadEditedInfoSuccess != null)
			{
				PixelDrawController.OnCompleteLoadEditedInfoSuccess(curPixelArt);
			}
		}
		else
		{
			UnityEngine.Debug.Log("PixelDrawController load info err!");
			SpinPanel.SetActive(value: false);
			NoteText.SetActive(value: true);
		}
		StartCoroutine(ShowMagnifyGlass());
	}

	private void Start()
	{
		MessageBroker.Default.Receive<PixelColorButtonController.ColorIndex>().Subscribe(delegate(PixelColorButtonController.ColorIndex idx)
		{
			if (idx.action == 1)
			{
				color = idx.id;
				drawHandle.setChooseColor(color);
			}
		});
		GestureTrigger gestureTrigger = base.transform.gameObject.AddComponent<GestureTrigger>();
		gestureTrigger.OnGestureAsObservable().Subscribe(delegate(GestureTrigger.GestureResponse gesture)
		{
			switch (gesture.action)
			{
			case GestureTrigger.ActionType.DRAW:
				if (gesture.singleOrNot)
				{
					UnityEngine.Debug.Log("gesture.singleOrNot...");
					secondCamera.gameObject.SetActive(value: true);
				}
				drawHandle.setColor(gesture.postion, color);
				colorHolds = true;
				break;
			case GestureTrigger.ActionType.DRAW_END:
				secondCamera.gameObject.SetActive(value: false);
				break;
			case GestureTrigger.ActionType.MOVE:
				drawHandle.movePanel(gesture.delta);
				break;
			case GestureTrigger.ActionType.MOVE_END:
				drawHandle.onMoveEnd(gesture.delta);
				break;
			case GestureTrigger.ActionType.SCALE:
				drawHandle.scalePanel(gesture.postion, gesture.scale);
				break;
			case GestureTrigger.ActionType.SCALE_END:
				drawHandle.onScaleEnd();
				break;
			}
		});
	}

	private IEnumerator Worker()
	{
		yield return new WaitForSeconds(1f);
		MessageBroker.Default.Publish(new PixelColorButtonController.ColorIndex
		{
			id = 1,
			action = 1
		});
	}

	public void Erase()
	{
		MessageBroker.Default.Publish(new PixelColorButtonController.ColorIndex
		{
			id = 0,
			action = 1
		});
	}

	private void Update()
	{
		if (time > 0f)
		{
			time -= Time.deltaTime;
		}
		else if (!isHasNote && !isInitFinish)
		{
			isHasNote = true;
			SpinPanel.SetActive(value: false);
			NoteText.SetActive(value: true);
		}
		if (isInitFinish && EventLock)
		{
			EventLock = false;
		}
#if UNITY_EDITOR
		checkInputPc();
#endif
    }

	private void PrintMousePosition()
	{
		UnityEngine.Debug.Log("PrintMouthPosition.mousePosition = " + UnityEngine.Input.mousePosition);
	}

	private IEnumerator ShowMagnifyGlass()
	{
		yield return null;
		Canvas canvas = pixelArtWorkshop.GetComponent<Canvas>();
		if (canvas != null)
		{
			canvas.renderMode = RenderMode.WorldSpace;
		}
	}

	private void CloseMagnifyGlass()
	{
		Canvas component = pixelArtWorkshop.GetComponent<Canvas>();
		if (component != null)
		{
			component.renderMode = RenderMode.ScreenSpaceCamera;
			secondCamera.gameObject.SetActive(value: false);
		}
	}

	private void checkInputPc()
	{
		if (UnityEngine.Input.GetAxis("Mouse ScrollWheel") != 0f)
		{
			float axis = UnityEngine.Input.GetAxis("Mouse ScrollWheel");
			drawHandle.scrollScalePanel(UnityEngine.Input.mousePosition, axis);
		}
		if (Input.GetMouseButtonDown(1))
		{
			startMove(UnityEngine.Input.mousePosition);
		}
		if (Input.GetMouseButtonUp(1))
		{
			endMove(UnityEngine.Input.mousePosition);
			StartCoroutine(moveEnd());
		}
	}

	protected IEnumerator moveEnd()
	{
		yield return new WaitForSeconds(0.2f);
		drawHandle.onMoveEnd(UnityEngine.Input.mousePosition);
	}

	private void startMove(Vector2 start)
	{
		if (RectTransformUtility.RectangleContainsScreenPoint(base.transform.parent.GetComponent<RectTransform>(), start, Camera.main))
		{
			RectTransformUtility.ScreenPointToLocalPointInRectangle(base.transform.parent.GetComponent<RectTransform>(), start, Camera.main, out Vector2 localPoint);
			startMovePos = localPoint;
		}
	}

	private void endMove(Vector2 endPos)
	{
		if (RectTransformUtility.RectangleContainsScreenPoint(base.transform.parent.GetComponent<RectTransform>(), endPos, Camera.main))
		{
			RectTransformUtility.ScreenPointToLocalPointInRectangle(base.transform.parent.GetComponent<RectTransform>(), endPos, Camera.main, out endPos);
			drawHandle.movePanel(endPos - startMovePos);
		}
	}

	private void OnDisable()
	{
		Save();
		PixelDrawController.OnCompleteLoadEditedInfoSuccess = null;
		isLoadInfoFinish = false;
	}

	private void Save()
	{
		UnityEngine.Debug.Log("Save...");
		drawHandle.SaveTexture();
		if (colorHolds)
		{
			UnityEngine.Debug.Log("save texture......");
			curPixelArt.SetEdited();
			curPixelArt.create = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds * 1000;
			UnityEngine.Debug.Log(" curPixelArt.create is:" + curPixelArt.create);
			UnityEngine.Debug.Log("ApplicationModel.PixleEditedImgList is:" + JsonConvert.SerializeObject(ApplicationModel.PixleEditedImgList));
			ApplicationModel.RestoreEditedPixelImglist();
			if (PublicUserInfoApi.IsHasLogin())
			{
				FileUploadEventProxy.Instance.SendUploadUserEditedFileMsg(curPixelArt);
			}
			MessageProxy.GetInstance().SendOnChangeImageMsg(curPixelArt.imageId);
		}
	}
}
