using UnityEngine;
using UnityEngine.UI;

public class ShareViewController : MonoBehaviour
{
	[SerializeField]
	private GameObject BeanBox;

	public void OnShareClick()
	{
		UnityEngine.Debug.Log("OnShareClick....");
		int count = ApplicationModel.EditedResourceImgList.Count;
		if (ApplicationModel.userInfo != null)
		{
			FileUploadEventProxy.Instance.SendShareWorkEventMsg(ApplicationModel.GetCurrentStickerTexture2d(), ApplicationModel.userInfo.Nickname, count, isHasinfo: true, ShareCallback);
		}
		else
		{
			FileUploadEventProxy.Instance.SendShareWorkEventMsg(ApplicationModel.GetCurrentStickerTexture2d(), string.Empty, 0, isHasinfo: false, ShareCallback);
		}
	}

	public void ShareCallback(Errcode err)
	{
		UnityEngine.Debug.Log("myself work share callback ");
		if (err.errorCode == Errcode.OK && PublicUserInfoApi.IsHasLogin())
		{
			BeanAction.request(BeanAction.SHARE, beanActionCallback);
		}
	}

	private void beanActionCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("myself work share callback ok");
			BeanBox.SetActive(value: true);
			ApplicationModel.userInfo.BeanCnt += amount;
			BeanBox.transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = PublicToolController.GetTextByLanguage("Share success");
				BeanBox.transform.GetChild(1).GetChild(4).GetComponent<Text>()
					.text = "+" + amount;
					DragEventProxy.Instance.SendRefreshBeanCnt(amount);
				}
			}
		}
