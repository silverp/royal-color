using UnityEngine;
using UnityEngine.UI;

public class RefImageController : MonoBehaviour
{
	private EditedImg currentImg;

	private RefImageModel RefImaModel = new RefImageModel();

	private Texture2D loadingTex;

	private void Awake()
	{
		loadingTex = Resources.Load<Texture2D>("Images/work/loading");
	}

	private void OnEnable()
	{
		currentImg = (EditedImg)ApplicationModel.currentEditedImg;
		UnityEngine.Debug.Log("RefImageController.OnEnable...currentImg = " + currentImg.imageId);
		StartRequst();
	}

	private void Start()
	{
	}

	public void StartRequst()
	{
		UnityEngine.Debug.Log("StartRequst...");
		base.transform.Find("Image").GetComponent<RawImage>().texture = loadingTex;
		RefRequstAPI.GetRefImage(FiniRequst, currentImg.imageId);
	}

	public void FiniRequst(Errcode err, RefImageInfo image)
	{
		if (image.referList != null && image.referList.Count > 0)
		{
			RefImaModel.imageURL = image.referList[0].referImage;
			UnityEngine.Debug.Log("FiniRequst.baseURL = " + RefImaModel.imageURL);
			UpdateRef(RefImaModel);
		}
		else
		{
			base.transform.parent.Find("RefImage").gameObject.SetActive(value: false);
			base.transform.parent.Find("NonRef").gameObject.SetActive(value: true);
		}
	}

	private void OnDisable()
	{
		currentImg = null;
		base.transform.Find("Image").GetComponent<RawImage>().texture = loadingTex;
		if (!base.transform.parent.gameObject.activeInHierarchy)
		{
			base.transform.gameObject.SetActive(value: false);
		}
	}

	private void UpdateRef(RefImageModel model)
	{
		RawImage image = base.transform.Find("Image").GetComponent<RawImage>();
		URLRequest request = new URLRequest(model.imageURL);
		DataBroker.getTexture2D(request, delegate(Texture2D texture)
		{
			image.texture = texture;
		}, delegate
		{
			UnityEngine.Debug.Log("request baseurl fail!");
		});
	}
}
