using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ColorPageScrollRectAdapter : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class ColorPageModel
	{
		public int num;

		public bool isGradient;

		public ColorPageModel(int i, bool isGradient)
		{
			num = i;
			this.isGradient = isGradient;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public RectTransform Root;

		public override void CollectViews()
		{
			Root = root;
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<ColorPageModel> _Data;

		public RectTransform root;

		public MyWorkScrollRectItemsAdapter(List<ColorPageModel> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		public void setNum(int i, bool isGradient)
		{
			// if (isGradient)
			// {
			// 	root.GetComponent<ColorGradientPageInitController>().index = i;
			// }
			// else
			// {
			// 	root.GetComponent<ColorPageInitController>().index = i;
			// }
		}

		protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
			myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return myWorkItemsViewHolder;
		}

		protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
		{
			root = newOrRecycled.Root;
			ColorPageModel colorPageModel = _Data[newOrRecycled.itemIndex];
			setNum(colorPageModel.num, colorPageModel.isGradient);
		}
	}

	[SerializeField]
	private MyWorkParams _ScrollRectAdapterParams;

	private List<ColorPageModel> _Data = new List<ColorPageModel>();

	private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

	private string colorThemeName;

	private List<ColorTheme> colorLists;

	private int colorListCount;

	private Vector2 startPos;

	protected int currentPage;

	public BaseParams Params => _ScrollRectAdapterParams;

	public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<ColorPageModel> Data => _Data;

	private void Awake()
	{
		UnityEngine.Debug.Log("ReferenceColorPageScrollRectAdapter.Awake._Data + _ScrollRectAdapterParams; " + _Data.Count);
		_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void AssginEvents()
	{
		DragEventProxy.OnClickCateListBar += smoothScrollToPage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnClickCateListBar -= smoothScrollToPage;
	}

	private void smoothScrollToPage(int index)
	{
		currentPage = index;
		_ScrollRectItemsAdapter.SmoothScrollTo(index, 0.25f);
	}

	private IEnumerator Start()
	{
		colorThemeName = base.transform.parent.gameObject.name;
		yield return null;
		if (colorThemeName == "SolidColor")
		{
			colorListCount = ApplicationModel.ColorThemesPure.Count;
		}
		else if (colorThemeName == "ThemeColor")
		{
			colorListCount = ApplicationModel.ColorThemes.Count;
		}
		else if (colorThemeName == "GradientColor")
		{
			colorListCount = ApplicationModel.ColorThemesGradient.gradientColors.Count;
		}
		initContent();
	}

	private void RefreshContent()
	{
		initContent();
	}

	private void initContent()
	{
		_Data.Clear();
		if (colorThemeName == "GradientColor")
		{
			for (int i = 0; i < colorListCount; i++)
			{
				_Data.Add(new ColorPageModel(i, isGradient: true));
			}
		}
		else
		{
			for (int j = 0; j < colorListCount; j++)
			{
				_Data.Add(new ColorPageModel(j, isGradient: false));
			}
		}
		_Data.Capacity = _Data.Count;
		_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
	}

	private void OnDestroy()
	{
		DiscardEvents();
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	public void OnDrag(PointerEventData aEventData)
	{
		DragEventProxy.Instance.SendShwoColorBarMsg();
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		startPos = _ScrollRectAdapterParams.content.anchoredPosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Vector2 vector = _ScrollRectAdapterParams.content.anchoredPosition - startPos;
		int num = (vector.x < 0f) ? 1 : (-1);
		if ((double)Mathf.Abs(vector.x) < (double)_ScrollRectAdapterParams.viewport.rect.width * 0.05)
		{
			_ScrollRectItemsAdapter.SmoothScrollTo(currentPage, 0.25f);
			return;
		}
		currentPage += num;
		currentPage = Mathf.Clamp(currentPage, 0, _Data.Count - 1);
		_ScrollRectItemsAdapter.SmoothScrollTo(currentPage, 0.25f);
		DragEventProxy.Instance.SendChangeColorlistPage(this, currentPage);
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void OnEnable()
	{
		AssginEvents();
	}
}
