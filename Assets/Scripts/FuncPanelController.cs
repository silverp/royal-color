using UnityEngine;

public class FuncPanelController : MonoBehaviour
{
	[SerializeField]
	private GameObject recommendPanel;

	[SerializeField]
	private GameObject Andpanel;

	private void Start()
	{
		JudgePanelStatus();
	}

	private void Update()
	{
	}

	private void JudgePanelStatus()
	{
		UnityEngine.Debug.Log("not ios!!");
		recommendPanel.SetActive(value: false);
		RectTransform component = Andpanel.GetComponent<RectTransform>();
		Vector2 anchoredPosition = Andpanel.GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(anchoredPosition.x, 0f);
	}
}
