using UnityEngine;
using UnityEngine.UI;

public class BtnClickForbidThroughcontroller : MonoBehaviour
{
	private void Start()
	{
		string name = base.transform.parent.name;
		GetComponent<Button>().onClick.AddListener(delegate
		{
			if (name == "otherAreaButton")
			{
				if (base.transform.parent.gameObject.activeSelf)
				{
					NewWorkerController.EventLock = true;
				}
			}
			else
			{
				NewWorkerController.EventLock = true;
			}
			UnityEngine.Debug.Log("BtnClickForbidThroughcontroller transform parent is:" + name);
		});
	}
}
