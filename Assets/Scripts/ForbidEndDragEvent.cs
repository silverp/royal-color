using UnityEngine;
using UnityEngine.EventSystems;

public class ForbidEndDragEvent : MonoBehaviour, IEndDragHandler, IEventSystemHandler
{
	[SerializeField]
	private new GameObject gameObject;

	public void OnEndDrag(PointerEventData eventData)
	{
		ForbidHitThroght component = gameObject.transform.GetComponent<ForbidHitThroght>();
		component.ForbidThoughtWithoutScaleChange();
	}
}
