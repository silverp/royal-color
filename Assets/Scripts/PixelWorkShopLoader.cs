using System;
using UniRx;
using UnityEngine;

public class PixelWorkShopLoader
{
	private Errcode errCode;

	private Action<Errcode> callback;

	public void CheckEditedInfo(PixleEditedImg pixleEditedImg, Action<Errcode> callback)
	{
		errCode = new Errcode();
		this.callback = callback;
		if (pixleEditedImg.baseUrl == null || pixleEditedImg.colorUrl == null || pixleEditedImg.countUrl == null)
		{
			PublicImgaeApi.GetPixelOriginInfo(pixleEditedImg.imageId, delegate(Errcode err, PixelArt x)
			{
				GetPixelOriginInfoCallback(err, x, pixleEditedImg);
			});
		}
		else
		{
			DownloadEditedInfo(pixleEditedImg);
		}
	}

	private void DownloadEditedInfo(PixleEditedImg pixleEditedImg)
	{
		if (pixleEditedImg.isRemote)
		{
			PublicImgaeApi.GetPixelEditedInfo(pixleEditedImg.workId, delegate(Errcode err, PixelEditedInfo x)
			{
				GetPixelEditedInfoCallbck(err, x, pixleEditedImg);
			});
			return;
		}
		errCode.errorCode = Errcode.OK;
		callback(errCode);
	}

	private void GetPixelEditedInfoCallbck(Errcode err, PixelEditedInfo pixelEditedInfo, PixleEditedImg pixelEditedImg)
	{
		if (err.errorCode == Errcode.OK)
		{
			pixelEditedImg.countUrl = pixelEditedInfo.countMap;
			pixelEditedImg.colorMapUrl = pixelEditedInfo.colorMap;
			IObservable<byte[][]> source = Observable.WhenAll<byte[]>(ObservableWWW.GetAndGetBytes(pixelEditedImg.countUrl), ObservableWWW.GetAndGetBytes(pixelEditedImg.colorMapUrl));
			source.Subscribe(delegate(byte[][] xs)
			{
				CopyBytesTolocal(xs[0], pixelEditedImg.imageId + "_cntMap");
				CopyBytesTolocal(xs[1], pixelEditedImg.imageId + "_colorMap");
				pixelEditedImg.SaveToLocal();
				errCode.errorCode = Errcode.OK;
				callback(errCode);
			});
		}
		else
		{
			errCode.errorCode = Errcode.FAIL;
			callback(errCode);
		}
	}

	private void CopyBytesTolocal(byte[] bytes, string path)
	{
		PublicToolController.FileCreatorBytes(bytes, path, isStoreChange: true);
	}

	private void GetPixelOriginInfoCallback(Errcode err, PixelArt pixelArt, PixleEditedImg pixleEditedImg)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("GetPixelOriginInfoCallback..  ok");
			pixleEditedImg.baseUrl = pixelArt.baseUrl;
			pixleEditedImg.colorUrl = pixelArt.colorUrl;
			pixleEditedImg.countUrl = pixelArt.countUrl;
			DownloadEditedInfo(pixleEditedImg);
		}
		else
		{
			UnityEngine.Debug.Log("GetPixelOriginInfoCallback..  fail");
			errCode.errorCode = Errcode.FAIL;
			callback(errCode);
		}
	}
}
