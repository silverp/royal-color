using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenVipController : MonoBehaviour
{
	[SerializeField]
	private GameObject PurchaseResult;

	[SerializeField]
	private GameObject productItem;

	[SerializeField]
	private Transform productListPanel;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject initPanel;

	[SerializeField]
	private GameObject loadingPage;

	private GameObject orderInfo;

	private CustomProduct curProduct;

	private bool Lock;

	public ResourceImg curImg;

	public PixelArt curPixelArtImg;

	public ImageType imageType;

	private float startPosY = -24f;

	private float diff = 220f;

	private List<CustomProduct> productlist;

	private string zipLevel1ColStr = "#D7CA16FF";

	private string zipLevel2ColStr = "#4BB74CFF";

	private string zipLevel3ColStr = "#F8671FFF";

	private string zipLevel1BtnPath = "Images/product/icon_vip_blue";

	private string zipLevel2BtnPath = "Images/product/icon_vip_yellow";

	private string zipLevel3BtnPath = "Images/product/icon_vip_red";

	private string vip_bg_level1 = "Images/product/vip_bg_1";

	private string vip_bg_level2 = "Images/product/vip_bg_2";

	private string vip_bg_level3 = "Images/product/vip_bg_3";

	public string returnPage = string.Empty;

	public bool isPurchaseSuccess;

	private string currentCanvasName = string.Empty;

	private float time;

	private void Start()
	{
		orderInfo = base.transform.parent.GetChild(0).GetChild(0).Find("orderInfo")
			.gameObject;
			UnityEngine.Debug.Log("OpenVipController satrt !!!");
			InitproductListPanel();
			currentCanvasName = base.transform.parent.parent.name;
			orderInfo.SetActive(value: false);
		}

		private void Update()
		{
			if (time > 3f)
			{
				Lock = false;
				time = 0f;
			}
			else
			{
				time += Time.deltaTime;
			}
		}

		private void InitproductListPanel()
		{
			if (ApplicationModel.productList == null || ApplicationModel.productList.Count == 0)
			{
				PublicControllerApi.RequestProductList(RequestProductListCallback);
			}
			else
			{
				startInitPanel();
			}
		}

		private void RequestProductListCallback(ProductListJson json)
		{
			if (json.status == 0 && json.data.Count != 0)
			{
				UnityEngine.Debug.Log("product list request callback is:" + JsonConvert.SerializeObject(json));
				ApplicationModel.productList = json.data;
				startInitPanel();
			}
			else
			{
				UnityEngine.Debug.Log("request product list err");
			}
		}

		private void startInitPanel()
		{
			List<CustomProduct> productList = ApplicationModel.productList;
			UnityEngine.Debug.Log("productlist count is:" + productList.Count);
			int count = productList.Count;
			float num = startPosY;
			for (int i = 0; i < count; i++)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(productItem, productListPanel, worldPositionStays: false);
				gameObject.transform.Find("name").GetComponent<Text>().text = productList[i].description;
				gameObject.transform.Find("price").GetComponent<Text>().text = productList[i].priceInfo;
				gameObject.transform.Find("originText").GetComponent<Text>().text = productList[i].oriPriceInfo;
				gameObject.transform.Find("detailName").GetComponent<Text>().text = productList[i].detail;
				CustomProduct product = productList[i];
				gameObject.GetComponent<Button>().onClick.AddListener(delegate
				{
					PurchaseProduct(product);
				});
				gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(0f, num, 0f);
				num -= diff;
				ColorControl(product.vipLevel, gameObject);
			}
		}

		private void ColorControl(int level, GameObject item)
		{
			Color color = default(Color);
			string path;
			string path2;
			switch (level)
			{
			case 1:
				ColorUtility.TryParseHtmlString(zipLevel1ColStr, out color);
				path = zipLevel1BtnPath;
				path2 = vip_bg_level1;
				break;
			case 2:
				ColorUtility.TryParseHtmlString(zipLevel2ColStr, out color);
				path = zipLevel2BtnPath;
				path2 = vip_bg_level2;
				break;
			default:
				ColorUtility.TryParseHtmlString(zipLevel3ColStr, out color);
				path = zipLevel3BtnPath;
				path2 = vip_bg_level3;
				break;
			}
			Texture2D texture = Resources.Load<Texture2D>(path);
			Texture2D texture2 = Resources.Load<Texture2D>(path2);
			item.transform.Find("icon").GetComponent<RawImage>().texture = texture;
			item.transform.Find("name").GetComponent<Text>().color = color;
			item.transform.Find("price").GetComponent<Text>().color = color;
			item.transform.Find("line").GetComponent<Image>().color = color;
			item.transform.Find("detailName").GetComponent<Text>().color = color;
			item.transform.Find("originText").GetComponent<Text>().color = color;
			item.GetComponent<RawImage>().texture = texture2;
		}

		private void OnEnable()
		{
			PurchaseManager.PurchaseCompleted += PurchaseCompletedHandler;
			PurchaseManager.PurchaseFailed += PurchaseFailedHandler;
			PurchaseManager.PurchaseCancle += PurchaseCancleHandler;
			Lock = false;
			isPurchaseSuccess = false;
			PurchaseResult.SetActive(value: false);
			initPanel.SetActive(value: true);
			productListPanel.gameObject.SetActive(value: true);
			loadingPage.SetActive(value: false);
		}

		private void OnDisable()
		{
			PurchaseManager.PurchaseCompleted -= PurchaseCompletedHandler;
			PurchaseManager.PurchaseFailed -= PurchaseFailedHandler;
			PurchaseManager.PurchaseCancle -= PurchaseCancleHandler;
		}

		public void Close()
		{
			UnityEngine.Debug.Log("close purchase page");
			LeanTween.moveY(base.transform.parent.GetComponent<RectTransform>(), -1920f, 0.3f).setEase(LeanTweenType.easeInOutCubic).setOnComplete((Action)delegate
			{
				base.transform.parent.gameObject.SetActive(value: false);
				if (isPurchaseSuccess)
				{
					if (currentCanvasName == "CateDetailCanvas")
					{
						EnterDetailFromCatelistevent.instance.GoToWorkShop(curImg);
					}
					else if (currentCanvasName == "PixelArtDetailCanvas")
					{
						EnterWorkshopFromPixelListPage.instance.GoToWorkShop(curPixelArtImg);
					}
					else
					{
						DragEventProxy.Instance.SendRepaintAfterPurchase(curImg, curPixelArtImg, imageType);
					}
				}
			});
		}

		public void PurchaseProduct(CustomProduct product)
		{
			UnityEngine.Debug.Log("PurchaseProduct lock status is:" + Lock);
			if (!Lock)
			{
				Lock = true;
				curProduct = product;
				PurchaseResult.SetActive(value: false);
				loadingPage.SetActive(value: true);
				PurchaseManager.Instance.Purchase(product);
				TotalGA.Event("BuyVip_click");
				StartCoroutine(unlockClick());
				TotalGA.CountSingleEvent_FB("purchase_" + product.name);
			}
		}

		private IEnumerator unlockClick()
		{
			yield return new WaitForSeconds(1f);
			Lock = false;
		}

		private void PurchaseCompletedHandler(string serialNumber, string platformSerialNumber)
		{
			UnityEngine.Debug.Log("openVipController...success");
			if (currentCanvasName == "ObtainBeanCanvas" || currentCanvasName == "MyworkCanvas")
			{
				PublicControllerApi.PurchaseSuccess(curProduct, serialNumber, platformSerialNumber, isComeFromImage: false);
			}
			else
			{
				PublicControllerApi.PurchaseSuccess(curProduct, serialNumber, platformSerialNumber, isComeFromImage: true);
			}
			loadingPage.SetActive(value: false);
			initPanel.SetActive(value: false);
			PurchaseResult.SetActive(value: true);
			PurchaseResult.transform.GetChild(0).gameObject.SetActive(value: true);
			PurchaseResult.transform.GetChild(0).Find("name").GetComponent<Text>()
				.text = curProduct.description;
				PurchaseResult.transform.GetChild(1).gameObject.SetActive(value: false);
				DragEventProxy.Instance.SendRefreshAfterPurchaseMsg();
				isPurchaseSuccess = true;
				Lock = false;
				TotalGA.Event("BuyVip_success");
				TotalGA.CountPurchaseResult_FB(success: true);
			}

			private void PurchaseFailedHandler()
			{
				UnityEngine.Debug.Log("openVipController...fail");
				loadingPage.SetActive(value: false);
				PurchaseResult.SetActive(value: true);
				PurchaseResult.transform.GetChild(0).gameObject.SetActive(value: false);
				PurchaseResult.transform.GetChild(1).gameObject.SetActive(value: true);
				PurchaseResult.transform.GetChild(1).GetChild(0).GetComponent<Text>()
					.text = PublicToolController.GetTextByLanguage("Purchase fail!");
					StartCoroutine(hideFailNote());
					Lock = false;
					TotalGA.Event("BuyVip_fail");
					TotalGA.CountPurchaseResult_FB(success: false);
				}

				private void PurchaseCancleHandler()
				{
					UnityEngine.Debug.Log("openVipController...cancle");
					loadingPage.SetActive(value: false);
					PurchaseResult.SetActive(value: true);
					PurchaseResult.transform.GetChild(0).gameObject.SetActive(value: false);
					PurchaseResult.transform.GetChild(1).gameObject.SetActive(value: true);
					PurchaseResult.transform.GetChild(1).GetChild(0).GetComponent<Text>()
						.text = PublicToolController.GetTextByLanguage("Purchase cancle!");
						StartCoroutine(hideFailNote());
						Lock = false;
						TotalGA.Event("BuyVip_cancel");
						TotalGA.CountPurchaseResult_FB(success: false);
					}

					private IEnumerator hideFailNote()
					{
						yield return new WaitForSeconds(2f);
						PurchaseResult.transform.GetChild(1).gameObject.SetActive(value: false);
					}

					public void GotoMyVip()
					{
						base.transform.parent.parent.gameObject.SetActive(value: false);
						base.transform.parent.gameObject.SetActive(value: false);
						UIRoot.ShowUI(MyworkCanvas);
					}

					public void GotoServiceItem()
					{
						Application.OpenURL("https://gameapi.orange-social.com/games/ecolor/legal/tos");
					}

					public void GotoPrivacyPoliy()
					{
						Application.OpenURL("https://gameapi.orange-social.com/games/ecolor/legal/privacy");
					}
				}
