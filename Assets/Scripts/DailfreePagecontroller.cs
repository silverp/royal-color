using SVGImporter;
using UnityEngine;

public class DailfreePagecontroller : MonoBehaviour
{
	[SerializeField]
	private GameObject DailyfreeCanvas;

	[SerializeField]
	private GameObject ImglistCanvas;

	[SerializeField]
	private GameObject WorkShopCanvas;

	public string freeImageId;

	private EditedImg freeImage;

	private void OnEnable()
	{
		GameQuit.Back += Back;
		initFreeImage();
		LoadImageDetail();
	}

	private void OnDisable()
	{
		GameQuit.Back -= Back;
	}

	public void Back()
	{
		DailyfreeCanvas.SetActive(value: false);
		UIRoot.ShowUI(ImglistCanvas);;
	}

	private void initFreeImage()
	{
		freeImage = PublicImgaeApi.GetEditedImgFromEditedList(freeImageId, isCreate: true);
	}

	private void LoadImageDetail()
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.ORIGIN_DETAIL);
		uRLRequest.addParam("imageId", freeImageId);
		DataBroker.getJson(uRLRequest, delegate(OriginDetailJson detail)
		{
			if (detail.msg == "ok")
			{
				UnityEngine.Debug.Log("detail.msg ok...");
				freeImage.imageId = freeImageId;
				freeImage.ouline = detail.data.outline;
				freeImage.index = detail.data.index;
				PublicCallFrameTool.Instance.LoadSvgAsset(freeImage, DownloadOutlineCallback);
			}
			else
			{
				UnityEngine.Debug.Log("request error!");
			}
		}, delegate
		{
			UnityEngine.Debug.Log("request error!");
		});
	}

	private void DownloadOutlineCallback(SVGAsset svg, Errcode errcode)
	{
		if (errcode.errorCode == 0)
		{
			base.transform.parent.Find("mask").Find("Image").GetComponent<SVGImage>()
				.vectorGraphics = svg;
			}
		}

		public void GotoWorkshop()
		{
			if (!PublicImgaeApi.IsExistEditedImgByImageId(freeImageId))
			{
				ApplicationModel.EditedResourceImgList.Add(freeImage);
			}
			ApplicationModel.currentEditedImg = freeImage;
			DailyfreeCanvas.SetActive(value: false);
			WorkShopCanvas.SetActive(value: true);
			ApplicationModel.lastVisibleScreen = DailyfreeCanvas.name;
		}
	}
