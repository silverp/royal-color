using UnityEngine;

public class NetErrorController : MonoBehaviour
{
	private void OnEnable()
	{
		base.transform.GetChild(1).gameObject.SetActive(value: false);
	}

	public void Refresh()
	{
		base.transform.GetChild(1).gameObject.SetActive(value: true);
		string name = base.transform.parent.name;
		if (name == "InspirationCanvas")
		{
			DragEventProxy.Instance.SendOnRefreshAfterPublishMsg();
		}
		else if (name == "ImglistCanvas")
		{
			UnityEngine.Debug.Log("start refresh catelist!!!");
			DragEventProxy.Instance.SendOnRefreshCatelist();
		}
		else if (name == "AritistCanvas")
		{
			DragEventProxy.Instance.SendRefreshAlbumList();
		}
	}
}
