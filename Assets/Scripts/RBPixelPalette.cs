using SVGImporter;
using System;
using System.Collections.Generic;
using System.IO;
using UniRx; 
using UnityEngine;
using UnityEngine.UI; 

public class RBPixelPalette //: MonoBehaviour
{
	public enum ColoringTheme
	{
		Solid = 0xFF,
		Gradient = 1
    }
	public enum AnimationState
	{
		// Fields
		START = 1,
		STOP = 255
	}

    public class ColorState
	{
		public int x;

		public int y;

		public Color32 clickPos;

		public Color32 color;

		public ColorState(int x, int y, Color32 clickPos, Color32 color)
		{
			this.x = x;
			this.y = y;
			this.clickPos = clickPos;
			this.color = color;
		}
	}

	public class UndoState
	{
		public ColorState oldState;

		public ColorState newState;

		public UndoState(ColorState oldState, ColorState newState)
		{
			this.oldState = oldState;
			this.newState = newState;
		}
	}

	private Material material;

	private Texture2D _mainTex;

	private Texture2D _Palette;

	private Texture2D _ExtraInfo;

    private Texture2D _PrevPalette;

    private Texture2D _PrevExtraInfo;

    private double xScale = 1.0;

	private double yScale = 1.0;

	private int palette_size = 256;

	private Stack<UndoState> undoBuffer;

	private Stack<UndoState> redoBuffer;

	private RawImage image;

	private EditedImg resourceImg;

	private Dictionary<int, Vector4> regionIdx;

	private SVGAsset svgAsset;

	public RBPixelPalette(RawImage image, Texture2D mainTex, EditedImg img)
	{
		resourceImg = img;
		_mainTex = mainTex;
		undoBuffer = new Stack<UndoState>();
		redoBuffer = new Stack<UndoState>();
		this.image = image;
	}

	protected void createRegionIdx(Color32[] pixels, int width, int height)
	{
		regionIdx = new Dictionary<int, Vector4>();
		for (int i = 0; i < width; i++)
		{
			for (int j = 0; j < height; j++)
			{
				Color32 color = pixels[i * width + j];
				int key = color.r * 256 + color.g;
				Vector4 value = Vector4.zero;
				if (regionIdx.ContainsKey(key))
				{
					value = regionIdx[key];
				}
				if (value.x == 0f)
				{
					value.x = i;
				}
				else
				{
					value.x = Mathf.Min(value.x, i);
				}
				if (value.y == 0f)
				{
					value.y = j;
				}
				else
				{
					value.y = Mathf.Min(value.y, j);
				}
				value.z = Mathf.Max(value.z, i);
				value.w = Mathf.Max(value.w, j);
				regionIdx[key] = value;
			}
		}
	}

	public void SetSvgAsset(SVGAsset svgAsset)
	{
		this.svgAsset = svgAsset;
	}

	public void setMainTex(Texture2D mainTex)
	{
		_mainTex = mainTex;
		material = new Material(Shader.Find("UI/GradientShader"));
		material.SetTexture("_MainTex", mainTex);
		material.SetTexture("_Gradients", Resources.Load<Texture2D>("Images/Image/gradiant"));
		material.mainTexture = _mainTex;
		image.material = material;
		Color32[] maintex_pixels = mainTex.GetPixels32();
		int width = mainTex.width;
		int height = mainTex.height;
		IObservable<Unit> observable = Observable.Start(delegate
		{
			createRegionIdx(maintex_pixels, width, height);
		});
		Observable.WhenAll(observable).ObserveOnMainThread().Subscribe(delegate
		{
			UnityEngine.Debug.Log("thread end");
		});
		loadPattle();
	}

	public void setMask(Texture2D mask)
	{
		material.SetTexture("_Mask", mask);
	}

	public void setHasMask(int value)
	{
		material.SetInt("_HasMask", value);
	}

	public void destoryTex()
	{
		material.SetTexture("_Mask", null);
		if (_mainTex != null)
		{
			UnityEngine.Object.DestroyImmediate(_mainTex);
		}
		if (_Palette != null)
		{
			UnityEngine.Object.DestroyImmediate(_Palette);
        }

        if (this._PrevExtraInfo != null)
        {
            UnityEngine.Object.DestroyImmediate(obj: this._PrevExtraInfo);
        }

        if (this._PrevPalette != null)
        {
            UnityEngine.Object.DestroyImmediate(obj: this._PrevPalette);
        } 
    }

    private UnityEngine.Texture2D getTexture(int width, int height)
    {
        var tex = new Texture2D(palette_size, palette_size, TextureFormat.RGBA32, mipChain: false);
        tex.filterMode = FilterMode.Point;
        tex.wrapMode = TextureWrapMode.Clamp;
        return tex;
    }
    private void loadPattle()
	{
		_Palette = getTexture(palette_size, palette_size);
        _PrevPalette = getTexture(palette_size, palette_size);
        if (resourceImg != null && resourceImg.HasEdited())
		{
			_Palette.LoadImage(PublicToolController.FileReaderBytes(resourceImg.imageId + "_ed"));
            _PrevPalette.LoadImage(PublicToolController.FileReaderBytes(resourceImg.imageId + "_ed")); 
        }
		else
		{
			Color32[] pixels = _Palette.GetPixels32();
			for (int i = 0; i < pixels.Length; i++)
			{
				pixels[i] = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
			}
			_Palette.SetPixels32(pixels); 
            _PrevPalette.SetPixels32(pixels);
        }
        _Palette.Apply();
        _PrevPalette.Apply();
        material.SetTexture("_Palette", _Palette);
        material.SetTexture("_PrevPalette", _PrevPalette);
        //material.SetInt("_Palette_Scale", 1);
		_ExtraInfo = getTexture(palette_size, palette_size);
        _PrevExtraInfo = getTexture(palette_size, palette_size);
        if (resourceImg != null && resourceImg.HasEdited())
		{
			_ExtraInfo.LoadImage(PublicToolController.FileReaderBytes(resourceImg.imageId + "_pos"));
            _PrevExtraInfo.LoadImage(PublicToolController.FileReaderBytes(resourceImg.imageId + "_pos"));
        }
		else
		{
			Color32[] pixels2 = _ExtraInfo.GetPixels32();
			for (int j = 0; j < pixels2.Length; j++)
			{
				pixels2[j] = new Color32(0, 0, 0, 0);
			}
			_ExtraInfo.SetPixels32(pixels2);
            _PrevExtraInfo.SetPixels32(pixels2);
        }
        _ExtraInfo.Apply();
        _PrevExtraInfo.Apply();
        material.SetTexture("_ExtraInfo", _ExtraInfo);
        material.SetTexture("_PrevExtraInfo", _PrevExtraInfo);
    }

	public void savePattle()
	{
		PublicToolController.FileCreatorBytes(_Palette.EncodeToPNG(), resourceImg.imageId + "_ed", isStoreChange: true);
		PublicToolController.FileCreatorBytes(_ExtraInfo.EncodeToPNG(), resourceImg.imageId + "_pos", isStoreChange: true);
	}

    public void setRadiusTime(float radiusTime)
    {
		if (!material) return;
        if (radiusTime > 3f)
        {
            return;
        } 
        this.material.SetFloat(name: "_RadiusTime", value: radiusTime * radiusTime * 1.5f);
    }
	private Color32 MaskColor(Color32 c)
	{
        c.r = (byte)(c.r & 0xFF);
        c.g = (byte)(c.g & 0xFF);
        c.b = (byte)(c.b & 0xFF);
        c.a = (byte)(c.a & 0xFF);
		return c;
    }
    public void setColor(int x, int y, Color32 color, ColoringTheme theme = ColoringTheme.Solid)
	{
        //UnityEngine.Debug.Log($"setColor at ({x},{y}), color={color}");

        this.setRadiusTime(radiusTime: 0);
        if (this.undoBuffer.Count >= 1)
		{
            var us = this.undoBuffer.Peek(); 
			 
            if (us.oldState == null)
            {
				return;
            }
			 
            UnityEngine.Color lastColor = this._ExtraInfo.GetPixel(x: us.oldState.x, y: us.oldState.y);
			if (lastColor.a != 0)
			{
				lastColor.a = 0;
				this._ExtraInfo.SetPixel(x: us.oldState.x, y: us.oldState.y, color: lastColor);
				this._ExtraInfo.Apply();
			}
        }
        //UnityEngine.Debug.Log(message: "setColor...");
        int coordX = Mathf.Min(x, _mainTex.width - 1);
		int coordY = Mathf.Min(y, _mainTex.height - 1);
		Color32 pixelColor = _mainTex.GetPixel(coordX, coordY);
		pixelColor.r = (byte)Mathf.Min(pixelColor.r, _Palette.width);
		pixelColor.g = (byte)Mathf.Min(pixelColor.g, _Palette.height);
		color.a = (byte)theme;

		var currentState = getCurrentState(pixelColor.r, pixelColor.g);
        Color32 pcolor = currentState.color;
        Color32 clickPos = currentState.clickPos;
		if (pcolor.Equals(color))
		{
			color = Color.white;
		}
		 
        _PrevPalette.SetPixel(pixelColor.r, pixelColor.g, pcolor);
        _PrevPalette.Apply();

        _Palette.SetPixel(pixelColor.r, pixelColor.g, color);
		_Palette.Apply();

        _PrevExtraInfo.SetPixel(pixelColor.r, pixelColor.g, clickPos);
        _PrevExtraInfo.Apply();

        Color32 posToColor = getPosToColor32(coordX, coordY, pixelColor.r, pixelColor.g); 
        _ExtraInfo.SetPixel(pixelColor.r, pixelColor.g, posToColor);
		_ExtraInfo.Apply();
		ColorState newState = new ColorState(pixelColor.r, pixelColor.g, posToColor, color);
		UndoState t = new UndoState(currentState, newState);
		undoBuffer.Push(t);
		redoBuffer.Clear();
	}

	private ColorState getCurrentState(int x, int y)
	{
		Color32 color = _Palette.GetPixel(x, y);
		Color32 clickPos = _ExtraInfo.GetPixel(x, y);
		return new ColorState(x, y, clickPos, color);
	}

	private Color32 getPosToColor32(int u, int v, int idxX, int idxY)
	{
		uint x = (uint)Mathf.Round(u * 256 / _mainTex.width);
		uint y = (uint)Mathf.Round(v * 256 / _mainTex.height);
		float value = 255f;
		if (regionIdx != null)
		{
			Vector4 vector = regionIdx[idxX * 256 + idxY];
			float rx = Mathf.Abs(vector.z - vector.x);
			float ry = Mathf.Abs(vector.w - vector.y);
			value = Mathf.Ceil(Mathf.Max(rx / (float)_mainTex.width, ry / (float)_mainTex.height) * 255f);
		}
		return new Color32((byte)x, (byte)y, (byte)value, 255);
	}

	public void saveTexture()
	{
		setMask(GetSvgTexture());
		RenderTexture renderTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
		Graphics.Blit(_mainTex, renderTexture, material);
		RenderTexture active = RenderTexture.active;
		Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
		texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture2D.Apply();
		RenderTexture.active = renderTexture;
		try
		{
			PublicToolController.FileCreatorBytes(texture2D.EncodeToPNG(), resourceImg.imageId + "_ok", isStoreChange: true);
		}
		catch (IOException e)
		{
			print(e);
        }
        RenderTexture.active = active;
    }

	private Texture2D GetSvgTexture()
	{
		int size = 1024;
		RenderTexture renderTexture = SVGRenderTexture.RenderSVG(textureSize: new Rect(0f, 0f, size, size), svgAsset: svgAsset);
		RenderTexture active = RenderTexture.active;
		Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
		RenderTexture.active = renderTexture;
		texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture2D.Apply();
		RenderTexture.active = active;
		return texture2D;
	}

	private void print(IOException e)
	{
		//throw new NotImplementedException();
		Debug.LogError(e);
	}

	public void Undo()
	{
		if (undoBuffer.Count > 0)
		{
			UndoState undoState = undoBuffer.Pop();
			if (undoState != null)
			{
				_Palette.SetPixel(undoState.oldState.x, undoState.oldState.y, undoState.oldState.color);
				_Palette.Apply();
				_ExtraInfo.SetPixel(undoState.oldState.x, undoState.oldState.y, undoState.oldState.clickPos);
				_ExtraInfo.Apply();
			}
			redoBuffer.Push(undoState);
		}
	}

	public void Redo()
	{
		if (redoBuffer.Count > 0)
		{
			UndoState undoState = redoBuffer.Pop();
			if (undoState != null)
			{
				_Palette.SetPixel(undoState.newState.x, undoState.newState.y, undoState.newState.color);
				_Palette.Apply();
				_ExtraInfo.SetPixel(undoState.newState.x, undoState.newState.y, undoState.newState.clickPos);
				_ExtraInfo.Apply();
			}
			undoBuffer.Push(undoState);
		}
	}
}
