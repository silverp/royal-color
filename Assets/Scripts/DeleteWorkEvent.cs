using System;
using UnityEngine;

public class DeleteWorkEvent : MonoBehaviour
{
	public MyworkScrollRectAdapter myworkScrollRectAdapter;

	public string imageId;

	private void OnEnable()
	{
		HidePopBox();
		ShowBottomBar();
	}

	private void ShowBottomBar()
	{
		base.transform.GetChild(0).gameObject.SetActive(value: true);
		RectTransform component = base.transform.GetChild(0).GetComponent<RectTransform>();
		Vector2 anchoredPosition = base.transform.GetChild(0).GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(anchoredPosition.x, -323f);
		LeanTween.moveY(base.transform.GetChild(0).GetComponent<RectTransform>(), 0f, 0.3f).setEase(LeanTweenType.easeOutQuad).setOnComplete((Action)delegate
		{
			UnityEngine.Debug.Log("ShowBottomBar....");
		});
	}

	private void HideBottomBar(bool HideAll = false)
	{
		LeanTween.moveY(base.transform.GetChild(0).GetComponent<RectTransform>(), -323f, 0.3f).setEase(LeanTweenType.easeOutQuad).setOnComplete((Action)delegate
		{
			base.transform.GetChild(0).gameObject.SetActive(value: false);
			if (HideAll)
			{
				base.gameObject.SetActive(value: false);
			}
		});
	}

	private void ShowPopBox()
	{
		base.transform.GetChild(1).gameObject.SetActive(value: true);
	}

	private void HidePopBox()
	{
		base.transform.GetChild(1).gameObject.SetActive(value: false);
	}

	public void DeleteWork()
	{
		HideBottomBar();
		ShowPopBox();
	}

	public void Cancel()
	{
		HideBottomBar(HideAll: true);
	}

	public void ConfirmDelete()
	{
		HidePopBox();
		base.gameObject.SetActive(value: false);
		myworkScrollRectAdapter.RemoveItem(imageId);
	}

	public void CancelDelete()
	{
		HidePopBox();
		base.gameObject.SetActive(value: false);
	}
}
