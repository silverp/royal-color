using UnityEngine;
using UnityEngine.UI;

public class ImageChangeController : MonoBehaviour
{
	public string imageId;

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void OnEnable()
	{
	}

	private void AssginEvents()
	{
		DragEventProxy.OnChangeIndexImage += ChangeIndexImage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnChangeIndexImage -= ChangeIndexImage;
	}

	private void ChangeIndexImage(string imageId)
	{
		if (imageId == this.imageId)
		{
			GetComponent<RawImage>().texture = ApplicationModel.GetCurrentText2d();
		}
	}
}
