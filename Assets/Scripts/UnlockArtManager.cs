using Newtonsoft.Json;
using System;
using UnityEngine;

public class UnlockArtManager : MonoBehaviour
{
	public static UnlockArtManager Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public bool IsShowLock(ResourceImg img)
	{
		if (!ApplicationModel.IsPayChannel)
		{
			return false;
		}
		if (img == null)
		{
			return false;
		}
		EditedImg editedImgFromEditedList = PublicImgaeApi.GetEditedImgFromEditedList(img.imageId);
		if (!img.locked)
		{
			return false;
		}
		if (editedImgFromEditedList != null && ((!editedImgFromEditedList.isRemote && editedImgFromEditedList.HasEdited()) || editedImgFromEditedList.isRemote))
		{
			return false;
		}
		if (PublicUserInfoApi.IsVip())
		{
			return false;
		}
		if (PublicUserInfoApi.IsExistInUnlocklist(img.imageId))
		{
			return false;
		}
		return true;
	}

	public bool IsShowLock(PixelArt img)
	{
		if (!ApplicationModel.IsPayChannel)
		{
			return false;
		}
		if (img == null)
		{
			return false;
		}
		PixleEditedImg editedPixelImgFromEditedList = PublicImgaeApi.GetEditedPixelImgFromEditedList(img.imageId);
		if (!img.locked)
		{
			return false;
		}
		if (editedPixelImgFromEditedList != null && ((!editedPixelImgFromEditedList.isRemote && editedPixelImgFromEditedList.HasEdited()) || editedPixelImgFromEditedList.isRemote))
		{
			return false;
		}
		if (PublicUserInfoApi.IsVip())
		{
			return false;
		}
		if (PublicUserInfoApi.IsExistInUnlocklist(img.imageId))
		{
			return false;
		}
		return true;
	}

	public void UnlockArt(ResourceImg img, Action<Errcode, int> callback)
	{
		TotalGA.Event("Unlock_action");
		if (ApplicationModel.userInfo != null)
		{
			BeanAction.request(BeanAction.UNCLOCK_OUTLINE, delegate(Errcode err, int amount)
			{
				UnlockArtCallback(err, amount, img, callback);
			});
		}
		else
		{
			TotalGA.Event("Unlock_unlogin");
		}
	}

	public void UnlockArt(PixelArt img, Action<Errcode, int> callback)
	{
		TotalGA.Event("Unlock_action");
		if (ApplicationModel.userInfo != null)
		{
			BeanAction.request(BeanAction.UNCLOCK_OUTLINE, delegate(Errcode err, int amount)
			{
				UnlockArtCallback(err, amount, img, callback);
			});
		}
		else
		{
			TotalGA.Event("Unlock_unlogin");
		}
	}

	private void UnlockArtCallback(Errcode err, int amount, PixelArt img, Action<Errcode, int> callback)
	{
		UnityEngine.Debug.Log("err is:" + err.ToString());
		callback(err, amount);
		if (err.errorCode == Errcode.OK)
		{
			TotalGA.Event("Unlock_useBeanToUnLock_success");
			TotalGA.CountEventNumber_FB("consume_beans", amount);
			img.locked = false;
			UnityEngine.Debug.Log("current userInfo.UnlockList is:" + JsonConvert.SerializeObject(ApplicationModel.userInfo.unlockList));
			PublicUserInfoApi.UploadImgIdToUnLockList(img.imageId);
		}
		else
		{
			TotalGA.Event("Unlock_useBeanToUnLock_fail");
		}
	}

	private void UnlockArtCallback(Errcode err, int amount, ResourceImg img, Action<Errcode, int> callback)
	{
		UnityEngine.Debug.Log("err is:" + err.ToString());
		callback(err, amount);
		if (err.errorCode == Errcode.OK)
		{
			TotalGA.Event("Unlock_useBeanToUnLock_success");
			TotalGA.CountEventNumber_FB("consume_beans", amount);
			img.locked = false;
			PublicUserInfoApi.UploadImgIdToUnLockList(img.imageId);
		}
		else
		{
			TotalGA.Event("Unlock_useBeanToUnLock_fail");
		}
	}
}
