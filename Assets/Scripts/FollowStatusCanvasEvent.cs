using UnityEngine;

public class FollowStatusCanvasEvent : MonoBehaviour
{
	[SerializeField]
	private GameObject FollowStatusCanvas;

	[SerializeField]
	private GameObject OtherWorkCanvas;

	public static FollowStatusCanvasEvent Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public void GotoOtherCanvas(FollowUserInfo user)
	{
		PublicUserInfoApi.AssignOtherUserInfo(user);
		FollowStatusCanvas.SetActive(value: false);
		OtherWorkCanvas.SetActive(value: true);
	}
}
