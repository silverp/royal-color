using UnityEngine;
using UnityEngine.UI;

public class CanvasScreenAutoFix : MonoBehaviour
{
	public static CanvasScreenAutoFix instance;

	private static float DEPEND_W = 1080f;

	private static float DEPEND_H = 1920f;

	public float scaleRatio = 1f;

	private void ResizeCanvas()
	{
		int width = Screen.width;
		int height = Screen.height;
		if (DEPEND_W == width && DEPEND_H == height)
		{
			scaleRatio = 1f;
			return;
		}
		float scaleW = width / DEPEND_W;
		float scaleH = height / DEPEND_H;
		float scaleFactor = (scaleW >= scaleH) ? scaleH : scaleW;
		GetComponent<CanvasScaler>().scaleFactor = scaleFactor;
		scaleRatio = scaleFactor;
	}

	private void Awake()
	{
		ResizeCanvas();
		if (instance == null)
		{
			instance = this;
		}
	}
}
