using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PassEventToParent : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	private ScrollRect ScrollRectParent;

	private void Start()
	{
		Transform parent = base.transform.parent;
		while (parent != null)
		{
			ScrollRectParent = parent.GetComponent<ScrollRect>();
			if (ScrollRectParent != null)
			{
				break;
			}
			parent = parent.parent;
		}
	}

	private void DoForParents<T>(Action<T> action) where T : IEventSystemHandler
	{
		Transform parent = base.transform.parent;
		while (parent != null)
		{
			Component[] components = parent.GetComponents<Component>();
			foreach (Component component in components)
			{
				if (component is T)
				{
					action((T)(IEventSystemHandler)component);
				}
			}
			parent = parent.parent;
		}
	}

	private void Update()
	{
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		DoForParents(delegate(IBeginDragHandler parent)
		{
			parent.OnBeginDrag(eventData);
		});
	}

	public void OnDrag(PointerEventData eventData)
	{
		DoForParents(delegate(IDragHandler parent)
		{
			parent.OnDrag(eventData);
		});
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		DoForParents(delegate(IEndDragHandler parent)
		{
			parent.OnEndDrag(eventData);
		});
	}
}
