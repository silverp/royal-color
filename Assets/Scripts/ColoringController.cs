using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ColoringController : MonoBehaviour
{
	private RBPixelPalette pixelPalette;

	private int count;

	private void Start()
	{
		Texture2D mainTex = Resources.Load("test_circle_idx") as Texture2D;
		pixelPalette = new RBPixelPalette(GetComponent<RawImage>(), mainTex, null);
	}

	public void Undo()
	{
		pixelPalette.Undo();
	}

	public void Redo()
	{
		pixelPalette.Redo();
	}

	public void saveTexture()
	{
		pixelPalette.saveTexture();
	}

	public void Coloring()
	{
		int x = (int)Mathf.Ceil(UnityEngine.Random.Range(0, 1024));
		int y = (int)Mathf.Ceil(UnityEngine.Random.Range(0, 1024));
		count++;
		Color32 color = new Color32((byte)count, (byte)Mathf.Floor(UnityEngine.Random.Range(0, 0)), (byte)Mathf.Floor(UnityEngine.Random.Range(0, 0)), 0);
		UnityEngine.Debug.Log("Color is:" + color.ToString());
		pixelPalette.setColor(x, y, color, RBPixelPalette.ColoringTheme.Gradient);
	}

	public void FileCreatorBytes(byte[] fileData, string fileName, bool isStoreChange)
	{
		string text = Application.persistentDataPath + "/" + fileName + ".png";
		if (File.Exists(text))
		{
			if (!isStoreChange)
			{
				return;
			}
			File.Delete(text);
		}
		UnityEngine.Debug.Log(text);
		FileStream fileStream = File.Create(text);
		fileStream.Write(fileData, 0, fileData.Length);
		fileStream.Close();
	}

	private void Update()
	{
	}
}
