using cn.sharesdk.unity3d;
using System;
using System.Collections;
using UI.ThreeDimensional;
using UnityEngine;
using UnityEngine.UI;

public class ShareEventController : MonoBehaviour
{
	[SerializeField]
	private Transform shareObjectprefab;

	[SerializeField]
	private Transform inviteObjectprefab;

	public UITemplate template;

	public ShareSDK ssdk;

	private bool IsShowMsg;

	private GUIStyle guiStyle = new GUIStyle();

	private string ShareMsg;

	private float WaitTime = 3f;

	private bool shareClock;

	private Action<Errcode> shareCallback;

	private void Start()
	{
		ssdk = base.gameObject.GetComponent<ShareSDK>();
		ssdk.shareHandler = OnShareResultHandler;
	}

	private void Update()
	{
	}

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		FileUploadEventProxy.OnShareWorkEvent += OnShareClick;
		FileUploadEventProxy.OnInviteFriend += OnInviteClick;
	}

	private void DiscardEvents()
	{
		FileUploadEventProxy.OnShareWorkEvent -= OnShareClick;
		FileUploadEventProxy.OnInviteFriend -= OnInviteClick;
	}

	private void OnShareRenderCallback(Texture2D texture)
	{
		UnityEngine.Debug.Log("OnRenderCallback:");
		if (texture != null)
		{
			string text = "share_test" + DateTime.Now.ToString("dd-MM-yy_hhmmss");
			string imagePath = Application.persistentDataPath + "/" + text + ".jpg";
			PublicToolController.FileCreatorBytesForJpg(texture.EncodeToJPG(100), text);
			ShareContent shareContent = new ShareContent();
			shareContent.SetImagePath(imagePath);
			shareContent.SetShareType(2);
			ssdk.ShowPlatformList(null, shareContent, 100, 100);
		}
		template.OnRenderCallback -= OnShareRenderCallback;
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void ShareRender(Texture2D texImage, string name, string info, bool isHasInfo)
	{
		UnityEngine.Debug.Log("Render");
		template = GetComponent<UITemplate>();
		template.OnRenderCallback += OnShareRenderCallback;
		Transform child = template.target.GetChild(0);
		if (isHasInfo)
		{
			TextEmojiConvert.Instance.ConvertEmoji(child.GetChild(0), name, isBig: false);
			UnityEngine.Debug.Log("change topPanelTransform name is:" + child.GetChild(0).GetComponent<Text>().text);
			child.GetChild(1).gameObject.SetActive(value: false);
			child.GetChild(2).GetComponent<Text>().text = info;
		}
		else
		{
			UnityEngine.Debug.Log("else isHasInfo");
			child.GetChild(0).GetComponent<Text>().text = string.Empty;
			child.GetChild(1).gameObject.SetActive(value: true);
			child.GetChild(2).GetComponent<Text>().text = string.Empty;
		}
		template.target.GetChild(1).GetComponent<RawImage>().texture = texImage;
		UnityEngine.Debug.Log("FINAL topPanelTransform name is:" + child.GetChild(0).GetComponent<Text>().text);
		template.renderPreferb();
	}

	private void OnShareClick(Texture2D texture, string name, int count, bool isHasInfo, Action<Errcode> callback)
	{
		UnityEngine.Debug.Log("OnShareClick....");
		GetComponent<UITemplate>().ObjectPrefab = shareObjectprefab;
		TotalGA.Event("share");
		ShareRender(texture, "@" + name, PublicToolController.GetShareStrByCount(count), isHasInfo);
		shareCallback = callback;
	}

	private void OnInviteRenderCallback(Texture2D texture)
	{
		UnityEngine.Debug.Log("OnRenderCallback:");
		if (texture != null)
		{
			string text = "share_test" + DateTime.Now.ToString("dd-MM-yy_hhmmss");
			string imagePath = Application.persistentDataPath + "/" + text + ".jpg";
			PublicToolController.FileCreatorBytesForJpg(texture.EncodeToJPG(100), text);
			ShareContent shareContent = new ShareContent();
			shareContent.SetImagePath(imagePath);
			shareContent.SetShareType(2);
			ssdk.ShowPlatformList(null, shareContent, 100, 100);
		}
		template.OnRenderCallback -= OnInviteRenderCallback;
		PublicCallFrameTool.Instance.CollectGarbage();
	}

	private void InviteRender(Texture2D headImg, string nickname)
	{
		UnityEngine.Debug.Log("nickname is:" + nickname);
		UnityEngine.Debug.Log("Render");
		template = GetComponent<UITemplate>();
		template.OnRenderCallback += OnInviteRenderCallback;
		template.target.transform.Find("headimg").Find("mask").Find("Image")
			.GetComponent<RawImage>()
			.texture = headImg;
			template.target.transform.Find("name").GetComponent<Text>().text = nickname;
			template.renderPreferb();
		}

		private void OnInviteClick(Texture2D texture, string name, Action<Errcode> callback)
		{
			UnityEngine.Debug.Log("OnInviteClick...");
			GetComponent<UITemplate>().ObjectPrefab = inviteObjectprefab;
			InviteRender(texture, name);
			shareCallback = callback;
		}

		private void OnShareResultHandler(int reqID, ResponseState state, PlatformType type, Hashtable result)
		{
			Errcode errcode = new Errcode();
			switch (state)
			{
			case ResponseState.Success:
				ShareMsg = "share successfully!";
				TotalGA.Event("share_success");
				errcode.errorCode = Errcode.OK;
				break;
			case ResponseState.Fail:
				TotalGA.Event("share_fail");
				ShareMsg = "fail! throwable stack = " + result["stack"] + "; error msg = " + result["msg"];
				errcode.errorCode = Errcode.FAIL;
				break;
			case ResponseState.Cancel:
				ShareMsg = "cancel !";
				TotalGA.Event("share_cancel");
				errcode.errorCode = Errcode.SHARE_CANCEL;
				break;
			}
			UnityEngine.Debug.Log(ShareMsg);
			shareClock = false;
		}

		private IEnumerator showSavedInfo()
		{
			yield return new WaitForSeconds(WaitTime);
			IsShowMsg = false;
		}

		private void OnDisable()
		{
			IsShowMsg = false;
		}
	}
