using Facebook.Unity;
using Firebase.Analytics;
using System.Collections.Generic; 

public class TotalGA
{
	public static void Event(string action_id)
	{
		//TotalGA.Event(action_id);
        //TalkingDataGA.OnEvent(action_id, null);
        FirebaseAnalytics.LogEvent(action_id);

    }

	public static void Event(string action_id, string label)
	{
		//TotalGA.Event(action_id, label);
		//Dictionary<string, object> dictionary = new Dictionary<string, object>();
		//dictionary.Add("label", label);
		FirebaseAnalytics.LogEvent(action_id, "label", label);
        //TalkingDataGA.OnEvent(action_id, dictionary);
    }

	public static void CountSingleEvent_FB(string name)
	{
		FB.LogAppEvent(name);
		FirebaseAnalytics.LogEvent(name);
	}

	public static void CountEvent_FB(string name)
	{
		FB.LogAppEvent(name, 1f);
		FirebaseAnalytics.LogEvent(name);
	}

	public static void CountEventNumber_FB(string name, int number)
	{
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary["number"] = number;
		FB.LogAppEvent(name, number, dictionary);
		FirebaseAnalytics.LogEvent(name, "number", number);
	}

	public static void CountPurchaseResult_FB(bool success)
	{
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		dictionary["fb_success"] = (success ? 1 : 0);
		FB.LogAppEvent("fb_mobile_add_payment_info", null, dictionary);
	}
}
