using System;
using System.Collections;
using UI.ThreeDimensional;
using UnityEngine;
using UnityEngine.UI;

public class ColorRecentPageInitController : MonoBehaviour
{
	[SerializeField] 
	private UIColorButton[] _colorButtons;
	
	[SerializeField]
	private Image HistoryBtnImage;

	[SerializeField] private CreateColorBarController _colorBarController;
	// [SerializeField]
	// private GameObject colorComplexItem;

	// private int ColorCLCount = 5;
 

	public void RemoveAllSelection()
	{
		foreach (var c in _colorButtons)
		{
			c.RemoveSelection();
		}
	}
	public void InitContent()
	{
		if (ApplicationModel.recentColorlist != null)
		{
			while (ApplicationModel.recentColorlist.Count > _colorButtons.Length)
			{
				ApplicationModel.recentColorlist.RemoveAt(0);
			}
 
			for(int i = 0; i < _colorButtons.Length; i++)
			{
				if (i >= ApplicationModel.recentColorlist.Count)
				{
					_colorButtons[i].gameObject.SetActive(false);
					continue;
				}
				var item = ApplicationModel.recentColorlist[i];
				var b = _colorButtons[i];
				b.gameObject.SetActive(true);

				b.attachedColor = item.color;
				if (item.isGradient)
				{ 
					b.EnableGradientMode(true);
					b.SetColor(CreateColorBarController.DEFAULT_COLOR); 
					b.SetGradientCoordColor(item.gradientColorCoordinate);
					b.PlayAnim();
					var button = b.GetComponent<Button>();
					button.onClick.RemoveAllListeners();
					button.onClick.AddListener(() =>
					{
						_colorBarController.RemoveAllSelection();
						RemoveAllSelection();
						b.Select();
						onClickGradient(item.color, item.gradientColorCoordinate);
					});
				} 
				else  
				{   
					b.EnableGradientMode(false); 
					b.SetColor(item.color); 
					b.PlayAnim();  
					var button = _colorButtons[i].GetComponent<Button>();
					button.onClick.RemoveAllListeners();
					button.onClick.AddListener(() =>
					{
						_colorBarController.RemoveAllSelection();
						RemoveAllSelection();
						b.Select();
						onClick(b.attachedColor);
					});
				}  
			}
		}
		// Transform transform = base.transform.Find("ColorComplex").transform;
		// int num = 0;
		// if (ApplicationModel.recentColorlist != null)
		// {
		// 	num = ApplicationModel.recentColorlist.Count;
		// }
		// for (int i = 0; i < 9; i++)
		// {
		// 	GameObject gameObject = UnityEngine.Object.Instantiate(colorComplexItem, transform, worldPositionStays: false);
		// 	int num2 = (i >= 4) ? ((i + 1) % 5) : i;
		// 	int num3 = (i >= 4) ? 1 : 0;
		// 	gameObject.transform.localPosition += Vector3.right * 111f * num2;
		// 	gameObject.transform.localPosition += Vector3.down * 121f * num3;
		// 	if (num3 == 0)
		// 	{
		// 		gameObject.transform.localPosition += Vector3.right * 59f;
		// 	}
		// 	if (ApplicationModel.recentColorlist != null && num > i)
		// 	{
		// 		ColorComplex colorComplex = ApplicationModel.recentColorlist[num - i - 1];
		// 		if (colorComplex.isGradient)
		// 		{
		// 			gameObject.transform.Find("colorGradientBtn").gameObject.SetActive(value: true);
		// 			gameObject.transform.Find("ColorSolidBtn").gameObject.SetActive(value: false);
		// 			gameObject.transform.Find("colData").GetComponent<Image>().color = colorComplex.color;
		// 			gameObject.transform.Find("colorGradientBtn").GetComponent<UIObject3D>().ColorCoord = colorComplex.gradientColorCoordinate;
		// 			gameObject.transform.Find("colorGradientBtn").GetComponent<Button>().onClick.AddListener(delegate
		// 			{
		// 				onClickGradient(colorComplex.color, isGradient: true, colorComplex.gradientColorCoordinate);
		// 			});
		// 			UnityEngine.Debug.Log("InitContent.colorComplex.if.color... = " + colorComplex.color);
		// 		}
		// 		else
		// 		{
		// 			gameObject.transform.Find("colorGradientBtn").gameObject.SetActive(value: false);
		// 			gameObject.transform.Find("ColorSolidBtn").gameObject.SetActive(value: true);
		// 			gameObject.transform.Find("ColorSolidBtn").GetComponent<Image>().color = colorComplex.color;
		// 			gameObject.transform.Find("colData").GetComponent<Image>().color = colorComplex.color;
		// 			gameObject.transform.Find("ColorSolidBtn").GetComponent<Button>().onClick.AddListener(delegate
		// 			{
		// 				onClick(colorComplex.color, isGradient: false);
		// 			});
		// 			UnityEngine.Debug.Log("InitContent.colorComplex.if.color... = " + colorComplex.color);
		// 		}
		// 	}
		// }
	}

	private void onClick(Color col)
	{
		UnityEngine.Debug.Log("onclick color");
		ApplicationModel.IsGradient = false;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(false, col, new Vector2(0f, 0f));
		this._colorBarController.SetHistoryColor(col);
	}

	private void onClickGradient(Color col, Vector2 colorcoord)
	{
		ApplicationModel.IsGradient = true;
		WorkShopManagerController.instance.ChangePencilColorTo(col);
		ApplicationModel.currentColorComplex = new ColorComplex(true, col, colorcoord);
		this._colorBarController.SetHistoryGradientCoordColor(colorcoord);
	}

	public void ScaleDown(float duration = 0.2f)
	{
		foreach (var b in _colorButtons)
		{
			LeanTween.scale(b.transform.parent.gameObject, Vector3.zero, duration);
		}
	}

	public void ScaleUp(float duration = 0.2f)
	{
		foreach (var b in _colorButtons)
		{
			LeanTween.scale(b.transform.parent.gameObject, Vector3.one * 0.65f, duration);
		}
	}

	private void Awake()
	{
		AssginEvents();
		foreach(var b in _colorButtons)
			b.Init(_colorBarController.gradientTexture);
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		DragEventProxy.OnAddColorToRecentList += RefreshPanel;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnAddColorToRecentList -= RefreshPanel;
	}

	private void RefreshPanel()
	{
		// DestoryChild(base.transform.Find("ColorComplex"));
		InitContent();
	}

	// private void DestoryChild(Transform transform)
	// {
	// 	IEnumerator enumerator = transform.GetEnumerator();
	// 	try
	// 	{
	// 		while (enumerator.MoveNext())
	// 		{
	// 			Transform transform2 = (Transform)enumerator.Current;
	// 			UnityEngine.Object.Destroy(transform2.gameObject);
	// 		}
	// 	}
	// 	finally
	// 	{
	// 		IDisposable disposable;
	// 		if ((disposable = (enumerator as IDisposable)) != null)
	// 		{
	// 			disposable.Dispose();
	// 		}
	// 	}
	// }
}
