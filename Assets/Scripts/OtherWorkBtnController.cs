using UnityEngine;

public class OtherWorkBtnController : MonoBehaviour
{
	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject OtherWorkCanvas;

	[SerializeField]
	private GameObject InspirationCanvas;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void BackToMyworkCanvas()
	{
		OtherWorkCanvas.SetActive(value: false);
		if (ApplicationModel.lastVisibleScreen == "InspirationCanvas")
		{
			UIRoot.ShowUI(InspirationCanvas);
		}
		else
		{
			UIRoot.ShowUI(MyworkCanvas);
		}
	}
}
