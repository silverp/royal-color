using System;
using UnityEngine;

public class MergeDataAfterLogin : MonoBehaviour
{
	private bool isGetRemoteEditedList;

	private bool isGetChoicedImgList;

	private string getMyworklistUrl = APIFactory.GET_WORK_LIST;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void StartMergeData(Action callback)
	{
		if (ApplicationModel.userInfo != null)
		{
			DragEventProxy.Instance.SendRefreshAfterLoginMsg();
			PublicUserInfoApi.StartMergeData(callback);
		}
	}
}
