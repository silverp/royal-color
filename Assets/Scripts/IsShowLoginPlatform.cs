using UnityEngine;

public class IsShowLoginPlatform : MonoBehaviour
{
	public string platformName;

	private void Start()
	{
		if (platformName == "facebook")
		{
			if (ApplicationModel.currentChannel == "googleplay")
			{
				base.gameObject.SetActive(value: true);
			}
			else
			{
				base.gameObject.SetActive(value: false);
			}
		}
		else if (ApplicationModel.currentChannel == "googleplay")
		{
			base.gameObject.SetActive(value: false);
		}
		else
		{
			base.gameObject.SetActive(value: true);
		}
	}
}
