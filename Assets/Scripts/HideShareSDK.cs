using UnityEngine;

public class HideShareSDK : MonoBehaviour
{
	[SerializeField]
	private GameObject shareSDK;

	private void OnEnable()
	{
		shareSDK.SetActive(value: false);
	}

	private void OnDisable()
	{
		shareSDK.SetActive(value: true);
	}
}
