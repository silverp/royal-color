using UnityEngine;

public class ChangePanelWidthController : MonoBehaviour
{
	private void Start()
	{
		if (Screen.width > 1080)
		{
			RectTransform component = base.gameObject.GetComponent<RectTransform>();
			float x = Screen.width;
			Vector2 sizeDelta = base.gameObject.GetComponent<RectTransform>().sizeDelta;
			component.sizeDelta = new Vector2(x, sizeDelta.y);
		}
	}

	private void Update()
	{
	}
}
