using System;
using UnityEngine;
using UnityEngine.UI;

public class OpenVipNote : MonoBehaviour
{
	[SerializeField]
	private GameObject OpenVipPanel;

	[SerializeField]
	private GameObject Purchase;

	[SerializeField]
	private GameObject Image;

	[SerializeField]
	private GameObject adButton;

	private void Start()
	{
	}

	public void close()
	{
		LeanTween.scale(OpenVipPanel.GetComponent<RectTransform>(), new Vector3(0f, 0f, 0f), 0.25f).setOnComplete((Action)delegate
		{
			OpenVipPanel.SetActive(value: false);
			OpenVipPanel.transform.Find("mask").Find("Image").GetComponent<RawImage>()
				.texture = null;
			});
		}

		public void GotoPurchasePage()
		{
			Purchase.SetActive(value: true);
			RectTransform component = Purchase.transform.GetComponent<RectTransform>();
			Vector2 anchoredPosition = Purchase.transform.GetComponent<RectTransform>().anchoredPosition;
			component.anchoredPosition = new Vector2(anchoredPosition.x, -1920f);
			LeanTween.moveY(Purchase.GetComponent<RectTransform>(), 0f, 0.4f).setEase(LeanTweenType.easeOutQuad).setOnComplete((Action)delegate
			{
				OpenVipPanel.SetActive(value: false);
			});
			TotalGA.Event("BuyVip_button_click");
		}

		private void OnEnable()
		{
			Image.GetComponent<RawImage>().texture = null;
		}
	}
