using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public class ColorTheme
{
	public string Name;

	public List<string> Colors;
	 
	[JsonIgnore]
	public List<Color> unityColors { get; set; } 

	public string bgColor; 
	
	[JsonIgnore]
	public Color bgColorUnity { get; set; }

	private bool initialized = false;
	public ColorTheme(string name, List<string> colors, string bgColor)
	{
		Name = name;
		Colors = colors; 
		this.bgColor = bgColor;
	}

	public void Initialize()
	{
		if (!initialized)
		{
			initialized = true;
			unityColors = new List<Color>();
			Color color = default;
			ColorUtility.TryParseHtmlString(bgColor, out color);
			bgColorUnity = color;
			
			foreach (var c in this.Colors)
			{
				color = default;
				ColorUtility.TryParseHtmlString(c, out color);
				unityColors.Add(color);
			}
		}
	}
}
