using Newtonsoft.Json;
using System;
using UnityEngine;

public class DataBroker
{
	public static void getString(URLRequest request, Action<string> callback, Action<Exception> onError)
	{
		if (request.Method == URLRequest.REQ_METHOD.POST)
		{
			HttpRequest.Instance.WWWPostRequest(request.getUrl(), request.body, callback, onError);
		}
		else
		{
			HttpRequest.Instance.RequestString(request.getUrl(), request.cacheType, callback, onError);
		}
	}

	public static void getBytes(URLRequest request, Action<byte[]> callback, Action<Exception> onError)
	{
		HttpRequest.Instance.RequestBytes(request.getUrl(), request.cacheType, callback, onError);
	}

	public static void getTexture2D(URLRequest request, Action<Texture2D> callback, Action<Exception> onError)
	{
		request.cacheType = HttpRequest.CacheType.FILE;
		HttpRequest.Instance.RequestTexture(request.getUrl(), request.cacheType, callback, onError);
	}

	public static void getJson<T>(URLRequest request, Action<T> callback, Action<Exception> onError)
	{
		getString(request, delegate(string res)
		{
			T obj = JsonConvert.DeserializeObject<T>(res);
			callback(obj);
		}, onError);
	}

	public static void getTexture2D(string url, Action<Texture2D> callback, Action<Exception> onError)
	{
	}
}
