using System;
using UnityEngine;
using UnityEngine.UI;

public class SiginEventController : MonoBehaviour
{
	[SerializeField]
	private GameObject beanCnt;

	[SerializeField]
	private GameObject BeanNoteBox;

	private void OnEnable()
	{
		QuerySignStatus();
	}

	private void QuerySignStatus()
	{
		if (ApplicationModel.userInfo != null)
		{
			URLRequest uRLRequest = APIFactory.create(APIFactory.QUERY_SIGN_STATUS);
			uRLRequest.addParam("uid", ApplicationModel.userInfo.Uid);
			uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
			DataBroker.getJson(uRLRequest, delegate(SignStatusJson detail)
			{
				if (detail.data.isSign)
				{
					base.transform.GetChild(0).gameObject.SetActive(value: false);
					base.transform.GetChild(1).gameObject.SetActive(value: true);
				}
				else
				{
					base.transform.GetChild(0).gameObject.SetActive(value: true);
					base.transform.GetChild(1).gameObject.SetActive(value: false);
				}
			}, delegate(Exception ex)
			{
				UnityEngine.Debug.Log(ex);
			});
		}
	}

	public void ClickSignBtn()
	{
		if (base.transform.GetChild(0).gameObject.activeSelf)
		{
			PublicControllerApi.Sign(SignCallBack);
			TotalGA.CountSingleEvent_FB("sign");
		}
	}

	private void SignCallBack(bool isSiginSuccess)
	{
		if (isSiginSuccess)
		{
			SignSuccess();
		}
		else
		{
			SignFail();
		}
	}

	private void SignSuccess()
	{
		base.transform.GetChild(0).gameObject.SetActive(value: false);
		base.transform.GetChild(1).gameObject.SetActive(value: true);
		BeanAction.request(BeanAction.SIGN, BeanActionRequestCallback);
	}

	private void BeanActionRequestCallback(Errcode err, int amount)
	{
		if (err.errorCode == Errcode.OK)
		{
			UnityEngine.Debug.Log("amount is:" + amount);
			BeanNoteBox.SetActive(value: true);
			BeanNoteBox.transform.GetChild(0).GetComponent<Text>().text = "+" + amount.ToString();
			LeanTween.alpha(BeanNoteBox, 1f, 1f).setOnComplete((Action)delegate
			{
				BeanNoteBox.SetActive(value: false);
				ApplicationModel.userInfo.BeanCnt += amount;
				beanCnt.GetComponent<Text>().text = ApplicationModel.userInfo.BeanCnt.ToString();
			});
		}
	}

	private void SignFail()
	{
		UnityEngine.Debug.Log("SignFail..");
	}
}
