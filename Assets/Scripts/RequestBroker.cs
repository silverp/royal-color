using System;

public class RequestBroker
{
	public static void GetReferenceColorList(string imageId, Action<Errcode, ReferenceColorData> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.ORIGIN_DETAIL, HttpRequest.CacheType.MEMORY);
		uRLRequest.addParam("imageId", imageId);
		DataBroker.getJson(uRLRequest, delegate(ReferenceColorDataJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
				callback(err, detail.data);
			}
			else
			{
				err.errorCode = Errcode.FAIL;
				callback(err, null);
			}
		}, delegate
		{
			err.errorCode = Errcode.FAIL;
			callback(err, null);
		});
	}
}
