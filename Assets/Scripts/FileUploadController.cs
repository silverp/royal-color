using Newtonsoft.Json;
using System;
using System.Collections;
using UnityEngine;

public class FileUploadController : MonoBehaviour
{
	private string likeEventUrl = "/image/like";

	private string uploadFileUrl = "/image/saveWork";

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		FileUploadEventProxy.OnClickImage += ClickImage;
		FileUploadEventProxy.OnUploadFile += UploadFile;
		FileUploadEventProxy.OnUploadUserEditedFile += UploadUserEditedFile;
		FileUploadEventProxy.OnLikeEvent += LikeEvent;
		FileUploadEventProxy.OnLikeMyworkEvent += LikeMyworkEvent;
		FileUploadEventProxy.OnPublishMyworkEvent += PublishWork;
	}

	private void DiscardEvents()
	{
		FileUploadEventProxy.OnClickImage -= ClickImage;
		FileUploadEventProxy.OnUploadFile -= UploadFile;
		FileUploadEventProxy.OnUploadUserEditedFile -= UploadUserEditedFile;
		FileUploadEventProxy.OnLikeEvent -= LikeEvent;
		FileUploadEventProxy.OnLikeMyworkEvent -= LikeMyworkEvent;
		FileUploadEventProxy.OnPublishMyworkEvent -= PublishWork;
	}

	private void StartPublish(BaseEditedImg img, Action<Errcode> callback)
	{
		Errcode err = new Errcode();
		URLRequest uRLRequest = APIFactory.create(APIFactory.PUBLISH_INSPIRATION);
		uRLRequest.addParam("workId", img.workId);
		uRLRequest.addParam("authorId", ApplicationModel.userInfo.Uid);
		uRLRequest.addParam("sessionId", ApplicationModel.userInfo.SessionId);
		UnityEngine.Debug.Log("PublishWork url is:" + uRLRequest.getUrl());
		DataBroker.getJson(uRLRequest, delegate(BeanActionReturnJson detail)
		{
			if (detail.status == 0)
			{
				err.errorCode = Errcode.OK;
			}
			else if (detail.status == 10406)
			{
				err.errorCode = Errcode.HAS_PUBLISHED;
			}
			else
			{
				err.errorCode = Errcode.FAIL;
			}
			callback(err);
		}, delegate(Exception ex)
		{
			UnityEngine.Debug.Log(ex);
			err.errorCode = Errcode.FAIL;
			callback(err);
		});
	}

	private void PublishWork(BaseEditedImg img, Action<Errcode> callback)
	{
		UnityEngine.Debug.Log("fileuploader PublishWork");
		if (img.workId == null || img.workId == string.Empty)
		{
			StartCoroutine(UploadUserEditedFileCo(img, delegate(BaseEditedImg image)
			{
				StartPublish(image, callback);
			}));
		}
		else
		{
			StartPublish(img, callback);
		}
	}

	private void LikeMyworkEvent(BaseEditedImg img, bool islike)
	{
		if (ApplicationModel.userInfo == null)
		{
			DragEventProxy.Instance.SendOffLineMsg("MyworkCanvas");
		}
		else if (img.workId == null || img.workId == string.Empty)
		{
			StartCoroutine(UploadUserEditedFileCo(img, delegate(BaseEditedImg image)
			{
				LikeMyworkEvent(image, islike);
			}));
		}
		else
		{
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + likeEventUrl + "?uid=" + ApplicationModel.userInfo.Uid + "&authorUid=" + ApplicationModel.userInfo.Uid + "&workId=" + img.workId + "&action=" + GetActionStrByBool(islike) + "&sessionId=" + ApplicationModel.userInfo.SessionId, delegate(string json, bool isSuccess)
			{
				LikeRequestCallback(json, isSuccess);
			});
		}
	}

	private void LikeEvent(string uid, string workId, bool islike)
	{
		if (ApplicationModel.userInfo == null)
		{
			DragEventProxy.Instance.SendOffLineMsg(ApplicationModel.lastVisibleScreen);
		}
		else
		{
			NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + likeEventUrl + "?uid=" + ApplicationModel.userInfo.Uid + "&authorUid=" + uid + "&workId=" + workId + "&action=" + GetActionStrByBool(islike) + "&sessionId=" + ApplicationModel.userInfo.SessionId, delegate(string json, bool isSuccess)
			{
				LikeRequestCallback(json, isSuccess);
			});
		}
	}

	public void LikeRequestCallback(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			StandardReturnCode standardReturnCode = JsonConvert.DeserializeObject<StandardReturnCode>(json);
			if (standardReturnCode.status != 0 && standardReturnCode.status == 10403)
			{
				DragEventProxy.Instance.SendOffLineMsg(ApplicationModel.lastVisibleScreen);
			}
		}
	}

	public void UploadFile(BaseEditedImg img, string uuid)
	{
		StartCoroutine(UploadFileCo(img, uuid));
		TotalGA.CountEventNumber_FB("complete_work", 1);
	}

	public void ClickImage(BaseEditedImg img, string uuid)
	{
		StartCoroutine(UploadClickEvent(uuid, img));
	}

	public void UploadUserEditedFile(BaseEditedImg img)
	{
		UnityEngine.Debug.Log("UploadUserEditedFile");
		StartCoroutine(UploadUserEditedFileCo(img));
	}

	public void UploadUserEditedPixleFile()
	{
	}

	public IEnumerator UploadUserEditedFileCo(BaseEditedImg img, Action<BaseEditedImg> callback = null)
	{
		Errcode err = new Errcode();
		if (JudgeIsExistLocalFile(img))
		{
			UnityEngine.Debug.Log("UploadUserEditedFileCo callback:");
			yield return new WaitForSeconds(1.5f);
			URLRequest urlRequest;
			if (img is EditedImg)
			{
				UnityEngine.Debug.Log("img is editedImg");
				urlRequest = GetUrlFormByEditedImg(img);
			}
			else
			{
				UnityEngine.Debug.Log("img is PixeleditedImg");
				urlRequest = GetUrlFormByPixelEditedImg(img);
			}
			DataBroker.getJson(urlRequest, delegate(UploadEditedFileReturnJson detail)
			{
				if (detail.status == 0)
				{
					err.errorCode = Errcode.OK;
					UploadUserEditedFileCallback(err, detail.data.workId, img, callback);
				}
				else
				{
					err.errorCode = Errcode.FAIL;
					UploadUserEditedFileCallback(err, string.Empty, null, callback);
				}
			}, delegate
			{
				err.errorCode = Errcode.FAIL;
				UploadUserEditedFileCallback(err, string.Empty, null, callback);
			});
		}
	}

	private URLRequest GetUrlFormByEditedImg(BaseEditedImg img)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.SAVE_WORK, HttpRequest.CacheType.None, URLRequest.REQ_METHOD.POST);
		uRLRequest.addbody("imageId", img.imageId);
		uRLRequest.addbody("deviceId", ApplicationModel.device_id);
		uRLRequest.addbody("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addbody("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addbody("clickPos", GetTex2dFromImgByPath(img.imageId + "_pos").EncodeToPNG(), img.imageId + "_pos");
		uRLRequest.addbody("palette", GetTex2dFromImgByPath(img.imageId + "_ed").EncodeToPNG(), img.imageId + "_ed");
		uRLRequest.addbody("finalImg", GetTex2dFromImgByPath(img.imageId + "_ok").EncodeToPNG(), img.imageId + "_ok");
		return uRLRequest;
	}

	private bool JudgeIsExistLocalFile(BaseEditedImg img)
	{
		if (!PublicToolController.IsExistFile(img.imageId + "_ok"))
		{
			return false;
		}
		if (img is EditedImg)
		{
			if (!PublicToolController.IsExistFile(img.imageId + "_pos"))
			{
				return false;
			}
			if (!PublicToolController.IsExistFile(img.imageId + "_ed"))
			{
				return false;
			}
		}
		else if (img is PixleEditedImg)
		{
			if (!PublicToolController.IsExistFile(img.imageId + "_cntMap"))
			{
				return false;
			}
			if (!PublicToolController.IsExistFile(img.imageId + "_colorMap"))
			{
				return false;
			}
		}
		return true;
	}

	private URLRequest GetUrlFormByPixelEditedImg(BaseEditedImg pixleEditedImg)
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.SAVE_PIXEL_WORK, HttpRequest.CacheType.None, URLRequest.REQ_METHOD.POST);
		uRLRequest.addParam("imageId", pixleEditedImg.imageId);
		uRLRequest.addParam("deviceId", ApplicationModel.device_id);
		uRLRequest.addParam("uid", PublicUserInfoApi.GetUserUid());
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		uRLRequest.addbody("countMap", GetTex2dFromImgByPath(pixleEditedImg.imageId + "_cntMap").EncodeToPNG(), pixleEditedImg.imageId + "_cntMap");
		uRLRequest.addbody("colorMap", GetTex2dFromImgByPath(pixleEditedImg.imageId + "_colorMap").EncodeToPNG(), pixleEditedImg.imageId + "_colorMap");
		uRLRequest.addbody("finalImg", GetTex2dFromImgByPath(pixleEditedImg.imageId + "_ok").EncodeToPNG(), pixleEditedImg.imageId);
		return uRLRequest;
	}

	private void UploadUserEditedFileCallback(Errcode err, string workId, BaseEditedImg img, Action<BaseEditedImg> calback)
	{
		UnityEngine.Debug.Log("UploadUserEditedFileCallback ........");
		if (err.errorCode == Errcode.OK)
		{
			img.workId = workId;
			calback?.Invoke(img);
		}
	}

	public IEnumerator UploadFileCo(BaseEditedImg img, string uuid)
	{
		yield return new WaitForSeconds(1f);
		WWWForm postForm = new WWWForm();
		postForm.AddBinaryData("file", GetTex2dFromImgByPath(img.imageId + "_ok").EncodeToPNG(), img.imageId);
		postForm.AddField("uuid", uuid);
		postForm.AddField("imageId", img.imageId);
		yield return new WWW(ApplicationModel.UPLOAD_IMAGE_EVENT, postForm);
	}

	private IEnumerator UploadClickEvent(string uuid, BaseEditedImg img)
	{
		WWWForm postForm = new WWWForm();
		postForm.AddField("userId", uuid);
		postForm.AddField("imageId", img.imageId);
		UnityEngine.Debug.Log("click image url is:" + ApplicationModel.CLICK_IMAGE_EVENT);
		yield return new WWW(ApplicationModel.CLICK_IMAGE_EVENT, postForm);
	}

	private Texture2D GetTex2dFromImgByPath(string path)
	{
		Texture2D texture2D = new Texture2D(1, 1, TextureFormat.ARGB32, mipChain: false);
		texture2D.LoadImage(PublicToolController.FileReaderBytes(path), markNonReadable: false);
		texture2D.Apply();
		return texture2D;
	}

	private string GetActionStrByBool(bool islike)
	{
		string empty = string.Empty;
		if (islike)
		{
			return "like";
		}
		return "unlike";
	}
}
