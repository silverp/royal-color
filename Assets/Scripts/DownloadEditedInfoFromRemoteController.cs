using Newtonsoft.Json;
using System;
using UnityEngine;

public class DownloadEditedInfoFromRemoteController : MonoBehaviour
{
	public class EditedImgInfo
	{
		public string imageId;

		public string palette;

		public string clickpos;
	}

	public class EditedImgInfoJson
	{
		public int status;

		public EditedImgInfo data;

		public string msg;
	}

	private Action<Errcode> callback;

	private bool isDownloadPalette;

	private bool isDownloadClickpos;

	private Errcode Errcode = new Errcode();

	[SerializeField]
	private RefImageController reImage;

	private void Start()
	{
	}

	private void Update()
	{
	}

	public void DownloadEditedInfo(string workId, Action<Errcode> callback)
	{
		UnityEngine.Debug.Log("DownloadEditedInfo!!!!");
		this.callback = callback;
		UnityEngine.Debug.Log("request detail url is:" + ApplicationModel.HOST + "/image/getEditedInfo?uid=" + ApplicationModel.userInfo.Uid + "&workId=" + workId + "&sessionId=" + ApplicationModel.userInfo.SessionId);
		NetLoadcontroller.Instance.RequestUrlWidthMethodGet(ApplicationModel.HOST + "/image/getEditedInfo?uid=" + ApplicationModel.userInfo.Uid + "&workId=" + workId + "&sessionId=" + ApplicationModel.userInfo.SessionId, RequestCallback);
	}

	private void RequestCallback(string json, bool isSuccess)
	{
		if (isSuccess)
		{
			EditedImgInfoJson editedImgInfoJson = JsonConvert.DeserializeObject<EditedImgInfoJson>(json);
			if (editedImgInfoJson.msg == "ok")
			{
				Errcode.errorCode = 0;
				EditedImgInfo data = editedImgInfoJson.data;
				DownLoadEditedImg(data);
			}
			else if (editedImgInfoJson.status == 10403)
			{
				Errcode.errorCode = 2;
				callback(Errcode);
			}
			else
			{
				Errcode.errorCode = 1;
				callback(Errcode);
			}
		}
		else
		{
			Errcode.errorCode = 1;
			callback(Errcode);
		}
	}

	private void DownLoadEditedImg(EditedImgInfo editedImgInfo)
	{
		NetLoadcontroller.Instance.RequestBytes(editedImgInfo.palette, delegate(byte[] bytes)
		{
			CopyPaletteTolocal(bytes, editedImgInfo.imageId);
			isDownloadPalette = true;
			TestIsDownloadAll();
		});
		NetLoadcontroller.Instance.RequestBytes(editedImgInfo.clickpos, delegate(byte[] bytes)
		{
			CopyClickposTolocal(bytes, editedImgInfo.imageId);
			isDownloadClickpos = true;
			TestIsDownloadAll();
		});
	}

	private void CopyPaletteTolocal(byte[] bytes, string imageId)
	{
		PublicToolController.FileCreatorBytes(bytes, imageId + "_ed", isStoreChange: true);
	}

	private void CopyClickposTolocal(byte[] bytes, string imageId)
	{
		PublicToolController.FileCreatorBytes(bytes, imageId + "_pos", isStoreChange: true);
	}

	private void TestIsDownloadAll()
	{
		if (isDownloadPalette && isDownloadClickpos)
		{
			callback(Errcode);
		}
	}
}
