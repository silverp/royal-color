using System.Collections;
using UnityEngine;

public class RestorePlayfebManagement : MonoBehaviour
{
	public static RestorePlayfebManagement instance;

	private float WAIT_TIME = 5f;

	private float time;

	private void Start()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Update()
	{
		if (time > 0f)
		{
			time -= Time.deltaTime;
		}
	}

	public void StoreEditedList()
	{
		StartCoroutine(StoreEditedImgList());
	}

	public void StoreRateTimes()
	{
		if (time <= 0f)
		{
			time = WAIT_TIME;
			StartCoroutine(ReStoreRateTimes());
		}
	}

	private IEnumerator ReStoreRateTimes()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		ApplicationModel.RestoreRateTimes();
	}

	private IEnumerator StoreEditedImgList()
	{
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		yield return null;
		ApplicationModel.RestoreEditedImgList();
	}
}
