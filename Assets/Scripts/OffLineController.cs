using System;
using UnityEngine;

public class OffLineController : MonoBehaviour
{
	[SerializeField]
	private GameObject loginPanel;

	[SerializeField]
	private GameObject canvasList;

	[SerializeField]
	private GameObject InspirationCanvas;

	[SerializeField]
	private GameObject MyworkCanvas;

	[SerializeField]
	private GameObject OtherWorkCanvas;

	private void Awake()
	{
		AssginEvents();
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private void AssginEvents()
	{
		DragEventProxy.OnOffLine += OffLine;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnOffLine -= OffLine;
	}

	private void OffLine(string canvasName, bool isOffline = false)
	{
		loginPanel.SetActive(value: true);
		loginPanel.transform.GetChild(0).GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
		LeanTween.scale(loginPanel.transform.GetChild(0).GetComponent<RectTransform>(), new Vector3(1f, 1f, 1f), 0.25f).setOnComplete((Action)delegate
		{
			Transform go;
			if (canvasName == "MyworkCanvas")
				go = MyworkCanvas.transform;
			else if (canvasName == "InspirationCanvas")
				go = InspirationCanvas.transform;
			else if (canvasName == "OtherWorkCanvas")
				go = OtherWorkCanvas.transform;
			else  
				go = canvasList.transform.Find(canvasName);
			go.gameObject.SetActive(value: false);
			UnityEngine.Debug.Log("complete scale!!!");
		});
		ApplicationModel.ReturnPageName = canvasName;
		if (isOffline)
		{
			ClearDataAfterLogout.Instance.ClearUserData();
			RestorePlayfebManagement.instance.StoreEditedList();
		}
	}
}
