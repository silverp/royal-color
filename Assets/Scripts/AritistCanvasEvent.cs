using UnityEngine;

public class AritistCanvasEvent : MonoBehaviour
{
	[SerializeField]
	private GameObject CateDetailCanvas;

	public static AritistCanvasEvent Instance;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	public void EnterCateDetailPage(string albumId)
	{
		UnityEngine.Debug.Log("albumid is:" + albumId);
		ArtistAlbumModel albumByAlbumId = GetAlbumByAlbumId(albumId);
		base.transform.parent.gameObject.SetActive(value: false);
		CateDetailCanvas.transform.Find("CatePage").GetComponent<CatePageScrollRectAdapter>().currentArtistAlbumModel = albumByAlbumId;
		CateDetailCanvas.transform.Find("CatePage").GetComponent<CatePageScrollRectAdapter>().isRefresh = true;
		CatePageScrollRectAdapter.from = CatePageScrollRectAdapter.PageFrom.artist;
		CateDetailCanvas.SetActive(value: true);
	}

	private ArtistAlbumModel GetAlbumByAlbumId(string albumId)
	{
		foreach (ArtistAlbumModel artistAlbumModel in ApplicationModel.artistAlbumModelList)
		{
			if (artistAlbumModel.albumId == albumId)
			{
				return artistAlbumModel;
			}
		}
		return null;
	}
}
