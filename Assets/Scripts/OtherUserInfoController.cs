using UnityEngine;
using UnityEngine.UI;

public class OtherUserInfoController : MonoBehaviour
{
	[SerializeField]
	private GameObject followStatus;

	[SerializeField]
	private GameObject followCnt;

	[SerializeField]
	private GameObject fancyCnt;

	[SerializeField]
	private GameObject vipIcon;

	private void OnEnable()
	{
		InitUserInfo();
	}

	public void InitUserInfo()
	{
		URLRequest uRLRequest = APIFactory.create(APIFactory.GET_USER_INFO);
		uRLRequest.addParam("uid", ApplicationModel.otherUserInfo.Uid);
		uRLRequest.addParam("sessionId", PublicUserInfoApi.GetSessionId());
		UnityEngine.Debug.Log("InitUserInfo urlRequest url is:" + uRLRequest.getUrl());
		DataBroker.getJson(uRLRequest, delegate(OtherUserInfoJson detail)
		{
			if (detail.status == 0)
			{
				RequestUserInfoCallback(detail.data);
			}
		}, delegate
		{
		});
	}

	private void RequestUserInfoCallback(OtherUserInfo otherUserInfo)
	{
		ApplicationModel.otherUserInfo.FollowCnt = otherUserInfo.followCnt;
		ApplicationModel.otherUserInfo.FansCnt = otherUserInfo.fansCnt;
		ApplicationModel.otherUserInfo.VipLevel = otherUserInfo.vipLevel;
		followCnt.GetComponent<Text>().text = otherUserInfo.followCnt.ToString();
		fancyCnt.GetComponent<Text>().text = otherUserInfo.fansCnt.ToString();
		FollowBtnController(otherUserInfo.isFollow);
		vipIcon.GetComponent<RawImage>().texture = PublicImgaeApi.GetVipIcon(otherUserInfo.vipLevel);
		changeVipIconPos();
	}

	private void changeVipIconPos()
	{
		RectTransform component = vipIcon.GetComponent<RectTransform>();
		Vector2 sizeDelta = base.transform.Find("name").GetComponent<RectTransform>().sizeDelta;
		float x = sizeDelta.x / 2f + 18f;
		Vector2 anchoredPosition = vipIcon.GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(x, anchoredPosition.y);
	}

	private void clickFollowBtn(bool isFollow)
	{
		UnityEngine.Debug.Log("clickFollowBtn....");
		bool flag = isFollow;
		flag = !flag;
		if (!PublicUserInfoApi.IsHasLogin())
		{
			DragEventProxy.Instance.SendOffLineMsg("OtherWorkCanvas");
			return;
		}
		if (flag)
		{
			PublicControllerApi.AddFollow(ApplicationModel.otherUserInfo.Uid, delegate(Errcode err)
			{
				clickFollowBtnCallback(err, isFollow: true);
			});
		}
		else
		{
			PublicControllerApi.RemoveFollow(ApplicationModel.otherUserInfo.Uid, delegate(Errcode err)
			{
				clickFollowBtnCallback(err, isFollow: false);
			});
		}
		FollowBtnController(flag);
	}

	private void clickFollowBtnCallback(Errcode err, bool isFollow)
	{
		if (err.errorCode == Errcode.OK)
		{
			int num = isFollow ? 1 : (-1);
			ApplicationModel.otherUserInfo.FansCnt += num;
			fancyCnt.GetComponent<Text>().text = ApplicationModel.otherUserInfo.FansCnt.ToString();
		}
	}

	private void FollowBtnController(bool isFollow)
	{
		FollowBtnControl(isFollow);
		followStatus.GetComponent<Button>().onClick.RemoveAllListeners();
		followStatus.GetComponent<Button>().onClick.AddListener(delegate
		{
			clickFollowBtn(isFollow);
		});
	}

	private void FollowBtnControl(bool isFollow)
	{
		if (isFollow)
		{
			followStatus.transform.GetChild(0).gameObject.SetActive(value: false);
			followStatus.transform.GetChild(1).gameObject.SetActive(value: true);
		}
		else
		{
			followStatus.transform.GetChild(0).gameObject.SetActive(value: true);
			followStatus.transform.GetChild(1).gameObject.SetActive(value: false);
		}
	}
}
