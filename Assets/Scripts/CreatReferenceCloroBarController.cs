using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatReferenceCloroBarController : MonoBehaviour
{
	[SerializeField]
	private GameObject ColorButtonBar;

	[SerializeField]
	private GameObject ColorGradientButtonBar;

	[SerializeField]
	private GameObject colorPanelContent;

	[SerializeField]
	private GameObject recentColor;

	[SerializeField]
	private GameObject HistoryBtn;

	private float ScreenWidth = 1080f;

	[SerializeField]
	private string ThemeName;

	private float leftDeviation;

	private List<ColorTheme> currentTheme;

	private int TotalNum;

	private int oldIndex;

	private Color grayCol;

	private Color activeCol;

	private Texture2D history_icon_active;

	private Texture2D history_icon_unactive;

	private void Awake()
	{
		TotalNum = 1;
		grayCol = default(Color);
		ColorUtility.TryParseHtmlString("#999999FF", out grayCol);
		activeCol = default(Color);
		ColorUtility.TryParseHtmlString("#333333FF", out activeCol);
		history_icon_active = Resources.Load<Texture2D>("Images/workshop/icon_History_active");
		history_icon_unactive = Resources.Load<Texture2D>("Images/workshop/icon_History_unactive");
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void OnEnable()
	{
		AssginEvents();
	}

	private void AssginEvents()
	{
		DragEventProxy.OnChangeColorlistPage += ChangeColorlistPage;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnChangeColorlistPage -= ChangeColorlistPage;
	}

	private void ChangeColorlistPage(UnityEngine.Object sender, int index)
	{
		UnityEngine.Debug.Log("ChangeColorlistPage.......");
		changePage(index);
	}

	private void Start()
	{
		InitColorButtonBar();
		SetHistoryBtnFalse();
		changePage(0);
	}

	private void Update()
	{
	}

	private void InitColorButtonBar()
	{
		float num = 0f;
		float num2 = 0f;
		float num3 = 0f;
		for (int i = 0; i < TotalNum; i++)
		{
			GameObject gameObject = (!(ThemeName == "gradient")) ? UnityEngine.Object.Instantiate(ColorButtonBar, base.transform, worldPositionStays: false) : UnityEngine.Object.Instantiate(ColorGradientButtonBar, base.transform, worldPositionStays: false);
			if (i == 0)
			{
				float num4 = ScreenWidth / 2f;
				Vector2 sizeDelta = gameObject.GetComponent<RectTransform>().sizeDelta;
				leftDeviation = num4 - sizeDelta.x / 2f;
				Vector2 sizeDelta2 = gameObject.GetComponent<RectTransform>().sizeDelta;
				num2 = sizeDelta2.y;
				Vector2 sizeDelta3 = gameObject.GetComponent<RectTransform>().sizeDelta;
				num3 = sizeDelta3.x;
				num = leftDeviation;
			}
			string empty = string.Empty;
			if (ThemeName == "gradient")
			{
				empty = ApplicationModel.ColorThemesGradient.gradientColors[i].name;
				gameObject.transform.Find("Text").GetComponent<Text>().text = empty;
			}
			else if (ThemeName == "solid")
			{
				empty = ApplicationModel.ColorThemesPure[i].Name;
				gameObject.transform.Find("Text").GetComponent<Text>().text = empty;
			}
			else
			{
				empty = "推荐";
				gameObject.transform.Find("Text").GetComponent<Text>().text = empty;
			}
			RectTransform component = gameObject.transform.Find("Image").GetComponent<RectTransform>();
			float x = getImageWidth(empty.Length);
			Vector2 sizeDelta4 = gameObject.transform.Find("Image").GetComponent<RectTransform>().sizeDelta;
			component.sizeDelta = new Vector2(x, sizeDelta4.y);
			RectTransform component2 = gameObject.GetComponent<RectTransform>();
			float x2 = num;
			Vector2 anchoredPosition = gameObject.GetComponent<RectTransform>().anchoredPosition;
			component2.anchoredPosition = new Vector2(x2, anchoredPosition.y);
			int _index = i;
			gameObject.GetComponent<Button>().onClick.AddListener(delegate
			{
				changePage(_index);
			});
			num += num3;
		}
		InitColorListBarLength(num + leftDeviation);
	}

	private void HideRecentColorEvent()
	{
		if (recentColor.activeSelf)
		{
			recentColor.SetActive(value: false);
			SetHistoryBtnFalse();
		}
		if (!colorPanelContent.activeSelf)
		{
			colorPanelContent.SetActive(value: true);
			SetBtnEnable(oldIndex);
		}
	}

	private void ShowRecentColorEvent()
	{
		if (!recentColor.activeSelf)
		{
			recentColor.SetActive(value: true);
			SetHistoryBtnActive();
		}
		if (colorPanelContent.activeSelf)
		{
			colorPanelContent.SetActive(value: false);
			SetBtnDisable(oldIndex);
		}
	}

	private void InitColorListBarLength(float length)
	{
		RectTransform component = base.transform.gameObject.GetComponent<RectTransform>();
		Vector2 sizeDelta = base.transform.gameObject.GetComponent<RectTransform>().sizeDelta;
		component.sizeDelta = new Vector2(length, sizeDelta.y);
	}

	private void InitColorListBarPosition()
	{
		RectTransform component = base.transform.gameObject.GetComponent<RectTransform>();
		Vector2 anchoredPosition = base.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
		component.anchoredPosition = new Vector2(-220f, anchoredPosition.y);
	}

	private void changePage(int index)
	{
		if (index <= TotalNum - 1 && index >= 0)
		{
			Vector2 anchoredPosition = base.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
			float x = anchoredPosition.x;
			Vector2 anchoredPosition2 = base.transform.GetChild(index).gameObject.GetComponent<RectTransform>().anchoredPosition;
			float x2 = anchoredPosition2.x;
			float num = x + x2 - leftDeviation;
			RectTransform component = base.transform.gameObject.GetComponent<RectTransform>();
			float x3 = x - num;
			Vector2 anchoredPosition3 = base.transform.gameObject.GetComponent<RectTransform>().anchoredPosition;
			StartCoroutine(MoveToPosition(component, new Vector2(x3, anchoredPosition3.y), 0.3f));
			DragEventProxy.Instance.SendClickColorListBar(index);
			ClickEventManagement.barCount = index;
			ClickEventManagement.ColorThemeIndex = index;
			if (base.transform.parent.name == "PanelColorPureBar")
			{
				ClickEventManagement.pureIndex = index;
			}
			else
			{
				ClickEventManagement.themeIndex = index;
			}
			HideRecentColorEvent();
			SetBtnDisable(oldIndex);
			SetBtnEnable(index);
			oldIndex = index;
		}
	}

	public void ShowRecentColor()
	{
		TotalGA.Event("click_historyColor_btn");
		ShowRecentColorEvent();
	}

	private Color getColorByString(string colorStr)
	{
		Color color = default(Color);
		ColorUtility.TryParseHtmlString(colorStr, out color);
		return color;
	}

	private Color getColorByColorIndex(int blockNum, int index)
	{
		Color32 c = new Color32((byte)(blockNum * 9 + index), 0, 0, 0);
		return c;
	}

	private IEnumerator MoveToPosition(RectTransform rectTransform, Vector2 postion, float TimeToMove)
	{
		Vector2 currentPos = rectTransform.anchoredPosition;
		float t = 0f;
		while (t < 1f)
		{
			t += Time.deltaTime / TimeToMove;
			rectTransform.anchoredPosition = Vector2.Lerp(currentPos, postion, t);
			yield return null;
		}
	}

	private void OnDestroy()
	{
		DiscardEvents();
	}

	private int getIndexByGradientColor(int blockNum)
	{
		return blockNum * 9;
	}

	private int getImageWidth(int num)
	{
		if (ApplicationModel.LANGUAGE == "zh-cn")
		{
			return num * 42 - 4;
		}
		return num * 23 - 4;
	}

	private void SetHistoryBtnFalse()
	{
		HistoryBtn.transform.Find("Image").gameObject.SetActive(value: false);
		HistoryBtn.transform.Find("Text").GetComponent<Text>().color = grayCol;
		HistoryBtn.transform.Find("Button").GetComponent<RawImage>().texture = history_icon_unactive;
	}

	private void SetHistoryBtnActive()
	{
		HistoryBtn.transform.Find("Image").gameObject.SetActive(value: true);
		HistoryBtn.transform.Find("Text").GetComponent<Text>().color = activeCol;
		HistoryBtn.transform.Find("Button").GetComponent<RawImage>().texture = history_icon_active;
	}

	private void SetBtnDisable(int index)
	{
		base.transform.GetChild(index).Find("Image").gameObject.SetActive(value: false);
		base.transform.GetChild(index).Find("Text").GetComponent<Text>()
			.color = grayCol;
		}

		private void SetBtnEnable(int index)
		{
			base.transform.GetChild(index).Find("Image").gameObject.SetActive(value: true);
			base.transform.GetChild(index).Find("Text").GetComponent<Text>()
				.color = activeCol;
			}
		}
