using UnityEngine;
using UnityEngine.UI;

public class NickNameLoginController : MonoBehaviour
{
	private Button button;

	private void Start()
	{
		button = GetComponent<Button>();
		button.onClick.AddListener(ClickNickNameBtn);
	}

	private void Update()
	{
	}

	private void ClickNickNameBtn()
	{
		if (ApplicationModel.userInfo == null)
		{
			DragEventProxy.Instance.SendOffLineMsg("MyworkCanvas");
		}
	}
}
