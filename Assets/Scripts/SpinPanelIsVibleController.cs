using UnityEngine;

public class SpinPanelIsVibleController : MonoBehaviour
{
	private float MAX_WAIT_TIME = 6f;

	private float time;

	private void Awake()
	{
	}

	private void Start()
	{
	}

	private void Update()
	{
		if (time >= 0f)
		{
			time -= Time.deltaTime;
		}
		else
		{
			base.gameObject.SetActive(value: false);
		}
	}

	private void OnEnable()
	{
		time = MAX_WAIT_TIME;
	}
}
