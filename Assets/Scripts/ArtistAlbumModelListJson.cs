using System.Collections.Generic;

public class ArtistAlbumModelListJson
{
	public int status;

	public string msg;

	public List<ArtistAlbumModel> data;
}
