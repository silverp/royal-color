using System;
using System.Collections;
using UnityEngine;

public class GameQuit : MonoBehaviour
{
	private int clickTimes;

	public static event Action Back;

	private void Update()
	{
		if (UnityEngine.Input.GetKeyUp(KeyCode.Escape))
		{
			clickTimes++;
			StartCoroutine(ResetMPressTimes(0.3f));
			if (clickTimes == 2)
			{
				UnityEngine.Debug.Log("Application.Quit()");
				Application.Quit();
			}
			if (clickTimes == 1)
			{
				UnityEngine.Debug.Log("Application.Quit()  back");
				GameQuit.Back();
			}
		}
	}

	private IEnumerator ResetMPressTimes(float sec)
	{
		yield return new WaitForSeconds(sec);
		clickTimes = 0;
	}
}
