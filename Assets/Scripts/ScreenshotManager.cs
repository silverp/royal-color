using System;
using System.Collections;
using System.IO;
using UnityEngine;

public class ScreenshotManager : MonoBehaviour
{
	private enum ImageType
	{
		IMAGE,
		SCREENSHOT
	}

	private enum SaveStatus
	{
		NOTSAVED,
		SAVED,
		DENIED,
		TIMEOUT
	}

	private static ScreenshotManager instance;

	private static GameObject go;

	private static AndroidJavaClass obj;

	public static ScreenshotManager Instance
	{
		get
		{
			if (instance == null)
			{
				go = new GameObject();
				go.name = "ScreenshotManager";
				instance = go.AddComponent<ScreenshotManager>();
				if (Application.platform == RuntimePlatform.Android)
				{
					obj = new AndroidJavaClass("com.wawagame.app.tsds.MainActivity");
				}
			}
			return instance;
		}
	}

	public static event Action<Texture2D> OnScreenshotTaken;

	public static event Action<string> OnScreenshotSaved;

	public static event Action<string> OnImageSaved;

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public static void SaveScreenshot(string fileName, string albumName = "MyScreenshots", string fileType = "jpg", Rect screenArea = default(Rect))
	{
		UnityEngine.Debug.Log("Save screenshot to gallery " + fileName);
		if (screenArea == default(Rect))
		{
			screenArea = new Rect(0f, 0f, Screen.width, Screen.height);
		}
		Instance.StartCoroutine(Instance.GrabScreenshot(fileName, albumName, fileType, screenArea));
	}

	private IEnumerator GrabScreenshot(string fileName, string albumName, string fileType, Rect screenArea)
	{
		yield return new WaitForEndOfFrame();
		Texture2D texture = new Texture2D((int)screenArea.width, (int)screenArea.height, TextureFormat.RGB24, mipChain: false);
		texture.ReadPixels(screenArea, 0, 0);
		texture.Apply();
		byte[] bytes;
		string fileExt;
		if (fileType == "png")
		{
			bytes = texture.EncodeToPNG();
			fileExt = ".png";
		}
		else
		{
			bytes = texture.EncodeToJPG();
			fileExt = ".jpg";
		}
		if (ScreenshotManager.OnScreenshotTaken != null)
		{
			ScreenshotManager.OnScreenshotTaken(texture);
		}
		else
		{
			UnityEngine.Object.Destroy(texture);
		}
		string date = DateTime.Now.ToString("hh-mm-ss_dd-MM-yy");
		string screenshotFilename = fileName + "_" + date + fileExt;
		string path = Application.persistentDataPath + "/" + screenshotFilename;
		if (Application.platform == RuntimePlatform.Android)
		{
			string path2 = Path.Combine(albumName, screenshotFilename);
			path = Path.Combine(Application.persistentDataPath, path2);
			string directoryName = Path.GetDirectoryName(path);
			Directory.CreateDirectory(directoryName);
		}
		Instance.StartCoroutine(Instance.Save(bytes, fileName, path, ImageType.SCREENSHOT, fileExt));
	}

	public static void SaveImage(Texture2D texture, string fileName, string albumName = "MyImages", string fileType = "jpg")
	{
		Instance.Awake();
		byte[] bytes;
		string text;
		if (fileType == "png")
		{
			bytes = texture.EncodeToPNG();
			text = ".png";
		}
		else
		{
			bytes = texture.EncodeToJPG();
			text = ".jpg";
		}
		string path = Application.persistentDataPath + "/" + fileName + text;
		if (Application.platform == RuntimePlatform.Android)
		{
			path = obj.CallStatic<string>("getGalleryPath", new object[0]);
			path = path + "/" + fileName + text;
		}
		Instance.StartCoroutine(Instance.Save(bytes, fileName, path, ImageType.IMAGE, text));
	}

	private IEnumerator Save(byte[] bytes, string fileName, string path, ImageType imageType, string fileExt)
	{
		int count = 0;
		SaveStatus saved = SaveStatus.NOTSAVED;
		string mimeType = string.Empty;
		if (fileExt == ".jpg")
		{
			mimeType = "image/jpeg";
		}
		else if (fileExt == ".png")
		{
			mimeType = "image/png";
		}
		if (Application.platform == RuntimePlatform.Android)
		{
			File.WriteAllBytes(path, bytes);
			while (saved == SaveStatus.NOTSAVED)
			{
				count++;
				saved = (SaveStatus)((count <= 30) ? obj.CallStatic<int>("addImageToGallery", new object[2]
				{
					path,
					mimeType
				}) : 3);
				yield return Instance.StartCoroutine(Instance.Wait(0.5f));
			}
		}
		switch (saved)
		{
		case SaveStatus.DENIED:
			path = "DENIED";
			break;
		case SaveStatus.TIMEOUT:
			path = "TIMEOUT";
			break;
		}
		switch (imageType)
		{
		case ImageType.IMAGE:
			if (ScreenshotManager.OnImageSaved != null)
			{
				ScreenshotManager.OnImageSaved(path);
			}
			break;
		case ImageType.SCREENSHOT:
			if (ScreenshotManager.OnScreenshotSaved != null)
			{
				ScreenshotManager.OnScreenshotSaved(path);
			}
			break;
		}
	}

	private IEnumerator Wait(float delay)
	{
		float pauseTarget = Time.realtimeSinceStartup + delay;
		while (Time.realtimeSinceStartup < pauseTarget)
		{
			yield return null;
		}
	}
}
