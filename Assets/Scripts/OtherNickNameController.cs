using UnityEngine;
using UnityEngine.UI;

public class OtherNickNameController : MonoBehaviour
{
	private void Start()
	{
	}

	private void Update()
	{
	}

	private void OnEnable()
	{
		UnityEngine.Debug.Log("change head tex");
		if (ApplicationModel.otherUserInfo != null && ApplicationModel.otherUserInfo.Nickname != null)
		{
			string nickname = ApplicationModel.otherUserInfo.Nickname;
			TextEmojiConvert.Instance.ConvertEmoji(base.transform, nickname, isBig: true);
		}
		else
		{
			string nickname = (!(ApplicationModel.LANGUAGE == "zh-en")) ? "No user info" : "没有用户信息";
			GetComponent<Text>().text = nickname;
		}
	}
}
