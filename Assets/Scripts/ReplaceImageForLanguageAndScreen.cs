using UnityEngine;
using UnityEngine.UI;

public class ReplaceImageForLanguageAndScreen : MonoBehaviour
{
	[SerializeField]
	private string imageName;

	private void Start()
	{
		string lANGUAGE = ApplicationModel.LANGUAGE;
		string path = (lANGUAGE != "en") ? ((Screen.width <= 1080) ? ("Icon_language/" + imageName + "_" + lANGUAGE) : ("Icon_language/" + imageName + "_" + lANGUAGE + "_big")) : ((Screen.width <= 1080) ? ("Icon_language/" + imageName) : ("Icon_language/" + imageName + "_big"));
		base.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>(path);
	}

	private void Update()
	{
	}
}
