using frame8.Logic.Misc.Visual.UI.ScrollRectItemsAdapter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CateListScrollRectAdapter : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	[Serializable]
	public class MyWorkParams : BaseParams
	{
		public RectTransform itemPrefab;
	}

	public class CatePageModel
	{
		public int num;

		public CatePageModel(int i)
		{
			num = i;
		}
	}

	public class MyWorkItemsViewHolder : BaseItemViewsHolder
	{
		public override void CollectViews()
		{
			base.CollectViews();
		}
	}

	public sealed class MyWorkScrollRectItemsAdapter : ScrollRectItemsAdapter8<MyWorkParams, MyWorkItemsViewHolder>
	{
		private bool _RandomizeSizes;

		private float _PrefabSize;

		private float[] _ItemsSizessToUse;

		private List<CatePageModel> _Data;

		private MyWorkItemsViewHolder myWorkItemsViewHolder;

		public MyWorkScrollRectItemsAdapter(List<CatePageModel> data, MyWorkParams parms)
		{
			_Data = data;
			if (parms.scrollRect.horizontal)
			{
				_PrefabSize = parms.itemPrefab.rect.width;
			}
			else
			{
				_PrefabSize = parms.itemPrefab.rect.height;
			}
			InitSizes();
			Init(parms);
		}

		private void InitSizes()
		{
			int count = _Data.Count;
			if (_ItemsSizessToUse == null || count != _ItemsSizessToUse.Length)
			{
				_ItemsSizessToUse = new float[count];
			}
			for (int i = 0; i < count; i++)
			{
				_ItemsSizessToUse[i] = _PrefabSize;
			}
		}

		public override void ChangeItemCountTo(int itemsCount)
		{
			InitSizes();
			base.ChangeItemCountTo(itemsCount);
		}

		protected override float GetItemHeight(int index)
		{
			if (index >= _ItemsSizessToUse.Length)
			{
				return 0f;
			}
			return _ItemsSizessToUse[index];
		}

		protected override float GetItemWidth(int index)
		{
			return _ItemsSizessToUse[index];
		}

		protected override MyWorkItemsViewHolder CreateViewsHolder(int itemIndex)
		{
			MyWorkItemsViewHolder myWorkItemsViewHolder = new MyWorkItemsViewHolder();
			myWorkItemsViewHolder.Init(_Params.itemPrefab, itemIndex);
			return myWorkItemsViewHolder;
		}

		public void SetNum(int i)
		{
			DragEventProxy.Instance.SendChangeCateItemMsg();
		}

		protected override void UpdateViewsHolder(MyWorkItemsViewHolder newOrRecycled)
		{
			myWorkItemsViewHolder = newOrRecycled;
			CatePageModel catePageModel = _Data[newOrRecycled.itemIndex];
			SetNum(catePageModel.num);
		}
	}

	[SerializeField]
	private MyWorkParams _ScrollRectAdapterParams;

	private List<CatePageModel> _Data = new List<CatePageModel>();

	private MyWorkScrollRectItemsAdapter _ScrollRectItemsAdapter;

	[SerializeField]
	private GameObject netErrorpanel;

	private Vector2 startPos;

	protected int currentPage;

	public BaseParams Params => _ScrollRectAdapterParams;

	public MyWorkScrollRectItemsAdapter Adapter => _ScrollRectItemsAdapter;

	public List<CatePageModel> Data => _Data;

	private void Awake()
	{
		_ScrollRectItemsAdapter = new MyWorkScrollRectItemsAdapter(_Data, _ScrollRectAdapterParams);
	}

	private void AssginEvents()
	{
		DragEventProxy.OnClickPage += smoothScrollToPage;
		DragEventProxy.OnRefreshCatelist += RefreshCateList;
	}

	private void DiscardEvents()
	{
		DragEventProxy.OnClickPage -= smoothScrollToPage;
		DragEventProxy.OnRefreshCatelist -= RefreshCateList;
	}

	private void RefreshCateList()
	{
		UnityEngine.Debug.Log("RefreshCateList");
		NetWorkRequest.DownloadCateListInfo(RefreshCateListFinish);
	}

	private void RefreshCateListFinish()
	{
		initContent();
		DragEventProxy.Instance.SendRefreshCatelistBarMsg();
	}

	private void smoothScrollToPage(int index)
	{
		currentPage = index;
		_ScrollRectItemsAdapter.SmoothScrollTo(index, 0.15f);
	}

	private IEnumerator Start()
	{
		yield return null;
		initContent();
	}

	private void initContent()
	{
		int count = ApplicationModel.CateResourceList.Count;
		if (count > 2)
		{
			netErrorpanel.SetActive(value: false);
			ApplicationModel.currentCategroy = ApplicationModel.CateResourceList[0];
			_Data.Clear();
			for (int i = 0; i < count; i++)
			{
				_Data.Add(new CatePageModel(i));
			}
			_Data.Capacity = _Data.Count;
			_ScrollRectItemsAdapter.ChangeItemCountTo(_Data.Capacity);
		}
		else
		{
			netErrorpanel.SetActive(value: true);
		}
	}

	private void OnEnable()
	{
		AssginEvents();
	}

	private void OnDisable()
	{
		DiscardEvents();
	}

	private void OnDestroy()
	{
		if (_ScrollRectItemsAdapter != null)
		{
			_ScrollRectItemsAdapter.Dispose();
		}
	}

	private bool isExistsImageid(string imageId, List<string> list)
	{
		foreach (string item in list)
		{
			if (item == imageId)
			{
				return true;
			}
		}
		return false;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		startPos = _ScrollRectAdapterParams.content.anchoredPosition;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		Vector2 vector = _ScrollRectAdapterParams.content.anchoredPosition - startPos;
		int num = (vector.x < 0f) ? 1 : (-1);
		if (Mathf.Abs(vector.x) < 10f)
		{
			_ScrollRectItemsAdapter.SmoothScrollTo(currentPage, 0.15f);
			return;
		}
		currentPage += num;
		currentPage = Mathf.Clamp(currentPage, 0, _Data.Count - 1);
		_ScrollRectItemsAdapter.SmoothScrollTo(currentPage, 0.15f);
		DragEventProxy.Instance.SendClickCateListBarMsg(currentPage);
	}
}
