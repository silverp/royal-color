using Mgl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class ShareController : MonoBehaviour
{
	[SerializeField]
	private GameObject loginPanel;

	[SerializeField]
	private GameObject SaveNoteText;

	private I18n i18n = I18n.Instance;

	[SerializeField]
	private GameObject BeanBox;

	[SerializeField]
	private GameObject PublishPanel;

	private Texture2D image;

	private Texture2D shareImage;

	private string savedPath = "default";

	private float infoBlurTime = 2f;

	private GUIStyle guiStyle = new GUIStyle();

	private float pdfScale = 1f;

	public Material material;

	private Texture2D mainTex;

	private BaseEditedImg curImg;

	public bool PublishLock;

	private IEnumerator changeIsAcceptableClick()
	{
		yield return new WaitForSeconds(0.3f);
		ClickEventManagement.isAcceptClickEvent = true;
		yield return null;
	}

	private IEnumerator showSavedInfo()
	{
		yield return new WaitForSeconds(infoBlurTime);
		SaveNoteText.SetActive(value: false);
	}

	private void OnDisable()
	{
		loginPanel.SetActive(value: false);
		ScreenshotManager.OnImageSaved -= ImageSaved;
		SaveNoteText.SetActive(value: false);
		if (image != null)
		{
			UnityEngine.Object.DestroyImmediate(image);
		}
		if (shareImage != null)
		{
			UnityEngine.Object.DestroyImmediate(shareImage);
		}
	}

	private IEnumerator WriteImageEvent()
	{
		yield return startWriteImage();
		yield return new WaitForSeconds(0.5f);
		SaveNoteText.SetActive(value: true);
		StartCoroutine(showSavedInfo());
	}

	private IEnumerator startWriteImage()
	{
		shareImage = mainTex;
		string dateTime = DateTime.Now.ToString("dd-MM-yy_hhmmss");
		string fileName = "small_paint_" + dateTime;
		Color32[] pixels = shareImage.GetPixels32();
		Color blackTransparent = Color.black;
		Color overwriteColor = Color.white;
		blackTransparent.a = 0f;
		for (int i = 0; i < pixels.Length; i++)
		{
			if (pixels[i].a < 80 && pixels[i].r + pixels[i].g + pixels[i].b == 0)
			{
				pixels[i] = overwriteColor;
			}
		}
		shareImage.SetPixels32(pixels);
		shareImage.Apply();
		ScreenshotManager.SaveImage(shareImage, fileName, "小涂鸦");
		yield return null;
	}

	public void Publish()
	{
		if (!PublishLock)
		{
			UnityEngine.Debug.Log("sharecontroller Pulish...");
			if (ApplicationModel.userInfo == null)
			{
				showLoginPanel();
				ApplicationModel.ReturnPageName = "ShareCanvas";
			}
			else
			{
				FileUploadEventProxy.Instance.SendPublishMyworkEventMsg((EditedImg)curImg, PublishEventCallback);
				PublishPanel.transform.GetChild(1).GetChild(1).GetComponent<Text>()
					.text = PublicToolController.GetTextByLanguage("Releasing...");
					PublishLock = true;
				}
			}
		}

		public void PublishEventCallback(Errcode err)
		{
			if (err.errorCode == Errcode.OK)
			{
				BeanAction.request(BeanAction.PUBLISH, BeanActionCallback);
				publishBtnControl(isRelease: true);
			}
			else if (err.errorCode == Errcode.HAS_PUBLISHED)
			{
				UnityEngine.Debug.Log("work has publish!!！");
			}
			else
			{
				UnityEngine.Debug.Log("PublishEventCallback err");
			}
		}

		private IEnumerator ADDThisImgToInspiration(string workId)
		{
			yield return new WaitForSeconds(1f);
			UnityEngine.Debug.Log("ADDThisImgToInspiration....");
			PublicImgaeApi.GetSingleInspirationByWorkId(workId, GetSingleInspirationCallback);
		}

		private void GetSingleInspirationCallback(Errcode err, ColoredImage image)
		{
			if (err.errorCode == Errcode.OK)
			{
				if (ApplicationModel.publishImglist == null)
				{
					ApplicationModel.publishImglist = new List<ColoredImage>();
				}
				ClearSameImg(image.img.imageId);
				ApplicationModel.publishImglist.Insert(0, image);
				UnityEngine.Debug.Log("start send msg!!!!");
				DragEventProxy.Instance.SendOnRefreshAfterPublishMsg();
			}
		}

		private void ClearSameImg(string imageId)
		{
			int count = ApplicationModel.publishImglist.Count;
			int num = -1;
			for (int i = 0; i < count; i++)
			{
				if (ApplicationModel.publishImglist[i].img.imageId == imageId)
				{
					num = i;
				}
			}
			if (num >= 0)
			{
				ApplicationModel.publishImglist.RemoveAt(num);
			}
		}

		private void BeanActionCallback(Errcode err, int amount)
		{
			UnityEngine.Debug.Log("amount is:" + amount);
			ApplicationModel.userInfo.BeanCnt += amount;
			BeanBox.transform.GetChild(1).GetChild(1).GetComponent<Text>()
				.text = PublicToolController.GetTextByLanguage("release successful, under review");
				BeanBox.transform.GetChild(1).GetChild(4).GetComponent<Text>()
					.text = "+" + amount;
					BeanBox.transform.Find("Panel").GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
					BeanBox.SetActive(value: true);
					LeanTween.scale(BeanBox.transform.Find("Panel").GetComponent<RectTransform>(), new Vector3(1f, 1f, 1f), 0.25f).setOnComplete((Action)delegate
					{
						UnityEngine.Debug.Log("complete scale!!!");
					});
				}

				public void SaveImageToDiskAlum()
				{
					if (ApplicationModel.userInfo == null)
					{
						showLoginPanel();
						ApplicationModel.ReturnPageName = "ShareCanvas";
					}
					else
					{
						TotalGA.Event("save_work");
						StartCoroutine(WriteImageEvent());
					}
				}

				private void showLoginPanel()
				{
					Transform child = loginPanel.transform.GetChild(0);
					loginPanel.SetActive(value: true);
					child.GetComponent<RectTransform>().localScale = new Vector3(0f, 0f, 0f);
					LeanTween.scale(child.GetComponent<RectTransform>(), new Vector3(1f, 1f, 1f), 0.25f).setOnComplete((Action)delegate
					{
						UnityEngine.Debug.Log("complete scale!!!");
					});
				}

				public void CancelLogin()
				{
					loginPanel.SetActive(value: false);
				}

				private void ImageSaved(string path)
				{
					SaveNoteText.SetActive(value: true);
					savedPath = path;
				}

				public void saveTexture()
				{
					RenderTexture renderTexture = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
					Graphics.Blit(GetComponent<RawImage>().mainTexture, renderTexture, material);
					try
					{
						PublicToolController.FileCreatorBytes((GetComponent<RawImage>().mainTexture as Texture2D).EncodeToPNG(), "RawImagemainTexture", isStoreChange: true);
						PublicToolController.FileCreatorBytes((GetComponent<RawImage>().material.GetTexture("_Mask") as Texture2D).EncodeToPNG(), "Mask", isStoreChange: true);
					}
					catch (IOException message)
					{
						MonoBehaviour.print(message);
					}
					RenderTexture active = RenderTexture.active;
					Texture2D texture2D = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, mipChain: false);
					RenderTexture.active = renderTexture;
					texture2D.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
					texture2D.Apply();
					RenderTexture.active = active;
					try
					{
						PublicToolController.FileCreatorBytes(texture2D.EncodeToPNG(), "test", isStoreChange: true);
					}
					catch (IOException message2)
					{
						MonoBehaviour.print(message2);
					}
				}

				private void DestoryChild(Transform transform)
				{
					IEnumerator enumerator = transform.GetEnumerator();
					try
					{
						while (enumerator.MoveNext())
						{
							Transform transform2 = (Transform)enumerator.Current;
							UnityEngine.Object.Destroy(transform2.gameObject);
						}
					}
					finally
					{
						IDisposable disposable;
						if ((disposable = (enumerator as IDisposable)) != null)
						{
							disposable.Dispose();
						}
					}
				}

				private void publishBtnControl(bool isRelease)
				{
					if (isRelease)
					{
						PublishPanel.transform.GetChild(0).gameObject.SetActive(value: true);
						PublishPanel.transform.GetChild(1).gameObject.SetActive(value: false);
					}
					else
					{
						PublishPanel.transform.GetChild(0).gameObject.SetActive(value: false);
						PublishPanel.transform.GetChild(1).gameObject.SetActive(value: true);
					}
				}

				private void JudgeWorkIsRelease()
				{
					PublicControllerApi.IsUploadInspiration(curImg.workId, publishBtnControl);
				}

				private void InitPublishPanel()
				{
					UnityEngine.Debug.Log("InitPublishPanel....");
					if (ApplicationModel.currentEditedImg is PixleEditedImg)
					{
						UnityEngine.Debug.Log("ApplicationModel.currentEditedImg is PixleEditedImg");
						PublishPanel.SetActive(value: false);
						return;
					}
					PublishPanel.SetActive(value: true);
					PublishPanel.transform.GetChild(0).gameObject.SetActive(value: false);
					PublishPanel.transform.GetChild(1).gameObject.SetActive(value: true);
					PublishPanel.transform.GetChild(1).GetChild(1).GetComponent<Text>()
						.text = PublicToolController.GetTextByLanguage("Release to square");
					}

					private void OnEnable()
					{
						curImg = ApplicationModel.currentEditedImg;
						ScreenshotManager.OnImageSaved += ImageSaved;
						StartCoroutine(changeIsAcceptableClick());
						InitMaintex();
						InitPublishPanel();
						PublishLock = false;
					}

					private void InitMaintex()
					{
						mainTex = ApplicationModel.GetCurrentText2d();
						GetComponent<RawImage>().texture = mainTex;
						loginPanel.transform.Find("LoginBox").Find("Image").GetComponent<RawImage>()
							.texture = mainTex;
						}
					}
