using UnityEngine;
using UnityEngine.EventSystems;

public class DragEventProxy : MonoBehaviour
{
	public delegate void DragX(PointerEventData eventData);

	public delegate void DragEnd(PointerEventData eventData);

	public delegate void DragBegin(PointerEventData eventData);

	public delegate void ClickPage(int index);

	public delegate void ClickCateListBar(int index);

	public delegate void CompletePageInit();

	public delegate void ClickColorListBar(UnityEngine.Object sender, int index);

	public delegate void ChangeColorlistPage(UnityEngine.Object sender, int index);

	public delegate void StartScrollRectInit();

	public delegate void ShwoColorBar();

	public delegate void HideColorBar();

	public delegate void CloseWorkShopCanvas();

	public delegate void OpenWorkShopCanvas();

	public delegate void ChangeText();

	public delegate void EnterImgListCanvas();

	public delegate void EnterWorkShopCanvas();

	public delegate void EnterShareCanvas();

	public delegate void EnterMyworkCanvas();

	public delegate void EnterInspirationCanvas();

	public delegate void EnterZoomPanel();

	public delegate void ChangeColorModel();

	public delegate void AddColorToRecentList();

	public delegate void OffLine(string canvasName, bool isOffline);

	public delegate void ChangeIndexImage(string imageId);

	public delegate void ChangeCateItem();

	public delegate void RefreshCatelistBar();

	public delegate void RequestNextPage();

	public delegate void UnclockOutline(ResourceImg image, PixelArt pixelArtImage, ImageType type);

	public delegate void RemoveFollow(string uid);

	public delegate void AddFollow(string uid);

	public delegate void AddFollowCnt(int x);

	public delegate void Repaint(WorkInfo workinfo);

	public delegate void RepaintAfterPurchase(ResourceImg resourceImg, PixelArt pixelArt, ImageType type);

	public delegate void ShareCallback(Errcode err);

	public delegate void GotoZoomImage(Texture2D tex);

	public delegate void RefreshBeanCnt(int amount);

	public delegate void RefreshAlbumList();

	public delegate void RefreshAfterLogin();

	public delegate void RefreshAfterPurchase();

	public delegate void RefreshAfterPublish();

	public delegate void RefreshCatelist();

	public static DragEventProxy Instance;

	public static HideColorBar OnHideColorBar;

	public static event DragX OnDragX;

	public static event DragEnd OnDragEnd;

	public static event DragEnd OnDragBegin;

	public static event ClickPage OnClickPage;

	public static event ClickCateListBar OnClickCateListBar;

	public static event CompletePageInit OnCompletePageInit;

	public static event ClickColorListBar OnClickColorListBar;

	public static event ChangeColorlistPage OnChangeColorlistPage;

	public static event StartScrollRectInit OnStartScrollRectInit;

	public static event ShwoColorBar OnShowColorBar;

	public static event CloseWorkShopCanvas OnCloseWorkShopCanvas;

	public static event OpenWorkShopCanvas OnOpenWorkShopCanvas;

	public static event ChangeText OnChangeText;

	private static event EnterImgListCanvas OnEnterImgListCanvas;

	public static event EnterWorkShopCanvas OnEnterWorkShopCanvas;

	public static event EnterShareCanvas OnEnterShareCanvas;

	public static event EnterMyworkCanvas OnEnterMyworkCanvas;

	public static event EnterInspirationCanvas OnEnterInspirationCanvas;

	public static event EnterZoomPanel OnEnterZoomPanel;

	public static event ChangeColorModel OnChangeColorModel;

	public static event AddColorToRecentList OnAddColorToRecentList;

	public static event OffLine OnOffLine;

	public static event ChangeIndexImage OnChangeIndexImage;

	public static event ChangeCateItem OnChangeCateItem;

	public static event RefreshCatelistBar OnRefreshCatelistBar;

	public static event RequestNextPage OnRequestNextPage;

	public static event UnclockOutline OnUnclockOutline;

	public static event RemoveFollow OnRemoveFollow;

	public static event AddFollow OnAddFollow;

	public static event AddFollowCnt OnAddFollowCnt;

	public static event Repaint OnRepaint;

	public static event RepaintAfterPurchase OnRepaintAfterPurchase;

	public static event ShareCallback OnShareCallback;

	public static event GotoZoomImage OnGotoZoomImage;

	public static event RefreshBeanCnt OnRefreshBeanCnt;

	public static event RefreshAlbumList OnRefreshAlbumList;

	public static event RefreshAfterLogin OnRefreshAfterLogin;

	public static event RefreshAfterPurchase OnRefreshAfterPurchase;

	public static event RefreshAfterPublish OnRefreshAfterPublish;

	public static event RefreshCatelist OnRefreshCatelist;

	public void SendOnRefreshCatelist()
	{
		if (DragEventProxy.OnRefreshCatelist != null)
		{
			DragEventProxy.OnRefreshCatelist();
		}
	}

	public void SendRefreshAfterLoginMsg()
	{
		if (DragEventProxy.OnRefreshAfterLogin != null)
		{
			DragEventProxy.OnRefreshAfterLogin();
		}
	}

	public void SendRefreshAfterPurchaseMsg()
	{
		if (DragEventProxy.OnRefreshAfterPurchase != null)
		{
			DragEventProxy.OnRefreshAfterPurchase();
		}
	}

	public void SendOnRefreshAfterPublishMsg()
	{
		if (DragEventProxy.OnRefreshAfterPublish != null)
		{
			DragEventProxy.OnRefreshAfterPublish();
		}
	}

	public void SendRefreshAlbumList()
	{
		if (DragEventProxy.OnRefreshAlbumList != null)
		{
			DragEventProxy.OnRefreshAlbumList();
		}
	}

	public void SendRefreshBeanCnt(int amount)
	{
		if (DragEventProxy.OnRefreshBeanCnt != null)
		{
			DragEventProxy.OnRefreshBeanCnt(amount);
		}
	}

	public void SendGotoZoomImageMsg(Texture2D tex)
	{
		if (DragEventProxy.OnGotoZoomImage != null)
		{
			DragEventProxy.OnGotoZoomImage(tex);
		}
	}

	public void SendShareCallbackMsg(Errcode err)
	{
		if (DragEventProxy.OnShareCallback != null)
		{
			DragEventProxy.OnShareCallback(err);
		}
	}

	public void SendRepaintAfterPurchase(ResourceImg resourceImg, PixelArt pixelArt, ImageType type)
	{
		if (DragEventProxy.OnRepaintAfterPurchase != null)
		{
			DragEventProxy.OnRepaintAfterPurchase(resourceImg, pixelArt, type);
		}
	}

	public void SendRepaintMsg(WorkInfo workInfo)
	{
		if (DragEventProxy.OnRepaint != null)
		{
			DragEventProxy.OnRepaint(workInfo);
		}
	}

	public void SendAddFollowCntMsg(int x)
	{
		if (DragEventProxy.OnAddFollowCnt != null)
		{
			DragEventProxy.OnAddFollowCnt(x);
		}
	}

	public void SendRemoveFollowMsg(string uid)
	{
		if (DragEventProxy.OnRemoveFollow != null)
		{
			DragEventProxy.OnRemoveFollow(uid);
		}
	}

	public void SendAddFollowMsg(string uid)
	{
		if (DragEventProxy.OnAddFollow != null)
		{
			DragEventProxy.OnAddFollow(uid);
		}
	}

	public void SendUnclockOutlineMsg(ResourceImg image, PixelArt pixelArtImage, ImageType type)
	{
		if (DragEventProxy.OnUnclockOutline != null)
		{
			DragEventProxy.OnUnclockOutline(image, pixelArtImage, type);
		}
	}

	public void SendRequestNextPageMsg()
	{
		if (DragEventProxy.OnRequestNextPage != null)
		{
			DragEventProxy.OnRequestNextPage();
		}
	}

	public void SendRefreshCatelistBarMsg()
	{
		if (DragEventProxy.OnRefreshCatelistBar != null)
		{
			DragEventProxy.OnRefreshCatelistBar();
		}
	}

	public void SendChangeCateItemMsg()
	{
		if (DragEventProxy.OnChangeCateItem != null)
		{
			DragEventProxy.OnChangeCateItem();
		}
	}

	public void SendOffLineMsg(string canvasName, bool isOffline = false)
	{
		if (DragEventProxy.OnOffLine != null)
		{
			DragEventProxy.OnOffLine(canvasName, isOffline);
		}
	}

	public void SendChangeIndexImageMsg(string imageId)
	{
		if (DragEventProxy.OnChangeIndexImage != null)
		{
			DragEventProxy.OnChangeIndexImage(imageId);
		}
	}

	public void SendHideColorBarMsg()
	{
		if (OnHideColorBar != null)
		{
			OnHideColorBar();
		}
	}

	public void SendAddColorToRecentList()
	{
		if (DragEventProxy.OnAddColorToRecentList != null)
		{
			DragEventProxy.OnAddColorToRecentList();
		}
	}

	public void SendChangeColorModel()
	{
		if (DragEventProxy.OnChangeColorModel != null)
		{
			DragEventProxy.OnChangeColorModel();
		}
	}

	public void SendEnterZoomPanelMsg()
	{
		if (DragEventProxy.OnEnterZoomPanel != null)
		{
			DragEventProxy.OnEnterZoomPanel();
		}
	}

	public void SendEnterInspirationCanvas()
	{
		if (DragEventProxy.OnEnterInspirationCanvas != null)
		{
			DragEventProxy.OnEnterInspirationCanvas();
		}
	}

	public void SendEnterMyworkCanvas()
	{
		if (DragEventProxy.OnEnterMyworkCanvas != null)
		{
			DragEventProxy.OnEnterMyworkCanvas();
		}
	}

	public void SendEnterShareCanvas()
	{
		if (DragEventProxy.OnEnterShareCanvas != null)
		{
			DragEventProxy.OnEnterShareCanvas();
		}
	}

	public void SendEnterImgListCanvasMsg()
	{
		if (DragEventProxy.OnEnterImgListCanvas != null)
		{
			DragEventProxy.OnEnterImgListCanvas();
		}
	}

	public void SendEnterWorkShopCanvasMsg()
	{
		if (DragEventProxy.OnEnterWorkShopCanvas != null)
		{
			DragEventProxy.OnEnterWorkShopCanvas();
		}
	}

	public void SendChangeTextMsg()
	{
		if (DragEventProxy.OnChangeText != null)
		{
			DragEventProxy.OnChangeText();
		}
	}

	public void SendOpenWorkShopCanvas()
	{
		if (DragEventProxy.OnOpenWorkShopCanvas != null)
		{
			DragEventProxy.OnOpenWorkShopCanvas();
		}
	}

	public void SendCloseWorkShopCanvasMsg()
	{
		if (DragEventProxy.OnCloseWorkShopCanvas != null)
		{
			DragEventProxy.OnCloseWorkShopCanvas();
		}
	}

	public void SendShwoColorBarMsg()
	{
		if (DragEventProxy.OnShowColorBar != null)
		{
			DragEventProxy.OnShowColorBar();
		}
	}

	public void SendScrollRectInitMsg()
	{
		if (DragEventProxy.OnStartScrollRectInit != null)
		{
			DragEventProxy.OnStartScrollRectInit();
		}
	}

	public void SendChangeColorlistPage(UnityEngine.Object sender, int index)
	{
		if (DragEventProxy.OnChangeColorlistPage != null)
		{
			DragEventProxy.OnChangeColorlistPage(sender, index);
		}
	}

	public void SendClickColorListBar(int index)
	{
		if (DragEventProxy.OnClickCateListBar != null)
		{
			DragEventProxy.OnClickCateListBar(index);
		}
	}

	public void SendCompletePageInitMsg()
	{
		if (DragEventProxy.OnCompletePageInit != null)
		{
			DragEventProxy.OnCompletePageInit();
		}
	}

	public void SendClickCateListBarMsg(int index)
	{
		if (DragEventProxy.OnClickCateListBar != null)
		{
			DragEventProxy.OnClickCateListBar(index);
		}
	}

	public void SendClickPageMsg(int index)
	{
		if (DragEventProxy.OnClickPage != null)
		{
			DragEventProxy.OnClickPage(index);
		}
	}

	public void SendDragXMsg(PointerEventData eventData)
	{
		if (DragEventProxy.OnDragX != null)
		{
			DragEventProxy.OnDragX(eventData);
		}
	}

	public void SendDragEnd(PointerEventData eventData)
	{
		if (DragEventProxy.OnDragEnd != null)
		{
			DragEventProxy.OnDragEnd(eventData);
		}
	}

	public void SendDragBegin(PointerEventData eventData)
	{
		if (DragEventProxy.OnDragBegin != null)
		{
			DragEventProxy.OnDragBegin(eventData);
		}
	}

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}
}
