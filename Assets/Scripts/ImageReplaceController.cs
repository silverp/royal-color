using UnityEngine;
using UnityEngine.UI;

public class ImageReplaceController : MonoBehaviour
{
	[SerializeField]
	private string imageName;

	private string imagePath;

	private string language;

	private void Start()
	{
		language = LanguageHelper.GetLocaleCodeSystemLanguage();
		if (language == "zh-cn")
		{
			imagePath = "Images/Icon_language/" + imageName + "_" + language;
		}
		else
		{
			imagePath = "Images/Icon_language/" + imageName;
		}
		base.transform.GetComponent<Image>().sprite = Resources.Load<Sprite>(imagePath);
	}

	private void Update()
	{
	}
}
