﻿Shader "Unlit/RadialGradient"
{
    Properties
    {
        [PerRendererData]
        _MainTex ("Texture", 2D) = "white" {}  
        _RampTex ("Ramp Texture", 2D) = "" { }
        _RampTexCoordX ("Ramp x Coordinate", float) = 0
        _RampTexCoordY ("Ramp Y Coordinate", float) = 0
        _RadiusMultiplier ("Radius", Range(0, 5)) = 0.67
        _GradientPivot ("Position", Vector) = (0.5, 0.5, 0, 0)
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" }

        Blend SrcAlpha OneMinusSrcAlpha 
        Pass // ind: 1, name:
        {  
            CGPROGRAM 
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc" 

            sampler2D _MainTex; 
            uniform sampler2D _RampTex;
            uniform int _RampTexCoordX;
            uniform int _RampTexCoordY;
            float _RadiusMultiplier;
            float4 _GradientPivot;
            
            struct appdata_t
            {
                float4 vertex : POSITION; 
                float4 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };  
            
            v2f vert(appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }
             
            float4 frag(v2f i) : SV_Target0
            {
                float2 pos;
                pos.x = float(_RampTexCoordX * 4) / 512.0;
                pos.y = float(_RampTexCoordY * 64) / 512.0;
                float4 colorFrom = tex2D(_RampTex, pos);
                float4 colorTo = tex2D(_RampTex, pos + float2(0, 63/512.0));

                fixed2 pixelPoint = fixed2(i.uv.x, i.uv.y);
                fixed4 m = tex2D(_MainTex, i.uv);
                float xDist = pow(pixelPoint.x - _GradientPivot.x, 2);
                float yDist = pow(pixelPoint.y - _GradientPivot.y, 2);
                float dist = sqrt(xDist + yDist) / _RadiusMultiplier;
                dist = clamp(dist, 0, 1); 
                fixed4 col = lerp(colorFrom, colorTo, dist); 
                col.w = m.a;
                return col;

                // fixed2 pixelPoint = fixed2(f.uv.x, f.uv.y);
                // fixed4 m = tex2D(_MainTex, f.uv);
                
                // float2 pos;
                // pos.x = (float((_RampTexCoordX * 4)) / 512.0);
                // pos.y = (float((_RampTexCoordY * 64)) / 512.0);
                
                // float2 center;
                // center.x = (6.0 * abs((f.uv.x - 0.5))) - _GradientPivot.x;
                // center.y = (3.0 * abs((f.uv.y - 0.5))) - _GradientPivot.y; 
                
                // float dist = sqrt((dot(center, center) / 2.0)) / _RadiusMultiplier;
                
                // float2 index; 
                // index.x = ((3.0 * dist) / 512.0); 
                // index.y = ((63.0 * dist) / 512.0);  
                
                // float4 color = tex2D(_RampTex, pos + index);
                // color.a = 0; 
                // return color;
            }
            ENDCG
        }// end phase
    }
    FallBack "Diffuse"
}
