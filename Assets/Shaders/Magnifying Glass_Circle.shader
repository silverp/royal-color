Shader "Magnifying Glass/Circle"
{
    Properties
    {
        [HideInInspector] _MainTex ("Base (RGB)", 2D) = "white" { }
        _View_Border ("View Border(RGBA)", 2D) = "white" { }
        _View_Center ("View Center(RGBA)", 2D) = "white" { }
        _View_Radial ("View _Radial(Vector)", Vector) = (0, 0, 0, 0)
        _DisplayCenterOffSet ("DisplayCenterOffSet", Vector) = (0, 0, 0, 0)
        _SimpleCenterRadial1 ("Center Radial 1", Vector) = (0, 0, 0, 0)
        _SimpleAmount1 ("Amount 1", float) = 0.5
        _SimpleCenterRadial2 ("Center Radial 2", Vector) = (0, 0, 0, 0)
        _SimpleAmount2 ("Amount 2", float) = 0.5
        _SimpleCenterRadial3 ("Center Radial 3", Vector) = (0, 0, 0, 0)
        _SimpleAmount3 ("Amount 3", float) = 0.5
        _SimpleCenterRadial4 ("Center Radial 4", Vector) = (0, 0, 0, 0)
        _SimpleAmount4 ("Amount 4", float) = 0.5
        _SimpleCenterRadial5 ("Center Radial 5", Vector) = (0, 0, 0, 0)
        _SimpleAmount5 ("Amount 5", float) = 0.5
        _SimpleCenterRadial6 ("Center Radial 6", Vector) = (0, 0, 0, 0)
        _SimpleAmount6 ("Amount 6", float) = 0.5
        _SimpleCenterRadial7 ("Center Radial 7", Vector) = (0, 0, 0, 0)
        _SimpleAmount7 ("Amount 7", float) = 0.5
        _SimpleCenterRadial8 ("Center Radial 8", Vector) = (0, 0, 0, 0)
        _SimpleAmount8 ("Amount 8", float) = 0.5
        _ComplicatedCenterRadial1 ("Complicated Center Radial 1", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount1 ("Complicated Amount 1", float) = 0.85
        _ComplicatedRadiusInner1 ("Complicated Radius Inner 1", float) = 0.2
        _ComplicatedRadiusOuter1 ("Complicated Radius Outer 1", float) = 0.27
        _ComplicatedCenterRadial2 ("Complicated Center Radial 2", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount2 ("Complicated Amount 2", float) = 0.85
        _ComplicatedRadiusInner2 ("Complicated Radius Inner 2", float) = 0.2
        _ComplicatedRadiusOuter2 ("Complicated Radius Outer 2", float) = 0.27
        _ComplicatedCenterRadial3 ("Complicated Center Radial 3", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount3 ("Complicated Amount 3", float) = 0.85
        _ComplicatedRadiusInner3 ("Complicated Radius Inner 3", float) = 0.2
        _ComplicatedRadiusOuter3 ("Complicated Radius Outer 3", float) = 0.27
        _ComplicatedCenterRadial4 ("Complicated Center Radial 4", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount4 ("Complicated Amount 4", float) = 0.85
        _ComplicatedRadiusInner4 ("Complicated Radius Inner 4", float) = 0.2
        _ComplicatedRadiusOuter4 ("Complicated Radius Outer 4", float) = 0.27
        _ComplicatedCenterRadial5 ("Complicated Center Radial 5", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount5 ("Complicated Amount 5", float) = 0.85
        _ComplicatedRadiusInner5 ("Complicated Radius Inner 5", float) = 0.2
        _ComplicatedRadiusOuter5 ("Complicated Radius Outer 5", float) = 0.27
        _ComplicatedCenterRadial6 ("Complicated Center Radial 6", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount6 ("Complicated Amount 6", float) = 0.85
        _ComplicatedRadiusInner6 ("Complicated Radius Inner 6", float) = 0.2
        _ComplicatedRadiusOuter6 ("Complicated Radius Outer 6", float) = 0.27
        _ComplicatedCenterRadial7 ("Complicated Center Radial 7", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount7 ("Complicated Amount 7", float) = 0.85
        _ComplicatedRadiusInner7 ("Complicated Radius Inner 7", float) = 0.2
        _ComplicatedRadiusOuter7 ("Complicated Radius Outer 7", float) = 0.27
        _ComplicatedCenterRadial8 ("Complicated Center Radial 8", Vector) = (0, 0, 0, 0)
        _ComplicatedAmount8 ("Complicated Amount 8", float) = 0.85
        _ComplicatedRadiusInner8 ("Complicated Radius Inner 8", float) = 0.2
        _ComplicatedRadiusOuter8 ("Complicated Radius Outer 8", float) = 0.27
    }
    SubShader
    {
        Tags {  }
        Pass // ind: 1, name:

        {
            Tags {  }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend One OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            uniform float4 _SimpleCenterRadial1;
            
            uniform float _SimpleAmount1;
            
            uniform float4 _DisplayCenterOffSet;
            
            uniform float4 _View_Radial;
            
            uniform sampler2D _View_Border;
            
            uniform sampler2D _View_Center;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1.w = 1.0;
                
                tmpvar_1.xyz = in_v.vertex.xyz;
                OUT_Data_Vert OUT;
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
                
                OUT.xlv_TEXCOORD0 = in_v.texcoord.xy;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 c_1;
                
                float2 uv_2;
                
                uv_2 = in_f.xlv_TEXCOORD0;
                
                float4 cr_3;
                
                cr_3 = (_SimpleCenterRadial1 + _DisplayCenterOffSet);
                
                int tmpvar_4;
                
                float tmpvar_5;
                
                float2 x_6;
                
                x_6 = ((uv_2 - cr_3.xy) / cr_3.zw);
                
                tmpvar_5 = sqrt(dot(x_6, x_6));
                
                if ((tmpvar_5 > 1.0))
                {
                    
                    tmpvar_4 = int(0);
                }
                else
                {
                    
                    tmpvar_4 = int(1);
                };
                
                if (tmpvar_4)
                {
                    
                    float4 border_7;
                    
                    float4 tmpvar_8;
                    
                    float2 uv_9;
                    
                    uv_9 = in_f.xlv_TEXCOORD0;
                    
                    float4 cr_10;
                    
                    cr_10 = (_SimpleCenterRadial1 - (_DisplayCenterOffSet * ((1.0 / (_SimpleAmount1)) - 1.0)));
                    
                    float2 tmpvar_11;
                    
                    tmpvar_11 = (cr_10.xy + ((1.0 - _SimpleAmount1) * (uv_9 - cr_10.xy)));
                    
                    float4 tmpvar_12;
                    
                    tmpvar_12 = tex2D(_MainTex, tmpvar_11);
                    
                    tmpvar_8 = tmpvar_12;
                    
                    c_1 = tmpvar_8;
                    
                    float2 tmpvar_13;
                    
                    tmpvar_13 = (0.5 + (((in_f.xlv_TEXCOORD0 - _DisplayCenterOffSet.xy) - _SimpleCenterRadial1.xy) / (2.0 * _SimpleCenterRadial1.zw)));
                    
                    float4 tmpvar_14;
                    
                    tmpvar_14 = tex2D(_View_Border, tmpvar_13);
                    
                    border_7 = tmpvar_14;
                    
                    float4 tmpvar_15;
                    
                    tmpvar_15.xy = float2(0.0, 0.0);
                    
                    tmpvar_15.zw = _View_Radial.zw;
                    
                    float2 uv_16;
                    
                    uv_16 = in_f.xlv_TEXCOORD0;
                    
                    float4 cr_17;
                    
                    cr_17 = ((_SimpleCenterRadial1 + _DisplayCenterOffSet) - tmpvar_15);
                    
                    int tmpvar_18;
                    
                    float tmpvar_19;
                    
                    float2 x_20;
                    
                    x_20 = ((uv_16 - cr_17.xy) / cr_17.zw);
                    
                    tmpvar_19 = sqrt(dot(x_20, x_20));
                    
                    if ((tmpvar_19 > 1.0))
                    {
                        
                        tmpvar_18 = int(0);
                    }
                    else
                    {
                        
                        tmpvar_18 = int(1);
                    };
                    
                    if (tmpvar_18)
                    {
                        
                        c_1 = lerp(tmpvar_8, border_7, border_7.wwww);
                    }
                    else
                    {
                        
                        float4 mainColor_21;
                        
                        float4 tmpvar_22;
                        
                        tmpvar_22 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                        
                        mainColor_21 = tmpvar_22;
                        
                        c_1 = lerp(mainColor_21, border_7, border_7.wwww);
                    };
                    
                    float4 tmpvar_23;
                    
                    tmpvar_23.xy = _SimpleCenterRadial1.xy;
                    
                    tmpvar_23.z = _View_Radial.x;
                    
                    tmpvar_23.w = _View_Radial.y;
                    
                    float2 uv_24;
                    
                    uv_24 = in_f.xlv_TEXCOORD0;
                    
                    float4 cr_25;
                    
                    cr_25 = (tmpvar_23 + _DisplayCenterOffSet);
                    
                    int tmpvar_26;
                    
                    float tmpvar_27;
                    
                    float2 x_28;
                    
                    x_28 = ((uv_24 - cr_25.xy) / cr_25.zw);
                    
                    tmpvar_27 = sqrt(dot(x_28, x_28));
                    
                    if ((tmpvar_27 > 1.0))
                    {
                        
                        tmpvar_26 = int(0);
                    }
                    else
                    {
                        
                        tmpvar_26 = int(1);
                    };
                    
                    if (tmpvar_26)
                    {
                        
                        float4 center_29;
                        
                        float2 tmpvar_30;
                        
                        tmpvar_30 = (0.5 + (((in_f.xlv_TEXCOORD0 - _DisplayCenterOffSet.xy) - _SimpleCenterRadial1.xy) / (2.0 * _View_Radial.xy)));
                        
                        float4 tmpvar_31;
                        
                        tmpvar_31 = tex2D(_View_Center, tmpvar_30);
                        
                        center_29 = tmpvar_31;
                        
                        c_1 = lerp(c_1, center_29, center_29.wwww);
                    };
                    
                    c_1.w = 1.0;
                }
                else
                {
                    
                    float4 tmpvar_32;
                    
                    tmpvar_32 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                    
                    c_1 = tmpvar_32;
                };
                OUT_Data_Frag out_f;
                out_f.color = c_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase
        Pass // ind: 2, name:

        {
            Tags {  }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend One OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            uniform float4 _SimpleCenterRadial1;
            
            uniform float _SimpleAmount1;
            
            uniform float4 _SimpleCenterRadial2;
            
            uniform float _SimpleAmount2;
            
            uniform float4 _SimpleCenterRadial3;
            
            uniform float _SimpleAmount3;
            
            uniform float4 _SimpleCenterRadial4;
            
            uniform float _SimpleAmount4;
            
            uniform float4 _SimpleCenterRadial5;
            
            uniform float _SimpleAmount5;
            
            uniform float4 _SimpleCenterRadial6;
            
            uniform float _SimpleAmount6;
            
            uniform float4 _SimpleCenterRadial7;
            
            uniform float _SimpleAmount7;
            
            uniform float4 _SimpleCenterRadial8;
            
            uniform float _SimpleAmount8;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1.w = 1.0;
                
                tmpvar_1.xyz = in_v.vertex.xyz;
                OUT_Data_Vert OUT;
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
                
                OUT.xlv_TEXCOORD0 = in_v.texcoord.xy;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 c_1;
                
                float2 uv_2;
                
                uv_2 = in_f.xlv_TEXCOORD0;
                
                int tmpvar_3;
                
                float tmpvar_4;
                
                float2 x_5;
                
                x_5 = ((uv_2 - _SimpleCenterRadial1.xy) / _SimpleCenterRadial1.zw);
                
                tmpvar_4 = sqrt(dot(x_5, x_5));
                
                if ((tmpvar_4 > 1.0))
                {
                    
                    tmpvar_3 = int(0);
                }
                else
                {
                    
                    tmpvar_3 = int(1);
                };
                
                if (tmpvar_3)
                {
                    
                    float4 tmpvar_6;
                    
                    float2 uv_7;
                    
                    uv_7 = in_f.xlv_TEXCOORD0;
                    
                    float2 tmpvar_8;
                    
                    tmpvar_8 = (_SimpleCenterRadial1.xy + ((1.0 - _SimpleAmount1) * (uv_7 - _SimpleCenterRadial1.xy)));
                    
                    float4 tmpvar_9;
                    
                    tmpvar_9 = tex2D(_MainTex, tmpvar_8);
                    
                    tmpvar_6 = tmpvar_9;
                    
                    c_1 = tmpvar_6;
                }
                else
                {
                    
                    float2 uv_10;
                    
                    uv_10 = in_f.xlv_TEXCOORD0;
                    
                    int tmpvar_11;
                    
                    float tmpvar_12;
                    
                    float2 x_13;
                    
                    x_13 = ((uv_10 - _SimpleCenterRadial2.xy) / _SimpleCenterRadial2.zw);
                    
                    tmpvar_12 = sqrt(dot(x_13, x_13));
                    
                    if ((tmpvar_12 > 1.0))
                    {
                        
                        tmpvar_11 = int(0);
                    }
                    else
                    {
                        
                        tmpvar_11 = int(1);
                    };
                    
                    if (tmpvar_11)
                    {
                        
                        float4 tmpvar_14;
                        
                        float2 uv_15;
                        
                        uv_15 = in_f.xlv_TEXCOORD0;
                        
                        float2 tmpvar_16;
                        
                        tmpvar_16 = (_SimpleCenterRadial2.xy + ((1.0 - _SimpleAmount2) * (uv_15 - _SimpleCenterRadial2.xy)));
                        
                        float4 tmpvar_17;
                        
                        tmpvar_17 = tex2D(_MainTex, tmpvar_16);
                        
                        tmpvar_14 = tmpvar_17;
                        
                        c_1 = tmpvar_14;
                    }
                    else
                    {
                        
                        float2 uv_18;
                        
                        uv_18 = in_f.xlv_TEXCOORD0;
                        
                        int tmpvar_19;
                        
                        float tmpvar_20;
                        
                        float2 x_21;
                        
                        x_21 = ((uv_18 - _SimpleCenterRadial3.xy) / _SimpleCenterRadial3.zw);
                        
                        tmpvar_20 = sqrt(dot(x_21, x_21));
                        
                        if ((tmpvar_20 > 1.0))
                        {
                            
                            tmpvar_19 = int(0);
                        }
                        else
                        {
                            
                            tmpvar_19 = int(1);
                        };
                        
                        if (tmpvar_19)
                        {
                            
                            float4 tmpvar_22;
                            
                            float2 uv_23;
                            
                            uv_23 = in_f.xlv_TEXCOORD0;
                            
                            float2 tmpvar_24;
                            
                            tmpvar_24 = (_SimpleCenterRadial3.xy + ((1.0 - _SimpleAmount3) * (uv_23 - _SimpleCenterRadial3.xy)));
                            
                            float4 tmpvar_25;
                            
                            tmpvar_25 = tex2D(_MainTex, tmpvar_24);
                            
                            tmpvar_22 = tmpvar_25;
                            
                            c_1 = tmpvar_22;
                        }
                        else
                        {
                            
                            float2 uv_26;
                            
                            uv_26 = in_f.xlv_TEXCOORD0;
                            
                            int tmpvar_27;
                            
                            float tmpvar_28;
                            
                            float2 x_29;
                            
                            x_29 = ((uv_26 - _SimpleCenterRadial4.xy) / _SimpleCenterRadial4.zw);
                            
                            tmpvar_28 = sqrt(dot(x_29, x_29));
                            
                            if ((tmpvar_28 > 1.0))
                            {
                                
                                tmpvar_27 = int(0);
                            }
                            else
                            {
                                
                                tmpvar_27 = int(1);
                            };
                            
                            if (tmpvar_27)
                            {
                                
                                float4 tmpvar_30;
                                
                                float2 uv_31;
                                
                                uv_31 = in_f.xlv_TEXCOORD0;
                                
                                float2 tmpvar_32;
                                
                                tmpvar_32 = (_SimpleCenterRadial4.xy + ((1.0 - _SimpleAmount4) * (uv_31 - _SimpleCenterRadial4.xy)));
                                
                                float4 tmpvar_33;
                                
                                tmpvar_33 = tex2D(_MainTex, tmpvar_32);
                                
                                tmpvar_30 = tmpvar_33;
                                
                                c_1 = tmpvar_30;
                            }
                            else
                            {
                                
                                float2 uv_34;
                                
                                uv_34 = in_f.xlv_TEXCOORD0;
                                
                                int tmpvar_35;
                                
                                float tmpvar_36;
                                
                                float2 x_37;
                                
                                x_37 = ((uv_34 - _SimpleCenterRadial5.xy) / _SimpleCenterRadial5.zw);
                                
                                tmpvar_36 = sqrt(dot(x_37, x_37));
                                
                                if ((tmpvar_36 > 1.0))
                                {
                                    
                                    tmpvar_35 = int(0);
                                }
                                else
                                {
                                    
                                    tmpvar_35 = int(1);
                                };
                                
                                if (tmpvar_35)
                                {
                                    
                                    float4 tmpvar_38;
                                    
                                    float2 uv_39;
                                    
                                    uv_39 = in_f.xlv_TEXCOORD0;
                                    
                                    float2 tmpvar_40;
                                    
                                    tmpvar_40 = (_SimpleCenterRadial5.xy + ((1.0 - _SimpleAmount5) * (uv_39 - _SimpleCenterRadial5.xy)));
                                    
                                    float4 tmpvar_41;
                                    
                                    tmpvar_41 = tex2D(_MainTex, tmpvar_40);
                                    
                                    tmpvar_38 = tmpvar_41;
                                    
                                    c_1 = tmpvar_38;
                                }
                                else
                                {
                                    
                                    float2 uv_42;
                                    
                                    uv_42 = in_f.xlv_TEXCOORD0;
                                    
                                    int tmpvar_43;
                                    
                                    float tmpvar_44;
                                    
                                    float2 x_45;
                                    
                                    x_45 = ((uv_42 - _SimpleCenterRadial6.xy) / _SimpleCenterRadial6.zw);
                                    
                                    tmpvar_44 = sqrt(dot(x_45, x_45));
                                    
                                    if ((tmpvar_44 > 1.0))
                                    {
                                        
                                        tmpvar_43 = int(0);
                                    }
                                    else
                                    {
                                        
                                        tmpvar_43 = int(1);
                                    };
                                    
                                    if (tmpvar_43)
                                    {
                                        
                                        float4 tmpvar_46;
                                        
                                        float2 uv_47;
                                        
                                        uv_47 = in_f.xlv_TEXCOORD0;
                                        
                                        float2 tmpvar_48;
                                        
                                        tmpvar_48 = (_SimpleCenterRadial6.xy + ((1.0 - _SimpleAmount6) * (uv_47 - _SimpleCenterRadial6.xy)));
                                        
                                        float4 tmpvar_49;
                                        
                                        tmpvar_49 = tex2D(_MainTex, tmpvar_48);
                                        
                                        tmpvar_46 = tmpvar_49;
                                        
                                        c_1 = tmpvar_46;
                                    }
                                    else
                                    {
                                        
                                        float2 uv_50;
                                        
                                        uv_50 = in_f.xlv_TEXCOORD0;
                                        
                                        int tmpvar_51;
                                        
                                        float tmpvar_52;
                                        
                                        float2 x_53;
                                        
                                        x_53 = ((uv_50 - _SimpleCenterRadial7.xy) / _SimpleCenterRadial7.zw);
                                        
                                        tmpvar_52 = sqrt(dot(x_53, x_53));
                                        
                                        if ((tmpvar_52 > 1.0))
                                        {
                                            
                                            tmpvar_51 = int(0);
                                        }
                                        else
                                        {
                                            
                                            tmpvar_51 = int(1);
                                        };
                                        
                                        if (tmpvar_51)
                                        {
                                            
                                            float4 tmpvar_54;
                                            
                                            float2 uv_55;
                                            
                                            uv_55 = in_f.xlv_TEXCOORD0;
                                            
                                            float2 tmpvar_56;
                                            
                                            tmpvar_56 = (_SimpleCenterRadial7.xy + ((1.0 - _SimpleAmount7) * (uv_55 - _SimpleCenterRadial7.xy)));
                                            
                                            float4 tmpvar_57;
                                            
                                            tmpvar_57 = tex2D(_MainTex, tmpvar_56);
                                            
                                            tmpvar_54 = tmpvar_57;
                                            
                                            c_1 = tmpvar_54;
                                        }
                                        else
                                        {
                                            
                                            float2 uv_58;
                                            
                                            uv_58 = in_f.xlv_TEXCOORD0;
                                            
                                            int tmpvar_59;
                                            
                                            float tmpvar_60;
                                            
                                            float2 x_61;
                                            
                                            x_61 = ((uv_58 - _SimpleCenterRadial8.xy) / _SimpleCenterRadial8.zw);
                                            
                                            tmpvar_60 = sqrt(dot(x_61, x_61));
                                            
                                            if ((tmpvar_60 > 1.0))
                                            {
                                                
                                                tmpvar_59 = int(0);
                                            }
                                            else
                                            {
                                                
                                                tmpvar_59 = int(1);
                                            };
                                            
                                            if (tmpvar_59)
                                            {
                                                
                                                float4 tmpvar_62;
                                                
                                                float2 uv_63;
                                                
                                                uv_63 = in_f.xlv_TEXCOORD0;
                                                
                                                float2 tmpvar_64;
                                                
                                                tmpvar_64 = (_SimpleCenterRadial8.xy + ((1.0 - _SimpleAmount8) * (uv_63 - _SimpleCenterRadial8.xy)));
                                                
                                                float4 tmpvar_65;
                                                
                                                tmpvar_65 = tex2D(_MainTex, tmpvar_64);
                                                
                                                tmpvar_62 = tmpvar_65;
                                                
                                                c_1 = tmpvar_62;
                                            }
                                            else
                                            {
                                                
                                                float4 tmpvar_66;
                                                
                                                tmpvar_66 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                                                
                                                c_1 = tmpvar_66;
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
                
                OUT_Data_Frag out_f;
                out_f.color = c_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase
        Pass // ind: 3, name:

        {
            Tags {  }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend One OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            uniform float4 _ComplicatedCenterRadial1;
            
            uniform float _ComplicatedAmount1;
            
            uniform float _ComplicatedRadiusInner1;
            
            uniform float _ComplicatedRadiusOuter1;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1.w = 1.0;
                
                tmpvar_1.xyz = in_v.vertex.xyz;
                
                OUT_Data_Vert OUT;
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
                
                OUT.xlv_TEXCOORD0 = in_v.texcoord.xy;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 c_1;
                
                float2 uv_2;
                
                uv_2 = in_f.xlv_TEXCOORD0;
                
                int tmpvar_3;
                
                float tmpvar_4;
                
                float2 x_5;
                
                x_5 = ((uv_2 - _ComplicatedCenterRadial1.xy) / _ComplicatedCenterRadial1.zw);
                
                tmpvar_4 = sqrt(dot(x_5, x_5));
                
                if ((tmpvar_4 < _ComplicatedRadiusOuter1))
                {
                    
                    tmpvar_3 = int(1);
                }
                else
                {
                    
                    tmpvar_3 = int(0);
                };
                
                if (tmpvar_3)
                {
                    
                    float4 tmpvar_6;
                    
                    float2 uv_7;
                    
                    uv_7 = in_f.xlv_TEXCOORD0;
                    
                    float2 newUV_8;
                    
                    float2 tmpvar_9;
                    
                    tmpvar_9 = (uv_7 - _ComplicatedCenterRadial1.xy);
                    
                    float tmpvar_10;
                    
                    float2 x_11;
                    
                    x_11 = (tmpvar_9 / _ComplicatedCenterRadial1.zw);
                    
                    tmpvar_10 = sqrt(dot(x_11, x_11));
                    
                    float tmpvar_12;
                    
                    tmpvar_12 = (1.0 - _ComplicatedAmount1);
                    
                    if ((tmpvar_10 < _ComplicatedRadiusInner1))
                    {
                        
                        newUV_8 = (_ComplicatedCenterRadial1.xy + (tmpvar_9 * tmpvar_12));
                    }
                    else
                    {
                        
                        float c_13;
                        
                        c_13 = (cos((((tmpvar_10 - _ComplicatedRadiusInner1) / (_ComplicatedRadiusOuter1 - _ComplicatedRadiusInner1)) * 3.14159)) + 1.0);
                        
                        c_13 = (c_13 / 2.0);
                        
                        newUV_8 = (((_ComplicatedCenterRadial1.xy + (tmpvar_9 * tmpvar_12)) * c_13) + (uv_7 * (1.0 - c_13)));
                    };
                    
                    float4 tmpvar_14;
                    
                    tmpvar_14 = tex2D(_MainTex, newUV_8);
                    
                    tmpvar_6 = tmpvar_14;
                    
                    c_1 = tmpvar_6;
                }
                else
                {
                    
                    float4 tmpvar_15;
                    
                    tmpvar_15 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                    
                    c_1 = tmpvar_15;
                };
                
                OUT_Data_Frag out_f;
                out_f.color = c_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase
        Pass // ind: 4, name:

        {
            Tags {  }
            ZWrite Off
            Cull Off
            Fog
            {
                Mode  Off
            }
            Blend One OneMinusSrcAlpha
            // m_ProgramMask = 6
            CGPROGRAM
            //#pragma target 4.0
            
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            
            #define CODE_BLOCK_VERTEX
            

            

            
            uniform sampler2D _MainTex;
            
            uniform float4 _ComplicatedCenterRadial1;
            
            uniform float _ComplicatedAmount1;
            
            uniform float _ComplicatedRadiusInner1;
            
            uniform float _ComplicatedRadiusOuter1;
            
            uniform float4 _ComplicatedCenterRadial2;
            
            uniform float _ComplicatedAmount2;
            
            uniform float _ComplicatedRadiusInner2;
            
            uniform float _ComplicatedRadiusOuter2;
            
            uniform float4 _ComplicatedCenterRadial3;
            
            uniform float _ComplicatedAmount3;
            
            uniform float _ComplicatedRadiusInner3;
            
            uniform float _ComplicatedRadiusOuter3;
            
            uniform float4 _ComplicatedCenterRadial4;
            
            uniform float _ComplicatedAmount4;
            
            uniform float _ComplicatedRadiusInner4;
            
            uniform float _ComplicatedRadiusOuter4;
            
            uniform float4 _ComplicatedCenterRadial5;
            
            uniform float _ComplicatedAmount5;
            
            uniform float _ComplicatedRadiusInner5;
            
            uniform float _ComplicatedRadiusOuter5;
            
            uniform float4 _ComplicatedCenterRadial6;
            
            uniform float _ComplicatedAmount6;
            
            uniform float _ComplicatedRadiusInner6;
            
            uniform float _ComplicatedRadiusOuter6;
            
            uniform float4 _ComplicatedCenterRadial7;
            
            uniform float _ComplicatedAmount7;
            
            uniform float _ComplicatedRadiusInner7;
            
            uniform float _ComplicatedRadiusOuter7;
            
            uniform float4 _ComplicatedCenterRadial8;
            
            uniform float _ComplicatedAmount8;
            
            uniform float _ComplicatedRadiusInner8;
            
            uniform float _ComplicatedRadiusOuter8;
            
            
            
            struct appdata_t
            {
                
                float4 vertex : POSITION;
                
                float4 texcoord : TEXCOORD0;
            };
            
            
            struct OUT_Data_Vert
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
                
                float4 vertex : SV_POSITION;
            };
            
            
            struct v2f
            {
                
                float2 xlv_TEXCOORD0 : TEXCOORD0;
            };
            
            
            struct OUT_Data_Frag
            {
                
                float4 color : SV_Target0;
            };
            
            
            OUT_Data_Vert vert(appdata_t in_v)
            {
                
                float4 tmpvar_1;
                
                tmpvar_1.w = 1.0;
                
                tmpvar_1.xyz = in_v.vertex.xyz;
                
                OUT_Data_Vert OUT;
                OUT.vertex = mul(unity_MatrixVP, mul(unity_ObjectToWorld, tmpvar_1));
                
                OUT.xlv_TEXCOORD0 = in_v.texcoord.xy;
                return OUT;
            }
            
            
            
            #define CODE_BLOCK_FRAGMENT
            OUT_Data_Frag frag(v2f in_f)
            {
                
                float4 c_1;
                
                float2 uv_2;
                
                uv_2 = in_f.xlv_TEXCOORD0;
                
                int tmpvar_3;
                
                float tmpvar_4;
                
                float2 x_5;
                
                x_5 = ((uv_2 - _ComplicatedCenterRadial1.xy) / _ComplicatedCenterRadial1.zw);
                
                tmpvar_4 = sqrt(dot(x_5, x_5));
                
                if ((tmpvar_4 < _ComplicatedRadiusOuter1))
                {
                    
                    tmpvar_3 = int(1);
                }
                else
                {
                    
                    tmpvar_3 = int(0);
                };
                
                if (tmpvar_3)
                {
                    
                    float4 tmpvar_6;
                    
                    float2 uv_7;
                    
                    uv_7 = in_f.xlv_TEXCOORD0;
                    
                    float2 newUV_8;
                    
                    float2 tmpvar_9;
                    
                    tmpvar_9 = (uv_7 - _ComplicatedCenterRadial1.xy);
                    
                    float tmpvar_10;
                    
                    float2 x_11;
                    
                    x_11 = (tmpvar_9 / _ComplicatedCenterRadial1.zw);
                    
                    tmpvar_10 = sqrt(dot(x_11, x_11));
                    
                    float tmpvar_12;
                    
                    tmpvar_12 = (1.0 - _ComplicatedAmount1);
                    
                    if ((tmpvar_10 < _ComplicatedRadiusInner1))
                    {
                        
                        newUV_8 = (_ComplicatedCenterRadial1.xy + (tmpvar_9 * tmpvar_12));
                    }
                    else
                    {
                        
                        float c_13;
                        
                        c_13 = (cos((((tmpvar_10 - _ComplicatedRadiusInner1) / (_ComplicatedRadiusOuter1 - _ComplicatedRadiusInner1)) * 3.14159)) + 1.0);
                        
                        c_13 = (c_13 / 2.0);
                        
                        newUV_8 = (((_ComplicatedCenterRadial1.xy + (tmpvar_9 * tmpvar_12)) * c_13) + (uv_7 * (1.0 - c_13)));
                    };
                    
                    float4 tmpvar_14;
                    
                    tmpvar_14 = tex2D(_MainTex, newUV_8);
                    
                    tmpvar_6 = tmpvar_14;
                    
                    c_1 = tmpvar_6;
                }
                else
                {
                    
                    float2 uv_15;
                    
                    uv_15 = in_f.xlv_TEXCOORD0;
                    
                    int tmpvar_16;
                    
                    float tmpvar_17;
                    
                    float2 x_18;
                    
                    x_18 = ((uv_15 - _ComplicatedCenterRadial2.xy) / _ComplicatedCenterRadial2.zw);
                    
                    tmpvar_17 = sqrt(dot(x_18, x_18));
                    
                    if ((tmpvar_17 < _ComplicatedRadiusOuter2))
                    {
                        
                        tmpvar_16 = int(1);
                    }
                    else
                    {
                        
                        tmpvar_16 = int(0);
                    };
                    
                    if (tmpvar_16)
                    {
                        
                        float4 tmpvar_19;
                        
                        float2 uv_20;
                        
                        uv_20 = in_f.xlv_TEXCOORD0;
                        
                        float2 newUV_21;
                        
                        float2 tmpvar_22;
                        
                        tmpvar_22 = (uv_20 - _ComplicatedCenterRadial2.xy);
                        
                        float tmpvar_23;
                        
                        float2 x_24;
                        
                        x_24 = (tmpvar_22 / _ComplicatedCenterRadial2.zw);
                        
                        tmpvar_23 = sqrt(dot(x_24, x_24));
                        
                        float tmpvar_25;
                        
                        tmpvar_25 = (1.0 - _ComplicatedAmount2);
                        
                        if ((tmpvar_23 < _ComplicatedRadiusInner2))
                        {
                            
                            newUV_21 = (_ComplicatedCenterRadial2.xy + (tmpvar_22 * tmpvar_25));
                        }
                        else
                        {
                            
                            float c_26;
                            
                            c_26 = (cos((((tmpvar_23 - _ComplicatedRadiusInner2) / (_ComplicatedRadiusOuter2 - _ComplicatedRadiusInner2)) * 3.14159)) + 1.0);
                            
                            c_26 = (c_26 / 2.0);
                            
                            newUV_21 = (((_ComplicatedCenterRadial2.xy + (tmpvar_22 * tmpvar_25)) * c_26) + (uv_20 * (1.0 - c_26)));
                        };
                        
                        float4 tmpvar_27;
                        
                        tmpvar_27 = tex2D(_MainTex, newUV_21);
                        
                        tmpvar_19 = tmpvar_27;
                        
                        c_1 = tmpvar_19;
                    }
                    else
                    {
                        
                        float2 uv_28;
                        
                        uv_28 = in_f.xlv_TEXCOORD0;
                        
                        int tmpvar_29;
                        
                        float tmpvar_30;
                        
                        float2 x_31;
                        
                        x_31 = ((uv_28 - _ComplicatedCenterRadial3.xy) / _ComplicatedCenterRadial3.zw);
                        
                        tmpvar_30 = sqrt(dot(x_31, x_31));
                        
                        if ((tmpvar_30 < _ComplicatedRadiusOuter3))
                        {
                            
                            tmpvar_29 = int(1);
                        }
                        else
                        {
                            
                            tmpvar_29 = int(0);
                        };
                        
                        if (tmpvar_29)
                        {
                            
                            float4 tmpvar_32;
                            
                            float2 uv_33;
                            
                            uv_33 = in_f.xlv_TEXCOORD0;
                            
                            float2 newUV_34;
                            
                            float2 tmpvar_35;
                            
                            tmpvar_35 = (uv_33 - _ComplicatedCenterRadial3.xy);
                            
                            float tmpvar_36;
                            
                            float2 x_37;
                            
                            x_37 = (tmpvar_35 / _ComplicatedCenterRadial3.zw);
                            
                            tmpvar_36 = sqrt(dot(x_37, x_37));
                            
                            float tmpvar_38;
                            
                            tmpvar_38 = (1.0 - _ComplicatedAmount3);
                            
                            if ((tmpvar_36 < _ComplicatedRadiusInner3))
                            {
                                
                                newUV_34 = (_ComplicatedCenterRadial3.xy + (tmpvar_35 * tmpvar_38));
                            }
                            else
                            {
                                
                                float c_39;
                                
                                c_39 = (cos((((tmpvar_36 - _ComplicatedRadiusInner3) / (_ComplicatedRadiusOuter3 - _ComplicatedRadiusInner3)) * 3.14159)) + 1.0);
                                
                                c_39 = (c_39 / 2.0);
                                
                                newUV_34 = (((_ComplicatedCenterRadial3.xy + (tmpvar_35 * tmpvar_38)) * c_39) + (uv_33 * (1.0 - c_39)));
                            };
                            
                            float4 tmpvar_40;
                            
                            tmpvar_40 = tex2D(_MainTex, newUV_34);
                            
                            tmpvar_32 = tmpvar_40;
                            
                            c_1 = tmpvar_32;
                        }
                        else
                        {
                            
                            float2 uv_41;
                            
                            uv_41 = in_f.xlv_TEXCOORD0;
                            
                            int tmpvar_42;
                            
                            float tmpvar_43;
                            
                            float2 x_44;
                            
                            x_44 = ((uv_41 - _ComplicatedCenterRadial4.xy) / _ComplicatedCenterRadial4.zw);
                            
                            tmpvar_43 = sqrt(dot(x_44, x_44));
                            
                            if ((tmpvar_43 < _ComplicatedRadiusOuter4))
                            {
                                
                                tmpvar_42 = int(1);
                            }
                            else
                            {
                                
                                tmpvar_42 = int(0);
                            };
                            
                            if (tmpvar_42)
                            {
                                
                                float4 tmpvar_45;
                                
                                float2 uv_46;
                                
                                uv_46 = in_f.xlv_TEXCOORD0;
                                
                                float2 newUV_47;
                                
                                float2 tmpvar_48;
                                
                                tmpvar_48 = (uv_46 - _ComplicatedCenterRadial4.xy);
                                
                                float tmpvar_49;
                                
                                float2 x_50;
                                
                                x_50 = (tmpvar_48 / _ComplicatedCenterRadial4.zw);
                                
                                tmpvar_49 = sqrt(dot(x_50, x_50));
                                
                                float tmpvar_51;
                                
                                tmpvar_51 = (1.0 - _ComplicatedAmount4);
                                
                                if ((tmpvar_49 < _ComplicatedRadiusInner4))
                                {
                                    
                                    newUV_47 = (_ComplicatedCenterRadial4.xy + (tmpvar_48 * tmpvar_51));
                                }
                                else
                                {
                                    
                                    float c_52;
                                    
                                    c_52 = (cos((((tmpvar_49 - _ComplicatedRadiusInner4) / (_ComplicatedRadiusOuter4 - _ComplicatedRadiusInner4)) * 3.14159)) + 1.0);
                                    
                                    c_52 = (c_52 / 2.0);
                                    
                                    newUV_47 = (((_ComplicatedCenterRadial4.xy + (tmpvar_48 * tmpvar_51)) * c_52) + (uv_46 * (1.0 - c_52)));
                                };
                                
                                float4 tmpvar_53;
                                
                                tmpvar_53 = tex2D(_MainTex, newUV_47);
                                
                                tmpvar_45 = tmpvar_53;
                                
                                c_1 = tmpvar_45;
                            }
                            else
                            {
                                
                                float2 uv_54;
                                
                                uv_54 = in_f.xlv_TEXCOORD0;
                                
                                int tmpvar_55;
                                
                                float tmpvar_56;
                                
                                float2 x_57;
                                
                                x_57 = ((uv_54 - _ComplicatedCenterRadial5.xy) / _ComplicatedCenterRadial5.zw);
                                
                                tmpvar_56 = sqrt(dot(x_57, x_57));
                                
                                if ((tmpvar_56 < _ComplicatedRadiusOuter5))
                                {
                                    
                                    tmpvar_55 = int(1);
                                }
                                else
                                {
                                    
                                    tmpvar_55 = int(0);
                                };
                                
                                if (tmpvar_55)
                                {
                                    
                                    float4 tmpvar_58;
                                    
                                    float2 uv_59;
                                    
                                    uv_59 = in_f.xlv_TEXCOORD0;
                                    
                                    float2 newUV_60;
                                    
                                    float2 tmpvar_61;
                                    
                                    tmpvar_61 = (uv_59 - _ComplicatedCenterRadial5.xy);
                                    
                                    float tmpvar_62;
                                    
                                    float2 x_63;
                                    
                                    x_63 = (tmpvar_61 / _ComplicatedCenterRadial5.zw);
                                    
                                    tmpvar_62 = sqrt(dot(x_63, x_63));
                                    
                                    float tmpvar_64;
                                    
                                    tmpvar_64 = (1.0 - _ComplicatedAmount5);
                                    
                                    if ((tmpvar_62 < _ComplicatedRadiusInner5))
                                    {
                                        
                                        newUV_60 = (_ComplicatedCenterRadial5.xy + (tmpvar_61 * tmpvar_64));
                                    }
                                    else
                                    {
                                        
                                        float c_65;
                                        
                                        c_65 = (cos((((tmpvar_62 - _ComplicatedRadiusInner5) / (_ComplicatedRadiusOuter5 - _ComplicatedRadiusInner5)) * 3.14159)) + 1.0);
                                        
                                        c_65 = (c_65 / 2.0);
                                        
                                        newUV_60 = (((_ComplicatedCenterRadial5.xy + (tmpvar_61 * tmpvar_64)) * c_65) + (uv_59 * (1.0 - c_65)));
                                    };
                                    
                                    float4 tmpvar_66;
                                    
                                    tmpvar_66 = tex2D(_MainTex, newUV_60);
                                    
                                    tmpvar_58 = tmpvar_66;
                                    
                                    c_1 = tmpvar_58;
                                }
                                else
                                {
                                    
                                    float2 uv_67;
                                    
                                    uv_67 = in_f.xlv_TEXCOORD0;
                                    
                                    int tmpvar_68;
                                    
                                    float tmpvar_69;
                                    
                                    float2 x_70;
                                    
                                    x_70 = ((uv_67 - _ComplicatedCenterRadial6.xy) / _ComplicatedCenterRadial6.zw);
                                    
                                    tmpvar_69 = sqrt(dot(x_70, x_70));
                                    
                                    if ((tmpvar_69 < _ComplicatedRadiusOuter6))
                                    {
                                        
                                        tmpvar_68 = int(1);
                                    }
                                    else
                                    {
                                        
                                        tmpvar_68 = int(0);
                                    };
                                    
                                    if (tmpvar_68)
                                    {
                                        
                                        float4 tmpvar_71;
                                        
                                        float2 uv_72;
                                        
                                        uv_72 = in_f.xlv_TEXCOORD0;
                                        
                                        float2 newUV_73;
                                        
                                        float2 tmpvar_74;
                                        
                                        tmpvar_74 = (uv_72 - _ComplicatedCenterRadial6.xy);
                                        
                                        float tmpvar_75;
                                        
                                        float2 x_76;
                                        
                                        x_76 = (tmpvar_74 / _ComplicatedCenterRadial6.zw);
                                        
                                        tmpvar_75 = sqrt(dot(x_76, x_76));
                                        
                                        float tmpvar_77;
                                        
                                        tmpvar_77 = (1.0 - _ComplicatedAmount6);
                                        
                                        if ((tmpvar_75 < _ComplicatedRadiusInner6))
                                        {
                                            
                                            newUV_73 = (_ComplicatedCenterRadial6.xy + (tmpvar_74 * tmpvar_77));
                                        }
                                        else
                                        {
                                            
                                            float c_78;
                                            
                                            c_78 = (cos((((tmpvar_75 - _ComplicatedRadiusInner6) / (_ComplicatedRadiusOuter6 - _ComplicatedRadiusInner6)) * 3.14159)) + 1.0);
                                            
                                            c_78 = (c_78 / 2.0);
                                            
                                            newUV_73 = (((_ComplicatedCenterRadial6.xy + (tmpvar_74 * tmpvar_77)) * c_78) + (uv_72 * (1.0 - c_78)));
                                        };
                                        
                                        float4 tmpvar_79;
                                        
                                        tmpvar_79 = tex2D(_MainTex, newUV_73);
                                        
                                        tmpvar_71 = tmpvar_79;
                                        
                                        c_1 = tmpvar_71;
                                    }
                                    else
                                    {
                                        
                                        float2 uv_80;
                                        
                                        uv_80 = in_f.xlv_TEXCOORD0;
                                        
                                        int tmpvar_81;
                                        
                                        float tmpvar_82;
                                        
                                        float2 x_83;
                                        
                                        x_83 = ((uv_80 - _ComplicatedCenterRadial7.xy) / _ComplicatedCenterRadial7.zw);
                                        
                                        tmpvar_82 = sqrt(dot(x_83, x_83));
                                        
                                        if ((tmpvar_82 < _ComplicatedRadiusOuter7))
                                        {
                                            
                                            tmpvar_81 = int(1);
                                        }
                                        else
                                        {
                                            
                                            tmpvar_81 = int(0);
                                        };
                                        
                                        if (tmpvar_81)
                                        {
                                            
                                            float4 tmpvar_84;
                                            
                                            float2 uv_85;
                                            
                                            uv_85 = in_f.xlv_TEXCOORD0;
                                            
                                            float2 newUV_86;
                                            
                                            float2 tmpvar_87;
                                            
                                            tmpvar_87 = (uv_85 - _ComplicatedCenterRadial7.xy);
                                            
                                            float tmpvar_88;
                                            
                                            float2 x_89;
                                            
                                            x_89 = (tmpvar_87 / _ComplicatedCenterRadial7.zw);
                                            
                                            tmpvar_88 = sqrt(dot(x_89, x_89));
                                            
                                            float tmpvar_90;
                                            
                                            tmpvar_90 = (1.0 - _ComplicatedAmount7);
                                            
                                            if ((tmpvar_88 < _ComplicatedRadiusInner7))
                                            {
                                                
                                                newUV_86 = (_ComplicatedCenterRadial7.xy + (tmpvar_87 * tmpvar_90));
                                            }
                                            else
                                            {
                                                
                                                float c_91;
                                                
                                                c_91 = (cos((((tmpvar_88 - _ComplicatedRadiusInner7) / (_ComplicatedRadiusOuter7 - _ComplicatedRadiusInner7)) * 3.14159)) + 1.0);
                                                
                                                c_91 = (c_91 / 2.0);
                                                
                                                newUV_86 = (((_ComplicatedCenterRadial7.xy + (tmpvar_87 * tmpvar_90)) * c_91) + (uv_85 * (1.0 - c_91)));
                                            };
                                            
                                            float4 tmpvar_92;
                                            
                                            tmpvar_92 = tex2D(_MainTex, newUV_86);
                                            
                                            tmpvar_84 = tmpvar_92;
                                            
                                            c_1 = tmpvar_84;
                                        }
                                        else
                                        {
                                            
                                            float2 uv_93;
                                            
                                            uv_93 = in_f.xlv_TEXCOORD0;
                                            
                                            int tmpvar_94;
                                            
                                            float tmpvar_95;
                                            
                                            float2 x_96;
                                            
                                            x_96 = ((uv_93 - _ComplicatedCenterRadial8.xy) / _ComplicatedCenterRadial8.zw);
                                            
                                            tmpvar_95 = sqrt(dot(x_96, x_96));
                                            
                                            if ((tmpvar_95 < _ComplicatedRadiusOuter8))
                                            {
                                                
                                                tmpvar_94 = int(1);
                                            }
                                            else
                                            {
                                                
                                                tmpvar_94 = int(0);
                                            };
                                            
                                            if (tmpvar_94)
                                            {
                                                
                                                float4 tmpvar_97;
                                                
                                                float2 uv_98;
                                                
                                                uv_98 = in_f.xlv_TEXCOORD0;
                                                
                                                float2 newUV_99;
                                                
                                                float2 tmpvar_100;
                                                
                                                tmpvar_100 = (uv_98 - _ComplicatedCenterRadial8.xy);
                                                
                                                float tmpvar_101;
                                                
                                                float2 x_102;
                                                
                                                x_102 = (tmpvar_100 / _ComplicatedCenterRadial8.zw);
                                                
                                                tmpvar_101 = sqrt(dot(x_102, x_102));
                                                
                                                float tmpvar_103;
                                                
                                                tmpvar_103 = (1.0 - _ComplicatedAmount8);
                                                
                                                if ((tmpvar_101 < _ComplicatedRadiusInner8))
                                                {
                                                    
                                                    newUV_99 = (_ComplicatedCenterRadial8.xy + (tmpvar_100 * tmpvar_103));
                                                }
                                                else
                                                {
                                                    
                                                    float c_104;
                                                    
                                                    c_104 = (cos((((tmpvar_101 - _ComplicatedRadiusInner8) / (_ComplicatedRadiusOuter8 - _ComplicatedRadiusInner8)) * 3.14159)) + 1.0);
                                                    
                                                    c_104 = (c_104 / 2.0);
                                                    
                                                    newUV_99 = (((_ComplicatedCenterRadial8.xy + (tmpvar_100 * tmpvar_103)) * c_104) + (uv_98 * (1.0 - c_104)));
                                                };
                                                
                                                float4 tmpvar_105;
                                                
                                                tmpvar_105 = tex2D(_MainTex, newUV_99);
                                                
                                                tmpvar_97 = tmpvar_105;
                                                
                                                c_1 = tmpvar_97;
                                            }
                                            else
                                            {
                                                
                                                float4 tmpvar_106;
                                                
                                                tmpvar_106 = tex2D(_MainTex, in_f.xlv_TEXCOORD0);
                                                
                                                c_1 = tmpvar_106;
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
                OUT_Data_Frag out_f;
                out_f.color = c_1;
                return out_f;
            }
            
            
            
            ENDCG
        }// end phase

    }
    FallBack Off
}
