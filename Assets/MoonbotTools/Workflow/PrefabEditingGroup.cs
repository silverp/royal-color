using UnityEngine;
using System.Collections;

namespace Moonbot.Workflow {

	/// <summary>
	/// Script added to the EditPrefab group that is created.
	/// Supports both destroying or disabling the object.
	/// </summary>
	public class PrefabEditingGroup : MonoBehaviour {

		public enum Action {
			Destroy,
			Disable,
			None,
		}

		public Action action;

		void Awake() {
			switch (action) {
				default:
					break;
				case Action.Destroy:
					DestroyImmediate(gameObject);
					break;
				case Action.Disable:
					gameObject.SetActive(false);
					break;
			}
		}

	}

}