using UnityEngine;
using UnityEditor;
using System.Collections;


namespace Moonbot.Workflow {

	public class Group : ScriptableObject {

		/// <summary>
		/// Group the selected objects in the hierarchy
		/// </summary>
		[MenuItem("Edit/Group %g", false, 2000)]
		static void GroupSelected() {
			GameObject grp = GroupTransforms(Selection.transforms);
			Selection.objects = new Object[]{grp};
		}

		/// <summary>
		/// Reset the selected transforms, whilst preserving
		/// the children's world transformations
		/// </summary>
		[MenuItem("GameObject/Reset Transform Keep Children", false, 25)]
		static void ResetSelectedTransformKeepChildren() {
			Undo.RecordObjects(Selection.transforms, "Center");
			foreach (Transform t in Selection.transforms) {
				ResetTransformPreserveChildren(t);
			}
		}

		[MenuItem("Edit/Group %g", true)]
		[MenuItem("Edit/Reset Transform Keep Children", true)]
		static bool ValidateHasSelection() {
			return Selection.transforms.Length > 0;
		}

		/// <summary>
		/// Group the given list of transforms. Attempts to place
		/// the new group at the lowest sibling index (4.5 and later).
		/// Returns the newly created group.
		/// </summary>
		public static GameObject GroupTransforms(Transform[] transforms) {
			GameObject grp = new GameObject("Group");
			Undo.RegisterCreatedObjectUndo(grp, "Undo Group");

			#if UNITY_4_5 || UNITY_4_6
			// sort by sibling index if available
			System.Array.Sort(transforms, (Transform a, Transform b) => a.GetSiblingIndex().CompareTo(b.GetSiblingIndex()));
			#endif

			grp.transform.parent = transforms[0].parent;
			grp.transform.localPosition = Vector3.zero;
			grp.transform.localRotation = Quaternion.identity;
			grp.transform.localScale = Vector3.one;

			#if UNITY_4_5 || UNITY_4_6
			// put group at same index as the first transform
			grp.transform.SetSiblingIndex(transforms[0].GetSiblingIndex());
			#endif
			

			for (int i = 0; i < transforms.Length; i++) {
				Undo.SetTransformParent(transforms[i], grp.transform, "Undo Group");
				#if UNITY_4_5 || UNITY_4_6
				transforms[i].SetSiblingIndex(i);
				#endif
			}
			return grp;
		}

		static Transform[] GetChildren(Transform t) {
			Transform[] children = new Transform[t.childCount];
			for (int i = 0; i < t.childCount; i++) {
				children[i] = t.GetChild(i);
			}
			return children;
		}
		
		/// <summary>
		/// Return the center point in world space of all the given transforms
		/// </summary>
		public static Vector3 GetCenter(Transform[] transforms) {
			Vector3 center = Vector3.zero;
			if (transforms.Length > 0) {
				foreach (Transform t in transforms) {
					if (t) {
						center += t.position;
					}
				}
				center /= (float)transforms.Length;
			}
			return center;
		}

		/// <summary>
		/// Resets the position rotation and scale of the given transform,
		/// without moving its children in world space.
		/// </summary>
		/// <param name="t">The transform to reset</param>
		public static void ResetTransformPreserveChildren(Transform t) {
			Undo.RecordObject(t, "Undo Reset Transform Keep Children");
			Undo.RecordObjects(GetChildren(t), "Undo Reset Transform Keep Children");
			MoveTransformPreserveChildren(t, Vector3.zero, Quaternion.identity, Vector3.one);
		}

		/// <summary>
		/// Move the given transform to a position, whilst preserving
		/// its children's world transformations. Preserves the transform's
		/// current local rotation and local scale
		/// </summary>
		/// <param name="t">The transform to move</param>
		/// <param name="position">The local position to move the transform to</param>
		public static void MoveTransformPreserveChildren(Transform t, Vector3 position) {
			MoveTransformPreserveChildren(t, position, t.localRotation, t.localScale);
		}
		/// <summary>
		/// Move the given transform to a position and rotation, whilst preserving
		/// its children's world transformations. Preserves the transform's current
		/// local scale
		/// </summary>
		/// <param name="t">The transform to move</param>
		/// <param name="position">The local position to move the transform to</param>
		/// <param name="rotation">The local rotation to move the transform to</param>
		public static void MoveTransformPreserveChildren(Transform t, Vector3 position, Quaternion rotation) {
			MoveTransformPreserveChildren(t, position, rotation, t.localScale);
		}
		/// <summary>
		/// Move the given transform to a position, rotation, and scale, whilst preserving
		/// its children's world transformations. Some scaling changes may occur on children
		/// when dealing with non-uniform scales
		/// </summary>
		/// <param name="t">The transform to move</param>
		/// <param name="position">The local position to move the transform to</param>
		/// <param name="rotation">The local rotation to move the transform to</param>
		/// <param name="scale">The local scale to move the transform to</param>
		public static void MoveTransformPreserveChildren(Transform t, Vector3 position, Quaternion rotation, Vector3 scale) {
			Transform[] children = GetChildren(t);
			for (int i = 0; i < children.Length; i++) {
				children[i].parent = t.parent;
			}
			t.localPosition = position;
			t.localRotation = rotation;
			t.localScale = scale;
			for (int i = 0; i < children.Length; i++) {
				children[i].parent = t;
			}
		}
	}
}