using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace Moonbot.Workflow {

	/// <summary>
	/// An editor for getting GUID information about any assets,
	/// as well as easily performing project-wide searches.
	/// The Search tab of this window supports searching for any
	/// string, not just GUIDs, and can search within the project,
	/// or within any folder on the file system. See FileContentSearch
	/// for information on how the searches are performed.
	/// </summary>
	public class GUIDUtilsWindow : EditorWindow {


		/// <summary>
		/// Represents information about a Unity Object. Contains
		/// the guid, and properties for the assetPath, icon, etc.
		/// </summary>
		public class InfoItem : object {

			private string _guid;
			/// <summary>
			/// The guid of the asset this item represents
			/// </summary>
			public string guid {
				get { return _guid; }
				set {
					if (_guid != value) {
						_guid = value;
						Refresh();
					}
				}
			}

			private string _assetPath;
			/// <summary>
			/// The asset path for this item. Retrieves path using guid
			/// </summary>
			public string assetPath {
				get {
					if (_assetPath == null) {
						_assetPath = AssetDatabase.GUIDToAssetPath(guid);
					}
					return _assetPath ?? "";
				}
			}

			public string basename {
				get { return System.IO.Path.GetFileNameWithoutExtension(assetPath); }
			}

			private Texture _icon;
			/// <summary>
			/// The cached icon for this item. Retieves icon using assetPath
			/// </summary>
			public Texture icon {
				get {
					if (_icon == null && GUIDUtils.IsAssetsPath(assetPath)) {
						_icon = AssetDatabase.GetCachedIcon(assetPath);
					}
					return _icon;
				}
			}

			public bool selected;

			public bool showCopyButton = true;
			private bool _showSearchNameButton = true;
			public bool showSearchNameButton {
				get {
					if (string.IsNullOrEmpty(basename)) {
						return false;
					}
					return _showSearchNameButton;
				}
				set {
					_showSearchNameButton = value;
				}
			}
			public bool showSearchButton = true;
			public bool showDeleteButton = true;
			
			public InfoItem() {}
			public InfoItem(string guid) {
				this.guid = guid;
			}
			public InfoItem(Object obj) {
				guid = GUIDUtils.ObjectToGUID(obj);
			}
			
			public override bool Equals(object o) {
				if (o == null) {
					return false;
				}
				if (o is InfoItem) {
					return guid == ((InfoItem)o).guid;
				}
				return false;
			}
			
			public override int GetHashCode() {
				return guid.GetHashCode();
			}

			/// <summary>
			/// Refresh this item, clears any cached properties,
			/// such as the icon or assetPath
			/// </summary>
			public void Refresh() {
				_assetPath = null;
				_icon = null;
			}
			
			public void Select() {
				Selection.activeObject = GUIDUtils.GUIDToObject(guid);
			}

			/// <summary>
			/// Returns how many buttons will be drawn
			/// </summary>
			int ButtonCount() {
				int count = 0;
				if (showCopyButton) {
					count++;
				}
				if (showSearchNameButton) {
					count++;
				}
				if (showSearchButton) {
					count++;
				}
				if (showDeleteButton) {
					count++;
				}
				return count;
			}

			public bool Draw(System.Action<string> searchCallback, System.Action<InfoItem> deleteCallback = null, bool drawBox = true) {
				// start horizontal layout with box style if applicable
				if (drawBox) {
					GUIStyle boxStyle = new GUIStyle("box");
					if (selected) {
						boxStyle.normal.background = GUIDUtilsWindow.lightBGTexture;
					}
					GUILayout.BeginHorizontal(boxStyle);
				} else {
					GUILayout.BeginHorizontal();
				}
				
				// button with icon of the asset, click to select it
				Texture icon = AssetDatabase.GetCachedIcon(assetPath);
				if (icon != null) {
					Rect r = GUILayoutUtility.GetAspectRect(1f, GUILayout.Width(40f));
					r.y += 8f;
					if (GUI.Button(r, icon)) {
						Select();
					}
				}
				
				GUILayout.BeginVertical();
				
				// filename of the asset, with a Copy, Search and remove (X) button
				GUILayout.BeginHorizontal();
				EditorGUILayout.SelectableLabel(System.IO.Path.GetFileName(assetPath), GUILayout.Height(16f));

				GUIStyle[] buttonStyles = EditorUtils.GetMiniButtonStyles(ButtonCount());
				GUILayoutOption opts = GUILayout.ExpandWidth(false);

				bool btnClicked = false;
				int i = 0;
				if (showCopyButton) {
					if (GUILayout.Button("copy", buttonStyles[i], opts)) {
						EditorUtils.CopyStringToClipboard(guid);
						btnClicked = true;
					}
					i++;
				}
				if (showSearchNameButton) {
					if (GUILayout.Button("search name", buttonStyles[i], opts)) {
						GUIDUtilsWindow.guidInputText = basename;
						if (searchCallback != null) {
							searchCallback(basename);
						}
						btnClicked = true;
					}
					i++;
				}
				if (showSearchButton) {
					if (GUILayout.Button("search", buttonStyles[i], opts)) {
						guidInputText = guid;
						if (searchCallback != null) {
							searchCallback(guid);
						}
						btnClicked = true;
					}
					i++;
				}
				if (showDeleteButton) {
					if (GUILayout.Button("x", buttonStyles[i], opts)) {
						if (deleteCallback != null) {
							deleteCallback(this);
						}
						btnClicked = true;
					}
					i++;
				}
				GUILayout.EndHorizontal();

				EditorGUILayout.SelectableLabel(assetPath, EditorStyles.miniLabel, GUILayout.Height(15f));
				EditorGUILayout.SelectableLabel(guid, EditorStyles.miniLabel, GUILayout.Height(15f));
				
				GUILayout.EndVertical();
				GUILayout.EndHorizontal();

				return btnClicked;
			}
		}



		/// <summary>
		/// Represents a search match item. Stores the path and match count
		/// primarily, and has an InfoItem that can be used to get other info
		/// if available.
		/// </summary>
		public class MatchItem : object {

			/// <summary>
			/// The MatchFile this item represents
			/// </summary>
			public FileContentSearch.MatchFile matchFile;

			/// <summary>
			/// The full path to the file that contained the matches
			/// </summary>
			public string path {
				get { return matchFile.path; }
			}
			/// <summary>
			/// The number of matches found in this file
			/// </summary>
			public int matchCount {
				get { return matchFile.Count; }
			}

			public string assetPath {
				get { return GUIDUtils.GetRelativeAssetsPath(path); }
			}

			public string filename {
				get { return System.IO.Path.GetFileName(path); }
			}

			public string basename {
				get { return System.IO.Path.GetFileNameWithoutExtension(path); }
			}

			public InfoItem infoItem;

			public MatchItem(FileContentSearch.MatchFile matchFile) {
				this.matchFile = matchFile;
				if (GUIDUtils.IsAssetsPath(path)) {
					string guid = AssetDatabase.AssetPathToGUID(assetPath);
					if (!string.IsNullOrEmpty(guid)) {
						infoItem = new InfoItem(guid);
					}
				}
			}

			public void Select() {
				Selection.activeObject = AssetDatabase.LoadMainAssetAtPath(assetPath);
			}

			public void Draw(GUIDUtilsWindow window) {
				GUILayout.BeginHorizontal("box");
				
				// button with icon of the asset, click to select it
				if (infoItem != null && infoItem.icon != null) {
					Rect r = GUILayoutUtility.GetAspectRect(1f, GUILayout.Width(40f));
					if (GUI.Button(r, infoItem.icon)) {
						Select();
					}
				}
				
				GUILayout.BeginVertical();

				// search results with a 'search' button to search for this item as well
				GUILayout.BeginHorizontal();
				EditorGUILayout.SelectableLabel(matchCount + " matches in " + filename, GUILayout.Height(16f));

				int buttonCount = 2;
				if (window.textEditorExists) {
					buttonCount++;
				}
				GUIStyle[] buttonStyles = EditorUtils.GetMiniButtonStyles(buttonCount);
				GUILayoutOption opts = GUILayout.ExpandWidth(false);
				int i = 0;

				if (infoItem != null) {
					if (GUILayout.Button("search name", buttonStyles[i], opts)) {
						if (!string.IsNullOrEmpty(infoItem.guid)) {
							window.StartSearch(infoItem.basename);
						}
					}
					i++;
					if (GUILayout.Button("search", buttonStyles[i], opts)) {
						if (!string.IsNullOrEmpty(infoItem.guid)) {
							window.StartSearch(infoItem.guid);
						}
					}
					i++;
					if (window.textEditorExists) {
						// if text editor is available, show a 'view' button that
						// opens the text editor to the first match in the file
						if (GUILayout.Button("view", buttonStyles[i], opts)) {
							// open to first match
							FileContentSearch.Match m = matchFile.matches[0];
							EditorUtils.OpenTextEditor(path, m.lineNumber);
						}
					}
				}
				GUILayout.EndHorizontal();

				EditorGUILayout.SelectableLabel(assetPath, EditorStyles.miniLabel, GUILayout.Height(15f));
				GUILayout.EndVertical();
				
				GUILayout.EndHorizontal();
			}
		}




		static GUIDUtilsWindow GetWindow() {
			return EditorWindow.GetWindow<GUIDUtilsWindow>("GUID Utils");
		}
		
		[MenuItem("Window/GUID Utils", false, 1000)]
		public static void ShowWindow() {
			GetWindow();
		}
		
		[MenuItem("Assets/Find References In All Assets", false, 25)]
		public static void SearchForAssetReferences() {
			searchPathType = SearchPath.Assets;
			GUIDUtilsWindow w = GetWindow();
			InfoItem item = w.AddInfoItem(Selection.activeObject);
			w.StartSearch(item.guid);
		}

		[MenuItem("Assets/Select Unused", false, 25)]
		public static void FilterSelectionToUnused() {
			SelectUnused(Selection.objects);
		}

		[MenuItem("Assets/Show In GUID Utils", false, 25)]
		public static void ShowInGUIDUtils() {
			GUIDUtilsWindow w = GetWindow();
			foreach (Object o in Selection.objects) {
				w.AddInfoItem(o);
			}
			selectedTab = Tab.Info;
		}
		


		public enum Tab {
			Info,
			Search
		};

		public enum SearchPath {
			Assets,
			Custom
		};


		/// <summary>
		/// The currently selected tab of the GUID Utils Window
		/// </summary>
		public static Tab selectedTab = Tab.Search;
		/// <summary>
		/// The current guid input text string value, this doesn't
		/// have to be a guid, eg. when searching for any string
		/// </summary>
		public static string guidInputText;
		/// <summary>
		/// The search path type used for searching operations
		/// </summary>
		public static SearchPath searchPathType;
		/// <summary>
		/// The custom search path, if searchPathType is Custom
		/// </summary>
		private static string searchCustomPath = "Assets";
		/// <summary>
		/// The evaluated search path, takes into account searchPathType and searchCustomPath
		/// </summary>
		private static string searchPath {
			get { return searchPathType == SearchPath.Assets ? Application.dataPath : searchCustomPath; }
		}

		public static List<string> searches = new List<string>();
		public static int previousSearchIndex;


		private static Texture2D _darkBGTexture;
		private static Texture2D darkBGTexture {
			get {
				if (_darkBGTexture == null) {
					_darkBGTexture = EditorUtils.GetSolidColorTexture(new Color(0f, 0f, 0f, 0.4f));
				}
				return _darkBGTexture;
			}
		}

		private static Texture2D _lightBGTexture;
		private static Texture2D lightBGTexture {
			get {
				if (_lightBGTexture == null) {
					_lightBGTexture = EditorUtils.GetSolidColorTexture(Color.white * 0.6f);
				}
				return _lightBGTexture;
			}
		}


		private HelpMessages help = new HelpMessages();
		
		private List<InfoItem> infoItems = new List<InfoItem>();
		private Vector2 infoScrollPosition;
		private List<MatchItem> matchItems = new List<MatchItem>();
		private List<string> selectedGUIDs = new List<string>();

		/// <summary>
		/// Determines whether a text editor can be found to enable viewing functionality
		/// </summary>
		private bool textEditorExists;

		/// <summary>
		/// The current or last search that was performed
		/// </summary>
		private FileContentSearch search;
		/// <summary>
		/// The InfoItem shown on the Search tab representing the item being searched
		/// </summary>
		private InfoItem searchInfoItem;
		private Vector2 searchScrollPos;

		void OnEnable() {
			FileContentSearch.onFinish += OnSearchFinish;
			textEditorExists = EditorUtils.TextEditorExists();
		}
		
		void OnDisable() {
			FileContentSearch.onFinish -= OnSearchFinish;
		}
		
		void Clear() {
			help.Clear();
			guidInputText = "";

			switch (selectedTab) {
				case Tab.Info:
					infoItems.Clear();
					break;
				case Tab.Search:
					search = null;
					searchCustomPath = "Assets";
					searches.Clear();
					break;
			}
		}


		public InfoItem AddInfoItem(string guid) {
			if (!string.IsNullOrEmpty(guid)) {
				InfoItem item = new InfoItem(guid);
				if (!infoItems.Contains(item)) {
					infoItems.Add(item);
					infoItems.Sort((a, b) => a.assetPath.CompareTo(b.assetPath));
					item.Select();
				}
				return item;
			}
			return null;
		}
		
		public InfoItem AddInfoItem(Object obj) {
			InfoItem item = new InfoItem(obj);
			if (!infoItems.Contains(item)) {
				infoItems.Add(item);
				infoItems.Sort((a, b) => a.assetPath.CompareTo(b.assetPath));
			}
			return item;
		}

		void RemoveInfoItem(InfoItem item) {
			infoItems.Remove(item);
		}


		void OnSelectionChange() {
			// update selected guids list
			selectedGUIDs.Clear();
			foreach (Object obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets)) {
				selectedGUIDs.Add(GUIDUtils.ObjectToGUID(obj));
			}
			Repaint();
		}
		
		void OnGUI() {		
			GUILayout.BeginHorizontal(EditorStyles.toolbar);
			if (GUILayout.Button("Clear", EditorStyles.toolbarButton)) {
				Clear();
				return;
			}
			GUILayout.Space(6f);
			selectedTab = (Tab)GUILayout.Toolbar((int)selectedTab, new string[] { "Info", "Search" }, EditorStyles.toolbarButton);
			GUILayout.FlexibleSpace();
			help.enabled = GUILayout.Toggle(help.enabled, "Help", EditorStyles.toolbarButton);
			GUILayout.EndHorizontal();

			switch (selectedTab) {
				case Tab.Info:
					DrawInfoGUI();
					break;
				case Tab.Search:
					DrawSearchGUI();
					break;
			}

		}
		
		void DrawInputForms() {
			// form to search for an object
			GUI.SetNextControlName("guidInputObject");
			Object inputObj = EditorGUILayout.ObjectField(null, typeof(Object), false);
			if (inputObj != null) {
				InfoItem item = AddInfoItem(inputObj);
				item.Select();
				if (selectedTab == Tab.Search) {
					guidInputText = item.guid;
				}
			}
			
			// form for entering a known guid
			GUILayout.BeginHorizontal();
			if (selectedTab == Tab.Search) {
				GUI.enabled = previousSearchIndex < searches.Count - 1;
				if (GUILayout.Button("<", EditorStyles.miniButtonLeft, GUILayout.ExpandWidth(false))) {
					GoToPreviousSearch();
				}
				GUI.enabled = previousSearchIndex > 0;
				if (GUILayout.Button(">", EditorStyles.miniButtonRight, GUILayout.ExpandWidth(false))) {
					GoToNextSearch();
				}
				GUI.enabled = true;
			}
			// input text field
			GUI.SetNextControlName("guidInputText");
			guidInputText = EditorGUILayout.TextField(guidInputText);
			// find button
			if (GUILayout.Button("Find", EditorStyles.miniButton, GUILayout.ExpandWidth(false))) {
				if (!string.IsNullOrEmpty(guidInputText)) {
					AddInfoItem(guidInputText);
					if (selectedTab == Tab.Search) {
						StartSearch(guidInputText);
					}
				}
			}
			GUILayout.EndHorizontal();
		}

		void DrawInfoGUI() {
			help.Draw("A list of recent searches, useful for keeping track of various GUIDs in once place", MessageType.Info, false, 10f);
			
			DrawInputForms();
			
			// draw the full list of InfoItems
			infoScrollPosition = EditorGUILayout.BeginScrollView(infoScrollPosition);
			foreach (InfoItem item in infoItems) {
				item.selected = selectedGUIDs.Contains(item.guid);
				if (item.Draw(StartSearch, RemoveInfoItem)) {
					// a button was clicked, might have changed items list
					break;
				}
			}
			EditorGUILayout.EndScrollView();
		}

		void DrawSearchGUI() {
			help.Draw("Search for a GUID (or any string at all) in all ascii files in a directory. Searches the Assets directory by default", MessageType.Info, false, 10f);

			DrawInputForms();

			GUILayout.Space(6f);

			searchPathType = (SearchPath)EditorGUILayout.EnumPopup("Search Path", searchPathType);

			if (searchPathType == SearchPath.Custom) {
				GUILayout.BeginHorizontal();
				searchCustomPath = EditorGUILayout.TextField(searchCustomPath);
				// searchCustomPath = EditorGUILayout.TextField("Search Path", searchCustomPath);
				if (GUILayout.Button("...", EditorStyles.miniButton, GUILayout.ExpandWidth(false))) {
					searchCustomPath = EditorUtility.OpenFolderPanel("Choose search directory", "", searchCustomPath);
					searchCustomPath = GUIDUtils.GetRelativeAssetsPath(searchCustomPath);
					GUI.FocusControl("guidInputText");
				}
				GUILayout.EndHorizontal();

				if (!GUIDUtils.IsAssetsPath(searchCustomPath)) {
					help.Draw("Searching outside the project or across multiple projects may take longer", MessageType.Warning, true);
				}
			}
			
			GUILayout.Space(10f);
			
			if (search != null && !string.IsNullOrEmpty(search.searchStr)) {

				// draw 'Searched for...' for box
				GUIStyle boxStyle = new GUIStyle("box");
				boxStyle.normal.background = darkBGTexture;
				GUILayout.BeginVertical(boxStyle);

				if (GUIDUtils.IsValidGUID(search.searchStr)) {
					GUILayout.Label("Searched for references of:");
					// probably a guid
					if (searchInfoItem == null) {
						searchInfoItem = new InfoItem(search.searchStr);
						searchInfoItem.showCopyButton = false;
						searchInfoItem.showSearchButton = false;
						searchInfoItem.showDeleteButton = false;
					}
					if (searchInfoItem.guid != search.searchStr) {
						searchInfoItem.guid = search.searchStr;
					}
					searchInfoItem.Draw(StartSearch, null, false);
				} else {
					// not a searching for a guid
					searchInfoItem = null;
					GUILayout.Label(string.Format("Searched for '{0}'", search.searchStr));
				}

				GUILayout.EndVertical();

				GUILayout.Space(10f);

				if (search.finished) {
					searchScrollPos = EditorGUILayout.BeginScrollView(searchScrollPos);
					for (int i = 0; i < matchItems.Count; i++) {
						matchItems[i].Draw(this);
					}
					EditorGUILayout.EndScrollView();
				}
			}
		}
		
		public void StartSearch(string text) {
			if (!string.IsNullOrEmpty(text)) {
				AddInfoItem(text);
				RecordSearch(text);
				StartSearchInternal(text);
			}
		}

		void StartSearchInternal(string text) {
			matchItems.Clear();
			GUI.FocusControl("guidInputObject");
			guidInputText = text;
			selectedTab = Tab.Search;
			search = new FileContentSearch(text, searchPath);
			// not using async here because certain unity editor
			// functions must be called on main thread
			search.Start(false);
		}

		/// <summary>
		/// A new search has been performed, record it in the searches stack
		/// </summary>
		void RecordSearch(string text) {
			if (string.IsNullOrEmpty(text)) {
				return;
			}
			if (searches.Count > 0 && searches[searches.Count - 1] == text) {
				return;
			}
			previousSearchIndex = 0;
			searches.Add(text);
		}

		/// <summary>
		/// Re-search for the last string that was searched for
		/// pops the current search from the list of previous searches
		/// </summary>
		public void GoToPreviousSearch() {
			if (searches.Count == 0) {
				return;
			}
			int newIndex = Mathf.Min(searches.Count - 1, previousSearchIndex + 1);
			if (previousSearchIndex != newIndex) {
				previousSearchIndex = newIndex;
				StartPreviousSearch();
			}
		}

		/// <summary>
		/// Re-search a previous search in the previous searches stack
		/// </summary>
		public void GoToNextSearch() {
			if (searches.Count == 0) {
				return;
			}
			int newIndex = Mathf.Max(0, previousSearchIndex - 1);
			if (previousSearchIndex != newIndex) {
				previousSearchIndex = newIndex;
				StartPreviousSearch();
			}
		}

		void StartPreviousSearch() {
			if (previousSearchIndex >= 0 && previousSearchIndex < searches.Count) {
				StartSearchInternal(searches[searches.Count - 1 - previousSearchIndex]);
			}
		}

		void OnSearchFinish(FileContentSearch srch) {
			matchItems.Clear();
			if (srch == search) {
				// update matches list
				for (int i = 0; i < search.matches.Count; i++) {
					matchItems.Add(new MatchItem(search.matches[i]));
				}
			}
			matchItems.Sort((a,b) => a.path.CompareTo(b.path));
			Repaint();
		}

		/// <summary>
		/// Given a list of objects, return a list of all that are not used anywhere in the project
		/// </summary>
		public static Object[] GetUnused(Object[] objects) {
			List<Object> filtered = new List<Object>();
			foreach (Object obj in objects) {
				FileContentSearch search = new FileContentSearch(GUIDUtils.ObjectToGUID(obj), searchPath);
				search.Start(false);
				if (search.matches.Count <= 1) {
					// only a single matched file means it was just the .meta file
					filtered.Add(obj);
				}
			}
			return filtered.ToArray();
		}

		/// <summary>
		/// Given a list of objects, select the ones that are not used anywhere in the project
		/// </summary>
		public static void SelectUnused(Object[] objects) {
			Object[] objs = GetUnused(objects);
			Selection.objects = objs;
		}

	}

}