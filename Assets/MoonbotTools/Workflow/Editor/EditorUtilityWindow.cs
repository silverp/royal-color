﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class EditorUtilityWindow : EditorWindow
{
    private int currentFileIndex = 0;
    private string startProcessingFile = "";
    private float progress = 0f;
    private string[] processingFiles = null;
    private bool running = false;
    private int resolvedFiles = 0;
    [MenuItem("Tools/Assets Fixer")]
    static void Init()
    {
        UnityEditor.EditorWindow window = GetWindow(typeof(EditorUtilityWindow), false, "Unknown name fixer");
        window.Show();
    }

    private static string CACHE_FILE = @"D:\Games\Unity\Projects\Animations\cache_full.txt";
    private static Dictionary<string, string> idcache = new Dictionary<string, string>();

    private static string animationPath = @"Assets";

    static Dictionary<string, string> Read(string file)
    {
        var result = new Dictionary<string, string>();
        //using (FileStream fs = File.OpenRead(file))
        //using (StreamReader reader = new StreamReader(fs))
        var lines = File.ReadAllLines(file);
        foreach (var str in lines)
        {
            // Get count.
            //int count = int.Parse(reader.ReadLine());
            // Read in all pairs.
            //for (int i = 0; i < count; i++)
            {
                var line = str.Split('=');
                if (line.Length == 2)
                {
                    string key = line[0];
                    string value = line[1];
                    result[key] = value;
                }
            }
        }
        return result;
    } 
    void FixAnimationUnknownNames(string file)
    { 
        try
        {
            Debug.Log($"{currentFileIndex + 1}/{processingFiles.Length} Processing file: {file}");
            startProcessingFile = file;
            var content = File.ReadAllText(file);
            bool obsolete = false;
            foreach (var kv in idcache)
            {
                if ((content.Contains(kv.Key)))
                {
                    obsolete = true;
                    content = content.Replace(kv.Key, kv.Value);
                }
            }
            if (content.Contains("unknown_") || content.Contains("Unknown_"))
            {
                obsolete = true;
            }
            if (obsolete)
            {
                content = content.Replace("unknown_", "").Replace("Unknown_", ""); 

                File.WriteAllText(file, content);
                resolvedFiles++;
            }
            if (running)
            {
                currentFileIndex++;
                Repaint();
                DoWork();
            }
        }
        catch(Exception ex)
        {
            Debug.LogError(ex);
            running = false;
            OnFinishProcessing();
            Repaint();
        } 
    }
    void Initialize()
    {
        idcache = Read(CACHE_FILE);
        Debug.LogWarning("Number of id caches " + idcache.Count);
        //startProcessingFile = "";
        resolvedFiles = 0;
        currentFileIndex = 0;
        processingFiles = Directory.GetFiles(animationPath, "*.anim", SearchOption.AllDirectories);
        //processingFiles = processingFiles.Take(50).ToArray();
        Repaint();

        Debug.LogWarning("Assets initalized, processing files " + processingFiles.Length);
    }
    void OnGUI()
    {
        if (!running)
        {
            if (GUILayout.Button("Find all relevant assets"))
            {
                Initialize();
            }
        }
        if (processingFiles == null)
        {
            // do not show GUI when not initialized yet.
        }
        else
        {
            //Debug.LogWarning("Assets initalized " + processingFiles.Length);
            startProcessingFile = EditorGUILayout.TextField($"Start file: ", startProcessingFile);
            EditorGUILayout.Separator();
            EditorGUILayout.LabelField($"Number of processing files: {processingFiles.Length}");
            EditorGUILayout.LabelField($"Processed files: {currentFileIndex}");
            EditorGUILayout.LabelField($"Resolved files: {resolvedFiles}");

            if (processingFiles.Length > 0)
            {
                if (!running)
                {
                    if (GUILayout.Button("Fix animations unknown names"))
                    {
                        running = true;
                        if (!string.IsNullOrEmpty(startProcessingFile))
                        {
                            startProcessingFile = startProcessingFile.Trim();
                            currentFileIndex = processingFiles.ToList().IndexOf(startProcessingFile);
                        } 
                        if (currentFileIndex < 0)
                            currentFileIndex = 0;
                        DoWork();
                        Repaint();
                    }
                }
                else
                {
                    if (GUILayout.Button("STOP"))
                    {
                        OnFinishProcessing();
                        Repaint(); 
                    }
                }
                progress = currentFileIndex / (float)processingFiles.Length;
                if (running && currentFileIndex < processingFiles.Length)
                    EditorUtility.DisplayProgressBar($"Processed files: {currentFileIndex}/{processingFiles.Length}", processingFiles[currentFileIndex], progress);
                else
                {
                    EditorUtility.ClearProgressBar();
                    if (currentFileIndex > 0)
                        EditorUtility.DisplayDialog("Information", $"DONE. Processed files: {currentFileIndex}", "OK"); 
                    currentFileIndex = 0;
                }
            }
        }
    }
    void OnFinishProcessing()
    { 
        running = false;
        Debug.LogWarning($"Number of processed files: {currentFileIndex}");
    }
    void DoWork()
    {
        if (running)
        {
            if (currentFileIndex < processingFiles.Length)
            {
                EditorCoroutines.Execute(() =>
                {
                    FixAnimationUnknownNames(processingFiles[currentFileIndex]);
                    return 0;
                }, (b) =>
                {
                    //Repaint();
                });
                
                //Task.Run(() =>
                //{
                //    FixAnimationUnknownNames(processingFiles[currentFileIndex]);
                //});
            }
            else
            {
                OnFinishProcessing();
            }
        }
    }

    void OnInspectorUpdate()
    {
        // Call Repaint on OnInspectorUpdate as it repaints the windows
        // less times as if it was OnGUI/Update
        Repaint();
    }
}