using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using Unity.VisualScripting;
using System.Reflection;
using TMPro;
//using Spine.Unity;
//using Spine.Unity.Editor;
//using I2.Loc.SimpleJSON;

namespace Moonbot.Workflow {

	/// <summary>
	/// A collection of prefab editing shortcuts that create
	/// a nicer workflow for editing prefabs in scene, and
	/// applying and reverting changes quickly. The main workflow
	/// is to EditSelected (%e), make changes, ApplyPrefabs (%&a),
	/// then DestroyPrefabEditingGroup (%#e)
	/// </summary>
	public class EditAnimations : Editor {

        static Dictionary<string, string> Read(string file)
        {
            var result = new Dictionary<string, string>();
            var lines = File.ReadAllLines(file);
            foreach (var str in lines)
            {
                var line = str.Split('=');
                if (line.Length == 2)
                {
                    string key = line[0];
                    string value = line[1];
                    result[key] = value;
                }
            }
            return result;
        }
        private static Type[] GetTypesInNamespace(Type type)
        {
            return
              Assembly.GetAssembly(type).GetTypes()
                      //.Where(t => string.Equals(t.Namespace, nameSpace, StringComparison.Ordinal))
                      .ToArray();
        }
        private readonly static string CACHE_FILE = @"D:\Games\Unity\Projects\Animations\cache_full.txt";
        [MenuItem("Tools/Create animation caches again", false, 1)]
        public static void CreateCacheAgain()
        {
            var needExpandTypes = new Type[]
            {
                typeof(Vector2),
                typeof(Vector3),
                typeof(Vector4),
                typeof(Color),
                typeof(Color32) 
            };
            int count = 0;
            var idcache = Read(CACHE_FILE);
            foreach (var type in new Type[]
            {
                typeof(TMP_Text),
                typeof(Image),
            })
            {
                var newTypes = GetTypesInNamespace(type).Where(t => t.IsClass).ToList();
                foreach (var t in newTypes)
                {
                    var serializedFields = t.GetFields(BindingFlags.NonPublic |
                             BindingFlags.Instance).Where(f => f.IsDefined(typeof(SerializeField))).ToList();
                    if (serializedFields.Count > 0)
                    {
                        Debug.LogWarning($"Fixing database for {t.FullName}");
                        foreach (var field in serializedFields)
                        {
                            Debug.LogWarning(field.Name);
                            var id = ConvertPropertyNameToID(field.Name);

                            if (!idcache.ContainsKey(id))
                            {
                                Debug.LogWarning($"{field.Name} does not exist in database");
                                idcache[id] = field.Name;
                                count++;
                            }
                            foreach(var nf in needExpandTypes)
                            {
                                if (field.FieldType == nf)
                                {
                                    Debug.LogWarning("Fixing for inner fields of " + nf.Name);
                                    var innerFields = nf.GetFields(BindingFlags.Public | BindingFlags.Instance).ToList();
                                    foreach(var f in innerFields)
                                    {
                                        var newName = field.Name + "." + f.Name;
                                        id = ConvertPropertyNameToID(newName);

                                        if (!idcache.ContainsKey(id))
                                        {
                                            Debug.LogWarning($"{newName} does not exist in database");
                                            idcache[id] = newName;
                                            count++;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (count > 0)
            {
                File.Delete(CACHE_FILE);
                var newLines = idcache.Select(t => $"{t.Key}={t.Value}").ToArray();
                File.WriteAllLines(CACHE_FILE, newLines);
            }

            Debug.LogWarning("DONE: " + count);
        }
		/// <summary>
		/// Name of the group that will be created when editing prefabs
		/// </summary>
		private const string nodeName = "PrefabEditingGroup";
        static AnimationClip GetClipByIndex(Animation animation, int index)
        {
            int i = 0;
            foreach (AnimationState animationState in animation)
            {
                if (i == index)
                    return animationState.clip;
                i++;
            }
            return null;
        }
        static void MoveAsset(string dest, string file)
        {
            if (Path.GetDirectoryName(file).Equals(new DirectoryInfo(dest).FullName, StringComparison.OrdinalIgnoreCase))
            {
                Debug.Log("SAME FOLDER!!!");
                return;
            }
            var name = Path.GetFileName(file);
            //var destName = Path.Combine(dest, name);
            int i = 1; 
            string tempDest = dest;
            while(File.Exists(Path.Combine(tempDest, name)))
            {
                //Debug.LogError("FILE EXISTS");
                //return;
                tempDest = Path.Combine(dest, Path.GetFileNameWithoutExtension(name) + "_ver" + i);
                i++;
            }
            dest = tempDest;
            if (!Directory.Exists(dest))
                Directory.CreateDirectory(dest);
            if (File.Exists(file))
                File.Move(file, Path.Combine(dest, name));
            if (File.Exists(file + ".meta"))
                File.Move(file + ".meta", Path.Combine(dest, name + ".meta"));

            Debug.Log($"Moved {file} to {dest}");
        }
        static void MoveSpineData(string dest, string folder = null)
        {
            //if (folder == null)
            //    folder = "Assets";
            ////foreach (var f in Directory.GetDirectories(folder))
            ////{
            ////    if (!f.Contains("Assets/SpineData"))
            ////        MoveSpineData(dest, f);
            ////}
            //foreach (var f in Directory.GetFiles(folder, "*_SkeletonData.asset"))
            //{
            //    var spine = AssetDatabase.LoadAssetAtPath<SkeletonDataAsset>(f);
            //    if (spine != null)
            //    {
            //        var parent = dest;// Path.GetDirectoryName(f);
            //        Debug.Log($"Reading spine from {f}"); 
            //        if (spine.skeletonJSON != null)
            //        {
            //            var jsonPath = AssetDatabase.GetAssetPath(spine.skeletonJSON);
            //            Debug.Log($"JSON path: {jsonPath}");
            //            MoveAsset(parent, jsonPath);
            //        }
            //        if (spine.atlasAssets != null)
            //        {
            //            foreach (var a in spine.atlasAssets.Where(t => t != null))
            //            {
            //                var atlas = AssetDatabase.GetAssetPath(a);
            //                if (atlas != null)
            //                {
            //                    var atlasAsset = AssetDatabase.LoadAssetAtPath<AtlasAsset>(atlas);
            //                    if (atlasAsset)
            //                    {
            //                        Debug.Log($"ATLAS path: {atlas}");
            //                        var atlasFile = AssetDatabase.GetAssetPath(atlasAsset.atlasFile);
            //                        MoveAsset(parent, atlasFile);
            //                        foreach (var m in atlasAsset.materials)
            //                        {
            //                            var mpath = AssetDatabase.GetAssetPath(m);
            //                            if (mpath != null)
            //                            {
            //                                var mat = AssetDatabase.LoadAssetAtPath<Material>(mpath);
            //                                if (mat && mat.mainTexture)
            //                                {
            //                                    var texture = AssetDatabase.GetAssetPath(mat.mainTexture);
            //                                    if (texture != null)
            //                                    {
            //                                        TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(texture);
            //                                        if (importer.alphaIsTransparency)
            //                                        {
            //                                            importer.alphaIsTransparency = false;
            //                                            EditorUtility.SetDirty(importer);
            //                                            importer.SaveAndReimport();
            //                                        }
            //                                        MoveAsset(parent, texture);
            //                                    }
            //                                }
            //                                MoveAsset(parent, mpath);
            //                            }
            //                        }
            //                        MoveAsset(parent, atlas);
            //                    }
            //                }
            //            }
            //        }

            //        MoveAsset(parent, f);
            //    }
            //}

            //foreach (var f in Directory.GetDirectories(folder))
            //{
            //    if (!f.Contains("Assets/SpineData"))
            //    { 
            //        MoveSpineData(dest, f);
            //    }
            //}
        }

        [MenuItem("Tools/Classify Spine Data", false, 2000)]
        static void ClassifySpineData()
        {
            var dest = "Assets/SpineData";
            if (!Directory.Exists(dest))    
                Directory.CreateDirectory(dest);

            MoveSpineData(dest);
            AssetDatabase.Refresh();

            Debug.Log("DONE DONE DONE");
        }
        [MenuItem("Tools/Delete Empty Folders", false, 2000)]
        static void DeleteEmptyFolders()
        {
            var dest = "Assets";

            foreach (var f in Directory.GetDirectories(dest, "*", SearchOption.AllDirectories))
            {
                if (Directory.GetFiles(f).Length == 0 && Directory.GetDirectories(f).Length == 0)
                {
                    Directory.Delete(f, false);
                    Debug.Log($"Deleted {f}");
                    if (File.Exists(f + ".meta"))
                        File.Delete(f + ".meta");
                }
            }

            AssetDatabase.Refresh();
            Debug.Log("DONE DONE DONE");
        }
        [MenuItem("Assets/Fix Animation %&m", false, 2000)]
        static void FixAnimation()
        {
            GameObject[] objects = Selection.gameObjects; 

            int count = 0;
            foreach (GameObject go in objects)
            {
                var animator = go.GetComponent<Animator>();
                if (animator != null)
                {
                    var animations = new List<AnimationClip>();
                    if (animator.runtimeAnimatorController != null && animator.runtimeAnimatorController.animationClips != null)
                        animations.AddRange(animator.runtimeAnimatorController.animationClips);
                     
                    count += FixAnimationClip(go, animations);
                }
            }
            foreach (GameObject go in objects)
            {
                var animation = go.GetComponent<Animation>();
                if (animation != null)
                {
                    var animations = new List<AnimationClip>();
                    if (animation.GetClipCount() > 0)
                    {
                        for (int i = 0; i < animation.GetClipCount(); i++)
                        {
                            var a = GetClipByIndex(animation, i);
                            if (a != null)
                                animations.Add(a);
                        }
                    }
                    else
                    {
                        animations.Add(animation.clip);
                    }

                    count += FixAnimationClip(go, animations);

                    //uint b = 0;
                    //var bindings = new Dictionary<EditorCurveBinding, AnimationCurve>();
                    //foreach (var a in animations)
                    //{
                    //    var curves = AnimationUtility.GetCurveBindings(a);
                    //    foreach (var c in curves)
                    //    {
                    //        var editorCurveBinding = c;
                    //        var animationCurve = AnimationUtility.GetEditorCurve(a, c);
                    //        if (uint.TryParse(c.path, out b) && b > 0)
                    //        {
                    //            var fixedPath = FindAllRelativePath(go.transform, c.path);
                    //            if (fixedPath != string.Empty)
                    //            {
                    //                editorCurveBinding.path = fixedPath;
                    //            }
                    //        }
                    //        bindings[editorCurveBinding] = animationCurve;
                    //    }

                    //    a.ClearCurves();
                    //    foreach (var item in bindings)
                    //    {
                    //        AnimationUtility.SetEditorCurve(a, item.Key, item.Value);
                    //    }

                    //    count++;
                    //    Debug.LogWarning("Fixed animation " + a.name);
                    //}
                }
            }
            Debug.LogWarning("Total number = " + count);
        }

        private static int FixAnimationClip(GameObject go, List<AnimationClip> animations)
        {
            int count = 0;
            //uint b = 0;
            var bindings = new Dictionary<EditorCurveBinding, AnimationCurve>();
            foreach (var a in animations)
            {
                if (a != null)
                {
                    var curves = AnimationUtility.GetCurveBindings(a);
                    foreach (var c in curves)
                    {
                        var editorCurveBinding = c;
                        var animationCurve = AnimationUtility.GetEditorCurve(a, c);
                        uint b;
                        if (uint.TryParse(c.path, out b) && b > 0)
                        {
                            var fixedPath = FindAllRelativePath(go.transform, c.path);
                            if (fixedPath != string.Empty)
                            {
                                editorCurveBinding.path = fixedPath;
                            }
                        }
                        bindings[editorCurveBinding] = animationCurve;
                    }

                    if (curves.Length > 0)
                    {
                        a.ClearCurves();
                        foreach (var item in bindings)
                        {
                            AnimationUtility.SetEditorCurve(a, item.Key, item.Value);
                        }

                        count++;
                        Debug.LogWarning("Fixed animation " + a.name);
                    }
                }
            }

            return count;
        }

        private static string FindAllRelativePath(Transform root, string hashId)
        {
            var r = FindRelativePath(root, hashId);
            if (r == string.Empty)
            {
                for (int i = 0; i < root.childCount; i++)
                {
                    var t = root.GetChild(i);
                    var p = FindRelativePath(t, hashId);
                    string pid = ConvertPropertyNameToID(p);
                    if (pid == hashId)
                    {
                        return p;
                    }
                }
            }
            return r;
        }
        private static string FindRelativePath(Transform root, string hashId)
        {
            return FindRelativePath(root, root, hashId);
        }
        private static string FindRelativePath(Transform root, Transform child, string hashId)
        {
            string path = AnimationUtility.CalculateTransformPath(child, root);
            string id = ConvertPropertyNameToID(path);
            if (id == hashId)
            {
                return path;
            }
            else
            {
                for (int i = 0; i < child.childCount; i++)
                {
                    var t = child.GetChild(i);
                    var p = FindRelativePath(root, t, hashId);
                    string pid = ConvertPropertyNameToID(p);
                    if (pid == hashId)
                    {
                        return p;
                    }
                }
            }
            return string.Empty;
        }

        private static string ConvertPropertyNameToID(string name)
        {
            return unchecked((uint)Animator.StringToHash(name)).ToString();
        }

        static void FixAnimation(GameObject go)
        {
            if (go)
            {
                var animator = go.GetComponent<Animator>();
                if (animator != null)
                {
                    var animations = new List<AnimationClip>();
                    if (animator.runtimeAnimatorController != null && animator.runtimeAnimatorController.animationClips != null)
                        animations.AddRange(animator.runtimeAnimatorController.animationClips);

                    FixAnimationClip(go, animations);
                }
                var animation = go.GetComponent<Animation>();
                if (animation != null)
                {
                    var animations = new List<AnimationClip>();
                    if (animation.GetClipCount() > 0)
                    {
                        for (int i = 0; i < animation.GetClipCount(); i++)
                        {
                            var a = GetClipByIndex(animation, i);
                            if (a != null)
                                animations.Add(a);
                        }
                    }
                    else
                    {
                        animations.Add(animation.clip);
                    }

                    FixAnimationClip(go, animations);
                }
            }
        }

        static void FixParticleSystem(GameObject go, ref int count)
        {
            if (go)
            {
                var ps = go.GetComponent<ParticleSystem>();
                if (ps != null)
                {
                    if (float.IsNaN(ps.main.startDelayMultiplier))
                    {
                        var m = ps.main;
                        m.startDelayMultiplier = 0;

                        Debug.LogWarning("Fixed particle system of " + ps.name);
                        count++;
                    }
                }
            }
        }

        static void FixRawImage(GameObject go, ref int count)
        {
            var ri = go.GetComponent<RawImage>();
            if (ri != null)
            {
                var rect = ri.uvRect;
                if (rect.width == 0)
                {
                    rect.width = 1;
                    Debug.LogWarning($"Fixed width of {go.name}");
                    count++;
                }
                if (rect.height == 0)
                {
                    rect.height = 1;
                    Debug.LogWarning($"Fixed height of {go.name}");
                }
                ri.uvRect = rect;
            }
        }

        static void FixTexture(GameObject go, ref int count)
        {
            //var ri = go.GetComponent<UITexture>();
            //if (ri != null)
            //{
            //    var rect = ri.uvRect;
            //    if (rect.width == 0)
            //    {
            //        rect.width = 1;
            //        Debug.LogWarning($"Fixed width of {go.name}");
            //        count++;
            //    }
            //    if (rect.height == 0)
            //    {
            //        rect.height = 1;
            //        Debug.LogWarning($"Fixed height of {go.name}");
            //    }
            //    ri.uvRect = rect;
            //}
        }

        static void FixObject(GameObject go, ref int count)
        {
            if (go)
            {
                FixAnimation(go);
                FixParticleSystem(go, ref count);
                FixRawImage(go, ref count);
                FixTexture(go, ref count);

                foreach (Transform t in go.transform)
                {
                    if (t != null)
                    {
                        FixObject(t.gameObject, ref count);
                    }
                }
            }
        }

        static int FixAllPrefabs_issueCount = 0;
        static int FixAllPrefabs_prefabCount = 0;  
        [MenuItem("Tools/Fix all prefabs %&f", false, 3000)]
        static void FixAllPrefabs()
        { 
            var prefabs = Directory.GetFiles("Assets/", "*.prefab", SearchOption.AllDirectories);

            //prefabs = new string[] { "Assets/Resources/unpack/ban/NewDragonBG.prefab" };
            //prefabs = prefabs.Take(20).ToArray(); 
            FixAllPrefabs_issueCount = 0;
            FixAllPrefabs_prefabCount = 0;

            var stackedPrefabs = new Stack<string>(prefabs);

            Debug.LogWarning($"Number of prefabs to be processed: {prefabs.Length}");
            EditorCoroutines.Execute(() => FixPrefabObject(stackedPrefabs, prefabs.Length), (done) =>
            {
                if (done)
                {
                    Debug.LogWarning("---------------------------------------------");
                    Debug.LogWarning("DONE. Number of resolved issues: " + FixAllPrefabs_issueCount);
                }
            });
        }
        static int FixAllScenes_issueCount = 0;
        static int FixAllScenes_sceneCount = 0;
        [MenuItem("Tools/Fix all scenes %&m", false, 3000)]
        static void FixAllScenes()
        {
            var scenes = Directory.GetFiles("Assets/", "*.unity", SearchOption.AllDirectories);
             
            FixAllScenes_issueCount = 0;
            FixAllScenes_sceneCount = 0;

            var stackedScenes = new Stack<string>(scenes);

            Debug.LogWarning($"Number of scenes to be processed: {scenes.Length}");
            EditorCoroutines.Execute(() => FixSceneObject(stackedScenes, scenes.Length), (done) =>
            {
                if (done)
                {
                    Debug.LogWarning("---------------------------------------------");
                    Debug.LogWarning("DONE. Number of resolved issues: " + FixAllScenes_issueCount);
                }
            }, 0.5f);
        }
        static object FixPrefabObject(Stack<string> stackedPrefabs, int total)
        {
            if (stackedPrefabs.Count > 0)
            {
                var file = stackedPrefabs.Pop();
                FixPrefab(file, total);
                if (stackedPrefabs.Count > 0)
                {
                    return EditorCoroutines.WrapAction(() => FixPrefabObject(stackedPrefabs, total));
                }
            }
            return 0;
        }
        static object FixSceneObject(Stack<string> stackedScenes, int total)
        {
            if (stackedScenes.Count > 0)
            {
                var file = stackedScenes.Pop();
                Debug.Log(file);
                //var sceneName = Path.GetFileNameWithoutExtension(file);
                var scene = EditorSceneManager.OpenScene(file);
                if (scene != null)
                {
                    bool isFirst = true;
                    var objects = scene.GetRootGameObjects();
                    foreach (var f in objects)
                    {
                        Debug.Log(f);
                        FixObject((GameObject)f, ref FixAllScenes_sceneCount);
                        if (isFirst)
                        {
                            isFirst = false;
                        }
                    }
                    EditorSceneManager.MarkSceneDirty(scene);
                    EditorSceneManager.SaveScene(scene);
                    EditorSceneManager.CloseScene(scene, true);
                }
                FixAllScenes_sceneCount++;  
                Debug.Log($"Processed prefabs: {FixAllScenes_sceneCount}/{total}");
                if (stackedScenes.Count > 0)
                {
                    return EditorCoroutines.WrapAction(() => FixSceneObject(stackedScenes, total), 0.5f);
                }
            }
            return 0;
        }
        static void FixPrefab(string file, int total)
        {
            Debug.Log(file);
            var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(file);
            if (prefab)
            {
                FixObject(prefab, ref FixAllPrefabs_issueCount);
                FixAllPrefabs_prefabCount++;
                EditorUtility.SetDirty(prefab);
                AssetDatabase.SaveAssets();
            }
            Debug.Log($"Processed prefabs: {FixAllPrefabs_prefabCount}/{total}");
        }

        [MenuItem("Tools/Fix all in Scene %&q", false, 3000)]
        static void FixAllInCurrentScene()
        {
            int count = 0;
            foreach (var f in UnityEngine.Object.FindObjectsOfType(typeof(GameObject)))
            {
                if (f is GameObject)
                {
                    Debug.Log(f);
                    FixObject((GameObject)f, ref count);
                }
            }

            Debug.LogWarning("Number resolved objects = " + count);
        }
        public static List<T> FindAssetsByType<T>() where T : UnityEngine.Object
        {
            List<T> assets = new List<T>();
            string[] guids = AssetDatabase.FindAssets(string.Format("t:{0}", typeof(T)));
            for (int i = 0; i < guids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);
                if (asset != null)
                {
                    assets.Add(asset);
                }
            }
            return assets;
        }

        //[MenuItem("Tools/Find shader variant collection", false, 3000)]
        //static void FindShaderVariantCollection()
        //{
        //    var shaderCollections = AssetDatabase.FindAssets("t:prefab");
        //    int count = 0;
        //    foreach (var s in shaderCollections)
        //    {
        //        var path = AssetDatabase.GUIDToAssetPath(s);
        //        var o = AssetDatabase.LoadAssetAtPath(path, typeof(ShaderVariantCollection));
        //        if (o != null)
        //        {
        //            Debug.LogWarning(path);
        //            count++;
        //        }
        //    }

        //    Debug.LogWarning("Number count = " + count);
        //}

        [MenuItem("Tools/Create and split script folders", false, 1000)]
        static void CreateSplitScriptFolders()
        {
            var map = new Dictionary<string, string>();
            foreach (var folder in Directory.GetDirectories("Assets/Scripts"))
            {
                var parent = Path.GetDirectoryName(folder);
                var name = Path.GetFileName(folder);
                
                if (name.Contains("_") && !name.StartsWith("_"))
                {
                    if (name.StartsWith("Assets_Scripts_"))
                    {
                        name = name.Substring("Assets_Scripts_".Length);
                    }
                    var destFolder = Path.Combine(parent, name.Replace("_", "/"));
                    Debug.Log($"Changed from {name} to {destFolder}");
                    if (!Directory.Exists(destFolder))
                    {
                        Directory.CreateDirectory(destFolder);
                        //Directory.Delete(destFolder);
                        //Directory.Delete(folder);
                    }
                    map[folder] = destFolder;
                }
            }

            AssetDatabase.Refresh();
            foreach(var f in map.Keys)
            {
                var dest = map[f];

                MoveFiles(f, dest);
            }
            foreach (var f in map.Keys)
            {
                if (Directory.GetFiles(f).Length == 0 && Directory.GetDirectories(f).Length == 0)
                {
                    Directory.Delete(f);
                }
            }
            AssetDatabase.Refresh();
        } 

        static void MoveFiles(string fromFolder, string destFolder)
        {
            try
            {
                foreach (var file in Directory.GetFiles(fromFolder))
                {
                    if (!Directory.Exists(destFolder))
                    {
                        Directory.CreateDirectory(destFolder);
                    }
                    FileUtil.MoveFileOrDirectory(file, Path.Combine(destFolder, Path.GetFileName(file)));
                }
                foreach (var folder in Directory.GetDirectories(fromFolder))
                {
                    var name = Path.GetFileName(folder);
                    MoveFiles(folder, Path.Combine(destFolder, name));
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        } 
    } 
}