using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Moonbot.Workflow {

	/// <summary>
	/// A QuickSelectWindow that filters the current selection.
	/// Routes the filtering directly into the selection, vs. using
	/// the standard list of items shown in the window.
	/// </summary>
	public class FilterSelection : QuickSelectWindow {

		public class TransformItem : QuickSelectItem {
			public Transform transform;
			public override string name {
				get { return transform.name; }
			}

			public GameObject gameObject {
				get { return transform.gameObject; }
			}

			public TransformItem(Transform transform) {
				this.transform = transform;
			}
		}
		
		[MenuItem("Edit/Filter Selection &#f", false, 2001)]
		static void Init() {
			var win = QuickSelectWindow.Create<FilterSelection>();
			win.showItems = false;
		}
		
		public override QuickSelectItem[] GetItems() {
			return System.Array.ConvertAll(Selection.GetTransforms(SelectionMode.Unfiltered), i => new TransformItem(i));
		}

		public override void OnEnterPressed() {
			Close();
		}

		public override void OnEscapePressed() {
			Selection.objects = System.Array.ConvertAll(items, i => ((TransformItem)i).gameObject);
			Close();
		}

		public override void UpdateFilteredItems() {
			base.UpdateFilteredItems();
			Selection.objects = System.Array.ConvertAll(filteredItems, i => ((TransformItem)i).gameObject);
		}

	}

}