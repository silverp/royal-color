using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


namespace Moonbot.Workflow {

	/// <summary>
	/// A HelpMessage manager that keeps a list of messages and provides
	/// some basic methods for adding messages of various types.
	/// Handles drawing the messages via EditorGUILayout.HelpBox
	/// </summary>
	public class HelpMessages {

		/// <summary>
		/// Simple class for storing a string and MessageType.
		/// Useful for gathering messages to be displayed in editor windows
		/// </summary>
		public class Message {

			/// <summary>
			/// The contents of the help message
			/// </summary>
			public string message;
			/// <summary>
			/// The type of the help message
			/// </summary>
			public MessageType type;
			/// <summary>
			/// Whether to always show this message, or only when 'showHelp' is enabled
			/// </summary>
			public bool alwaysShow;
			/// <summary>
			/// How much space to put after the help message
			/// </summary>
			public float space;

			public Message() {}
			public Message(string message, MessageType type = MessageType.None, bool alwaysShow = false, float space = 0f) {
				this.message = message;
				this.type = type;
				this.alwaysShow = alwaysShow;
				this.space = space;
			}

		}

		public List<Message> messages = new List<Message>();

		/// <summary>
		/// Whether help messages should be enabled or not.
		/// When this is false, drawing via this HelpMessages
		/// object will do nothing
		/// </summary>
		public bool enabled;

		/// <summary>
		/// Returns the length of the current list of Messages
		/// </summary>
		public int Count {
			get { return messages.Count; }
		}

		public Message this[int index] {
			get { return messages[index]; }
		}

		/// <summary>
		/// Clear all Messages in the messages list
		/// </summary>
		public void Clear() {
			messages.Clear();
		}

		/// <summary>
		/// Add a message to the messages list with the given MessageType
		/// </summary>
		public void Add(string message, MessageType type = MessageType.None, bool alwaysShow = false, float space = 0f) {
			messages.Add(new Message(message, type, alwaysShow, space));
		}

		/// <summary>
		/// Add a message to the messages list with the given MessageType
		/// </summary>
		public void Add(string message, bool alwaysShow = false, float space = 0f) {
			messages.Add(new Message(message, MessageType.None, alwaysShow, space));
		}

		/// <summary>
		/// Add a message to the messages list with a type of MessageType.Info
		/// </summary>
		public void AddInfo(string message, bool alwaysShow = false, float space = 0f) {
			Add(message, MessageType.Info, alwaysShow, space);
		}

		/// <summary>
		/// Add a message to the messages list with a type of MessageType.Warning
		/// </summary>
		public void AddWarning(string message, bool alwaysShow = false, float space = 0f) {
			Add(message, MessageType.Warning, alwaysShow, space);
		}

		/// <summary>
		/// Add a message to the messages list with a type of MessageType.Error
		/// </summary>
		public void AddError(string message, bool alwaysShow = false, float space = 0f) {
			Add(message, MessageType.Error, alwaysShow, space);
		}

		/// <summary>
		/// Draw all messages that have been added to the list
		/// </summary>
		public void DrawAll() {
			for (int i = 0; i < messages.Count; i++) {
				Draw(messages[i]);
			}
		}

		/// <summary>
		/// Draw all messages that have been added and
		/// that match the given MessageType
		/// </summary>
		/// <param name="type">The MessageType to draw</param>
		public void DrawAll(MessageType type) {
			for (int i = 0; i < messages.Count; i++) {
				if (messages[i].type == type) {
					Draw(messages[i]);
				}
			}
		}

		/// <summary>
		/// Draw a specific message with the default MessageType
		/// </summary>
		/// <param name="message">The message string to draw</param>
		/// <param name="alwaysShow">Whether this message should always be drawn, or only when this object is enabled</param>
		/// <param name="space">How much space to draw after the HelpBox</param>
		public void Draw(string message, bool alwaysShow, float space = 0f) {
			Draw(new Message(message, MessageType.None, alwaysShow, space));
		}

		/// <summary>
		/// Draw a specific message with the given type
		/// </summary>
		/// <param name="message">The message string to draw</param>
		/// <param name="type">The MessageType of the HelpBox to be displayed</param>
		/// <param name="alwaysShow">Whether this message should always be drawn, or only when this object is enabled</param>
		/// <param name="space">How much space to draw after the HelpBox</param>
		public void Draw(string message, MessageType type = MessageType.None, bool alwaysShow = false, float space = 0f) {
			Draw(new Message(message, type, alwaysShow, space));
		}

		/// <summary>
		/// Draw the given Message using EditorGUILayout.HelpBox
		/// </summary>
		public void Draw(Message message) {
			if (enabled || message.alwaysShow) {
				EditorGUILayout.HelpBox(message.message, message.type);
				if (message.space > 0) {
					GUILayout.Space(message.space);
				}
			}
		}

	}

}