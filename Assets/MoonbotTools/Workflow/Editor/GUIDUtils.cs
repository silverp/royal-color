using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;


namespace Moonbot.Workflow {

	/// <summary>
	/// Some basic utils for going between GUIDs and Objects
	/// </summary>
	public static class GUIDUtils {

		/// <summary>
		/// A regex pattern for matching GUIDs
		/// </summary>
		public const string guidPattern = "^[a-f0-9]{32}$";

		/// <summary>
		/// Returns whether the given string matches against the GUID regex pattern
		/// </summary>
		public static bool IsValidGUID(string guid) {
			return !string.IsNullOrEmpty(guid) && Regex.IsMatch(guid, guidPattern);
		}

		/// <summary>
		/// Return the GUID for this Object, must be an Asset
		/// </summary>
		public static string ObjectToGUID(Object obj) {
			string assetPath = AssetDatabase.GetAssetPath(obj);
			if (!string.IsNullOrEmpty(assetPath)) {
				return AssetDatabase.AssetPathToGUID(assetPath);
			}
			return "";
		}
		
		/// <summary>
		/// Return the Object in Assets for the given GUID
		/// </summary>
		public static Object GUIDToObject(string guid) {
			string assetPath = AssetDatabase.GUIDToAssetPath(guid);
			if (!string.IsNullOrEmpty(assetPath)) {
				return AssetDatabase.LoadMainAssetAtPath(assetPath);
			}
			return null;
		}

		/// <summary>
		/// Return the given path relative to the Application.dataPath
		/// Includes "Assets" as the first item of the path
		/// </summary>
		public static string GetRelativeAssetsPath(string path) {
			if (path.StartsWith(Application.dataPath)) {
				return "Assets" + path.Substring(Application.dataPath.Length);
			}
			return path;
		}

		/// <summary>
		/// Returns whether the given path is within the Assets
		/// directory of this project
		/// </summary>
		public static bool IsAssetsPath(string path) {
			return path.StartsWith("Assets") || path.StartsWith(Application.dataPath);
		}

	}

}
