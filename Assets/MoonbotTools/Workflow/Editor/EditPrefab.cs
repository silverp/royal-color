using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Moonbot.Workflow {

	/// <summary>
	/// A collection of prefab editing shortcuts that create
	/// a nicer workflow for editing prefabs in scene, and
	/// applying and reverting changes quickly. The main workflow
	/// is to EditSelected (%e), make changes, ApplyPrefabs (%&a),
	/// then DestroyPrefabEditingGroup (%#e)
	/// </summary>
	public class EditPrefab : Editor {

		/// <summary>
		/// Name of the group that will be created when editing prefabs
		/// </summary>
		private const string nodeName = "PrefabEditingGroup";

		/// <summary>
		/// Edit the selected prefabs by creating instances of them
		/// in the scene, grouped under the PrefabEditingGroup.
		/// The PrefabEditingGroup will always appear at the top of
		/// the hierarchy (4.5 and later)
		/// </summary>
		[MenuItem("Assets/Edit Prefab %e", false, 2000)]
		static void EditSelected() {
			GameObject[] prefabs = Selection.gameObjects;
			GameObject grp = GetOrCreateEditGroup();
			GameObject[] clones = new GameObject[prefabs.Length];
			for (int i = 0; i < prefabs.Length; i++) {
				clones[i] = PrefabUtility.InstantiatePrefab(prefabs[i]) as GameObject;
				clones[i].transform.parent = grp.transform;
				Undo.RegisterCreatedObjectUndo(clones[i], "Edit Prefabs");
			}
			Selection.objects = clones;
		}

		[MenuItem("Assets/Edit Prefab %e", true)]
		static bool EditSelectedValidate() {
			GameObject[] prefabs = Selection.gameObjects;
			if (prefabs.Length == 0) {
				return false;
			}
			for (int i = 0; i < prefabs.Length; i++) {
				if (PrefabUtility.GetPrefabType(prefabs[i]) != PrefabType.Prefab && PrefabUtility.GetPrefabType(prefabs[i]) != PrefabType.ModelPrefab) {
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// Apply any prefab modifications to the selected prefabs. Any and
		/// all nodes can be selected from within the modified prefabs.
		/// </summary>
		[MenuItem("Assets/Apply Prefabs %&a", false, 2000)]
		static void ApplyPrefabs() {
			GameObject[] objects = Selection.gameObjects;
			List<GameObject> roots = new List<GameObject>();
			foreach (GameObject go in objects) {
				PrefabType type = PrefabUtility.GetPrefabType(go);
				if (type == PrefabType.None || type == PrefabType.MissingPrefabInstance) {
					continue;
				}
				GameObject root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(go);
				if (root == null || roots.Contains(root)) {
					// already been applied
					continue;
				}
				PrefabUtility.ReplacePrefab(root, PrefabUtility.GetPrefabParent(root), ReplacePrefabOptions.ConnectToPrefab);
				Debug.Log("Applied changes to prefab: <b>" + root.name + "</b>", root);
				roots.Add(root);
			}
        }

        /// <summary>
        /// Revert any prefab modifications of the selected prefabs. Any and
        /// all nodes can be selected from within the modified prefabs.
        /// </summary>
        [MenuItem ("Assets/Revert Prefabs %&r", false, 2000)]
		static void RevertPrefabs() {
			GameObject[] objects = Selection.gameObjects;
			List<GameObject> roots = new List<GameObject>();
			if (objects.Length > 0) {
				for (int i = 0; i < objects.Length; i++) {
					PrefabType type = PrefabUtility.GetPrefabType(objects[i]);
					if (type == PrefabType.None) {
						continue;
					}
					GameObject root = PrefabUtility.FindRootGameObjectWithSameParentPrefab(objects[i]);
					if (root == null || roots.Contains(root)) {
						// already been reverted
						continue;
					}
					if (type == PrefabType.DisconnectedPrefabInstance || type == PrefabType.DisconnectedModelPrefabInstance) {
						PrefabUtility.ReconnectToLastPrefab(root);
					}
					if (PrefabUtility.RevertPrefabInstance(root)) {
						Debug.Log("Reverted prefab: <b>" + root.name + "</b>", root);
					}
					roots.Add(root);
				}
			}
		}

		[MenuItem("Assets/Apply Prefabs %&a", true)]
		[MenuItem("Assets/Revert Prefabs %&r", true)]
		static bool PrefabsSelectedValidate() {
			GameObject[] objects = Selection.gameObjects;
			foreach (GameObject go in objects) {
				PrefabType type = PrefabUtility.GetPrefabType(go);
				if (type != PrefabType.None && type != PrefabType.MissingPrefabInstance) {
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Destroy the PrefabEditingGroup and all its children if it exist in the scene.
		/// This is used to cleanup edited prefabs when finished. NOTE: this will NOT
		/// apply changes to the prefabs, remember to apply changes before running this
		/// </summary>
		[MenuItem("Assets/Destroy Prefab Editing Group %#e", false, 2000)]
		static void DestroyPrefabEditingGroup() {
			GameObject grp = GetEditGroup();
			if (grp != null) {
				Undo.DestroyObjectImmediate(grp);
			}
		}

		[MenuItem("Assets/Destroy Prefab Editing Group %#e", true)]
		static bool DestroyPrefabEditingGroupValidate() {
			GameObject grp = GetEditGroup();
			return grp != null;
		}

		/// <summary>
		/// Return the PrefabEditingGroup if it exists, returns null if it doesn't
		/// </summary>
		public static GameObject GetEditGroup() {
			return GameObject.Find(nodeName);
		}

		/// <summary>
		/// Return the PrefabEditingGroup. If it does not exist, it will be created.
		/// </summary>
		public static GameObject GetOrCreateEditGroup() {
			GameObject grp = GetEditGroup();
			if (grp == null) {
				grp = new GameObject(nodeName);
				#if UNITY_4_5 || UNITY_4_6
				grp.transform.SetSiblingIndex(0);
				#endif
				grp.AddComponent<PrefabEditingGroup>();
				Undo.RegisterCreatedObjectUndo(grp, "Edit Prefabs");
			}
			return grp;
		}

	}

}