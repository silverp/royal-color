using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace Moonbot.Workflow {

	/// <summary>
	/// A collection of selection related shortcuts
	/// </summary>
	public class SelectionTools {

		/// <summary>
		/// Select all children of the selected transforms.
		/// If a transform has no children, it will be deselected
		/// </summary>
		[MenuItem("Edit/Select all children #&c", false, 2001)]
		public static void SelectChildren() {
			Transform[] sel = Selection.GetTransforms(SelectionMode.Editable);
			List<Object> children = new List<Object>();
			foreach (Transform t in sel) {
				foreach (Transform c in t) {
					children.Add(c.gameObject as Object);
				}
			}
			Selection.objects = children.ToArray();
		}

		/// <summary>
		/// Select all parents of the selected transforms.
		/// If a tranfsorm has no parent, it will be deselected
		/// </summary>
		[MenuItem("Edit/Select Parents #&p", false, 2001)]
		public static void SelectParents() {
			Transform[] sel = Selection.GetTransforms(SelectionMode.Editable);
			List<Object> parents = new List<Object>();
			foreach (Transform t in sel) {
				if (t.parent != null) {
					parents.Add(t.parent.gameObject as Object);
				}
			}
			Selection.objects = parents.ToArray();
		}
	}

}