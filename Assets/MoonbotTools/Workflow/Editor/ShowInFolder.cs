using UnityEngine;
using UnityEditor;
using System.Collections;


namespace Moonbot.Workflow {

	/// <summary>
	/// A simple shortcut for showing the selected assets
	/// in Finder or Explorer (Windows). Simply executes the
	/// existing menu item found in the Assets menu.
	/// </summary>
	public class ShowInFolder : Editor {

		[MenuItem("Assets/Show in Folder #&s")]
		static void Command() {
			if (Application.platform == RuntimePlatform.OSXEditor) {
				EditorApplication.ExecuteMenuItem("Assets/Reveal in Finder");
			} else if (Application.platform == RuntimePlatform.WindowsEditor) {
				EditorApplication.ExecuteMenuItem("Assets/Show in Explorer");
			}
		}

	}

}