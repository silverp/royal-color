using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;


namespace Moonbot.Workflow {

	/// <summary>
	/// The base class for any popup-style windows used for
	/// quickly finding an item from a list. This is akin
	/// to launcher-style apps like Alfred for OSX.
	///
	/// Prevents more than one quick select window from
	/// being open at the same time.
	/// </summary>
	public abstract class QuickSelectWindow : EditorWindow {

		/// <summary>
		/// Base class for any items to be shown in the window
		/// When filtering by search, uses the name property
		/// </summary>
		public class QuickSelectItem {
			public virtual string name { get; set; }
		}


		private static QuickSelectWindow instance;

		/// <summary>
		/// Default width of the window
		/// </summary>
		private static int defaultWidth = 200;

		public static T Create<T>() where T : QuickSelectWindow {
			return QuickSelectWindow.Create<T>(defaultWidth);
		}

		public static T Create<T>(int width) where T : QuickSelectWindow {
			if (instance != null) {
				instance.Close();
			}
			T win = EditorWindow.CreateInstance<T>();
			// need to store target width for later, and initialize
			// size with 1, 1 so that its mostly hidden at first
			win.targetWidth = width;
			win.ShowAsDropDown(new Rect(0f, 0f, 0f, 0f), new Vector2(1f, 1f));
			// mark win as initialized so size updates can now run
			win.initialized = true;
			instance = win;
			return win;
		}

		public int targetWidth = defaultWidth;

		/// <summary>
		/// Maximum height of the window. Contents are
		/// put in a scroll view if the window
		/// is taller than this
		/// </summary>
		public int maxHeight = 800;

		/// <summary>
		/// Height of each button, so that the total
		/// height of the window can be determined
		/// </summary>
		public int rowHeight = 31;

		/// <summary>
		/// Whether to show items in the actual window or not.
		/// If false, assumes that the display of filtered
		/// items will be handled a different way
		/// </summary>
		private bool _showItems = true;
		public bool showItems {
			get { return _showItems; }
			set {
				if (_showItems != value) {
					_showItems = value;
					UpdateWindowSize();
				}
			}
		}

		/// <summary>
		/// Current list of items to potentially display
		/// </summary>
		protected QuickSelectItem[] _items;
		public QuickSelectItem[] items {
			get { return _items; }
			set {
				if (_items != value) {
					_items = value;
					UpdateFilteredItems();
				}
			}
		}

		/// <summary>
		/// Current list of items filtered by the search
		/// </summary>
		public QuickSelectItem[] filteredItems;

		/// <summary>
		/// Search string for finding items
		/// </summary>
		protected string _search = "";
		public string search {
			get { return _search; }
			set {
				if (_search != value) {
					_search = value;
					UpdateFilteredItems();
				}
			}
		}

		/// <summary>
		/// Returns the height of the search bar
		/// and title bar and anything extra that will
		/// be drawn besides the items themselves
		/// </summary>
		public int extrasHeight {
			get { return 32; }
		}

		/// <summary>
		/// Flag that gets set once the window has been placed and sized
		/// </summary>
		protected bool initialized;
		/// <summary>
		/// Flag used to determine if we've officially set the window position
		/// Needed to workaround some of the annoying ShowAsDropDown functionality
		/// </summary>
		protected bool hasSetPosition;

		/// <summary>
		/// Whether the contents of the window requires a scroll view.
		/// This happens when the list becomes taller than maxHeight
		/// </summary>
		protected bool needsScroll;

		/// <summary>
		/// Scroll position when window needs a scroll view
		/// </summary>
		protected Vector2 scrollPos;

		protected virtual void OnEnable() {
			LoadItems();
		}

		protected virtual void OnDestroy() {
			instance = null;
		}

		protected virtual void OnGUI() {

			// If needed, update position of window.
			// This happens continuously until fully initialized
			// OnGUI is called twice by ShowAsDropDown, and it resets
			// the position each time, so we need to keep doing this.
			if (initialized && !hasSetPosition) {
				UpdateWindowPosition();
				hasSetPosition = true;
			}
			
			if (Event.current.isKey) {
				if (Event.current.keyCode == KeyCode.Escape) {
					OnEscapePressed();
				}
				if (Event.current.keyCode == KeyCode.Return) {
					OnEnterPressed();
				}
			}
			EditorGUILayout.BeginVertical("box");

			// search field
			GUI.SetNextControlName("search");
			search = EditorGUILayout.TextField(search);
			if (!initialized) {
				EditorGUI.FocusTextInControl("search");
			}

			// only show items if requested
			if (showItems) {

				if (needsScroll) {
					scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, true);
				}

				// list filtered items
				foreach (QuickSelectItem s in filteredItems) {
					if (GUILayout.Button(s.name)) {
						SelectItem(s);
					}
				}

				if (needsScroll) {
					EditorGUILayout.EndScrollView();
				}
				EditorGUILayout.EndVertical();
			}

		}

		/// <summary>
		/// Moves the window to the current mouse position
		/// </summary>
		protected virtual void UpdateWindowPosition() {
			if (Event.current == null) {
				return;
			}
			UpdateWindowSize();
			// get position (its relative to this window's position)
			Vector2 pos = Event.current.mousePosition;
			// move window to be at mouse position
			position = new Rect(position.x + pos.x, position.y + pos.y, position.width, position.height);
		}

		/// <summary>
		/// Update size of the window based on current list of items
		/// </summary>
		protected virtual void UpdateWindowSize() {
			int height = GetWindowHeight();
			// clamp at a decent height, use a scroll bar if above this
			if (height > maxHeight) {
				needsScroll = true;
				height = maxHeight;
			} else {
				needsScroll = false;
			}
			position = new Rect(position.x, position.y, targetWidth, height);
		}

		protected virtual int GetWindowHeight() {
			// should be total extra height from borders, other forms, etc
			// border (12) + search (20)
			int height = extrasHeight;
			if (showItems) {
				height += filteredItems.Length * rowHeight;
			}
			return height;
		}

		/// <summary>
		/// Updates the current items list by assigning
		/// it to the result of GetItems
		/// </summary>
		public virtual void LoadItems() {
			items = GetItems();
		}

		/// <summary>
		/// Called on enable, should be overridden to return
		/// the relevant items for this window
		/// </summary>
		public virtual QuickSelectItem[] GetItems() {
			return new QuickSelectItem[]{};
		}

		/// <summary>
		/// Update the current filtered items list based
		/// on the current search pattern
		/// </summary>
		public virtual void UpdateFilteredItems() {
			Regex regex = new Regex(search.Replace(" ", ".*"), RegexOptions.IgnoreCase);
			filteredItems = items.Where(i => regex.IsMatch(i.name)).ToArray();
			UpdateWindowSize();
		}

		/// <summary>
		/// Called when enter is pressed in the search bar,
		/// By default this selects the first item in the filtered list
		/// </summary>
		public virtual void OnEnterPressed() {
			SelectItem(0);
		}

		/// <summary>
		/// Called when enter is pressed in the search bar,
		/// By default this selects the first item in the filtered list
		/// </summary>
		public virtual void OnEscapePressed() {
			Close();
		}

		/// <summary>
		/// Select the item at the given index in the current filteredItems array
		/// </summary>
		public virtual void SelectItem(int index) {
			if (index < filteredItems.Length) {
				SelectItem(filteredItems[index]);
			}
		}

		/// <summary>
		/// Select the given item. Override this to implement different functionality
		/// for when an item in the list is selected
		/// </summary>
		public virtual void SelectItem(QuickSelectItem item) {
			Close();
		}

	}

}