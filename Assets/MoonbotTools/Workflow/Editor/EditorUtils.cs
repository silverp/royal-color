using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

namespace Moonbot.Workflow
{

    /// <summary>
    /// A collection of various utils that can be used
    /// across any editor classes as desired
    /// </summary>
    public static class EditorUtils
    {

        /// <summary>
        /// Path to text editor to use when opening text assets.
        /// This is set to the subl shortcut by default.
        /// </summary>
        public const string textEditor = "/usr/local/bin/subl";

        /// <summary>
        /// Return a new 1x1 texture with the given color
        /// </summary>
        public static Texture2D GetSolidColorTexture(Color color)
        {
            Texture2D tex = new Texture2D(1, 1);
            tex.SetPixel(0, 0, color);
            tex.Apply();
            return tex;
        }

        /// <summary>
        /// Copy the given string to the clipboard
        /// </summary>
        public static void CopyStringToClipboard(string text)
        {
            TextEditor te = new TextEditor();
            te.content = new GUIContent(text);
            te.SelectAll();
            te.Copy();
        }

        /// <summary>
        /// Creates an array of mini button styles that can be
        /// used for a group of buttons. Automatically handles
        /// using end cap and mid styles based on the count.
        /// </summary>
        /// <param name="count">How many buttons will be in the button group</param>
        public static GUIStyle[] GetMiniButtonStyles(int count)
        {
            GUIStyle[] styles = new GUIStyle[count];
            for (int i = 0; i < count; i++)
            {
                if (count > 1)
                {
                    if (i == 0)
                    {
                        styles[i] = EditorStyles.miniButtonLeft;
                    }
                    else if (i == count - 1)
                    {
                        styles[i] = EditorStyles.miniButtonRight;
                    }
                    else
                    {
                        styles[i] = EditorStyles.miniButtonMid;
                    }
                }
                else
                {
                    styles[i] = EditorStyles.miniButton;
                }
            }
            return styles;
        }

        /// <summary>
        /// Returns whether the current text editor exists, this can be used
        /// to enable certain functionality in other tools
        /// </summary>
        public static bool TextEditorExists()
        {
            return System.IO.File.Exists(textEditor);
        }

        /// <summary>
        /// Opens the given file with the current text editor. Optionally
        /// supports a line number at which to open the file
        /// </summary>
        public static void OpenTextEditor(string filepath, int lineNumber = -1)
        {
            if (lineNumber >= 0)
            {
                filepath = filepath + ":" + lineNumber;
            }
            System.Diagnostics.Process.Start(textEditor, filepath);

        }

        [MenuItem("Tools/Clear Cache %&c", false, 2000)]
        static void FixAnimation()
        {
            Caching.ClearCache();
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            Debug.LogWarning("All cache cleared");
        }

        [MenuItem("Tools/Screenshot %&s", false, 2000)]
        static void CaptureScreen()
        {
            if (!Directory.Exists("Screenshots"))
            {
                Directory.CreateDirectory("Screenshots");
            }
            int index = 1;
            while(File.Exists("Screenshots\\" + index + ".png"))
            {
                index++;
            }
            ScreenCapture.CaptureScreenshot("Screenshots\\" + index + ".png", 2);
            Debug.LogWarning("All cache cleared");
        }

        [MenuItem("Assets/Export Sprites %&x", false, 2000)]
        static void FixAnimationParticleSystemCurrentScene()
        {
            Texture2D selectedTexture;
            if (Selection.objects.Length > 1)
            {
                selectedTexture = null;
            }
            else
            {
                selectedTexture = Selection.activeObject as Texture2D;
            }

            if (selectedTexture != null)
            {
                var texturePath = AssetDatabase.GetAssetPath(Selection.activeObject);
                Object[] allSprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(texturePath);
                if (allSprites != null)
                {
                    var path = @"Export/" + selectedTexture.name;
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    // Create a temporary RenderTexture of the same size as the texture
                    RenderTexture tmp = RenderTexture.GetTemporary(
                                        selectedTexture.width, selectedTexture.height,
                                        0,
                                        RenderTextureFormat.Default,
                                        RenderTextureReadWrite.Linear);

                    // Blit the pixels on texture to the RenderTexture
                    Graphics.Blit(selectedTexture, tmp);

                    // Backup the currently set RenderTexture
                    RenderTexture previous = RenderTexture.active;
                    // Set the current RenderTexture to the temporary one we created
                    RenderTexture.active = tmp; 

                    foreach (Sprite s in allSprites)
                    {
                        if (s != null)
                        {
                            Debug.LogWarning("Exporting " + s.name);
                            // Copy the reversed image data to a new texture.
                            var destTex = new Texture2D((int)s.textureRect.width, (int)s.textureRect.height, TextureFormat.RGBA32, false);

                            destTex.ReadPixels(new Rect(s.textureRect.x, s.textureRect.y, s.textureRect.width, s.textureRect.height), 0, 0, false); 
                            destTex.Apply();

                            System.IO.File.WriteAllBytes(System.IO.Path.Combine(path, s.name + ".png"), destTex.EncodeToPNG());
                        }
                    }
                    
                    // Reset the active RenderTexture
                    RenderTexture.active = previous;
                    // Release the temporary RenderTexture
                    RenderTexture.ReleaseTemporary(tmp);
                }
            }
        }
    }

}