using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Text.RegularExpressions;


namespace Moonbot.Workflow {

	/// <summary>
	/// Simple set of utils for searching any directory recursively
	/// for a specific string within any non-binary files. Searches are
	/// performed using <c>grep</c> or <c>findstr</c> (Windows).
	/// The search class itself is used to represent an active search.
	/// </summary>
	public class FileContentSearch {

		/// <summary>
		/// Regex pattern for splitting results from the raw command line searches
		/// </summary>
		private static Dictionary<RuntimePlatform, string> searchResultSplit = new Dictionary<RuntimePlatform, string>() {
			{RuntimePlatform.OSXEditor, ":"},
			{RuntimePlatform.WindowsEditor, ":\\d*:"},
		};

		/// <summary>
		/// Search command to execute
		/// </summary>
		private static Dictionary<RuntimePlatform, string> searchCmd = new Dictionary<RuntimePlatform, string>() {
			{RuntimePlatform.OSXEditor, "grep"},
			{RuntimePlatform.WindowsEditor, "findstr"},
		};

		/// <summary>
		/// Format for arg string passed to the search command
		/// </summary>
		private static Dictionary<RuntimePlatform, string> searchArgsFormat = new Dictionary<RuntimePlatform, string>() {
			{RuntimePlatform.OSXEditor, "-nRI {0} {1}"},
			{RuntimePlatform.WindowsEditor, "/p /s /n /c:\"{0}\" {1}/*.*"},
		};

		public static event System.Action<FileContentSearch> onFinish = delegate {};

		/// <summary>
		/// The string to search for
		/// </summary>
		public string searchStr;
		/// <summary>
		/// The directory to search within
		/// </summary>
		public string searchPath;
		/// <summary>
		/// Whether the operation has finished yet or not
		/// </summary>
		public bool finished;

		/// <summary>
		/// Represents a file that contained matches
		/// </summary>
		public class MatchFile {
			public string path;
			public List<Match> matches = new List<Match>();

			public int Count {
				get { return matches.Count; }
			}

			public MatchFile() {}
			public MatchFile(string path) {
				this.path = path;
			}

			public void Add(Match match) {
				matches.Add(match);
			}
		}

		/// <summary>
		/// Represents a single match within a file
		/// </summary>
		public class Match {
			public string matchStr;
			public int lineNumber;

			public Match() {}
			public Match(string matchStr, int lineNumber = -1) {
				this.matchStr = matchStr;
				this.lineNumber = lineNumber;
			}
		}

		/// <summary>
		/// List of MatchFiles representing all matches that were found
		/// </summary>
		public List<MatchFile> matches = new List<MatchFile>();

		public FileContentSearch(string searchStr, string searchPath) {
			this.searchStr = searchStr;
			this.searchPath = searchPath;
		}

		/// <summary>
		/// Start the search
		/// </summary>
		public void Start(bool async = true) {
			if (!System.IO.Directory.Exists(searchPath)) {
				Debug.LogWarning("Directory not found: " + searchPath);
				return;
			}
            ThreadStartSearch();

   //         Thread t = new Thread(() => ThreadStartSearch());
			//t.Start();
			//// not supporting async yet since we
			//// cant call unity methods from another thread
			//if (true) {
			//	t.Join();
			//}
			onFinish(this);
		}
		
		/// <summary>
		/// Add a discovered match to this search
		/// </summary>
		void AddMatch(string filePath, string matchStr, int lineNumber = -1) {
			MatchFile file = null;
			// check for existing match file
			for (int i = 0; i < matches.Count; i++) {
				if (matches[i].path.Equals(filePath)) {
					file = matches[i];
					break;
				}
			}
			// create new match file
			if (file == null) {
				file = new MatchFile(filePath);
				matches.Add(file);
			}
			// add new Match
			file.Add(new Match(matchStr, lineNumber));
		}

		void ThreadStartSearch() {
			// setup Process
			System.Diagnostics.Process process = new System.Diagnostics.Process();
			process.StartInfo.FileName = searchCmd[Application.platform];
			process.StartInfo.Arguments = string.Format(searchArgsFormat[Application.platform], searchStr, searchPath);
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			process.StartInfo.CreateNoWindow = true;
			process.OutputDataReceived += (sender, args) => DataReceivedHandler(args.Data);
			// start the process
			process.Start();
			process.BeginOutputReadLine();
			process.WaitForExit();
			finished = true;
		}

		/// <summary>
		/// Called whenever data is received from the search process
		/// </summary>
		void DataReceivedHandler(object data) {
			if (data != null) {
				// split at the first colon
				string[] split = Regex.Split((string)data, searchResultSplit[Application.platform]);
				if (Application.platform == RuntimePlatform.OSXEditor) {
					// second item is the line number with grep
					AddMatch(split[0], split[2], int.Parse(split[1]));
				} else {
					AddMatch(split[0], split[1]);
				}
			}
		}
		
	}

}