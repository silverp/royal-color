using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

namespace Moonbot.Workflow {

	/// <summary>
	/// A QuickSelectWindow that can be used to quickly open
	/// any scene listed in the projects build settings
	/// </summary>
	public class SceneLauncher : QuickSelectWindow {

		public class SceneItem : QuickSelectItem {

			public string path;

			public override string name {
				get { return System.IO.Path.GetFileNameWithoutExtension(path); }
			}

			public SceneItem(string path) {
				this.path = path;
			}
		}

		[MenuItem("File/Scene Launcher... %#t")]
		static void Init() {
			QuickSelectWindow.Create<SceneLauncher>();
		}

		/// <summary>
		/// Returns the current list of scenes as SceneItems from the EditorBuildSettings
		/// </summary>
		public override QuickSelectItem[] GetItems() {
			List<SceneItem> objs = new List<SceneItem>();
			foreach (UnityEditor.EditorBuildSettingsScene s in UnityEditor.EditorBuildSettings.scenes) {
				if (s.enabled) {
					objs.Add(new SceneItem(s.path));
				}
			}
			return objs.ToArray();
		}

		/// <summary>
		/// Prompts the user to save the current scene if needed, then
		/// opens the scene represented by the given item
		/// </summary>
		public override void SelectItem(QuickSelectItem item) {
			SceneItem scene = item as SceneItem;
			if (scene != null) {
				base.SelectItem(item);
				if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) {
					EditorSceneManager.OpenScene(scene.path);
				}
			}
		}

	}

}