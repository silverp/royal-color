using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace Moonbot.Workflow {

	/// <summary>
	/// An editor tool that can be used to replace one or more objects
	/// in the scene with prefabs from the project. Has two modes,
	/// the Default mode replaces all selected objects with a single prefab,
	/// and the SearchAndReplace mode uses the names of the selected objects
	/// to determine which prefab to use from a list of prefabs.
	/// An option exists to more loosely match the names of the selected
	/// objects against the prefab names where necessary.
	/// </summary>
	public class GameObjectReplacer : EditorWindow {

		[MenuItem("Window/GameObject Replacer", false, 1000)]
		public static void ShowWindow() {
			var win = EditorWindow.GetWindow<GameObjectReplacer>(true, "GameObject Replacer");
			win.autoRepaintOnSceneChange = true;
		}


		public enum Tab {
			Default, SearchAndReplace
		};
		/// <summary>
		/// The currently selected tab of the GameObjectReplacer window
		/// </summary>
		public static Tab selectedTab;
		
		/// <summary>
		/// Whether objects in the hierarchy should keep their names, or be
		/// replace by the name of the prefab that is used to replace them
		/// </summary>
		public bool keepName;
		/// <summary>
		/// Whether objects in the hierarchy should keep their children or not.
		/// When true, children of the objects being replaced will be parented
		/// under the new prefab instances
		/// </summary>
		public bool keepChildren;

		/// <summary>
		/// The prefab to use when replacing all selected objects in Default mode
		/// </summary>
		public GameObject prefab;

		/// <summary>
		/// Whether to match names exactly in SearchAndReplace mode or not.
		/// When disabled, the prefab names need only contain the selected object's
		/// name, not match it exactly
		/// </summary>
		public bool exactNameMatch = true;
		/// <summary>
		/// The list of prefabs to use when using SearchAndReplace mode
		/// </summary>
		public GameObject[] prefabs;

		private HelpMessages help = new HelpMessages();

		private SerializedObject serializedObject;
		private SerializedProperty keepNameProp;
		private SerializedProperty keepChildrenProp;
		private SerializedProperty prefabProp;
		private SerializedProperty exactNameMatchProp;
		private SerializedProperty prefabsProp;

		private const string providePrefabMsg = "Set the target prefab to begin";
		private const string providePrefabsMsg = "Provide one or more prefabs to begin";
		private const string selectHierarchyMsg = "Select objects in the hierarchy";
		private const string selectHierarchyWarn = "No objects were selected in the hierarchy";

		private class SearchAndReplaceStats {
			/// <summary>
			/// Number of uniquely named objects selected
			/// </summary>
			public int uniqueNameCount;
			/// <summary>
			/// Number of objects that were unmatched against prefabs
			/// </summary>
			public int unmatchedCount;
			/// <summary>
			/// Number of prefabs that will not be used
			/// </summary>
			public int unusedPrefabCount;
		}

		private SearchAndReplaceStats searchAndReplaceStats;

		void OnEnable() {
			prefabs = new GameObject[]{};
			serializedObject = new SerializedObject(this);
			keepNameProp = serializedObject.FindProperty("keepName");
			keepChildrenProp = serializedObject.FindProperty("keepChildren");
			prefabProp = serializedObject.FindProperty("prefab");
			exactNameMatchProp = serializedObject.FindProperty("exactNameMatch");
			prefabsProp = serializedObject.FindProperty("prefabs");
			searchAndReplaceStats = new SearchAndReplaceStats();
			Clear();
		}

		void Clear() {
			help.Clear();
			prefabProp.objectReferenceValue = null;
			exactNameMatchProp.boolValue = true;
			prefabsProp.ClearArray();
		}

		void OnSelectionChange() {
			help.Clear();
			UpdateSearchAndReplacePreview();
			Repaint();
		}

		void OnGUI() {
			GUILayout.BeginHorizontal(EditorStyles.toolbar);
			if (GUILayout.Button("Clear", EditorStyles.toolbarButton)) {
				Clear();
			}
			GUILayout.Space(6f);
			selectedTab = (Tab)GUILayout.Toolbar((int)selectedTab, new string[] { "Default", "Search and Replace" }, EditorStyles.toolbarButton);
			GUILayout.FlexibleSpace();
			help.enabled = GUILayout.Toggle(help.enabled, "Help", EditorStyles.toolbarButton);
			GUILayout.EndHorizontal();

			GUILayout.Space(6f);

			switch (selectedTab) {
				case Tab.Default:
					DrawDefault();
					break;
				case Tab.SearchAndReplace:
					DrawSearchAndReplace();
					break;
			}

			serializedObject.ApplyModifiedProperties();
		}

		void DrawDefault() {
			help.Draw("Replace all selected objects in the hierarchy with a single prefab", MessageType.Info, true);
			
			DrawKeepProps();

			EditorGUILayout.PropertyField(prefabProp);
			help.Draw("The prefab to instantiate for all selected objects");

			// some preview statistics
			help.Clear();
			if (prefab != null) {
				int count = SelectedGameObjects().Length;
				if (count > 0) {
					help.AddInfo(string.Format("Will replace {0} object(s) with: {1}", count, prefab.name), true);
				} else {
					help.Add(selectHierarchyMsg, true);
				}
			} else {
				help.Add(providePrefabMsg, true);
			}

			DrawButtons();
		}

		void DrawSearchAndReplace() {
			help.Draw("Search and replace selected objects in the hierarchy with one or more prefabs", MessageType.Info, true);
			
			DrawKeepProps();
			
			EditorGUILayout.PropertyField(exactNameMatchProp);
			string msg = "When true, prefab names must be exactly the same as objects they replace. ";
			msg += "When false, prefab names must only contain part of objects' names. ";
			msg += "Note that the order of the prefabs list matters, first matched prefab will be used.\n\n";
			msg += "example: when false, 'tree' will match against a prefab named 'big_tree' or 'tree_small'";
			help.Draw(msg);

			EditorGUILayout.PropertyField(prefabsProp, true);

			// some preview statistics
			help.Clear();
			if (prefabs.Length > 0) {
				int count = SelectedGameObjects().Length;
				if (count > 0) {
					UpdateSearchAndReplacePreview();
					help.AddInfo("Number of uniquely named objects selected: " + searchAndReplaceStats.uniqueNameCount, true);
					if (searchAndReplaceStats.unmatchedCount > 0) {
						help.AddWarning(string.Format("No match found for {0} objects", searchAndReplaceStats.unmatchedCount), true);
					}
					if (searchAndReplaceStats.unusedPrefabCount > 0) {
						help.AddWarning(string.Format("{0} prefabs will not be used", searchAndReplaceStats.unusedPrefabCount), true);
					}
				} else {
					help.Add(selectHierarchyMsg, true);
				}
			} else {
				help.Add(providePrefabsMsg, true);
			}

			DrawButtons();
		}

		void DrawKeepProps() {
			EditorGUILayout.PropertyField(keepNameProp);
			help.Draw("When enabled, all new instances will preserve the name of the objects they replace");

			EditorGUILayout.PropertyField(keepChildrenProp);
			help.Draw("When enabled, keeps children of the replaced objects, and simply reparents them to the new instances");
		}


		void DrawButtons() {
			GUILayout.FlexibleSpace();
			help.DrawAll();
			// show a button for selecting unmatched objects
			if (selectedTab == Tab.SearchAndReplace) {
				GUI.enabled = searchAndReplaceStats.unmatchedCount > 0;
				if (GUILayout.Button("Select Unmatched")) {
					SelectUnmatched();
				}
				GUI.enabled = true;
			}
			if (GUILayout.Button("Replace")) {
				switch (selectedTab) {
					case Tab.Default:
						RunDefault();
						break;
					case Tab.SearchAndReplace:
						RunSearchAndReplace();
						break;
				}
			}
			GUILayout.Space(12f);
		}

		void UpdateSearchAndReplacePreview() {
			searchAndReplaceStats.uniqueNameCount = 0;
			searchAndReplaceStats.unmatchedCount = 0;
			searchAndReplaceStats.unusedPrefabCount = 0;

			GameObject[] objs = SelectedGameObjects();
			string[] names = System.Array.ConvertAll(objs, i => i.name);
			GameObject[] matches = GetMatchingPrefabs(names, prefabs);

			int unmatched = 0;
			List<GameObject> usedPrefabs = new List<GameObject>();
			for (int i = 0; i < matches.Length; i++) {
				if (matches[i] == null) {
					unmatched++;
				} else {
					if (!usedPrefabs.Contains(matches[i])) {
						usedPrefabs.Add(matches[i]);
					}
				}
			}
			searchAndReplaceStats.uniqueNameCount = names.Distinct().Count();
			searchAndReplaceStats.unmatchedCount = unmatched;
			searchAndReplaceStats.unusedPrefabCount = prefabs.Length - usedPrefabs.Count;
		}

		void SelectUnmatched() {
			GameObject[] objs = SelectedGameObjects();
			string[] names = System.Array.ConvertAll(objs, i => i.name);
			GameObject[] matches = GetMatchingPrefabs(names, prefabs);

			List<GameObject> unmatched = new List<GameObject>();
			for (int i = 0; i < objs.Length; i++) {
				if (matches[i] == null) {
					unmatched.Add(objs[i]);
				}
			}
			Selection.objects = (Object[])unmatched.ToArray();
		}

		/// <summary>
		/// Run the standard replace operation, replaces
		/// all selected objects with one prefab
		/// </summary>
		public void RunDefault() {
			help.Clear();
			if (prefab == null) {
				help.AddWarning("No prefab was given", true);
				return;
			}
			GameObject[] objs = SelectedGameObjects();
			if (objs.Length == 0) {
				help.AddWarning(selectHierarchyWarn, true);
				return;
			}
			List<GameObject> newObjs = new List<GameObject>();
			foreach (GameObject go in objs) {
				newObjs.Add(ReplaceWithPrefab(go, prefab));
			}
			Selection.objects = (Object[])newObjs.ToArray();
		}
		
		/// <summary>
		/// Run the search and replace operation,
		/// getting list of matching prefabs and replacing each
		/// </summary>
		public void RunSearchAndReplace() {
			help.Clear();
			if (prefabs.Length == 0) {
				help.AddWarning("No prefabs were given", true);
				return;
			}
			GameObject[] objs = SelectedGameObjects();
			if (objs.Length == 0) {
				help.AddWarning(selectHierarchyWarn, true);
				return;
			}
			string[] names = System.Array.ConvertAll(objs, i => i.name);
			// search within given list of prefabs
			GameObject[] matches = GetMatchingPrefabs(names, prefabs);
			List<GameObject> unmatched = new List<GameObject>();

			for (int i = 0; i < objs.Length; i++) {
				if (matches[i] != null) {
					ReplaceWithPrefab(objs[i], matches[i]);
				} else {
					unmatched.Add(objs[i]);
				}
			}
			Selection.objects = (Object[])unmatched.ToArray();
		}

		GameObject[] SelectedGameObjects() {
			return System.Array.ConvertAll(Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab), i => i.gameObject).ToArray();
		}


		GameObject[] GetMatchingPrefabs(string[] names, GameObject[] prefabs) {
			return GetMatchingPrefabs(names, prefabs, exactNameMatch);
		}

		GameObject[] GetMatchingPrefabs(string[] names, GameObject[] prefabs, bool exactNameMatch) {
			return System.Array.ConvertAll(names, i => FindMatchingPrefab(i, prefabs, exactNameMatch)).ToArray();
		}

		/// <summary>
		/// Find the best matched prefab within the given list for the given name
		/// will simply check for the name in the prefabs name if exactNameMatch is false
		/// </summary>
		GameObject FindMatchingPrefab(string name, GameObject[] prefabs, bool exactNameMatch) {
			foreach (GameObject prefab in prefabs) {
				if (prefab == null) {
					continue;
				}
				if (exactNameMatch) {
					if (name.Equals(prefab.name)) {
						return prefab;
					}
				} else {
					if (prefab.name.Contains(name)) {
						return prefab;
					}
				}
			}
			return null;
		}
		
		GameObject ReplaceWithPrefab(GameObject obj, Object prefab) {
			// instantiate new prefab
			GameObject newObj = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
			Undo.RegisterCreatedObjectUndo(newObj, "Replace Objects");
			// make sibling
			newObj.transform.parent = obj.transform.parent;
			// match transform
			newObj.transform.localPosition = obj.transform.localPosition;
			newObj.transform.localRotation = obj.transform.localRotation;
			newObj.transform.localScale = obj.transform.localScale;

			// keep sibling index
			#if UNITY_4_5 || UNITY_4_6
			newObj.transform.SetSiblingIndex(obj.transform.GetSiblingIndex());
			#endif

			// reparent children
			if (keepChildren) {
				foreach (Transform t in obj.transform) {
					Undo.SetTransformParent(t, newObj.transform, "Replace Objects");
				}
			}
			// rename
			if (keepName) {
				newObj.name = obj.name;
			}

			// delete old
			Undo.DestroyObjectImmediate(obj);

			return newObj;
		}
	}

}